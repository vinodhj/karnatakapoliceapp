﻿using System;
using Examroom.KarnatakaStatePolice.DataAccess.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Examroom.KarnatakaStatePolice.DataAccess
{
    public partial class KspContext : DbContext
    {
        public KspContext(DbContextOptions<KspContext> options)
            : base(options)
        {
        }

        public virtual DbSet<DepartmentMapping> DepartmentMapping { get; set; }
        public virtual DbSet<FormMapping> FormMapping { get; set; }
        public virtual DbSet<FormMaster> FormMaster { get; set; }
        public virtual DbSet<UserToDepartmentMapping> UserToDepartmentMapping { get; set; }
        public virtual DbSet<Role> Role { get; set; }
        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<UserForm> UserForm { get; set; }
        public virtual DbSet<UserRole> UserRole { get; set; }
        public virtual DbSet<ErrorLog> ErrorLog { get; set; } 

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<DepartmentMapping>(entity =>
            {
                entity.ToTable("DepartmentMapping", "KSP");



                entity.Property(e => e.CreatedDatetime).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDatetime).HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.HasOne(d => d.Parent)
                    .WithMany(p => p.InverseParent)
                    .HasForeignKey(d => d.ParentId)
                    .HasConstraintName("FK__Departmen__Paren__46E78A0C");

                entity.HasOne(x => x.Created)
                    .WithMany(p => p.DepartmentMappingCreated)
                    .HasForeignKey(d => d.CreatedBy)
                    .HasConstraintName("FK__Departmen__Creat__0A9D95DB");

                entity.HasOne(x => x.Modified)
                   .WithMany(p => p.DepartmentMappingModified)
                   .HasForeignKey(d => d.ModifiedBy)
                   .HasConstraintName("FK__Departmen__Modif__0B91BA14");


               
            });

            modelBuilder.Entity<UserToDepartmentMapping>(entity =>
            {
                entity.ToTable("UserToDepartmentMapping", "KSP");

                entity.Property(e => e.CreatedDatetime).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDatetime).HasColumnType("datetime");

            });


            modelBuilder.Entity<FormMapping>(entity =>
            {
                entity.ToTable("FormMapping", "KSP");

                entity.Property(e => e.CreatedDatetime).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDatetime).HasColumnType("datetime");

                entity.HasOne(d => d.Department)
                    .WithMany(p => p.FormMapping)
                    .HasForeignKey(d => d.DepartmentId)
                    .HasConstraintName("FK__FormMappi__Depar__48CFD27E");

                entity.HasOne(d => d.Form)
                    .WithMany(p => p.FormMapping)
                    .HasForeignKey(d => d.FormId)
                    .HasConstraintName("FK__FormMappi__FormI__47DBAE45");
            });


            modelBuilder.Entity<User>(entity =>
            {
                entity.ToTable("User", "KSP");

                entity.HasIndex(e => e.Pseudoname)
                    .HasName("Pseudoname");

                entity.HasIndex(e => new { e.Firstname, e.Lastname })
                    .HasName("Firstname_Lastname");

                entity.Property(e => e.CreatedDatetime)
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getutcdate())");

                entity.Property(e => e.EmailId)
                    .IsRequired()
                    .HasMaxLength(128);

                entity.Property(e => e.Firstname)
                    .IsRequired()
                    .HasMaxLength(64);

                entity.Property(e => e.IsActive)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedDatetime)
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getutcdate())");

                entity.Property(e => e.Lastname)
                    .IsRequired()
                    .HasMaxLength(64);

                entity.Property(e => e.PasswordHash)
                    .IsRequired()
                    .HasMaxLength(256);

                entity.Property(e => e.PasswordResetRequired)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.PasswordSalt)
                    .IsRequired()
                    .HasMaxLength(256);

                entity.Property(e => e.Phone)
                    .IsRequired()
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.Pseudoname).HasMaxLength(64);

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasMaxLength(64);
            });


            modelBuilder.Entity<FormMaster>(entity =>
            {
                entity.ToTable("FormMaster", "KSP");

                entity.HasIndex(e => e.Code)
                    .HasName("UQ__FormMast__A25C5AA7874C18D8")
                    .IsUnique();

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IsPublished);

                entity.Property(e => e.Content)
                        .IsUnicode();

                entity.Property(e => e.CreatedDatetime).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDatetime).HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false);


                entity.HasOne(x => x.Created)
                    .WithMany(p => p.FormMaster)
                    .HasForeignKey(d => d.CreatedBy)
                    .HasConstraintName("FK__FormMaste__Creat__05D8E0BE");

                entity.HasOne(x => x.Modified)
                   .WithMany(p => p.FormMasterModified)
                   .HasForeignKey(d => d.ModifiedBy)
                   .HasConstraintName("FK__FormMaste__Modif__06CD04F7");

                entity.HasOne(x => x.TemplateMapping)
                        .WithMany(p => p.FormsMaster)
                        .HasForeignKey(x => x.TemplateType)
                        .HasConstraintName("FK__FormMaste__Templ__0C85DE4D");

               

            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.ToTable("Role", "KSP");

                entity.Property(e => e.CreatedDatetime)
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getutcdate())");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(64);

                entity.Property(e => e.IsActive)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.IsDeleted)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedDatetime)
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getutcdate())");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(64);

            });

            modelBuilder.Entity<UserForm>(entity =>
            {
                entity.ToTable("UserForm", "KSP");

                entity.Property(e => e.CreatedDatetime).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDatetime).HasColumnType("datetime");


                entity.HasOne(x => x.User)
                        .WithMany(x => x.UserForms)
                        .HasForeignKey(x => x.UserId)
                        .HasConstraintName("FK__UserForm__UserId__07C12930");

                entity.HasOne(x => x.Created)
                        .WithMany(x => x.UserFormsCreated)
                        .HasForeignKey(x => x.CreatedBy)
                        .HasConstraintName("FK__UserForm__Create__08B54D69");

                entity.HasOne(x => x.Modified)
                        .WithMany(x => x.UserFormsModified)
                        .HasForeignKey(x => x.ModifiedBy)
                        .HasConstraintName("FK__UserForm__Modifi__09A971A2");

                entity.HasOne(x => x.FormMaster)
                     .WithMany(x => x.UserForm)
                     .HasForeignKey(x => x.FormId)
                     .HasConstraintName("FK_UserForm_FormMaster_Id");

                entity.HasOne(x => x.PoliceStation)
                       .WithMany(p => p.UserForms)
                       .HasForeignKey(x => x.PoliceStationNo);

                entity
                    .HasOne(x => x.FormStatus)
                    .WithMany(p => p.UserFormStatus)
                    .HasForeignKey(x => x.StatusId); 

            });

            modelBuilder.Entity<ErrorLog>(entity =>
            {
                entity.ToTable("ErrorLog", "KSP");

                entity.Property(e => e.CreatedDatetime).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDatetime).HasColumnType("datetime"); 
            });


            modelBuilder.Entity<UserRole>(entity =>
            {
                entity.ToTable("UserRole", "KSP");

                entity.HasIndex(e => new { e.UserId, e.RoleId })
                    .HasName("User_Role")
                    .IsUnique();

                entity.Property(e => e.CreatedDatetime)
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getutcdate())");

                entity.Property(e => e.IsAppRole)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.IsActive)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedDatetime)
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getutcdate())");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
