﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Examroom.KarnatakaStatePolice.DataAccess.Models
{
    public partial class DepartmentMapping : IDatabaseModel
    {
        public DepartmentMapping()
        {
            FormMapping = new HashSet<FormMapping>();
            InverseParent = new HashSet<DepartmentMapping>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public int? LevelId { get; set; }
        public int? ParentId { get; set; }
        public int? OrderNo { get; set; }
        public int CreatedBy { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedDatetime { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime ModifiedDatetime { get; set; }
        public virtual User Created { get; set; }
        public virtual User Modified { get; set; }
        public virtual DepartmentMapping Parent { get; set; }
        public virtual ICollection<FormMapping> FormMapping { get; set; }
        public virtual ICollection<DepartmentMapping> InverseParent { get; set; }
        public virtual ICollection<FormMaster> FormsMaster { get; set; }
        public virtual ICollection<UserForm> UserForms { get; set; }
        public virtual ICollection<UserForm> UserFormStatus { get; set; }
    }
}
