﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Examroom.KarnatakaStatePolice.DataAccess.Models
{
    public partial class ErrorLog : IDatabaseModel
    {
        public int Id { get; set; }
        public string PageURL { get; set; }
        public string MethodName { get; set; }
        public string Module { get; set; }
        public string ErrorMessage { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDatetime { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime ModifiedDatetime { get; set; } 
    }
}
