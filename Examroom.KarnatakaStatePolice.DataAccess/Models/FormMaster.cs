﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Examroom.KarnatakaStatePolice.DataAccess.Models
{
    public partial class FormMaster : IDatabaseModel
    {
        public FormMaster()
        {
            FormMapping = new HashSet<FormMapping>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string FileName { get; set; }
        public string SystemFileName { get; set; }
        public string Code { get; set; }
        public string Content { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDatetime { get; set; }
        public int TemplateType { get; set; }
        public bool IsPublished { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime ModifiedDatetime { get; set; }
        public virtual User Created { get; set; }
        public virtual User Modified { get; set; }
        public virtual ICollection<UserForm> UserForm { get; set; }
        public virtual DepartmentMapping TemplateMapping { get; set; }
        public virtual ICollection<FormMapping> FormMapping { get; set; }
    }
}
