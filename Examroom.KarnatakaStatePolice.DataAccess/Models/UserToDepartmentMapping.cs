﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Examroom.KarnatakaStatePolice.DataAccess.Models
{
    public partial class UserToDepartmentMapping : IDatabaseModel
    {
        public UserToDepartmentMapping()
        {

        }

        public int Id { get; set; }
        public int UserId { get; set; }
        public int PoliceStationId { get; set; }
        public int CreatedBy { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedDatetime { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime ModifiedDatetime { get; set; }

        public virtual DepartmentMapping PoliceStation { get; set; }
        public virtual User User { get; set; }
    }
}
