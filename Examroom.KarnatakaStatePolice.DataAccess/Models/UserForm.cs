﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Examroom.KarnatakaStatePolice.DataAccess.Models
{
    public partial class UserForm : IDatabaseModel
    {
        public int Id { get; set; }
        public int? FormId { get; set; }
        public int? UserId { get; set; }
        public int?StatusId { get; set; }
        public string Comment { get; set; }
        public string UserInput { get; set; }
        public string Name { get; set; }
        public string FileName { get; set; }
        public string SystemFileName { get; set; }
        public int? PoliceStationNo { get; set; }
        public string CandidateName { get; set; }
        public string CandidatePhoneNo { get; set; }
        public string Code { get; set; }
        public string CandidateEmailId { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDatetime { get; set; }
        public int ModifiedBy { get; set; }
        public string UserInputBase64 { get; set; }
        public DateTime ModifiedDatetime { get; set; }
        public FormMaster FormMaster { get; set; }
        public virtual DepartmentMapping PoliceStation { get; set; }
        public virtual DepartmentMapping FormStatus { get; set; }
        public User User { get; set; }
        public User Created { get; set; }
        public User Modified { get; set; }
    }
}
