﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Examroom.KarnatakaStatePolice.DataAccess.Models
{
    public partial class FormMapping : IDatabaseModel
    {
        public int Id { get; set; }
        public int? FormId { get; set; }
        public int? DepartmentId { get; set; }
        public bool? IsActive { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDatetime { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime ModifiedDatetime { get; set; }
        public virtual DepartmentMapping Department { get; set; }
        public virtual FormMaster Form { get; set; }
    }
}
