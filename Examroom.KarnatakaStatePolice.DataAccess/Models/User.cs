﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Examroom.KarnatakaStatePolice.DataAccess.Models
{
    public partial class User : IDatabaseModel
    {
        public User()
        {
            UserRole = new HashSet<UserRole>();
        }

        public int Id { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Username { get; set; }
        public string Pseudoname { get; set; }
        public string EmailId { get; set; }
        public byte[] PasswordHash { get; set; }
        public byte[] PasswordSalt { get; set; }
        public bool? PasswordResetRequired { get; set; }
        public string Phone { get; set; }
        //public string CountryCode { get; set; }
        public string ImageUrl { get; set; }
        public bool? IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDatetime { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime ModifiedDatetime { get; set; }
        public string Otp { get; set; }
        public virtual ICollection<UserToDepartmentMapping> UserToDepartmentMapping { get; set; }
        public virtual ICollection<UserRole> UserRole { get; set; }
        public virtual ICollection<FormMaster> FormMaster { get; set; }
        public virtual ICollection<FormMaster> FormMasterModified { get; set; }
        public virtual ICollection<UserForm> UserForms { get; set; }
        public virtual ICollection<UserForm> UserFormsCreated { get; set; }
        public virtual ICollection<UserForm> UserFormsModified { get; set; }
        public virtual ICollection<DepartmentMapping> DepartmentMappingCreated { get; set; }
        public virtual ICollection<DepartmentMapping> DepartmentMappingModified { get; set; }
    }
}
