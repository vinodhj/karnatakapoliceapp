﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Examroom.KarnatakaStatePolice.DataAccess
{
    public static class Constants
    {
        public const string Department = "Place-Name";
        public const string SubDivision = "Sub-Division";
        public const string PoliceStation = "Police-Station";

        public const string FormStatus_Approved = "Approved";
        public const string FormStatus_Draft = "Draft";
        public const string FormStatus_Rejected = "Rejected";
        public const string FormStatus_Submitted = "Submitted";

        public const string TemplateStatus_Published = "Published";
        public const string TemplateStatus_Unpublished = "Unpublished";

        public const string Orientation_Horizontal = "horizontal";
        public const string Orientation_Vertical = "vertical";


    }
}
