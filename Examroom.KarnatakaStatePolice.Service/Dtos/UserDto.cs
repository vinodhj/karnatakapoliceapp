﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Examroom.KarnatakaStatePolice.Service.Dtos
{
    public class UserShortDto
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string EmailId { get; set; }

    }
    public class UserDto : UserShortDto
    {
        public int UserRoleId { get; set; }
        public int RoleId { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Password { get; set; }
        public string Phone { get; set; }
        public string Token { get; set; }
        public int PoliceStationId { get; set; }
        public string PoliceStationName { get; set; }
        public bool? IsActive { get; set; }
        public string RoleName { get; set; }
        public virtual ICollection<UserRoleDto> UserRole { get; set; }
        public virtual ICollection<UserToDepartmentMappingDto> UserToDepartmentMapping { get; set; }

    }
    public class UpdateUserStatusDto
    {
        public int Id { get; set; }
        public bool IsActive { get; set; }
    }

    public class UpdatePasswordDto
    {
        public int Id { get; set; }
        public string NewPassword { get; set; }
    }

    public class AddUserDto
    {
        public int Id { get; set; }
        public int RoleId { get; set; }
        public string Username { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Password { get; set; }
        public string EmailId { get; set; }
        public string Phone { get; set; }
        public int? PoliceStationId { get; set; }
        public string PoliceStationsIds { get; set; }
    }

    public class VertifyOtpDto
    {
        public int UserId { get; set; }
        public string Otp { get; set; }
    }
}
