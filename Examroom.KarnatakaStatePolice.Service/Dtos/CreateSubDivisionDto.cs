﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Examroom.KarnatakaStatePolice.Service.Dtos
{
    /// <summary>
    /// Defines the <see cref="CreateSubDivisionDto" />.
    /// </summary>
    public class CreateSubDivisionDto
    {
        /// <summary>
        /// Defines the Value.
        /// </summary>
        [Required(ErrorMessage = "Value is required field.")]
        [MaxLength(250, ErrorMessage ="Maximum lenght is 250 characters.")]
        public string Value { get; set; }
        /// <summary>
        /// Defines the OrderNo.
        /// </summary>
        public int? OrderNo { get; set; }
        /// <summary>
        /// Defines the ParentId.
        /// </summary>
        [Required(ErrorMessage ="Sub-Division must have a parent Division.")]
        public int ParentId { get; set; }
    }
}
