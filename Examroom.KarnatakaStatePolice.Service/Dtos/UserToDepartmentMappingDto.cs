﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Examroom.KarnatakaStatePolice.Service.Dtos
{
    public class UserToDepartmentMappingDto
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int PoliceStationId { get; set; }
        public bool IsActive { get; set; }

        public virtual RoleDto PoliceStation { get; set; }
        //public virtual User User { get; set; }
    }
}
