﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Examroom.KarnatakaStatePolice.Service.Dtos
{
    public class TemplateDto
    {
        public string Name { get; set; }
        public int Id { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedById { get; set; }
        public string CreatedBy { get; set; }
        public string TemplateType { get; set; }
        public int ModifiedId { get; set; }
        public string ModifiedBy { get; set; }
        public bool IsPublished { get; set; }
        public DateTime ModifiedOn { get; set; }
        public int Count { get; set; }
        public List<RoleDto> Departments { get; set; }   
    }
    public class SetTemplateStatusDto
    {
        public int Id { get; set; }
        public bool IsPublished { get; set; }
    }
    public class UpdateDetartmentDto
    {
        public int Id { get; set; }
        public string PoliceStationIds { get; set; }
    }
}
