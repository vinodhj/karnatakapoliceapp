﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Examroom.KarnatakaStatePolice.Service.Dtos
{
    public class RoleDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public int? LevelId { get; set; }
        public int? ParentId { get; set; }
        public int? OrderNo { get; set; }
        public bool IsActive { get; set; }
        public RoleDto Parent { get; set; }
    }
    

}
