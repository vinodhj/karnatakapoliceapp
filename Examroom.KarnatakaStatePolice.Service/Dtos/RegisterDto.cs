﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Examroom.KarnatakaStatePolice.Service.Dtos
{
    public class RegisterDto
    {
        public int Id { get; set; }
        public int RoleId { get; set; }
        public int UserRoleId { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string EmailId { get; set; }
        [Required]
        public string Username { get; set; }
        [Required]
        public string Password { get; set; }
        public bool PasswordResetRequired { get; set; }
        public string CountryCode { get; set; }
        public string Phone { get; set; }
        public string Pseudoname { get; set; }
        public bool IsActive { get; set; }
    }
}
