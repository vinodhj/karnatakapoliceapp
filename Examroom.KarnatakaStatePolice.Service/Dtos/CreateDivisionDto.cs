﻿using System.ComponentModel.DataAnnotations;

namespace Examroom.KarnatakaStatePolice.Service.Dtos
{
    /// <summary>
    /// Defines the <see cref="CreateDivisionDto" />.
    /// </summary>
    public class CreateDivisionDto
    {
        /// <summary>
        /// Defines the Value.
        /// </summary>
        [Required(ErrorMessage = "Field is required.")]
        [MaxLength(250, ErrorMessage = "Maximum length for the field is 250.")]
        public string Value { get; set; } = null;

        /// <summary>
        /// Defines the ParentId.
        /// </summary>
        public int? ParentId { get; set; } = null;

        /// <summary>
        /// Defines the OrderNo.
        /// </summary>
        public int? OrderNo { get; set; } = null;
    }
}