﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Examroom.KarnatakaStatePolice.Service.Dtos
{
    public class DownloadContentDto
    {
        public Stream Stream { get; set; }
        public byte[] FileInBytes { get; set; }
        public string FileName { get; set; }
        public string ContentType { get; set; }
    }
}
