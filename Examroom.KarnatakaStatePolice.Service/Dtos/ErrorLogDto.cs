﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Examroom.KarnatakaStatePolice.Service.Dtos
{
    public class ErrorLogDto
    {
        public int Id { get; set; }
        public string PageURL { get; set; }
        public string MethodName { get; set; }
        public string Module { get; set; }
        public string ErrorMessage { get; set; } 
        public int CreatedBy { get; set; } 
    }
}
