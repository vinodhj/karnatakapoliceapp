﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Examroom.KarnatakaStatePolice.Service.Dtos
{
    public class BlobDto
    {
        public byte[] Content { get; set; }
        public string Name { get; set; }
    }
}
