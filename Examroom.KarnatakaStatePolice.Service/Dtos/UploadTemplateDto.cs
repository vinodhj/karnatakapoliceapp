﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Examroom.KarnatakaStatePolice.Service.Dtos
{
    public class UploadTemplateDto
    {
        public string Name { get; set; }
        public byte[] Content { get; set; }
    }
}
