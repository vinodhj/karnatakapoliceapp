﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Examroom.KarnatakaStatePolice.Service.Dtos
{
    public class UserFormDto
    {
        public int Id { get; set; }
        public int FormId { get; set; }
        public int StatusId { get; set; }
        public string TemplateName { get; set; }
        public string FormName { get; set; }
        public string TemplateType { get; set; }
        public string CreatedBy { get; set; }
        public string Status { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string PoliceStationName { get; set; }
        public string Comment { get; set; }
        public int PoliceStationId { get; set; }
    }

    public class UserFormSummaryDto
    {
        public int FormId { get; set; }
        public string FormType { get; set; }
        public string FormName { get; set; }
        public int[] PoliceStationIds { get; set; }
        public string[] PoliceStations { get; set; }
        public int Count { get; set; }
        public string FormStatus { get; set; }

    }

    public class UserFormDetailDto
    {
        public int UserFormId { get; set; }
        public int FormId { get; set; }
        public string FormType { get; set; }
        public string FormName { get; set; }
        public string Code { get; set; }
 
        //public string CandidateName { get; set; }
        //public string CandidateEmailId { get; set; }
        //public string CandidatePhoneNo { get; set; }

        public string Status { get; set; }
        public string PoliceStationName { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }

        public DateTime ModifiedOn { get; set; }
        public string Comment { get; set; }
        public bool ShowSubmitButton { get; set; }
        public bool ShowReviewButton { get; set; }


    }

    public class ActionOnFromDto
    {
        public int UserFormId { get; set; }
        public string Status { get; set; } // AP, RJ, SU
        public string Comment { get; set; }

    }
}
