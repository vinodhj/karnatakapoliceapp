﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Examroom.KarnatakaStatePolice.Service.Dtos
{
    public class ExcelStyle
    {
        public string Align { get; set; }
        public ExcelBorder Border { get; set; }
        public string Bgcolor { get; set; }
        public string Color { get; set; }

    }
    public class ExcelBorder
    {
        public string[] Bottom { get; set; }
        public string[] Top { get; set; }
        public string[] Left { get; set; }
        public string[] Right { get; set; }


    }

    public class ExcelContentDto
    {
        public string Name { get; set; }
        public string Freeze { get; set; }
        public ExcelStyle[] Styles { get; set; }
        public string[] Merges { get; set; }
        public Dictionary<string, object> Rows { get; set; }
    }

    public class ExcelRow
    {
        public Dictionary<string, ExcelCell> Cells { get; set; }
    }

    public class ExcelCell
    {
        public string Text { get; set; }
        public int Style { get; set; }
    }
}
