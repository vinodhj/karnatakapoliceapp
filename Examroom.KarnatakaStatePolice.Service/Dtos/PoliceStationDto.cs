﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Examroom.KarnatakaStatePolice.Service.Dtos
{
    public class PoliceStationDto
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        [MaxLength(250,ErrorMessage = "The maximum number of characters is 250.")]
        public string Value { get; set; }
        [Required]
        public int? LevelId { get; set; }
        [Required(ErrorMessage = "The parent department is required.")]
        public int? ParentId { get; set; }
        public int? OrderNo { get; set; }
        [Required]
        public bool IsActive { get; set; }
        [Required]
        public RoleDto Parent { get; set; }
    }
    public class CreatePoliceStationDto
    {
        [Required]
        [MaxLength(250, ErrorMessage = "The maximum number of characters is 250.")]
        public string Value { get; set; }
        [Required(ErrorMessage = "The parent department is required.")]
        public int? ParentId { get; set; }
    }
    public class UpdatePoliceStationDto
    {
        [Required]
        public int Id { get; set; }
        [Required]
        [MaxLength(250, ErrorMessage = "The maximum number of characters is 250.")]
        public string Value { get; set; }
        [Required(ErrorMessage = "The parent department is required.")]
        public int? ParentId { get; set; }
    }
}
