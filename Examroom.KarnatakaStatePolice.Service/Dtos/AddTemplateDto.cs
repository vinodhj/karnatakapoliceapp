﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Examroom.KarnatakaStatePolice.Service.Dtos
{
    public class AddTemplateDto
    {
        public int TemplateId { get; set; }
        [JsonProperty("options")]
        public string TemplateType { get; set; }
        public string Name { get; set; }

        public string PoliceStation { get; set; }
    }

    public class UpdateTemplateContentDto
    {
        public int Id { get; set; }
        public string Content { get; set; }

    }

    public class AddNewUserFormDto
    {
        public int Id { get; set; }
        public int FormId { get; set; }
        public int PoliceStationId { get; set; }
        public string FormName { get; set; }    
        //public string CandidateName { get; set; }
        //public string CandidatePhoneNo { get; set; }
        //public string CandidateEmailId { get; set; }
        public string Content { get; set; }
        public string ContentBase64 { get; set; }

    }
    public class CompleteFormDto
    {
        public int Id { get; set; }
        public string UserResponse { get; set; }
        public string FormContent { get; set; }
    }
    public class UpdateFormContentDto
    {
        public int Id { get; set; }
        public string Content { get; set; }
        public string ContentBase64 { get; set; }

    }
}
