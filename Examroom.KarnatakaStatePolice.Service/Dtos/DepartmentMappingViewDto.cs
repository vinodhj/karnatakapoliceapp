﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Examroom.KarnatakaStatePolice.Service.Dtos
{
    public class DepartmentMappingViewDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
