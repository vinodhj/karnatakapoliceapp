﻿using AutoMapper;
using Examroom.KarnatakaStatePolice.DataAccess;
using Examroom.KarnatakaStatePolice.DataAccess.Models;
using Examroom.KarnatakaStatePolice.Service.Dtos;
using Examroom.KarnatakaStatePolice.Service.Helpers;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examroom.KarnatakaStatePolice.Service.Services
{
    /// <summary>
    /// Defines the <see cref="IDivisionService" />.
    /// </summary>
    public interface IDivisionService
    {
        /// <summary>
        /// The CreateDivisionAsync.
        /// </summary>
        /// <param name="userId">The userId<see cref="int"/>.</param>
        /// <param name="createDivisionDto">The createDivisionDto<see cref="CreateDivisionDto"/>.</param>
        /// <returns>The <see cref="Task{RoleDto}"/>.</returns>
        Task<RoleDto> CreateDivisionAsync(int userId, CreateDivisionDto createDivisionDto);

        /// <summary>
        /// The GetDivisionsAsync.
        /// </summary>
        /// <returns>The <see cref="Task{List{RoleDto}}"/>.</returns>
        Task<List<RoleDto>> GetDivisionsAsync();

        /// <summary>
        /// The GetDivisionsDropdownAsync.
        /// </summary>
        /// <returns>The <see cref="Task{List{DepartmentMappingViewDto}}"/>.</returns>
        Task<List<DepartmentMappingViewDto>> GetDivisionsDropdownAsync();

        /// <summary>
        /// The GetDivisionAsync.
        /// </summary>
        /// <param name="id">The id<see cref="int"/>.</param>
        /// <returns>The <see cref="Task{RoleDto}"/>.</returns>
        Task<RoleDto> GetDivisionAsync(int id);

        /// <summary>
        /// The UpdateDivisionAsync.
        /// </summary>
        /// <param name="userId">The userId<see cref="int"/>.</param>
        /// <param name="updateDivisionDto">The updateDivisionDto<see cref="UpdateDivisionDto"/>.</param>
        /// <returns>The <see cref="Task{RoleDto}"/>.</returns>
        Task<RoleDto> UpdateDivisionAsync(int userId, UpdateDivisionDto updateDivisionDto);

        /// <summary>
        /// The SwitchBlockedStatusAsync.
        /// </summary>
        /// <param name="userId">The userId<see cref="int"/>.</param>
        /// <param name="id">The id<see cref="int"/>.</param>
        /// <param name="blocked">The blocked<see cref="bool"/>.</param>
        /// <returns>The <see cref="Task"/>.</returns>
        Task SwitchBlockedStatusAsync(int userId, int id, bool blocked = false);
    }

    /// <summary>
    /// Defines the <see cref="DivisionService" />.
    /// </summary>
    public class DivisionService : IDivisionService
    {
        /// <summary>
        /// Defines the _kspContext.
        /// </summary>
        private readonly KspContext _kspContext;

        /// <summary>
        /// Defines the _mapper.
        /// </summary>
        private readonly IMapper _mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="DivisionService"/> class.
        /// </summary>
        /// <param name="kspContext">The kspContext<see cref="KspContext"/>.</param>
        /// <param name="mapper">The mapper<see cref="IMapper"/>.</param>
        public DivisionService(KspContext kspContext, IMapper mapper)
        {
            _kspContext = kspContext ?? throw new ArgumentNullException(nameof(kspContext));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        /// <summary>
        /// The CreateDivisionAsync.
        /// </summary>
        /// <param name="userId">The userId<see cref="int"/>.</param>
        /// <param name="createDivisionDto">The createDivisionDto<see cref="CreateDivisionDto"/>.</param>
        /// <returns>The <see cref="Task{RoleDto}"/>.</returns>
        public async Task<RoleDto> CreateDivisionAsync(int userId, CreateDivisionDto createDivisionDto)
        {
            if (!await _kspContext.User.UserExists(userId))
                throw new KspException("User does not exist.",
                    System.Net.HttpStatusCode.BadRequest);

            if (await _kspContext.DepartmentMapping.ActiveDivisionValueExists(createDivisionDto.Value))
                throw new KspException("Division value already exists.",
                    System.Net.HttpStatusCode.BadRequest);

            var division = _mapper.Map<DepartmentMapping>(createDivisionDto);
            division.SetDivisionDefaultValues(isAdd: true, userId);

            await _kspContext.DepartmentMapping.AddAsync(division);
            await _kspContext.SaveChangesAsync();

            return _mapper.Map<RoleDto>(division);
        }

        /// <summary>
        /// The GetDivisionsAsync.
        /// </summary>
        /// <returns>The <see cref="Task{List{RoleDto}}"/>.</returns>
        public async Task<List<RoleDto>> GetDivisionsAsync()
        {
            var divisions = await _kspContext.DepartmentMapping
                                    .Where(departmentMapping =>
                                           departmentMapping.Name.Equals("Place-Name") &&
                                           departmentMapping.IsActive)
                                    .ToListAsync();

            return _mapper.Map<List<RoleDto>>(divisions);
        }

        /// <summary>
        /// The GetDivisionsDropdownAsync.
        /// </summary>
        /// <returns>The <see cref="Task{List{DepartmentMappingViewDto}}"/>.</returns>
        public async Task<List<DepartmentMappingViewDto>> GetDivisionsDropdownAsync()
        {
            return await (

                from departmentMapping in _kspContext.DepartmentMapping
                where departmentMapping.Name.Equals("Place-Name") && departmentMapping.IsActive
                select new DepartmentMappingViewDto()
                {
                    Id = departmentMapping.Id,
                    Name = departmentMapping.Name,
                    Value = departmentMapping.Value
                }

            ).ToListAsync();
        }

        /// <summary>
        /// The GetDivisionAsync.
        /// </summary>
        /// <param name="id">The id<see cref="int"/>.</param>
        /// <returns>The <see cref="Task{RoleDto}"/>.</returns>
        public async Task<RoleDto> GetDivisionAsync(int id)
        {
            if(id is default(int))
                return null;

            var division = await _kspContext.DepartmentMapping
                                    .SingleOrDefaultAsync(
                                        departmentMapping =>
                                        departmentMapping.Name.Equals("Place-Name") &&
                                        departmentMapping.Id == id &&
                                        departmentMapping.IsActive);

            return _mapper.Map<RoleDto>(division);
        }

        /// <summary>
        /// The UpdateDivisionAsync.
        /// </summary>
        /// <param name="userId">The userId<see cref="int"/>.</param>
        /// <param name="updateDivisionDto">The updateDivisionDto<see cref="UpdateDivisionDto"/>.</param>
        /// <returns>The <see cref="Task{RoleDto}"/>.</returns>
        public async Task<RoleDto> UpdateDivisionAsync(int userId, UpdateDivisionDto updateDivisionDto)
        {
            if (!await _kspContext.User.UserExists(userId))
                throw new KspException("User does not exist.",
                    System.Net.HttpStatusCode.BadRequest);

            var division = await _kspContext.DepartmentMapping.SingleOrDefaultAsync(division => 
                division.Name.Equals("Place-Name") && division.Id == updateDivisionDto.Id && division.IsActive);

            if(division is null)
                throw new KspException("Division is not found.",
                    System.Net.HttpStatusCode.BadRequest);

            if (await _kspContext.DepartmentMapping.ActiveDivisionValueExists(updateDivisionDto.Value, updateDivisionDto.Id))
                throw new KspException("Division value already exists.",
                    System.Net.HttpStatusCode.BadRequest);

            division.Value = updateDivisionDto.Value;
            division.ParentId = updateDivisionDto.ParentId is default(int) 
                ? null : updateDivisionDto.ParentId;
            division.OrderNo = updateDivisionDto.OrderNo;
            division.ModifiedBy = userId;
            division.ModifiedDatetime = DateTime.Now;

            await _kspContext.SaveChangesAsync();

            return _mapper.Map<RoleDto>(division);
        }

        /// <summary>
        /// The SwitchBlockedStatusAsync.
        /// </summary>
        /// <param name="userId">The userId<see cref="int"/>.</param>
        /// <param name="id">The id<see cref="int"/>.</param>
        /// <param name="blocked">The blocked<see cref="bool"/>.</param>
        /// <returns>The <see cref="Task"/>.</returns>
        public async Task SwitchBlockedStatusAsync(int userId, int id, bool blocked = false)
        {
            if (!await _kspContext.User.UserExists(userId))
                throw new KspException("User does not exist.",
                    System.Net.HttpStatusCode.BadRequest);

            var division = await _kspContext.DepartmentMapping.SingleOrDefaultAsync(division =>
                division.Name.Equals("Place-Name") && division.Id == id);

            if (division is null)
                throw new KspException("Division is not found.",
                    System.Net.HttpStatusCode.BadRequest);

            division.IsActive = !blocked;
            division.ModifiedBy = userId;
            division.ModifiedDatetime = DateTime.Now;

            await _kspContext.SaveChangesAsync();
        }
    }
}