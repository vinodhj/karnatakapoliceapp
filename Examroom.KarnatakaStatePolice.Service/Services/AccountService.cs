﻿using AutoMapper;
using Examroom.KarnatakaStatePolice.DataAccess;
using Examroom.KarnatakaStatePolice.DataAccess.Models;
using Examroom.KarnatakaStatePolice.Service.Dtos;
using Examroom.KarnatakaStatePolice.Service.Helpers;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Examroom.KarnatakaStatePolice.Service.Services
{
    public interface IAccountService
    {
        Task<UserDto> Register(RegisterDto userDto);
        Task<UserDto> Login(LoginDto loginDto);
        Task<UserShortDto> SendOtp(string username);
        Task<UserShortDto> VerfiyOtp(VertifyOtpDto vertifyOtpDto);
        Task<List<RoleDto>> GetRoles();
        //Task<ProfileDto> GetProfile(int userId);
        //Task<int> PostProfile(ProfileDto profile);
        //Task Logout(int userId);
        //Task<bool> ForgetPassword(string userName);
        //Task<bool> ResetPassword(string token, string password);
        //Task<UserDto> Refresh(int userId);
    }
    public class AccountService : BaseApiService, IAccountService
    {

        private readonly ITokenService _tokenService;
        private readonly IEmailService _emailService;
        string OverrideOtp = null;

        public AccountService(KspContext kspContext, ITokenService tokenService, IMapper mapper, IEmailService emailService, IConfiguration configuration)
            : base(kspContext, mapper)
        {
            _tokenService = tokenService;
            _emailService = emailService;
            OverrideOtp = configuration["OverrideOtp"];
        }

        public async Task<UserDto> Register(RegisterDto registerDto)
        {
            if (await UserExists(registerDto.Username))
                throw new KspException("Username is taken", HttpStatusCode.BadRequest);

            if (await UserEmailExists(registerDto.EmailId))
                throw new KspException("Emaild is Exists", HttpStatusCode.BadRequest);

            using var hmac = new HMACSHA512();

            //move to automapper
            var user = new User
            {
                Firstname = registerDto.Firstname,
                Lastname = registerDto.Lastname,
                Username = registerDto.Username.ToLower(),
                PasswordHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(registerDto.Password)),
                PasswordSalt = hmac.Key,
                EmailId = registerDto.EmailId,
                //CountryCode = registerDto.CountryCode,
                Phone = registerDto.Phone,
                CreatedBy = 1,//Needs to be taken from Enum for Administrator Account
                ModifiedBy = 1,//Needs to be taken from Enum for Administrator Account
                ModifiedDatetime = DateTime.UtcNow,
                CreatedDatetime = DateTime.UtcNow,

            };

            _kspContext.User.Add(user);
            await _kspContext.SaveChangesAsync();

            return new UserDto
            {
                Id = user.Id,
                Username = user.Username,
                Token = _tokenService.CreateToken(user.Id, user.EmailId, user.Username, registerDto.RoleId)
            };
        }

        public async Task<UserDto> Login(LoginDto loginDto)
        {
            var user = await _kspContext
                        .User
                        .Include(x => x.UserToDepartmentMapping)
                        //.Include(x=> x.UserRole)
                        //    .ThenInclude(x=> x.Role)
                        .Where(x => x.Username == loginDto.Username)
                        .FirstOrDefaultAsync();

            if (user == null)
                throw new KspException("Invalid username", HttpStatusCode.Unauthorized);

            user.UserRole = _kspContext.UserRole.Include(x => x.Role).Where(x => x.UserId == user.Id).ToList();

            var userRoles = _kspContext.UserRole
                            .Where(x => x.UserId == user.Id)
                            .ToList();

            if (!user.IsActive.HasValue || !user.IsActive.Value)
                if (!user.IsActive.HasValue || user.IsActive.Value != true)
                    throw new KspException("Your account is disabled. Please contact Admin Team.", HttpStatusCode.Unauthorized);

            using var hmac = new HMACSHA512(user.PasswordSalt);
            var computedHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(loginDto.Password));

            for (int i = 0; i < computedHash.Length; i++)
            {
                if (computedHash[i] != user.PasswordHash[i])
                    throw new KspException("Invalid password", HttpStatusCode.Unauthorized);
            }

            await _kspContext.SaveChangesAsync();

            var userDept = await _kspContext
                                    .UserToDepartmentMapping
                                    .Include(x => x.PoliceStation)
                                        .ThenInclude(x => x.Parent)
                                            .ThenInclude(x => x.Parent)
                                    .Where(x => x.UserId == user.Id && x.IsActive == true)
                                    .ToListAsync();

            //move to automapper
            return new UserDto
            {
                Id = user.Id,
                EmailId = user.EmailId,
                Username = user.Username,
                Firstname = user.Firstname,
                Lastname = user.Lastname,
                RoleId = user.UserRole.FirstOrDefault().RoleId.Value,
                Password = "",
                //RoleId = userRoles.FirstOrDefault().RoleId,
                //Pseudoname = user.Pseudoname,
                //RoleId = role.roles.Id,
                //UserRoleId = role.userRoles.Id,
                Token = _tokenService.CreateToken(user.Id, user.EmailId, user.Username, userRoles.FirstOrDefault().RoleId.Value),
                 RoleName = user.UserRole.FirstOrDefault().Role.Name, 
                UserToDepartmentMapping = _mapper.Map<List<UserToDepartmentMappingDto>>(userDept)
            };
        }

        public async Task<UserShortDto> SendOtp(string username)
        {

            if (string.IsNullOrEmpty(username))
                throw new KspException("Please enter valid username or emailid");

            var user = await _kspContext.User.Where(x => x.Username == username || x.EmailId == username).FirstOrDefaultAsync();

            if (user == null)
                throw new KspException("Invalid username");


            user.Otp = string.IsNullOrEmpty(OverrideOtp) ? BaseMethod.GetOtp() : OverrideOtp;

            var isOtpEmailSent = await _emailService.SendOtp(user.EmailId, user.Firstname, user.Lastname, user.Otp);

            await _kspContext.SaveChangesAsync();

            var userDto = new UserShortDto()
            {
                Id = user.Id,
                Username = user.Username,
                EmailId = user.EmailId

            };
            return userDto;
        }

        public async Task<UserShortDto> VerfiyOtp(VertifyOtpDto vertifyOtpDto)
        {
            var user = await _kspContext.User.FindAsync(vertifyOtpDto.UserId);

            if (user == null)
                throw new KspException("Invalid user id", HttpStatusCode.BadRequest);



            if (user.Otp != vertifyOtpDto.Otp)
                throw new KspException("Invalid Otp", HttpStatusCode.Unauthorized);

            user.Otp = null;
            await _kspContext.SaveChangesAsync();

            return new UserShortDto()
            {
                Id = user.Id,
                Username = user.Username,
                EmailId = user.EmailId
            };
        }

        public async Task<List<RoleDto>> GetRoles()
        {
            var roles = await _kspContext.Role
                            .Where(x => x.IsActive == true)
                            .Select(x => new RoleDto()
                            {
                                Id = x.Id,
                                Name = x.Name
                            }).ToListAsync();
            return roles;
        }
    }
}
