﻿using AutoMapper;
using Examroom.KarnatakaStatePolice.DataAccess;
using Examroom.KarnatakaStatePolice.DataAccess.Models;
using Examroom.KarnatakaStatePolice.Service.Dtos;
using Examroom.KarnatakaStatePolice.Service.Helpers;
using Examroom.KarnatakaStatePolice.Service.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Examroom.KarnatakaStatePolice.Service.Services
{
    public interface IUsersService
    {
        Task<List<UserDto>> GetUsers();
        Task<UserDto> GetUser(int id);
        Task<int> SaveUser(int UserId, AddUserDto userDto);
        Task<bool> SetUserStatus(int UserId, UpdateUserStatusDto updateUserStatusDto);
        Task<bool> ResetPassword(int userId, UpdatePasswordDto updatePasswordDto);
    }
    public class UsersService : BaseApiService, IUsersService
    {
        public UsersService(KspContext kspContext, IMapper mapper)
           : base(kspContext, mapper)
        {

        }

        #region GetUsers
        public async Task<List<UserDto>> GetUsers()
        {
            var usersList = await _kspContext.User
                                    .Include(x => x.UserRole).Include(x=>x.UserToDepartmentMapping)
                                    .ThenInclude(x=>x.PoliceStation).ThenInclude(x=>x.Parent).ThenInclude(x => x.Parent)
                                    .ToListAsync();

            var usersDto = _mapper.Map<List<UserDto>>(usersList);

            foreach (var userDto in usersDto)
            {
                userDto.PoliceStationId = userDto.UserToDepartmentMapping.FirstOrDefault()?.PoliceStationId ?? -1;
                userDto.RoleId = userDto.UserRole.FirstOrDefault()?.RoleId ?? -1;

                userDto.RoleName = "Admin";
                if (userDto.RoleId == 1)
                    userDto.RoleName = "Super Admin";

                userDto.PoliceStationName = (await _kspContext.DepartmentMapping.FindAsync(userDto.PoliceStationId))?.Value;
            }

            return usersDto;
        }

        public async Task<UserDto> GetUser(int id)
        {
            var userDetails = await _kspContext
                                    .User
                                    .Where(x => x.IsActive.Value && x.Id == id)
                                    .Include(x => x.UserRole)
                                    .Include(x => x.UserToDepartmentMapping)
                                        .ThenInclude(x => x.PoliceStation)
                                            .ThenInclude(x => x.Parent)
                                                .ThenInclude(x => x.Parent)
                                    .FirstOrDefaultAsync();

            var userDto = _mapper.Map<UserDto>(userDetails);

            userDto.PoliceStationId = userDto.UserToDepartmentMapping.FirstOrDefault()?.PoliceStationId ?? -1;
            userDto.RoleId = userDto.UserRole.FirstOrDefault()?.RoleId ?? -1;

            return userDto;
        }

        #endregion

        #region Upsert User
        public async Task<int> SaveUser(int UserId, AddUserDto userDto)
        {
            int result;
            
            if (string.IsNullOrEmpty(userDto.EmailId))
                throw new KspException("Emailid cannot be empty", HttpStatusCode.BadRequest);

            if (string.IsNullOrEmpty(userDto.Firstname))
                throw new KspException("First Name cannot be empty", HttpStatusCode.BadRequest);

            if (string.IsNullOrEmpty(userDto.Username))
                throw new KspException("Username cannot be empty", HttpStatusCode.BadRequest);

            if (userDto.Id == default(int))
                result = await AddNewUser(UserId, userDto);
            else
                result = await UpdateCurrentUser(UserId, userDto);

            return result;
        }
        public async Task<bool> SetUserStatus(int UserId, UpdateUserStatusDto updateUserStatusDto)
        {
            var user = await _kspContext.User.FindAsync(updateUserStatusDto.Id);

            if (user == null)
                throw new KspException("Invalid user id", System.Net.HttpStatusCode.BadRequest);

            user.IsActive = updateUserStatusDto.IsActive;

            await _kspContext.SaveChangesAsync();
            return true;
        }
        private async Task<int> AddNewUser(int userId, AddUserDto userDto)
        {
            if (string.IsNullOrEmpty(userDto.Password))
                throw new KspException("Password cannot be empty", HttpStatusCode.BadRequest);

            var dupUserName = await _kspContext.User.AnyAsync(x => x.Username == userDto.Username);

            if (dupUserName)
                throw new KspException("Username already exists. Please choose another one", System.Net.HttpStatusCode.BadRequest);

            using var hmac = new HMACSHA512();
            var user = _mapper.Map<AddUserDto, User>(userDto, opt =>
            {
                opt.AfterMap((src, dest) =>
                {
                    dest.PasswordHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(src.Password));
                    dest.PasswordSalt = hmac.Key;
                    dest.UserRole = null;
                    dest.SetDefaultValues(isAdd: true, userId);
                });
            });
            var userrole = new UserRole()
            {
                User = user,
                RoleId = userDto.RoleId,
                IsActive = true

            };
            userrole.SetDefaultValues(isAdd: true, userId);
            if (userDto.RoleId == (int)Roles.User)
            {
                var userMappting = new UserToDepartmentMapping()
                {
                    PoliceStationId = userDto.PoliceStationId.Value,
                    User = user,
                    IsActive = true
                };
                userMappting.SetDefaultValues(isAdd: true, userId);
                _kspContext.UserToDepartmentMapping.Add(userMappting);
            }
            else if (userDto.RoleId == (int)Roles.Admin)
            {
                var userMappings = new List<UserToDepartmentMapping>();

                if (string.IsNullOrEmpty(userDto.PoliceStationsIds))
                    throw new KspException("Please select police-stations(s) for the admin", HttpStatusCode.BadRequest);

                if(userDto.PoliceStationsIds.Split(",").Count() == 0)
                    throw new KspException("Please select police-stations(s) for the admin", HttpStatusCode.BadRequest);

                foreach ( var ps in userDto.PoliceStationsIds.Split(","))
                {
                    var userMapping = new UserToDepartmentMapping()
                    {
                        PoliceStationId = Convert.ToInt32(ps),
                        User = user,
                        IsActive = true
                    };
                    userMapping.SetDefaultValues(isAdd: true, userId);
                    userMappings.Add(userMapping);
                }

                await _kspContext.UserToDepartmentMapping.AddRangeAsync(userMappings);
            }

            _kspContext.User.Add(user);
            _kspContext.UserRole.Add(userrole);
            await _kspContext.SaveChangesAsync();
            return user.Id;

        }
        private async Task<int> UpdateCurrentUser(int userId, AddUserDto userDto)
        {
            var dupUserName = await _kspContext.User.AnyAsync(x => x.Username == userDto.Username && x.Id != userDto.Id);

            if (dupUserName)
                throw new KspException("Username already exists for another user. Please choose another one", System.Net.HttpStatusCode.BadRequest);

            var dbEntity = await _kspContext
                                .User
                                .Include(x=> x.UserRole)
                                .Where(x=> x.Id == userDto.Id)
                                .FirstOrDefaultAsync();

            if (dbEntity == null)
                throw new KspException("User not found");

            var dbEntityUserName = await _kspContext.User.Select(x => x.Username).ToListAsync();

            dbEntity.Firstname = userDto.Firstname;
            dbEntity.Lastname = userDto.Lastname;
            dbEntity.Phone = userDto.Phone;
            dbEntity.EmailId = userDto.EmailId;
            dbEntity.Username = userDto.Username;

            var dbUserRoles = dbEntity.UserRole;

            foreach (var dbUserRole in dbUserRoles)
            {
                dbUserRole.RoleId = userDto.RoleId;
            }

            dbEntity.SetDefaultValues(isAdd: false, userId);

            if(userDto.RoleId == (int)Roles.Admin)
            {
                var dbUserMapping = await _kspContext.UserToDepartmentMapping.Where(x => x.UserId == userDto.Id).ToListAsync();

                var deleteUserMappings = dbUserMapping.Where(x => x.PoliceStationId != userDto.PoliceStationId).ToArray();
                var newUserMapping = new UserToDepartmentMapping()
                {
                    IsActive = true,
                    PoliceStationId = userDto.PoliceStationId.Value,
                    ModifiedBy = userId,
                    CreatedBy = userId,
                    CreatedDatetime = DateTime.Now,
                    ModifiedDatetime = DateTime.Now,
                    UserId = userDto.Id,
                    Id = 0
                };

                await _kspContext.UserToDepartmentMapping.AddAsync(newUserMapping);
                _kspContext.UserToDepartmentMapping.RemoveRange(deleteUserMappings);

            }

            var result = await _kspContext.SaveChangesAsync();
            return result;

        }
        #endregion


        public async Task<bool> ResetPassword(int userId, UpdatePasswordDto updatePasswordDto)
        {
            
            if (string.IsNullOrEmpty(updatePasswordDto.NewPassword))
                throw new KspException("Invalid password", HttpStatusCode.BadRequest);
            
            var user = await _kspContext.User.FindAsync(updatePasswordDto.Id);

            if (user == null)
                throw new KspException("Invalid userId", HttpStatusCode.BadRequest);

            using (var hmac = new HMACSHA512())
            {
                user.EmailId = user.EmailId;
                user.PasswordHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(updatePasswordDto.NewPassword));
                user.PasswordSalt = hmac.Key;
                await _kspContext.SaveChangesAsync();
            }

            return true;

        }
    }
}
