﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Examroom.KarnatakaStatePolice.Service.Services
{
    public interface IEmailService
    {
        Task<bool> SendOtp(string emailId, string firstName, string lastName, string Otp);
        Task<bool> SendNewUserMail(string emailId, string roleNname, string firstName, string lastName, string username, string password);
    }
    public class EmailService : IEmailService
    {
        SmtpClient _smtpClient;
        string FromMailAddress;
        string OverrideEmail = null;
        bool AllowEmails;

        public EmailService(IConfiguration configuration)
        {
            FromMailAddress = configuration["EmailUserName"];

            var host = configuration["EmailSmtpHost"];
            var username = configuration["EmailUserName"];
            var password = configuration["EmailPassword"];
            var port = Convert.ToInt32( configuration["EmailPort"]);

            AllowEmails = Convert.ToBoolean(configuration["SendEmail"]);
            OverrideEmail = configuration["OverrideEmail"];

            _smtpClient = new SmtpClient(host, port);
            _smtpClient.Credentials = new NetworkCredential(username, password);
        }

        public async Task<bool> SendEmail(string emailIds, string subject, string body)
        {
            try
            {

                if (AllowEmails == false)
                    return true;

                var from = new MailAddress(FromMailAddress);
                var to = new MailAddress(emailIds);

                if (!string.IsNullOrEmpty(OverrideEmail))
                    to = new MailAddress(OverrideEmail);

                var myMail = new MailMessage(from, to);

                myMail.Subject = subject;
                myMail.SubjectEncoding = Encoding.UTF8;

                myMail.Body = body;
                myMail.BodyEncoding = Encoding.UTF8;

                myMail.IsBodyHtml = true;

                await _smtpClient.SendMailAsync(myMail);

                return true;

            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<bool> SendOtp(string emailId, string firstName, string lastName, string Otp)
        {
            var subject = "Karanataka Police Application : Verfiy OTP";
            var body = $"Hi {firstName} {lastName},<br/>You have requested for OTP. Please use the below OTP to reset your password<br/><h3>{Otp}</h3>";

            emailId = "vijayk@examroom.ai";

            return await SendEmail(emailId, subject, body);
        }

        public async Task<bool> SendNewUserMail(string emailId, string roleNname, string firstName, string lastName, string username, string password)
        {
            var subject = "Karanataka Police Application : Welcome";
            var body = @$"Hi {firstName} {lastName},<br/>You have been added as {roleNname} for Karanata State Police Application. Please use the below credentials to login<br/>
                            Url: <h3><a href='#'>{"Click here"}</a></h3>
                            Username: {username}
                            Password: {password}";

            emailId = "vijayk@examroom.ai";


            return await SendEmail(emailId, subject, body);
        }

       
    }
}
