﻿using AutoMapper;
using Examroom.KarnatakaStatePolice.DataAccess;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Examroom.KarnatakaStatePolice.Service.Services
{
    public abstract class BaseApiService
    {
        protected readonly KspContext _kspContext;
        protected readonly IMapper _mapper;
        public BaseApiService(KspContext kspcontext, IMapper mapper)
        {
            _kspContext = kspcontext;
            _mapper = mapper;
        }

        protected async Task<bool> UserExists(string username)
        {
            return await _kspContext.User.AnyAsync(x => x.Username == username.ToLower() || x.EmailId.ToLower() == username.ToLower());
        }

        protected async Task<bool> UserEmailExists(string emailid)
        {
            return await _kspContext.User.AnyAsync(x => x.EmailId.ToLower() == emailid.ToLower());
        }
    }
}
