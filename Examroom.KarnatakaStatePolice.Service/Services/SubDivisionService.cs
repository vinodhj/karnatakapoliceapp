﻿using AutoMapper;
using Examroom.KarnatakaStatePolice.DataAccess;
using Examroom.KarnatakaStatePolice.DataAccess.Models;
using Examroom.KarnatakaStatePolice.Service.Dtos;
using Examroom.KarnatakaStatePolice.Service.Helpers;
using Examroom.KarnatakaStatePolice.Service.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Examroom.KarnatakaStatePolice.Service.Services
{
    /// <summary>
    /// Defines the <see cref="ISubDivisionService" />.
    /// </summary>
    public interface ISubDivisionService
    {
        /// <summary>
        /// The AddSubDivision.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="createSubDivision"></param>
        /// <returns></returns>
        Task<RoleDto> AddSubDivision(int userId, CreateSubDivisionDto createSubDivision);
        /// <summary>
        /// The ModifySubDivision.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="updateSubDivision"></param>
        /// <returns></returns>
        Task<RoleDto> ModifySubDivision(int userId, UpdateSubDivisionDto updateSubDivision);
        /// <summary>
        /// The BlockSubDivision.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="subDivisionId"></param>
        /// <returns></returns>
        Task<RoleDto> BlockSubDivision(int userId, int subDivisionId);
        /// <summary>
        /// The UnblockSubDivision.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="subDivisionId"></param>
        /// <returns></returns>
        Task<RoleDto> UnblockSubDivision(int userId, int subDivisionId);
        /// <summary>
        /// The ShowAllSubDivisions.
        /// </summary>
        /// <returns></returns>
        Task<List<RoleDto>> ShowAllSubDivisions();
        /// <summary>
        /// The GetSubDivisionById.
        /// </summary>
        /// <param name="subDivisionId"></param>
        /// <returns></returns>
        Task<RoleDto> GetSubDivisionById(int subDivisionId);
        /// <summary>
        /// The GetAllSubDivisionsDropdown.
        /// </summary>
        /// <returns></returns>
        Task<List<DepartmentMappingViewDto>> GetAllSubDivisionsDropdown();
    }
    /// <summary>
    /// Defines the <see cref="SubDivisionService" />.
    /// </summary>
    public class SubDivisionService : BaseApiService, ISubDivisionService
    {
        /// <summary>
        /// Defines the _formService.
        /// </summary>
        private readonly IFormService _formService;
        /// <summary>
        /// Initializes a new instance of the <see cref="SubDivisionService"/> class.
        /// </summary>
        /// <param name="formService"></param>
        /// <param name="kspContext"></param>
        /// <param name="mapper"></param>
        public SubDivisionService(IFormService formService, KspContext kspContext, IMapper mapper)
           : base(kspContext, mapper)
        {
            _formService = formService;
        }
        /// <summary>
        /// The DuplicateDepartmentCheck.
        /// </summary>
        /// <param name="createSubDivision"></param>
        /// <returns></returns>
        private async Task<bool> DuplicateDepartmentCheck(CreateSubDivisionDto createSubDivision)
        {
            return await _kspContext.DepartmentMapping.AnyAsync(x => x.Name == Constants.SubDivision && x.Value == createSubDivision.Value && x.IsActive == true);
        }
        /// <summary>
        /// The AddSubDivision.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="createSubDivision"></param>
        /// <returns></returns>
        public async Task<RoleDto> AddSubDivision(int userId, CreateSubDivisionDto createSubDivision)
        {
            if (createSubDivision == null)
                throw new KspException("Invalid input", System.Net.HttpStatusCode.BadRequest);

            if (await DuplicateDepartmentCheck(createSubDivision))
                throw new KspException("Sub-Division with this name already exists", System.Net.HttpStatusCode.BadRequest);

            var departmentMapping = _mapper.Map<DepartmentMapping>(createSubDivision);

            departmentMapping.Name = Constants.SubDivision;

            departmentMapping.LevelId = (int)DepartmentLevel.SubDivision;

            departmentMapping.IsActive = true;

            departmentMapping.SetDefaultValues(isAdd: true, userId);

            departmentMapping.CreatedDatetime = DateTime.Now;

            departmentMapping.CreatedBy = userId;

            departmentMapping.ModifiedDatetime = DateTime.Now;

            departmentMapping.ModifiedBy = userId;

            _kspContext.DepartmentMapping.Add(departmentMapping);

            await _kspContext.SaveChangesAsync();

            var roles = await _kspContext.DepartmentMapping.Where(x => x.Id == departmentMapping.Id).Include(x => x.Parent).SingleOrDefaultAsync();

            return _mapper.Map<RoleDto>(roles);
        }
        /// <summary>
        /// The ModifySubDivision.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="updateSubDivision"></param>
        /// <returns></returns>
        public async Task<RoleDto> ModifySubDivision(int userId, UpdateSubDivisionDto updateSubDivision)
        {
            if (updateSubDivision is null)
                throw new KspException("Invalid input", System.Net.HttpStatusCode.BadRequest);

            var subdivision = await _kspContext.DepartmentMapping.Where(x => x.Id == updateSubDivision.Id).Include(x => x.Parent).FirstOrDefaultAsync();

            if(subdivision is null) 
                throw new KspException("Sub-Division with this Id does not exist.", System.Net.HttpStatusCode.BadRequest);

            subdivision.ModifiedDatetime = DateTime.Now;

            subdivision.ModifiedBy = userId;

            subdivision.Value = updateSubDivision.Value;

            subdivision.ParentId = updateSubDivision.ParentId;

            await _kspContext.SaveChangesAsync();

            return _mapper.Map<RoleDto>(subdivision);
        }
        /// <summary>
        /// The BlockSubDivision.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="subDivisionId"></param>
        /// <returns></returns>
        public async Task<RoleDto> BlockSubDivision(int userId, int subDivisionId)
        {
            if (subDivisionId == 0)
                throw new KspException("Sub-Division Id is required.", System.Net.HttpStatusCode.BadRequest);

            var subdivision = await _kspContext.DepartmentMapping.Where(x => x.Id == subDivisionId).Include(x => x.Parent).FirstOrDefaultAsync();

            if (subdivision is null)
                throw new KspException("Sub-Division with this Id does not exist.", System.Net.HttpStatusCode.BadRequest);

            subdivision.ModifiedDatetime = DateTime.Now;

            subdivision.ModifiedBy = userId;

            subdivision.IsActive = false;

            await _kspContext.SaveChangesAsync();

            return _mapper.Map<RoleDto>(subdivision);
        }
        /// <summary>
        /// The UnblockSubDivision.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="subDivisionId"></param>
        /// <returns></returns>
        public async Task<RoleDto> UnblockSubDivision(int userId, int subDivisionId)
        {
            if (subDivisionId == 0)
                throw new KspException("Sub-Division Id is required.", System.Net.HttpStatusCode.BadRequest);

            var subdivision = await _kspContext.DepartmentMapping.Where(x => x.Id == subDivisionId).Include(x => x.Parent).FirstOrDefaultAsync();

            if (subdivision is null)
                throw new KspException("Sub-Division with this Id does not exist.", System.Net.HttpStatusCode.BadRequest);

            subdivision.ModifiedDatetime = DateTime.Now;

            subdivision.ModifiedBy = userId;

            subdivision.IsActive = true;

            await _kspContext.SaveChangesAsync();

            return _mapper.Map<RoleDto>(subdivision);
        }
        /// <summary>
        /// The ShowAllSubDivisions.
        /// </summary>
        /// <returns></returns>
        public async Task<List<RoleDto>> ShowAllSubDivisions()
        {
            var subdivisions = await _kspContext.DepartmentMapping.Where(x=> x.Name == Constants.SubDivision).Include(x => x.Parent).ToListAsync();

            return _mapper.Map<List<RoleDto>>(subdivisions);
        }
        /// <summary>
        /// The GetAllSubDivisionsDropdown.
        /// </summary>
        /// <returns></returns>
        public async Task<List<DepartmentMappingViewDto>> GetAllSubDivisionsDropdown()
        {
            return await _kspContext.DepartmentMapping.Where(x => x.Name == Constants.SubDivision).Select(x => new DepartmentMappingViewDto()
            {
                Id = x.Id,
                Name = x.Name,
                Value = x.Value
            }).ToListAsync();

        }
        /// <summary>
        /// The GetSubDivisionById.
        /// </summary>
        /// <param name="subDivisionId"></param>
        /// <returns></returns>
        public async Task<RoleDto> GetSubDivisionById(int subDivisionId)
        {
            var subdivision = await _kspContext.DepartmentMapping.Where(x => x.Id == subDivisionId).Include(x => x.Parent).FirstOrDefaultAsync();

            return _mapper.Map<RoleDto>(subdivision);
        }
    }
}
