﻿using AutoMapper;
using Examroom.KarnatakaStatePolice.DataAccess;
using Examroom.KarnatakaStatePolice.DataAccess.Models;
using Examroom.KarnatakaStatePolice.Service.Dtos;
using Examroom.KarnatakaStatePolice.Service.Helpers;
using Examroom.KarnatakaStatePolice.Service.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Examroom.KarnatakaStatePolice.Service.Services
{
    public interface IPoliceStationService
    {
        Task<int> AddPoliceStation(int userId, CreatePoliceStationDto policeStationDto);
        Task<int> ModifyPoliceStation(int userId, int id, UpdatePoliceStationDto policeStationDto);
        Task<int> BlockPoliceStation(int userId, int id);
        Task<int> UnblockPoliceStation(int userId, int id);
        Task<List<PoliceStationDto>> GetAllPoliceStations();
        Task<List<DepartmentMappingViewDto>> GetAllPoliceStationsDropdown();
        Task<List<PoliceStationDto>> GetAllPoliceStationsForParent(int parentId);
        Task<PoliceStationDto> GetPoliceStation(int id);
    }
    public class PoliceStationService : BaseApiService, IPoliceStationService
    {
        public PoliceStationService(KspContext kspcontext, IMapper mapper) : base(kspcontext, mapper)
        {
        }

        public async Task<int> AddPoliceStation(int userId, CreatePoliceStationDto policeStationDto)
        {
            var policeStation = _mapper.Map<DepartmentMapping>(policeStationDto);

            policeStation.Name = Constants.PoliceStation;
            policeStation.LevelId = (int)DepartmentLevel.PoliceStation;
            policeStation.OrderNo = default(int);
            policeStation.IsActive = true;
            policeStation.CreatedBy = userId;
            policeStation.CreatedDatetime = DateTime.Now;
            policeStation.ModifiedBy = userId;
            policeStation.ModifiedDatetime = DateTime.Now;

            _kspContext.DepartmentMapping.Add(policeStation);

            await _kspContext.SaveChangesAsync();

            return policeStation.Id;
        }

        public async Task<int> BlockPoliceStation(int userId, int id)
        {
            var policeStation = await _kspContext.DepartmentMapping.Where(x => x.LevelId == 3 && x.Id == id).SingleOrDefaultAsync();

            if (policeStation == null)
                throw new KspException("The police station can't be found.", System.Net.HttpStatusCode.NotFound);

            policeStation.IsActive = false;
            policeStation.ModifiedBy = userId;
            policeStation.ModifiedDatetime = DateTime.Now;

            await _kspContext.SaveChangesAsync();

            return policeStation.Id;
        }

        public async Task<List<PoliceStationDto>> GetAllPoliceStations()
        {

            var policeStations = await _kspContext.DepartmentMapping.Where(x => x.LevelId == 3 && x.Name == "Police-Station").Include(x => x.Parent).ToListAsync();

            return _mapper.Map<List<PoliceStationDto>>(policeStations);
            
        }

        public async Task<List<DepartmentMappingViewDto>> GetAllPoliceStationsDropdown()
        {
            var policeStations = await _kspContext.DepartmentMapping.Where(x => x.LevelId == 3 && x.Name == "Police-Station").ToListAsync();
            

            return _mapper.Map<List<DepartmentMappingViewDto>>(policeStations);
        }

        public async Task<List<PoliceStationDto>> GetAllPoliceStationsForParent(int parentId)
        {
            var policeStations = await _kspContext.DepartmentMapping.Where(x => x.LevelId == 3 && x.ParentId == parentId && x.Name == "Police-Station").ToListAsync();

            return _mapper.Map<List<PoliceStationDto>>(policeStations);
        }

        public async Task<PoliceStationDto> GetPoliceStation(int id)
        {
            // var policeStation = await  _kspContext.DepartmentMapping.FindAsync(id);
            var policeStation = await _kspContext.DepartmentMapping.Where(x => x.LevelId == 3 && x.Id == id && x.Name == "Police-Station").Include(x => x.Parent).SingleOrDefaultAsync();

            if (policeStation == null)
                throw new KspException("The police station can't be found.", System.Net.HttpStatusCode.NotFound);

            return _mapper.Map<PoliceStationDto>(policeStation);

        }

        public async Task<int> ModifyPoliceStation(int userId, int id, UpdatePoliceStationDto policeStationDto)
        {
            if (policeStationDto.Id != id)
                throw new KspException("The police station id doesn't match.", System.Net.HttpStatusCode.BadRequest);

            var policeStation = await _kspContext.DepartmentMapping.Where(x => x.LevelId == 3 && x.Id == id).SingleOrDefaultAsync();

            if (policeStation == null)
                throw new KspException("The police station can't be found.", System.Net.HttpStatusCode.NotFound);

            policeStation.Value = policeStationDto.Value;
            policeStation.ParentId = policeStationDto.ParentId;
            policeStation.ModifiedBy = userId;
            policeStation.ModifiedDatetime = DateTime.Now;

            await _kspContext.SaveChangesAsync();

            return policeStation.Id;
        }

        public async Task<int> UnblockPoliceStation(int userId, int id)
        {
            var policeStation = await _kspContext.DepartmentMapping.Where(x => x.LevelId == 3 && x.Id == id).SingleOrDefaultAsync();

            if (policeStation == null)
                throw new KspException("The police station can't be found.", System.Net.HttpStatusCode.NotFound);

            policeStation.IsActive = true;
            policeStation.ModifiedBy = userId;
            policeStation.ModifiedDatetime = DateTime.Now;

            await _kspContext.SaveChangesAsync();

            return policeStation.Id;
        }
    }
}
