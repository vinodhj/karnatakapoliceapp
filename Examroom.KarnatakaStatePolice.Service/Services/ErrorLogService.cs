﻿using AutoMapper;
using Examroom.KarnatakaStatePolice.DataAccess;
using Examroom.KarnatakaStatePolice.DataAccess.Models;
using Examroom.KarnatakaStatePolice.Service.Dtos;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Examroom.KarnatakaStatePolice.Service.Services
{

    public interface IErrorLogService
    {
        Task<CommonOutput> Create(ErrorLogDto objIp);
    }

    public class ErrorLogService : BaseApiService, IErrorLogService
    {
        public ErrorLogService(KspContext kspContext, IMapper mapper)
          : base(kspContext, mapper)
        {

        }

        public async Task<CommonOutput> Create(ErrorLogDto objIp)
        {

            CommonOutput objCommonOutput = new CommonOutput();
            var objIps = _mapper.Map<ErrorLog>(objIp);
            objIps.CreatedDatetime = System.DateTime.Now;
            objIps.ModifiedDatetime = System.DateTime.Now;
            objIps.IsActive = true;
            objIps.IsDeleted = false;
            _kspContext.ErrorLog.Add(objIps);
            await _kspContext.SaveChangesAsync();
            objCommonOutput.Status = true;
            objCommonOutput.Message = "Log Created Successfully";
            return objCommonOutput;
        }
    }
}
