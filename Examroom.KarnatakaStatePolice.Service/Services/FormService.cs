﻿using Examroom.KarnatakaStatePolice.DataAccess;
using Examroom.KarnatakaStatePolice.DataAccess.Models;
using Examroom.KarnatakaStatePolice.Service.Dtos;
using Examroom.KarnatakaStatePolice.Service.Helpers;
using Examroom.KarnatakaStatePolice.Service.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Data;
using System.Net;
using Newtonsoft.Json;
using Examroom.KarnatakaStatePolice.Service.FormBuilder;
using ClosedXML.Excel;
using AutoMapper;
using System.IO.Compression;

namespace Examroom.KarnatakaStatePolice.Service.Services
{
    public interface IFormService
    {
        Task<int> AddTemplate(int userId, int roleId, IFormFile file, string name, string policeStationsNos, string content);
        Task<int> AddTemplate(int userId, int roleId, AddTemplateDto addTemplateDto);
        Task<FormsSummaryDto> GetFormsSummaryDashboard(int userId, int roleId);
        Task<int> AddNewUserForm(int userId, int roleId, AddNewUserFormDto addNewUserFormDto);
        Task<bool> UpdateTemplateContent(int userId, int roleId, UpdateTemplateContentDto addTemplateDto);
        Task<bool> UpdateForm(int userId, int formId, IFormFile file, string content);
        Task<bool> UpdateDocForm(int userId, int formId, string content);
        Task<bool> SubmitDocForm(int userId, int formId, string content);
        Task<bool> UpdateTemplate(int userId, int templateId, string content);
        Task<string> GetTemplateContent(int tempateId);
        Task<string> GetFormContent(int tempateId);
        Task<DownloadContentDto> DownloadTemplate(int id);
        Task<List<TemplateDto>> GetTemplates(int userId, int roleId);
        Task<bool> AddEmptyForm(int userId, int roleId, int formId, string name);
        Task<DownloadContentDto> GetForm(int id);
        Task<List<UserFormDto>> GetForms(int userId, int roleId);
        Task<List<UserFormSummaryDto>> GetFormsSummary(int userId, int roleId);
        Task<List<RoleDto>> GetPoliceStationsForForm(int userId, int roleId, int templateId);
        Task<BlobDto> GetTemplateBlob(int templateId);
        Task<DownloadContentDto> DownloadExcelTemplate(int templateId);
        Task<DownloadContentDto> DownloadExcelForm(int formId);
        Task<string[]> GetUserInput(int formId);
        Task<bool> SubmitForApproval(int userId, int id);
        Task<bool> ApproveForm(int userId, int id);
        Task<bool> RejectForm(int userId, int id);
        Task<UserFormDto> GetFormDetails(int formId);
        Task<bool> UpdateFormContent(int userId, int roleId, UpdateFormContentDto formContentDto);
        Task<CompleteFormDto> GetCompleteFormDetails(int userId, int userFormId);
        Task<bool> ActionOnForm(int userId, ActionOnFromDto actionDto);
        Task<bool> SetTemplateStatus(int userId, int roleId, SetTemplateStatusDto statusDto);
        Task<DownloadContentDto> DownloadDynamicFiles(int userId, int roleId, int templateId);
        Task<List<UserFormDetailDto>> GetUserFormDetails(int userId, int roleId, int templateId, string policeStationIds = null);
        Task<bool> UpdateDepartment(int userId, int roleId, UpdateDetartmentDto updateDto);
        Task<DownloadContentDto> DownloadStaticFiles(int userId, int roleId, int templateId, string formIds = null);
        Task<DepartmentSummaryDo> GetDepartmentWiseSummaries(int userId, int roleId);
    }
    public class FormService : IFormService
    {
        private readonly KspContext _kspContext;
        private readonly string _templatePath;
        private readonly string _formPath;
        private readonly IMapper _mapper;

        public FormService(KspContext kspContext, IConfiguration configuration, IMapper mapper)
        {
            _kspContext = kspContext;
            _templatePath = configuration["TemplatesPath"];
            _formPath = configuration["FormsPath"];
            _mapper = mapper;
        }

        public async Task<List<RoleDto>> GetPoliceStationsForForm(int userId, int roleId, int templateId)
        {
            var template = await _kspContext
                            .FormMaster
                            .Include(x=> x.FormMapping)
                                .ThenInclude(x=> x.Department)
                            .Where(t => t.Id == templateId)
                            .FirstOrDefaultAsync();

            var depts = template
                            .FormMapping
                            .Select(x => _mapper.Map<RoleDto>(x.Department))
                            .ToList();

            return depts;
        }
        public async Task<bool> UpdateFormContent(int userId, int roleId, UpdateFormContentDto formContentDto)
        {
            if (string.IsNullOrEmpty(formContentDto.Content))
                throw new KspException("Cannot send empty content", HttpStatusCode.BadRequest);

            var userForm = await _kspContext
                                    .UserForm
                                    .Include(x=> x.FormMaster)
                                    .Where(x=> x.Id == formContentDto.Id)
                                    .FirstOrDefaultAsync();

            if (userForm == null)
                throw new KspException("Invalid User-Form Id", HttpStatusCode.BadRequest);
            
            //var parsedAnswers = FormBuilderHelper
            //                        .ParseUserResponse(userForm.FormMaster.Content, formContentDto.Content);

            userForm.UserInputBase64 = formContentDto.ContentBase64;
            userForm.UserInput = formContentDto.Content;
            userForm.ModifiedBy = userId;
            userForm.ModifiedDatetime = DateTime.Now;
            
            await _kspContext.SaveChangesAsync();
            
            return true;

        }
        public async Task<bool> SetTemplateStatus(int userId, int roleId, SetTemplateStatusDto statusDto)
        {
            var template = await _kspContext.FormMaster.FindAsync(statusDto.Id);

            if (template == null)
                throw new KspException("Invalid template Id", HttpStatusCode.BadRequest);

            template.IsPublished = statusDto.IsPublished;
            template.ModifiedBy = userId;
            template.ModifiedDatetime = DateTime.Now;

            await _kspContext.SaveChangesAsync();

            return true;

        }
        public async Task<int> AddTemplate(int userId, int roleId, IFormFile file, string name, string policeStationsNos, string content)
        {

            if (file == null && string.IsNullOrEmpty(content))
                throw new KspException("Invalid file");

            
            if (string.IsNullOrEmpty(policeStationsNos))
                throw new KspException("Please specify police stations");

            var arr = policeStationsNos.Split(',').Select(x=> Convert.ToInt32(x)).ToList();

            var policeStationIds = await _kspContext
                                    .DepartmentMapping
                                    .Where(x => arr.Contains(x.Id))
                                    .Select(x => x.Id)
                                    .ToListAsync();

            if (policeStationIds == null || policeStationIds.Count() == 0)
                throw new KspException("Invalid Police stations");
             
            string systemFileName = null;

            if (file != null)
                systemFileName = await SaveTemplate(file);
            else
                systemFileName = await SaveTemplateContent(content);

            string fileName = file?.FileName ?? $"{name}.json";

            var mapping = await _kspContext
                            .DepartmentMapping
                            .Where(x => x.Name == "FormStatus" || x.Name == "Template")
                            .ToListAsync();

            var code = GenerateUniqueCode();
            var formMaster = new FormMaster();
            formMaster.Code = code;
            formMaster.Name = name;
            formMaster.FileName = fileName;
            formMaster.CreatedBy = userId;
            formMaster.ModifiedBy = userId;
            formMaster.CreatedDatetime = DateTime.Now;
            formMaster.ModifiedDatetime = DateTime.Now;
            formMaster.SystemFileName = systemFileName;
            formMaster.TemplateType = mapping.SingleOrDefault(x => x.Value == FileHelper.GetTemplateType(fileName).ToString()).Id;
            
            await _kspContext.FormMaster.AddAsync(formMaster);
            await _kspContext.SaveChangesAsync();

            var templateId = formMaster.Id;

            var policeStations = policeStationsNos
                                    .Split(',')
                                    .Select(x => new FormMapping()
                                    {
                                        FormId = templateId,
                                        Form = formMaster,
                                        IsActive = true,
                                        DepartmentId = Convert.ToInt32(x),
                                        CreatedBy = userId,
                                        CreatedDatetime = DateTime.Now,
                                        ModifiedBy = userId,
                                        ModifiedDatetime = DateTime.Now,
                                    })
                                    .ToList();

            await _kspContext.FormMapping.AddRangeAsync( policeStations);
            await _kspContext.SaveChangesAsync();

            return formMaster.Id;
        }
        
        public async Task<int> AddTemplate(int userId, int roleId, AddTemplateDto addTemplateDto)
        {
            var policeStationsNos = addTemplateDto.PoliceStation;

            if (string.IsNullOrEmpty(policeStationsNos))
                throw new KspException("Please specify police stations");

            var arr = policeStationsNos.Split(',').Select(x => Convert.ToInt32(x)).ToList();

            var policeStationIds = await _kspContext
                                    .DepartmentMapping
                                    .Where(x => arr.Contains(x.Id))
                                    .Select(x => x.Id)
                                    .ToListAsync();

            if (policeStationIds == null || policeStationIds.Count() == 0)
                throw new KspException("Invalid Police stations");

            var mapping = await _kspContext
                          .DepartmentMapping
                          .Where(x => x.Name == "FormStatus" || x.Name == "Template")
                          .ToListAsync();

            var tt = _kspContext.DepartmentMapping.FirstOrDefault(x => x.Value == addTemplateDto.TemplateType);

            var code = GenerateUniqueCode();
            var formMaster = new FormMaster();
            formMaster.Code = code;
            formMaster.Name = addTemplateDto.Name;
            formMaster.CreatedBy = userId;
            formMaster.ModifiedBy = userId;
            formMaster.CreatedDatetime = DateTime.Now;
            formMaster.ModifiedDatetime = DateTime.Now;
            formMaster.TemplateType = _kspContext.DepartmentMapping.FirstOrDefault(x => x.Value == addTemplateDto.TemplateType).Id;
            formMaster.Content = null;

            formMaster.FormMapping = policeStationsNos
                                    .Split(',')
                                    .Select(x => new FormMapping()
                                    {
                                        Form = formMaster,
                                        IsActive = true,
                                        DepartmentId = Convert.ToInt32(x),
                                        CreatedBy = userId,
                                        CreatedDatetime = DateTime.Now,
                                        ModifiedBy = userId,
                                        ModifiedDatetime = DateTime.Now,
                                    })
                                    .ToList();

            await _kspContext.FormMaster.AddAsync(formMaster);
            await _kspContext.SaveChangesAsync();

            var templateId = formMaster.Id;

            return formMaster.Id;

        }

        public async Task<BlobDto> GetTemplateBlob(int id)
        {
            var formDetails = _kspContext.FormMaster.Where(x => x.Id == id).FirstOrDefault();

            if (formDetails == null)
                throw new KspException("File Not found", System.Net.HttpStatusCode.BadRequest);

            var systemFileName = formDetails.SystemFileName;

            if (!System.IO.File.Exists(systemFileName))
                throw new KspException("File not available", System.Net.HttpStatusCode.NotFound);

            var blob = new BlobDto()
            {
                Content = System.IO.File.ReadAllBytes(systemFileName),
                Name = formDetails.FileName

            };

            return await Task.FromResult(blob);

        }
        public async Task<DownloadContentDto> DownloadTemplate(int id)
        {
            var formDetails = _kspContext.FormMaster.Where(x => x.Id == id).FirstOrDefault();

            if (formDetails == null)
                throw new KspException("File Not found", System.Net.HttpStatusCode.BadRequest);

            var (fileContent, fileName) = await DownloadTemplateFile(id);

            var downloadContent =  new DownloadContentDto()
            {
                ContentType = "application/octet-stream",
                FileName = fileName,
                FileInBytes = fileContent
            };

            return await Task.FromResult(downloadContent);
        }

        public async Task<List<TemplateDto>> GetTemplates(int userId, int roleId)
        {

            var templates = _kspContext
                                .FormMaster
                                .Include(x=> x.UserForm)
                                .Select(x => x);

            if(roleId == Convert.ToInt32(Roles.Admin) || roleId == (int)Roles.User)
            {
                var departments = _kspContext
                                    .UserToDepartmentMapping
                                    .Where(x => x.UserId == userId)
                                    .Select(x => x.PoliceStationId)
                                    .ToList();

                var forms = _kspContext
                            .FormMapping
                            .Where(x => departments.Contains(x.DepartmentId.Value))
                            .Where(x=> x.FormId.HasValue)
                            .Select(x => x.FormId.Value)
                            .ToList();

                templates = templates.Where(x => forms.Contains(x.Id));

                templates = templates.Where(x => x.IsPublished || x.CreatedBy == userId);

            }

            var templatesDto = templates
                                .Include(x => x.Created)
                                .Include(x => x.Modified)
                                .Include(x => x.TemplateMapping)
                                .Include(x=> x.FormMapping)
                                    .ThenInclude(x=> x.Department)
                                        .ThenInclude(x=> x.Parent)
                                            .ThenInclude(x=> x.Parent)
                                .Select(x => new TemplateDto()
                                {
                                    Id = x.Id,
                                    Name = x.Name,
                                    CreatedById = x.Created.Id,
                                    CreatedBy = x.Created.Username,
                                    CreatedOn = x.CreatedDatetime,
                                    ModifiedId = x.Modified.Id,
                                    ModifiedBy = x.Modified.Username,
                                    ModifiedOn = x.ModifiedDatetime,
                                    TemplateType = x.TemplateMapping.Value,
                                    IsPublished = x.IsPublished,
                                    Departments = _mapper.Map<List<DepartmentMapping>, List<RoleDto>>(x.FormMapping.Select(y=> y.Department).ToList())
                                })
                                .ToList();

            return await Task.FromResult(templatesDto);
        }
        public IQueryable<FormMaster> GetTemplates(int userId, int roleId, string policeStationIds = null)
        {
            var templates = _kspContext
                            .FormMaster
                            .Include(x => x.TemplateMapping)
                            .Include(x=> x.FormMapping)
                            .Select(x => x);

            var policeStations = new List<int>();
            
            if(roleId == (int)Roles.Admin || roleId == (int)Roles.User)
            {
                policeStations = _kspContext.UserToDepartmentMapping.Where(x => x.UserId == userId).Select(x => x.PoliceStationId).ToList();

                if (!string.IsNullOrEmpty(policeStationIds))
                {
                    var arrPolieStationId = policeStationIds.Split(",").Select(x => Convert.ToInt32(x));
                    policeStations = policeStations.Where(x => arrPolieStationId.Contains(x)).ToList();
                }

                templates = templates
                               .Where(t => t.FormMapping.Any(d => policeStations.Contains(d.DepartmentId.Value)));

            }

            return templates;

        }
        private IQueryable<UserForm> GetUserForms(int userId, int roleId, int? templateId = null, string policeStationIds = null)
        {
            var forms = _kspContext
                            .UserForm
                            .Include(x => x.FormMaster)
                            .ThenInclude(x => x.TemplateMapping)
                                .Include(x => x.User)
                            .Include(x => x.Created)
                            .Include(x => x.Modified)
                            .Include(x => x.PoliceStation)
                            .Select(x => x);

            List<int> policeStations = new List<int>();
            if (roleId == (int)Roles.Admin || roleId == (int)Roles.User)
            {
                policeStations = _kspContext.UserToDepartmentMapping.Where(x => x.UserId == userId).Select(x => x.PoliceStationId).ToList();

                if (!string.IsNullOrEmpty(policeStationIds))
                {
                    var arrPolieStationId = policeStationIds.Split(",").Select(x=> Convert.ToInt32(x));
                    policeStations = policeStations.Where(x => arrPolieStationId.Contains(x)).ToList();
                }

                forms = forms.Where(x => policeStations.Contains(x.PoliceStationNo ?? -1));
            }

            if (templateId.HasValue)
                forms = forms.Where(x => x.FormId == templateId);

            return forms;
        }
        public async Task<List<UserFormDto>> GetForms(int userId, int roleId)
        {
            var forms = _kspContext
                            .UserForm
                            .Include(x => x.FormMaster)
                            .ThenInclude(x => x.TemplateMapping)
                                .Include(x => x.User)
                            .Include(x => x.Created)
                            .Include(x => x.Modified)
                            .Include(x => x.PoliceStation)
                            .Select(x=> x);

            List<int> policeStations = new List<int>();
            if (roleId == (int)Roles.Admin || roleId == (int)Roles.User)
            {
                policeStations = _kspContext.UserToDepartmentMapping.Where(x => x.UserId == userId).Select(x => x.PoliceStationId).ToList();

                forms = forms.Where(x => policeStations.Contains(x.PoliceStationNo ?? -1 ));

            }

            var formsDto = forms
                            //.Include(x => x.FormMaster)
                            //.ThenInclude(x => x.TemplateMapping)
                            //    .Include(x => x.User)
                            //.Include(x => x.Created)
                            //.Include(x => x.Modified)
                            //.Include(x=> x.PoliceStation)
                            .Select(x => new UserFormDto()
                            {
                                Id = x.Id,
                                FormId = x.FormId.Value,
                                FormName = x.Name,
                                TemplateName = x.FormMaster.Name,
                                CreatedBy = x.Created.Username,
                                CreatedOn = x.CreatedDatetime,
                                ModifiedBy = x.Modified.Username,
                                ModifiedOn = x.ModifiedDatetime,
                                TemplateType = x.FormMaster.TemplateMapping.Value,
                                PoliceStationName = x.PoliceStation.Value,
                                PoliceStationId = x.PoliceStation.Id,
                                Status = x.FormStatus.Value
                                ,Comment = x.Comment

                            })
                            .ToList();

            return await Task.FromResult(formsDto);
        }

        public async Task<List<UserFormSummaryDto>> GetFormsSummary(int userId, int roleId)
        {
            var templatesQuery = GetTemplates(userId, roleId, null);

            if(roleId == (int)Roles.Admin || roleId == (int)Roles.User)
            {
                templatesQuery = templatesQuery
                                    .Where(x => x.IsPublished == true || x.CreatedBy == userId);
            }

            var templates = await templatesQuery
                                    .Select(x => new UserFormSummaryDto()
                                    {
                                        FormId = x.Id,
                                        FormStatus = x.IsPublished ? Constants.TemplateStatus_Published : Constants.TemplateStatus_Unpublished,
                                        FormType = x.TemplateMapping.Value,
                                        FormName = x.Name,
                                        PoliceStationIds = x.FormMapping.Select(x => x.DepartmentId.Value).ToArray(),
                                        PoliceStations = x.FormMapping.Select(x => x.Department.Value).ToArray()
                                    })
                                .ToListAsync();

            var userForms = await GetUserForms(userId, roleId)
                               .Select(x => new { TemplateId = x.FormMaster.Id, FormId = x.Id })
                               .ToListAsync();
            
            foreach (var template in templates)
            {
                template.Count = userForms.Where(x => x.TemplateId == template.FormId).Count();
            }

            return templates;

        }

         
        public async Task<List<UserFormDetailDto>> GetUserFormDetails(int userId, int roleId, int templateId, string policeStationIds = null)
        {
            var userForms = GetUserForms(userId, roleId, templateId, policeStationIds);

            var userFormsDto = await userForms
                                    .Select(x => new UserFormDetailDto()
                                    {
                                        UserFormId = x.Id,
                                        FormId = x.FormMaster.Id,
                                        FormName = x.Name,
                                        CreatedBy = x.Created.Username,
                                        CreatedOn = x.CreatedDatetime,
                                        ModifiedBy = x.Modified.Username,
                                        ModifiedOn = x.ModifiedDatetime,
                                        PoliceStationName = x.PoliceStation.Value,
                                        Status = x.FormStatus.Value,
                                        Comment = x.Comment,
                                        FormType = x.FormMaster.TemplateMapping.Value,
                                        //CandidateName = x.CandidateName,
                                        //CandidateEmailId = x.CandidateEmailId,
                                        //CandidatePhoneNo = x.CandidatePhoneNo,
                                        Code = x.Code,
                                        ShowSubmitButton = (x.Created.Id == userId && (x.FormStatus.Value == Constants.FormStatus_Draft || x.FormStatus.Value == Constants.FormStatus_Rejected)),
                                        ShowReviewButton = ((roleId == (int)Roles.SuperAdmin || roleId == (int)Roles.Admin) && (x.FormStatus.Value == Constants.FormStatus_Submitted)) 
                                    })
                                    .ToListAsync();
            return userFormsDto;
        }

        private string GetFormName(int id)
        {
            return _kspContext.FormMaster.Where(x => x.Id == id).Select(x => x.Name).FirstOrDefault();
        }

        public async Task<bool> AddEmptyForm(int userId, int roleId, int templateId, string name)
        {
            var formMaster = await _kspContext
                                .FormMaster
                                .Include(x=> x.TemplateMapping)
                                .FirstOrDefaultAsync(x => x.Id == templateId);

            var mappings = await _kspContext.DepartmentMapping.Where(x => x.Name == "FormStatus" || x.Name == "Template").ToListAsync();

            var policeStations = _kspContext
                                    .FormMapping
                                    .Where(x=> x.FormId == formMaster.Id)
                                    .Select(x=> x.DepartmentId)
                                    .ToList();

            if(roleId == (int)Roles.Admin || roleId == (int)Roles.User)
            {
                var departments = _kspContext.UserToDepartmentMapping.Where(x => x.UserId == userId).Select(x=> x.PoliceStationId).ToList();
                policeStations = policeStations.Where(x => x.HasValue && departments.Contains(x.Value) ).ToList();
            }

            var newPS = policeStations;

            if(formMaster.TemplateMapping.Value == "Dynamic")
            {

                var existingPoliceStations = _kspContext.UserForm.Where(x => x.FormId == templateId).Select(x => x.PoliceStationNo).ToList();
                newPS = policeStations.Except(existingPoliceStations).ToList();

                if (newPS.Count() == 0)
                    throw new KspException("File has already been created. ", HttpStatusCode.BadRequest);

            }

            var draftStatus = mappings.Where(x => x.Name == "FormStatus" && x.Value == "Draft").FirstOrDefault().Id;

            foreach (var policeStationNo in newPS)
            {
                var extn = System.IO.Path.GetExtension(formMaster.SystemFileName);
                var fileContent = await DownloadTemplate(templateId);
                var systemFileName = $"{_formPath}{Guid.NewGuid()}{extn}";

                await SaveFile(fileContent.FileInBytes, systemFileName);

                var userForm = new UserForm()
                {
                    UserId = userId,
                    CreatedBy = userId,
                    CreatedDatetime = DateTime.Now,
                    ModifiedBy = userId,
                    ModifiedDatetime = DateTime.Now,
                    FormId = templateId,
                    FileName = formMaster.FileName,
                    Name = name,
                    SystemFileName = systemFileName,
                    StatusId = draftStatus, 
                    PoliceStationNo = policeStationNo
                };

                await _kspContext.UserForm.AddAsync(userForm);
            }

            await _kspContext.SaveChangesAsync();

            return true;
        }

        public async Task<DownloadContentDto> GetForm(int id)
        {
            var userForm = _kspContext
                            .UserForm
                            .Where(x => x.Id == id)
                            .FirstOrDefault();

            var (fileContent, fileName) = await DownloadFormFile(id);

            var downloadFile = new DownloadContentDto()
            {
                ContentType = "application/octet-stream",
                FileName = fileName,
                FileInBytes = fileContent
            };

            return await Task.FromResult(downloadFile);
        }
        public async Task<bool> UpdateTemplateContent(int userId, int roleId, UpdateTemplateContentDto updateTemplateContentDto)
        {
            var template = await _kspContext.FormMaster.Where(x => x.Id == updateTemplateContentDto.Id).SingleOrDefaultAsync();

            if (template == null)
                throw new KspException("Unable to find form", HttpStatusCode.NotFound);


            template.Content = updateTemplateContentDto.Content;
            template.ModifiedBy = userId;
            template.ModifiedDatetime = DateTime.Now;

            await _kspContext.SaveChangesAsync();


            return true;

        }
        public async Task<bool> UpdateTemplate(int userId, int templateId, string content)
        {
            var template = await _kspContext.FormMaster.Where(x => x.Id == templateId).SingleOrDefaultAsync();
            
            if (template == null)
                throw new KspException("Unable to find form", HttpStatusCode.NotFound);

            //var oldSysFileName = template.SystemFileName;

            //var sysFileName = await SaveTemplateContent(content);
            //template.SystemFileName = sysFileName;

            template.Content = content;
            template.ModifiedBy = userId;
            template.ModifiedDatetime = DateTime.Now;

            await _kspContext.SaveChangesAsync();

            //if (System.IO.File.Exists(oldSysFileName))
            //    System.IO.File.Delete(oldSysFileName);

            return true;

        }

        public async Task<string[]> GetUserInput(int formId)
        {
            var formDetails = await _kspContext.UserForm.FirstOrDefaultAsync(x => x.Id == formId);
            var userInput = formDetails.UserInput;

            if (string.IsNullOrEmpty(userInput))
                return null;

            var result = JsonConvert.DeserializeObject<string[]>(userInput);
            return result;
        }

        public async Task<bool> UpdateForm(int userId, int formId, IFormFile file, string content)
        {
            var form =   _kspContext.UserForm.SingleOrDefault(x => x.Id == formId);
            var oldSysFileName = form.SystemFileName;

            if (form == null)
                throw new KspException("Unable to find the form");
            
            var systemFileName = await SaveFormFile(content);

            //form.UserInput = content;
            form.SystemFileName = systemFileName;
            form.ModifiedBy = userId;
            form.ModifiedDatetime = DateTime.Now;

            await _kspContext.SaveChangesAsync();

            if (System.IO.File.Exists(oldSysFileName))
                System.IO.File.Delete(oldSysFileName);

            return true;
        }

        public async Task<bool> UpdateDocForm(int userId, int formId, string content)
        {
            var form = _kspContext.UserForm.SingleOrDefault(x => x.Id == formId);

            if (form == null)
                throw new KspException("Unable to find the form");

            form.UserInput = content;
            form.ModifiedBy = userId;
            form.ModifiedDatetime = DateTime.Now;

            await _kspContext.SaveChangesAsync();
            return true;

        }

        public async Task<bool> SubmitDocForm(int userId, int formId, string content)
        {
            var form = _kspContext.UserForm.SingleOrDefault(x => x.Id == formId);

            if (form == null)
                throw new KspException("Unable to find the form");

            form.UserInput = content;
            form.ModifiedBy = userId;
            form.ModifiedDatetime = DateTime.Now;
            form.StatusId = _kspContext.DepartmentMapping.Where(x => x.Name == "FormStatus" && x.Value == "Submitted").FirstOrDefault().Id;

            await _kspContext.SaveChangesAsync();
            return true;

        }


        private string GenerateUniqueCode()
        {
            var codes = Guid.NewGuid().ToString().Split('-');
            return $"{codes[0]}{codes[1]}";
        }

        private async Task<string> SaveTemplate(IFormFile file)
        {
            var extn = System.IO.Path.GetExtension(file.FileName);
            var systemFileName = $"{Guid.NewGuid()}.{extn}";
            var fileNameWithPath = $"{_templatePath}{systemFileName}";

            await SaveFile(file, fileNameWithPath);

            return fileNameWithPath;
        }

        private async Task<string> SaveTemplateContent(string fileContent)
        {
            var fileNameWithPath = $"{_templatePath}{Guid.NewGuid()}.json";

            await System.IO.File.WriteAllTextAsync(fileNameWithPath, fileContent);

            return fileNameWithPath;
        }

        private async Task<string> SaveFormFile(string fileContent)
        {
            var fileNameWithPath = $"{_formPath}{Guid.NewGuid()}.json";

            await System.IO.File.WriteAllTextAsync(fileNameWithPath, fileContent);

            return fileNameWithPath;
        }

        private async Task<(byte[], string)> DownloadTemplateFile(int id)
        {
            var file = await _kspContext
                                .FormMaster
                                .Where(x => x.Id == id)
                                .FirstOrDefaultAsync();

            var fileContent = await System.IO.File.ReadAllBytesAsync(file.SystemFileName);

            return (fileContent, file.FileName);
        }

        private async Task<(byte[], string)> DownloadFormFile(int id)
        {
            var file = await _kspContext
                                .UserForm
                                .Where(x => x.Id == id)
                                .FirstOrDefaultAsync();

            var fileContent = await System.IO.File.ReadAllBytesAsync(file.SystemFileName);

            return (fileContent, file.FileName);
        }

    

        private async Task SaveFile(byte[] fileContent, string fileNameWithPath)
        {
            await System.IO.File.WriteAllBytesAsync(fileNameWithPath, fileContent);
        }

        private async Task SaveFile(IFormFile file, string fileNameWithPath)
        {
            using (Stream fileStream = new FileStream(fileNameWithPath, FileMode.Create))
            {
                await file.CopyToAsync(fileStream);
            }
        }

        public async Task<string> GetTemplateContent(int templateId)
        {

            var template = await _kspContext.FormMaster.FindAsync(templateId);
            
            if (template == null)
                throw new KspException("Form file does not exist");

            //var fileContent = await System.IO.File.ReadAllTextAsync(template.SystemFileName);

            //return fileContent;

            return template.Content;

        }

        public async Task<string> GetFormContent(int formId)
        {

            var form = await _kspContext.UserForm.FindAsync(formId);
            //if (string.IsNullOrEmpty(form?.SystemFileName))
            //    throw new KspException("File does not exist");

            //var fileContent = await System.IO.File.ReadAllTextAsync(form.SystemFileName);
            var fileContent = form.UserInput;

            return fileContent;

        }

        public async Task<DownloadContentDto> DownloadExcelTemplate(int templateId)
        {
            var template = await _kspContext.FormMaster.FirstOrDefaultAsync(x => x.Id == templateId);

            if (template == null)
                throw new KspException("Invalid form id", System.Net.HttpStatusCode.NotFound);

            if (String.IsNullOrEmpty(template.SystemFileName))
                throw new KspException("File not uploaded", System.Net.HttpStatusCode.NotFound);

            var downloadContent = await DownloadFile(template.SystemFileName);

            downloadContent.FileName = template.FileName;

            return downloadContent;
        }

        public async Task<DownloadContentDto> DownloadExcelForm(int formId)
        {
            var form = await _kspContext.UserForm.FirstOrDefaultAsync(x => x.Id == formId);

            if (form == null)
                throw new KspException("Invalid form id", System.Net.HttpStatusCode.NotFound);

            if (String.IsNullOrEmpty(form.SystemFileName))
                throw new KspException("File not uploaded", System.Net.HttpStatusCode.NotFound);

            var downloadContent = await DownloadFile(form.SystemFileName);

            downloadContent.FileName = System.IO.Path.GetFileNameWithoutExtension(form.FileName) + ".xlsx";

            return downloadContent;
        }

        private async Task<DownloadContentDto> DownloadFile(string sysFileName)
        {

            if (!System.IO.File.Exists(sysFileName))
                throw new KspException("Could not locate file", System.Net.HttpStatusCode.NotFound);

            var fileContent = await System.IO.File.ReadAllTextAsync(sysFileName);
            var fileContentJson = Newtonsoft.Json.JsonConvert.DeserializeObject<ExcelContentDto[]>(fileContent);

            var excelFile = FileHelper.GetXLWorkbook(fileContentJson);
            var downloadContent = new DownloadContentDto();

            using (var stream = new MemoryStream())
            {
                excelFile.SaveAs(stream);
                downloadContent.FileInBytes = stream.ToArray();
            }
            downloadContent.ContentType = "application/octet-stream";
            return downloadContent;
        }

        public async Task<bool> SubmitForApproval(int userId, int id)
        {
            var form = await _kspContext.UserForm.FirstOrDefaultAsync(x => x.Id == id);

            if (form == null)
                throw new KspException("Invalid form id", HttpStatusCode.NotFound);

            var approval = await _kspContext
                                    .DepartmentMapping
                                    .FirstOrDefaultAsync(x => x.Name == "FormStatus" && x.Value == "Submitted");

            form.StatusId = approval.Id;
            form.ModifiedBy = userId;
            form.ModifiedDatetime = DateTime.Now;

            await _kspContext.SaveChangesAsync();

            return true;
        }

        public async Task<bool> ApproveForm(int userId, int id)
        {
            var form = await _kspContext.UserForm.FirstOrDefaultAsync(x => x.Id == id);

            if (form == null)
                throw new KspException("Invalid form id", HttpStatusCode.NotFound);

            var approval = await _kspContext
                                    .DepartmentMapping
                                    .FirstOrDefaultAsync(x => x.Name == "FormStatus" && x.Value == "Approved");

            form.StatusId = approval.Id;
            form.ModifiedBy = userId;
            form.ModifiedDatetime = DateTime.Now;

            await _kspContext.SaveChangesAsync();

            return true;
        }
        public async Task<bool> RejectForm(int userId, int id)
        {
            var form = await _kspContext.UserForm.FirstOrDefaultAsync(x => x.Id == id);

            if (form == null)
                throw new KspException("Invalid form id", HttpStatusCode.NotFound);

            var approval = await _kspContext
                                    .DepartmentMapping
                                    .FirstOrDefaultAsync(x => x.Name == "FormStatus" && x.Value == "Rejected");

            form.StatusId = approval.Id;
            form.ModifiedBy = userId;
            form.ModifiedDatetime = DateTime.Now;

            await _kspContext.SaveChangesAsync();

            return true;
        }

        public async Task<UserFormDto> GetFormDetails(int formId)
        {
            var form = await _kspContext.UserForm.FirstOrDefaultAsync(x => x.Id == formId);

            if (form == null)
                throw new KspException("Invalid form id", HttpStatusCode.NotFound);

            var formDto = _kspContext
                            .UserForm
                            .Include(x => x.FormMaster)
                            .ThenInclude(x => x.TemplateMapping)
                                .Include(x => x.User)
                            .Include(x => x.Created)
                            .Include(x => x.Modified)
                            .Select(x => new UserFormDto()
                            {
                                Id = x.Id,
                                FormId = x.FormId.Value,
                                FormName = x.Name,
                                TemplateName = x.FormMaster.Name,
                                StatusId = x.StatusId ?? -1,
                                CreatedBy = x.Created.Username,
                                CreatedOn = x.CreatedDatetime,
                                ModifiedBy = x.Modified.Username,
                                ModifiedOn = x.ModifiedDatetime,
                                TemplateType = x.FormMaster.TemplateMapping.Value
                            })
                            .Where(x=> x.Id == formId)
                            .FirstOrDefault();

            formDto.Status = await _kspContext.DepartmentMapping.Where(x => x.Id == formDto.StatusId).Select(x => x.Value).FirstOrDefaultAsync();


            return formDto;
        }

        public async Task<int> AddNewUserForm(int userId, int roleId, AddNewUserFormDto addNewUserFormDto)
        {
            var template = await _kspContext.FormMaster.FindAsync(addNewUserFormDto.FormId);

            var userFormCount = await _kspContext.UserForm.CountAsync(x => x.FormMaster.Id == addNewUserFormDto.FormId);

            if (template == null)
                throw new KspException("Invalid formId", HttpStatusCode.BadRequest);

            var policeStation = await _kspContext.DepartmentMapping.FindAsync(addNewUserFormDto.PoliceStationId);

            if (policeStation == null)
                throw new KspException("Invalid Police Station", HttpStatusCode.BadRequest);

            var formStatus = await _kspContext.DepartmentMapping.Where(x => x.Name == "FormStatus" && x.Value == Constants.FormStatus_Draft).FirstOrDefaultAsync();

            var userFormCode = $"{template.Code}_{userFormCount}_{policeStation.Value.Replace(" ", "")}";

            //var parsedAnswers = FormBuilderHelper.ParseUserResponse(template.Content, addNewUserFormDto.Content);

            UserForm userForm = new UserForm()
            {
                FormId = addNewUserFormDto.FormId,
                StatusId = formStatus.Id,
                Name = addNewUserFormDto.FormName ?? template.Name,
                UserInput = addNewUserFormDto.Content,
                PoliceStationNo = addNewUserFormDto.PoliceStationId,
                UserInputBase64 = addNewUserFormDto.ContentBase64,
                //CandidateName = addNewUserFormDto.CandidateName,
                //CandidateEmailId = addNewUserFormDto.CandidateEmailId,
                //CandidatePhoneNo = addNewUserFormDto.CandidatePhoneNo,
                Code = userFormCode,
                CreatedBy = userId,
                CreatedDatetime = DateTime.Now,
                ModifiedBy = userId,
                ModifiedDatetime = DateTime.Now
            };

            await _kspContext.UserForm.AddAsync(userForm);
            await _kspContext.SaveChangesAsync();

            return userForm.Id;
        }

        public async Task<CompleteFormDto> GetCompleteFormDetails(int userId, int userFormId)
        {
            var userForm = await _kspContext.UserForm.FindAsync(userFormId);

            if (userForm == null)
                throw new KspException("Invalid form", HttpStatusCode.BadRequest);


            var templateId = userForm.FormId;

            var template = await _kspContext.FormMaster.FindAsync(templateId);

            if (template == null)
                throw new KspException("Invalid template", HttpStatusCode.BadRequest);

            var completeFormData = new CompleteFormDto()
            {
                 Id= userForm.Id,
                 FormContent = template.Content,
                 UserResponse = userForm.UserInput
            };


            return completeFormData;

        }

        public async Task<bool> ActionOnForm(int userId, ActionOnFromDto actionDto)
        {
            var userForm = await _kspContext.UserForm.FindAsync(actionDto.UserFormId);

            if (userForm == null)
                throw new KspException("Invalid user form id", HttpStatusCode.BadRequest);

            var status = "";

            switch (actionDto.Status)
            {
                case "AP": status = Constants.FormStatus_Approved; break;
                case "RJ": status = Constants.FormStatus_Rejected; break;
                case "SU": status = Constants.FormStatus_Submitted; break;
                default:status = Constants.FormStatus_Draft;break;
            }

            var statusObj = await _kspContext.DepartmentMapping.Where(x => x.Name == "FormStatus" && x.Value == status).FirstOrDefaultAsync();


            userForm.StatusId = statusObj.Id;
            userForm.Comment = actionDto.Comment;
            userForm.ModifiedBy = userId;
            userForm.ModifiedDatetime = DateTime.Now;

            await _kspContext.SaveChangesAsync();
            return true;
        }

        public async Task<DownloadContentDto> DownloadDynamicFiles(int userId, int roleId, int templateId)
        {

            var template = await _kspContext.FormMaster.FindAsync(templateId);

            if (template == null)
                throw new KspException("Unable to fetch the template", HttpStatusCode.InternalServerError);

            var formTemplate = FormBuilderHelper.GetTemplate(template.Content);
            var questions = formTemplate.GetFormQuestionGroups();

            var userFormContents = await _kspContext
                                    .UserForm
                                    .Where(x => x.FormId == templateId)
                                    .Select(x => new UserResponseParseDto
                                    {
                                       UserResponse = FormBuilderHelper.GetUserResponseDetail(questions, x.UserInput)
                                    })
                                    .ToListAsync();



            var workbook = FileHelper.GetReport(template.Name, formTemplate, userFormContents);
            
            var downloadContent = new DownloadContentDto();
            using (var stream = new MemoryStream())
            {
                workbook.SaveAs(stream);
                downloadContent.FileInBytes = stream.ToArray();
            }

            downloadContent.ContentType = "application/octet-stream";
            downloadContent.FileName = template.Name + ".xlsx";
            return downloadContent;

        }

        public async Task<bool> UpdateDepartment(int userId, int roleId, UpdateDetartmentDto updateDto)
        {
            var template = await _kspContext
                                .FormMaster
                                    .Include(x => x.FormMapping)
                                .Where(x => x.Id == updateDto.Id)
                                .FirstOrDefaultAsync();

            if (template == null)
                throw new KspException("Invalid template id", HttpStatusCode.BadRequest);

            var inputPoliceStationIds = updateDto.PoliceStationIds.Split(",").Select(x => Convert.ToInt32(x)).ToList();

            var dbDepts = await _kspContext
                                .DepartmentMapping
                                .Where(x => inputPoliceStationIds.Contains(x.Id))
                                .Select(x => x.Id)
                                .ToListAsync();

            var invalidPoliceStationIds = inputPoliceStationIds.Where(x => !dbDepts.Contains(x));

            if (invalidPoliceStationIds.Any())
                throw new KspException("Invalid Police Station Ids", HttpStatusCode.BadRequest);

            foreach (var item in template.FormMapping)
            {
                item.IsActive = true;

                if (!inputPoliceStationIds.Contains(item.DepartmentId.Value))
                {
                    item.IsActive = false;
                    item.ModifiedBy = userId;
                    item.ModifiedDatetime = DateTime.Now;
                }
            }
         
            var newMappings = inputPoliceStationIds
                            .Where(y => !template.FormMapping.Select(x => x.DepartmentId).Contains(y))
                            .Select(x => new FormMapping()
                            {
                                FormId = updateDto.Id,
                                DepartmentId = x,
                                CreatedBy = userId,
                                CreatedDatetime = DateTime.Now,
                                ModifiedBy = userId,
                                ModifiedDatetime = DateTime.Now,
                                IsActive = true
                            })
                            .ToList();

            await _kspContext.FormMapping.AddRangeAsync(newMappings);
            await _kspContext.SaveChangesAsync();

            return true;
        }

        public async Task<DownloadContentDto> DownloadStaticFiles(int userId, int roleId, int templateId, string formIds = null)
        {
            var template = await _kspContext.FormMaster.FindAsync(templateId);

            if (template == null)
                throw new KspException("No template found", HttpStatusCode.NotFound);

            var forms = GetUserForms(userId, roleId, templateId);

            if (!string.IsNullOrEmpty(formIds))
            {
                var formList = formIds.Split(",").Select(x => Convert.ToInt32(x)).ToList();
                forms = forms
                            .Where(x => formList.Contains(x.Id));
            }

            var formImages = await forms
                                    .Select(x => new { FileName = $"{x.Name}_{Guid.NewGuid()}.png", Content = FileHelper.GetBytes(x.UserInputBase64) })
                                    .ToListAsync();


            using (var memoryStream = new MemoryStream())
            {
                using (var ziparchive = new ZipArchive(memoryStream, ZipArchiveMode.Create, true))
                {
                    for (int i = 0; i < formImages.Count(); i++)
                    {
                        var zipEntry = ziparchive.CreateEntry(formImages[i].FileName);

                        using (var imgStream = new MemoryStream(formImages[i].Content))
                        {
                            using (var zipEntryStream = zipEntry.Open()) 
                            {

                                await imgStream.CopyToAsync(zipEntryStream);
                            }
                        }
                    }
                }

                var fileContentDto = new DownloadContentDto()
                {
                    FileInBytes = memoryStream.ToArray(),
                    FileName = $"{template.Name}_{DateTime.Now.ToString("dd-MM-yyyy")}.zip",
                    ContentType = "application/zip"
                };

                return fileContentDto;
            }

            throw new KspException("Unable to link download files", HttpStatusCode.InternalServerError);
        }

        public async Task<FormsSummaryDto> GetFormsSummaryDashboard(int userId, int roleId)
        {
            var summaryDto = new FormsSummaryDto();


            var templates = GetTemplates(userId, roleId, policeStationIds: null);

            summaryDto.Created = await templates.CountAsync();
            summaryDto.Published = await templates.CountAsync(x => x.IsPublished == true);
            summaryDto.Unpublished = await templates.CountAsync(x => x.IsPublished == false);

            var forms = GetUserForms(userId, roleId, null, null);

            summaryDto.Approved = await forms.CountAsync(x => x.FormStatus.Value == "Approved");
            summaryDto.Rejected = await forms.CountAsync(x => x.FormStatus.Value == "Rejected");
            summaryDto.Submitted = await forms.CountAsync(x => x.FormStatus.Value == "Submitted");

            return summaryDto;
        }

        public async Task<DepartmentSummaryDo> GetDepartmentWiseSummaries(int userId, int roleId)
        {
            DepartmentSummaryDo result = new DepartmentSummaryDo();
            var forms = GetUserForms(userId, roleId, null );

            result.Divisions = await forms
                            .GroupBy(x => x.PoliceStation.Parent.Parent.Value)
                            .Select(x => new DepartmentWiseSummaryDto() { Department = x.Key, Count = x.Count() })
                            .ToListAsync();


            result.SubDivisions = await forms
                            .GroupBy(x => x.PoliceStation.Parent.Value)
                            .Select(x => new DepartmentWiseSummaryDto() { Department = x.Key, Count = x.Count() })
                            .ToListAsync();

            result.PoliceStations = await forms
                            .GroupBy(x => x.PoliceStation.Value)
                            .Select(x => new DepartmentWiseSummaryDto() { Department = x.Key, Count = x.Count() })
                            .ToListAsync();

            return result;

        }
    }
}
