﻿using AutoMapper;
using Examroom.KarnatakaStatePolice.DataAccess;
using Examroom.KarnatakaStatePolice.DataAccess.Models;
using Examroom.KarnatakaStatePolice.Service.Dtos;
using Examroom.KarnatakaStatePolice.Service.Helpers;
using Examroom.KarnatakaStatePolice.Service.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Examroom.KarnatakaStatePolice.Service.Services
{
    public interface IDepartmentService
    {
        Task<List<RoleDto>> GetDepartmentDetails(int userId, int roleId, int levelId, string parentIds = null);
        Task<List<DepartmentMappingViewDto>> GetDepartmentDropDownList(int levelId);
        Task<int> AddDepartmentDetails(int userId, RoleDto departmentMapping);
        Task<List<RoleDto>> GetDeptMapping(string name);
        Task<List<RoleDto>> GetDivisions(string parentIds = null);
        Task<List<RoleDto>> GetSubDivisions(string parentIds = null);
        Task<List<RoleDto>> GetPoliceStations(string parentIds = null);
        Task<DepartmentSummaryDo> GetDepartmentSummary(int userId, int roleId);

    }
    public class DepartmentService : BaseApiService, IDepartmentService
    {
        readonly IFormService _formService;
        public DepartmentService(IFormService formService, KspContext kspContext, IMapper mapper)
           : base(kspContext, mapper)
        {
            _formService = formService;
        }

        public async Task<List<RoleDto>> GetDepartmentDetails(int userId, int roleId, int levelId, string parentIds = null)
        {
            var name = "";

            switch (levelId)
            {
                case 1: name = "Place-Name"; break;
                case 2: name = "Sub-Division"; break;
                case 3: name = "Police-Station"; break;
            }

            List<RoleDto> userDeptsDto = null;
            List<DepartmentMapping> deptMappingDb = null;

            if(roleId == (int)Roles.Admin || roleId == (int)Roles.User)
            {
                var userDepts = _kspContext
                                        .UserToDepartmentMapping
                                            .Include(x => x.PoliceStation)
                                                .ThenInclude(x => x.Parent)
                                                    .ThenInclude(x => x.Parent)
                                        .Where(x => x.UserId == userId && x.IsActive == true);

                if (levelId == 1)
                {
                    deptMappingDb = await userDepts
                                    .Select(x => x.PoliceStation.Parent.Parent)
                                    .ToListAsync(); 
                }

                else if (levelId == 2)
                {
                    deptMappingDb = userDepts
                                    .Select(x => x.PoliceStation.Parent)
                                    .ToList();
                }

                else if (levelId == 3)
                {
                    deptMappingDb = userDepts
                                    .Select(x => x.PoliceStation)
                                    .ToList();
                }

                if (levelId != 4)
                {
                    userDeptsDto = _mapper.Map<List<RoleDto>>(deptMappingDb);
                    return userDeptsDto;
                }


            }


            if (levelId == 4)
                return await GetFormsForPoliceStation(userId, roleId, parentIds);


            var departmentsList = await _kspContext.DepartmentMapping
                                    .Where(x => x.LevelId == levelId && x.IsActive && x.Name == name)
                                    .Include(x=> x.Created)
                                    .Include(x=> x.Modified)
                                    .Include(x => x.Parent)
                                    .ThenInclude(x => x.Parent)
                                    .ToListAsync();

            if (!string.IsNullOrEmpty(parentIds))
            {
                var parents = parentIds.Split(',').Select(x => Convert.ToInt32(x));
                departmentsList = departmentsList.Where(x => parents.Any(p => p == x.ParentId)).ToList();
            }

            return _mapper.Map<List<RoleDto>>(departmentsList);
        }

        private async Task<List<RoleDto>> GetFormsForPoliceStation(int userId, int roleId, string policeStationIds)
        {
            var policeStations = policeStationIds.Split(',').Select(x=> Convert.ToInt32(x)).ToList();


            var formIds =  _kspContext.FormMapping.Where(x => policeStations.Contains(x.DepartmentId.Value)).Where(x=> x.FormId.HasValue).Select(x => x.FormId.Value).ToList();

            if(roleId == (int)Roles.Admin || roleId == (int)Roles.User)
            {
                var userFormIds = _kspContext.UserToDepartmentMapping.Where(x => x.UserId == userId).Select(x => x.PoliceStationId).Distinct().ToList();
                formIds = userFormIds;
            }

            var forms = await _kspContext.FormMaster.Where(x => formIds.Contains(x.Id)).ToListAsync();

            var deptFormat = forms
                                .Select(x => new RoleDto()
                                {
                                    Id = x.Id,
                                    LevelId = 4,
                                    IsActive = true,
                                    Name = "Form",
                                    Value = x.Name,
                                    OrderNo = 1,
                                    ParentId = policeStationIds.FirstOrDefault()
                                })
                                .ToList();

            return deptFormat;

        }


        public async Task<List<RoleDto>> GetDivisions(string parentIds = null)
        {
            return await GetDepartmentDetails("Place-Name", parentIds);
        }

        public async Task<List<RoleDto>> GetSubDivisions(string parentIds = null)
        {
            return await GetDepartmentDetails("Sub-Division", parentIds);
        }

        public async Task<List<RoleDto>> GetPoliceStations(string parentIds = null)
        {
            return await GetDepartmentDetails("Police-Station", parentIds);
        }

        private async Task<List<RoleDto>> GetDepartmentDetails(string name, string parentIds = null)
        {
            var departmentsList = await _kspContext.DepartmentMapping
                                    .Where(x => x.Name == name && x.IsActive)
                                    .Include(x => x.Created)
                                    .Include(x => x.Modified)
                                    .Include(x => x.Parent)
                                    .ThenInclude(x => x.Parent)
                                    .ToListAsync();

            if (!string.IsNullOrEmpty(parentIds))
            {
                var parents = parentIds.Split(',').Select(x => Convert.ToInt32(x));
                departmentsList = departmentsList.Where(x => parents.Any(p => p == x.ParentId)).ToList();
            }

            return _mapper.Map<List<RoleDto>>(departmentsList);
        }

        public async Task<List<RoleDto>> GetDeptMapping(string name)
        {
            var departmentsList = await _kspContext.DepartmentMapping
                                    .Where(x => x.Name == name && x.IsActive)
                                    .Include(x => x.Created)
                                    .Include(x => x.Modified)
                                    .Include(x => x.Parent)
                                    .ThenInclude(x => x.Parent)
                                    .ToListAsync();

            return _mapper.Map<List<RoleDto>>(departmentsList);
        }

        public async Task<int> AddDepartmentDetails(int userId, RoleDto departmentMappingDto)
        {
            if (departmentMappingDto == null)
                throw new KspException("Invalid input", System.Net.HttpStatusCode.BadRequest);

            if (string.IsNullOrEmpty(departmentMappingDto.Name?.Trim()))
                throw new KspException("Invalid name", System.Net.HttpStatusCode.BadRequest);

            if (string.IsNullOrEmpty(departmentMappingDto.Value?.Trim()))
                throw new KspException("Invalid value", System.Net.HttpStatusCode.BadRequest);
            
            if (DuplicateDepartmentCheck(departmentMappingDto))
                throw new KspException("Duplicate Entry", System.Net.HttpStatusCode.BadRequest);

            if (departmentMappingDto.Id == default(int))
            {
                var departmentMapping = _mapper.Map<DepartmentMapping>(departmentMappingDto);
                departmentMapping.Name = (int) DepartmentLevel.Department == departmentMappingDto.LevelId.Value ? Constants.Department
                    : ((int)DepartmentLevel.SubDivision == departmentMappingDto.LevelId.Value) ? Constants.SubDivision : Constants.PoliceStation;
                departmentMapping.IsActive = true;
                departmentMapping.SetDefaultValues(isAdd: true, userId);
                _kspContext.DepartmentMapping.Add(departmentMapping);
            }
            else
            {
                var dbEntity = _kspContext.DepartmentMapping.Where(x => x.Id == departmentMappingDto.Id).FirstOrDefault();
                if (!departmentMappingDto.IsActive && CascadingDeleteCheck(departmentMappingDto))
                {
                    throw new KspException($"Department is in use. It cannot be deleted", System.Net.HttpStatusCode.BadRequest);
                }
                if (dbEntity != null && !departmentMappingDto.IsActive)
                {
                    dbEntity.IsActive = false;
                }
                else if (dbEntity != null)
                {
                    dbEntity.Name = departmentMappingDto.Name;
                    dbEntity.ParentId = departmentMappingDto.ParentId;
                    dbEntity.Value = departmentMappingDto.Value;
                }
            }
            return await _kspContext.SaveChangesAsync();
        }

        public async Task<List<DepartmentMappingViewDto>> GetDepartmentDropDownList(int levelId)
        {
            var departmentsList = await _kspContext.DepartmentMapping.Where(x => x.LevelId == levelId && x.IsActive == true).ToListAsync();
            return _mapper.Map<List<DepartmentMappingViewDto>>(departmentsList);
        }


        private bool CascadingDeleteCheck(RoleDto departmentMappingDto)
        {
            return _kspContext.DepartmentMapping.Where(x => x.IsActive && x.ParentId == departmentMappingDto.Id).Any();
        }
        private bool DuplicateDepartmentCheck(RoleDto departmentMappingDto)
        {
            return _kspContext.DepartmentMapping.Where(x => x.Value == departmentMappingDto.Value && x.IsActive == true && x.Id != departmentMappingDto.Id).Any();
        }


        public async Task<DepartmentSummaryDo> GetDepartmentSummary(int userId, int roleId)
        {
            var divisions = await GetDivisions();
            var subDivisions = await GetSubDivisions();
            var policeStations = await GetPoliceStations();

            var result =  new DepartmentSummaryDo()
            {
                Divisions = divisions.Select(x =>new DepartmentWiseSummaryDto() { Department =  x.Value }).ToList(),
                SubDivisions = subDivisions.Select(x => new DepartmentWiseSummaryDto() { Department = x.Value }).ToList(),
                PoliceStations = policeStations.Select(x => new DepartmentWiseSummaryDto() { Department = x.Value }).ToList()
            };

            var forms = await _formService.GetDepartmentWiseSummaries(userId, roleId);

            result.Divisions.ForEach(x =>
            {
                x.Count = forms.Divisions.Where(y => y.Department == x.Department).Count();
            });
            result.SubDivisions.ForEach(x =>
            {
                x.Count = forms.SubDivisions.Where(y => y.Department == x.Department).Count();
            });
            result.PoliceStations.ForEach(x =>
            {
                x.Count = forms.PoliceStations.Where(y => y.Department == x.Department).Count();
            });
            return result;
        }

        
    }
}
