﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Examroom.KarnatakaStatePolice.Service.Models
{
    //LevelId is Maintained in Enum Level any changes done here should reflect in UI
    public enum DepartmentLevel
    {
        Department = 1,
        SubDivision = 2,
        PoliceStation = 3
    }
}
