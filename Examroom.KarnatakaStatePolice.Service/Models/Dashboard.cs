﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Examroom.KarnatakaStatePolice.Service.Models
{
    public class FormsSummaryDto
    {
        public int Created { get; set; }
        public int Published { get; set; }
        public int Unpublished { get; set; }
        public int Submitted { get; set; }
        public int Approved { get; set; }
        public int Rejected { get; set; }
    }


    public class DepartmentSummaryDo
    {
        public List<DepartmentWiseSummaryDto> Divisions { get; set; }
        public List<DepartmentWiseSummaryDto> SubDivisions { get; set; }
        public List<DepartmentWiseSummaryDto> PoliceStations { get; set; }
    }

    public class DepartmentWiseSummaryDto
    {
        public string Department { get; set; }
        public int Count { get; set; }
    }
}
