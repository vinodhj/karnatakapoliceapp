﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Examroom.KarnatakaStatePolice.Service.Models
{
    public class ErrorMessageDto
    {
        public string Message { get; set; }

        public ErrorMessageDto(string message)
        {
            Message = message;
        }

        public ErrorMessageDto Get(string message)
        {
            return new ErrorMessageDto(message);
        }
    }
}
