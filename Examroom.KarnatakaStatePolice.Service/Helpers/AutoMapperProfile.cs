﻿﻿using AutoMapper;
using Examroom.KarnatakaStatePolice.DataAccess.Models;
using Examroom.KarnatakaStatePolice.Service.Dtos;

namespace Examroom.KarnatakaStatePolice.Service.Helpers
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<RoleDto, DepartmentMapping>().ReverseMap();
            CreateMap<PoliceStationDto, DepartmentMapping>().ReverseMap();
            CreateMap<CreatePoliceStationDto, DepartmentMapping>().ReverseMap();
            CreateMap<UpdatePoliceStationDto, DepartmentMapping>().ReverseMap();
            CreateMap<DepartmentMapping, CreateSubDivisionDto>().ReverseMap();
            CreateMap<RoleDto, CreateSubDivisionDto>().ReverseMap();

            CreateMap<DepartmentMappingViewDto, DepartmentMapping>().ReverseMap();
            CreateMap<UserToDepartmentMappingDto, UserToDepartmentMapping>().ReverseMap();
            CreateMap<UserDto, User>().ReverseMap();
            CreateMap<UserRoleDto, UserRole>().ReverseMap();
            CreateMap<ErrorLogDto, ErrorLog>().ReverseMap(); 
            CreateMap<AddUserDto, User>().ReverseMap();

            CreateMap<CreateDivisionDto, DepartmentMapping>()
                .ForMember(dest => dest.Value, opt => opt.MapFrom(src => src.Value))
                .ForMember(dest => dest.ParentId, opt => opt.MapFrom(src => src.ParentId == default(int) ? null : src.ParentId))
                .ForMember(dest => dest.OrderNo, opt => opt.MapFrom(src => src.OrderNo))
                .ReverseMap();
        }
    }
}

