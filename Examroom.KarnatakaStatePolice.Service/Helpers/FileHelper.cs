﻿using Examroom.KarnatakaStatePolice.Service.Dtos;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using DocumentFormat.OpenXml;
using ClosedXML.Excel;
using Examroom.KarnatakaStatePolice.Service.FormBuilder;
using System.Drawing;
using Examroom.KarnatakaStatePolice.DataAccess;

namespace Examroom.KarnatakaStatePolice.Service.Helpers
{
    public enum TemplateType
    {
        Static , Dynamic
    }
    public static class FileHelper
    {
        public static TemplateType GetTemplateType(string fileName)
        {
            var extn = System.IO.Path.GetExtension(fileName);

            extn = extn.ToLower();

            if (extn.Contains("html") || extn.Contains("htm"))
                return TemplateType.Static;

            return TemplateType.Dynamic;
        }
        public static string ReadFile(IFormFile file)
        {
            if (file == null)
                throw new KspException("File Not Found", System.Net.HttpStatusCode.BadRequest);

            using (var ms = new MemoryStream())
            {
                file.CopyTo(ms);
                var fileBytes = ms.ToArray();
                // act on the Base64 data
                string s = Convert.ToBase64String(fileBytes);
                return s;
            }
        }

        public static Image GetImage(string base64)
        {
            //if (string.IsNullOrEmpty(base64))
            //    return null;

            byte[] bytes = Convert.FromBase64String("R0lGODlhAQABAIAAAAAAAAAAACH5BAAAAAAALAAAAAABAAEAAAICTAEAOw==");

            Image image;
            using (MemoryStream ms = new MemoryStream(bytes))
            {
                image = Image.FromStream(ms);
            }

            return image;

        }

        public static byte[] GetBytes(string fileContent)
        {
            fileContent = "R0lGODlhAQABAIAAAAAAAAAAACH5BAAAAAAALAAAAAABAAEAAAICTAEAOw==";
            var bytes = System.Convert.FromBase64String(fileContent);
            return bytes;
        }
        public static Stream GetStream(string fileContent)
        {
            var bytes = System.Convert.FromBase64String(fileContent);
            Stream stream = new MemoryStream(bytes);
            return stream;
        }


        public static XLWorkbook GetXLWorkbook(ExcelContentDto[] fileContentJson)
        {
            var workbook = new ClosedXML.Excel.XLWorkbook();

            if (fileContentJson == null)
                throw new KspException("Invalid file");

            foreach (var sheetContent in fileContentJson)
            {
                IXLWorksheet worksheet = workbook.Worksheets.Add(sheetContent.Name);

                if (sheetContent.Rows.Keys.Contains("len"))
                    sheetContent.Rows.Remove("len");

                var rows = sheetContent.Rows.Values.Select(x => Newtonsoft.Json.JsonConvert.DeserializeObject<ExcelRow>(Newtonsoft.Json.JsonConvert.SerializeObject(x))).ToList();

                int rowIndex =1;
                foreach(var row in rows)
                {
                    int colIndex = 1;

                    foreach (var col in row.Cells.Values)
                    {
                        worksheet.Cell(rowIndex, colIndex).Value = col.Text ?? "";
                        ApplyStyle(worksheet.Cell(rowIndex, colIndex), sheetContent.Styles, col.Style);
                        colIndex++;
                    }
                    rowIndex++;
                }
            }


            return workbook;
        }

        
        private static void ApplyStyle(IXLCell cell, ExcelStyle[] styles, int? style)
        {
            if ((style ?? -1) < 0)
                return;

            if (style.Value >= styles?.Length)
                return;

            var currStyle = styles[style.Value];

            if (string.IsNullOrEmpty(currStyle.Align))
            {
                XLAlignmentHorizontalValues horizontalValues = XLAlignmentHorizontalValues.General;

                if (currStyle.Align?.ToLower() == "center")
                    horizontalValues = XLAlignmentHorizontalValues.Center;
                else if (currStyle.Align?.ToLower() == "left")
                    horizontalValues = XLAlignmentHorizontalValues.Left;
                else if (currStyle.Align?.ToLower() == "right")
                    horizontalValues = XLAlignmentHorizontalValues.Right;
                else if (currStyle.Align?.ToLower() == "justify")
                    horizontalValues = XLAlignmentHorizontalValues.Justify;

                cell.Style.Alignment.SetHorizontal(horizontalValues);
            }

            if (!string.IsNullOrEmpty(currStyle.Color))
            {
                cell.Style.Font.FontColor = XLColor.FromHtml(currStyle.Color);
            }

            if (!string.IsNullOrEmpty(currStyle.Bgcolor))
            {
                var bgColor = XLColor.FromHtml(currStyle.Bgcolor);
                cell.Style.Fill.SetBackgroundColor(bgColor);
            }

            if(currStyle.Border != null)
            {
                cell.Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                cell.Style.Border.BottomBorderColor = XLColor.FromHtml(currStyle.Border.Bottom[1]);

                cell.Style.Border.TopBorder = XLBorderStyleValues.Medium;
                cell.Style.Border.TopBorderColor = XLColor.FromHtml(currStyle.Border.Top[1]);

                cell.Style.Border.LeftBorder = XLBorderStyleValues.Medium;
                cell.Style.Border.LeftBorderColor = XLColor.FromHtml(currStyle.Border.Left[1]);

                cell.Style.Border.RightBorder = XLBorderStyleValues.Medium;
                cell.Style.Border.RightBorderColor = XLColor.FromHtml(currStyle.Border.Right[1]);

            }
        }

        public static XLWorkbook GetReport(string templateName, FormTemplate formTemplate, List<UserResponseParseDto> userFormContents)
        {

            var questions = formTemplate.GetFormQuestionGroups();
            var qsFormats = formTemplate.GetFormQuestionsFormat();

            var workbook = new XLWorkbook();
            IXLWorksheet worksheet = workbook.Worksheets.Add(templateName);

            if (qsFormats.Any(x=> x.MergeToNextRow == true))
            {
                AddBodyWithHeaders(worksheet, questions, qsFormats, userFormContents);
                return workbook;
            }

            AddHeaders(worksheet, questions, qsFormats, 1);
            AddBody(worksheet, questions, qsFormats, userFormContents);

            return workbook;
        }

        private static void AddHeaders(IXLWorksheet worksheet, List<FormQuestionGroup> questions, List<ParsingMetaData> qsFormats, int rowIndex)
        {

            worksheet.Cell(rowIndex, 1).Value = "Sl No";
            worksheet.Cell(rowIndex, 1).Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin);
            worksheet.Cell(rowIndex, 1).Style.Fill.SetBackgroundColor(XLColor.LightGray);


            var cellIndex = 2;
            var qsIndex = 0;

            foreach (var qsFormat in qsFormats)
            {
                if (qsFormat.MergeToNextRow == true)
                    break;

                worksheet.Cell(rowIndex, cellIndex).Value = questions.ElementAt(qsIndex).Label;
                worksheet.Cell(rowIndex, cellIndex).Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin);
                worksheet.Cell(rowIndex, cellIndex).Style.Fill.SetBackgroundColor(XLColor.LightGray);

                worksheet.Range(worksheet.Cell(1, cellIndex), worksheet.Cell(1, cellIndex + qsFormat.ColSpan.Value - 1)).Merge();
                cellIndex += qsFormat.ColSpan.Value;
                qsIndex++;
            }
        }

        private static void AddBodyWithHeaders(IXLWorksheet worksheet, List<FormQuestionGroup> questions, List<ParsingMetaData> qsFormats, List<UserResponseParseDto> userFormContents)
        {
            int rowIndex = 1;
            int qsIndex = 0;
            foreach (var userForm in userFormContents)
            {

                AddHeaders(worksheet, questions, qsFormats, rowIndex);
                rowIndex++;

                worksheet.Cell(rowIndex, 1).Value = ++qsIndex;
                worksheet.Cell(rowIndex, 1).Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin);


                var cellIndex = 2;
                for (int i = 0; i < questions.Count(); i++)
                {

                    var qsFormat = qsFormats.ElementAt(i);
                    var colSpan = qsFormat.ColSpan.Value - 1;

                    if (qsFormat.MergeToNextRow == true)
                    {
                        rowIndex++;
                        cellIndex = 1;
                        colSpan++;
                    }

                    worksheet.Cell(rowIndex, cellIndex).Value = userForm.UserResponse.ElementAt(i).Value ?? "";

                    if (qsFormat.Orientation == Constants.Orientation_Vertical)
                    {
                        worksheet.Cell(rowIndex, cellIndex).Style.Alignment.SetTopToBottom(true);
                    }

                    worksheet.Cell(rowIndex, cellIndex).Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin);
                    worksheet.Range(worksheet.Cell(rowIndex, cellIndex), worksheet.Cell(rowIndex, cellIndex + colSpan)).Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin);
                    worksheet.Range(worksheet.Cell(rowIndex, cellIndex), worksheet.Cell(rowIndex, cellIndex + colSpan)).Merge();
                    cellIndex += qsFormat.ColSpan.Value;
                }
                rowIndex++;
            }
        }
        private static void AddBody(IXLWorksheet worksheet, List<FormQuestionGroup> questions, List<ParsingMetaData> qsFormats, List<UserResponseParseDto> userFormContents)
        {
            int rowIndex = 2;
            var qsIndex = 0;
            foreach (var userForm in userFormContents)
            {

                worksheet.Cell(rowIndex, 1).Value = ++qsIndex;
                worksheet.Cell(rowIndex, 1).Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin);
                worksheet.Cell(rowIndex, 1).Style.Fill.SetBackgroundColor(XLColor.LightGray);
                
                var cellIndex = 2;

                for (int i = 0; i < questions.Count(); i++)
                {

                    var qsFormat = qsFormats.ElementAt(i);
                    var colSpan =  qsFormat.ColSpan.Value - 1;

                    if (qsFormat.MergeToNextRow == true)
                    {
                        rowIndex++;
                        cellIndex = 1;
                        colSpan++;
                    }

                    worksheet.Cell(rowIndex, cellIndex).Value = userForm.UserResponse.ElementAt(i).Value ?? "";

                    if (qsFormat.Orientation == Constants.Orientation_Vertical)
                        worksheet.Cell(rowIndex, cellIndex).Style.Alignment.SetTopToBottom(true);

                    worksheet.Cell(rowIndex, cellIndex).Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin);
                    worksheet.Range(worksheet.Cell(rowIndex, cellIndex), worksheet.Cell(rowIndex, cellIndex + colSpan)).Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin);
                    worksheet.Range(worksheet.Cell(rowIndex, cellIndex), worksheet.Cell(rowIndex, cellIndex + colSpan)).Merge();
                    cellIndex += qsFormat.ColSpan.Value;
                }
                rowIndex++;
            }


        }


    }
}
