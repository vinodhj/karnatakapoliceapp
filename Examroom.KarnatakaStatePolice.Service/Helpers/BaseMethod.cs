﻿using Examroom.KarnatakaStatePolice.DataAccess;
using Examroom.KarnatakaStatePolice.DataAccess.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examroom.KarnatakaStatePolice.Service.Helpers
{
    public static class BaseMethod
    {
        public static T SetDefaultValues<T>(this T entity, bool isAdd, int userId) where T : IDatabaseModel
        {
            if (entity != null)
            {
                if (isAdd)
                {
                    entity.CreatedBy = userId;
                    entity.CreatedDatetime = DateTime.Now;
                }
                entity.ModifiedBy = userId;
                entity.ModifiedDatetime = DateTime.Now;

            }
            return entity;
        }
        private static Random random = new Random();

        public static string GetOtp()
        {
            const string chars = "0123456789";
            return new string(Enumerable.Repeat(chars, 6)
                        .Select(s => s[random.Next(s.Length)]).ToArray());

        }

        /// <summary>
        /// The SetDivisionDefaultValues.
        /// </summary>
        /// <param name="division">The division<see cref="DepartmentMapping"/>.</param>
        /// <param name="isAdd">The isAdd<see cref="bool"/>.</param>
        /// <param name="userId">The userId<see cref="int"/>.</param>
        public static void SetDivisionDefaultValues(this DepartmentMapping division, bool isAdd, int userId)
        {
            if (division != null)
            {
                division.Name = Constants.Department;
                division.LevelId = 1;
                division.IsActive = true;
                division.ModifiedBy = userId;
                division.ModifiedDatetime = DateTime.Now;

                if (isAdd)
                {
                    division.CreatedBy = userId;
                    division.CreatedDatetime = DateTime.Now;
                }
            }
        }

        /// <summary>
        /// The ActiveDivisionValueExists.
        /// </summary>
        /// <param name="departmentMappings">The departmentMappings<see cref="DbSet{DepartmentMapping}"/>.</param>
        /// <param name="value">The value<see cref="string"/>.</param>
        /// <param name="divisionId">The divisionId<see cref="int?"/>.</param>
        /// <returns>The <see cref="Task{bool}"/>.</returns>
        public static async Task<bool> ActiveDivisionValueExists(this DbSet<DepartmentMapping> departmentMappings, string value, int? divisionId = null)
        {
            if (divisionId is null)
                return await departmentMappings.AnyAsync(departmentMapping =>
                                departmentMapping.Value == value && departmentMapping.IsActive);

            return await departmentMappings.AnyAsync(departmentMapping =>
                            departmentMapping.Value == value &&
                            departmentMapping.IsActive &&
                            departmentMapping.Id != divisionId);
        }

        /// <summary>
        /// The UserExists.
        /// </summary>
        /// <param name="users">The users<see cref="DbSet{User}"/>.</param>
        /// <param name="userId">The value<see cref="int?"/>.</param>
        /// <returns>The <see cref="Task{bool}"/>.</returns>
        public static async Task<bool> UserExists(this DbSet<User> users, int? userId = null)
        {
            return await users.AnyAsync(user => user.Id == userId && (user.IsActive ?? false) && !user.IsDeleted);
        }
    }
}
