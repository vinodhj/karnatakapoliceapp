﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace Examroom.KarnatakaStatePolice.Service.Helpers
{
    public class KspException : Exception
    {
        public HttpStatusCode StatusCode { get; set; }

        public KspException(string message, HttpStatusCode statusCode = HttpStatusCode.InternalServerError) : base(message)
        {
            StatusCode = statusCode;
        }
    }
}
