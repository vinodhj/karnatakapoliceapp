﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Examroom.KarnatakaStatePolice.Service.FormBuilder
{
    public class FormTemplate
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public List<FormSection> Sections { get; set; }
    }

    public class FormSection
    {
        public string Id  { get; set; }
        public string Name { get; set; }
        public List<FormGroup> QuestionGroups { get; set; }
        public QuestionGroupMetaData QuestionGroupsMetaData { get; set; }
    }

    public class QuestionGroupMetaData
    {
        [JsonProperty("parsingMetaData")]
        public List<ParsingMetaData> ParsingMetaData { get; set; }
    }

    public class ParsingMetaData
    {
        [JsonProperty("feildID")]
        public string Id { get; set; }

        [JsonProperty("orientation")]
        public string Orientation { get; set; } = "H";

        [JsonProperty("mergeToNextRow")]
        public bool? MergeToNextRow { get; set; } = false;

        [JsonProperty("colspan")]
        public int? ColSpan { get; set; } = 1;

    }

    public class FormQuestionGroup
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Label { get; set; }
    }

    public class FormGroup
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public List<FormQuestionGroup> Group { get; set; }
    }
    public class UserResponseParseDto 
    {
        public List<UserResponseContentDto> UserResponse { get; set; }
    }

    public class UserResponseContentDto
    {
        public UserResponseContentDto() { }
        public UserResponseContentDto(string id, string name)
        {
            Id = id;
            Name = name;
        }
        public UserResponseContentDto(string id, string name, string value)
        {
            Id = id;
            Name = name;
            Value = value;
        }
        public string Id { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
    }

    public class SavedResponseFormatDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string  Value { get; set; }
    }

}
