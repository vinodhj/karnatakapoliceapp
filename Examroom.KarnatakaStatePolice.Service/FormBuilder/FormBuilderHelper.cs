﻿using Examroom.KarnatakaStatePolice.Service.Helpers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Examroom.KarnatakaStatePolice.Service.FormBuilder
{
    public static class FormBuilderHelper
    {
        public static List<UserResponseContentDto> Add(this List<UserResponseContentDto> columns, string id, string name, string value)
        {
            if (columns.Any(x => x.Name == id))
                return columns;

            columns.Add(new UserResponseContentDto(id, name, value));

            return columns;
        }

        public static FormTemplate GetTemplate(string strTemplate)
        {
            var template = JsonConvert.DeserializeObject<FormTemplate>(strTemplate);

            if (template == null)
                throw new KspException("Invalid form");

            return template;
        }
        public static List<FormQuestionGroup> GetFormQuestions(string strTemplate)
        {
            var template = JsonConvert.DeserializeObject<FormTemplate>(strTemplate);

            var qs = template
                        .Sections
                        .SelectMany(x => x.QuestionGroups)
                        .SelectMany(x => x.Group)
                        .Select(x => x)
                        .ToList();

            return qs;
        }

        public static List<FormQuestionGroup> GetFormQuestionGroups(this FormTemplate template)
        {
            var qs = template
                         .Sections
                         .SelectMany(x => x.QuestionGroups)
                         .SelectMany(x => x.Group)
                         .Select(x => x)
                         .ToList();

            return qs;
        }

        public static List<ParsingMetaData> GetFormQuestionsFormat(this FormTemplate template)
        {
            var qsFormat = template
                         .Sections
                         .SelectMany(x=> x.QuestionGroupsMetaData.ParsingMetaData)
                         .ToList();

            return qsFormat;
        }

        private static Dictionary<string,string> GetUserResponse(string strUserResponse)
        {
            var userResponseContent = JsonConvert.DeserializeObject<List<Dictionary<string, Dictionary<string, string>>>>(strUserResponse);
            var dictUserResponse = userResponseContent.Select(x => x.Values).SelectMany(x => x).SelectMany(x => x).ToDictionary(x => x.Key, x => x.Value);
            return dictUserResponse;
        }

        public static string ParseUserResponse(string strTemplate, string strUserResponse)
        {
            var questions = GetFormQuestions(strTemplate);
            return ParseUserResponse(questions, strUserResponse);
        }

        public static string ParseUserResponse(List<FormQuestionGroup> questions, string strUserResponse)
        {
            var userResponses = GetUserResponse(strUserResponse);

            var outputColumns = new List<UserResponseContentDto>();

            foreach (var question in questions)
            {
                outputColumns.Add(question.Id, question.Name, userResponses[question.Id]);
            }

            var parsedAnswers = JsonConvert.SerializeObject(outputColumns);
            return parsedAnswers;
        }

        public static List<UserResponseContentDto> GetUserResponseDetail(List<FormQuestionGroup> questions, string strUserResponse)
        {
            var userResponses = GetUserResponse(strUserResponse);
            var outputColumns = new List<UserResponseContentDto>();

            foreach (var question in questions)
            {
                outputColumns.Add(question.Id, question.Label, userResponses[question.Id]);
            }

            return outputColumns;
        }
    }
}
