import { AfterViewInit, Component, Inject, OnInit } from '@angular/core';;
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { HttpService } from 'src/@exai/services/http.service';
import { DataFilterService } from 'src/@exai/services/data-filter.service';
import { MatSelectChange } from '@angular/material/select';

@Component({
  selector: 'exai-pop-up',
  templateUrl: './pop-up.component.html',
  styleUrls: ['./pop-up.component.scss']
})
export class PopUpComponent implements OnInit, AfterViewInit {
  options: Array<Array<any>> = [];
  constructor(public dialogRef: MatDialogRef<PopUpComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public httpService: HttpService,
    private dataFilter: DataFilterService,
    public formBuilder: FormBuilder
  ) {
    console.log(data);
  }

  ngOnInit(): void {

    for (let i = 0; i < this.data.labels.length; i++) {
      this.options.push([]);
      // if(this.data.labels[i].initialValue){
      //   (this.data.controls.controls[i] as FormControl).patchValue(this.data.labels[i].initialValue)
      // }
    }
    let indexOfLevel1 = (this.data.labels as Array<any>).indexOf(this.data.labels.filter((x: any) => { return x.level === 1 })[0]);

    this.httpService.getDepartmentsByLevel(1).subscribe((response: any) => {
      this.options[indexOfLevel1] = response;

    });
  }
  ngAfterViewInit() {
    for (let i = 0; i < this.data.labels.length; i++) {
      if (this.data.labels[i].initialValue) {
        if (!(this.options[i].includes(this.data.labels[i].initialValue)))
          this.options[i].push(this.data.labels[i].initialValue);
        (this.data.controls.controls[i] as FormControl).setValue(this.data.labels[i].initialValue);
      }
    }
    console.log(this.options);
  }

  getOptionsForChild(event: MatSelectChange, dependentIndex: number) {
    console.log(event.value, dependentIndex);
    if (dependentIndex) {
      this.httpService.getDepartmentsByLevel(this.data.labels[dependentIndex].level,
        typeof (event.value) === "object" ? event.value.id : event.value.map((x: any) => { 
          return x.id }).join(',')).subscribe((response: any) => {
          this.options[dependentIndex] = response;
        })
    }
  }

  onSubmitForm() {
    if (this.data.controls.valid) {
      this.dialogRef.close({
        value: this.data.controls.value
      });
    }
    else {
      console.log('Invalid Form', this.data.controls.value);
    }
  }
  onCancel() {
    this.dialogRef.close();
  }

}
