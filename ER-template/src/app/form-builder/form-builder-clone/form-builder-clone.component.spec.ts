import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormBuilderCloneComponent } from './form-builder-clone.component';

describe('FormBuilderCloneComponent', () => {
  let component: FormBuilderCloneComponent;
  let fixture: ComponentFixture<FormBuilderCloneComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormBuilderCloneComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormBuilderCloneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
