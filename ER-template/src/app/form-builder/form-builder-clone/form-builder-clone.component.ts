import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { concatMap, finalize, map } from 'rxjs/operators';
import { HttpService } from 'src/@exai/services/http.service';
import { LanguageService } from 'src/app/language.service';

@Component({
  selector: 'exai-form-builder-clone',
  templateUrl: './form-builder-clone.component.html',
  styleUrls: ['./form-builder-clone.component.scss']
})
export class FormBuilderCloneComponent implements OnInit {
  formGroup: FormGroup
  loadingDataForDropdown: boolean = true;
  prebuiltTemplates$;
  
  isLoading: boolean = false;

  constructor(private snackbar: MatSnackBar,private route: ActivatedRoute, public dialogRef: MatDialogRef<FormBuilderCloneComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private fb: FormBuilder, public languageService: LanguageService, private httpService: HttpService) { }

  ngOnInit(): void {

    this.formGroup = this.fb.group({
      templateId: [null, Validators.required],
    })

    this.prebuiltTemplates$ = this.httpService.getBasicForm().pipe(
      finalize(() => this.loadingDataForDropdown = false)
    ).pipe(
      map((response: any[]) => response.filter(template => template.templateType === this.data.cloneType && template.id != this.data.formId))
    )

  }

  onCancel() {
    this.dialogRef.close();
  }

  onSubmitForm() {

    this.formGroup.markAllAsTouched();

    if(!this.formGroup.valid)
      return;

    this.isLoading = true;

    this.httpService.templateGetContent(this.formGroup.get('templateId').value).pipe(
      concatMap(content => this.templateUpdateContent(this.data.formId, content)),
      finalize(() => this.isLoading = false)
    ).subscribe(res => {
      
      this.snackbar.open('Form Cloned Successfully.', 'Ok', {
        duration: 2500
      })

      this.dialogRef.close(true);
      
    }, err => {

      this.snackbar.open(err || 'Failed to Clone the form', 'Ok', {
        duration: 2500
      })

    })
  }

  templateUpdateContent(formId: number, content): Observable<any> {

    if(content)
      return this.httpService.templateUpdateContent(formId, JSON.stringify(content));
    else {
      return throwError('Chosen form template is empty.')
    }
  }

}
