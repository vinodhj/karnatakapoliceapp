import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { forkJoin, Observable, of } from 'rxjs';
import { catchError, concatMap, finalize, switchMap, tap } from 'rxjs/operators';
import { HttpService } from 'src/@exai/services/http.service';
import { Role } from '../shared/enums/role.enum';
import { FormBuilderCloneComponent } from './form-builder-clone/form-builder-clone.component';
import { FormBuilderContentComponent } from './form-builder-content/form-builder-content.component';
import { FormBuilderResultComponent } from './form-builder-result/form-builder-result.component';
import { FormBuilderDownloadService } from './services/form-builder-download.service';
import { FormBuilderElementsService } from './services/form-builder-elements.service';
import { FormBuilderIndexeddbService } from './services/form-builder-indexeddb.service';

@Component({
  selector: 'app-form-builder',
  templateUrl: './form-builder.component.html',
  styleUrls: ['./form-builder.component.scss'],
  providers: [FormBuilderElementsService]
})
export class FormBuilderComponent implements OnInit {
  @ViewChild('formBuilderContent') formBuilderContent!: FormBuilderContentComponent;
  @ViewChild('buttonPrint') buttonPrint;
  @ViewChild('tabGroup') tabGroup;

  // Custom
  predefinedPageBuilder = this.formBuilderElementsService.predefinedPageBuilder;
  // Custom

  pageBuilder = this.formBuilderElementsService.pageBuilder;

  isEdit = this.router.url.includes('edit');
  isFill = this.router.url.includes('fill-form');
  isPreview = this.router.url.includes('view-form');
  isCreate = this.router.url.includes('create');

  loadedData: boolean = false;

  templateId: number;

  isTimelineVisible = this.httpService.getRole() != Role.User;

  constructor(private dialog: MatDialog, private formBuilderIndexeddb: FormBuilderIndexeddbService, private formBuilderElementsService: FormBuilderElementsService, private httpService: HttpService, private route: ActivatedRoute, private router: Router, private formBuilderDownloadService: FormBuilderDownloadService, private snackbar: MatSnackBar) {
  }

  ngOnInit(): void {

    this.route.paramMap.pipe(
      switchMap(res => {
        this.templateId = +res.get('id');

        return this.httpService.templateGetContent(this.templateId).pipe(
          catchError(() => {
            //empty error if template is not found
            return of(null);
          }),
          concatMap((result) => {

            //if no data was found check db storage
            if (!result)
              return this.formBuilderIndexeddb.initializeDataIntoIndexedDb(
                this.templateId,
                this.formBuilderElementsService.pageBuilder);

            //data was found return result
            return of(result);

          }),
          finalize(() => this.loadedData = true)
        )
      }),
      switchMap(
        response => {
          if (response)
            this.formBuilderElementsService.pageBuilder = response;

          return this.httpService.templateGetApproveRejectContent(this.templateId);
        }
      )
    ).subscribe((content: any) => {

      if (content)
        this.formBuilderElementsService.predefinedPageBuilder = content;

    })
  }

  eventEmittedFromItems(event: any) {

    if (event.eventName === 'paste')
      this.formBuilderContent.pasteContent();

  }

  showSideMenu: boolean = true;
  disableClickOutside: boolean = false;

  savingForm: boolean = false;

  saveForm() {
    const formId = +this.route.snapshot.paramMap.get('id');

    this.savingForm = true;

    forkJoin([
      this.templateUpdateContent(formId),
      this.templateUpdateApproveRejectContent(formId)
    ]).pipe(
      finalize(() => this.savingForm = false),
    ).subscribe(
      response => {
        //remove data from db store of this document
        this.formBuilderIndexeddb.removeDataInsideIndexedDb(formId);

        if (response[0] && response[1])
          this.router.navigate(['forms', 'config-form']);

        if (this.isEdit)
          this.snackbar.open(`Form ${response[0].message} updated successfully.`, 'Ok', {
            duration: 3000
          })

        if (this.isCreate)
          this.snackbar.open(`Form ${response[0].message} created successfully.`, 'Ok', {
            duration: 3000
          })
      }
    )
  }

  templateUpdateContent(formId: number): Observable<any> {
    return this.httpService.templateUpdateContent(formId, JSON.stringify(this.formBuilderElementsService.pageBuilder));
  }

  templateUpdateApproveRejectContent(formId: number): Observable<any> {
    return this.httpService.templateUpdateApproveRejectContent(formId, JSON.stringify(this.formBuilderElementsService.predefinedPageBuilder));
  }

  generateDataForDownload(type) {
    this.formBuilderDownloadService.generateDataForDownload(this.formBuilderElementsService.pageBuilder, {}, type);
  }

  printDocument() {

    //on preview and on fill we have only one tab so the "View Form" tab will be positioned on 0 index
    const viewTabIndex: number = (this.isPreview || this.isFill) ? 0 : 1;

    //get current active tab
    const activeTabIndex: number = this.tabGroup.selectedIndex;

    if (viewTabIndex === activeTabIndex) {
      //print document
      this.buttonPrint._elementRef.nativeElement.click();

    } else {
      //don't allow user to print Form Builder tab
      this.snackbar.open('To print the document please select "View Form" tab first.', 'Ok', {
        duration: 2500
      })

    }

  }

  cancelForm() {

    const formId = +this.route.snapshot.paramMap.get('id');

    this.httpService.deleteForm(null, formId).subscribe(res => {

      this.router.navigateByUrl('forms/config-form');

    }, err => {

      this.snackbar.open('Cancelation Failed', 'Ok', {
        duration: 3000
      });

    })
  }

  cloneForm() {

    let dialogConfiguration: any = {
      width: '25vw',
      height: 'min-content',
      data: {
        formId : +this.route.snapshot.paramMap.get('id'),
        cloneType: 'Letter'
      },
      disableClose: true
    }

    const dialogRef = this.dialog.open(FormBuilderCloneComponent, dialogConfiguration);
  
    dialogRef.afterClosed().subscribe(res => {

      if(res) {

        this.loadedData = false;
  
        this.ngOnInit();

      }


    })
  }

  backForm() {
    if(this.isEdit)
      this.router.navigateByUrl('forms/config-form');
    else {
      this.router.navigateByUrl('forms/submitted-form');
    }
    
  }
}