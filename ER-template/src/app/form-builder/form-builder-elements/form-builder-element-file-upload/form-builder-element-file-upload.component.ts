import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ChangeDetectorRef, Component, ElementRef, HostListener, Input, OnInit, ViewChild } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { forkJoin, of } from 'rxjs';
import { catchError, concatMap, finalize, map } from 'rxjs/operators';
import { HttpService } from 'src/@exai/services/http.service';
import { getUniqueId } from 'src/app/utils/randomNumber';


export interface UploadFilesInputDto {
  entityId: number;
  entityType: string;
  path: string | null;
  formBuilderElementId: string;
  formBuilderElementType: string;
  fileAttachments: FileAttachmentInputDto[];
}
export interface FileAttachmentInputDto {
  file: File | null;
}
@Component({
  selector: 'app-form-builder-element-file-upload',
  templateUrl: './form-builder-element-file-upload.component.html',
  styleUrls: ['./form-builder-element-file-upload.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: FormBuilderElementFileUploadComponent,
      multi: true
    }
  ]
})
export class FormBuilderElementFileUploadComponent implements OnInit {
  @ViewChild('fileInput') fileInput!: any;
  @Input() item!: any;
  @Input() progress!: any;
  @Input() insideForm!: boolean;
  onChange!: Function;
  files = [];
  uploadingFiles = false;
  @Input() previewMode: boolean;
  // @Input() url: string;
  // @Input() removeUrl: string;
  @Input() label: string;
  @Input() name: string;
  @Input() accept: Array<string>;
  @Input() multiple: boolean;
  @Input() showFileList: boolean;
  @Input() autoUpload: boolean;

  get allowMultipleFiles() {
    return this.item.fileUpload.multiple;
  }

  uploadedFilesObject = [];

  @HostListener('change', ['$event.target.files']) emitFiles(event: FileList) {

    const files = event;
    this.validateFileFormat(files, this.item.fileUpload.accept);
    this.validateFileSize(files, this.item.fileUpload.maxSize, this.item.fileUpload.minSize);
    this.filesDropped(files);

  }

  validateFileFormat(files, allowedFilesFormat) {
    for (const file of files) {

      if(!allowedFilesFormat)
        return;

      const fileExtension = '.' + file.name.split('.')[1].toLowerCase();

      if(!allowedFilesFormat.includes(fileExtension)) {
        file.validationFailed = true;
      }

    }
  }

  validateFileSize(files, maxSize, minSize) {
    for (const file of files) {

      const documentSize = file.size / 1024 / 1024;

      if(maxSize) {
        maxSize < documentSize ? file.validationMaxSizeFailed = true : null;
      }

      if(minSize) {
        minSize > documentSize ? file.validationMinSizeFailed = true : null;
      }

    }
  }
  

  filesDropped(files) {
    // create preview for file

    for (const file of files) {

      if(file.validationFailed) {

        this.snackbar.open(`File with name ${ file.name } can't be uploaded because of file format. Formats that are allowed are ${this.item.fileUpload.accept}.`, 'Ok', {
          duration: 3500
        })

        this.clearFileInput();
        continue;
      }

      if(file.validationMaxSizeFailed) {

        this.snackbar.open(`File with name ${ file.name } can't be uploaded because file is too big in size. Maximum file size is ${this.item.fileUpload.maxSize}mb.`, 'Ok', {
          duration: 3500
        })

        this.clearFileInput();
        continue;
      }

      if(file.validationMinSizeFailed) {

        this.snackbar.open(`File with name ${ file.name } can't be uploaded because file is to low in size. Minimum file size is ${this.item.fileUpload.minSize}mb.`, 'Ok', {
          duration: 3500
        })

        this.clearFileInput();
        continue;
      }

      file.id = getUniqueId(1);

      if(!file.type.includes('image/')) {

        this.fileIsNotImage(file);

      } else {

        const reader = new FileReader();
        reader.onload = () => {
          
          if(!this.allowMultipleFiles)
            this.files = [];  
          
            this.files.push({ id: file.id, attachmentId: file.attachmentId, name: file.name, imageContent: reader.result });
          
          this.onChange(this.files);
        }
        reader.readAsDataURL(file);

      }

    }
  }

  fileIsNotImage(file) {

    let icon = 'description';

    file.type.includes('pdf') ? (icon = 'picture_as_pdf') : null;

    const formData = new FormData();

    formData.append('files', file);

    file.url = null;
    file.loading = true;

    if(!this.allowMultipleFiles)
      this.files = [];  

    this.files.push({ id: file.id, name: file.name, noPreview: true, url: file.url, icon, loading: file.loading });

    this.httpService.tableFilesUpload(formData).subscribe(res => {

      this.files.map(f => {

        if(f.id === file.id)
          f.url = res[0].url;
          f.loading = false;
      })

      this.onChange(this.files);

    })

  }

  constructor(private host: ElementRef<HTMLInputElement>, private httpService: HttpService,private http: HttpClient, public snackbar: MatSnackBar, private cdr: ChangeDetectorRef) {
  }
  
  ngOnInit(): void {
  }

  writeValue(value: null) {
    this.uploadedFilesObject = value || [];
    // clear file input
    this.host.nativeElement.value = '';
    this.files = value || [];
  }

  registerOnChange(fn: Function) {
    this.onChange = fn;
  }
  registerOnTouched(fn: Function) {
  }

  disabled: boolean = false;

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  removeFile(file) {
    
    this.files = this.files.filter(f => f.id !== file.id);
    
    this.onChange(this.files);
    
    this.clearFileInput();
    
  }

  clearFileInput() {
    // clear file input
    this.fileInput.nativeElement.value = '';
  }

  downloadFileUrl(url, name) {

    const headers = new HttpHeaders();

    this.http.get(url, { headers, responseType: 'blob' as 'json' }).subscribe(
      (response: any) => {
        let dataType = response.type;
        let binaryData = [];
        binaryData.push(response);
        let downloadLink = document.createElement('a');
        downloadLink.href = window.URL.createObjectURL(new Blob(binaryData, { type: dataType }));

        downloadLink.setAttribute('download', name);
        document.body.appendChild(downloadLink);
        downloadLink.click();
      }
    )

  }
}