import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormBuilderElementFileUploadComponent } from './form-builder-element-file-upload.component';

describe('FormBuilderElementFileUploadComponent', () => {
  let component: FormBuilderElementFileUploadComponent;
  let fixture: ComponentFixture<FormBuilderElementFileUploadComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormBuilderElementFileUploadComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormBuilderElementFileUploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
