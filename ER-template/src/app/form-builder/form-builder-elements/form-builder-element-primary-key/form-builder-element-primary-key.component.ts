import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BehaviorSubject, Subject } from 'rxjs';
import { finalize, map } from 'rxjs/operators';
import { HttpService } from 'src/@exai/services/http.service';

@Component({
  selector: 'exai-form-builder-element-primary-key',
  templateUrl: './form-builder-element-primary-key.component.html',
  styleUrls: ['./form-builder-element-primary-key.component.scss']
})
export class FormBuilderElementPrimaryKeyComponent implements OnInit {
  @Input() item;
  @Input() view: boolean;
  @Output() removeEmitted = new EventEmitter();
  constructor(private httpService: HttpService, private route: ActivatedRoute) { }

  isLoading = true;

  primaryKeyContent: any;

  ngOnInit(): void {

    this.httpService.getPrimaryKeysBySubDivision(this.route.snapshot.paramMap.get('id')).pipe(
      map((primaryKeys: any) => {
        return primaryKeys.find(k => k.id === this.item.primaryKeyContent.id);
      }),
      finalize(() => this.isLoading = false)
    ).subscribe(response => {

      this.primaryKeyContent = response;

    });

  }

}
