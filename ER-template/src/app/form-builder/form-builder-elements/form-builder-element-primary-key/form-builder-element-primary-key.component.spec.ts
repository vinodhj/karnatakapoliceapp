import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormBuilderElementPrimaryKeyComponent } from './form-builder-element-primary-key.component';

describe('FormBuilderElementPrimaryKeyComponent', () => {
  let component: FormBuilderElementPrimaryKeyComponent;
  let fixture: ComponentFixture<FormBuilderElementPrimaryKeyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormBuilderElementPrimaryKeyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormBuilderElementPrimaryKeyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
