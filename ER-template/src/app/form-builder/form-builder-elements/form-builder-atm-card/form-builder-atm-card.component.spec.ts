import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormBuilderAtmCardComponent } from './form-builder-atm-card.component';

describe('FormBuilderAtmCardComponent', () => {
  let component: FormBuilderAtmCardComponent;
  let fixture: ComponentFixture<FormBuilderAtmCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormBuilderAtmCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormBuilderAtmCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
