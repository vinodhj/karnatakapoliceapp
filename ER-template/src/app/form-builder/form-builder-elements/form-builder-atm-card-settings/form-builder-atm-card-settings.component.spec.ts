import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormBuilderAtmCardSettingsComponent } from './form-builder-atm-card-settings.component';

describe('FormBuilderAtmCardSettingsComponent', () => {
  let component: FormBuilderAtmCardSettingsComponent;
  let fixture: ComponentFixture<FormBuilderAtmCardSettingsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormBuilderAtmCardSettingsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormBuilderAtmCardSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
