import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormBuilderElementSettingsComponent } from './form-builder-element-settings.component';

describe('FormBuilderElementSettingsComponent', () => {
  let component: FormBuilderElementSettingsComponent;
  let fixture: ComponentFixture<FormBuilderElementSettingsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormBuilderElementSettingsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormBuilderElementSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
