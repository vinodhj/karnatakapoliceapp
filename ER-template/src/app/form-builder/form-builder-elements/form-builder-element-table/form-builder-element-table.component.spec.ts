import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormBuilderElementTableComponent } from './form-builder-element-table.component';

describe('FormBuilderElementTableComponent', () => {
  let component: FormBuilderElementTableComponent;
  let fixture: ComponentFixture<FormBuilderElementTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormBuilderElementTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormBuilderElementTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
