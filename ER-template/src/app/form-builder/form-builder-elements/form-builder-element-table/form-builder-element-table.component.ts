import { DOCUMENT } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { AfterViewInit, ChangeDetectorRef, Component, ElementRef, EventEmitter, Inject, Input, OnInit, Output, ViewChild } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { HtmlEditorService, ImageService, LinkService, QuickToolbarEventArgs, QuickToolbarSettingsModel, RichTextEditorComponent, TableService, ToolbarService } from '@syncfusion/ej2-angular-richtexteditor';
import { isThisQuarter } from 'date-fns';
import { mergeMap, switchMap } from 'rxjs/operators';
import * as uuid from 'uuid';

@Component({
  selector: 'exai-form-builder-element-table',
  templateUrl: './form-builder-element-table.component.html',
  styleUrls: ['./form-builder-element-table.component.scss'],
  providers: [ToolbarService, LinkService, ImageService, HtmlEditorService, TableService,
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: FormBuilderElementTableComponent,
      multi: true
    }
  ],
})
export class FormBuilderElementTableComponent implements OnInit, ControlValueAccessor {
  @ViewChild('textEditorRef') public rteObj: RichTextEditorComponent;

  public tools: object = {
    items: ['CreateTable', 'Image'],
    enabled: false,
  };

  public tableSettings = {
    resize: true,
  }

  imageSettings = { 
    saveFormat: "Base64" 
  } 

  public quickToolbarSettings: QuickToolbarSettingsModel = {
    table:
      ["TableHeader", "TableRows", "TableColumns", "BackgroundColor", "-", "TableRemove", "Alignments", "TableCell", "TableCellVerticalAlign", "Styles"],
      showOnRightClick: true
    }

  @Input() item!: any;
  @Input() view: boolean = false;

  @Output('removeEmitted') removeEmitted = new EventEmitter();

  constructor(@Inject(DOCUMENT) private document: Document, private http: HttpClient, private cdr: ChangeDetectorRef) { }

  ngOnInit(): void {

    this.view ? this.quickToolbarSettings.enable = false : this.quickToolbarSettings.enable = true;
    this.view ? this.tableSettings.resize = false : this.tableSettings.resize = true;

  }

  onChange = (value: any) => { };

  onTouched = () => { };

  writeValue(obj: any): void {
    this.tableValueReactiveForm = obj;
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }
  
  tableValueChanged({ value }) {
    this.onChange(value);
  }

  tableValueReactiveForm = '';
}
