import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormBuilderElementTextareaComponent } from './form-builder-element-textarea.component';

describe('FormBuilderElementTextareaComponent', () => {
  let component: FormBuilderElementTextareaComponent;
  let fixture: ComponentFixture<FormBuilderElementTextareaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormBuilderElementTextareaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormBuilderElementTextareaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
