import { DOCUMENT } from '@angular/common';
import { ChangeDetectorRef, Component, EventEmitter, Inject, Input, OnInit, Output, ViewChild } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { HttpClient } from '@microsoft/signalr';
import { ToolbarService, LinkService, ImageService, HtmlEditorService, TableService, RichTextEditorComponent, QuickToolbarSettingsModel } from '@syncfusion/ej2-angular-richtexteditor';

@Component({
  selector: 'exai-form-builder-element-textarea',
  templateUrl: './form-builder-element-textarea.component.html',
  styleUrls: ['./form-builder-element-textarea.component.scss'],
  providers: [ToolbarService, LinkService, ImageService, HtmlEditorService, TableService,
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: FormBuilderElementTextareaComponent,
      multi: true
    }
  ],
})
export class FormBuilderElementTextareaComponent implements OnInit, ControlValueAccessor {

  @ViewChild('textEditorRef') public rteObj: RichTextEditorComponent;

  public tools: object = {
    items: [],
    enabled: false,
  };

  public tableSettings = {
    resize: true,
  }

  imageSettings = { 
    saveFormat: "Base64" 
  } 

  public quickToolbarSettings: QuickToolbarSettingsModel = {
    table:
      ["TableHeader", "TableRows", "TableColumns", "BackgroundColor", "-", "TableRemove", "Alignments", "TableCell", "TableCellVerticalAlign", "Styles"],
      showOnRightClick: true
    }

  @Input() item!: any;
  @Input() view: boolean = false;

  @Output('removeEmitted') removeEmitted = new EventEmitter();

  constructor(@Inject(DOCUMENT) private document: Document, private cdr: ChangeDetectorRef) { }

  ngOnInit(): void {

    this.view ? this.quickToolbarSettings.enable = false : this.quickToolbarSettings.enable = true;
    this.view ? this.tableSettings.resize = false : this.tableSettings.resize = true;

  }

  onChange = (value: any) => { };

  onTouched = () => { };

  writeValue(obj: any): void {
    this.tableValueReactiveForm = obj;
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }
  
  tableValueChanged({ value }) {
    this.onChange(value);
  }

  tableValueReactiveForm = '';
}
