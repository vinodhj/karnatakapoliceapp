import { ChangeDetectorRef, Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { RichTextEditorComponent } from '@syncfusion/ej2-angular-richtexteditor';
import { Editor, Toolbar } from 'ngx-editor';

@Component({
  selector: 'app-form-builder-element-html',
  templateUrl: './form-builder-element-html.component.html',
  styleUrls: ['./form-builder-element-html.component.scss']
})
export class FormBuilderElementHtmlComponent implements OnInit, OnDestroy {
  @Input() item!: any;
  @Output('removeEmitted') removeEmitted = new EventEmitter();

  @ViewChild('textEditorRef')
  public rteObj: RichTextEditorComponent;

  fontFamily = {
    default:"Arial Unicode MS", // to define default font-family
    items:[
      {text: "Arial Unicode MS", value: "Arial Unicode MS", class: "e-arial-unicode-ms",  command: "Font", subCommand: "FontName"},
      {text: "Noto Sans", value: "Noto Sans",  command: "Font", subCommand: "FontName"},
      {text: "Impact", value: "Impact,Charcoal,sans-serif", class: "e-impact", command: "Font", subCommand: "FontName"},
      {text: "Tahoma", value: "Tahoma,Geneva,sans-serif", class: "e-tahoma", command: "Font", subCommand: "FontName"},
    ],
    width: '60px'
  };

  fontSize = {
    default: '12px',
    items: [
        { text: '8px', value: '8px' },
        { text: '10px', value: '10px' },
        { text: '12px', value: '12px' },
        { text: '14px', value: '14px' },
        { text: '42px', value: '42px' }
    ],
    width: '40px'
};

  public tools: any = {
    items: [
        'Bold', 'Italic', 'Underline', 'StrikeThrough', '|',
        'FontName', 'FontSize', 'FontColor', 'BackgroundColor', '|',
        'Alignments', '|', 'CreateLink',
        'Image'],
    enable: true,
    };

  imageSettings = { 
    saveFormat: "Base64" 
  } 

  constructor(private cdr: ChangeDetectorRef) { }

  ngOnInit(): void {
  }
  
  ngOnDestroy(): void {
  }

  toggleToolbar(value) {
    // this.rteObj.toolbarSettings.enable = value;
  }

  onFocusEditor(value) {
    const toolbar: any = this.rteObj.element.querySelector('.e-toolbar-wrapper');

    toolbar.style.visibility = value ? 'visible' : 'hidden';
  }
}
