import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormBuilderElementHtmlComponent } from './form-builder-element-html.component';

describe('FormBuilderElementHtmlComponent', () => {
  let component: FormBuilderElementHtmlComponent;
  let fixture: ComponentFixture<FormBuilderElementHtmlComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormBuilderElementHtmlComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormBuilderElementHtmlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
