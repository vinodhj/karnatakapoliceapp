import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { FormBuilderImageSettingsComponent } from '../../form-builder-image-settings/form-builder-image-settings.component';

@Component({
  selector: 'exai-form-builder-image',
  templateUrl: './form-builder-image.component.html',
  styleUrls: ['./form-builder-image.component.scss']
})
export class FormBuilderImageComponent implements OnInit {
  @Input() item;
  @Output() removeEmitted = new EventEmitter();
  @Input() view;

  constructor(private dialog: MatDialog) { }

  ngOnInit(): void {
    this.initializeDialog();
  }

  initializeDialog() {
    if(!this.item.openOnDrop) return;

    this.openDialog();

    this.item.openOnDrop = false;
  }

  openDialog() {

    if(this.view) return;

    const dialogRef = this.dialog.open(FormBuilderImageSettingsComponent, {
      data: this.item,
      disableClose: true,
      maxHeight: '75vh'
    });

    dialogRef.afterClosed().subscribe(uploadedImage => {

      console.log(uploadedImage);

      if(!uploadedImage) return;

      this.item.imageData = {
        imageContent: uploadedImage.imageContent,
        width: uploadedImage.width,
        height: uploadedImage.height,
        originalHeight: uploadedImage.originalHeight,
        originalWidth: uploadedImage.originalWidth,
      }

    })
  }
}
