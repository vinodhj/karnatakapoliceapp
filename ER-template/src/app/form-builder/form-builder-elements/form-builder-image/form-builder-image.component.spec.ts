import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormBuilderImageComponent } from './form-builder-image.component';

describe('FormBuilderImageComponent', () => {
  let component: FormBuilderImageComponent;
  let fixture: ComponentFixture<FormBuilderImageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormBuilderImageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormBuilderImageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
