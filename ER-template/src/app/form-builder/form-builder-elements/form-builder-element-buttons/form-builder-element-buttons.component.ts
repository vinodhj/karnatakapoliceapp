import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-form-builder-element-buttons',
  templateUrl: './form-builder-element-buttons.component.html',
  styleUrls: ['./form-builder-element-buttons.component.scss']
})
export class FormBuilderElementButtonsComponent implements OnInit {
  @Input() item!: any;

  @Output('removeEmitted') removeEmitted = new EventEmitter(); 

  constructor() { }

  ngOnInit(): void {
  }

}
