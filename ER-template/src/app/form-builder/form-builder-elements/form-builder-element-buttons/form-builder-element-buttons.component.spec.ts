import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormBuilderElementButtonsComponent } from './form-builder-element-buttons.component';

describe('FormBuilderElementButtonsComponent', () => {
  let component: FormBuilderElementButtonsComponent;
  let fixture: ComponentFixture<FormBuilderElementButtonsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormBuilderElementButtonsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormBuilderElementButtonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
