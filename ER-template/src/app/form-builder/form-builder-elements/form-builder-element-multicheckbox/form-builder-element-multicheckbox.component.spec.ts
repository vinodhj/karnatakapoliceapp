import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormBuilderElementMulticheckboxComponent } from './form-builder-element-multicheckbox.component';

describe('FormBuilderElementMulticheckboxComponent', () => {
  let component: FormBuilderElementMulticheckboxComponent;
  let fixture: ComponentFixture<FormBuilderElementMulticheckboxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormBuilderElementMulticheckboxComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormBuilderElementMulticheckboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
