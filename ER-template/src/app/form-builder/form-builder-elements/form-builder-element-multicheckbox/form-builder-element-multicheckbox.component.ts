import { ChangeDetectorRef, Component, EventEmitter, forwardRef, Input, OnInit, Output } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-form-builder-element-multicheckbox',
  templateUrl: './form-builder-element-multicheckbox.component.html',
  styleUrls: ['./form-builder-element-multicheckbox.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: FormBuilderElementMulticheckboxComponent,
      multi: true
    }
  ]
})
export class FormBuilderElementMulticheckboxComponent implements OnInit, ControlValueAccessor {
  @Input() item!: any;
  selectedValues = [];
  @Input() disabled: boolean = false;

  constructor(private cdr: ChangeDetectorRef) {
    
  }

  ngOnInit(): void {
    
  }
  
  onToggle() {
    const checkedOptions = this.item.values.filter((x: any) => x.checked);
    this.selectedValues = checkedOptions.map((x: any) => x);

    this.onChange(this.selectedValues);
  }

  onChange = (value: any) => { };

  onTouched = () => { };

  writeValue(obj: any): void {
    this.selectedValues = obj;

    //match default array with selected values
    if(obj)
    this.item.values.map(checkboxItem => {

      this.selectedValues.map(checkedItem => {

        if(checkboxItem.value === checkedItem.value)
          checkboxItem.checked = true;

      })

    })
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }
  
}
