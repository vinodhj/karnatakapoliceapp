import { DOCUMENT } from '@angular/common';
import { Component, Inject, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { popoverAnimation } from 'src/@exai/animations/popover.animation';
import { returnAllFormControls } from 'src/app/utils/randomNumber';

declare var objectScan;

@Component({
  selector: 'exai-form-builder-grid-settings',
  templateUrl: './form-builder-grid-settings.component.html',
  styleUrls: ['./form-builder-grid-settings.component.scss'],
  animations: [popoverAnimation]
})
export class FormBuilderGridSettingsComponent implements OnInit {

  @Input() item;

  constructor(@Inject(DOCUMENT) private document: Document) { }

  ngOnInit(): void {
    console.log('row on init')
  }

  makeAsGroup(event, tableGroup = false) {
    if (event.checked) {
      this.item.grouped = true;

      this.editFormElements(true);
    }

    if (!event.checked) {
      this.item.grouped = false;

      this.editFormElements(false);
    }
  }

  makeAsVerticalGroup(event) {
    if (event.checked) {
      this.item.verticalGrouped = true;
      this.editFormElementsVerticalGrouping(true);
    }

    if (!event.checked) {
      this.item.verticalGrouped = false;

      this.editFormElementsVerticalGrouping(false);
    }
  }

  editFormElementsVerticalGrouping(status) {
    const formElements = returnAllFormControls(this.item.children);

    this.item.disableDropzone = status;

    formElements.map((element) => {
      //get first row of element
      const row = this.document.querySelector(`[id="${element.id}"]`).closest('[type="row"]');
      const rowId = row.getAttribute('id');

      const rowElement = this.findNode(rowId, this.item);

      if (rowElement) {

        rowElement.children.map(c => {
          c.disableDropzone = status;
          c.disable = status;
          c.disableAction = status;
        })

        //this is important for document export...
        element.disable = status;
        element.disableDelete = status;
        element.verticalGrouped = status;
        element.verticalGroupName = status ? this.item.groupName : null;
      }

    })


    

  }

  editFormElements(status) {
    const formElements = returnAllFormControls(this.item.children);

    formElements.map((element) => {
      //get first row of element
      const row = this.document.querySelector(`[id="${element.id}"]`).closest('[type="row"]');
      const rowId = row.getAttribute('id');

      const rowElement = this.findNode(rowId, this.item);

      if (rowElement) {

        //dont disable actions on main row
        if (rowElement.id != this.item.id)
          rowElement.disableAction = status;

        //disable child columns
        rowElement.children.map(c => {
          c.disableDropzone = status;
          c.disable = status;
          c.disableAction = status;
        })


        //we are counting number of columns inside row
        //to determine how large is our field
        const numberOfColumns = rowElement.children.filter(c => c.type == 'col').length;

        //this is important for document export...
        element.colsNumber = status ? numberOfColumns : null;
        element.disable = status;
        element.disableDelete = status;
        element.grouped = status;
        element.groupName = status ? this.item.groupName : null;
      }

    })


  }

  findNode = (id, input) => objectScan(['**'], {
    abort: true,
    rtn: 'parent',
    filterFn: ({ value }) => value === id
  })(input);


}
