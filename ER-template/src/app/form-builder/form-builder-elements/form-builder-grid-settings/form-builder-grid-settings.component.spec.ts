import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormBuilderGridSettingsComponent } from './form-builder-grid-settings.component';

describe('FormBuilderGridSettingsComponent', () => {
  let component: FormBuilderGridSettingsComponent;
  let fixture: ComponentFixture<FormBuilderGridSettingsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormBuilderGridSettingsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormBuilderGridSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
