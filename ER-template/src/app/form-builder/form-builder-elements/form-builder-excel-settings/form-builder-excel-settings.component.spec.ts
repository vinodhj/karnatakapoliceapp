import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormBuilderExcelSettingsComponent } from './form-builder-excel-settings.component';

describe('FormBuilderExcelSettingsComponent', () => {
  let component: FormBuilderExcelSettingsComponent;
  let fixture: ComponentFixture<FormBuilderExcelSettingsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormBuilderExcelSettingsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormBuilderExcelSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
