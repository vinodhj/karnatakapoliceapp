import { Component, Input, OnInit } from '@angular/core';
import { popoverAnimation } from 'src/@exai/animations/popover.animation';

@Component({
  selector: 'exai-form-builder-excel-settings',
  templateUrl: './form-builder-excel-settings.component.html',
  styleUrls: ['./form-builder-excel-settings.component.scss'],
  animations: [popoverAnimation]
})
export class FormBuilderExcelSettingsComponent implements OnInit {
  @Input() item;

  constructor() { }

  ngOnInit(): void {
  }

}
