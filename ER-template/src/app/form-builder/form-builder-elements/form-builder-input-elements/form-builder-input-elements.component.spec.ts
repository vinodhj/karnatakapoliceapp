import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormBuilderInputElementsComponent } from './form-builder-input-elements.component';

describe('FormBuilderInputElementsComponent', () => {
  let component: FormBuilderInputElementsComponent;
  let fixture: ComponentFixture<FormBuilderInputElementsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormBuilderInputElementsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormBuilderInputElementsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
