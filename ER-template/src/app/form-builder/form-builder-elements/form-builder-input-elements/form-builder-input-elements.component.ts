import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-form-builder-input-elements',
  templateUrl: './form-builder-input-elements.component.html',
  styleUrls: ['./form-builder-input-elements.component.scss']
})
export class FormBuilderInputElementsComponent implements OnInit, OnDestroy {
  @Input() item!: any;
  formElements: any = [];
  @Output('removeEmitted') removeEmitted = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  toggleValue(data: any) {

  }

  ngOnDestroy(): void {
  }
}
