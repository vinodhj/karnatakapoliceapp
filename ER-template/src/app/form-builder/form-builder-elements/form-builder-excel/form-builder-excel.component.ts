import { AfterViewInit, Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BeforeSaveEventArgs, getRangeAddress, SpreadsheetComponent } from '@syncfusion/ej2-angular-spreadsheet';
import { finalize, switchMap } from 'rxjs/operators';
import { HttpService } from 'src/@exai/services/http.service';
import { getUniqueId } from 'src/app/utils/randomNumber';
import { environment } from 'src/environments/environment';
import * as uuid from 'uuid';
import { ContentType, SaveSettings, SaveType, VersionType } from './excel-model';

@Component({
  selector: 'exai-form-builder-excel',
  templateUrl: './form-builder-excel.component.html',
  styleUrls: ['./form-builder-excel.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: FormBuilderExcelComponent,
      multi: true
    }
  ]
})
export class FormBuilderExcelComponent implements OnInit, AfterViewInit, ControlValueAccessor {
  @Input() item;
  @Output('removeEmitted') removeEmitted = new EventEmitter();
  @Input() view;

  isSaved: boolean = false;

  savedDate;

  isLoading: boolean = false;

  openUrl = `${environment.backendUrl}files/open-syncfusion-excel-file`;

  excelData

  @ViewChild('default')
  public spreadsheetObj: SpreadsheetComponent;

  constructor(private httpService: HttpService, private snackbar: MatSnackBar) { }

  onChange = (value: any) => { };

  onTouched = () => { };

  writeValue(obj: any): void {
    setTimeout(() => {
      //to set default values

      console.log(obj);

      if (obj)
        this.spreadsheetObj.openFromJson({ file: obj.jsonObject });
    })
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  ngOnInit(): void {

  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      //to set default values
      if (this.item.tableValue.jsonObject) {
        this.spreadsheetObj.openFromJson({ file: this.item.tableValue.jsonObject });
      }
    })

  }

  async saveExcelTable() {


  this.spreadsheetObj.selectRange(getRangeAddress([0, 0]));

    this.item.tableValue = await this.spreadsheetObj.saveAsJson();

    this.isLoading = true;

    var formData: any = new FormData();
    formData.append(`FileName`, `${this.item.label}-${getUniqueId(2)}.xlsx`);
    formData.append(`JSONData`, JSON.stringify(this.item.tableValue?.jsonObject?.Workbook));
    formData.append(`SaveUrl`, `${environment.backendUrl}files/save-syncfusion-excel-file`);
    formData.append(`FileContentType`, ContentType.Xlsx);
    formData.append(`VersionType`, VersionType.Xlsx);
    formData.append(`SaveType`, SaveType.Xlsx);
    formData.append(`PdfLayoutSettings`, `{\"fitSheetOnOnePage\":false}`);

    console.log(this.item.tableValue.jsonObject);

    this.httpService.saveSpreadsheet(formData).pipe(finalize(() => { 
      
      this.isLoading = false 
    
      this.snackbar.open('Excel table is being saved', "Ok", {
        duration: 2500
      })
      
    })).subscribe(
      response => { 
        this.item.tableValue.excelUrl = response;
        this.isSaved = true;
        this.savedDate = new Date();
      }
    );

  }

  async saveEnteredData() {

    this.spreadsheetObj.selectRange(getRangeAddress([0, 0]));

    this.isLoading = true;

    const fileObject: any = await this.spreadsheetObj.saveAsJson();

    var formData: any = new FormData();
    formData.append(`FileName`, `${this.item.label}-${getUniqueId(2)}.xlsx`);
    formData.append(`JSONData`, JSON.stringify(fileObject.jsonObject?.Workbook));
    formData.append(`SaveUrl`, `${environment.backendUrl}files/save-syncfusion-excel-file`);
    formData.append(`FileContentType`, ContentType.Xlsx);
    formData.append(`VersionType`, VersionType.Xlsx);
    formData.append(`SaveType`, SaveType.Xlsx);
    formData.append(`PdfLayoutSettings`, `{\"fitSheetOnOnePage\":false}`);

    this.httpService.saveSpreadsheet(formData).pipe(finalize(() => { 
      
      this.isLoading = false 
    
      this.snackbar.open('Excel table is being saved', "Ok", {
        duration: 2500
      })
      
    })).subscribe(
      response => {
        fileObject.excelUrl = response
        this.onChange(fileObject);

        this.isSaved = true;
        this.savedDate = new Date();
      }
    );

  }

}
