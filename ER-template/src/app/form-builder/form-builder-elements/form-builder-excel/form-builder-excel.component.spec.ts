import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormBuilderExcelComponent } from './form-builder-excel.component';

describe('FormBuilderExcelComponent', () => {
  let component: FormBuilderExcelComponent;
  let fixture: ComponentFixture<FormBuilderExcelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormBuilderExcelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormBuilderExcelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
