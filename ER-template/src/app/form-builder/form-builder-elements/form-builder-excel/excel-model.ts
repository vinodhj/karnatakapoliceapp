export interface SaveSettings {
    saveUrl: string;
    JSONData: string;
    saveType: SaveType;
    fileName: string;
    fileContentType: ContentType;
    versionType: VersionType;
    pdfLayoutSettings: string;
  }
  export enum SaveType {
    Xlsx,
    Xls,
    Csv,
    Pdf
  }
  export enum ContentType {
    Excel97,
    Excel2000,
    Excel2007,
    Excel2010,
    Excel2013,
    Excel2016,
    Xlsx,
    CSV,
    PDF
  }
  export enum VersionType {
    Excel97to2003,
    Excel2007,
    Excel2010,
    Excel2013,
    Excel2016,
    Xlsx
  }