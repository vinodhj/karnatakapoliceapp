import { DOCUMENT } from '@angular/common';
import { Component, Inject, Input, OnInit } from '@angular/core';
import { popoverAnimation } from 'src/@exai/animations/popover.animation';

@Component({
  selector: 'exai-form-builder-table-settings',
  templateUrl: './form-builder-table-settings.component.html',
  styleUrls: ['./form-builder-table-settings.component.scss'],
  animations: [popoverAnimation]
})
export class FormBuilderTableSettingsComponent implements OnInit {

  @Input() item;

  constructor(@Inject(DOCUMENT) private document: Document) { }


  ngOnInit(): void {
  }

  addedSuffix(event) {

    this.item.suffix = event.target.value;
    let textEditorBody = this.stringToHTML(this.item.htmlTable)

    const rows = textEditorBody.querySelectorAll('tr');

    //we are adding suffix as last td inside the row 
    rows.forEach(row => {
      
      const existingSuffix = row.querySelector('.suffix');

      
      //if sufix already exist just change content
      if(existingSuffix) {
        
        existingSuffix.textContent = event.target.value;
        
        return;
      }

      //if sufix doesn't exist then create new one
      const td = this.document.createElement('td');
      td.style.cssText = `
        border: none;
        white-space: nowrap;
        padding-left: 7px;
      `

      td.classList.add('suffix');
      td.setAttribute('contenteditable', 'false');
      td.textContent = event.target.value;

      row.appendChild( td );

    });

    let formatChildren = `` 
    textEditorBody.childNodes.forEach((item: any) => {
      formatChildren += item.outerHTML;
    })

    this.item.htmlTable = formatChildren;
  }

  stringToHTML(str) {
    var dom = this.document.createElement('div');
    dom.classList.add('wrapper')
    dom.innerHTML = str;
    return dom;
  };
}
