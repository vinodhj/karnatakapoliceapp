import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormBuilderTableSettingsComponent } from './form-builder-table-settings.component';

describe('FormBuilderTableSettingsComponent', () => {
  let component: FormBuilderTableSettingsComponent;
  let fixture: ComponentFixture<FormBuilderTableSettingsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormBuilderTableSettingsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormBuilderTableSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
