import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormBuilderComponent } from './form-builder.component';

const routes: Routes = [
  { path: 'edit/:id', component: FormBuilderComponent },
  { path: 'create/:id', component: FormBuilderComponent },
  { path: 'fill-form/:id', component: FormBuilderComponent },
  { path: 'view-form/:id/:userFormId', component: FormBuilderComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FormBuilderRoutingModule { }
