
import { trigger, transition, style, animate } from '@angular/animations';
import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { DndDropEvent, DropEffect } from 'ngx-drag-drop';
import { NgxIndexedDBService } from 'ngx-indexed-db';
import { getUniqueId } from 'src/app/utils/randomNumber';
import { FormBuilderElementsService } from '../services/form-builder-elements.service';
import { FormBuilderIndexeddbService } from '../services/form-builder-indexeddb.service';

@Component({
  selector: 'app-form-builder-content',
  templateUrl: './form-builder-content.component.html',
  styleUrls: ['./form-builder-content.component.scss'],
})
export class FormBuilderContentComponent implements OnInit {
  ngOnInit(): void {

  }

  activeElement = null;

  pageBuilder: any = this.formBuilderElementsService.pageBuilder;

  private currentDraggableEvent?: DragEvent;
  private currentDragEffectMsg?: string;

  constructor(private formBuilderElementsService: FormBuilderElementsService, private formBuilderIndexeddbService: FormBuilderIndexeddbService, private route: ActivatedRoute) {
  }

  removeElement(item: any) {
    this.formBuilderElementsService.deleteElementById(this.pageBuilder, item);
    this.onDragEnd();

    this.formBuilderIndexeddbService.updateDataInsideIndexedDb(
      +this.route.snapshot.paramMap.get('id'),
      this.pageBuilder);
  }


  onDragStart(event: DragEvent) {
  }

  onDragged(item: any, list: any[], effect: DropEffect) {
    this.formBuilderElementsService.onDragged(item, list, effect);
  }

  onDragEnd() {
    //if row doesn't have any columns delete that raw
    //same if colum doesn't have any row then remove it
    // this.pageBuilder.forEach((list: any, index: any, object: any) => {
    //   if (list.children?.length === 0) {
    //     object.splice(index, 1);
    //   }
    // })

    this.formBuilderIndexeddbService.updateDataInsideIndexedDb(
      +this.route.snapshot.paramMap.get('id'),
      this.pageBuilder);
  }

  onDrop(event: DndDropEvent, list?: any[]) {
    this.formBuilderElementsService.onDrop(event, list);

    this.formBuilderIndexeddbService.updateDataInsideIndexedDb(
      +this.route.snapshot.paramMap.get('id'),
      this.pageBuilder);


    console.log(this.pageBuilder)
  }

  async pasteContent() {
    this.pageBuilder.splice(0, 0, {
      id: getUniqueId(4),
      html: await this.formBuilderElementsService.pasteContent(),
      dndType: "droppableFormat"
    });
  }

  showFormData: boolean = false;
  
  submitTableData(event) {
    this.formBuilderElementsService.tableValues = event
    
  }


}


