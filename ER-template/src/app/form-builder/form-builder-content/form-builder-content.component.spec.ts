import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormBuilderContentComponent } from './form-builder-content.component';

describe('FormBuilderContentComponent', () => {
  let component: FormBuilderContentComponent;
  let fixture: ComponentFixture<FormBuilderContentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormBuilderContentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormBuilderContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
