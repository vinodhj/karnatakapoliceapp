import { trigger, transition, style, animate } from '@angular/animations';
import { AfterViewInit, Component, ElementRef, EventEmitter, OnInit, Output, QueryList, ViewChildren } from '@angular/core';
import { Router } from '@angular/router';
import { formBuilderItems } from './formItemsData';

@Component({
  selector: 'app-form-builder-items',
  templateUrl: './form-builder-items.component.html',
  styleUrls: ['./form-builder-items.component.scss'],
  animations: [
    trigger('slideInOut', [
      transition(':enter', [
        style({ transform: 'translateX(100%)', opacity: 1 }),
        animate('200ms ease-in', style({ transform: 'translateX(0%)' }))
      ]),
      transition(':leave', [
        animate('200ms ease-in', style({ transform: 'translateX(100%)', opacity: 0 }))
      ])
    ])
  ]
})
export class FormBuilderItemsComponent implements OnInit, AfterViewInit {
  formBuilderItems = formBuilderItems;
  disableClickOutside: boolean = false;


  isEdit = this.router.url.includes('edit');
  isFill = this.router.url.includes('fill-form');
  isPreview = this.router.url.includes('view-form');

  showSideMenu: boolean = (this.isFill || this.isPreview) ? false : true;

  @ViewChildren('items') items!: QueryList<ElementRef>;
  @Output() emitEvent = new EventEmitter();


  constructor(private router: Router) { }

  ngOnInit(): void {
    this.generateDataForDrop();
  }

  ngAfterViewInit(): void {
    this.items.forEach(el => {

      if (el.nativeElement.getAttribute('actionBtn'))
        el.nativeElement.addEventListener('click', () =>
          this.emitEvent.emit({ eventName: el.nativeElement.getAttribute('actionBtn') }))

    })
  }

  generateDataForDrop() {

    this.formBuilderItems.map(item => {

      item.elements.map((el: any) => {
        //generate rows and columns for grid items
        this.generateRow(el.children, el);
        this.generateFormFields(el);
        this.generateATMCard(el);
      })
    })
  }

  generateFormFields(element: any) {
    if (element.elementType === 'form' || 
    element.elementType === 'table' || 
    element.elementType === 'image' || element.elementType === 'textEditor' || element.elementType === 'tableForm' || element.elementType === 'excel')
      element.firstDropForm = true;
  }

  generateATMCard(element) {
    if(element.type === 'atm-card') {
 
      const card = {
        id: null,
        type: "atm-card",
        class: "atm-card",
        children: [],
        allowedTypes: ['droppableFormat', 'row'],
        dndType: "columns",
        firstDrop: true,
      }

      element.dropData = card;

    }
  }

  generateRow(colsNumber: any, element: any) {
    if (!colsNumber) return;

    const row: any = {
      type: "row",
      class: 'flex row-indicator',
      allowedTypes: ['columns'],
      children: [],
      id: null,
      firstDrop: true,
      dndType: "row",
      disableDropzone: true
    }

    for (let i = 0; i < colsNumber; i++) {
      row.children.push({
        id: null,
        type: "col",
        class: "min-h-10 col-indicator",
        children: [],
        allowedTypes: ['droppableFormat', 'row'],
        dndType: "columns",
      })
    }

    element.dropData = row;
  }


}
