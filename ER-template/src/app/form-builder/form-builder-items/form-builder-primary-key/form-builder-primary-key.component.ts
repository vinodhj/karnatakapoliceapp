import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { HttpService } from 'src/@exai/services/http.service';

@Component({
  selector: 'exai-form-builder-primary-key',
  templateUrl: './form-builder-primary-key.component.html',
  styleUrls: ['./form-builder-primary-key.component.scss']
})
export class FormBuilderPrimaryKeyComponent implements OnInit {

  pickedPrimaryKey;

  primaryKeyList$

  constructor(public dialogRef: MatDialogRef<FormBuilderPrimaryKeyComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private httpService: HttpService) { }

  ngOnInit(): void {
    this.primaryKeyList$ = this.httpService.getPrimaryKeysBySubDivision(this.data.routeId);
  }

}
