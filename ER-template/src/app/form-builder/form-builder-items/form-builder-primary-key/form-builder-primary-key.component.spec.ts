import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormBuilderPrimaryKeyComponent } from './form-builder-primary-key.component';

describe('FormBuilderPrimaryKeyComponent', () => {
  let component: FormBuilderPrimaryKeyComponent;
  let fixture: ComponentFixture<FormBuilderPrimaryKeyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormBuilderPrimaryKeyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormBuilderPrimaryKeyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
