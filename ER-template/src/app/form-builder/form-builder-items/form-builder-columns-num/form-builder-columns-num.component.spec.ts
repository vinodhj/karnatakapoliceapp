import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormBuilderColumnsNumComponent } from './form-builder-columns-num.component';

describe('FormBuilderColumnsNumComponent', () => {
  let component: FormBuilderColumnsNumComponent;
  let fixture: ComponentFixture<FormBuilderColumnsNumComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormBuilderColumnsNumComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormBuilderColumnsNumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
