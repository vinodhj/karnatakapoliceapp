import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'exai-form-builder-columns-num',
  templateUrl: './form-builder-columns-num.component.html',
  styleUrls: ['./form-builder-columns-num.component.scss']
})
export class FormBuilderColumnsNumComponent implements OnInit {
  columns = [1,2,3,4,5,6,7,8,9,10,11,12];

  constructor() { }

  ngOnInit(): void {
  }

}
