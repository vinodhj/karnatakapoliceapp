import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormBuilderItemsComponent } from './form-builder-items.component';

describe('FormBuilderItemsComponent', () => {
  let component: FormBuilderItemsComponent;
  let fixture: ComponentFixture<FormBuilderItemsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormBuilderItemsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormBuilderItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
