export const formBuilderItems = [
  {
    title: "Basic Elements",
    elements: [
      {
        type: "one-column",
        label: "1 Column",
        icon: 'check_box_outline_blank',
        children: 1
      },
      {
        type: "two-columns",
        label: "2 Columns",
        icon: "view_agenda",
        children: 2
      },
      {
        type: "three-columns",
        label: "3 Columns",
        icon: "view_column",
        children: 3
      },
      {
        type: "custom-columns",
        label: "Columns",
        icon: "add_box",
        customColumnNumber: true,
        firstDrop: true
      },
      {
        type: "paste-data",
        label: "Paste Data",
        icon: "content_paste_go",
        actionBtn: 'paste'
      },
      {
        "type": "text",
        "elementType": "textEditor",
        "icon":"text_fields",
        "label": "Text",
        html: "&nbsp;",
        dndType: "droppableFormat",
        "className": "w-full",
      },
      {
        type: 'primary-key',
        elementType: 'primaryKey',
        icon: 'vpn_key',
        label: "Primary Key",
        dndType: "droppableFormat",
        primaryKey: true,
        firstDrop: true
      }
    ]
  },
  {
    title: 'Form Elements',
    elements: [
      {
        "type": "text",
        "elementType": "form",
        "icon": "description_outline",
        "label": "Text",
        "value": "",
        "description": "Enter your name",
        "placeholder": "Enter your name",
        "className": "w-full",
        "subtype": "text",
        hideCondition: {},
        disabledCondition: {},
        dndType: "droppableFormat",
        validators: {},
        mask: {
          enableMask: false,
          prefix: '₹ ',
          suffix: '',
          thousandsSeparator: ',',
          decimalSeparator: '.',
          decimals: 2,
          align: 'right'
        }
      },
      {
        "type": "email",
        "elementType": "form",
        "icon": "email_outline",
        "label": "Email",
        "description": "Enter your email",
        "placeholder": "Enter your email",
        "className": "w-full",
        "subtype": "text",
        disabledCondition: {},
        hideCondition: {},
        "errorText": "Please enter a valid email",
        dndType: "droppableFormat",
        validators: {
          email: true,
        }
      },
      {
        "type": "phone",
        "elementType": "form",
        "icon": "phone",
        "label": "Phone",
        "description": "Enter your phone",
        "placeholder": "Enter your phone",
        "className": "w-full",
        "subtype": "text",
        disabledCondition: {},
        hideCondition: {},
        "errorText": "Please enter a valid phone number",
        dndType: "droppableFormat",
        validators: {},
        phoneMask: '(000) 000-0000'
      },
      {
        "type": "number",
        "elementType": "form",
        "label": "Number",
        "icon": "numbers",
        "description": "Number",
        "placeholder": "Enter number",
        "className": "w-full",
        "value": "",
        hideCondition: {},
        disabledCondition: {},
        dndType: "droppableFormat",
        validators: {
          pattern: "^[0-9]*$",
          min: null,
          max: null,
        },
      },
      {
        "type": "password",
        "elementType": "form",
        "label": "Password",
        "icon": "password",
        "description": "Password",
        "placeholder": "Enter password",
        "className": "w-full",
        hideCondition: {},
        disabledCondition: {},
        dndType: "droppableFormat",
        validators: {}
      },
      {
        "type": "date",
        "elementType": "form",
        "icon":"event",
        "label": "Date",
        "placeholder": "Date",
        "className": "w-full",
        hideCondition: {},
        disabledCondition: {},
        dndType: "droppableFormat",
        validators: {}
      },
      {
        "type": "datetime-local",
        "elementType": "form",
        "icon":"event",
        "label": "DateTime",
        "placeholder": "Date Time",
        "className": "w-full",
        hideCondition: {},
        disabledCondition: {},
        dndType: "droppableFormat",
        validators: {}
      },
      {
        "type": "textarea",
        "elementType": "form",
        "icon":"notes",
        "className": "w-full",
        "label": "Textarea",
        disabledCondition: {},
        hideCondition: {},
        dndType: "droppableFormat",
        validators: {},
        htmlTextarea: ''
      },
      {
        "type": "checkbox",
        "elementType": "form",
        "required": true,
        "label": "Checkbox",
        "icon":"check_box",
        "className": "w-full",
        "description": "Checkbox",
        disabledCondition: {},
        hideCondition: {},
        dndType: "droppableFormat",
        validators: {},
        "inline": true,
        "values": [
          {
            "label": "Option 1",
            "value": "option-1"
          },
          {
            "label": "Option 2",
            "value": "option-2"
          }
        ]
      },
      {
        "type": "radio",
        "elementType": "form",
        "icon":"radio_button_checked",
        "label": "Radio",
        "description": "Radio boxes",
        "className": "w-full",
        disabledCondition: {},
        hideCondition: {},
        dndType: "droppableFormat",
        validators: {},
        "values": [
          {
            "label": "Option 1",
            "value": "option-1"
          },
          {
            "label": "Option 2",
            "value": "option-2"
          }
        ]
      },
      {
        "type": "select",
        "elementType": "form",
        "icon":"fact_check",
        "label": "Dropdown",
        dndType: "droppableFormat",
        validators: {},
        "description": "Select",
        disabledCondition: {},
        hideCondition: {},
        "placeholder": "Select",
        "className": "w-full",
        "values": [
          {
            "label": "Option 1",
            "value": "option-1"
          },
          {
            "label": "Option 2",
            "value": "option-2"
          },
          {
            "label": "Option 3",
            "value": "option-3"
          }
        ]
      },
      {
        "type": "file",
        "elementType": "form",
        "icon":"file_upload",
        "label": "File Upload",
        dndType: "droppableFormat",
        fileUpload: {},
        disabledCondition: {},
        "className": "w-full",
        hideCondition: {},
        "subtype": "file",
        validators: {}
      },
      {
        "type": "button",
        "elementType": "button",
        "buttonType": "submit",
        "icon":"smart_button",
        "label": "Submit",
        dndType: "droppableFormat",
        "className": "w-full",
      },
      {
        "type": "table",
        "elementType": "table",
        "icon":"table",
        "label": "Table",
        dndType: "droppableFormat",
        "className": "w-full",
        htmlTable: '',
        validators: {},
        hideCondition: {},
        disabledCondition: {},
      },
      // {
      //   "type": "imageTable",
      //   "elementType": "imageTable",
      //   "icon":"image",
      //   "label": "Image",
      //   dndType: "droppableFormat",
      //   "className": "w-full",
      //   validators: {},
      //   hideCondition: {},
      //   disabledCondition: {},
      // },
      {
        "type": "image",
        "elementType": "image",
        "icon":"image",
        "label": "Image",
        dndType: "droppableFormat",
        "className": "w-full",
        openOnDrop: true,
        imageData: {}
      },
      {
        "type": "table-form",
        "elementType": "tableForm",
        "icon":"Table",
        "label": "Table Form",
        dndType: "droppableFormat",
        "className": "w-full",
        tableValue: {}
      },
      {
        "type": "excel-form",
        "elementType": "excel",
        "icon":"table_view",
        "label": "Excel Table",
        dndType: "droppableFormat",
        "className": "w-full",
        tableValue: {}
      },

    ]
  }
]