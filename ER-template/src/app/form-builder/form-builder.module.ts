import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormBuilderRoutingModule } from './form-builder-routing.module';
import { FormBuilderComponent } from './form-builder.component';
import { FormBuilderItemsComponent } from './form-builder-items/form-builder-items.component';
import { FormBuilderContentComponent } from './form-builder-content/form-builder-content.component';
import { MatExpansionModule } from '@angular/material/expansion';

import { MatIconModule } from '@angular/material/icon';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatCardModule } from '@angular/material/card';
import { DndModule } from 'ngx-drag-drop';
import { SanitizeHtmlPipe } from '../shared/pipes/sanitize-html.pipe';
import { MatButtonModule } from '@angular/material/button';
import { FormBuilderInputElementsComponent } from './form-builder-elements/form-builder-input-elements/form-builder-input-elements.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { MatRadioModule } from '@angular/material/radio';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatInputModule } from '@angular/material/input';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { NgxEditorModule } from 'ngx-editor';
import { MdePopoverModule } from '@material-extended/mde';
import { FormBuilderElementSettingsComponent } from './form-builder-elements/form-builder-element-settings/form-builder-element-settings.component';
import { MatTabsModule } from '@angular/material/tabs';
import { FormBuilderElementHtmlComponent } from './form-builder-elements/form-builder-element-html/form-builder-element-html.component';
import { FormBuilderResultComponent } from './form-builder-result/form-builder-result.component';
import { FormBuilderControlComponent } from './form-builder-result/form-builder-control/form-builder-control.component';
import { FormBuilderElementButtonsComponent } from './form-builder-elements/form-builder-element-buttons/form-builder-element-buttons.component';
import { FormBuilderElementFileUploadComponent } from './form-builder-elements/form-builder-element-file-upload/form-builder-element-file-upload.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { FormStatusHistoryModule } from '../shared/form-status-history/form-status-history.module';
import { FormBuilderSharedModule } from '../shared/form-status-history/form-builder-shared.module';
import { RichTextEditorAllModule } from '@syncfusion/ej2-angular-richtexteditor';
import { DialogModule } from '@syncfusion/ej2-angular-popups';
import { ExaiPopoverDirective } from './directives/popover.directive';
import { MatMenuModule } from '@angular/material/menu';
import { FormBuilderResultForExportComponent } from './form-builder-result-for-export/form-builder-result-for-export.component';

// Custom
import { FormBuilderColumnsNumComponent } from './form-builder-items/form-builder-columns-num/form-builder-columns-num.component';
import { MatDialogModule } from '@angular/material/dialog';
import { FormBuilderImageComponent } from './form-builder-elements/form-builder-image/form-builder-image.component';
import { FormBuilderImageSettingsComponent } from './form-builder-image-settings/form-builder-image-settings.component';
// Custom
import {ClipboardModule} from '@angular/cdk/clipboard';
import { NgxPrintModule } from 'ngx-print';
import { FormBuilderElementTextareaComponent } from './form-builder-elements/form-builder-element-textarea/form-builder-element-textarea.component';
import { FormBuilderCloneComponent } from './form-builder-clone/form-builder-clone.component';
import { FormBuilderAtmCardComponent } from './form-builder-elements/form-builder-atm-card/form-builder-atm-card.component';
import { FormBuilderAtmCardSettingsComponent } from './form-builder-elements/form-builder-atm-card-settings/form-builder-atm-card-settings.component';
import { FormBuilderPrimaryKeyComponent } from './form-builder-items/form-builder-primary-key/form-builder-primary-key.component';
import { FormBuilderElementPrimaryKeyComponent } from './form-builder-elements/form-builder-element-primary-key/form-builder-element-primary-key.component';
import { NgxCurrencyModule } from 'ngx-currency';
import { NgxMaskModule } from 'ngx-mask';
import { AdvancedGridItemModule } from '../shared/advanced-grid-item/advanced-grid-item.module';
import { SpreadsheetAllModule } from '@syncfusion/ej2-angular-spreadsheet';
import { FormBuilderExcelComponent } from './form-builder-elements/form-builder-excel/form-builder-excel.component';
import { FormBuilderExcelSettingsComponent } from './form-builder-elements/form-builder-excel-settings/form-builder-excel-settings.component';



@NgModule({
  declarations: [
    FormBuilderComponent,
    FormBuilderItemsComponent,
    FormBuilderContentComponent,
    FormBuilderInputElementsComponent,
    FormBuilderElementSettingsComponent,
    FormBuilderElementHtmlComponent,
    FormBuilderResultComponent,
    FormBuilderControlComponent,
    FormBuilderElementButtonsComponent,
    FormBuilderResultForExportComponent,
    FormBuilderColumnsNumComponent,
    FormBuilderImageComponent,
    FormBuilderImageSettingsComponent,
    FormBuilderElementTextareaComponent,
    FormBuilderCloneComponent,
    FormBuilderAtmCardComponent,
    FormBuilderAtmCardSettingsComponent,
    FormBuilderPrimaryKeyComponent,
    FormBuilderElementPrimaryKeyComponent,
    FormBuilderExcelComponent,
    FormBuilderExcelSettingsComponent
  ],
  imports: [
    CommonModule,
    FormBuilderRoutingModule,
    MatExpansionModule,
    MatIconModule,
    MatSnackBarModule,
    RichTextEditorAllModule,
    DialogModule,
    MatCardModule,
    DndModule,
    ClipboardModule,
    MatButtonModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatRadioModule,
    MatCheckboxModule,
    MatMenuModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    NgxEditorModule,
    MdePopoverModule,
    MatTabsModule,
    MatProgressSpinnerModule,
    MatProgressBarModule,
    MatTooltipModule,
    FormBuilderSharedModule,
    FormStatusHistoryModule,
    MatDialogModule,
    NgxPrintModule,
    NgxCurrencyModule,
    NgxMaskModule,
    AdvancedGridItemModule,
    SpreadsheetAllModule 
  ],
  providers: []
})

export class FormBuilderModule { }
