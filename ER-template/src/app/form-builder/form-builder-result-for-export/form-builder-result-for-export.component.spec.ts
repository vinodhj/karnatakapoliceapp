import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormBuilderResultForExportComponent } from './form-builder-result-for-export.component';

describe('FormBuilderResultForExportComponent', () => {
  let component: FormBuilderResultForExportComponent;
  let fixture: ComponentFixture<FormBuilderResultForExportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormBuilderResultForExportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormBuilderResultForExportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
