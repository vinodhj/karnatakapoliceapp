import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, ValidationErrors } from '@angular/forms';
import { returnAllFormControls } from 'src/app/utils/randomNumber';
import { JsonFormControls } from '../models/JsonForm';
import { FormBuilderElementsService } from '../services/form-builder-elements.service';
import { FormBuilderFillDataService } from '../services/form-builder-fill-data.service';
import { FormBuilderReactiveService } from '../services/form-builder-reactive.service';

@Component({
  selector: 'exai-form-builder-result-for-export',
  templateUrl: './form-builder-result-for-export.component.html',
  styleUrls: ['./form-builder-result-for-export.component.scss']
})
export class FormBuilderResultForExportComponent implements OnInit {


  @Input() isPreview: boolean;
  @Input() fillData;

  formControls: any = []

  public generatedForm: FormGroup = this.fb.group({});

  @Input() pageBuilder: any;
  loadingUserData: boolean;

  constructor(private fb: FormBuilder, private formBuilderReactive: FormBuilderReactiveService, private formBuilderFillData: FormBuilderFillDataService) { }

  getFormValidationErrors() {
    Object.keys(this.generatedForm.controls).forEach(key => {
      const controlErrors: ValidationErrors = this.generatedForm.get(key).errors;
      if (controlErrors != null) {
        Object.keys(controlErrors).forEach(keyError => {
          console.log(this.allForms.filter(c => c.id === key))

         console.log('Key control: ' + key + ', keyError: ' + keyError + ', err value: ', controlErrors[keyError]);
        });
      }
    });
  }

  allForms;

  ngOnInit(): void {
    let allFormControls: any = returnAllFormControls(this.pageBuilder);

    const filteredFormControls = allFormControls.filter(c => c.elementType === 'form');

    this.allForms = filteredFormControls;

    this.createForm(filteredFormControls);

    if(this.isPreview) {
      this.fillFormData();
    }

    this.getFormValidationErrors();

    //run value changes on initialization
    this.generatedForm.updateValueAndValidity();
  }

  fillFormData() {
    this.loadingUserData = true;

    this.generatedForm.patchValue(this.fillData, {
        emitEvent: false,
        onlySelf: true
      });
  }

  addHideConditions(allFormControls: any, key: any) {
    //current form field
    const formItem = allFormControls.find((c: any) => c.id === key);

    if (formItem.hideCondition?.field) {
      //get field from hide condition if value match then disable and hide field
      //we need to disable because if field is required and hidden form is invalid
      this.getFormField(formItem.hideCondition.field.id)?.value === formItem.hideCondition.value ?
        (this.enableFormField(formItem.id), formItem.hide = false) :
        (this.disableFormField(formItem.id), formItem.hide = true)
    }
  }

  addDisableConditions(allFormControls: any, key: any) {
    //current form field
    const formItem = allFormControls.find((c: any) => c.id === key);

    if (formItem.disabledCondition?.field) {
      //get field from disabled condition if value match then disable else enable
      this.getFormField(formItem.disabledCondition.field.id)?.value === formItem.disabledCondition.value ?
        this.disableFormField(formItem.id) :
        this.enableFormField(formItem.id)
    }
  }

  getFormField(id: any) {
    return this.generatedForm.get(id);
  }

  disableFormField(id: any) {
    this.generatedForm.get(id)?.disable({ emitEvent: false, onlySelf: true })
  }

  enableFormField(id: any) {
    this.generatedForm.get(id)?.enable({ emitEvent: false, onlySelf: true })
  }



  createForm(controls: JsonFormControls[]) {
    for (const control of controls) {
      const validatorsToAdd = this.formBuilderReactive.returnValidators(control);

      this.generatedForm.addControl(
        control.id,
        this.fb.control(control.value, validatorsToAdd)
      );
    }
  }

  submitForm() {
  }

}
