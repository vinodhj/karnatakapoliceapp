interface JsonFormValidators {
    min?: number;
    max?: number;
    required?: boolean;
    requiredTrue?: boolean;
    email?: boolean;
    minLength?: boolean;
    maxLength?: boolean;
    pattern?: string;
    nullValidator?: boolean;
}

interface JsonFormControlOptions {
    min?: string;
    max?: string;
    step?: string;
    icon?: string;
}
export interface JsonFormControls {
    id: string;
    label: string;
    value: string;
    className: string;
    icon: string;
    description: string;
    placeholder: string;
    subtype: string;
    regex: string
    type: string;
    options?: JsonFormControlOptions;
    required?: boolean;
    validators: JsonFormValidators;
    errorText?: string
}

// export interface JsonFormData {
//     controls: JsonFormControls[];
// }