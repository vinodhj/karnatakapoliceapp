import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormBuilderControlComponent } from './form-builder-control.component';

describe('FormBuilderControlComponent', () => {
  let component: FormBuilderControlComponent;
  let fixture: ComponentFixture<FormBuilderControlComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormBuilderControlComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormBuilderControlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
