import { Component, ElementRef, Input, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { distinctUntilChanged, finalize } from 'rxjs/operators';
import { HttpService } from 'src/@exai/services/http.service';
import { returnAllFormControls } from 'src/app/utils/randomNumber';
import { JsonFormControls } from '../models/JsonForm';
import { FormBuilderElementsService } from '../services/form-builder-elements.service';
import { FormBuilderFileUploadService } from '../services/form-builder-file-upload.service';
import { FormBuilderFillDataService } from '../services/form-builder-fill-data.service';
import { FormBuilderReactiveService } from '../services/form-builder-reactive.service';

@Component({
  selector: 'app-form-builder-result',
  templateUrl: './form-builder-result.component.html',
  styleUrls: ['./form-builder-result.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FormBuilderResultComponent implements OnInit {
  
  isPreview: boolean = this.router.url.includes('view-form');

  updateAllowed: boolean;

  formControls: any = []

  public generatedForm: FormGroup = this.fb.group({});

  @Input() pageBuilder: any = this.formBuilderElementsService.pageBuilder;
  loadingUserData: boolean;

  constructor(private router: Router, private fb: FormBuilder, private formBuilderReactive: FormBuilderReactiveService, private formBuilderFileUpload: FormBuilderFileUploadService, private formBuilderElementsService: FormBuilderElementsService, private route: ActivatedRoute, private httpService: HttpService, private formBuilderFillData: FormBuilderFillDataService, private snackbar: MatSnackBar) { }

  getFormValidationErrors() {
    Object.keys(this.generatedForm.controls).forEach(key => {
      const controlErrors: ValidationErrors = this.generatedForm.get(key).errors;
      if (controlErrors != null) {
        Object.keys(controlErrors).forEach(keyError => {
          console.log(this.allForms.filter(c => c.id === key))

          console.log('Key control: ' + key + ', keyError: ' + keyError + ', err value: ', controlErrors[keyError]);
        });
      }
    });
  }

  allForms;

  ngOnInit(): void {

    let allFormControls: any = returnAllFormControls(this.pageBuilder);    

    const filteredFormControls = allFormControls.filter(c => c.elementType === 'form' || c.elementType === 'table' || c.elementType === 'tableForm' || c.elementType === 'excel');

    this.allForms = filteredFormControls;

    this.createForm(filteredFormControls);

    this.isEditAllowed();

    this.generatedForm.valueChanges.pipe(
      distinctUntilChanged()
    ).subscribe((response) => {

      //we need disabled fields too
      const rawFormValues = this.generatedForm.getRawValue();

      Object.entries(rawFormValues).forEach(([key, value]) => {

        this.addHideConditions(allFormControls, key);

        this.addDisableConditions(allFormControls, key);
      });
      
    })

    if (this.isPreview) {
      this.fillFormData();
    }

    this.getFormValidationErrors();

    //run value changes on initialization
    this.generatedForm.updateValueAndValidity();
  }


  isEditAllowed() {

    if (this.isPreview)
      this.httpService.canUserEditFormRecord(this.route.snapshot.paramMap.get('userFormId')).subscribe((res: boolean) => {
        this.isPreview = !res;
        this.updateAllowed = res;
      });

  }

  fillFormData() {

    this.loadingUserData = true;

    this.httpService.getViewForFormBulder(this.route.snapshot.paramMap.get('userFormId')).pipe(
      finalize(() => this.loadingUserData = false)
    ).subscribe(res => {
      this.generatedForm.patchValue(res, {
        emitEvent: false,
        onlySelf: true
      });
    })
  }

  addHideConditions(allFormControls: any, key: any) {
    //current form field
    const formItem = allFormControls.find((c: any) => c.id === key);


    if (formItem?.hideCondition?.field) {
      //get field from hide condition if value match then disable and hide field
      //we need to disable because if field is required and hidden form is invalid
      this.getFormField(formItem.hideCondition.field.id)?.value === formItem.hideCondition.value ?
        (this.enableFormField(formItem.id), formItem.hide = false) :
        (this.disableFormField(formItem.id), formItem.hide = true)
    }
  }

  addDisableConditions(allFormControls: any, key: any) {
    //current form field
    const formItem = allFormControls.find((c: any) => c.id === key);

    if (formItem?.disabledCondition?.field) {
      //get field from disabled condition if value match then disable else enable
      this.getFormField(formItem.disabledCondition.field.id)?.value === formItem.disabledCondition.value ?
        this.disableFormField(formItem.id) :
        this.enableFormField(formItem.id)
    }
  }

  getFormField(id: any) {
    return this.generatedForm.get(id);
  }

  disableFormField(id: any) {
    this.generatedForm.get(id)?.disable({ emitEvent: false, onlySelf: true })
  }

  enableFormField(id: any) {
    this.generatedForm.get(id)?.enable({ emitEvent: false, onlySelf: true })
  }

  createForm(controls: JsonFormControls[]) {
    for (const control of controls) {
      const validatorsToAdd = this.formBuilderReactive.returnValidators(control);

      this.generatedForm.addControl(
        control.id,
        this.fb.control(control.value || (control as any).htmlTable || (control as any).htmlTextarea || (control as any)?.tableValue?.rows, validatorsToAdd)
      );
    }
  }

  submittingForm: boolean = false;

  submitForm() {

    const data: any = this.formBuilderFillData.getFormData();

    if (data.formId && !this.updateAllowed) {
      this.submittingForm = true;
      this.httpService.formAddNew(data.formId, null, JSON.stringify(this.generatedForm.value), data.candidateName, data.candidateNumber, data.candidateEmail, data.formName)
        .pipe(
          finalize(() => this.submittingForm = false)
        ).subscribe(
          res => {
            this.router.navigateByUrl('/forms/submitted-form');
          },
          err => {

            this.snackbar.open(err?.error?.Message, "Ok", {
              duration: 3000
            });

          });
    }


    if(this.updateAllowed) {

      this.submittingForm = true;

      this.httpService.templateFormRecordUpdate(this.route.snapshot.paramMap.get('userFormId'), JSON.stringify(this.generatedForm.value)).pipe(
        finalize(() => this.submittingForm = false)
      ).subscribe(res => {
 
        this.router.navigateByUrl('/forms/submitted-form'); 
 
      }, err => { 
        this.snackbar.open(err?.error?.Message, "Ok", { 
          duration: 3000 
        });
      })

    }

  }
}
