import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormBuilderResultComponent } from './form-builder-result.component';

describe('FormBuilderResultComponent', () => {
  let component: FormBuilderResultComponent;
  let fixture: ComponentFixture<FormBuilderResultComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormBuilderResultComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormBuilderResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
