import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormBuilderImageSettingsComponent } from './form-builder-image-settings.component';

describe('FormBuilderImageSettingsComponent', () => {
  let component: FormBuilderImageSettingsComponent;
  let fixture: ComponentFixture<FormBuilderImageSettingsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormBuilderImageSettingsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormBuilderImageSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
