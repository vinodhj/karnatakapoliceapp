import { HttpClient } from '@angular/common/http';
import { ChangeDetectorRef, Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { calculateAspectRatioFit } from 'src/app/utils/randomNumber';
import * as uuid from "uuid";

@Component({
  selector: 'exai-form-builder-image-settings',
  templateUrl: './form-builder-image-settings.component.html',
  styleUrls: ['./form-builder-image-settings.component.scss']
})
export class FormBuilderImageSettingsComponent implements OnInit {

  keepAspectRatio: boolean = true;

  uploadedFile;

  constructor(private http: HttpClient, @Inject(MAT_DIALOG_DATA) public data: any, private cdr: ChangeDetectorRef, private snackbar: MatSnackBar) { }

  ngOnInit(): void {

    this.initializeImage();

  }

  changeDimension(width, height) {

    let widthNumber = parseFloat(width);
    
    let originalHeight = parseFloat(this.uploadedFile.originalHeight);
    let originalWidth = parseFloat(this.uploadedFile.originalWidth);
  
    this.uploadedFile.height = this.determineNewHeight(originalHeight, originalWidth, widthNumber);

  }

  determineNewHeight(originalHeight, originalWidth, newWidth){
    return Math.floor((originalHeight / originalWidth) * newWidth);
}

  checkDimensions(uploadedFile) {
    if(uploadedFile.width > 700) {
      
      uploadedFile.width = 700;

      let originalHeight = this.uploadedFile.originalHeight;
      let originalWidth = this.uploadedFile.originalWidth;

      uploadedFile.height = this.determineNewHeight(originalHeight, originalWidth, uploadedFile.width);

    }

  }

  initializeImage() {

    if(this.data?.imageData?.imageContent) {

      const { imageContent, width, height, originalHeight, originalWidth } = this.data.imageData;

      this.uploadedFile = { imageContent, width, height, originalHeight, originalWidth };

    }
  }

  filesDropped(files) {

    const file = files[0];

    if(!file.type.includes('image/')) {

      this.fileIsNotImage(file);

      return;
    }

    file.attachmentId = uuid.v4();

    const reader = new FileReader();

    reader.onload = () => {
      
      var image: any = new Image();

      image.src = reader.result;

      image.onload = () => {

        this.uploadedFile = { imageContent: reader.result, width: image.width, height: image.height, originalWidth: image.width, originalHeight: image.height };

      }
    }

    reader.readAsDataURL(file);

  }

  fileIsNotImage(file) {
    this.snackbar.open("Only image file allowed! Please select image.", 'Ok', {
      duration: 3000
    })
  }

  uploadFilesFromInput(event) {
    const files = event.target.files;
    this.filesDropped(files);

    //reset value so it can be run multiple times
    event.target.value = '';
  }

  getMeta(url, callback) {
    const img = new Image();
    img.src = url;
    img.onload = () => { 
      callback(img.width, img.height); 
      
    }
}
}
