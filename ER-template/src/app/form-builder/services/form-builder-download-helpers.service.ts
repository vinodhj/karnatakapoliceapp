import { Injectable } from '@angular/core';
import { HtmlToDocxService } from 'src/app/shared/services/html-to-docx.service';

@Injectable({
  providedIn: 'root'
})
export class FormBuilderDownloadHelpersService {

  constructor(private htmlToDocxService: HtmlToDocxService) { }

  makeGroupsFromFormElements(pageBuilder) {

    const formattedArray = [];

    pageBuilder.map(element => {
      if (!element.grouped) {
        //if element is not grouped then we don't need to change anything
        formattedArray.push(element);
        return;
      }

      let found = false;

      formattedArray.map(f => {

        //find group and add element to that group
        if (f.groupName === element.groupName) {
          f.children.push(element);
          found = true;
        }

      })

      if (found) return;

      //if there is no group name with this name then create it and add first child
      formattedArray.push({
        groupName: element.groupName,
        group: true,
        children: [element],
        verticalGrouped: element.verticalGrouped,
        verticalGroupName: element.verticalGroupName
      })

    })

    return formattedArray;
  }

  makeVerticalGroupElements(pageBuilder) {
    const formattedArray = [];

    pageBuilder.map(element => {
      if (!element.verticalGrouped) {
        //if element is not grouped then we don't need to change anything
        formattedArray.push(element);
        return;
      }

      let found = false;

      formattedArray.map(f => {

        //find group and add element to that group
        if (f.verticalGroupName === element.verticalGroupName) {
          f.children.push(element);
          found = true;
        }

      })

      if (found) return;

      //if there is no group name with this name then create it and add first child
      formattedArray.push({
        children: [element],
        verticalGrouped: element.verticalGrouped,
        verticalGroupName: element.verticalGroupName
      })
    })

    return formattedArray;
  }

  createElement(tag, styles = null, attributes = null) {
    var element = document.createElement(tag);
    styles ? element.style.cssText = styles : null;

    if (attributes)
      for (let key in attributes) {
        element.setAttribute(key, attributes[key])
      }

    return element;
  }

  dateParser(chaine) {
    let newDate: any = new Date(chaine).toLocaleDateString("fr-FR", {
      year: "numeric",
      month: "numeric",
      day: "numeric",
    });

    return newDate;
  }

  formatTableForm(form, matchFields) {

    let formattedTableValues = []

    form?.rows?.forEach((row: any) => {
      let oneRowInTable = {};
      for (const rowKey in matchFields) {
        const headerName = matchFields[rowKey];
        if (row[rowKey])
          oneRowInTable[headerName] = { value: row[rowKey].value, rowSpan: row[rowKey].rowSpan, colSpan: row[rowKey].colSpan, type: row[rowKey].type, visible: true };
        if (row.hiddenFromUser) {
          oneRowInTable[headerName] = { value: row[rowKey].value, rowSpan: row[rowKey].rowSpan, colSpan: row[rowKey].colSpan, type: row[rowKey].type, visible: false };
        }

        if (!row[rowKey].value) {
          oneRowInTable[headerName].value = ''
        }
        if (row[rowKey].type === 'file' && row[rowKey].value) {
          let files = []
          row[rowKey].value.forEach(file => {
            if (file.type && file.type === 'image') {
              files.push({
                imageContent: file.imageContent,
                type: file.type
              })
            } else {
              files.push({
                url: file.url,
                name: file.name
              })
            }
          })
          oneRowInTable[headerName].value = files
        }
        if (row[rowKey].type === 'hyperlink' && row[rowKey].value) {
          let links = []
          row[rowKey].value.forEach(link => {

            links.push({
              url: link.url,
              name: link.name
            })

          })
          oneRowInTable[headerName].value = links
        }
      }
      formattedTableValues.push(oneRowInTable);

    })

    return this.htmlToDocxService.generateTableWithMerge(formattedTableValues)
  }
}
