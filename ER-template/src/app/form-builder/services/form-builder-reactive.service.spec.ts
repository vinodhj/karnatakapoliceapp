import { TestBed } from '@angular/core/testing';

import { FormBuilderReactiveService } from './form-builder-reactive.service';

describe('FormBuilderReactiveService', () => {
  let service: FormBuilderReactiveService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FormBuilderReactiveService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
