import { TestBed } from '@angular/core/testing';

import { FormBuilderFileUploadService } from './form-builder-file-upload.service';

describe('FormBuilderFileUploadService', () => {
  let service: FormBuilderFileUploadService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FormBuilderFileUploadService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
