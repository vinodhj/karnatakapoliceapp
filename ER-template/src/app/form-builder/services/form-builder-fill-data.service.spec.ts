import { TestBed } from '@angular/core/testing';

import { FormBuilderFillDataService } from './form-builder-fill-data.service';

describe('FormBuilderFillDataService', () => {
  let service: FormBuilderFillDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FormBuilderFillDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
