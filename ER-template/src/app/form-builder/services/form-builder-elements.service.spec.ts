import { TestBed } from '@angular/core/testing';

import { FormBuilderElementsService } from './form-builder-elements.service';

describe('FormBuilderElementsService', () => {
  let service: FormBuilderElementsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FormBuilderElementsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
