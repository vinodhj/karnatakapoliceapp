import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FormBuilderFillDataService {

  private formDataFill = {};

  setFormData(object: any) {
    this.formDataFill = object;
  }
  
  getFormData() {
    return this.formDataFill;
  }

  constructor() { }
}
