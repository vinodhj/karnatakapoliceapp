import { TestBed } from '@angular/core/testing';

import { FormBuilderIndexeddbService } from './form-builder-indexeddb.service';

describe('FormBuilderIndexeddbService', () => {
  let service: FormBuilderIndexeddbService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FormBuilderIndexeddbService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
