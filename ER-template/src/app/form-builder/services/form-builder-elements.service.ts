import { ChangeDetectorRef, Injectable, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { DndDropEvent, DropEffect } from 'ngx-drag-drop';
import { getUniqueId } from 'src/app/utils/randomNumber';
import { PREDEFINED_APPROVE_REJECT_FORM } from '../form-builder-custom/models/predefined-approve-reject-form';
import { FormBuilderColumnsNumComponent } from '../form-builder-items/form-builder-columns-num/form-builder-columns-num.component';
import { FormBuilderPrimaryKeyComponent } from '../form-builder-items/form-builder-primary-key/form-builder-primary-key.component';

declare var require: any

@Injectable()
export class FormBuilderElementsService implements OnDestroy {

  ngOnDestroy(): void {
  }

  //Table values to show
  tableValues

  // Custom
  predefinedPageBuilder: any[] = JSON.parse(JSON.stringify(PREDEFINED_APPROVE_REJECT_FORM));
  // Custom

  public pageBuilder = [];

  constructor(private cdr: ChangeDetectorRef, public dialog: MatDialog, private route: ActivatedRoute) { }

  async pasteContent() {
    try {
      const clipboardItems = await (navigator.clipboard as any).read();

      let text = null;
      let htmlText = null;

      for (const clipboardItem of clipboardItems) {
        for (const type of clipboardItem.types) {
          text = await clipboardItem.getType('text/html');
          htmlText = await text.text();
        }
      }

      return htmlText

    } catch (err) {
      return "";
    }
  }

  externalDataDrop(event: any) {
    return {
      id: getUniqueId(4),
      html: event.dataTransfer?.getData("text/html"),
      dndType: "droppableFormat"
    }
  }

  regenerateIds(event: any) {
    //regenerate ids of rows and columns
    event.data.id = getUniqueId(4);

    event.data.children.map((c: any) => {
      c.id = getUniqueId(4);
    })

    event.data.firstDrop = false;
  }


  async customNumberOfColumnsDialog() {
    const dialogRef = this.dialog.open(FormBuilderColumnsNumComponent, {
      width: '25rem'
    });

    const row = {
      type: "row",
      class: 'flex row-indicator',
      allowedTypes: ['columns'],
      children: [],
      id: null,
      firstDrop: true,
      dndType: "row",
      disableDropzone: true
    }

    const colsNumber = await dialogRef.afterClosed().toPromise();

    if (!colsNumber) return null;

    for (let i = 0; i < colsNumber; i++) {
      row.children.push({
        id: null,
        type: "col",
        class: "min-h-10 col-indicator",
        children: [],
        allowedTypes: ['droppableFormat', 'row'],
        dndType: "columns",
      })
    }

    return row;
  }

  async getPrimaryKeyDialog() {
    const dialogRef = this.dialog.open(FormBuilderPrimaryKeyComponent, {
      width: '25rem',
      data: {
        routeId: this.route.snapshot.paramMap.get('id')
      }
    });

    const primaryKeyContent = await dialogRef.afterClosed().toPromise();

    const pmContent = {
      id: null,
      "type": "primary-key",
      "elementType": "primaryKey",
      "className": "w-full",
      dndType: "droppableFormat",
      primaryKeyContent: primaryKeyContent
    }

    if(!primaryKeyContent) return null;

    return pmContent;
  }


  async onDrop(event: DndDropEvent, list?: any[]) {

    if (event.data?.firstDrop && event.data.customColumnNumber) {
      const generatedData = await this.customNumberOfColumnsDialog();

      if (!generatedData) return;

      event.data = generatedData;
    }

    if(event.data?.firstDrop && event.data.primaryKey) {

      const getPrimary = await this.getPrimaryKeyDialog();

      if(!getPrimary) return;

      event.data = getPrimary;
    }

    if (event.data?.firstDrop) {
      this.regenerateIds(event);
      delete event.data.firstDrop
    }

    if (event.data?.firstDropForm) {
      event.data.id = getUniqueId(4);
      delete event.data.firstDropForm
    }


    if (event.isExternal)
      event.data = this.externalDataDrop(event.event);


    if (list
      && (event.dropEffect === "copy"
        || event.dropEffect === "move")) {

      let index = event.index;

      if (typeof index === "undefined") {

        index = list.length;
      }

      list.splice(index, 0, event.data);
    }
  }

  onDragged(item: any, list: any[], effect: DropEffect) {
    if (effect === "move") {

      const index = list.indexOf(item);
      list.splice(index, 1);
    }
  }

  recursiveRemove(arr: any, id: any) {
    return arr
      .filter((el: any) => el.id !== id)
      .map((el: any) => {
        if (!el.children || !Array.isArray(el.children)) return el;
        el.children = this.recursiveRemove(el.children, id);
        return el;
      });
  }

  //DELETING
  deleteElementById(pageBuilder: any, item: any) {
    //deleting element from page builder 
    //if it's first level then we are removing element by foreach
    let insideChildrenArray = true;

    pageBuilder.forEach((element: any, index: any) => {
      if (element.id === item.id) {

        insideChildrenArray = false;

        pageBuilder.splice(index, 1);

      }
    });
    //nested level removal
    if (insideChildrenArray)
      this.recursiveRemove(pageBuilder, item.id)
  }

  stringToHTML = function (str) {
    var dom = this.document.createElement('div');
    dom.innerHTML = str;
    return dom;
  };

  resetApproveRejectContent(): void {
    this.predefinedPageBuilder = PREDEFINED_APPROVE_REJECT_FORM;
  }
}
