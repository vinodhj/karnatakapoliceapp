import { TestBed } from '@angular/core/testing';

import { FormBuilderDownloadService } from './form-builder-download.service';

describe('FormBuilderDownloadService', () => {
  let service: FormBuilderDownloadService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FormBuilderDownloadService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
