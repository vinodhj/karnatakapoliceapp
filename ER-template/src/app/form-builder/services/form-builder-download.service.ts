import { Injectable } from '@angular/core';
import { HttpService } from 'src/@exai/services/http.service';
import { HtmlToDocxService } from 'src/app/shared/services/html-to-docx.service';
import { returnAllFormControls } from 'src/app/utils/randomNumber';

import { FormBuilderDownloadHelpersService } from './form-builder-download-helpers.service';

@Injectable({
  providedIn: 'root'
})
export class FormBuilderDownloadService {

  constructor(private httpService: HttpService,private HtmlToDocxService: HtmlToDocxService, private formBuilderHelpers: FormBuilderDownloadHelpersService) { }

  generateDataForDownload(pageBuilder: any, formValues: any, type: any, fileName = null) {
    const allFormControls = returnAllFormControls(pageBuilder);
    //formatted form elements
    const groupedElements = this.formBuilderHelpers.makeGroupsFromFormElements(allFormControls);

    const formElements = this.formBuilderHelpers.makeVerticalGroupElements(groupedElements);

    var wrapper = this.formBuilderHelpers.createElement('table', `${ type === 'word' ? '' : 'letter-spacing: 1px;' }`, {
      cellspacing: 0,
      cellpadding: 7,
    });

    formElements.map((formElement, index) => {

      if (!formElement.group && !formElement.verticalGrouped) {

        if(formElement.elementType == "textEditor")
          wrapper.appendChild(this.generateHTMLText(formElement, index));

        if (formElement.type == 'checkbox' || formElement.type == 'radio') {
          wrapper.appendChild(this.generateCheckbox(formElement, index).topTitle);
          wrapper.appendChild(this.generateCheckbox(formElement, index).tableWrapper);

          return;
        }

        if(formElement.type === 'table') {
          wrapper.appendChild(this.generateHTMLTable(formElement, index).topTitle);
          wrapper.appendChild(this.generateHTMLTable(formElement, index).tableWrapper);

          return;
        }

        if(formElement.type === 'image') {

          wrapper.appendChild(this.generateDroppedImage(formElement, index))

          return;
        }

        if(formElement.type === 'file') {

          wrapper.appendChild(this.generateFileUploadImages(formElement, index).topTitle);
          wrapper.appendChild(this.generateFileUploadImages(formElement, index).tableWrapper);

          return;
        }

        if(formElement.type === 'table-form') {

          // wrapper.appendChild(this.generateTableForm(formElement, index).topTitle);
          wrapper.appendChild(this.generateTableForm(formElement, index).tableWrapper);

          return;
        }

        if(formElement.type === 'excel-form') {

          wrapper.appendChild(this.generateRowWithLink(formElement, index));

          return;
        }

        wrapper.appendChild(this.generateRow(formElement, index));
      }

      if (formElement.group) {
        const group = this.generateGroup(formElement, index);
        if(group.topTitle && group.content) {

          wrapper.appendChild(group.topTitle)
          wrapper.appendChild(group.content);

          return;
        }

        wrapper.appendChild(group);

      }

      if (formElement.verticalGrouped)
        wrapper.appendChild(this.generateVerticalGroup(formElement, index));
    });

    const allrows = wrapper.querySelectorAll('tr')
    const allTables = wrapper.querySelectorAll('tr')

    allrows.forEach(element => {
      element.style.cssText = `
      page-break-after: always;
      `
    });

    allTables.forEach(element => {
      element.style.cssText = `4
      page-break-after: always;
      `
    })

    if(type === 'word')
    this.HtmlToDocxService.generateDocxFromHTML(wrapper.outerHTML, fileName);

    if(type === 'pdf')
    this.httpService.generatePdfFromHTML(wrapper.outerHTML, fileName);
  }


  generateHTMLText(element, number) {

    let tableWrapper = this.formBuilderHelpers.createElement('tr', ``, {
      colspan: 3
    });

    const td = this.formBuilderHelpers.createElement('td', ``, {
      colspan: 3
    })

    td.innerHTML = element.html;

    tableWrapper.appendChild(td);

    return tableWrapper;

  }

  generateTableForm(element, number) {

    var topTitle = this.formBuilderHelpers.createElement('tr', ``, {
      colspan: 3
    });

    topTitle.innerHTML = this.generateTopTitle(element, number);

    //generating table
    let mathOperationsColumnVisible = this.httpService.roleId == 4        
    const matchFields = {}

    //match column with label
    element.tableValue?.columns?.forEach(column => {
      if (column.type === 'mathOperations' && mathOperationsColumnVisible) {            
        matchFields[column.property] = column.label
      }

      if (column.property != 'firstColumn' && column.property != 'actionsColumn' && column.type !== 'mathOperations') {
        matchFields[column.property] = column.label
      }
    })

    // let tableData = this.htmlToDocxService.generateTableWithMerge(formattedTableValues)
    let tableData = this.formBuilderHelpers.formatTableForm(element.tableValue, matchFields);

    let tableWrapper = this.formBuilderHelpers.createElement('tr', ``, {
      colspan: 3
    });

    const td = this.formBuilderHelpers.createElement('td', ``, {
      colspan: 3
    })

    td.appendChild(tableData);

    tableWrapper.appendChild(td);

    return {topTitle, tableWrapper};
  }

  generateFileUploadImages(element, number) {
    var topTitle = this.formBuilderHelpers.createElement('tr', ``, {
      colspan: 3
    });

    topTitle.innerHTML = this.generateTopTitle(element, number);

    let tableWrapper = this.formBuilderHelpers.createElement('tr', ``, {
      colspan: element.value?.length
    });

    const td = this.formBuilderHelpers.createElement('td', `overflow-wrap: break-word;`, {
      colspan: 3,
      width: 700
    });

   element.value?.forEach(el => {
    //if it's image create img element and attach source
    if(!el.noPreview) {

      const image = this.formBuilderHelpers.createElement('img', `overflow-wrap: break-word;`);
  
      image.src = el.imageContent;
  
      td.appendChild(image);


    }


    //if it's document that has no preview and it has url then add that url
    if(el.noPreview && el.url) {

      const urlElement = this.formBuilderHelpers.createElement('a', `overflow-wrap: break-word;color: green;`);

      urlElement.innerText = el.name;

      urlElement.href = el.url

      td.appendChild(urlElement)

      const span = this.formBuilderHelpers.createElement('span');

      //more spaces for the link element
      span.innerHTML = `&nbsp;&nbsp;&nbsp;`;

      td.appendChild(span);
    }

    
    const span = this.formBuilderHelpers.createElement('span');

    span.innerHTML = `&nbsp;`;

    td.appendChild(span);
  });

  tableWrapper.appendChild(td);

    return { topTitle, tableWrapper };
  }

  generateDroppedImage(element, number) {
    let tableWrapper = this.formBuilderHelpers.createElement('tr');

    let td = this.formBuilderHelpers.createElement('td', `text-align: center;`, {
      colspan: 3,
      width: element?.imageData?.width,
      height: Number(element?.imageData?.height) + 100
    })

    let image = this.formBuilderHelpers.createElement('img', `text-align: center;`, {
      width: element?.imageData?.width,
      height: element?.imageData?.height
    });

    image.src = element?.imageData?.imageContent;

    let imageWrapper = this.formBuilderHelpers.createElement('div', `text-align: center;`);

    imageWrapper.appendChild(image);

    td.appendChild(imageWrapper);

    tableWrapper.appendChild(td);

    return tableWrapper;
  }


  generateHTMLTable(element, number) {
    var topTitle = this.formBuilderHelpers.createElement('tr', ``, {
      colspan: 3
    });

    topTitle.innerHTML = this.generateTopTitle(element, number);

    let tableWrapper = this.formBuilderHelpers.createElement('tr');
    
    let td = this.formBuilderHelpers.createElement('td', ``, {
      colspan: 2
    });

    td.innerHTML = this.generateHTMLTableContent(element);

    td.querySelectorAll('td').forEach(element => {

      element.style.border = `1px solid #000000`;
      element.style.height = "20px";
      element.style.fontSize = '9pt';

      if(element.className.split(' ').includes('suffix') || element.className.split(' ').includes('no-borders')) 
        element.style.border = 'none';

      const p = this.formBuilderHelpers.createElement('p');

      if(element.className.split(' ').includes('no-paragraph'))
        return;

      const elementContent = element.innerHTML;

      element.innerHTML = '';

      p.innerHTML = elementContent;

        element.appendChild(p);
    });

    const table = td.querySelectorAll('table');

    table.forEach(t => {

      t.style.borderCollapse = 'collapse';
      t.setAttribute('align', 'left');
      
    })
    

    tableWrapper.appendChild(this.formBuilderHelpers.createElement('td'))
    tableWrapper.appendChild(td);

    console.log(tableWrapper);

    return { topTitle, tableWrapper }
  }

  generateHTMLTableContent(element) {
    return element.htmlTable;
  }

  generateCheckbox(element, number) {
    var topTitle = this.formBuilderHelpers.createElement('tr', ``, {
      colspan: 3
    });

    topTitle.innerHTML = this.generateTopTitle(element, number);

    let tableWrapper = this.formBuilderHelpers.createElement('tr');
    
    let td = this.formBuilderHelpers.createElement('td', ``, {
      colspan: 2
    });

    td.appendChild(this.generateCheckboxContent(element));
    tableWrapper.appendChild(this.formBuilderHelpers.createElement('td'))
    tableWrapper.appendChild(td);
    

    return { topTitle, tableWrapper };
  }

  generateCheckboxContent(element) {
    let formatSelections = [];

    const table = this.formBuilderHelpers.createElement('table', `border-collapse: collapse;`, { width: 670 })

    const tableRow = this.formBuilderHelpers.createElement('tr');

    element.values?.map((value, index) => {

      formatSelections.push({label: `${index + 1}. ${value.label}`, value: value.value})

    })

    formatSelections.map(selection => {


      let checkBox = '';

      //if value is selected then check box inside exported document
      if(selection.value === element.value)
        checkBox = 'X';

      //if element is type checkbox then element.value is array so we need to go through that array and to match values
      if(element.type === 'checkbox') {

        element.value?.map(checkedValue => {

          if(selection.value === checkedValue.value)
            checkBox = 'X';

        })

      }

      tableRow.innerHTML += `
      <td height="3" style="border-collapse: collapse;background: transparent; border: 1px solid #000000; padding: 4px 10px">
      <p  style="font-size: 9pt">
      ${selection.label}
      </p></td>
      <td style="border-collapse: collapse;background: transparent; border: 1px solid #000000; padding: 4px 10px" height="3" width="30"><p style="font-size: 9pt">${checkBox}</p></td>
      `

    })

    table.appendChild(tableRow);

    return table;
  }

  generateTopTitle(element, number) {
    return `<td width="30" style="vertical-align: top;border-collapse: collapse;"><p style="font-size: 9pt;margin-top: 3.5px;">
    ${number + 1}.
    </p></td>
    <td colspan="2" width="171" style="vertical-align: top;" ><p style="font-size: 9pt;margin-top: 3.5px;">
    ${element.label || element.groupName}
    </p></td>`
  }

  generateVerticalGroup(groupedElement, number) {


    let formattedGroup = this.formBuilderHelpers.createElement('div');

    const letters = groupedElement.verticalGroupName.split('');

    groupedElement.children.map((element, number) => {

      if (element.group) {
        formattedGroup.appendChild(this.generateGroup(element, letters[0]));
        letters.shift();
      }

      if (!element.group) {
        formattedGroup.appendChild(this.generateRow(element, letters[0]));
        letters.shift();
      }
    })

    letters.length ? letters.map(letter => {
      const div = this.formBuilderHelpers.createElement('tr');

      div.innerHTML += `
        <td style="vertical-align: top;border-collapse: collapse;border-left: 1px solid #000000;border-right: 1px solid #000000;text-align: center;" 
    width="30"><p style="font-size: 9pt;margin-top: 3.5px;">
    ${letter.toUpperCase()}
    </p></td>
      `
      formattedGroup.appendChild(div);
    }) : null;

    //add top border for first letter and bottom border for last letter of vertical group
    formattedGroup.querySelector('td').style.borderTop = '1px solid #000000';
    formattedGroup.lastChild.querySelector('td').style.borderBottom = '1px solid #000000';

    return formattedGroup;
  }

  generateSideTitle(element, number) {

    //here is also a logic for vertical align rows
    //they need to have letters instead of numbers

    let textToShow = ' ';

    if (number)
      textToShow = isNaN(number) ? number.toUpperCase() : `${number + 1}.`

    if (number == 0)
      textToShow = '1.';

    return `<td style="vertical-align: top;border-collapse: collapse;
    ${isNaN(number) ? `border-left: 1px solid #000000;border-right: 1px solid #000000;text-align: center;` : ``}" 
    width="30"><p style="font-size: 9pt;margin-top: 3.5px;">
    ${textToShow}
    </p></td>
    <td style="vertical-align: top;"  width="171"><p style="font-size: 9pt;margin-top: 3.5px;">
    ${element.groupName || element.label}
    </p></td>`

  }

  generateFieldData(element, number) {

    let elementValue;

    if(element.value)
      elementValue = element.value.toString();

    return `<td  width="13" height="4" style="border-collapse: collapse;background: transparent; border: 1px solid #000000; padding: 4px 10px">
    <p  style="font-size: 9pt">
    ${elementValue?.length && elementValue[number] ? elementValue[number].toUpperCase() : "&nbsp;&nbsp;"}
    </p></td>`
  }

  generateGroupTable(groupElement, index) {
    var div = this.formBuilderHelpers.createElement('tr');

    var divTable = this.formBuilderHelpers.createElement('tr');

    div.innerHTML = this.generateTopTitle(groupElement, index);
    let tableContent = this.generateGroupFieldTableContent(groupElement);

    divTable.innerHTML = `<td></td><td colspan="2">${tableContent}</td>`;

    return {
      topTitle: div,
      content: divTable
    }
  }

  generateGroup(groupElement, index) {

    if(groupElement.children.some(element => element.type === 'table')) {
      return this.generateGroupTable(groupElement, index);
    }

    var div = this.formBuilderHelpers.createElement('tr');

    div.innerHTML = this.generateSideTitle(groupElement, index);

    let tableContent = this.generateGroupFieldContent(groupElement);

    div.innerHTML += `<td>${tableContent}</td>`
    return div;
  }



  generateMultipleColumns(cols) {

    let allowedNumberOfBoxesInOneRow = 20;

    let tableContent = this.formBuilderHelpers.createElement('table', `border-collapse: collapse`);

    let header = this.formBuilderHelpers.createElement('thead');

    cols.map(formElement => {

      //HERE WE generate thead for group columns

      if (!formElement) return;

      //if cols number of a field is 2 and formelement next to this element is 1
      //we need to return without changing
      if (formElement.colsNumber == 1) return;

      formElement.alreadyAdded = true;

      let th = this.formBuilderHelpers.createElement(`td`, null, {
        align: 'center',
        colspan: allowedNumberOfBoxesInOneRow / formElement.colsNumber
      });

      let thContent = this.formBuilderHelpers.createElement('p', `font-size: 9pt`)

      thContent.innerText = formElement.label;

      th.appendChild(thContent);

      header.appendChild(th);

      tableContent.appendChild(header);
    })

    let rowHtml = this.formBuilderHelpers.createElement('tr');

    cols.map(formElement => {

      if (!formElement) return;

      let allowedNumberOfBoxesInOneRow = Math.ceil(20 / formElement.colsNumber);

      formElement.colsNumber === 3 ? allowedNumberOfBoxesInOneRow - 1 : allowedNumberOfBoxesInOneRow;

      const boxSizeLength = Array.from(Array(allowedNumberOfBoxesInOneRow).keys());

      boxSizeLength.map(number => {

        let tableData = this.generateFieldData(formElement, number);

        rowHtml.insertAdjacentHTML('beforeend', tableData);
      });
    })

    tableContent.appendChild(rowHtml);

    return tableContent.outerHTML;
  }

  generateGroupFieldTableContent(groupElement) {
    
    let tableContentGroup = ``;

    groupElement.children.map((formElement, index) => {

      //we are checking here if field has cols number greater than 1
      //if it has then generatemultiple columns for that 
      if (formElement?.colsNumber > 1) {

        if (formElement.alreadyAdded) return;

        const cols = [];
        //get elements next to this field depending of cols number
        for (let i = 0; i < formElement.colsNumber; i++) {
          cols.push(groupElement.children[index + i]);
        }

        tableContentGroup += this.generateMultipleTableColumns(cols);

        return;
      }

      tableContentGroup += this.generateFieldContentWithHeader(20, formElement, true).outerHTML;

    })

    groupElement.children.map(formElement => {
      formElement.alreadyAdded = false;
    })

    return tableContentGroup;

  }

  generateMultipleTableColumns(cols) {

    let tableContent = this.formBuilderHelpers.createElement('table', `border-collapse: separate;width: 100%;`);

    let header = this.formBuilderHelpers.createElement('thead', `width: 100%;`);

    cols.map(formElement => {

      //HERE WE generate thead for group columns

      if (!formElement) return;

      //if cols number of a field is 2 and formelement next to this element is 1
      //we need to return without changing
      if (formElement.colsNumber == 1) return;

      formElement.alreadyAdded = true;

      let th = this.formBuilderHelpers.createElement(`td`, null, {
        align: 'center'
      });

      let thContent = this.formBuilderHelpers.createElement('p', `font-size: 9pt`)

      thContent.innerText = formElement.label;

      th.appendChild(thContent);

      header.appendChild(th);

      tableContent.appendChild(header);
    })

    let rowHtml = this.formBuilderHelpers.createElement('tr');

    cols.map((formElement, index) => {

      if (!formElement) return;


      let tableData = `<td style="width: ${100 / formElement.colsNumber}%;" class="no-borders not-included">${formElement.htmlTable}</td>`


      rowHtml.insertAdjacentHTML('beforeend', tableData);

      rowHtml.querySelectorAll('td').forEach((td, index) => {

        if(td.className.split(' ').includes('not-included')) {
            
          if(index > 1)
            td.style.paddingLeft = '50px';

          
          return
        };

        td.style.border = `1px solid #000000`;
        td.style.height = "20px";
        td.style.fontSize = '9pt';
  
        if(td.className.split(' ').includes('suffix') || td.className.split(' ').includes('no-borders')) 
          td.style.border = 'none';
  
        const p = this.formBuilderHelpers.createElement('p');
  
        if(td.className.split(' ').includes('no-paragraph'))
          return;
  
        const elementContent = td.textContent;
  
        td.textContent = '';
  
        p.innerHTML = elementContent;
  
        td.appendChild(p);
      });

      rowHtml.querySelectorAll('table').forEach(element => {
        element.style.borderCollapse = 'collapse';
      });
    })

    tableContent.appendChild(rowHtml);

    return tableContent.outerHTML;
  }

  generateGroupFieldContent(groupElement) {

    let tableContentGroup = ``;

    groupElement.children.map((formElement, index) => {

      //we are checking here if field has cols number greater than 1
      //if it has then generatemultiple columns for that 
      if (formElement?.colsNumber > 1) {

        if (formElement.alreadyAdded) return;

        const cols = [];
        //get elements next to this field depending of cols number
        for (let i = 0; i < formElement.colsNumber; i++) {
          cols.push(groupElement.children[index + i]);
        }

        tableContentGroup += this.generateMultipleColumns(cols);

        return;
      }

      tableContentGroup += this.generateFieldContentWithHeader(20, formElement, true).outerHTML;

    })

    groupElement.children.map(formElement => {
      formElement.alreadyAdded = false;
    })

    return tableContentGroup;
  }

  generateRow(formElement, index) {
    var div = this.formBuilderHelpers.createElement('tr');

    div.innerHTML = this.generateSideTitle(formElement, index);

    let tableContent = this.generateFieldContentWithHeader(19, formElement, false).outerHTML;

    div.innerHTML += `<td>${tableContent}</td>`

    return div;
  }

  generateRowWithLink(formElement, index) {
    var div = this.formBuilderHelpers.createElement('tr');

    div.innerHTML = this.generateSideTitle(formElement, index);

    let tableContent = this.formBuilderHelpers.createElement('a');
    tableContent.href = formElement.tableValue.excelUrl;
    
    tableContent.innerHTML = 'Download Excel document';

    div.innerHTML += `<td>${tableContent.outerHTML}</td>`

    return div;
  }

  generateFieldContentWithHeader(allowedNumberOfBoxesInOneRow = 19, formElement, hasHeader = false) {
    //format date value
    if (formElement.type === 'date') {

      formElement.dateValue = this.formBuilderHelpers.dateParser(formElement.value);
      allowedNumberOfBoxesInOneRow = 10;

    }

    let tableContent = this.formBuilderHelpers.createElement('table', `border-collapse: collapse`);

    const boxSizeLength = Array.from(Array(formElement.validators?.maxLength || allowedNumberOfBoxesInOneRow).keys());

    //we can have 21 columns inside one row
    //after that text will break
    //so we need to generate row for each 21 column
    let rows = [];
    for (let i = 0; i < boxSizeLength.length; i += allowedNumberOfBoxesInOneRow) {
      rows.push(boxSizeLength.slice(i, i + allowedNumberOfBoxesInOneRow));
    }

    if (hasHeader) {
      let header = this.formBuilderHelpers.createElement('thead');

      let th = this.formBuilderHelpers.createElement(`td`, null, {
        align: 'center',
        colspan: rows[0].length
      });

      let thContent = this.formBuilderHelpers.createElement('p', `font-size: 9pt`)

      thContent.innerText = formElement.label;

      th.appendChild(thContent);

      header.appendChild(th);

      tableContent.appendChild(header);
    }

    rows.map(row => {

      let rowHtml = this.formBuilderHelpers.createElement('tr');

      row.map(number => {

        let tableData = this.generateFieldData(formElement, number);

        rowHtml.insertAdjacentHTML('beforeend', tableData);
      });

      tableContent.appendChild(rowHtml);
    });

    tableContent.appendChild(this.formBuilderHelpers.createElement('tr'));

    return tableContent;
  }


}
