import { TestBed } from '@angular/core/testing';

import { FormBuilderPdfPreviewService } from './form-builder-pdf-preview.service';

describe('FormBuilderPdfPreviewService', () => {
  let service: FormBuilderPdfPreviewService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FormBuilderPdfPreviewService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
