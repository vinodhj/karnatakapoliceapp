import { Injectable } from '@angular/core';
import { NgxIndexedDBService } from 'ngx-indexed-db';
import { Observable, of } from 'rxjs';
import { concatMap, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FormBuilderIndexeddbService {

  storeKey = 'formTemplates';

  constructor(private dbService: NgxIndexedDBService) { }

  initializeDataIntoIndexedDb(formId, content): Observable<any> {

    return this.dbService.getByIndex(this.storeKey, 'formId', formId).pipe(
      concatMap((result: any) => {

        if ( result || ( result && JSON.parse(result.content).rows.length > 0)) return of(result.content);
        return this.dbService.update(this.storeKey, {
          formId,
          content: content.toString()
        }).pipe(
          map(() => null)
        )

      })
    )
  }

  initializeRecordIntoIndexedDb(formId, content): Observable<any> {    
    
    return this.dbService.getByIndex(this.storeKey, 'formId', formId).pipe(
      concatMap((data: any) => {

        if (data) return of(data.content)

        return this.dbService.update(this.storeKey, {
          formId,
          content: content
        }).pipe(
          map(() => content)
        )
      })
    )
  }

  updateDataInsideIndexedDb(formId, content) {    
     
    this.dbService.getByIndex(this.storeKey, 'formId', formId).pipe(
      concatMap((result: any) => {
        
        if(!result) return of();

        return this.dbService.update(this.storeKey, {
          id: result.id,
          formId,
          content,
        })

      })
    )
    .subscribe()
  }

  removeDataInsideIndexedDb(formId) {
    this.dbService.getByIndex(this.storeKey, 'formId', formId).pipe(
      concatMap((result: any) => {

        if(!result) return of();

        return this.dbService.deleteByKey(this.storeKey, result.id);

      })
    )
    .subscribe()
  }

}
