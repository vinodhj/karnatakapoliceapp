import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { Editor } from 'ngx-editor';

@Component({
  selector: 'app-form-builder-approve-reject-element-html',
  templateUrl: './form-builder-element-html.component.html',
  styleUrls: ['./form-builder-element-html.component.scss']
})

export class FormBuilderApproveRejectElementHtmlComponent implements OnInit, OnDestroy {

  @Input() item!: any;
  editor: any;
  @Output(`removeEmitted`) removeEmitted = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
    this.editor = new Editor();
  }

  ngOnDestroy(): void {
    if (this.editor)
      this.editor.destroy();
  }
}