import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormBuilderApproveRejectElementHtmlComponent } from './form-builder-element-html.component';

describe('FormBuilderApproveRejectElementHtmlComponent', () => {
  let component: FormBuilderApproveRejectElementHtmlComponent;
  let fixture: ComponentFixture<FormBuilderApproveRejectElementHtmlComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FormBuilderApproveRejectElementHtmlComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormBuilderApproveRejectElementHtmlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
