import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, ValidationErrors } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { distinctUntilChanged } from 'rxjs/operators';
import { HttpService } from 'src/@exai/services/http.service';
import { returnAllFormControls } from 'src/app/utils/randomNumber';
import { JsonFormControls } from '../../models/JsonForm';
import { FormBuilderReactiveService } from '../../services/form-builder-reactive.service';
import { PREDEFINED_APPROVE_REJECT_FORM } from '../models/predefined-approve-reject-form';

@Component({
  selector: 'app-form-builder-approve-reject-result',
  templateUrl: './form-builder-result.component.html',
  styleUrls: ['./form-builder-result.component.scss']
})

export class FormBuilderApproveRejectResultComponent implements OnInit {

  generatedForm: FormGroup = this.formBuilder.group({});
  loadingData: boolean = true;
  formTemplate: any[] = PREDEFINED_APPROVE_REJECT_FORM;
  pageTemplate: any;
  allForms: any;
  title: string = ``;
  buttonTitle: string = ``;
  save: boolean = true;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private formBuilder: FormBuilder,
    private httpService: HttpService,
    private formBuilderReactive: FormBuilderReactiveService,
    private dialogRef: MatDialogRef<FormBuilderApproveRejectResultComponent>
  ) { }

  ngOnInit(): void {
    this.getTemplateApproveRejectContent();
  }

  cancel(): void {
    this.save = false;
    this.dialogRef.close(null);
  }

  switchTitle(): void {
    switch (this.data?.action) {
      case `approve`: this.title = `Reason for approval`; this.buttonTitle = `Approve`; break;
      case `reject`: this.title = `Reason for rejection`; this.buttonTitle = `Reject`; break;
      case `submit`: this.title = `Submit comment`; this.buttonTitle = `Submit`; break;
      default: break;
    }
  }

  getTemplateApproveRejectContent(): void {
    if (this.data === null || this.data === undefined)
      return;
    this.httpService.templateGetApproveRejectContent(this.data?.formId).subscribe(
      response => {
        if (response !== null && response !== undefined)
          this.formTemplate = response as any[];
        console.log(this.formTemplate);
        this.prepareFormTemplate();
      }
    )
  }

  prepareFormTemplate(): void {
    this.pageTemplate = ((this.formTemplate[0] as any | null)?.children[1] as any | null)
      ?.children?.filter(element => element?.actions?.includes(this.data?.action));
    console.log(this.pageTemplate);
    if (this.pageTemplate !== null && this.pageTemplate !== undefined) {
      this.initialize();
      this.loadingData = false;
    }
  }

  initialize(): void {
    let allFormControls: any = returnAllFormControls(this.pageTemplate);
    const filteredFormControls = allFormControls.filter(c => c.elementType === 'form');
    this.allForms = filteredFormControls;
    this.createForm(filteredFormControls);
    this.generatedForm.valueChanges.pipe(
      distinctUntilChanged()
    ).subscribe(() => {
      const rawFormValues = this.generatedForm.getRawValue();
      Object.entries(rawFormValues).forEach(([key, value]) => {
        this.addDisableConditions(allFormControls, key);
        this.addHideConditions(allFormControls, key);
      });
    });
    this.getFormValidationErrors();
    this.generatedForm.updateValueAndValidity();
    this.switchTitle();
  }

  addHideConditions(allFormControls: any, key: any) {
    const formItem = allFormControls.find((c: any) => c.id === key);
    if (formItem.hideCondition?.field)
      this.getFormField(formItem.hideCondition.field.id)?.value === formItem.hideCondition.value ?
        (this.enableFormField(formItem.id), formItem.hide = false) :
        (this.disableFormField(formItem.id), formItem.hide = true)
  }

  addDisableConditions(allFormControls: any, key: any) {
    const formItem = allFormControls.find((c: any) => c.id === key);
    if (formItem.disabledCondition?.field)
      this.getFormField(formItem.disabledCondition.field.id)?.value === formItem.disabledCondition.value ?
        this.disableFormField(formItem.id) :
        this.enableFormField(formItem.id)
  }

  getFormField(formControlName: any) {
    return this.generatedForm.get(formControlName);
  }

  disableFormField(id: any) {
    this.generatedForm.get(id)?.disable({ emitEvent: false, onlySelf: true })
  }

  enableFormField(id: any) {
    this.generatedForm.get(id)?.enable({ emitEvent: false, onlySelf: true })
  }

  createForm(controls: JsonFormControls[]) {
    for (const control of controls) {
      const validatorsToAdd = this.formBuilderReactive.returnValidators(control);
      this.generatedForm.addControl(
        control.id,
        this.formBuilder.control(control.value, validatorsToAdd)
      );
    }
  }

  submitForm() {
    if (this.save)
      this.dialogRef.close({
        data: this.data,
        formData: this.getApproveRejectSubmitFormData()
      });
  }

  getApproveRejectSubmitFormData(): any {
    return {
      approveReason: this.isControlValueNullOrUndefinedOrWhiteSpace(this.generatedForm.get(`approveReason`)?.value) ?
        null : this.generatedForm.get(`approveReason`)?.value,
      rejectReason: this.isControlValueNullOrUndefinedOrWhiteSpace(this.generatedForm.get(`rejectReason`)?.value) ?
        null : this.generatedForm.get(`rejectReason`)?.value,
      comment: this.isControlValueNullOrUndefinedOrWhiteSpace(this.generatedForm.get(`comment`)?.value) ?
        null : this.generatedForm.get(`comment`)?.value
    }
  }

  getFormValidationErrors() {
    Object.keys(this.generatedForm.controls).forEach(key => {
      const controlErrors: ValidationErrors = this.generatedForm.get(key).errors;
      if (controlErrors != null) {
        Object.keys(controlErrors).forEach(keyError => {
          console.log(this.allForms.filter(c => c.id === key))
          console.log('Key control: ' + key + ', keyError: ' + keyError + ', err value: ', controlErrors[keyError]);
        });
      }
    });
  }

  isControlValueNullOrUndefinedOrWhiteSpace(value: any): boolean {
    return value === undefined || value === null || value === ``;
  }
}