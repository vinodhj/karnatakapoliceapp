import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormBuilderApproveRejectResultComponent } from './form-builder-result.component';

describe('FormBuilderApproveRejectResultComponent', () => {
  let component: FormBuilderApproveRejectResultComponent;
  let fixture: ComponentFixture<FormBuilderApproveRejectResultComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FormBuilderApproveRejectResultComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormBuilderApproveRejectResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
