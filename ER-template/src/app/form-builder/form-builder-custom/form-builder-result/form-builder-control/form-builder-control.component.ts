
import { Component, Input, OnInit } from '@angular/core';
import { ControlValueAccessor, FormControl, NG_VALUE_ACCESSOR } from '@angular/forms';
import { distinctUntilChanged } from 'rxjs/operators';
import { JsonFormControls } from 'src/app/form-builder/models/JsonForm';
import { FormBuilderReactiveService } from 'src/app/form-builder/services/form-builder-reactive.service';

@Component({
  selector: 'app-form-builder-approve-reject-control',
  templateUrl: './form-builder-control.component.html',
  styleUrls: ['./form-builder-control.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: FormBuilderControlApproveRejectComponent
    }
  ]
})
export class FormBuilderControlApproveRejectComponent implements OnInit, ControlValueAccessor {
  @Input() itemData: any;


  formControl: FormControl = new FormControl();
  _isDisabled!: boolean;

  constructor(private formBuilderReactive: FormBuilderReactiveService) { }

  onChange = (value: any) => { };

  onTouched = () => { };


  writeValue(obj: any): void {
    this.formControl.setValue(obj);
  }
  registerOnChange(fn: any): void {
    this.onChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  addValidators(control: JsonFormControls) {
    const validatorsToAdd = this.formBuilderReactive.returnValidators(control);
    this.formControl.setValidators(validatorsToAdd);
  }

  ngOnInit(): void {
    this.addValidators(this.itemData);

    this.formControl.valueChanges.pipe(
      distinctUntilChanged()
    ).subscribe(value => {
      this.onChange(value);
    })
  }

  setDisabledState(isDisabled: boolean): void {
    if (isDisabled) {
      this.formControl.disable();
      this.disabledCheckbox = true;
    } else {
      this.disabledCheckbox = false;
      this.formControl.enable();
    }
  }

  disabledCheckbox = false;

  //for checkbox field only
  checkboxChanged(event: any) {
    this.onChange(event);
  }
}