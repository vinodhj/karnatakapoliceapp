import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormBuilderControlApproveRejectComponent } from './form-builder-control.component';

describe('FormBuilderControlApproveRejectComponent', () => {
  let component: FormBuilderControlApproveRejectComponent;
  let fixture: ComponentFixture<FormBuilderControlApproveRejectComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormBuilderControlApproveRejectComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormBuilderControlApproveRejectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
