import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormBuilderApproveRejectInputElementsComponent } from './form-builder-input-elements.component';

describe('FormBuilderApproveRejectInputElementsComponent', () => {
  let component: FormBuilderApproveRejectInputElementsComponent;
  let fixture: ComponentFixture<FormBuilderApproveRejectInputElementsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FormBuilderApproveRejectInputElementsComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormBuilderApproveRejectInputElementsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
