import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-form-builder-approve-reject-input-elements',
  templateUrl: './form-builder-input-elements.component.html',
  styleUrls: ['./form-builder-input-elements.component.scss']
})

export class FormBuilderApproveRejectInputElementsComponent {

  @Input() item!: any;
  formElements: any = [];
  @Output(`removeEmitted`) removeEmitted = new EventEmitter();

  constructor() { }

  toggleValue(data: any) { }
}