import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormBuilderApproveRejectElementButtonsComponent } from './form-builder-element-buttons.component';

describe('FormBuilderApproveRejectElementButtonsComponent', () => {
  let component: FormBuilderApproveRejectElementButtonsComponent;
  let fixture: ComponentFixture<FormBuilderApproveRejectElementButtonsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormBuilderApproveRejectElementButtonsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormBuilderApproveRejectElementButtonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
