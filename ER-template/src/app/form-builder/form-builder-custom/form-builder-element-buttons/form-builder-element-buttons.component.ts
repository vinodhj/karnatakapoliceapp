import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-form-builder-approve-reject-element-buttons',
  templateUrl: './form-builder-element-buttons.component.html',
  styleUrls: ['./form-builder-element-buttons.component.scss']
})

export class FormBuilderApproveRejectElementButtonsComponent implements OnInit {

  @Input() item!: any;

  @Output('removeEmitted') removeEmitted = new EventEmitter();

  constructor() { }

  ngOnInit(): void { }

}
