export const PREDEFINED_APPROVE_REJECT_FORM: any[] = [
    {
        type: `row`,
        class: `flex row-indicator`,
        allowedTypes: [
            `columns`
        ],
        children: [
            {
                id: `3db0-ee17-c0e1-206f`,
                type: `col`,
                class: `h-10 col-indicator`,
                children: [],
                allowedTypes: [
                    `droppableFormat`,
                    `row`
                ],
                dndType: `columns`
            },
            {
                id: `7924-ab3a-8a62-2ffc`,
                type: `col`,
                class: `h-10 col-indicator`,
                children: [
                    {
                        type: `text`,
                        icon: `text_fields`,
                        label: `Text`,
                        html: `<h4><em>Approve Configuration</em></h4>`,
                        dndType: `droppableFormat`,
                        className: `w-full`,
                        actions: []
                    },
                    {
                        type: `select`,
                        elementType: `form`,
                        icon: `fact_check`,
                        label: `Select`,
                        dndType: `droppableFormat`,
                        validators: {},
                        description: `Select`,
                        disabledCondition: {},
                        hideCondition: {},
                        placeholder: `Select`,
                        className: `w-full`,
                        values: [
                            {
                                label: `Option 1`,
                                value: `option-1`
                            },
                            {
                                label: `Option 2`,
                                value: `option-2`
                            },
                            {
                                label: `Option 3`,
                                value: `option-3`
                            }
                        ],
                        id: `approveReason`,
                        actions: [`approve`]
                    },
                    {
                        type: `text`,
                        icon: `text_fields`,
                        label: `Text`,
                        html: `<h4><em>Reject Configuration</em></h4>`,
                        dndType: `droppableFormat`,
                        className: `w-full`,
                        actions: []
                    },
                    {
                        type: `select`,
                        elementType: `form`,
                        icon: `fact_check`,
                        label: `Select`,
                        dndType: `droppableFormat`,
                        validators: {},
                        description: `Select`,
                        disabledCondition: {},
                        hideCondition: {},
                        placeholder: `Select`,
                        className: `w-full`,
                        values: [
                            {
                                label: `Option 1`,
                                value: `option-1`
                            },
                            {
                                label: `Option 2`,
                                value: `option-2`
                            },
                            {
                                label: `Option 3`,
                                value: `option-3`
                            }
                        ],
                        id: `rejectReason`,
                        actions: [`reject`]
                    },
                    {
                        type: `text`,
                        icon: `text_fields`,
                        label: `Text`,
                        html: `<h4><em>Comment Configuration</em></h4>`,
                        dndType: `droppableFormat`,
                        className: `w-full`,
                        actions: []
                    },
                    {
                        type: `textarea`,
                        elementType: `form`,
                        icon: `notes`,
                        className: `w-full`,
                        label: `Textarea`,
                        disabledCondition: {},
                        hideCondition: {},
                        dndType: `droppableFormat`,
                        validators: {},
                        id: `comment`,
                        actions: [`approve`, `reject`, `submit`]
                    },
                    {
                        type: `row`,
                        class: `flex row-indicator`,
                        allowedTypes: [
                            `columns`
                        ],
                        children: [
                            {
                                id: `16e5-e0df-9792-4336`,
                                type: `col`,
                                class: `h-10 col-indicator`,
                                children: [
                                    {
                                        type: `cancel`,
                                        elementType: `cancel`,
                                        buttonType: `cancel`,
                                        icon: `smart_button`,
                                        label: `Cancel`,
                                        dndType: `droppableFormat`,
                                        className: `w-1/2`,
                                        parentClass: `example-full-width floatLeft`,
                                        color: `warn`
                                    }
                                ],
                                allowedTypes: [
                                    `droppableFormat`,
                                    `row`
                                ],
                                dndType: `columns`
                            },
                            {
                                id: `c3e2-d8dc-c127-8ea9`,
                                type: `col`,
                                class: `h-10 col-indicator`,
                                children: [
                                    {
                                        type: `button`,
                                        elementType: `button`,
                                        buttonType: `submit`,
                                        icon: `smart_button`,
                                        label: `Submit`,
                                        dndType: `droppableFormat`,
                                        className: `w-1/2`,
                                        parentClass: `example-full-width floatRight`,
                                        color: `primary`
                                    }
                                ],
                                allowedTypes: [
                                    `droppableFormat`,
                                    `row`
                                ],
                                dndType: `columns`
                            }
                        ],
                        id: `4a17-1567-f445-854a`,
                        dndType: `row`,
                        disableDropzone: true,
                        actions: [`approve`, `reject`, `submit`]
                    }
                ],
                allowedTypes: [
                    `droppableFormat`,
                    `row`
                ],
                dndType: `columns`
            },
            {
                id: `f7bb-0a78-5a9c-b77c`,
                type: `col`,
                class: `h-10 col-indicator`,
                children: [],
                allowedTypes: [
                    `droppableFormat`,
                    `row`
                ],
                dndType: `columns`
            }
        ],
        id: `e63f-5d01-a060-f11e`,
        dndType: `row`,
        disableDropzone: true
    }
];