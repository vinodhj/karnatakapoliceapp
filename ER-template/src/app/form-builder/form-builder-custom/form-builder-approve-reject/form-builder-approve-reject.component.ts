import { Component, OnInit } from '@angular/core';
import { FormBuilderElementsService } from '../../services/form-builder-elements.service';

@Component({
  selector: 'app-form-builder-approve-reject',
  templateUrl: './form-builder-approve-reject.component.html',
  styleUrls: ['./form-builder-approve-reject.component.scss']
})

export class FormBuilderApproveRejectComponent implements OnInit {

  activeElement = null;
  predefinedPageBuilder: any[] = this.formBuilderElementsService.predefinedPageBuilder;
  showFormData: boolean = false;

  constructor(
    private formBuilderElementsService: FormBuilderElementsService
  ) { }

  ngOnInit(): void {}
}