import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormBuilderApproveRejectComponent } from './form-builder-approve-reject.component';

describe('FormBuilderApproveRejectComponent', () => {
  let component: FormBuilderApproveRejectComponent;
  let fixture: ComponentFixture<FormBuilderApproveRejectComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormBuilderApproveRejectComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormBuilderApproveRejectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
