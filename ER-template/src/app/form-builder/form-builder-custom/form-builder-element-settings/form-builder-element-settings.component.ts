import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { popoverAnimation } from 'src/@exai/animations/popover.animation';
import { returnAllFormControls } from 'src/app/utils/randomNumber';
import { FormBuilderElementsService } from '../../services/form-builder-elements.service';

@Component({
  selector: 'app-form-builder-element-approve-reject-settings',
  templateUrl: './form-builder-element-settings.component.html',
  styleUrls: ['./form-builder-element-settings.component.scss'],
  animations: [popoverAnimation]
})
export class FormBuilderElementSettingsApproveRejectComponent implements OnInit, OnDestroy {
  @Input() item!: any;
  @Input() pageBuilder!: any;

  disableCondition: boolean = false;
  hideCondition: boolean = false;
  formElements: any;

  get formElementsFiltered() {
    return this.formElements.filter((e: any) => e.id != this.item.id);
  }

  constructor(private formBuilderElementsService: FormBuilderElementsService) {
    this.initializeFieldsDropdown();
  }

  initializeFieldsDropdown() {
    this.formElements = JSON.parse(JSON.stringify(returnAllFormControls(this.formBuilderElementsService.predefinedPageBuilder)));
    
    this.formElements.map(item => {
      item.recursionSkip = true;

      return item;
    })
  }
  

  ngOnInit(): void {
    this.initializeFieldAfterDrag();
  }

  initializeFieldAfterDrag() {
    this.item.disabledCondition?.field ? this.disableCondition = true : false;
    this.item.hideCondition?.field ? this.hideCondition = true : false;
  }

  value: any ={
    label:"",
    value:""
  };

  @Input() fieldsList = [];
  
  selectedField: any;

  //for checkboxes and radio buttons
  addValue(values: any){
    //if there is no value for label don't add
    if(this.value.label === "") return;

    values.push(this.value);
    this.value={label:"",value:""};
  }


  disableConditionChanged(event: any) {
    this.disableCondition = event.checked;

    if(!event.checked) {
      this.item.disabledCondition = {};
    }
  }

  hideConditionChanged(event: any) {
    this.hideCondition = event.checked;

    if(!event.checked) {
      this.item.hideCondition = {};
    }
  }
  ngOnDestroy(): void {
    console.log('hello')
  }

  objectComparisonFunction(value: any, object: any) {
    return value?.id === object?.id;
  }
}
