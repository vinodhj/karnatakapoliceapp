import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormBuilderElementSettingsApproveRejectComponent } from './form-builder-element-settings.component';

describe('FormBuilderElementSettingsApproveRejectComponent', () => {
  let component: FormBuilderElementSettingsApproveRejectComponent;
  let fixture: ComponentFixture<FormBuilderElementSettingsApproveRejectComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FormBuilderElementSettingsApproveRejectComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormBuilderElementSettingsApproveRejectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
