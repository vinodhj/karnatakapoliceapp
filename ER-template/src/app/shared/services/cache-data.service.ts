import { Injectable } from '@angular/core';
import { BehaviorSubject, forkJoin } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { HttpService } from 'src/@exai/services/http.service';

@Injectable({
  providedIn: 'root'
})
export class CacheDataService {

  divisionsSubject = new BehaviorSubject([]);
  divisionsReducedSubject = new BehaviorSubject([]);
  subDivisionsSubject = new BehaviorSubject([]);
  policeStationsSubject = new BehaviorSubject([]);

  constructor(private httpService: HttpService) {

  }

  getDivisionsSubject() {
    return this.divisionsSubject;
  }
  getSubDivisionsSubject() {
    return this.subDivisionsSubject;
  }
  getPoliceStationsSubject() {
    return this.policeStationsSubject;
  }

  getInformationsForCaching() {
    return forkJoin([
      this.httpService.getDivisionsDropdownV1(),
      this.httpService.getSubDivisions(),
      this.httpService.getAllPoliceStationsDropdown()
    ]).pipe(
      map(([divisions, subdivisions, policeStations]) => ({ divisions, subdivisions, policeStations })),
      tap(({ divisions, subdivisions, policeStations }: any) => {
        this.divisionsSubject.next(divisions?.items);
        this.subDivisionsSubject.next(subdivisions);
        this.policeStationsSubject.next(policeStations?.data);
      })
    )
  }


}
