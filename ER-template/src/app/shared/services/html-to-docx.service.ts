import { Injectable } from '@angular/core';
import { asBlob } from 'html-docx-js-typescript';
import { saveAs } from 'file-saver';

declare var require: any;

import * as pdfMake from "pdfmake/build/pdfmake";
import * as pdfFonts from "pdfmake/build/vfs_fonts";
import * as XLSX from 'xlsx-js-style';
import { HttpService } from 'src/@exai/services/http.service';
import { getUniqueId } from 'src/app/utils/randomNumber';
import { DateFormatPipe } from '../advanced-grid-item/pipes/date-format.pipe';
import * as uuid from 'uuid';
import { DatePipe } from '@angular/common';
import jsPDF from 'jspdf'
import 'jspdf-autotable'
import html2pdf from 'html2pdf.js'

const jspdfDoc = new jsPDF()
const htmlToPdfmake = require("html-to-pdfmake");
(pdfMake as any).vfs = pdfFonts.pdfMake.vfs;

@Injectable({
  providedIn: 'root'
})
export class HtmlToDocxService {

  pipe = new DatePipe('en-US'); // Use your own locale

  constructor(private httpService: HttpService, private dateFormat: DateFormatPipe) { }

  async generateDocxFromHTML(data, fileName = null) {

    const now = Date.now();
    const dateFormatted = this.pipe.transform(now, 'medium');

    fileName += '-' + dateFormatted;

    const docx: any = await asBlob(data, {
      margins: {
        right: 500,
        left: 500,
        bottom: 500,
        top: 500
      }
    }
    );

    saveAs(docx, fileName ? fileName : uuid.v4() + '.docx');
  }




  //EXCEL FUNCTION
  autoWidthBasedOnCharacters(json, ws, header = null) {

    const jsonKeys = header ? header : Object.keys(json[0]);

    let objectMaxLength = [];
    for (let i = 0; i < json.length; i++) {
      let value = json[i];
      for (let j = 0; j < jsonKeys.length; j++) {
        if (typeof value[jsonKeys[j]] == "number") {
          objectMaxLength[j] = 10;
        } else {

          const l = value[jsonKeys[j]] ? value[jsonKeys[j]].length : 0;

          objectMaxLength[j] =
            objectMaxLength[j] >= l
              ? objectMaxLength[j]
              : l;
        }
      }

      let key = jsonKeys;
      for (let j = 0; j < key.length; j++) {
        objectMaxLength[j] =
          objectMaxLength[j] >= key[j].length
            ? objectMaxLength[j]
            : key[j].length;
      }
    }

    const wscols = objectMaxLength.map(w => { return { width: w } });

    ws["!cols"] = wscols;

    //ADDING STYLES
    for (let i in ws) {
      if (typeof (ws[i]) != "object") continue;

      ws[i].s = { // styling for all cells
        font: {
          name: "arial"
        },
        alignment: {
          vertical: "center",
          horizontal: "center",
          wrapText: '1', // any truthy value here
        },
        border: {
          right: {
            style: "thin",
            color: "000000"
          },
          left: {
            style: "thin",
            color: "000000"
          },
          bottom: {
            style: "thin",
            color: "000000"
          },
          top: {
            style: "thin",
            color: "000000"
          },
        }
      };
    }
  }

  generateExcelFromHTML(data, formattedJsonData, fileName = null) {

    data.querySelectorAll('td').forEach(element => {
      element.style.background = '#000';
    })


    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(data);

    //   ws.s = { // styling for all cells
    //     font: {
    //         name: "arial"
    //     },
    //     alignment: {
    //         vertical: "center",
    //         horizontal: "center",
    //         wrapText: '1', // any truthy value here
    //     },
    //     border: {
    //         right: {
    //             style: "thin",
    //             color: "000000"
    //         },
    //         left: {
    //             style: "thin",
    //             color: "000000"
    //         },
    //     }
    // };


    this.autoWidthBasedOnCharacters(formattedJsonData, ws);

    const wb: XLSX.WorkBook = XLSX.utils.book_new();

    XLSX.utils.book_append_sheet(wb, ws, "Data");

    fileName = fileName ? fileName : uuid.v4();

    XLSX.writeFile(wb, fileName + ".xlsx");
  }

  downloadBasedOnType(type: string, data, fileName = null) {

    let tableData = this.generateTable(data)

    const now = Date.now();
    const dateFormatted = this.pipe.transform(now, 'medium');

    const fileNameWithDate = fileName + '-' + dateFormatted;

    if (type === 'word')
      this.generateDocxFromHTML(tableData.outerHTML, fileName)

    if (type === 'excel')
      this.generateExcelFromHTML(tableData, data, fileNameWithDate);

    if (type === 'pdf')
      this.downloadAsPDFAutoSizeTable(tableData.outerHTML, fileNameWithDate);

  }


  public downloadAsPDF(data, fileName = null) {
    var html = htmlToPdfmake(data, {
      tableAutoSize: true
    });

    const documentDefinition = { content: html };
    pdfMake.createPdf(documentDefinition).download(fileName ? fileName : 'example')
  }

  public downloadAsJsPDF(data, formName = null) {
    html2pdf().from(data).save(`${formName}`)
  }

  public downloadAsPDFFromBackend(data) {
    this.httpService.uploadHTMLToDownloadAsPDF(data);
  }

  public downloadAsPDFAutoSizeTable(data, fileName) {
    var html = htmlToPdfmake(data, {
      tableAutoSize: true,
    });
    const documentDefinition = { content: html };
    pdfMake.createPdf(documentDefinition).download(fileName ? fileName : 'example');
  }



  generateTable(data) {
    var col = [];
    for (var i = 0; i < data.length; i++) {
      for (var key in data[i]) {
        if (col.indexOf(key) === -1) {
          col.push(key);
        }
      }
    }

    // Create a table.
    var table = document.createElement("table");
    table.style.cssText = `
          border-collapse: collapse;
          width: 100%;
          `

    // Create table header row using the extracted headers above.
    var tr = table.insertRow(-1);                   // table row.

    for (var i = 0; i < col.length; i++) {
      var th = document.createElement("th");      // table header.

      th.style.cssText = `
              padding-top: 12px;
              padding-left: 5px;
              text-align: left;
              background-color: white;
              border: 1px solid #ddd;
              font-size: 10px;
              width: ${100 / col.length}%;
              `

      th.innerHTML = col[i] ?? " ";
      tr.appendChild(th);
    }

    // add json data to the table as rows.
    for (var i = 0; i < data.length; i++) {

      tr = table.insertRow(-1);

      for (var j = 0; j < col.length; j++) {
        var tabCell = tr.insertCell(-1);
        tabCell.style.cssText = `
                  border: 1px solid #ddd;
                  padding-top: 10px;
                  padding-left: 5px;
                  font-size: 10px;
                  `
        tabCell.innerHTML = data[i][col[j]];
      }
    }


    const wrapper = document.createElement('div');
    wrapper.style.width = '100%';
    wrapper.appendChild(table);

    return wrapper;
  }

  generateTableWithMerge(data) {
    var col = [];
    for (var i = 0; i < data.length; i++) {
      for (var key in data[i]) {
        if (col.indexOf(key) === -1) {
          col.push(key);
        }
      }
    }

    // Create a table.
    var table = document.createElement("table");
    table.setAttribute('id', 'table')
    table.style.cssText = `
          border-collapse: collapse;
          width: 100%;
          `

    // Create table header row using the extracted headers above.
    var tr = table.insertRow(-1);                   // table row.

    for (var i = 0; i < col.length; i++) {
      var th = document.createElement("th");      // table header.

      th.style.cssText = `
              padding-top: 12px;
              padding-left: 5px;
              padding-bottom: 7px;
              padding-right: 5px;
              text-align: left;
              background-color: white;
              color: black;
              border: 0.5px solid #ddd;
              font-size: 10px;
              width: ${100 / col.length}%;
              `

      th.innerHTML = col[i];
      tr.appendChild(th);
    }

    // add json data to the table as rows.
    for (var i = 0; i < data.length; i++) {

      tr = table.insertRow(-1);

      for (var j = 0; j < col.length; j++) {
        let cell = data[i][col[j]]
        if (!cell.visible) {
          tabCell.style.display = 'none'
          tr.style.cssText = `
            display: none;
          `
        }
        var tabCell = tr.insertCell(-1);
        tabCell.setAttribute('colspan', cell.colSpan)
        tabCell.setAttribute('rowspan', cell.rowSpan)
        tabCell.style.cssText = `
                  border: 0.5px solid #ddd;
                  padding-top: 10px;
                  padding-left: 5px;
                  padding-bottom: 7px;
                  paddin-right: 5px;
                  font-size: 10px;
                  `
        if (!cell.rowSpan || !cell.colSpan) {
          tabCell.style.cssText = 'display: none;'
        }

        if (cell.type === 'date') {
          cell.value ? tabCell.innerHTML = `${this.dateFormat.transform(cell.value)}` : tabCell.innerHTML = ''
        } else if (cell.type === 'file' && Array.isArray(cell.value)) {
          cell.value.forEach(file => {

            if (file.type && file.type === 'image') {

              let img = document.createElement('img')
              img.src = file.imageContent
              img.setAttribute('width', '200')
              img.setAttribute('height', '150')
              tabCell.appendChild(img)
            } else {
              let d = document.createElement('div')
              let a = document.createElement('a')
              a.setAttribute('href', file.url)
              a.innerHTML = `${file.name}, `
              d.appendChild(a)
              tabCell.appendChild(d)
            }
          })

        } else if (cell.type === 'hyperlink' && Array.isArray(cell.value)) {
          cell.value.forEach(link => {
            let d = document.createElement('div')
            let a = document.createElement('a')
            a.setAttribute('href', link.url)
            a.innerHTML = `${link.name}, `
            d.appendChild(a)
            tabCell.appendChild(d)
          })
        } else {
          tabCell.innerHTML = cell.value;
        }

        if (cell.rotateVertical) {
          let p = document.createElement('div')
          p.innerHTML = cell.value
          p.style.cssText = `
            -webkit-transform: rotate(45deg);
            -moz-transform: rotate(45deg);
            -ms-transform: rotate(45deg);
            -o-transform: rotate(45deg);
            transform: rotate(45deg);
          `
          tabCell.appendChild(p)

          tr.style.cssText = `
                      height: 150px;
                    `
        }

      }
    }


    const wrapper = document.createElement('div');
    wrapper.style.width = '100%';
    wrapper.appendChild(table);

    return wrapper;
  }
}
