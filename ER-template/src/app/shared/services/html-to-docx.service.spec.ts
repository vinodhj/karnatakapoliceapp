import { TestBed } from '@angular/core/testing';

import { HtmlToDocxService } from './html-to-docx.service';

describe('HtmlToDocxService', () => {
  let service: HtmlToDocxService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HtmlToDocxService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
