import { TestBed } from '@angular/core/testing';

import { IndexeddbStoreService } from './indexeddb-store.service';

describe('IndexeddbStoreService', () => {
  let service: IndexeddbStoreService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(IndexeddbStoreService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
