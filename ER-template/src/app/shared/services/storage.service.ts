import { Injectable } from '@angular/core';
import { Observable, Observer, of } from 'rxjs';
import { HttpService } from 'src/@exai/services/http.service';
import { isValidStorage, isValidStorageItem } from 'src/app/police-app/utils/utils';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor(private httpService: HttpService) { }

  getSessionStorage(name: string): Observable<any> {
    if (!isValidStorage()) {
      alert('No web storage support');
      return of();
    }

    const item = sessionStorage.getItem(name);

    if (!isValidStorageItem(item))
      return of();

    const data = JSON.parse(item);

    if (!isValidStorageItem(data))
      switch (name) {
        case `divisions`:
          return this.httpService.getDivisions();
        case `sub-divisions`:
          return this.httpService.getSubDivisions();
        case `police-stations`:
          return this.httpService.getAllPoliceStations();
        default:
          return of();
      }

    return new Observable((observer: Observer<any>) => {
      observer.next(data);
      observer.complete();
    });
  }

  setSessionStorage(name: string, data: any): void {
    if (!isValidStorage())
      alert('No web storage support');

    if (isValidStorageItem(data))
      sessionStorage.setItem(name, JSON.stringify(data));
  }
}
