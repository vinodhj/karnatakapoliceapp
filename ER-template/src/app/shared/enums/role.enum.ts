export enum Role {
    SuperAdmin = 1,
    Admin = 2,
    StateHead = 3,
    DistrictHead = 4,
    DivisionHead = 5,
    SubDivision = 6,
    PoliceStation = 7,
    User = 8
}
