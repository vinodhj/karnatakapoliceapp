import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { ExaiPopoverDirective } from '../form-builder/directives/popover.directive';
import { AngularMaterialModel } from './angular-material.module';



@NgModule({
  declarations: [
  ],
  imports: [
    CommonModule,
    AngularMaterialModel,
    FlexLayoutModule,
    MatSnackBarModule
  ],
  exports: [
  ]
})

export class SharedModule { }
