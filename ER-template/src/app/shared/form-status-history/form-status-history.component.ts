import { animate, style, transition, trigger } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpService } from 'src/@exai/services/http.service';
import { StatusDetails } from '../models/status-details';

@Component({
  selector: 'exai-form-status-history',
  templateUrl: './form-status-history.component.html',
  styleUrls: ['./form-status-history.component.scss'],
  animations: [
    trigger('slideInOut', [
      transition(':enter', [
        style({ transform: 'translateX(100%)', opacity: 1 }),
        animate('200ms ease-in', style({ transform: 'translateX(0%)' }))
      ]),
      transition(':leave', [
        animate('200ms ease-in', style({ transform: 'translateX(100%)', opacity: 0 }))
      ])
    ])
  ]
})

export class FormStatusHistoryComponent implements OnInit {

  showSideMenu: boolean = true;
  statusDetails: StatusDetails | null = null;
  disableClickOutside: boolean = false;
  isLinear: boolean = false;
  panelOpenState: boolean = false;

  constructor(
    private httpService: HttpService,
    private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.initData();
  }

  initData(): void {
    const paramId = this.route.snapshot.paramMap.get('userFormId');
    const queryParamId = this.route.snapshot.queryParamMap.get('userFormId');
    if (paramId !== null && paramId !== undefined && paramId !== ``) {
      this.getStatusDetails(paramId);
      return;
    }
    if (queryParamId !== null && queryParamId !== undefined && queryParamId !== ``)
      this.getStatusDetails(queryParamId);
  }

  getStatusDetails(id: string): void {
    this.httpService.getStatusDetails(Number(id)).subscribe(
      response => {
        this.statusDetails = response as StatusDetails;
        console.log(this.statusDetails);
      });
  }
}