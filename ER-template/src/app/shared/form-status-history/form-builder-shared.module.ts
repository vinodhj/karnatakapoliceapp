import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatStepperModule } from '@angular/material/stepper';
import { ClickoutsideDirective } from 'src/app/directives/clickoutside.directive';
import { DragElementDirective } from 'src/app/directives/drag-element.directive';
import { DragHandlerElementDirective } from 'src/app/directives/drag-handler-element.directive';
import { MatExpansionModule } from '@angular/material/expansion';
import { ExaiPopoverDirective } from 'src/app/form-builder/directives/popover.directive';
import { SanitizeHtmlPipe } from '../pipes/sanitize-html.pipe';
import { FileDragDropDirective } from 'src/app/directives/file-drag-drop.directive';
import { FormBuilderApproveRejectComponent } from 'src/app/form-builder/form-builder-custom/form-builder-approve-reject/form-builder-approve-reject.component';
import { FormBuilderApproveRejectElementButtonsComponent } from 'src/app/form-builder/form-builder-custom/form-builder-element-buttons/form-builder-element-buttons.component';
import { FormBuilderApproveRejectElementHtmlComponent } from 'src/app/form-builder/form-builder-custom/form-builder-element-html/form-builder-element-html.component';
import { FormBuilderElementSettingsApproveRejectComponent } from 'src/app/form-builder/form-builder-custom/form-builder-element-settings/form-builder-element-settings.component';
import { FormBuilderApproveRejectInputElementsComponent } from 'src/app/form-builder/form-builder-custom/form-builder-input-elements/form-builder-input-elements.component';
import { FormBuilderControlApproveRejectComponent } from 'src/app/form-builder/form-builder-custom/form-builder-result/form-builder-control/form-builder-control.component';
import { FormBuilderApproveRejectResultComponent } from 'src/app/form-builder/form-builder-custom/form-builder-result/form-builder-result.component';
import { FormBuilderElementFileUploadComponent } from 'src/app/form-builder/form-builder-elements/form-builder-element-file-upload/form-builder-element-file-upload.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularMaterialModel } from '../angular-material.module';
import { DndModule } from 'ngx-drag-drop';
import { FormBuilderGridSettingsComponent } from 'src/app/form-builder/form-builder-elements/form-builder-grid-settings/form-builder-grid-settings.component';
import { ClipboardModule } from '@angular/cdk/clipboard';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatNativeDateModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTabsModule } from '@angular/material/tabs';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MdePopoverModule } from '@material-extended/mde';
import { NgxEditorModule } from 'ngx-editor';
import { FormBuilderElementMulticheckboxComponent } from 'src/app/form-builder/form-builder-elements/form-builder-element-multicheckbox/form-builder-element-multicheckbox.component';
import { FormBuilderElementTableComponent } from 'src/app/form-builder/form-builder-elements/form-builder-element-table/form-builder-element-table.component';
import { RichTextEditorAllModule } from '@syncfusion/ej2-angular-richtexteditor';
import { FormBuilderTableSettingsComponent } from 'src/app/form-builder/form-builder-elements/form-builder-table-settings/form-builder-table-settings.component';

@NgModule({
    declarations: [
        DragHandlerElementDirective,
        DragElementDirective,
        ClickoutsideDirective,
        ExaiPopoverDirective,
        SanitizeHtmlPipe,
        FileDragDropDirective,
        FormBuilderElementFileUploadComponent,
        FormBuilderApproveRejectComponent,
        FormBuilderApproveRejectInputElementsComponent,
        FormBuilderApproveRejectElementHtmlComponent,
        FormBuilderApproveRejectElementButtonsComponent,
        FormBuilderApproveRejectResultComponent,
        FormBuilderControlApproveRejectComponent,
        FormBuilderElementSettingsApproveRejectComponent,
        FormBuilderGridSettingsComponent,
        FormBuilderElementMulticheckboxComponent,
        FormBuilderElementTableComponent,
        FormBuilderTableSettingsComponent

    ],
    imports: [
        CommonModule,
        MatStepperModule,
        MatCardModule,
        MatIconModule,
        MatExpansionModule,
        FormsModule,
        ReactiveFormsModule,
        AngularMaterialModel,
        DndModule,
        MatExpansionModule,
        MatIconModule,
        MatSnackBarModule,
        ClipboardModule,
        MatButtonModule,
        MatSelectModule,
        MatRadioModule,
        MatCheckboxModule,
        MatMenuModule,
        MatInputModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MdePopoverModule,
        MatTabsModule,
        MatProgressSpinnerModule,
        MatProgressBarModule,
        MatTooltipModule,
        MatDialogModule,
        NgxEditorModule,
        RichTextEditorAllModule
    ],
    exports: [
        DragHandlerElementDirective,
        DragElementDirective,
        ClickoutsideDirective,
        MatExpansionModule,
        ExaiPopoverDirective,
        SanitizeHtmlPipe,
        FileDragDropDirective,
        FormBuilderElementFileUploadComponent,
        FormBuilderApproveRejectComponent,
        FormBuilderApproveRejectInputElementsComponent,
        FormBuilderApproveRejectElementHtmlComponent,
        FormBuilderApproveRejectElementButtonsComponent,
        FormBuilderApproveRejectResultComponent,
        FormBuilderControlApproveRejectComponent,
        FormBuilderElementSettingsApproveRejectComponent,
        FormBuilderGridSettingsComponent,
        FormBuilderElementMulticheckboxComponent,
        FormBuilderElementTableComponent,
        FormBuilderTableSettingsComponent
    ],
    providers: [
        {
            provide: STEPPER_GLOBAL_OPTIONS,
            useValue: { displayDefaultIndicatorType: false }
        }
    ]
})

export class FormBuilderSharedModule { }