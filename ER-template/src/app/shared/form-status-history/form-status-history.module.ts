import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatCardModule } from '@angular/material/card';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatIconModule } from '@angular/material/icon';
import { MatStepperModule } from '@angular/material/stepper';
import { FormBuilderSharedModule } from './form-builder-shared.module';
import { FormStatusHistoryComponent } from './form-status-history.component';

@NgModule({
    declarations: [
        FormStatusHistoryComponent
    ],
    imports: [
        CommonModule,
        MatStepperModule,
        MatCardModule,
        MatIconModule,
        FormBuilderSharedModule,
        MatExpansionModule
    ],
    exports: [
        FormStatusHistoryComponent,
        FormBuilderSharedModule
    ]
})

export class FormStatusHistoryModule { }