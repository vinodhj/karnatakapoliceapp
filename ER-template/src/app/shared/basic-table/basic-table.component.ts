import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { Component, OnInit, Input, Output, EventEmitter, ViewChild, AfterViewInit, ElementRef } from '@angular/core';
import { LanguageService } from 'src/app/language.service';
import { fadeInUp400ms } from 'src/@exai/animations/fade-in-up.animation';
import { stagger40ms } from 'src/@exai/animations/stagger.animation';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { MatSort, MatSortable } from '@angular/material/sort';
import { ReportsService } from 'src/app/police-app/reports/services/reports.service';
import moment from 'moment';
import { isValidDate } from 'src/app/police-app/utils/utils';
import { HttpService } from 'src/@exai/services/http.service';

@Component({
  selector: 'exai-basic-table',
  templateUrl: './basic-table.component.html',
  styleUrls: ['./basic-table.component.scss'],
  animations: [
    fadeInUp400ms,
    stagger40ms
  ]

})
export class BasicTableComponent implements OnInit {

  reportDateFrom: string | null = null;
  reportDateTo: string | null = null;
  reportData: { dateFrom: Date | number | null, dateTo: Date | number | null } | null = null;
  completeReportDate: string | null = null;

  @Input() canAddEntry: boolean = true;
  @Input() canChangeStatus: boolean = true;



  @Input() isLoading
  @Input() slideTooltip
  @Input() layoutCtrl
  @Input() searchValues
  @Input() searchCtrl
  @Input() dataSource
  @Input() trackByProperty
  @Input() visibleColumns
  @Input() tableTitle
  @Input() buttonModifyLabel
  @Input() buttonResetLabel
  @Input() addButtonTooltip
  @Input() configFormsActions = false
  @Input() divisionActions = false
  @Input() subDivisionActions = false
  @Input() publishFormsActions = false
  @Input() policeStationsActions = false
  @Input() userManageActions = false
  @Input() columnsSearch;
  @Input() reportsSearch: boolean = false;
  @Input() downloadEnabled: boolean = true;
  @Input() superAdmin: boolean = false;
  @Input() primaryKeyActions: boolean = false;
  @Input() canBulkImport: boolean = false;
  @Input() reportsActions = false;

  @Output() removeEvent = new EventEmitter()
  @Output() addEvent = new EventEmitter()
  @Output() appendToSearchEvent = new EventEmitter()
  @Output() dropEvent = new EventEmitter()
  @Output() openEditEvent = new EventEmitter()
  @Output() toggleColumnVisibilityEvent = new EventEmitter()
  @Output() sendPaginator = new EventEmitter()
  @Output() updateStatusEvent = new EventEmitter()
  @Output() createDivisionEvent = new EventEmitter()
  @Output() createSubDivisionEvent = new EventEmitter()
  @Output() createStationEvent = new EventEmitter()
  @Output() openResetEvent = new EventEmitter()
  @Output() addDivisionEvent = new EventEmitter()
  @Output() updateDivisionEvent = new EventEmitter()
  @Output() updateSubDivisionEvent = new EventEmitter()
  @Output() createPoliceStationEvent = new EventEmitter()
  @Output() createUserEvent = new EventEmitter()
  @Output() bulkAddUsersEvent = new EventEmitter()
  @Output() bulkAddFileUploadedEvent = new EventEmitter()
  @Output() addEditPoliceStationEvent = new EventEmitter()
  @Output() updateFormEvent = new EventEmitter()
  @Output() createFormEvent = new EventEmitter()
  @Output() downloadWordEvent = new EventEmitter()
  @Output() deleteFormEvent = new EventEmitter();
  @Output() cloneFormEvent = new EventEmitter();
  @Output() downloadBulkImportTemplateEvent = new EventEmitter();
  @Output() openSelectDatesDialogEvent = new EventEmitter();
  @Output() createPrimaryKeyEvent = new EventEmitter();
  @Output() updatePrimaryKeyEvent = new EventEmitter();
  @Output() deletePrimaryKeyEvent = new EventEmitter();
  @Output() previewRecordEvent = new EventEmitter();

  private _columns;
  public get columns() {
    return this._columns;
  }
  @Input() public set columns(value) {
    this._columns = value;
    this.columnsForSearchingData = value.filter(x => x.visible);
  }

  @ViewChild('FileSelectInputDialog') FileSelectInputDialog: ElementRef;



  readonly separatorKeysCodes = [ENTER, COMMA] as const;

  constructor(
    public languageService: LanguageService,
    private reportsService: ReportsService,
    private httpService: HttpService
  ) { }

  ngOnInit(): void {
    this.showDates();
    this.checkRole();
  }

  showDates(): void {
    this.reportsService.reportDatesInput.subscribe(
      data => {
        this.reportData = data;
        this.reportDateFrom = this.reportData?.dateFrom === null ? null : `from ${moment(this.reportData?.dateFrom).format('MMMM Do YYYY')}`;
        this.reportDateTo = this.reportData?.dateTo === null ? null : ` to ${moment(this.reportData?.dateTo).format('MMMM Do YYYY')}`;
      }
    )
  }

  checkRole(): void {
    var userRole = this.httpService.getRole();
    // if (userRole === 1)
      // this.configFormsActions = true;
    this.canAddEntry = true;
    this.canChangeStatus = true;
  }

  columnsForSearchingData;

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.columns, event.previousIndex, event.currentIndex);
  }




  removeHandler(value) {
    this.removeEvent.emit(value)
  }

  addHandler(event) {
    this.addEvent.emit(event)
  }

  appendToSearchHandler(columnLabel) {
    this.appendToSearchEvent.emit(columnLabel)
  }

  dropHandler(event) {
    this.dropEvent.emit(event)
  }

  openEditHandler(row) {
    this.openEditEvent.emit(row)
  }

  toggleColumnVisibilityHandler(column, event) {
    this.toggleColumnVisibilityEvent.emit({ column, event })
  }

  updateStatusHandler(event, element) {
    this.updateStatusEvent.emit({ event, element })
  }

  createDivisionHandler(customer) {
    this.createDivisionEvent.emit(customer)
  }

  updateDivisionHandler(customer) {
    this.updateDivisionEvent.emit(customer)
  }

  updatePrimaryKeyHandler(primaryKey) {
    this.updatePrimaryKeyEvent.emit(primaryKey);
  }

  createSubDivisionHandler(customer) {
    this.createSubDivisionEvent.emit(customer)
  }

  createStationHandler(customer) {
    this.createStationEvent.emit(customer)
  }

  openResetHandler(customer) {
    this.openResetEvent.emit(customer)
  }

  cloneFormHandler(customer) {
    this.cloneFormEvent.emit(customer);
  }

  updateSubDivisionHandler(customer) {
    this.updateSubDivisionEvent.emit(customer)
  }

  deleteFormHandler(customer) {
    this.deleteFormEvent.emit(customer);
  }

  deletePrimaryKeyHandler(primaryKey) {
    this.deletePrimaryKeyEvent.emit(primaryKey);
  }

  addButton() {
    if (this.divisionActions) {
      this.addDivisionEvent.emit()
    } else if (this.subDivisionActions) {
      this.createSubDivisionEvent.emit()
    } else if (this.policeStationsActions) {
      this.createPoliceStationEvent.emit()
    } else if (this.userManageActions) {
      this.createUserEvent.emit()
    } else if (this.configFormsActions) {
      this.createFormEvent.emit()
    } else if (this.primaryKeyActions) {
      this.createPrimaryKeyEvent.emit()
    }
  }
  bulkAddButton() {

    /*  document.querySelector('input').click(); */
    this.bulkAddUsersEvent.emit(this.FileSelectInputDialog.nativeElement);
  }

  bulkAddFileUploaded(file) {
    this.bulkAddFileUploadedEvent.emit(file);
    this.FileSelectInputDialog.nativeElement.value = null;
  }

  addEditPoliceStationHandler(customer) {
    this.addEditPoliceStationEvent.emit(customer)
  }

  updateFormHandler(customer) {
    this.updateFormEvent.emit(customer)
  }

  publishOptionExists(element): boolean {
    return element?.isPublished !== null &&
      element?.isPublished !== undefined &&
      element?.isPublished !== ``;
  }

  isActiveOptionExists(element): boolean {
    return element?.isActive !== null &&
      element?.isActive !== undefined &&
      element?.isActive !== ``;
  }

  public getSubDivisions(rowValue) {
    let subDivisions = []
    rowValue.departments.forEach(element => {
      if (element.name === 'Police-Station') {
        subDivisions.push(element.parent.value)
      } else if (element.name === 'Sub-Division') {
        subDivisions.push(element.value)
      }
    })
    return [...new Set(subDivisions)].join(', ')
  }

  public getPoliceStations(row) {
    let policeStations = []
    row.departments.forEach(element => {
      if (element.name === 'Police-Station') {
        policeStations.push(element.value)
      }
    })
    return [...new Set(policeStations)].join(', ')
  }

  downloadHandler(type: string) {
    this.downloadWordEvent.emit(type);
  }

  downloadBulkImportTemplate(): void {
    this.downloadBulkImportTemplateEvent.emit();
  }

  openSelectDatesDialog(): void {
    this.openSelectDatesDialogEvent.emit();
  }

  remove(date: string): void {
    if (date === `dateFrom`)
      this.reportsService.sendReportInputData({
        dateFrom: null,
        dateTo: this.reportData?.dateTo
      });
    if (date === `dateTo`)
      this.reportsService.sendReportInputData({
        dateFrom: this.reportData?.dateFrom,
        dateTo: null
      });
  }

  previewRecord(record) {
    this.previewRecordEvent.emit(record);
  }
}