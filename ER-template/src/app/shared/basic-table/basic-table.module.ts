import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common'

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { FlexLayoutModule } from '@angular/flex-layout';
import { AngularMaterialModel } from '../angular-material.module';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { BasicTableComponent } from './basic-table.component';
import { PageLayoutModule } from 'src/@exai/components/page-layout/page-layout.module';
import { TableSearchColumnsPipe } from '../pipes/table-search-columns.pipe';

@NgModule({
  declarations: [
      BasicTableComponent,
      TableSearchColumnsPipe   
  ],
  imports: [
    CommonModule,
    AngularMaterialModel,
    FormsModule,
    ReactiveFormsModule,
    NgxMaterialTimepickerModule,
    FlexLayoutModule,
    PageLayoutModule,
    DragDropModule,
  ],
  exports: [
    BasicTableComponent,
    TableSearchColumnsPipe
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ],
})
export class BasicTableModule { }