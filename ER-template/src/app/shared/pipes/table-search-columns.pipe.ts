import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'tableSearchColumns',
  pure: false
})
export class TableSearchColumnsPipe implements PipeTransform {

  transform(value: any[]): unknown {

    const hiddenColumns = ['Status', 'Action', 'Checkbox']

    return value.filter(x => x.visible && !hiddenColumns.includes(x.label));
  }

}
