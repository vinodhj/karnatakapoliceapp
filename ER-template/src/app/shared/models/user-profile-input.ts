export class UserProfileInput {
    firstName: string;
    lastName: string;
    phone: string;
}