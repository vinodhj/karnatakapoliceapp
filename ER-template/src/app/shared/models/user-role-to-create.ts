export interface UserRoleToCreate {
    id: number;
    name: string;
    description: string;
    isActive: boolean | null;
    isDeleted: boolean | null;
    createdBy: number;
    createdDatetime: string;
    modifiedBy: number;
    modifiedDatetime: string;
}
