export interface StatusDetails {
    currentStatusId: number;
    currentStatusName: string;
    currentStatusDatetime: Date | string | null;
    statusHistoryList: StatusHistory[];
}

export interface StatusHistory {
    statusId: number;
    statusName: string;
    proccessLogId: number;
    formId: number;
    type: string;
    actionBy: number;
    actionByFullName: string;
    actionOnDatetime: Date | string;
    actionByRoleId: number | null;
    actionByRoleName: string | null;
}