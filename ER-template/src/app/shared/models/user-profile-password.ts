export class UserProfilePassword {
    currentPassword: string;
    newPassword: string;
    confirmNewPassword: string;
}