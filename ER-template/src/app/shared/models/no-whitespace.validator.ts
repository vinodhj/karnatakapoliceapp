import { AbstractControl, ValidationErrors, ValidatorFn } from "@angular/forms";

export function noWhitespaceValidator(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
        const noWhitespace = /^\s*$/.test(control.value?.toString());
        return noWhitespace ? { noWhitespace: { value: control.value } } : null;
    };
}