import { ErrorMessage } from "./error-message";

export const PROFILE_ERROR_MESSAGES: ErrorMessage[] = [
    {
        formControlName: `firstName`,
        errors: [
            {
                key: `required`,
                message: `First Name is required.`
            },
            {
                key: `maxlength`,
                message: `Maximum length is 64.`
            },
            {
                key: `noWhitespace`,
                message: `Only whitespace is not allowed.`
            }
        ]
    },
    {
        formControlName: `lastName`,
        errors: [
            {
                key: `required`,
                message: `Last Name is required.`
            },
            {
                key: `maxlength`,
                message: `Maximum length is 64.`
            },
            {
                key: `noWhitespace`,
                message: `Only whitespace is not allowed.`
            }
        ]
    },
    {
        formControlName: `email`,
        errors: [
            {
                key: `required`,
                message: `Email is required.`
            },
            {
                key: `maxlength`,
                message: `Maximum length is 128.`
            },
            {
                key: `email`,
                message: `Email is not in valid format.`
            }
        ]
    },
    {
        formControlName: `password`,
        errors: [
            {
                key: `required`,
                message: `Password is required.`
            }
        ]
    },
    {
        formControlName: `phone`,
        errors: [
            {
                key: `required`,
                message: `Phone is required.`
            },
            {
                key: `maxlength`,
                message: `Maximum length is 16.`
            },
            {
                key: `noWhitespace`,
                message: `Only whitespace is not allowed.`
            }
        ]
    },
    {
        formControlName: `role`,
        errors: [
            {
                key: `required`,
                message: `Role is required.`
            },
            {
                key: `maxlength`,
                message: `Maximum length is 64.`
            }
        ]
    },
    {
        formControlName: `division`,
        errors: [
            {
                key: `required`,
                message: `Division is required.`
            },
            {
                key: `maxlength`,
                message: `Maximum length is 250.`
            }
        ]
    },
    {
        formControlName: `subDivision`,
        errors: [
            {
                key: `required`,
                message: `Sub Division is required.`
            },
            {
                key: `maxlength`,
                message: `Maximum length is 250.`
            }
        ]
    },
    {
        formControlName: `policeStation`,
        errors: [
            {
                key: `required`,
                message: `Police Station is required.`
            },
            {
                key: `maxlength`,
                message: `Maximum length is 250.`
            }
        ]
    }
]

export const PASSWORD_ERROR_MESSAGES: ErrorMessage[] = [
    {
        formControlName: `password`,
        errors: [
            {
                key: `required`,
                message: `Current password is required.`
            },
            {
                key: `pattern`,
                message: `Current password is not in correct format.`
            },
            {
                key: `noWhitespace`,
                message: `Only whitespace is not allowed.`
            }
        ]
    },
    {
        formControlName: `newPassword`,
        errors: [
            {
                key: `required`,
                message: `New password is required.`
            },
            {
                key: `pattern`,
                message: `New password is not in correct format.`
            },
            {
                key: `noWhitespace`,
                message: `Only whitespace is not allowed.`
            }
        ]
    },
    {
        formControlName: `repeatNewPassword`,
        errors: [
            {
                key: `required`,
                message: `Repeat new password is required.`
            },
            {
                key: `pattern`,
                message: `Repeat new password is not in correct format.`
            },
            {
                key: `noWhitespace`,
                message: `Only whitespace is not allowed.`
            }
        ]
    }
]