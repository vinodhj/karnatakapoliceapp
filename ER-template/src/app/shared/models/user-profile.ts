export interface UserProfile {
    firstName: string;
    lastName: string;
    email: string;
    phone: string;
    roleId: number | null;
    roleName: string;
    policeStations: UserProfilePoliceStation[];
}

export interface UserProfilePoliceStation {
    policeStationId: number;
    policeStationName: string;
    parent: any;
}
