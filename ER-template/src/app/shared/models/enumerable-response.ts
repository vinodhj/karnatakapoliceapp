export class EnumerableResponse<T> {
    items: T[] = [];
    totalResultsCount: number | null = 0;
}