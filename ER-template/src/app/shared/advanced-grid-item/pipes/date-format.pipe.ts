import { Injectable, Pipe, PipeTransform } from '@angular/core';
@Injectable({
  providedIn: 'root'
})
@Pipe({
  name: 'dateFormat'
})
export class DateFormatPipe implements PipeTransform {

  padTo2Digits(num) {
    return num.toString().padStart(2, '0');
  }

  transform(value: unknown, ...args: unknown[]): unknown {
    let date = new Date(value + '')
    
    let wantedFormat =  [
      this.padTo2Digits(date.getDate()),
      this.padTo2Digits(date.getMonth() + 1),
      date.getFullYear(),
    ].join('/');

    console.log(wantedFormat);
    return wantedFormat
    
  }


}
