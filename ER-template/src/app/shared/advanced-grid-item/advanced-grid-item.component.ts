import { SelectionModel } from '@angular/cdk/collections';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatChipInputEvent } from '@angular/material/chips';
import { MatDialog } from '@angular/material/dialog';
import { MatFormFieldDefaultOptions, MAT_FORM_FIELD_DEFAULT_OPTIONS } from '@angular/material/form-field';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { UntilDestroy } from '@ngneat/until-destroy';
import moment from 'moment';
import { BehaviorSubject, Observable } from 'rxjs';
import { filter, tap } from 'rxjs/operators';
import { HttpService } from 'src/@exai/services/http.service';
import { FormBuilderFillDataService } from 'src/app/form-builder/services/form-builder-fill-data.service';
import { SubmitPopupComponent } from 'src/app/police-app/forms/user-filled-form/submit-popup/submit-popup.component';
import { statusCodes } from 'src/app/police-app/forms/user-filled-form/user-filled-form.component';
import { DataService } from 'src/app/police-app/services/data-service.service';
import { getUniqueId } from '../../police-app/utils/utils';
import { Role } from '../enums/role.enum';
import { SnackBarService } from '../services/snack-bar.service';
import { AddColumnDialogComponent } from './dialogs/add-column-dialog/add-column-dialog.component';
import { ConfirmDialogComponent } from './dialogs/confirm-dialog/confirm-dialog.component';
import { NewColumnDialogComponent } from './dialogs/new-column-dialog/dialog.component';
import { SetLabelsComponent } from './dialogs/set-labels/set-labels.component';



@UntilDestroy()
@Component({
  selector: 'exai-advanced-grid-item',
  templateUrl: './advanced-grid-item.component.html',
  styleUrls: ['./advanced-grid-item.component.scss'],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        appearance: 'standard'
      } as MatFormFieldDefaultOptions
    }
  ]
})
export class AdvancedGridItemComponent implements OnInit {

  @Input() initTableValues
  @Input() columns = [];
  @Input() tableRowsData

  @Output() submitTableDataEvent = new EventEmitter()

  @ViewChild(MatPaginator, { static: true }) public paginator: MatPaginator;


  dataSource
  layoutCtrl = new FormControl('boxed'); // check if i need this 

  subject$: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);
  data$: Observable<any[]> = this.subject$.asObservable();

  columnsFiltered = []
  rowsValues = []
  rowsFilteredByColumnsVisibility = []

  tableDataOnSubmit = {
    columns: null,
    rows: null
  }

  @Output() CNFDisableEvent = new EventEmitter()
  @Output() CNFAddEditPoliceStationEvent = new EventEmitter()
  @Output() CNFUpdateFormHandler = new EventEmitter()
  @Output() CNFAddNewFormEvent = new EventEmitter()

  showTable: boolean = false
  showEditSelected: boolean = false

  pageSize: number = 10;
  pageSizeOptions: number[] = [5, 10, 20, 50];
  selection = new SelectionModel<any>(true, []);
  searchCtrl = new FormControl();

  filterByText: string = ''
  filterType: string = ''
  allFilterValues = []

  summeryAlwaysVisible?: boolean
  tableTextStyle: string = 'justify'

  dataForMerge
  showMergeDataComponent = false

  colNumberFromParm = false
  loadingTableData = true

  disableMergeButton = false

  constructor(
    private dialog: MatDialog,
    private snackBarService: SnackBarService,
    private changeDetectionRef: ChangeDetectorRef,
    private dataService: DataService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private httpService: HttpService,
    private formBuilderFillData: FormBuilderFillDataService
  ) { }

  get visibleColumns() {
    return this.columns.filter((column) => column.visible).map((column) => column.property);
  }

  get rowNumber() {
    if (this.subject$.value) {
      return this.subject$.value.filter(row => !row.hidden).map(row => row)
    } else {
      return []
    }
  }

  get hasSavedData() {
    return sessionStorage.getItem('masterTableData')
  }

  userRole = this.httpService.getRole();

  get cantChange(): boolean {
    return [Role.User].includes(this.userRole);
  }


  readonly separatorKeysCodes = [ENTER, COMMA] as const;

  userFormId

  ngOnInit() {
    this.userFormId = this.activatedRoute.snapshot.queryParamMap.get('userFormId')
    this.dataService.curFormId = +this.activatedRoute.snapshot.queryParamMap.get('formId')
    console.log(this.dataService.curFormId);
    

    this.dataSource = new MatTableDataSource();
    this.dataSource.filterPredicate = (data, filters) => {
      const matchFilter = [];
      const filterArray = filters.split('+');
      const columns = (<any>Object).values(data);

      filterArray.forEach(filter => {
        let formattedFilter = filter.trim().toLowerCase()
        const customFilter = [];
        columns.forEach(column => {
          this.wantedSearchedValues.forEach(colProperty => {
            if (colProperty == 'all') {
              for (let key in data) {
                if (typeof (data[key] === 'object')) {
                  let formattedDataValue = ((('' + data[key].value).toLowerCase()).trim())
                  if (formattedDataValue.includes(formattedFilter) || formattedDataValue == (formattedFilter)) {
                    customFilter.push(true)
                  }
                }
              }
            } else {
              let formattedDataColProperty = (('' + data[colProperty].value).toLowerCase()).trim()
              customFilter.push(formattedDataColProperty.includes(formattedFilter))
            }
          })
        });
        if (data.summery.length > 0) {
          let formattedSummery = data.summery.toLowerCase().trim()
          customFilter.push(formattedSummery.includes(formattedFilter))
        }
        matchFilter.push(customFilter.some(Boolean)); // OR
      });
      return matchFilter.every(Boolean); // AND
    }

    this.data$.pipe(
      filter<any[]>(Boolean)
    ).subscribe(rows => {
      if (this.dataSource && rows) {
        this.dataSource.data = rows;
      }
    })

  }

  submitStatus(status) {
    this.dialog.open(SubmitPopupComponent).afterClosed().subscribe((response) => {
      if (response.confirmed) {
        this.loadingTableData = true
        this.httpService.postStatus(this.userFormId, statusCodes[status], response.comment).subscribe(() => {
          this.snackBarService.showMessage(`Record ${status === 'approve' ? 'Approved' : 'Rejected'} Successfully`, 2000)
          this.router.navigate(['forms', 'user-filled-form'])
        })
      }
    });
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator

    this.activatedRoute.queryParams.subscribe(param => {
      
      if (param.formId && param.userFormId) {
        this.httpService.getViewForFormBulder(+param.userFormId).subscribe((data: any) => {
          this.loadingTableData = false
          this.columns = data.columns
          this.columns.forEach((column, index) => {
            if (column.expectedFilterType === 'date' || column.rangeDate) {
              column['rangeDate'] = new FormGroup({
                start: new FormControl(''),
                end: new FormControl('')
              })
            }
            column['cantChange'] = this.cantChange
            if (column.property !== 'firstColumn' || column.property !== 'actionsColumn') {
              data.rows.forEach(row => {
                row['cantChange'] = this.cantChange
                if (row[column.property]) {
                  row[column.property]['cantChange'] = this.cantChange
                }
              })
            }
          })
          this.subject$.next(data.rows)
        })
      } else if (param.formId) {  
        this.dataService.curFormId = +param.formId 
        this.httpService.templateGetContent(+param.formId).pipe( 
          tap(data => {
            if (!data) {
              this.snackBarService.showMessage('There Is No Table Form Type With This ID', 3000)
              // this.router.navigate(['forms', 'config-form'])
            } else {
              return data
            }
          })
        ).subscribe((data: any) => {
          this.closeMergeDataComponent()
          this.disableMergeButton = false
          this.loadingTableData = false          
          if (data) {
            this.columns = data.columns
            this.columns.forEach((column, index) => {
              if (column.expectedFilterType === 'date') {
                column['rangeDate'] = new FormGroup({
                  start: new FormControl(''),
                  end: new FormControl('')
                })
              }
              column['cantChange'] = this.cantChange
              if (column.property !== 'firstColumn' || column.property !== 'actionsColumn') {
                data.rows.forEach(row => {
                  row['cantChange'] = this.cantChange
                  if (row[column.property]) {
                    row[column.property]['cantChange'] = this.cantChange
                  }
                })
              }
            })
            this.subject$.next(data.rows)
          }
        })
        return
      }
    })

    if (this.activatedRoute.snapshot.queryParamMap.get('colNumber')) {
      this.loadingTableData = false
      let colNumber = this.activatedRoute.snapshot.queryParamMap.get('colNumber')
      this.colNumberFromParm = true
      let indexNumber
        = this.columns.length ? this.columns.length : 0
      let forLength = this.columns.length + +colNumber ? this.columns.length + +colNumber : +colNumber

      this.columns = []
      this.subject$.next([])
      indexNumber = 1
      this.columns.push({ label: 'Actions', property: 'actionsColumn', type: 'actionsButtons', visible: true, id: getUniqueId(2) })
      this.columns.unshift({ label: 'Checkbox', property: 'firstColumn', type: 'checkbox', visible: true, id: getUniqueId(2) })


      for (let i = indexNumber; i <= forLength; i++) {
        let uniqueProperty = getUniqueId(2)
        let singleColumn = { label: '', property: uniqueProperty, type: null, visible: true, id: uniqueProperty, filterRules: [], addedData: false }
        this.columns.push(singleColumn)
      }
      console.log(8);

      this.updateTableValues()

      if (this.initTableValues && Object.keys(this.initTableValues).length > 0) {
        let tableValuesInString = JSON.stringify(this.initTableValues)
        let tableValues = JSON.parse(tableValuesInString)
        this.tableDataOnSubmit = tableValues
        this.columns = tableValues.columns

        this.subject$.next(tableValues.rows)
      } else {
        if (!this.colNumberFromParm) {
          this.columns = []
          this.subject$.next([])
        }
      }
      this.changeDetectionRef.detectChanges()

    }
  }

  getDate() {
    this.subject$.value.forEach(element => {
      element['subDate'] = moment(element.createdOn).format('L')
      element['lastDate'] = moment(element.modifiedOn).format('L')
    });
  }

  onFilterChange(value: string) {
    value = value.trim();
    value = value.toLowerCase();
    this.dataSource.filter = value;
    // Should not be required, doesn't give wanted 
    // this.updateTableValues()
  }

  drop(event: CdkDragDrop<string[]>) {
    if (event.previousIndex > 0) {
      moveItemInArray(this.columns, event.previousIndex, event.currentIndex);
    }
    this.updateTableValues()
  }


  createTable({ newTable }) {
    if (this.columns.length > 2 && newTable) {
      let message = 'You have data in table. Are you sure you want to create a new one?'
      const confirmDialog = this.dialog.open(ConfirmDialogComponent, {
        width: '500px',
        data: {
          message: message,
        }
      });

      confirmDialog.afterClosed().subscribe(result => {
        if (result) {
          this.resetTable()
          console.log(result);

          this.addColumnNumber(newTable)
        }
      })
    } else {
      this.addColumnNumber(newTable)
    }
  }

  resetTable() {
    this.subject$.next([])
    this.columns = []
    this.tableDataOnSubmit = {
      columns: [],
      rows: []
    }
  }

  addColumnNumber(newTable) {
    this.showTable = true
    let enteredNumberColumns = 1
    const addColumnDialog = this.dialog.open(AddColumnDialogComponent, {
      width: '500px',
      data: { enteredNumberColumns: enteredNumberColumns }
    });

    addColumnDialog.afterClosed().subscribe(result => {
      let indexNumber = this.columns.length
      let forLength = (this.columns.length - 1) + result

      if (newTable) {
        this.columns = []
        this.subject$.next([])
        indexNumber = 1
        forLength = result
        this.columns.push({ label: 'Actions', property: 'actionsColumn', type: 'actionsButtons', visible: true, id: getUniqueId(2) })
        this.columns.unshift({ label: 'Checkbox', property: 'firstColumn', type: 'checkbox', visible: true, id: getUniqueId(2) })
      }

      for (let i = indexNumber; i <= forLength; i++) {
        let uniqueProperty = getUniqueId(2)
        let singleColumn = { label: '', property: uniqueProperty, type: null, visible: true, id: uniqueProperty, filterRules: [], addedData: false }
        this.columns.push(singleColumn)
      }
      this.updateTableValues()

    })
  }

  // adding column data
  createColumnData(index, existingColumn = null) {
    let columnName: ''
    let type: string
    let rating: number
    const dialogRef = this.dialog.open(NewColumnDialogComponent, {
      width: '500px',
      data: {
        columnName: columnName,
        rating: rating,
        type: type,
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      let duplicateColumnName = false
      if (result) {
        if (existingColumn) {
          this.subject$.value.forEach((row) => {
            for (let key in row) {
              if (key == existingColumn)
                Reflect.deleteProperty(row, key)
            }
          })
        }
        let propertyString = this.toCamelCase(result.columnName)

        this.columns.forEach((column) => {
          column.property === propertyString ? duplicateColumnName = true : null
          column.addedData = true
        })

        if (!duplicateColumnName || this.columns[index].property == propertyString) {
          this.columns[index] = { ...this.columns[index], label: result.columnName, property: propertyString, type: result.type, expectedFilterType: 'text' }
          result.dropdownData ? this.columns[index].dropdownData = result.dropdownData : null
          result.type == 'date' || result.type == 'time' ? this.columns[index].format = result.format : null

          if (result.type === 'date')
            this.columns[index].expectedFilterType = 'date'
          this.columns[index].rangeDate = new FormGroup({
            start: new FormControl(),
            end: new FormControl(),
          });

          if (result.type === 'time')
            this.columns[index].expectedFilterType = 'time'

          if (result.type === 'rating') {
            let ratingValue: number = 0.5
            this.columns[index].expectedFilterType = 'number'
            this.columns[index].ratings = []
            if (result.rating) {
              for (let i = 0; i < result.rating; i += 0.5) {
                this.columns[index].ratings.unshift(ratingValue)
                ratingValue += 0.5
              }
            } else {
              this.columns[index].ratings = [5, 4.5, 4, 3.5, 3, 2.5, 2, 1.5, 1, 0.5]
            }
          }

          result.type === 'number' ? this.columns[index].expectedFilterType = 'number' : null


          // add one row if there are none                    
          if (this.subject$.value.length < 1) {
            this.addRow(result.format, result.ratings)
          } else {
            this.updateRow(propertyString, this.columns[index], result.format, result.rating)
          }

          // turn on edit for all rows
          this.subject$.value.forEach(row => row.edit = true)
        } else {
          this.snackBarService.showMessage("Can't have duplicate column names", 3000)
        }
      }
    })
  }

  toCamelCase(str) {
    return str.replace(/(?:^\w|[A-Z]|\b\w)/g, function (word: string, index: number) {
      return index == 0 ? word.toLowerCase() : word.toUpperCase();
    }).replace(/\s+/g, '');
  }

  // delete wanted column and key from rows
  deleteColumn({ index, column }) {
    this.columns.splice(index, 1)
    // update row values
    this.updateRowsOnDelete(column.property)
    this.updateTableValues(column)
  }

  openFile(url) {
    document.open('https://images.unsplash.com/photo-1640622841908-3a691b7b7ac9?ixlib=rb-1.2.1&ixid=MnwxMjA3fDF8MHxlZGl0b3JpYWwtZmVlZHwxfHx8ZW58MHx8fHw%3D&auto=format&fit=crop&w=600&q=60', '', 'noopener=true')
  }

  editColumn(index) {
    let message = 'You have data in this column. Are you sure you want to edit it?'
    let answer: boolean
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '500px',
      data: {
        message: message,
        answer: answer
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.answer) {
        this.createColumnData(index, this.columns[index].property)
        this.columns.splice(index, 1, { label: '', property: `${index}`, type: null, visible: true, id: index, filterRules: [] })
      }
    })
  }

  confirmRow(row) {
    let emailRegex = new RegExp(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/);

    for (let columnProperty in row) {
      if (columnProperty !== 'edit' && row[columnProperty].type !== 'file' && row[columnProperty].visible) {
        if (!row[columnProperty].value) {
          this.snackBarService.showMessage('Please validly fill all the values', 3000)
          return row.edit = true
        } else if (row[columnProperty].type === 'email' && !emailRegex.test(row[columnProperty].value)) {
          this.snackBarService.showMessage('Please enter a valid email', 3000)
          return row.edit = true
        }
      } else {
        row.edit = false
      }
    }
    this.filterRowsByAllFilters()
    this.updateTableValues()
  }

  addRow(format = false, ratings = 5) {
    let row = {
      edit: true,
      hidden: false,
      id: getUniqueId(2),
      summery: '',
    }

    let currentRowsState = [...this.subject$.value]

    for (let i = 2; i < this.columns.length; i++) {
      let currentColumn = this.columns[i]
      let key = currentColumn.property
      let type = currentColumn.type

      if (currentColumn.addedData) {
        row[key] = {
          value: '',
          data: [],
          visible: true,
          type
        }
        this.setInitValueAndData(row, key, format, type, ratings)
      }
    }

    currentRowsState.push(row)
    this.subject$.next(currentRowsState)
    this.updateTableValues()

    const { length, pageSize } = this.dataSource.paginator

    if (length > pageSize - 1) {
      this.changeDetectionRef.detectChanges()
      this.dataSource.paginator.lastPage()
    }
  }

  // Set value, rating and format for column type of date, time, checkbox, file, and rating
  setInitValueAndData(row, key, format, type, ratings = 5) {
    row[key].rowSpan = 1
    row[key].colSpan = 1

    if (format && (type == 'date' || type == 'time')) {
      row[key].data = format
    }
    if (type === 'checkbox' || type === 'file') {
      row[key].value = []
    }
    if (type === 'rating') {
      let middleValue = ratings / 2
      row[key].value = middleValue
    }

  }

  // Update existing row
  updateRow(newProperty: string, column, format = false, ratings) {
    this.subject$.value.forEach(row => {
      for (let key in row) {
        if (key == column.id)
          Reflect.deleteProperty(row, key)
      }

      row[newProperty] = {
        value: '',
        data: [],
        visible: column.visible,
        type: column.type,
        rowSpan: 1,
        colSpan: 1
      }

      this.setInitValueAndData(row, newProperty, format, column.type, ratings)
    })
    this.updateTableValues()
  }

  // update rows if column is edited or deleted
  updateRowsOnDelete(columnProperty: string) {
    let newRowElements = []
    this.subject$.value.forEach((row) => {
      let newRowElement = Object.keys(row).reduce((object, key) => {
        if (key !== columnProperty) {
          object[key] = row[key]
        }
        return object
      }, {})
      newRowElements.push(newRowElement)
    })
    this.subject$.next(newRowElements)
  }

  actionButton({ event, row }) {
    if (event.target.files && event.target.files[0]) {
      var filesAmount = event.target.files.length;
      for (let i = 0; i < filesAmount; i++) {
        let reader = new FileReader();
        reader.readAsDataURL(event.target.files[i]);
        reader.onload = (e) => {
          row.value.push({ file: e.target.result, name: event.target.files[0].name })
        }
      }
    }
    this.updateTableValues()
  }

  itemAction({ row, action }) {
    let indexOfItemRow = this.subject$.value.indexOf(row)

    if (action === 'deleteSelected') {
      let tableValueCopy = [...this.subject$.value]
      tableValueCopy.splice(indexOfItemRow, 1)
      this.subject$.next(tableValueCopy)
    } else if (action === 'editSelected') {
      this.showEditSelected = true
      this.subject$.value[indexOfItemRow].edit = true
    } else {
      this.showEditSelected = false
      this.subject$.value[indexOfItemRow].edit = false
    }
    this.updateTableValues()
  }

  itemsAction({ rows, action }) {
    rows.forEach(row => this.itemAction({ row, action }))
    if (action === 'deleteSelected') this.selection.clear()
  }

  toggleColumnVisibility({ column, event }) {
    event.stopPropagation();
    column.visible = !column.visible;
    this.updateTableValues(column)
  }

  updateTableValues(column = null) {
    this.columnsFiltered = []
    this.rowsFilteredByColumnsVisibility = []
    this.columnsFiltered = this.columns.filter(Column => Column.visible)

    if (column != null) {
      this.subject$.value.forEach(row => {
        let obj = {
          hidden: row.hidden,
          edit: row.edit,
          id: row.id
        }

        for (let key in row) {
          row[key].value && row[key].value.length < 1 ? row.edit = true : null
          if (key === column.property) {
            row[key].visible = column.visible
          }
          if (row[key].visible) {
            obj[key] = { ...row[key] }
          }
        }
        this.rowsFilteredByColumnsVisibility.push(obj)
      })
    }

    // set table values to be valid when emitting output
    this.columnsFiltered.length > 0 ? this.tableDataOnSubmit.columns = this.columnsFiltered : this.tableDataOnSubmit.columns = this.columns
    this.rowsFilteredByColumnsVisibility.length > 0 ? this.tableDataOnSubmit.rows = [...this.rowsFilteredByColumnsVisibility] : this.tableDataOnSubmit.rows = [...this.subject$.value]
    this.tableDataOnSubmit.rows = [...this.tableDataOnSubmit.rows].filter(row => !row.hidden)
    this.submitTableDataEvent.emit(this.tableDataOnSubmit)
    console.log('table data on submit', this.tableDataOnSubmit);
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const page = this.dataSource.connect().value.length
    return numSelected === page;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.connect().value.forEach(row => this.selection.select(row));
  }

  // filter over the original table with filter array
  checkWhatRowsToFilter(column, removeFilter, filterValue = null, type = null) {
    if (!removeFilter) {
      this.allFilterValues.push({ columnProperty: column.property, columnLabel: column.label, filterValue: filterValue, filterType: type })
      column.filterRules.push({ filterValue: filterValue, filterType: type, columnProperty: column.property })
    }
    this.filterRowsByAllFilters()
    console.log(1);

    this.updateTableValues(column)
  }

  // filter rows by all filter values from columns
  filterRowsByAllFilters() {
    if (this.allFilterValues.length > 0) {
      this.allFilterValues.forEach(currentFilterEl => {
        let Property = currentFilterEl.columnProperty
        let currentFilterType = currentFilterEl.filterType

        currentFilterType.filterValue === 'number' ? currentFilterEl.filterValue = +currentFilterEl.filterValue : null

        this.subject$.value.forEach(row => {
          let rowProperty = row[Property]

          row.hidden = false
          if (currentFilterType.label === 'Start with' && !rowProperty.value.startsWith(currentFilterEl.filterValue)) {
            row.hidden = true
          } else if (currentFilterType.label === 'Contains' && !rowProperty.value.includes(currentFilterEl.filterValue)) {
            row.hidden = true
          } else if (currentFilterType.label === 'Not contains' && !(rowProperty.value.includes(currentFilterEl.filterValue) == false)) {
            row.hidden = true
          } else if (currentFilterType.label === 'Ends with' && !rowProperty.value.endsWith(currentFilterEl.filterValue)) {
            row.hidden = true
          } else if (currentFilterType.label == 'Equals' && !(rowProperty.value == currentFilterEl.filterValue)) {
            row.hidden = true
          } else if (currentFilterType.label === 'Greater then' && !(currentFilterEl.filterValue < rowProperty.value)) {
            row.hidden = true
          } else if (currentFilterType.label === 'Less then' && !(currentFilterEl.filterValue > rowProperty.value)) {
            row.hidden = true
          } else if (currentFilterType.label === 'Greater or equal to' && !(currentFilterEl.filterValue <= rowProperty.value)) {
            row.hidden = true
          } else if (currentFilterType.label === 'Less or equal to' && !(currentFilterEl.filterValue >= rowProperty.value)) {
            row.hidden = true
          } else if (currentFilterType.label === 'preset ' + currentFilterEl.filterValue) { // check if its preset 1 or preset 3 or preset 7 depending on the filterValue            
            let pastDate = new Date().getDate() - currentFilterEl.filterValue
            let lastDateSetter = new Date().setDate(pastDate)
            let lastDate = new Date(lastDateSetter)
            if (!(new Date(rowProperty.value).getTime() >= new Date(lastDate.getFullYear(), lastDate.getMonth(), lastDate.getDate(), 0, 0, 0).getTime() && (new Date(rowProperty.value).getTime() <= new Date().getTime()))) {
              row.hidden = true
            }
          } else if (currentFilterType.label === 'rangeDate') {
            (new Date(rowProperty.value).getTime() >= new Date(currentFilterEl.filterValue.start).getTime() && (new Date(rowProperty.value).getTime() <= new Date(currentFilterEl.filterValue.end).getTime())) ? null : row.hidden = true
          } else {
            row.hidden = false
          }
        })
      })
    } else {
      this.subject$.value.forEach(row => row.hidden = false)
    }
  }

  // push checked checkboxes to row value
  checkboxChecked({ row, checkbox, event }) {
    if (event.checked) {
      row.value.push(checkbox)
    } else {
      let index = row.value.indexOf(checkbox)
      row.splice(index, 1)
    }
    console.log(2);

    this.updateTableValues()
  }

  // clear specific filter rule for a column
  clearFilterRules({ property, filterType, column, filterIndex, wholeColumn }) {
    this.allFilterValues.forEach((el, i) => {
      if (wholeColumn && el.columnProperty === property) {
        this.allFilterValues.splice(i, 1)
        column.filterRules = []
      }
      if (el.columnProperty === property && el.filterType === filterType) {
        this.allFilterValues.splice(i, 1)
        column.filterRules.splice(filterIndex, 1)
      }
    })
    this.checkWhatRowsToFilter(column, true)
    console.log(3);

    this.updateTableValues()
  }

  clearFilterRulesHandler(property, filterType, column, filterIndex, wholeColumn) {
    this.clearFilterRules({ property, filterType, column, filterIndex, wholeColumn })
  }

  // Set label values for checkboxes and radio buttons
  setLabelValues({ row, property }) {
    let labels
    const setLabelsDialog = this.dialog.open(SetLabelsComponent, {
      width: '500px',
      data: {
        labels: labels
      }
    })

    setLabelsDialog.afterClosed().subscribe(result => {
      result.labels ? row[property].data = result.labels : this.snackBarService.showMessage('Please enter label values', 2000)
    })
    console.log(4);

    this.updateTableValues()
  }

  // clear all filters
  clearAllFilters() {
    this.allFilterValues = []
    this.columns.forEach(column => column.filterRules = [])
    this.filterRowsByAllFilters()
    console.log(5);

    this.updateTableValues()
  }

  downloadTableExcelFormat(ShowLabel = true,) {
    // let jsonForExcel = []
    // this.tableDataOnSubmit.rows.forEach(row => {
    //   let obj = {}
    //   for (let key in row) {
    //     // check if the key is a key/value pair with column valid column
    //     if (key !== 'edit' && key !== 'hidden' && key !== 'id') {
    //       obj[key] = row[key].value
    //     }
    //   }
    //   jsonForExcel.push(obj)
    // })

    // let arrData = typeof jsonForExcel != 'object' ? JSON.parse(jsonForExcel) : jsonForExcel;
    // var CSV = 'sep=,' + '\r\n\n';

    // //This condition will generate the Label/Header
    // if (ShowLabel) {
    //   var row = "";

    //   //This loop will extract the label from 1st index of on array
    //   for (var index in arrData[0]) {
    //     //Now convert each value to string and comma-seprated
    //     row += index + ',';
    //   }

    //   row = row.slice(0, -1);

    //   //append Label row with line break
    //   CSV += row + '\r\n';
    // }

    // //1st loop is to extract each row
    // for (var i = 0; i < arrData.length; i++) {
    //   var row = "";

    //   //2nd loop will extract each column and convert it in string comma-seprated
    //   for (var index in arrData[i]) {
    //     row += '"' + arrData[i][index] + '",';
    //   }

    //   row.slice(0, row.length - 1);

    //   //add a line break after each row
    //   CSV += row + '\r\n';
    // }

    // if (CSV == '') {
    //   alert("Invalid data");
    //   return;
    // }

    // //Generate a file name
    // var fileName = "Grid_Item_Table_";

    // //Initialize file format you want csv or xls    
    // var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);

    // var link = document.createElement("a");
    // link.href = uri;

    // //set the visibility hidden so it will not effect on your web-layout
    // // link.style = "visibility:hidden";
    // link.download = fileName + ".csv";

    // //this part will append the anchor tag and remove it after automatic click
    // document.body.appendChild(link);
    // link.click();
    // document.body.removeChild(link);

  }

  setSummeryVisibility(summeryAlwaysVisible: boolean) {
    this.summeryAlwaysVisible = summeryAlwaysVisible
  }

  changeTableTextStyle(event) {
    this.tableTextStyle = event
  }

  publishData() {
    // get updated values of columns
    this.updateTableValues()
    this.tableDataOnSubmit.columns.forEach(column => column.rangeDate ? column.rangeDate = column.rangeDate.value : null)
    console.log('table to show', this.tableDataOnSubmit)
    let json = JSON.stringify(this.tableDataOnSubmit)
    console.log(this.dataService.curFormId);

    const data: any = this.formBuilderFillData.getFormData();
    console.log(data);

    this.httpService.templateUpdateContent(this.dataService.curFormId, json).subscribe(
      res => {
        this.router.navigate(['forms', 'config-form'])
      }
    );
  }

  submitData() {
    this.updateTableValues()
    this.tableDataOnSubmit.columns.forEach(column => column.rangeDate ? column.rangeDate = column.rangeDate.value : null)
    console.log('table to show', this.tableDataOnSubmit)
    let json = JSON.stringify(this.tableDataOnSubmit)

    const data: any = this.formBuilderFillData.getFormData();

    if (data.formId && !this.userFormId) {
      this.httpService.formAddNew(data.formId, data.policeStationId, json, data.candidateName, data.candidateNumber, data.candidateEmail, data.formName).subscribe(
        res => {
          this.router.navigate(['forms', 'config-form'])
        }
      );
      return;
    }

    if (this.userFormId)
      this.httpService.formUpdateContent(this.userFormId, json).subscribe(data => {
        this.router.navigate(['forms', 'config-form'])
      })
  }

  saveTable() {
    this.updateTableValues()
    let wantedTableValues = {
      columns: [...this.columns],
      rows: [...this.subject$.value]
    }
    wantedTableValues.columns.forEach(column => column.rangeDate && column.rangeDate.controls ? column.rangeDate = { start: column.rangeDate.controls.start.value, end: column.rangeDate.controls.end.value } : null)
    sessionStorage.setItem('masterTableData', JSON.stringify(wantedTableValues))
    this.snackBarService.showMessage('Saved Successfully')
    this.changeDetectionRef.detectChanges()
  }

  loadTable() {
    this.disableMergeButton = false
    if (sessionStorage.getItem('masterTableData')) {
      let dataParsed = JSON.parse(sessionStorage.getItem('masterTableData'))
      dataParsed.columns.forEach(column => column.rangeDate ? column.rangeDate = column.rangeDate.value : null)
      this.columns = dataParsed.columns
      this.subject$.next(dataParsed.rows)
      this.loadingTableData = false
      this.snackBarService.showMessage('Loaded Successfully')
      this.changeDetectionRef.detectChanges()
    } else {
      this.snackBarService.showMessage('There Is No Saved Data')
    }
  }

  mergeDataToggle() {
    this.dataForMerge = {
      columns: this.columns,
      rows: this.subject$.value,
      result: {
        column: null,
        rowIndex: null,
        cellNumberDown: 1,
        cellNumberRight: 1,
        cellNumberLeft: 1,
        cellNumberUp: 1
      }
    }
    this.showMergeDataComponent = !this.showMergeDataComponent
  }

  closeMergeDataComponent() {
    this.showMergeDataComponent = false
  }

  mergeDataValues(result) {
    const { column, rowIndex, cellNumberDown, cellNumberRight, cellNumberLeft, cellNumberUp } = result.result

    this.subject$.value.forEach((row, index) => {
      let columnsArr = []
      let wantedColumnIndex
      let startIndexDown
      if (cellNumberLeft || cellNumberRight) {
        this.columns.forEach(column => columnsArr.push(column.property))
        wantedColumnIndex = columnsArr.indexOf(column)
      }

      if (cellNumberDown) {
        this.subject$.value[rowIndex][column].rowSpan = cellNumberDown * 2
        if (index > rowIndex && index < rowIndex + cellNumberDown) {
          if (row && row[column]) {
            row[column].rowSpan = 0
            row[column].visible = false
          }
        }

        if (cellNumberRight) {

          this.subject$.value[rowIndex][column].colSpan = cellNumberRight
          // columnsArr.splice(0, wantedColumnIndex + 1)
          columnsArr.forEach((columnProperty, index) => {
            if (index > wantedColumnIndex && index < wantedColumnIndex + cellNumberRight) {
              console.log(columnProperty);

              this.subject$.value[rowIndex][columnProperty].colSpan = 0
              this.subject$.value[rowIndex][columnProperty].visible = false
              if (this.subject$.value[rowIndex][column].rowSpan > 1) {
                for (let i = 1; i < this.subject$.value[rowIndex][column].rowSpan / 2; i++) {

                  if (this.subject$.value[rowIndex] && this.subject$.value[rowIndex + i][columnProperty]) {
                    this.subject$.value[rowIndex + i][columnProperty].colSpan = 0
                  }

                  if (this.subject$.value[rowIndex + i] && this.subject$.value[rowIndex + i][columnProperty].colSpan < 1) {
                    // console.log(this.subject$.value[rowIndex + i][columnProperty]);
                    this.subject$.value[rowIndex + i][columnProperty].visible = false
                  }
                }
              }
            }
          })
        }
      }



      if (cellNumberUp) {
        this.subject$.value[rowIndex - (cellNumberUp - 1)][column] = { ...this.subject$.value[rowIndex][column], rowSpan: cellNumberUp * 2 }
        this.subject$.value[rowIndex][column].visible = false

        startIndexDown = rowIndex - cellNumberUp + 1
        startIndexDown < 0 ? startIndexDown = 0 : null
        for (let indexDown = startIndexDown; indexDown <= rowIndex; indexDown++) {
          if (startIndexDown < rowIndex && startIndexDown !== rowIndex) {
            this.subject$.value[indexDown][column].visible = false
          }

        }
        this.subject$.value[rowIndex - (cellNumberUp - 1)][column].visible = true

        if (cellNumberLeft) {
          this.subject$.value[rowIndex - (cellNumberUp - 1)][column].colSpan = cellNumberLeft
          let indexToStartFrom = wantedColumnIndex - cellNumberLeft
          columnsArr.forEach((columnProperty, index) => {
            if (index > indexToStartFrom && index < wantedColumnIndex) {

              this.subject$.value[rowIndex][columnProperty].colSpan = 0
              this.subject$.value[rowIndex][columnProperty].visible = false


              if (this.subject$.value[rowIndex - (cellNumberUp - 1)][column].rowSpan > 1) {
                for (let i = 1; i < this.subject$.value[rowIndex - (cellNumberUp - 1)][column].rowSpan / 2; i++) {
                  if (this.subject$.value[rowIndex - i]) {
                    this.subject$.value[rowIndex - i][columnProperty].visible = false
                  }
                }
                //   let numOfRows = this.subject$.value[rowIndex][column].rowSpan
                //   for (let i = 1; i < numOfRows; i++) {
                //     this.subject$.value[rowIndex + i][columnProperty].visible = false
                //   }
              }
            }
          })
        }

        //new
        if (cellNumberRight) {
          this.subject$.value[rowIndex][column].colSpan = cellNumberRight
          // columnsArr.splice(0, wantedColumnIndex + 1)
          console.log(columnsArr);

          columnsArr.forEach((columnProperty, index) => {
            if (index > wantedColumnIndex && index < wantedColumnIndex + cellNumberRight) {
              this.subject$.value[rowIndex][columnProperty].colSpan = 0
              this.subject$.value[rowIndex][columnProperty].visible = false

              if (this.subject$.value[rowIndex - (cellNumberUp - 1)][column].rowSpan > 1) {
                console.log(columnProperty);

                for (let i = 1; i < this.subject$.value[rowIndex - (cellNumberUp - 1)][column].rowSpan / 2; i++) {

                  if (this.subject$.value[rowIndex - i] && this.subject$.value[rowIndex - i][columnProperty]) {
                    this.subject$.value[rowIndex - i][columnProperty].colSpan = 0
                  }

                  if (this.subject$.value[rowIndex - i] && this.subject$.value[rowIndex - i][columnProperty].colSpan < 1) {
                    console.log(this.subject$.value[rowIndex - i][columnProperty]);
                    this.subject$.value[rowIndex - i][columnProperty].visible = false
                  }
                }
              }
            }
          })
        }

        // this.subject$.value[rowIndex - (cellNumberUp - 1)][column] = { ...this.subject$.value[rowIndex][column], rowSpan: cellNumberUp }

        // startIndexDown = rowIndex - cellNumberUp
        // console.log(rowIndex);
        // console.log(cellNumberUp);
        // startIndexDown < 0 ? startIndexDown = 0 : null
        // for (let indexDown = startIndexDown; indexDown <= rowIndex; indexDown++) {
        //   this.subject$.value[indexDown][column].visible = false
        // }
        // this.subject$.value[rowIndex - (cellNumberUp - 1)][column].visible = true


        // if (this.subject$.value[rowIndex - (cellNumberUp - 1)][column].colSpan > 1) {
        //   console.log(this.subject$.value[rowIndex - (cellNumberUp - 1)][column].colSpan);

        //   for( let i = this.subject$.value.indexOf(this.subject$.value[rowIndex - (cellNumberUp - 1)]); i < rowIndex; i++) {
        //    columnsArr.splice(0, wantedColumnIndex + 1)
        //    columnsArr.forEach(columnProperty => {
        //      this.subject$.value[i][columnProperty].visible = false
        //    })

        //   }
        // }
      }

      if (cellNumberLeft) {
        this.subject$.value[rowIndex][column].colSpan = cellNumberLeft
        let indexToStartFrom = wantedColumnIndex - cellNumberLeft
        columnsArr.forEach((columnProperty, index) => {
          if (index > indexToStartFrom && index < wantedColumnIndex) {

            this.subject$.value[rowIndex][columnProperty].colSpan = 0
            this.subject$.value[rowIndex][columnProperty].visible = false


            if (this.subject$.value[rowIndex][column].rowSpan > 1) {
              for (let i = 1; i < this.subject$.value[rowIndex][column].rowSpan / 2; i++) {
                if (this.subject$.value[rowIndex + i]) {
                  this.subject$.value[rowIndex + i][columnProperty].visible = false
                }
              }
              //   let numOfRows = this.subject$.value[rowIndex][column].rowSpan
              //   for (let i = 1; i < numOfRows; i++) {
              //     this.subject$.value[rowIndex + i][columnProperty].visible = false
              //   }
            }
          }
        })
      }


      if (cellNumberRight) {

        this.subject$.value[rowIndex][column].colSpan = cellNumberRight
        columnsArr.splice(0, wantedColumnIndex + 1)
        columnsArr.forEach((columnProperty, index) => {
          if (index < cellNumberRight - 1) {
            this.subject$.value[rowIndex][columnProperty].colSpan = 0
            this.subject$.value[rowIndex][columnProperty].visible = false
          }
          // if (this.subject$.value[rowIndex][column].rowSpan > 1) {
          //   for (let i = 1; i < this.subject$.value[rowIndex][column].rowSpan / 2; i++) {

          //     if (this.subject$.value[rowIndex] && this.subject$.value[rowIndex + i][columnProperty]) {
          //       this.subject$.value[rowIndex + i][columnProperty].colSpan = 0
          //     }

          //     if (this.subject$.value[rowIndex + i] && this.subject$.value[rowIndex + i][columnProperty].colSpan < 1) {
          //       console.log(this.subject$.value[rowIndex + i][columnProperty]);
          //       this.subject$.value[rowIndex + i][columnProperty].visible = false
          //     }
          //   }
          // }
        })
      }
    })
    this.closeMergeDataComponent()
  }

  // mergeDataValues(result) {
  //   console.log(result);
  //   const { column, rowIndex, cellNumberDown, cellNumberRight, cellNumberLeft, cellNumberUp } = result.result

  //   this.subject$.value.forEach((row, index) => {
  //     let columnsArr = []
  //     let wantedColumnIndex
  //     let startIndexDown
  //     if (cellNumberLeft || cellNumberRight) {
  //       this.columns.forEach(column => columnsArr.push(column.property))
  //       wantedColumnIndex = columnsArr.indexOf(column)
  //     }

  //     if (cellNumberDown) {
  //       this.subject$.value[rowIndex][column].rowSpan = cellNumberDown * 2
  //       if (index > rowIndex  && index < rowIndex + cellNumberDown) {
  //         if (row) {
  //           row[column].visible = false
  //         }
  //       }
  //     }
  //     if (cellNumberRight) {
  //       this.subject$.value[rowIndex][column].colSpan = cellNumberRight
  //       columnsArr.splice(0, wantedColumnIndex + 1)
  //       columnsArr.forEach((columnProperty, index) => {
  //         if (index < cellNumberRight - 1) {
  //           this.subject$.value[rowIndex][columnProperty].visible = false
  //         }
  //       })
  //     }
  //     if (cellNumberLeft) {
  //       this.subject$.value[rowIndex][column].colSpan = cellNumberLeft
  //       let indexToStartFrom = wantedColumnIndex - (cellNumberLeft - 1)
  //       columnsArr.forEach((columnProperty, index) => {
  //         if (index > indexToStartFrom && index < wantedColumnIndex) {
  //           this.subject$.value[rowIndex][columnProperty].visible = false
  //         }
  //       })
  //     }
  //     if (cellNumberUp) {
  //       this.subject$.value[rowIndex - 1][column] = { ...this.subject$.value[rowIndex][column], rowSpan: cellNumberUp }
  //       this.subject$.value[rowIndex][column].visible = false
  //       startIndexDown = rowIndex - cellNumberUp + 1
  //       startIndexDown < 0 ? startIndexDown = 0 : null
  //       for (let indexDown = startIndexDown; indexDown < rowIndex; indexDown++) {
  //         if (startIndexDown < rowIndex && startIndexDown !== rowIndex - 1) {
  //           this.subject$.value[indexDown][column].visible = false
  //         }
  //       }
  //       this.subject$.value[rowIndex - 1][column].visible = true
  //     }
  //   })
  //   this.closeMergeDataComponent()
  // }


  // global filter start

  wantedSearchedValues = []
  searchValues = []

  add(event: MatChipInputEvent): void {
    console.log(event);

    if (event.value) {
      this.searchValues.push(event.value);
    }

    let wantedValues = []
    this.searchValues.forEach(el => {
      if (el.split(':').length == 1) {
        let textForSearch = el.split(':')[0];
        // this.dataSource.filter = textForSearch;
        this.wantedSearchedValues.push('all')
        wantedValues.push(textForSearch)
      }
      else {
        let value = el.split(':')[1].trim()
        this.columns.forEach(column => {
          if (column.label === el.split(':')[0]) {
            this.wantedSearchedValues.push(column.property)
          }
        })
        wantedValues.push(value)
      }
    })
    this.dataSource.filter = wantedValues.join('+')

    // Clear the input value
    event.chipInput!.clear();
  }

  remove(value): void {
    const index = this.searchValues.indexOf(value);
    let dataSourceFilters = this.dataSource.filter.split('+')
    let valueSplitted = value.split(':')

    if (index >= 0) {
      this.searchValues.splice(index, 1);
      if (valueSplitted.length > 1) {
        dataSourceFilters.splice(dataSourceFilters.indexOf(valueSplitted[1].trim()), 1);
      } else {
        dataSourceFilters.splice(dataSourceFilters.indexOf(valueSplitted[0].trim()), 1);
      }
      this.dataSource.filter = dataSourceFilters.join('+')
    }

    if (this.searchValues.length < 1) {
      this.dataSource.filter = ''
    }
  }

  appendToSearch(label) {
    let configuredLabelForSearch = `${label}: `
    this.searchCtrl.setValue(configuredLabelForSearch)
  }

  // global filter end


  disableHandler(event) {
    this.CNFDisableEvent.emit(event)
  }

  addEditPoliceStationHandler(event) {
    this.CNFAddEditPoliceStationEvent.emit(event)
  }

  updateFormHandler(event) {
    this.CNFUpdateFormHandler.emit(event)
  }

  addNewFormHandler() {
    this.CNFAddNewFormEvent.emit()
  }
}