import { HttpHeaders } from '@angular/common/http';
import { ChangeDetectorRef, Component, Inject, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs';
import { DocViewerService } from 'src/app/police-app/forms/doc-viewer/services/doc-viewer.service';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'exai-doc-viewer',
  templateUrl: './doc-viewer.component.html',
  styleUrls: ['./doc-viewer.component.scss']
})
export class DocViewerComponent implements OnInit {

  constructor(private route: ActivatedRoute, private docViewer: DocViewerService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private changeDetectionRef: ChangeDetectorRef, private http: HttpClient) {

  }


  loading: boolean = true
  file
  url

  ngOnInit(): void {
    this.url = of(this.data.url)
    console.log(this.docViewer.currentFile.value);

  }


  ngAfterViewInit(): void {
    // this.route.queryParams.subscribe(data => {
    //   this.url = data.url
    // }) 

    this.changeDetectionRef.detectChanges()
  }

  loaded() {
    this.loading = false
    this.changeDetectionRef.detectChanges()
  }

  downloadFile() {
    const headers = new HttpHeaders();

    this.http.get(this.data.url, { headers, responseType: 'blob' as 'json' }).subscribe(
      (response: any) => {
        let dataType = response.type;
        let binaryData = [];
        binaryData.push(response);
        let downloadLink = document.createElement('a');
        downloadLink.href = window.URL.createObjectURL(new Blob(binaryData, { type: dataType }));

        downloadLink.setAttribute('download', this.docViewer.currentFile.value.name);
        document.body.appendChild(downloadLink);
        downloadLink.click();
      }
    )
  }
}
