import { Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SnackBarService } from 'src/app/shared/services/snack-bar.service';
import { ConfirmDialogComponent } from '../confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'exai-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss']
})
export class NewColumnDialogComponent implements OnInit {

  constructor(
    public dialogRefNewColumn: MatDialogRef<NewColumnDialogComponent>, public dialogRefConfirm: MatDialogRef<ConfirmDialogComponent>, private dialog: MatDialog,
    private snackBarService: SnackBarService,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  @ViewChild('chipsInput') chipsInput: ElementRef<HTMLInputElement>;
  selectedItem = null
  typesList = [
    { name: 'Primary Key', type: 'primaryKey' },
    { name: 'Text', type: 'text' },
    { name: 'Number', type: 'number' },
    { name: 'Email', type: 'email' },
    { name: 'Dropdown', type: 'dropdown' },
    { name: 'Date', type: 'date' },
    { name: 'Add a file', type: 'file' },
    { name: 'Time', type: 'time' },
    { name: 'Checkbox', type: 'checkbox' },
    { name: 'Radio Button', type: 'radioButton' },
    { name: 'Currency', type: 'currency' },
    { name: 'Percentage ', type: 'percentage' },
    { name: 'Hyperlink', type: 'hyperlink' },
    { name: 'Math Operations', type: 'mathOperations' },
  ]
  fileTypes = ['.pdf', '.docx', '.xml', '.xlsx', '.svg', '.png', '.jpeg']
  dateFormats = [
    { name: 'M/d/yy', value: 'shortDate', example: '6/15/15' },
    { name: 'MMM/d/y', value: 'mediumDate', example: 'Jun 15, 2015' },
    { name: 'EEEE, MMMM d, y', value: 'fullDate', example: 'Monday, June 15, 2015' }
  ]

  timeFormats = ['12', '24']

  mathOperationsList = ['+', '-', '*', '/']
  mathOperationsValidColumns = ['number']
  mathOperationsData = []
  mathOperationsListToDisplay = []
  mathOperationsColumns = []
  mathOperationsValid = true


  dropdownData = [];
  addOnBlur = true;
  fileSize: number = 2


  readonly separatorKeysCodes = [ENTER, COMMA] as const;

  ngOnInit() {       
    if (this.data.columns) {
      this.data.columns.forEach((column, index) => {
        if (index > 1 && index !== this.data.columnIndex && column.type === 'number') this.mathOperationsColumns.push(column)
      })
    }

    if (this.data.removePrimaryKey) {
      this.typesList.forEach((type, index, array) => {
        if (type.type === 'primaryKey') {
          array.splice(index, 1)
        }
      })
    }
  }

  addOperation(event) {
    this.mathOperationsData.push(event.value)
    this.mathOperationsListToDisplay.push(event.value)
    event.input.value = ''
  }

  removeOperation(index) {
    this.mathOperationsData.splice(index, 1)
    this.mathOperationsListToDisplay.splice(index, 1)
  }

  editOperator() {

  }

  append(columnLabel, columnProperty) {
    this.mathOperationsData.push(columnProperty)
    this.mathOperationsListToDisplay.push({ label: columnLabel, property: columnProperty })
  }

  appendOperator(operator) {
    this.mathOperationsData.push(operator)
    this.mathOperationsListToDisplay.push({ label: operator, property: false })
  }

  onClose(): void {
    this.dialogRefNewColumn.close();
  }

  add(): void {
    const value = this.chipsInput.nativeElement.value.trim()
    let valid = true

    if (!value.length) {
      this.snackBarService.showMessage('You must enter a label', 2000)
      return
    }

    this.data.dropdownData.forEach((el, index) => {
      if (el.name === value) {
        this.snackBarService.showMessage('Cannot have duplicate labels', 2000)
        valid = false
      }
    })

    if (valid) {
      if (this.selectedItem !== null) {
        this.data.dropdownData[this.selectedItem].name = value
      } else {
        this.data.dropdownData.push({ name: value })
      }
      this.chipsInput.nativeElement.value = '';
      this.selectedItem = null
    }
  }

  remove(index, array) {
    this.selectedItem = null
    array.splice(index, 1);
  }

  // Edit chip item 
  editField(index) {
    this.selectedItem = index
    this.chipsInput.nativeElement.value = this.data.dropdownData[index].name
    this.chipsInput.nativeElement.focus()
  }

  // drag and drop chip item
  drop(event: CdkDragDrop<any[]>) {
    moveItemInArray(this.dropdownData, event.previousIndex, event.currentIndex);
  }

  confirm() {

    if (this.data.type === 'mathOperations') {
      if (!this.mathOperationsData.length) {
        this.snackBarService.showMessage('Please Add a Formula', 3000)
        return
      }
      this.mathOperationsValid = true
      this.data.mathOperationsData = this.mathOperationsData
      this.mathOperationsListToDisplay.forEach((column, index) => {
        if (column.property) {
          if (index % 2 !== 0) {
            this.mathOperationsValid = false
            this.snackBarService.showMessage('Not Valid Formula', 3000)
          }
        }
      })
    }
    if (!this.mathOperationsValid) return

    if (this.data.type === 'file') {
      if (!this.fileSize) {
        this.snackBarService.showMessage('Please Set Maximum File Size', 3000)
        return
      } else {
        this.data.dropdownData = this.fileTypes
        this.data.fileSize = this.fileSize
      }
    }

    if (this.data.columnName === undefined || this.data.columnName.length < 1) {
      this.snackBarService.showMessage('Please enter a column name')
    } else if (this.data.type === undefined) {
      this.snackBarService.showMessage('Please select a type')
    } else if (this.data.type === 'dropdown' && !this.data.dropdownData) {
      this.snackBarService.showMessage('Please enter a value')
    }


    if (this.data.fromColumn && this.data.type !== this.data.fromColumn.type) {
      let message = 'You have data in this column. Are you sure you want to edit it?'
      let answer: boolean
      const dialogRefConfirm = this.dialog.open(ConfirmDialogComponent, {
        width: '450px',
        data: {
          message: message,
          answer: answer
        }
      });

      dialogRefConfirm.afterClosed().subscribe(result => {
        if (result.answer) {
          let wantedIndex
          this.data.columns.forEach((column, index) => {
            if (column.property === this.data.fromColumn.property) {
              wantedIndex = index
            }
          })
          this.dialogRefNewColumn.close(this.data)
          this.data.columns.splice(wantedIndex, 1, { label: '', property: `${wantedIndex}`, type: null, visible: true, id: wantedIndex, filterRules: [] })
        }
      })
    } else {
      this.dialogRefNewColumn.close(this.data)
    }
  }
}
