import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AddHeaderNameComponent } from '../add-header-name/add-header-name.component';

@Component({
  selector: 'exai-add-header-rows',
  templateUrl: './add-header-rows.component.html',
  styleUrls: ['./add-header-rows.component.scss']
})
export class AddHeaderRowsComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<AddHeaderNameComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {}


  ngOnInit(): void {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
