import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddHeaderRowsComponent } from './add-header-rows.component';

describe('AddHeaderRowsComponent', () => {
  let component: AddHeaderRowsComponent;
  let fixture: ComponentFixture<AddHeaderRowsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddHeaderRowsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddHeaderRowsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
