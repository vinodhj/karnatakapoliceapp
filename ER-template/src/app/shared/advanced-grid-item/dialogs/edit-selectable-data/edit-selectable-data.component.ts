import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, } from '@angular/material/dialog';
import { SnackBarService } from 'src/app/shared/services/snack-bar.service';

@Component({
  selector: 'exai-edit-selectable-data',
  templateUrl: './edit-selectable-data.component.html',
  styleUrls: ['./edit-selectable-data.component.scss']
})
export class EditSelectableDataComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<EditSelectableDataComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private snackBarService: SnackBarService
  ) { }

  @ViewChild('chipsInput') chipsInput: ElementRef<HTMLInputElement>;



  dropdownData = [];
  selectedItem = null
  addOnBlur = true
  readonly separatorKeysCodes = [ENTER, COMMA] as const;


  ngOnInit(): void {
  }

  // Edit chip item 
  editField(label, index) {
    this.selectedItem = index
    this.chipsInput.nativeElement.value = label.name
    this.chipsInput.nativeElement.focus()
  }

  // drag and drop chip item
  drop(event: CdkDragDrop<any[]>) {
    moveItemInArray(this.data.dropdownData, event.previousIndex, event.currentIndex);
  }

  add(event): void {

    const value = this.chipsInput.nativeElement.value.trim()
    let valid = true
    
    if (!value.length) {
      this.snackBarService.showMessage('You must enter a label', 2000)
      return
    }

    this.data.dropdownData.forEach(item => {
      if (item.name === value && this.selectedItem === null) {        
        this.snackBarService.showMessage('Cannot have duplicate labels', 3000)
        valid = false
        this.chipsInput.nativeElement.value = '';
        return
      }
    })

    if (valid) {
      if (this.selectedItem !== null) {
        this.data.dropdownData[this.selectedItem].name = value
      } else {
        this.data.dropdownData.push({name: value})
      }
      // Clear the input value        
      this.chipsInput.nativeElement.value = '';
    } 
    
    this.selectedItem = null
  }

  remove(index) {
    this.selectedItem = null
    this.data.dropdownData.splice(index, 1);
  }

  confirm() {
    this.snackBarService.showMessage("Labels successfully changed", 2000)
    this.dialogRef.close(this.data)
  }
}
