import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditSelectableDataComponent } from './edit-selectable-data.component';

describe('EditSelectableDataComponent', () => {
  let component: EditSelectableDataComponent;
  let fixture: ComponentFixture<EditSelectableDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditSelectableDataComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditSelectableDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
