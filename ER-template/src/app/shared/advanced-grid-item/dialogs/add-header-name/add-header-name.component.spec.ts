import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddHeaderNameComponent } from './add-header-name.component';

describe('AddHeaderNameComponent', () => {
  let component: AddHeaderNameComponent;
  let fixture: ComponentFixture<AddHeaderNameComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddHeaderNameComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddHeaderNameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
