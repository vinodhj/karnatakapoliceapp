import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'exai-add-header-name',
  templateUrl: './add-header-name.component.html',
  styleUrls: ['./add-header-name.component.scss']
})
export class AddHeaderNameComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<AddHeaderNameComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {}

  ngOnInit(): void {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
  
}
