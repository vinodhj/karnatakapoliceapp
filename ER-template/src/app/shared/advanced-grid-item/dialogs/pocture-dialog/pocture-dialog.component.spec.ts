import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PoctureDialogComponent } from './pocture-dialog.component';

describe('PoctureDialogComponent', () => {
  let component: PoctureDialogComponent;
  let fixture: ComponentFixture<PoctureDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PoctureDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PoctureDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
