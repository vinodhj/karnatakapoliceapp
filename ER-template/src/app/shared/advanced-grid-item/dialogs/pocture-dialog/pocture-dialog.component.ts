import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'exai-pocture-dialog',
  templateUrl: './pocture-dialog.component.html',
  styleUrls: ['./pocture-dialog.component.scss']
})
export class PoctureDialogComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,) { }

  ngOnInit(): void {
  }

}
