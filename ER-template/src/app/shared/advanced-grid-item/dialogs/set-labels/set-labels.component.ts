import { Component, ElementRef, Inject, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SnackBarService } from 'src/app/shared/services/snack-bar.service';

@Component({
  selector: 'exai-set-labels',
  templateUrl: './set-labels.component.html',
  styleUrls: ['./set-labels.component.scss']
})
export class SetLabelsComponent {
  
  constructor(
    public dialogRef: MatDialogRef<SetLabelsComponent>,
    private _snackBar: MatSnackBar,
    private snackBarService: SnackBarService,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  types = new FormControl();
  @ViewChild('chipsInput') chipsInput: ElementRef<HTMLInputElement>;
  selectedItem = null

  onNoClick(): void {
    this.dialogRef.close();
  }

  addOnBlur = true;
  readonly separatorKeysCodes = [ENTER, COMMA] as const;
  labels = [];

  add(): void {
    const value = this.chipsInput.nativeElement.value.trim()
    let valid = true
    // Add our fruit

    this.labels.forEach(label => {
      if (label.name === value && this.selectedItem === null) {
        this.snackBarService.showMessage('Cannot have duplicate labels', 3000)
        valid = false
      }
    })

    if (valid) {
      
      if (value.length) {
        if (this.selectedItem !== null) {
          this.labels.splice(this.selectedItem, 1, { name: value })
        } else {
          this.labels.push({ name: value });
        }

        this.data.labels = this.labels
        this.chipsInput.nativeElement.value = '';
        this.selectedItem = null

      } else {
        this.openSnackBar('You must enter a label')
        return
      }
    }

  }

  remove(index, array): void {
    this.selectedItem = null
    array.splice(index, 1);
  }

  editField(element, index) {
    this.selectedItem = index    
    this.chipsInput.nativeElement.value = this.labels[index].name
    this.chipsInput.nativeElement.focus()
  }

  openSnackBar(message: string) {
    this._snackBar.open(message, 'Confirm', {
      duration: 5000
    });
  }

  drop(event: CdkDragDrop<any[]>) {
    moveItemInArray(this.labels, event.previousIndex, event.currentIndex);
  }

  confirm(data) {
    if (this.data.type === 'file' )
    this.data.labels = this.labels
    this.snackBarService.showMessage('Labels have been successfully created', 2000)
  }
}
