import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'exai-create-hyperlink',
  templateUrl: './create-hyperlink.component.html',
  styleUrls: ['./create-hyperlink.component.scss']
})
export class CreateHyperlinkComponent  {

  constructor(
    public dialogRef: MatDialogRef<CreateHyperlinkComponent>,
    @Inject(MAT_DIALOG_DATA) public data
    ) { }

    urlRegex: string = '[Hh][Tt][Tt][Pp][Ss]?:\/\/(?:(?:[a-zA-Z\u00a1-\uffff0-9]+-?)*[a-zA-Z\u00a1-\uffff0-9]+)(?:\.(?:[a-zA-Z\u00a1-\uffff0-9]+-?)*[a-zA-Z\u00a1-\uffff0-9]+)*(?:\.(?:[a-zA-Z\u00a1-\uffff]{2,}))(?::\d{2,5})?(?:\/[^\s]*)?'

    check(event) {
      console.log(event) 
    }
}
