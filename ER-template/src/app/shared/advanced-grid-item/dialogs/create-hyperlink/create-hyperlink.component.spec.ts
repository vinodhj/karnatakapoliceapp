import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateHyperlinkComponent } from './create-hyperlink.component';

describe('CreateHyperlinkComponent', () => {
  let component: CreateHyperlinkComponent;
  let fixture: ComponentFixture<CreateHyperlinkComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateHyperlinkComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateHyperlinkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
