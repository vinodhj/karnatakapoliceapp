import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { CreateHyperlinkComponent } from '../../dialogs/create-hyperlink/create-hyperlink.component';

@Component({
  selector: 'exai-hyperlink',
  templateUrl: './hyperlink.component.html',
  styleUrls: ['./hyperlink.component.scss']
})
export class HyperlinkComponent implements OnInit {

  constructor(
    public dialog: MatDialog,
  ) { }

  @Input() links
  @Input() editState
  @Input() readOnly
  @Input() tableTextStyle

  get _filePositioning() {
    let position
    if (this.tableTextStyle === 'justify' || this.tableTextStyle === 'text-left') {
      position = 'flex-start'
    } else if (this.tableTextStyle === 'text-right') {
      position = 'flex-end'
    } else {
      position = 'center'
    }
    return position
  }

  ngOnInit(): void {
  }

  createHyperlink() {
    const dialogRef = this.dialog.open(CreateHyperlinkComponent, {
      width: '450px',
      data: {
        name: '',
        link: ''
      },
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log(result);
        
        this.links.push(result)
      }
      return
    });
  }

  removeLinkHandler(index) {
    this.links.splice(index, 1)
  }
}


