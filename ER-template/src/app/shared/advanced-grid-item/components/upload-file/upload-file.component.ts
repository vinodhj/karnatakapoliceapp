
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { Component, ElementRef, HostListener, Input, OnInit, Output, ViewChild, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { forkJoin, of } from 'rxjs';
import { catchError, concatMap, finalize, map } from 'rxjs/operators';
import { HttpService } from 'src/@exai/services/http.service';
import { SnackBarService } from 'src/app/shared/services/snack-bar.service';
import { getUniqueId } from 'src/app/utils/randomNumber';
import * as uuid from "uuid";
import { Router } from '@angular/router';
import { DocViewerService } from 'src/app/police-app/forms/doc-viewer/services/doc-viewer.service';
import { MatDialog } from '@angular/material/dialog';
import { DocViewerComponent } from '../../dialogs/doc-viewer/doc-viewer.component';
import { PoctureDialogComponent } from '../../dialogs/pocture-dialog/pocture-dialog.component';


export interface UploadFilesInputDto {
  entityId: number;
  entityType: string;
  path: string | null;
  formBuilderElementId: string;
  formBuilderElementType: string;
  fileAttachments: FileAttachmentInputDto[];
}


export interface FileAttachmentInputDto {
  file: File | null;
}


@Component({
  selector: 'exai-upload-file',
  templateUrl: './upload-file.component.html',
  styleUrls: ['./upload-file.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: UploadFileComponent,
      multi: true
    }
  ]
})


export class UploadFileComponent {
  @ViewChild('fileInput') fileInput!: any;
  @Input() item!: any;
  @Input() progress!: any;
  @Input() insideForm!: boolean;
  onChange!: Function;
  @Input() files;
  uploadingFiles = false;
  @Input() previewMode: boolean;
  @Input() url: string;
  @Input() removeUrl: string;
  @Input() label: string;
  @Input() name: string;
  @Input() accept: Array<string>;
  @Input() multiple: boolean;
  @Input() showFileList: boolean;
  @Input() autoUpload: boolean;
  uploadedFilesObject = [];
  @Input() fileTypes
  @Input() showInput
  @Input() tableTextStyle = ''
  @Input() showCancelButton
  @Input() fileSize

  loading = false

  get enteredFiles() {
    let filesInString = ''
    if (this.files) {

      this.files.forEach((file, index) => {
        if (file) {
          filesInString += file.name
          if (index < this.files.length - 1) {
            filesInString += ', '
          }
        }
      })
    }
    return filesInString
  }

  get _filePositioning() {
    let position
    if (this.tableTextStyle === 'justify' || this.tableTextStyle === 'text-left') {
      position = 'flex-start'
    } else if (this.tableTextStyle === 'text-right') {
      position = 'flex-end'
    } else {
      position = 'center'
    }
    return position
  }


  Url: string = 'http://www.pdf995.com/samples/pdf.pdf'
  urlSafe: SafeResourceUrl;

  constructor(
    private host: ElementRef<HTMLInputElement>, 
    private http: HttpClient, private HttpService: HttpService, 
    private snackBarServiece: SnackBarService, 
    public sanitizer: DomSanitizer, 
    private changeDetectionRef: ChangeDetectorRef,
    private router: Router,
    private docViewer: DocViewerService,
    private dialog: MatDialog
  ) {}

  createSafeUrls(string) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(this.Url + string + '&embedded=true');
  }

  @Output() filesUploaded: EventEmitter<any> = new EventEmitter()

  // @HostListener('change', ['$event.target.files']) 

  viewFile(file) {
    this.docViewer.currentFile.next(file)
    // this.router.navigate(['forms', 'doc-viewer'],  { queryParams: { url: file.url }})

      const dialogRef = this.dialog.open(DocViewerComponent, {
        width: '95%',
        height: '97%',
        data: {
          url: file.url
        },
      });
  
      dialogRef.afterClosed().subscribe();
    
  }

  openPictureDialog(file) {
    const dialogRef = this.dialog.open(PoctureDialogComponent, {
      
      data: {
        url: file.imageContent
      },
    });

    dialogRef.afterClosed().subscribe();
  }

  uploadFilesFromInput(event) {
    const files = event.target.files;
    this.FilesDropped(files);

    //reset value so it can be run multiple times
    event.target.value = '';
  }

  uploadedFile

  FilesDropped(files) {

    const file = files[0];

    // file["attachmentId"] = uuid.v4();

    const reader = new FileReader();

    reader.onload = () => {

      var image: any = new Image();

      image.src = reader.result;

      image.onload = () => {
        this.loading = false
        this.uploadedFile = { imageContent: reader.result, type: 'image' };
        this.filesUploaded.emit(this.uploadedFile)

      }
    }

    reader.readAsDataURL(file);
  }

  remove(index) {
    this.files.splice(index, 1)
  }

  emitFiles(files) {
    let fileIsImage
    this.loading = true
    const formData = new FormData();

    // Append files to the virtual form.
    for (const file of files) {
      fileIsImage = false
      // Check if file is image
      if (file.type.includes('image')) {
        fileIsImage = true
        console.log(file);

        this.FilesDropped([file])
      } else {
        this.changeDetectionRef.detectChanges()
        console.log(file);
        if (file.size <= this.fileSize * 1000000) {
          file.id = uuid.v4()
          formData.append('files', file)
        } else {
          this.snackBarServiece.showMessage(`Maximum Size to Upload is ${this.fileSize} MB`, 3000)
          this.loading = false
          return
        }
      }
    }

    // Send it.
    if (!fileIsImage) {
      return this.HttpService.tableFilesUpload(formData).subscribe((data: any) => {
        let valid = true
        data.forEach(file => {
          let splittedName = file.name.split('.')

          if (!this.fileTypes.toString().includes(splittedName[splittedName.length - 1])) {
            this.snackBarServiece.showMessage(`Only allowed files are ${this.fileTypes.join(', ')}! Please select file`, 5000)
            valid = false
          }
  
        })
        this.loading = false
        if (valid) {
          this.filesUploaded.emit(data)
        }
      })
    }
  }

  attachIdsToFiles(files) {
    for (const file of files) {
      file.attachmentId = uuid.v4();;
    }
  }
  matchFilesWithUrls(filesList, files) {
    const requests = [];
    filesList.map((fileResponse) => {
      let fileToUpload;
      for (let i = 0; i < files.length; i++) {
        let file = files.item(i);
        if (file.type !== 'image' && fileResponse.attachmentId === file.attachmentId) {
          fileToUpload = file
        }
      }
      const formData = new FormData();
      formData.append("file", fileToUpload);
      formData.append("Content-Type", fileToUpload.type);
      requests.push(this.http.put(fileResponse.presignedUrl.url, formData.get('file')).pipe(
        catchError(err => of({ isError: true, error: err, attachmentId: fileResponse.attachmentId })),
      ));
    })
    return requests;
  }
  uploadFilesToDb(files) {
    const fileAttachments = [];
    // Loop through files
    for (let i = 0; i < files.length; i++) {
      let file = files.item(i);
      fileAttachments.push({
        attachmentId: file.attachmentId,
        fileName: file.name
      })
    }
    if (!fileAttachments.length) return;
    this.uploadingFiles = true;
    this.http.put(
      'https://jsk1gc4ms9.execute-api.us-east-2.amazonaws.com/Prod/api/files/generate-presigned-urls', { fileAttachments })
      .pipe(
        concatMap((filesList: any) => forkJoin(this.matchFilesWithUrls(filesList, files)).pipe(
          map((results: any) => {
            const formattedResults = [];
            results?.forEach((result, index) => {
              if (result?.isError) {
                const errorFile = this.files.find(file => file.attachmentId === result.attachmentId)
                errorFile.error = true;
                errorFile.errorMessage = "File Upload Fail";
              } else {
                formattedResults.push(fileAttachments[index]);
              }
            })
            return formattedResults;
          })
        )),
        finalize(() => this.uploadingFiles = false)
      ).subscribe((results) => {
        this.uploadedFilesObject.push(...results);
        //set form result
        this.onChange(this.uploadedFilesObject);
      })
  }
  filesDropped(files) {
    // create preview for file
    for (const file of files) {
      file.id = getUniqueId(1);
      const reader = new FileReader();
      reader.onload = () => {
        this.files.push({ id: file.id, attachmentId: file.attachmentId, name: file.name, preview: reader.result, progress: 0 });
      }
      reader.readAsDataURL(file);
    }
  }

  writeValue(value: null) {
    this.uploadedFilesObject = value || [];
    // clear file input
    this.host.nativeElement.value = '';
    this.files = value || [];
  }
  registerOnChange(fn: Function) {
    this.onChange = fn;
  }
  registerOnTouched(fn: Function) {
  }
  disabled: boolean = false;
  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }
}