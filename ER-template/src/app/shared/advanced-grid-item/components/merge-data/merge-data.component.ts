import { Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';

@Component({
  selector: 'exai-merge-data',
  templateUrl: './merge-data.component.html',
  styleUrls: ['./merge-data.component.scss']
})
export class MergeDataComponent {

  vertical: string[] = ['Downwards', 'Upwards']
  horizontal: string[] = ['Left', 'Right']
  mergeActions: string[] = ['Merge', 'Unmerge']

  mergeAction: string = 'Merge'

  mergeDirections = {
    upDown: this.vertical[0],
    leftRight: this.horizontal[0]
  }

  startingColumn: string
  endColumn: string


  get validDirectionNumbers() {
    const { cellNumberDown, cellNumberRight, cellNumberLeft, cellNumberUp } = this.data.result
    if (cellNumberDown < 1 || cellNumberRight < 1 || cellNumberLeft < 1 || cellNumberUp < 1) {
      return true
    } else {
      return false
    }
  }

  get columnsForSelection() {
    return this.data.columns.filter(column => column.property !== 'firstColumn' && column.property !== 'actionsColumn')
  }

  get isMergedCell() {
    if (this.startingColumn && this.data.result.rowIndex >= 0) {
      let columnProperty = this.columnsForSelection.filter(column => column.label === this.startingColumn)[0].property
      let row = this.data.rows[this.data.result.rowIndex][columnProperty]
      if ('mergeDirectionsDown' in row || 'mergeDirectionsUp' in row || 'mergeDirectionsLeft' in row || 'mergeDirectionsRight' in row) {
        return true
      }
    } else {

      return false
    }
  }

  @Input() data

  @Output() applyMergeDataValuesEvent = new EventEmitter()
  @Output() unmergeDataValuesEvent = new EventEmitter()
  @Output() closeMergeDataComponentEvent = new EventEmitter()

  constructor() { }

  applyMergeDataValuesHandler() {
    console.log(this.data);

    this.data.result.column = this.data.columns.filter(column => column.label === this.startingColumn)[0].property
    this.applyMergeDataValuesEvent.emit(this.data)

  }

  closeMergeDataComponentHandler() {
    this.closeMergeDataComponentEvent.emit()
  }


  setDirections(column) {
    let startingColumn
    let endColumn
    this.data.columns.forEach((column, index) => {
      if (column.label === this.startingColumn) {
        startingColumn = index
        this.data.result.column = column.property
      }

      if (column.label === this.endColumn) endColumn = index

      if (startingColumn < endColumn) {
        this.data.result.cellNumberRight = endColumn - startingColumn + 1
      } else if (startingColumn > endColumn) {
        this.data.result.cellNumberLeft = startingColumn - endColumn + 1
      }
    })
  }

  unmergeDataValuesHandler() {
    let payload = {
      event: this.data.event,
      column: this.startingColumn,
      columnProperty: this.data.columns.filter(column => column.label === this.startingColumn)[0].property,
      row: this.data.result.rowIndex,
    }
    this.unmergeDataValuesEvent.emit(payload)
  }

}
