import { SelectionModel } from '@angular/cdk/collections';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { ConnectionPositionPair } from '@angular/cdk/overlay';
import { AfterViewInit, ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatChipInputEvent } from '@angular/material/chips';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatFormFieldDefaultOptions, MAT_FORM_FIELD_DEFAULT_OPTIONS } from '@angular/material/form-field';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { UntilDestroy } from '@ngneat/until-destroy';
import { BehaviorSubject, forkJoin, Observable, of } from 'rxjs';
import { concatMap, filter, finalize, switchMap } from 'rxjs/operators';
import { fadeInUp400ms } from 'src/@exai/animations/fade-in-up.animation';
import { stagger40ms } from 'src/@exai/animations/stagger.animation';
import { HttpService } from 'src/@exai/services/http.service';
import { FormBuilderElementsService } from 'src/app/form-builder/services/form-builder-elements.service';
import { FormBuilderFillDataService } from 'src/app/form-builder/services/form-builder-fill-data.service';
import { FormBuilderIndexeddbService } from 'src/app/form-builder/services/form-builder-indexeddb.service';
import { AddNewFormDialogComponent } from 'src/app/police-app/forms/create-new-form/add-new-form-dialog/add-new-form-dialog.component';
import { DataService } from 'src/app/police-app/services/data-service.service';
import { AddColumnDialogComponent } from 'src/app/shared/advanced-grid-item/dialogs/add-column-dialog/add-column-dialog.component';
import { ConfirmDialogComponent } from 'src/app/shared/advanced-grid-item/dialogs/confirm-dialog/confirm-dialog.component';
import { EditSelectableDataComponent } from 'src/app/shared/advanced-grid-item/dialogs/edit-selectable-data/edit-selectable-data.component';
import { NewColumnDialogComponent } from 'src/app/shared/advanced-grid-item/dialogs/new-column-dialog/dialog.component';
import { SetLabelsComponent } from 'src/app/shared/advanced-grid-item/dialogs/set-labels/set-labels.component';
import { DownloadToExcelService } from 'src/app/shared/advanced-grid-item/services/download-to-excel.service';
import { Role } from 'src/app/shared/enums/role.enum';
import { SnackBarService } from 'src/app/shared/services/snack-bar.service';
import { getUniqueId } from 'src/app/utils/randomNumber';
import { AddHeaderNameComponent } from '../../dialogs/add-header-name/add-header-name.component';
import { AddHeaderRowsComponent } from '../../dialogs/add-header-rows/add-header-rows.component';




@UntilDestroy()
@Component({
  selector: 'exai-table-form',
  templateUrl: './table-form.component.html',
  styleUrls: ['./table-form.component.scss'],
  animations: [
    fadeInUp400ms,
    stagger40ms
  ],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        appearance: 'standard'
      } as MatFormFieldDefaultOptions
    },
    FormBuilderElementsService
  ]

})
export class TableFormComponent implements OnInit, AfterViewInit {
  @Input() item;
  @Input() initTableValues
  @Input() columns = [];
  @Input() tableRowsData

  @Output() submitTableDataEvent = new EventEmitter()
  @Output() removeEmitted = new EventEmitter()

  @ViewChild(MatPaginator) public paginator: MatPaginator;
  @ViewChild('print') print

  dataSource
  layoutCtrl = new FormControl('boxed'); // check if i need this 

  subject$: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);
  data$: Observable<any[]> = this.subject$.asObservable();

  columnsFiltered = []
  rowsValues = []
  rowsFilteredByColumnsVisibility = []

  tableDataOnSubmit = {
    columns: null,
    rows: null
  }
  tableHeaderRows = []

  showTable: boolean = false
  showEditSelected: boolean = false

  pageSize: number = 10;
  pageSizeOptions: number[] = [5, 10, 20, 50];
  selection = new SelectionModel<any>(true, []);
  searchCtrl = new FormControl();

  filterByText: string = ''
  filterType: string = ''
  allFilterValues = []

  summeryAlwaysVisible?: boolean
  tableTextStyle: string = 'justify'

  dataForMerge
  showMergeDataComponent = false

  colNumberFromParm = false
  loadingTableData = true

  disableMergeButton = false;
  updateDisabled = false

  loadedData: boolean = true;
  templateId: number;
  // Custom

  userFormId
  currentStatus
  formName
  edit
  userRole = this.httpService.getRole();
  templateColumns

  // cdk overlay position for merge data component
  positionPairs: ConnectionPositionPair[] = [
    {
      offsetX: -14,
      offsetY: -14,
      originX: 'end',
      originY: 'bottom',
      overlayX: 'start',
      overlayY: 'bottom',
      panelClass: null,
    },
  ];


  readonly separatorKeysCodes = [ENTER, COMMA] as const;

  constructor(
    private dialog: MatDialog,
    private snackBarService: SnackBarService,
    private changeDetectionRef: ChangeDetectorRef,
    private dataService: DataService,
    private activatedRoute: ActivatedRoute,
    private formBuilderFillData: FormBuilderFillDataService,
    private router: Router,
    private httpService: HttpService,
    private masterTableToExcel: DownloadToExcelService,
    private formBuilderIndexeddb: FormBuilderIndexeddbService,
    private formBuilderElementsService: FormBuilderElementsService,
    private route: ActivatedRoute
  ) { }

  get visibleColumns() {
    if (this.columns) {
      return this.columns.filter((column) => column.visible).map((column) => column.property)
    }
  }

  get rowNumber() {
    if (this.subject$.value) {
      return this.subject$.value.filter(row => !row.hidden).map(row => row)
    } else {
      return []
    }
  }

  get hasSavedData() {
    return sessionStorage.getItem('masterTableDataCreateNew')
  }

  get userCantChange(): boolean {
    return [Role.User].includes(this.userRole);
  }

  get canApproveReject() {
    return this.currentStatus === 'Submitted'
  }

  ngOnInit() {
    this.dataService.curFormId = +this.activatedRoute.snapshot.paramMap.get('id')

    this.edit = this.activatedRoute.snapshot.queryParamMap.get('edit')

    if (this.item.tableValue && !this.item.tableValue.columns) {
      this.columns = []
      this.columns.push({ label: 'Actions', property: 'actionsColumn', type: 'actionsButtons', visible: true, id: getUniqueId(2) })
      this.columns.unshift({ label: 'Checkbox', property: 'firstColumn', type: 'checkbox', visible: true, id: getUniqueId(2) })
      this.subject$.next([])
    }

    this.dataSource = new MatTableDataSource();
    this.dataSource.filterPredicate = (data, filters) => {
      const matchFilter = [];
      const filterArray = filters.split('+');
      const columns = (<any>Object).values(data);
      const columnLabels = [...this.columns.filter(column => column?.visible && !column?.property?.includes(`actions`)).map(({ property }) => property) as string[]];
      filterArray.forEach(filter => {
        let formattedFilter = filter.trim().toLowerCase()
        const customFilter = [];
        columns.forEach(column => {
          this.wantedSearchedValues.forEach(colProperty => {
            if (colProperty == 'all') {
              for (let key in data) {
                let formattedDataValue = ((('' + data[key]).toLowerCase()).trim())
                if (columnLabels.indexOf(key) >= 0 && (formattedDataValue.includes(formattedFilter) || formattedDataValue == (formattedFilter))) {
                  customFilter.push(true)
                }
              }
            } else {
              let formattedDataColProperty = (('' + data[colProperty].value).toLowerCase()).trim()
              customFilter.push(formattedDataColProperty.includes(formattedFilter))
            }
          })
        });
        if (data.summery.length > 0) {
          let formattedSummery = data.summery.toLowerCase().trim()
          customFilter.push(formattedSummery.includes(formattedFilter))
        }
        matchFilter.push(customFilter.some(Boolean)); // OR
      });
      return matchFilter.every(Boolean); // AND
    }

    this.data$.pipe(
      filter<any[]>(Boolean)
    ).subscribe(rows => {
      if (this.dataSource && rows) {
        this.dataSource.data = rows;
      }
    })

    this.changeDetectionRef.detectChanges();
  }

  ngAfterViewInit() {

    this.dataSource.paginator = this.paginator

    if (this.columns.length < 2) {

      this.loadingTableData = false
      let colNumber = this.activatedRoute.snapshot.queryParamMap.get('colNumber')
      this.colNumberFromParm = true
      let indexNumber = this.columns.length ? this.columns.length : 0
      let forLength = this.columns.length + +colNumber ? this.columns.length + +colNumber : +colNumber

      this.columns = []
      this.subject$.next([])
      indexNumber = 1
      this.columns.push({ label: 'Actions', property: 'actionsColumn', type: 'actionsButtons', visible: true, id: getUniqueId(2) })
      this.columns.unshift({ label: 'Checkbox', property: 'firstColumn', type: 'checkbox', visible: true, id: getUniqueId(2) })

      for (let i = indexNumber; i <= forLength; i++) {
        let uniqueProperty = getUniqueId(2)
        let singleColumn = { label: '', property: uniqueProperty, type: null, visible: true, id: uniqueProperty, filterRules: [], addedData: false }
        this.columns.push(singleColumn)
      }

      if (this.initTableValues && Object.keys(this.initTableValues).length > 0) {
        let tableValuesInString = JSON.stringify(this.initTableValues)
        let tableValues = JSON.parse(tableValuesInString)
        this.tableDataOnSubmit = tableValues
        this.columns = tableValues.columns
        this.tableHeaderRows = tableValues.tableHeaderRows

        this.subject$.next(tableValues.rows)
      } else {
        if (!this.colNumberFromParm) {
          this.columns = []
          this.subject$.next([])
        }
      }
      this.changeDetectionRef.detectChanges()
    }

    if (this.item.tableValue.columns && !this.item.tableValue.columns.length) {
      this.columns = []
      this.columns.push({ label: 'Actions', property: 'actionsColumn', type: 'actionsButtons', visible: true, id: getUniqueId(2) })
      this.columns.unshift({ label: 'Checkbox', property: 'firstColumn', type: 'checkbox', visible: true, id: getUniqueId(2) })
      this.subject$.next([])
    }

    this.changeDetectionRef.detectChanges()
  }

  uploadRowFiles({ event, row, columnProperty }) {
    let currentRowValue = row[columnProperty].value
    if (Array.isArray(event)) {
      currentRowValue.push(...event)
    } else {
      let arr = []
      arr.push(event)
      currentRowValue.push(...arr)
    }

    this.changeDetectionRef.detectChanges()
  }

  onFilterChange(value: string) {
    value = value.trim();
    value = value.toLowerCase();
    this.dataSource.filter = value;
  }

  drop(event: CdkDragDrop<string[]>) {
    if (event.previousIndex > 0) {
      moveItemInArray(this.columns, event.previousIndex, event.currentIndex);
    }
    this.updateTableValues()
  }


  createTable({ newTable }) {
    if (this.columns.length > 2 && newTable) {
      let message = 'You have data in table. Are you sure you want to create a new one?'
      const confirmDialog = this.dialog.open(ConfirmDialogComponent, {
        width: '500px',
        data: {
          message: message,
        }
      });

      confirmDialog.afterClosed().subscribe(result => {
        if (result) {
          this.resetTable()
          console.log(result);

          this.addColumnNumber(newTable)
        }
      })
    } else {
      this.addColumnNumber(newTable)
    }
  }

  createTableHeader() {
    let enteredNumberColumns = 1
    const addColumnDialog = this.dialog.open(AddHeaderRowsComponent, {
      width: '500px',
      data: { enteredNumberColumns: enteredNumberColumns }
    });

    addColumnDialog.afterClosed().subscribe(result => {
      for (let headerRow = 0; headerRow < result; headerRow++) {
        let wantedHeaderRow = {}
        let uniqueIds = []
        for (let i = 0; i < this.columns.length; i++) {
          let uniqueId = getUniqueId(2)
          uniqueIds.push(uniqueId)
          wantedHeaderRow[this.columns[i].property] = {
            visible: true,
            columnProperty: this.columns[i].property,
            name: '',
            colSpan: 1,
            rowSpan: 1,
            id: uniqueId
          }
        }
        wantedHeaderRow["uniqueIds"] = uniqueIds
        this.tableHeaderRows.push(wantedHeaderRow)
        console.log(this.tableHeaderRows);

      }
      this.columns[0].visible = !this.columns[0].visible
      setTimeout(() => {
        this.columns[0].visible = !this.columns[0].visible
      }, 0)
    })
    this.updateTableValues()
  }

  removeHeaderRow(headerIndex) {
    this.tableHeaderRows.splice(headerIndex, 1)
    this.columns[0].visible = !this.columns[0].visible
    setTimeout(() => {
      this.columns[0].visible = !this.columns[0].visible
    }, 0)
    this.updateTableValues()
  }

  updateTableHeaderValues() {
    for (let columnIndex = 0; columnIndex < this.columns.length; columnIndex++) {
      let column = this.columns[columnIndex]
      this.tableHeaderRows.forEach((header, headerIndex) => {
        console.log(header, column.property);
        if (header[column.property].columnProperty !== column.property) {
          let headerCopy = { ...header[column.property] }

          Reflect.deleteProperty(header, column.property)
          header[column.property] = { ...headerCopy, columnProperty: column.property }
        }
      })

    }

    this.columns[0].visible = !this.columns[0].visible
    setTimeout(() => {
      this.columns[0].visible = !this.columns[0].visible
    }, 0)
    this.updateTableValues()
  }

  addHeaderName({ headerRowIndex, columnProperty }) {

    const addHeaderNameDialog = this.dialog.open(AddHeaderNameComponent, {
      width: '400px',
      data: { name: '' }
    })

    addHeaderNameDialog.afterClosed().subscribe(data => {
      data ? this.tableHeaderRows[headerRowIndex][columnProperty].name = data.name : null
      this.updateTableValues()  
    })
  }

  editHeader({ headerRowIndex, columnProperty }) {
    const addHeaderNameDialog = this.dialog.open(AddHeaderNameComponent, {
      width: '400px',
      data: { name: this.tableHeaderRows[headerRowIndex][columnProperty].name }
    })

    addHeaderNameDialog.afterClosed().subscribe(data => {
      data ? this.tableHeaderRows[headerRowIndex][columnProperty].name = data.name : null
      this.updateTableValues()
    })
  }


  resetTable() {
    this.subject$.next([])
    this.columns = []
    this.tableDataOnSubmit = {
      columns: [],
      rows: []
    }
  }

  addColumnNumber(newTable) {
    this.showTable = true
    let enteredNumberColumns = 1
    const addColumnDialog = this.dialog.open(AddColumnDialogComponent, {
      width: '500px',
      data: { enteredNumberColumns: enteredNumberColumns }
    });

    addColumnDialog.afterClosed().subscribe(result => {
      let indexNumber = this.columns.length
      let forLength = (this.columns.length - 1) + result

      if (newTable) {
        this.columns = []
        this.subject$.next([])
        indexNumber = 1
        forLength = result
        this.columns.push({ label: 'Actions', property: 'actionsColumn', type: 'actionsButtons', visible: true, id: getUniqueId(2) })
        this.columns.unshift({ label: 'Checkbox', property: 'firstColumn', type: 'checkbox', visible: true, id: getUniqueId(2) })
      }

      for (let i = indexNumber; i <= forLength; i++) {
        let uniqueProperty = getUniqueId(2)
        let singleColumn = { label: '', property: uniqueProperty, type: null, visible: true, id: uniqueProperty, filterRules: [], addedData: false }
        this.columns.push(singleColumn)
        this.tableHeaderRows.forEach(headerRow => {
          let uniqueId = getUniqueId(2)
          headerRow[uniqueProperty] = {
            visible: true,
            columnProperty: uniqueProperty,
            name: '',
            colSpan: 1,
            rowSpan: 1,
            id: uniqueId
          }
          headerRow.uniqueIds.push(uniqueId)
        })
      }
      this.updateTableValues()
    })
  }

  addColumnToSide({ newTable, currColumnIndex, rightSide }) {
    this.showTable = true
    let enteredNumberColumns = 1
    const addColumnDialog = this.dialog.open(AddColumnDialogComponent, {
      width: '500px',
      data: { enteredNumberColumns: enteredNumberColumns }
    });

    addColumnDialog.afterClosed().subscribe(result => {
      let indexNumber = this.columns.length
      let forLength = (this.columns.length - 1) + result

      for (let i = indexNumber; i <= forLength; i++) {
        let uniqueProperty = getUniqueId(2)
        let singleColumn = { label: '', property: uniqueProperty, type: null, visible: true, id: uniqueProperty, filterRules: [], addedData: false }

        if (rightSide) {
          this.columns.splice(currColumnIndex + 1, 0, singleColumn)
          this.tableHeaderRows.forEach(headerRow => {
            let uniqueId = getUniqueId(2)
            headerRow[uniqueProperty] = {
              visible: true,
              columnProperty: uniqueProperty,
              name: '',
              colSpan: 1,
              rowSpan: 1,
              id: uniqueId
            }
            headerRow.uniqueIds.splice(currColumnIndex + 1, 0, uniqueId)
          })
        } else {
          this.columns.splice(currColumnIndex, 0, singleColumn)
          this.tableHeaderRows.forEach(headerRow => {
            let uniqueId = getUniqueId(2)
            headerRow[uniqueProperty] = {
              visible: true,
              columnProperty: uniqueProperty,
              name: '',
              colSpan: 1,
              rowSpan: 1,
              id: uniqueId
            }
            headerRow.uniqueIds.splice(currColumnIndex, 0, uniqueId)
          })
        }
      }
      this.updateTableValues()
    })

  }

  // adding column data
  createColumnData(index, existingColumn = null) {
    let fromColumn
    let columnName = ''
    if (existingColumn) {
      fromColumn = this.columns.filter(column => column.property === existingColumn)[0]
    }
    let type = fromColumn ? fromColumn.type : ''
    let rating: number
    const dialogRef = this.dialog.open(NewColumnDialogComponent, {
      width: '500px',
      data: {
        fromColumn,
        columnIndex: index,
        columns: this.columns,
        dropdownData: fromColumn ? fromColumn.dropdownData : [],
        columnName: fromColumn ? fromColumn.label : columnName,
        type: type
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      let duplicateColumnName = false

      if (result) {

        let propertyString = this.toCamelCase(result.columnName)

        // Update table headers with new column property
        this.tableHeaderRows.forEach((header, index, array) => {
          let wantedColumn = this.columns[result.columnIndex]
          if (header[wantedColumn.property].columnProperty === wantedColumn.property) {
            let headerCopy = { ...header[wantedColumn.property] }
            Reflect.deleteProperty(header, wantedColumn.property)
            header[propertyString] = { ...headerCopy, columnProperty: propertyString }
          }
        })

        if (existingColumn) {
          this.subject$.value.forEach((row) => {
            for (let key in row) {
              if (key == existingColumn)
                Reflect.deleteProperty(row, key)
            }
          })
        }

        this.columns.forEach((column) => {
          column.property === propertyString ? duplicateColumnName = true : null
          column.addedData = true
        })

        if (!duplicateColumnName || this.columns[index].property == propertyString) {
          this.columns[index] = { ...this.columns[index], label: result.columnName, property: propertyString, type: result.type, expectedFilterType: 'text' }
          result.dropdownData ? this.columns[index].dropdownData = result.dropdownData : null
          result.type == 'date' || result.type == 'time' ? this.columns[index].format = result.format : null

          if (result.type === 'date') {
            this.columns[index].expectedFilterType = 'date'
            this.columns[index].rangeDate = new FormGroup({
              start: new FormControl(),
              end: new FormControl(),
            });
          }

          if (result.type === 'time')
            this.columns[index].expectedFilterType = 'time'

          if (result.type === 'file')
            this.columns[index].fileSize = result.fileSize

          else if (result.type === 'mathOperations') {
            this.columns[index]["mathOperationsData"] = result.mathOperationsData
            this.columns[index]["showTotal"] = false
          }

          if (result.type === 'number') {
            this.columns[index].expectedFilterType = 'number'
            this.columns[index]["showTotal"] = false
          }

          this.updateRow(propertyString, this.columns[index], result.format, result.rating)

          // turn on edit for all rows
          this.subject$.value.forEach(row => row.edit = true)
        } else {
          this.snackBarService.showMessage("Can't have duplicate column names", 3000)
        }
      }
    })
  }

  toCamelCase(str) {
    return str.replace(/(?:^\w|[A-Z]|\b\w)/g, function (word: string, index: number) {
      return index == 0 ? word.toLowerCase() : word.toUpperCase();
    }).replace(/\s+/g, '');
  }

  // delete wanted column and key from rows
  deleteColumn({ index, column }) {
    let removedColumn = this.columns.splice(index, 1)[0]
    this.columns.splice(index, 1)
    this.tableHeaderRows.forEach((header, index) => {
      if (header[removedColumn.property]) {
        console.log(header[removedColumn.property]);
        header.uniqueIds.splice(header.uniqueIds.indexOf(header[removedColumn.property].id), 1)
        Reflect.deleteProperty(header, removedColumn.property)
      }
    })

    // update row values
    this.updateRowsOnDelete(column.property)
    this.updateTableValues(column)
  }

  openFile(url) {
    document.open('https://images.unsplash.com/photo-1640622841908-3a691b7b7ac9?ixlib=rb-1.2.1&ixid=MnwxMjA3fDF8MHxlZGl0b3JpYWwtZmVlZHwxfHx8ZW58MHx8fHw%3D&auto=format&fit=crop&w=600&q=60', '', 'noopener=true')
  }

  editColumn(index) {
    this.createColumnData(index, this.columns[index].property)
  }

  confirmRow(row) {
    let emailRegex = new RegExp(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/);

    for (let columnProperty in row) {
      if (columnProperty !== 'edit' && row[columnProperty].type !== 'file' && row[columnProperty].visible) {
        if (!row[columnProperty].value) {
          this.snackBarService.showMessage('Please validly fill all the values', 3000)
          return row.edit = true
        } else if (row[columnProperty].type === 'email' && !emailRegex.test(row[columnProperty].value)) {
          this.snackBarService.showMessage('Please enter a valid email', 3000)
          return row.edit = true
        }
      } else {
        row.edit = false
      }
    }
    this.filterRowsByAllFilters()
    this.updateTableValues()
  }

  addRow(format = false, ratings = 5) {
    let row = {
      edit: true,
      hidden: false,
      id: getUniqueId(2),
      summery: '',
    }

    let currentRowsState = [...this.subject$.value]

    for (let i = 2; i < this.columns.length; i++) {
      let currentColumn = this.columns[i]
      let key = currentColumn.property
      let type = currentColumn.type

      if (currentColumn.addedData) {
        row[key] = {
          value: '',
          data: [],
          visible: true,
          type,
          rotateVertical: false
        }
        this.setInitValueAndData(row, key, format, type, ratings)
      }
    }

    currentRowsState.push(row)
    this.subject$.next(currentRowsState)
    this.updateTableValues()

    const { length, pageSize } = this.dataSource.paginator

    if (length > pageSize - 1) {
      this.changeDetectionRef.detectChanges()
      this.dataSource.paginator.lastPage()
    }
  }

  // Set value, rating and format for column type of date, time, checkbox, file, and rating
  setInitValueAndData(row, key, format, type, ratings = 5) {
    row[key].rowSpan = 1
    row[key].colSpan = 1

    if (format && (type == 'date' || type == 'time')) {
      row[key].data = format
    }
    if (type === 'checkbox' || type === 'file' || type === 'hyperlink') {
      row[key].value = []
    }
    if (type === 'rating') {
      let middleValue = ratings / 2
      row[key].value = middleValue
    }

  }

  // Update existing row
  updateRow(newProperty: string, column, format = false, ratings) {
    this.subject$.value.forEach(row => {
      for (let key in row) {
        if (key == column.id)
          Reflect.deleteProperty(row, key)
      }

      row[newProperty] = {
        value: '',
        data: [],
        visible: column.visible,
        type: column.type,
        rowSpan: 1,
        colSpan: 1
      }

      this.setInitValueAndData(row, newProperty, format, column.type, ratings)
    })
    this.updateTableValues()
  }

  // update rows if column is edited or deleted
  updateRowsOnDelete(columnProperty: string) {
    let newRowElements = []
    this.subject$.value.forEach((row) => {
      let newRowElement = Object.keys(row).reduce((object, key) => {
        if (key !== columnProperty) {
          object[key] = row[key]
        }
        return object
      }, {})
      newRowElements.push(newRowElement)
    })
    this.subject$.next(newRowElements)
  }


  itemAction({ row, action }) {
    let indexOfItemRow = this.subject$.value.indexOf(row)

    if (action === 'deleteSelected') {
      let tableValueCopy = [...this.subject$.value]
      tableValueCopy.splice(indexOfItemRow, 1)
      this.subject$.next(tableValueCopy)
    } else if (action === 'editSelected') {
      this.showEditSelected = true
      this.subject$.value[indexOfItemRow].edit = true
    } else {
      this.showEditSelected = false
      this.subject$.value[indexOfItemRow].edit = false
    }
    this.updateTableValues()
  }

  itemsAction({ rows, action }) {
    rows.forEach(row => this.itemAction({ row, action }))
    if (action === 'deleteSelected') this.selection.clear()
  }

  toggleColumnVisibility({ column, event }) {
    event.stopPropagation();
    column.visible = !column.visible;
    this.updateTableValues(column)
  }

  updateTableValues(column = null) {
    this.columnsFiltered = []
    this.rowsFilteredByColumnsVisibility = []
    this.columnsFiltered = this.columns.filter(Column => Column.visible)

    if (column != null) {
      this.subject$.value.forEach(row => {
        let obj = {
          hidden: row.hidden,
          edit: row.edit,
          id: row.id
        }

        for (let key in row) {
          row[key].value && row[key].value.length < 1 ? row.edit = true : null
          if (key === column.property) {
            row[key].visible = column.visible
          }
          if (row[key].visible) {
            obj[key] = { ...row[key] }
          }
        }
        this.rowsFilteredByColumnsVisibility.push(obj)
      })
    }

    // set table values to be valid when emitting output
    this.columnsFiltered.length > 0 ? this.tableDataOnSubmit.columns = this.columnsFiltered : this.tableDataOnSubmit.columns = this.columns
    this.rowsFilteredByColumnsVisibility.length > 0 ? this.tableDataOnSubmit.rows = [...this.rowsFilteredByColumnsVisibility] : this.tableDataOnSubmit.rows = [...this.subject$.value]
    this.tableDataOnSubmit.rows = [...this.tableDataOnSubmit.rows].filter(row => !row.hidden)
    this.tableDataOnSubmit["templateRows"] = this.subject$.value
    this.tableDataOnSubmit["tableHeaderRows"] = this.tableHeaderRows
    // console.log(this.tableDataOnSubmit);
    // let json = JSON.stringify(this.tableDataOnSubmit)


    let json = { ...this.tableDataOnSubmit }

    json.columns.forEach(column => column.expectedFilterType === 'date' ? column.rangeDate = column.rangeDate.value : null)
    let Json = JSON.stringify(json)

    this.columns.forEach((column, index) => {
      if (column.expectedFilterType === 'date') {
        column['rangeDate'] = new FormGroup({
          start: new FormControl(''),
          end: new FormControl('')
        })
      }
    })

    this.submitTableDataEvent.emit(this.tableDataOnSubmit)
    this.item.tableValue = this.tableDataOnSubmit;
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const page = this.dataSource.connect().value.length
    return numSelected === page;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.connect().value.forEach(row => this.selection.select(row));
  }

  // filter over the original table with filter array
  checkWhatRowsToFilter(column, removeFilter, filterValue = null, type = null) {
    if (!removeFilter) {
      this.allFilterValues.push({ columnProperty: column.property, columnLabel: column.label, filterValue: filterValue, filterType: type })
      column.filterRules.push({ filterValue: filterValue, filterType: type, columnProperty: column.property })
    }
    this.filterRowsByAllFilters()
    this.updateTableValues(column)
  }

  // filter rows by all filter values from columns
  filterRowsByAllFilters() {
    if (this.allFilterValues.length > 0) {
      this.allFilterValues.forEach(currentFilterEl => {
        let Property = currentFilterEl.columnProperty
        let currentFilterType = currentFilterEl.filterType

        currentFilterType.filterValue === 'number' ? currentFilterEl.filterValue = +currentFilterEl.filterValue : null

        this.subject$.value.forEach(row => {
          let rowProperty = row[Property]

          row.hidden = false
          if (currentFilterType.label === 'Start with' && !rowProperty.value.startsWith(currentFilterEl.filterValue)) {
            row.hidden = true
          } else if (currentFilterType.label === 'Contains' && !rowProperty.value.includes(currentFilterEl.filterValue)) {
            row.hidden = true
          } else if (currentFilterType.label === 'Not contains' && !(rowProperty.value.includes(currentFilterEl.filterValue) == false)) {
            row.hidden = true
          } else if (currentFilterType.label === 'Ends with' && !rowProperty.value.endsWith(currentFilterEl.filterValue)) {
            row.hidden = true
          } else if (currentFilterType.label == 'Equals' && !(rowProperty.value == currentFilterEl.filterValue)) {
            row.hidden = true
          } else if (currentFilterType.label === 'Greater then' && !(currentFilterEl.filterValue < rowProperty.value)) {
            row.hidden = true
          } else if (currentFilterType.label === 'Less then' && !(currentFilterEl.filterValue > rowProperty.value)) {
            row.hidden = true
          } else if (currentFilterType.label === 'Greater or equal to' && !(currentFilterEl.filterValue <= rowProperty.value)) {
            row.hidden = true
          } else if (currentFilterType.label === 'Less or equal to' && !(currentFilterEl.filterValue >= rowProperty.value)) {
            row.hidden = true
          } else if (currentFilterType.label === 'preset ' + currentFilterEl.filterValue) { // check if its preset 1 or preset 3 or preset 7 depending on the filterValue            
            let pastDate = new Date().getDate() - currentFilterEl.filterValue
            let lastDateSetter = new Date().setDate(pastDate)
            let lastDate = new Date(lastDateSetter)
            if (!(new Date(rowProperty.value).getTime() >= new Date(lastDate.getFullYear(), lastDate.getMonth(), lastDate.getDate(), 0, 0, 0).getTime() && (new Date(rowProperty.value).getTime() <= new Date().getTime()))) {
              row.hidden = true
            }
          } else if (currentFilterType.label === 'rangeDate') {
            (new Date(rowProperty.value).getTime() >= new Date(currentFilterEl.filterValue.start).getTime() && (new Date(rowProperty.value).getTime() <= new Date(currentFilterEl.filterValue.end).getTime())) ? null : row.hidden = true
          } else {
            row.hidden = false
          }
        })
      })
    } else {
      this.subject$.value.forEach(row => row.hidden = false)
    }
  }

  // push checked checkboxes to row value
  checkboxChecked({ row, checkbox, event }) {
    if (event.checked) {
      row.value.push(checkbox)
    } else {

      let index = row.value.indexOf(checkbox)
      row.value.splice(index, 1)
      console.log(row.value);
    }
    this.updateTableValues()
  }

  // clear specific filter rule for a column
  clearFilterRules({ property, filterType, column, filterIndex, wholeColumn }) {
    this.allFilterValues.forEach((el, i) => {
      if (wholeColumn && el.columnProperty === property) {
        this.allFilterValues.splice(i, 1)
        column.filterRules = []
      }
      if (el.columnProperty === property && el.filterType === filterType) {
        this.allFilterValues.splice(i, 1)
        column.filterRules.splice(filterIndex, 1)
      }
    })
    this.checkWhatRowsToFilter(column, true)
    this.updateTableValues()
  }

  clearFilterRulesHandler(property, filterType, column, filterIndex, wholeColumn) {
    this.clearFilterRules({ property, filterType, column, filterIndex, wholeColumn })
  }

  // Set label values for checkboxes and radio buttons
  setLabelValues({ row, property }) {
    let labels
    const setLabelsDialog = this.dialog.open(SetLabelsComponent, {
      width: '500px',
      data: {
        labels: labels
      }
    })

    setLabelsDialog.afterClosed().subscribe(result => {
      result.labels ? row[property].data = result.labels : this.snackBarService.showMessage('Please enter label values', 2000)
    })
    this.updateTableValues()
  }

  //Align cell Vertically
  alignCellVertically({ row, columnProperty }) {
    row.cellVertical = true
    row[columnProperty].rotateVertical = true
  }

  //Align cell Horizontally
  alignCellHorizontally({ row, columnProperty }) {
    row[columnProperty].rotateVertical = false
    this.checkIfRowCanShrink(row)
  }

  checkIfRowCanShrink(row) {
    for (let key in row) {
      if (row[key].rotateVertical) {
        row.cellVertical = true
        return
      } else {
        row.cellVertical = false
      }
    }
  }

  // clear all filters
  clearAllFilters() {
    this.allFilterValues = []
    this.columns.forEach(column => column.filterRules = [])
    this.filterRowsByAllFilters()
    this.updateTableValues()
  }

  downloadTableExcelFormat(ShowLabel = true,) {
    this.updateTableValues()
    this.masterTableToExcel.downloadMasterTableToExcel(this.tableDataOnSubmit.rows, ShowLabel)
  }

  setSummeryVisibility(summeryAlwaysVisible: boolean) {
    this.summeryAlwaysVisible = summeryAlwaysVisible
  }

  changeTableTextStyle(event) {
    this.tableTextStyle = event
  }

  initStatus = 'draft'

  updateData() {
    this.updateTableValues()

    let columns = this.initTableValues.columns;

    let allClear = columns.every(column => column.label)
    let labelsFilled = []
    let primaryKeyValid = true

    console.log('Hello', allClear, columns);

    if (columns.length <= 2) {
      this.snackBarService.showMessage('Please Add Columns to Create a Table Form', 3000)
      return
    }

    if (allClear) {
      this.tableDataOnSubmit["templateRows"].forEach((row, index, array) => {
        if (index < 1) {
          row.actionsMergeStart = true
          // row.actionsRowSpan = array.length
          row.actionsRowSpan = 1
        } else {
          row.actionsMergeStart = true
          // row.actionsMergeStart = null
          row.actionsRowSpan = 1
        }
      })

      columns.forEach(column => {
        if (column.type === 'primaryKey') {
          primaryKeyValid = this.subject$.value.every((row => row[column.property].value))
        }
        if ((column.type === 'radioButton' || column.type === 'checkbox') && column.property !== 'firstColumn') {
          if (!this.subject$.value.length) {
            this.snackBarService.showMessage('Please enter a row to fill the required labels', 3000)
          } else {
            labelsFilled = []
            this.subject$.value.forEach((row, index) => {
              if (row[column.property].rowSpan && row[column.property].colSpan) {
                row[column.property].data.length ? labelsFilled.push(true) : labelsFilled.push(false)
              }
            })

          }
        }
      })

      if (!primaryKeyValid) return this.snackBarService.showMessage('Please Select Primary Key to Continue', 3000)

      if (!labelsFilled.includes(false)) {
        this.tableDataOnSubmit["templateRows"].forEach((row, index, array) => {
          if (index < 1) {
            row.actionsMergeStart = true
            // row.actionsRowSpan = array.length
            row.actionsRowSpan = 1
          } else {
            row.actionsMergeStart = true
            row.actionsRowSpan = 1
          }
        })

        this.updateDisabled = true
        //check this function
        // this.publishData()

      } else {
        this.snackBarService.showMessage('Please Insert labels for Required Columns', 3000)
      }
    } else {
      this.snackBarService.showMessage('Please Fill all Columns to Continue', 3000)
    }

  }

  publishData() {
    this.loadingTableData = true
    this.tableDataOnSubmit.columns.forEach(column => column.expectedFilterType === 'date' ? column.rangeDate = column.rangeDate.value : null)

    let json = JSON.stringify(this.tableDataOnSubmit)

    const data: any = this.formBuilderFillData.getFormData();

  }

  templateUpdateApproveRejectContent(formId: number): Observable<any> {
    return this.httpService.templateUpdateApproveRejectContent(formId, JSON.stringify(this.formBuilderElementsService.predefinedPageBuilder));
  }

  mergeDataToggle(event) {
    this.dataForMerge = {
      columns: this.columns,
      event: event,
      rows: event === 'data' ? this.subject$.value : this.tableHeaderRows,
      result: {
        column: undefined,
        rowIndex: undefined,
        cellNumberDown: 1,
        cellNumberRight: 1,
        cellNumberLeft: 1,
        cellNumberUp: 1
      }
    }
    this.showMergeDataComponent = !this.showMergeDataComponent
  }

  closeMergeDataComponent() {
    this.showMergeDataComponent = false
  }

  mergeDataValues(result) {
    const { column, rowIndex, cellNumberDown, cellNumberRight, cellNumberLeft, cellNumberUp } = result.result
    let tableRowsOrHeaderRowsArray
    result.event === 'headers' ? tableRowsOrHeaderRowsArray = this.tableHeaderRows : tableRowsOrHeaderRowsArray = this.subject$.value
    console.log(tableRowsOrHeaderRowsArray);
    console.log(this.tableHeaderRows);

    if (cellNumberUp) {
      tableRowsOrHeaderRowsArray[rowIndex][column]["cellsUp"] = cellNumberUp
    }

    tableRowsOrHeaderRowsArray.forEach((row, index) => {
      let columnsArr = []
      let wantedColumnIndex
      let startIndexDown
      if (cellNumberLeft || cellNumberRight) {
        this.columns.forEach(column => columnsArr.push(column.property))
        wantedColumnIndex = columnsArr.indexOf(column)
      }

      if (cellNumberDown > 1) {
        tableRowsOrHeaderRowsArray[rowIndex][column].rowSpan = cellNumberDown
        tableRowsOrHeaderRowsArray[rowIndex][column]["mergeDirectionsDown"] = true
        if (index > rowIndex && index < rowIndex + cellNumberDown) {
          if (row && row[column]) {
            row[column].rowSpan = 0
            row[column].visible = false
          }
        }

        if (cellNumberRight > 1) {
          tableRowsOrHeaderRowsArray[rowIndex][column].colSpan = cellNumberRight
          tableRowsOrHeaderRowsArray[rowIndex][column]["mergeDirectionsRight"] = true

          columnsArr.forEach((columnProperty, index) => {
            if (index > wantedColumnIndex && index < wantedColumnIndex + cellNumberRight) {
              tableRowsOrHeaderRowsArray[rowIndex][columnProperty].colSpan = 0
              tableRowsOrHeaderRowsArray[rowIndex][columnProperty].visible = false
              if (tableRowsOrHeaderRowsArray[rowIndex][column].rowSpan > 1) {
                for (let i = 1; i < tableRowsOrHeaderRowsArray[rowIndex][column].rowSpan; i++) {

                  if (tableRowsOrHeaderRowsArray[rowIndex] && tableRowsOrHeaderRowsArray[rowIndex + i][columnProperty]) {
                    tableRowsOrHeaderRowsArray[rowIndex + i][columnProperty].colSpan = 0
                  }

                  if (tableRowsOrHeaderRowsArray[rowIndex + i] && tableRowsOrHeaderRowsArray[rowIndex + i][columnProperty].colSpan < 1) {
                    // console.log(tableRowsOrHeaderRowsArray[rowIndex + i][columnProperty]);
                    tableRowsOrHeaderRowsArray[rowIndex + i][columnProperty].visible = false
                  }
                }
              }
            }
          })
        }

        if (cellNumberLeft > 1) {
          tableRowsOrHeaderRowsArray[rowIndex][column].colSpan = cellNumberLeft
          tableRowsOrHeaderRowsArray[rowIndex][column]["mergeDirectionsLeft"] = true

          let indexToStartFrom = wantedColumnIndex - cellNumberLeft
          columnsArr.forEach((columnProperty, index) => {
            if (index > indexToStartFrom && index < wantedColumnIndex) {

              tableRowsOrHeaderRowsArray[rowIndex][columnProperty].colSpan = 0
              tableRowsOrHeaderRowsArray[rowIndex][columnProperty].visible = false


              if (tableRowsOrHeaderRowsArray[rowIndex][column].rowSpan > 1) {
                for (let i = 1; i < tableRowsOrHeaderRowsArray[rowIndex][column].rowSpan; i++) {
                  if (tableRowsOrHeaderRowsArray[rowIndex + i]) {
                    tableRowsOrHeaderRowsArray[rowIndex + i][columnProperty].visible = false
                  }
                }
              }
            }
          })
        }
      }



      if (cellNumberUp > 1) {
        tableRowsOrHeaderRowsArray[rowIndex - (cellNumberUp - 1)][column] = { ...tableRowsOrHeaderRowsArray[rowIndex][column], rowSpan: cellNumberUp, cellsUp: null }
        tableRowsOrHeaderRowsArray[rowIndex][column].visible = false
        tableRowsOrHeaderRowsArray[rowIndex][column]["mergeDirectionsUp"] = true
        console.log(rowIndex);


        startIndexDown = rowIndex - cellNumberUp + 1
        startIndexDown < 0 ? startIndexDown = 0 : null
        for (let indexDown = startIndexDown; indexDown <= rowIndex; indexDown++) {
          if (startIndexDown < rowIndex && startIndexDown !== rowIndex) {
            tableRowsOrHeaderRowsArray[indexDown][column].visible = false
          }

        }
        tableRowsOrHeaderRowsArray[rowIndex - (cellNumberUp - 1)][column].visible = true

        if (cellNumberLeft > 1) {
          tableRowsOrHeaderRowsArray[rowIndex - (cellNumberUp - 1)][column].colSpan = cellNumberLeft
          let indexToStartFrom = wantedColumnIndex - cellNumberLeft
          columnsArr.forEach((columnProperty, index) => {
            if (index > indexToStartFrom && index < wantedColumnIndex) {

              tableRowsOrHeaderRowsArray[rowIndex][columnProperty].colSpan = 0
              tableRowsOrHeaderRowsArray[rowIndex][columnProperty].visible = false


              if (tableRowsOrHeaderRowsArray[rowIndex - (cellNumberUp - 1)][column].rowSpan > 1) {
                for (let i = 1; i < tableRowsOrHeaderRowsArray[rowIndex - (cellNumberUp - 1)][column].rowSpan; i++) {
                  if (tableRowsOrHeaderRowsArray[rowIndex - i]) {
                    tableRowsOrHeaderRowsArray[rowIndex - i][columnProperty].visible = false
                    tableRowsOrHeaderRowsArray[rowIndex - i][columnProperty].rowSpan = 0
                  }
                }
              }
            }
          })
        }

        //new
        if (cellNumberRight > 1) {
          tableRowsOrHeaderRowsArray[rowIndex][column].colSpan = cellNumberRight
          tableRowsOrHeaderRowsArray[rowIndex][column]["mergeDirectionsRight"] = true

          columnsArr.forEach((columnProperty, index) => {
            if (index > wantedColumnIndex && index < wantedColumnIndex + cellNumberRight) {
              tableRowsOrHeaderRowsArray[rowIndex][columnProperty].colSpan = 0
              tableRowsOrHeaderRowsArray[rowIndex][columnProperty].visible = false

              if (tableRowsOrHeaderRowsArray[rowIndex - (cellNumberUp - 1)][column].rowSpan > 1) {
                for (let i = 1; i < tableRowsOrHeaderRowsArray[rowIndex - (cellNumberUp - 1)][column].rowSpan; i++) {

                  if (tableRowsOrHeaderRowsArray[rowIndex - i] && tableRowsOrHeaderRowsArray[rowIndex - i][columnProperty]) {
                    tableRowsOrHeaderRowsArray[rowIndex - i][columnProperty].colSpan = 0
                  }

                  if (tableRowsOrHeaderRowsArray[rowIndex - i] && tableRowsOrHeaderRowsArray[rowIndex - i][columnProperty].colSpan < 1) {
                    console.log(tableRowsOrHeaderRowsArray[rowIndex - i][columnProperty]);
                    tableRowsOrHeaderRowsArray[rowIndex - i][columnProperty].visible = false
                  }
                }
              }
            }
          })
        }
      }

      if (cellNumberLeft > 1) {
        tableRowsOrHeaderRowsArray[rowIndex][column].colSpan = cellNumberLeft
        tableRowsOrHeaderRowsArray[rowIndex][column]["mergeDirectionsLeft"] = true

        let indexToStartFrom = wantedColumnIndex - cellNumberLeft
        columnsArr.forEach((columnProperty, index) => {
          if (index > indexToStartFrom && index < wantedColumnIndex) {

            tableRowsOrHeaderRowsArray[rowIndex][columnProperty].colSpan = 0
            tableRowsOrHeaderRowsArray[rowIndex][columnProperty].visible = false


            if (tableRowsOrHeaderRowsArray[rowIndex][column].rowSpan > 1) {
              for (let i = 1; i < tableRowsOrHeaderRowsArray[rowIndex][column].rowSpan / 2; i++) {
                if (tableRowsOrHeaderRowsArray[rowIndex + i]) {
                  tableRowsOrHeaderRowsArray[rowIndex + i][columnProperty].visible = false
                }
              }
            }
          }
        })
      }


      if (cellNumberRight > 1) {
        tableRowsOrHeaderRowsArray[rowIndex][column].colSpan = cellNumberRight
        tableRowsOrHeaderRowsArray[rowIndex][column]["mergeDirectionsRight"] = true
        columnsArr.splice(0, wantedColumnIndex + 1)
        columnsArr.forEach((columnProperty, index) => {
          if (index < cellNumberRight - 1) {
            tableRowsOrHeaderRowsArray[rowIndex][columnProperty].colSpan = 0
            tableRowsOrHeaderRowsArray[rowIndex][columnProperty].visible = false
          }
        })
      }
    })
    this.closeMergeDataComponent()
    this.updateTableValues()
  }

  unmergeDataValues(event) {
    let wantedRows
    event.event === 'headers' ? wantedRows = this.tableHeaderRows : wantedRows = this.subject$.value

    let wantedColumnProperty = event.columnProperty
    let wantedRowKey = wantedRows[event.row][wantedColumnProperty]
    let wantedRow = wantedRows[event.row]
    let wantedColumnIndex
    this.columns.forEach((column, index) => {
      if (column.property === wantedColumnProperty) {
        wantedColumnIndex = index
      }
    })

    this.columns.forEach((column, currentColumnIndex) => {

      if (column.label === event.column) {
        if (wantedRowKey.mergeDirectionsRight) {
          for (let i = 1; i < wantedRowKey.colSpan; i++) {
            let nextColumnProperty = this.columns[currentColumnIndex + i].property
            wantedRow[nextColumnProperty].colSpan = 1
            wantedRow[nextColumnProperty].visible = true
          }
        }

        if (wantedRowKey.mergeDirectionsLeft) {
          for (let i = wantedColumnIndex - wantedRowKey.colSpan + 1; i <= currentColumnIndex; i++) {
            if (this.columns[i]) {
              let nextColumnProperty = this.columns[i].property
              wantedRow[nextColumnProperty].colSpan = 1
              wantedRow[nextColumnProperty].visible = true
            }
          }
        }
      }
    })

    if (wantedRowKey.mergeDirectionsDown || wantedRowKey.mergeDirectionsUp) {
      wantedRows.forEach((currentRow, currentIndex) => {
        if (wantedRowKey.mergeDirectionsDown) {
          if (currentIndex > event.row && currentIndex <= event.row + wantedRowKey.rowSpan) {
            currentRow[wantedColumnProperty].rowSpan = 1
            currentRow[wantedColumnProperty].visible = true

            if (wantedRowKey.mergeDirectionsRight || wantedRowKey.mergeDirectionsLeft) {
              this.columns.forEach((column, currentColumnIndex) => {
                if (column.label === event.column) {
                  if (wantedRowKey.mergeDirectionsRight) {
                    for (let i = 0; i <= wantedRowKey.colSpan; i++) {
                      if (this.columns[currentColumnIndex + i]) {
                        let nextColumnProperty = this.columns[currentColumnIndex + i].property
                        currentRow[nextColumnProperty].colSpan = 1
                        currentRow[nextColumnProperty].visible = true
                      }
                    }
                  }

                  if (wantedRowKey.mergeDirectionsLeft) {
                    for (let i = currentColumnIndex; i >= currentColumnIndex - wantedRowKey.colSpan - 1; i--) {
                      if (this.columns[i]) {
                        if (event.event === 'data' && this.columns[i].property !== 'actionsColumn' && this.columns[i].property !== 'firstColumn') {
                          let nextColumnProperty = this.columns[i].property
                          currentRow[nextColumnProperty].colSpan = 1
                          currentRow[nextColumnProperty].visible = true
                        } else {
                          let nextColumnProperty = this.columns[i].property
                          currentRow[nextColumnProperty].colSpan = 1
                          currentRow[nextColumnProperty].visible = true
                        }
                      }
                    }
                  }
                }
              })
            }
          }
        }

        if (wantedRowKey.mergeDirectionsUp) {
          if (currentIndex <= event.row && currentIndex >= event.row - wantedRowKey.cellsUp + 1) {
            currentRow[wantedColumnProperty].rowSpan = 1
            currentRow[wantedColumnProperty].visible = true

            if (wantedRowKey.mergeDirectionsRight || wantedRowKey.mergeDirectionsLeft) {
              this.columns.forEach((column, currentColumnIndex) => {
                if (column.label === event.column) {
                  if (wantedRowKey.mergeDirectionsRight) {
                    for (let i = 0; i <= wantedRowKey.colSpan; i++) {
                      if (this.columns[currentColumnIndex + i]) {
                        let nextColumnProperty = this.columns[currentColumnIndex + i].property
                        currentRow[nextColumnProperty].colSpan = 1
                        currentRow[nextColumnProperty].visible = true
                      }
                    }
                  }

                  if (wantedRowKey.mergeDirectionsLeft) {
                    for (let i = wantedColumnIndex - wantedRowKey.colSpan - 1; i <= wantedColumnIndex; i++) {
                      if (this.columns[i]) {
                        if (event.event === 'data' && this.columns[i].property !== 'actionsColumn' && this.columns[i].property !== 'firstColumn') {
                          let nextColumnProperty = this.columns[i].property
                          currentRow[nextColumnProperty].colSpan = 1
                          currentRow[nextColumnProperty].rowSpan = 1
                          currentRow[nextColumnProperty].visible = true
                        } else {
                          let nextColumnProperty = this.columns[i].property
                          currentRow[nextColumnProperty].colSpan = 1
                          currentRow[nextColumnProperty].rowSpan = 1
                          currentRow[nextColumnProperty].visible = true
                        }
                      }
                    }

                  }
                }
              })
            }
          }

        }
      })
    }
    wantedRowKey.mergeDirectionsLeft = false
    wantedRowKey.mergeDirectionsRight = false
    wantedRowKey.mergeDirectionsUp = false
    wantedRowKey.mergeDirectionsDown = false
    wantedRowKey.rowSpan = 1
    wantedRowKey.colSpan = 1
    wantedRowKey.cellsUp = false
    wantedRowKey.visible = true
    this.closeMergeDataComponent()
    this.updateTableValues()
  }
  
  // global filter start

  wantedSearchedValues = []
  searchValues = []

  add(event: MatChipInputEvent): void {
    console.log(event);

    if (event.value) {
      this.searchValues.push(event.value);
    }

    let wantedValues = []
    this.searchValues.forEach(el => {
      if (el.split(':').length == 1) {
        let textForSearch = el.split(':')[0];
        // this.dataSource.filter = textForSearch;
        this.wantedSearchedValues.push('all')
        wantedValues.push(textForSearch)
      }
      else {
        let value = el.split(':')[1].trim()
        this.columns.forEach(column => {
          if (column.label === el.split(':')[0]) {
            this.wantedSearchedValues.push(column.property)
          }
        })
        wantedValues.push(value)
      }
    })
    this.dataSource.filter = wantedValues.join('+')

    // Clear the input value
    event.chipInput!.clear();
  }

  remove(value): void {
    const index = this.searchValues.indexOf(value);
    let dataSourceFilters = this.dataSource.filter.split('+')
    let valueSplitted = value.split(':')

    if (index >= 0) {
      this.searchValues.splice(index, 1);
      if (valueSplitted.length > 1) {
        dataSourceFilters.splice(dataSourceFilters.indexOf(valueSplitted[1].trim()), 1);
      } else {
        dataSourceFilters.splice(dataSourceFilters.indexOf(valueSplitted[0].trim()), 1);
      }
      this.dataSource.filter = dataSourceFilters.join('+')
    }

    if (this.searchValues.length < 1) {
      this.dataSource.filter = ''
    }
  }

  appendToSearch(label) {
    let configuredLabelForSearch = `${label}: `
    this.searchCtrl.setValue(configuredLabelForSearch)
  }


  printDocument() {
    document.getElementById('table-container').classList.remove("table-container-overflow")
    this.print.nativeElement.click();
    document.getElementById('table-container').classList.add("table-container-overflow")
  }

  getNonDuplicatedArray(array: Array<any>) {
    let retVal = [];
    for (let item of array) {
      if (!retVal.includes(JSON.stringify(item))) {
        retVal.push(JSON.stringify(item));
      }
    }
    retVal = retVal.map((x: any) => { return JSON.parse(x) });
    return retVal;
  }

  setLabels(readonly: boolean = false, disabled: boolean = false) {
    this.formLabels = [
      {
        label: 'Name',
        type: 1,
        level: null,
        dependentIndex: null,
        readonly
      },
      {
        label: 'Division',
        type: 2,
        level: 1,
        dependentIndex: 2,
        multiple: true
      },
      {
        label: 'Sub-Division',
        type: 2,
        level: 2,
        dependentIndex: 3,
        multiple: true
      },
      {
        label: 'Police Station',
        type: 2,
        level: 3,
        dependentIndex: null,
        multiple: true
      },
    ];
  }

  formLabels: Array<any>;


  editSelectable({ column, row }) {
    console.log(row);
    const dialogRef = this.dialog.open(EditSelectableDataComponent, {
      width: '500px',
      data: {
        dropdownData: row[column.property].data,
        type: column.type
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        if (column.type === 'radioButton') {
          row[column.property].data = [...result.dropdownData]
          row[column.property].value = ''
        } else if (column.type === 'checkbox') {
          row[column.property].data = [...result.dropdownData]
          row[column.property].value = []
        } else {
          column["dropdownData"] = [...result.dropdownData]
        }
      }
    });
  }

  toggleTotal(column) {
    column.showTotal = !column.showTotal
  }
}