import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TableFormViewComponent } from './table-form-view.component';

describe('TableFormViewComponent', () => {
  let component: TableFormViewComponent;
  let fixture: ComponentFixture<TableFormViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TableFormViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TableFormViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
