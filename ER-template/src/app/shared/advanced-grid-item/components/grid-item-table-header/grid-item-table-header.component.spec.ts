import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GridItemTableHeaderComponent } from './grid-item-table-header.component';

describe('GridItemTableHeaderComponent', () => {
  let component: GridItemTableHeaderComponent;
  let fixture: ComponentFixture<GridItemTableHeaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GridItemTableHeaderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GridItemTableHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
