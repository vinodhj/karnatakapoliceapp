import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { AfterViewInit, ChangeDetectorRef, Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'exai-grid-item-table-header',
  templateUrl: './grid-item-table-header.component.html',
  styleUrls: ['./grid-item-table-header.component.scss']
})
export class GridItemTableHeaderComponent implements AfterViewInit {

  @Input() selection
  @Input() showEditSelected
  @Input() searchCtrl
  @Input() allFilterValues
  @Input() columns
  @Input() showMergeButton = true
  @Input() disableMergeButton
  @Input() userCantChange
  @Input() searchValues
  @Input() showAddColumn
  @Input() formName
  @Input() columnsForFilter
  @Input() showEditSelectedButton
  @Input() showConfirmSelectedButton = true
  @Input() showAddRowButton
  @Input() showTitle = true

  textAlignmentSwitch: string = 'text-left'

  @Output() itemsActionEvent: EventEmitter<any> = new EventEmitter()
  @Output() clearFilterRulesEvent: EventEmitter<any> = new EventEmitter()
  @Output() clearAllFiltersEvent: EventEmitter<any> = new EventEmitter()
  @Output() downloadTableExcelFormatEvent: EventEmitter<any> = new EventEmitter()
  @Output() toggleColumnVisibilityEvent: EventEmitter<any> = new EventEmitter()
  @Output() createTableEvent: EventEmitter<any> = new EventEmitter()
  @Output() addRowEvent: EventEmitter<any> = new EventEmitter()
  @Output() setVisibilityEvent: EventEmitter<any> = new EventEmitter()
  @Output() changeTableTextStyleEvent: EventEmitter<any> = new EventEmitter()
  @Output() addNewFormEvent: EventEmitter<any> = new EventEmitter()
  @Output() mergeDataEvent: EventEmitter<any> = new EventEmitter()
  @Output() removeEvent: EventEmitter<any> = new EventEmitter()
  @Output() addEvent: EventEmitter<any> = new EventEmitter()
  @Output() appendToSearchEvent: EventEmitter<any> = new EventEmitter()
  @Output() createTableHeaderEvent: EventEmitter<any> = new EventEmitter()
  @Output() mergeHeaderEvent: EventEmitter<any> = new EventEmitter()

  summeryVisibility: boolean = false
  
  readonly separatorKeysCodes = [ENTER, COMMA] as const;

  constructor(private changeDetectionRef: ChangeDetectorRef) {}
  
  ngAfterViewInit() {
    !this.columnsForFilter ? this.columnsForFilter = this.columns : null 
    this.changeDetectionRef.detectChanges()
  }

  createTableHeaderHandler() {
    this.createTableHeaderEvent.emit()
  }

  addNewFormHandler() {
    this.addNewFormEvent.emit()
  }

  removeHandler(value) {
    this.removeEvent.emit(value)
  }

  addHandler(event) {
    this.addEvent.emit(event)
  }

  appendToSearchHandler(columnLabel) {
    this.appendToSearchEvent.emit(columnLabel)
  }

  setSummeryVisibilityHandler() {
    this.setVisibilityEvent.emit(this.summeryVisibility)
  }

  itemsActionHandler(rows, action) {
    this.itemsActionEvent.emit({ rows, action })
  }

  clearFilterRulesHandler(property, filterType, column, filterIndex, wholeColumn) {
    this.clearFilterRulesEvent.emit({ property, filterType, column, filterIndex, wholeColumn })
  }

  clearAllFiltersHandler() {
    this.clearAllFiltersEvent.emit()
  }

  downloadTableExcelFormatHandler() {
    this.downloadTableExcelFormatEvent.emit()
  }

  toggleColumnVisibilityHandler(column, event) {
    this.toggleColumnVisibilityEvent.emit({ column, event })
  }

  createTableHandler(newTable) {
    this.createTableEvent.emit(newTable)
  }

  addRowHandler() {
    this.addRowEvent.emit()
  }

  changeTableTextStyle(style) {
    this.textAlignmentSwitch = style
    this.changeTableTextStyleEvent.emit(style)
  }

  mergeDataHandler(event) {
    this.mergeDataEvent.emit(event)
  }
}
