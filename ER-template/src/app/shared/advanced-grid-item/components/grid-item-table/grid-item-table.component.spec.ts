import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GridItemTableComponent } from './grid-item-table.component';

describe('GridItemTableComponent', () => {
  let component: GridItemTableComponent;
  let fixture: ComponentFixture<GridItemTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GridItemTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GridItemTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
