import { animate, state, style, transition, trigger } from '@angular/animations';
import { ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { MatSnackBar } from '@angular/material/snack-bar';
import { fadeInUp400ms } from 'src/@exai/animations/fade-in-up.animation';
import { stagger40ms } from 'src/@exai/animations/stagger.animation';
import { LanguageService } from 'src/app/language.service';
import { SelectFilter } from '../../interfaces/select-filter';

import jsPDF from 'jspdf';
import { DateFormatPipe } from '../../pipes/date-format.pipe';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmDialogComponent } from '../../dialogs/confirm-dialog/confirm-dialog.component';
import { HttpService } from 'src/@exai/services/http.service';
import { Observable } from 'rxjs/internal/Observable';
import { tap } from 'rxjs/operators';
import { DataService } from 'src/app/police-app/services/data-service.service';
import { SnackBarService } from 'src/app/shared/services/snack-bar.service';
import { MatTable } from '@angular/material/table';





@Component({
  selector: 'exai-grid-item-table',
  templateUrl: './grid-item-table.component.html',
  styleUrls: ['./grid-item-table.component.scss'],
  providers: [DateFormatPipe],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
    fadeInUp400ms,
    stagger40ms
  ]
})
export class GridItemTableComponent implements OnInit {
  selectFilterText: SelectFilter[] = [
    { label: 'Start with', filterType: 'text' },
    { label: 'Contains', filterType: 'text' },
    { label: 'Not contains', filterType: 'text' },
    { label: 'Ends with', filterType: 'text' },
    { label: 'Equals', filterType: 'text' },
  ]

  selectFilterNumber: SelectFilter[] = [
    { label: 'Equals', filterType: 'number' },
    { label: 'Greater then', filterType: 'number' },
    { label: 'Greater or equal to', filterType: 'number' },
    { label: 'Less then', filterType: 'number' },
    { label: 'Less or equal to', filterType: 'number' },
  ]

  selectFilterDate: SelectFilter[] = [
    { label: 'Equals', filterType: 'number' },
    { label: 'Greater then', filterType: 'number' },
    { label: 'Greater or equal to', filterType: 'number' },
    { label: 'Less then', filterType: 'number' },
    { label: 'Less or equal to', filterType: 'number' },
  ]

  presetButtonsDate: SelectFilter[] = [
    { buttonLabel: 1, label: 'preset 1', filterType: 'date' },
    { buttonLabel: 3, label: 'preset 3', filterType: 'date' },
    { buttonLabel: 7, label: 'preset 7', filterType: 'date' }
  ]

  expandedRow
  filterValue = null
  filterType = null

  @Input() selection
  @Input() columns
  @Input() dataSource
  @Input() rowNumber
  @Input() pageSizeOptions
  @Input() pageSize
  @Input() summeryAlwaysVisible
  @Input() tableTextStyle
  @Input() visibleColumns
  @Input() tableRowsData
  @Input() isAllSelected
  @Input() showIndex
  @Input() activateHideFromUser
  @Input() readOnly
  @Input() cellValidators = true
  @Input() showAlignButton = true
  @Input() showThOptions
  @Input() showErrorMessages = true
  @Input() showEditDropdownButton = true
  @Input() templateColumns = []
  @Input() editForm
  @Input() selectPrimaryKey
  @Input() showConfirmRow = true
  @Input() showMathOperationsColumn = false
  @Input() showTotalButton = false
  @Input() onConfirmError;
  @Input() tableHeaderRows = [];
  @Input() tableHeaderLength = [];
  @Input() showEditHeader = true;
  @Input() showAddHeaderNameButton = true;

  @Output() dropEvent = new EventEmitter()
  @Output() masterToggleEvent: EventEmitter<any> = new EventEmitter()
  @Output() itemActionEvent: EventEmitter<any> = new EventEmitter()
  @Output() confirmRowEvent: EventEmitter<any> = new EventEmitter()
  @Output() createColumnDataEvent: EventEmitter<any> = new EventEmitter()
  @Output() clearFilterRulesEvent: EventEmitter<any> = new EventEmitter()
  @Output() applyFiltersEvent: EventEmitter<any> = new EventEmitter()
  @Output() deleteColumnEvent: EventEmitter<any> = new EventEmitter()
  @Output() editColumnEvent: EventEmitter<any> = new EventEmitter()
  @Output() checkboxCheckedEvent: EventEmitter<any> = new EventEmitter()
  @Output() setLabelValuesEvent: EventEmitter<any> = new EventEmitter()
  @Output() actionButtonEvent: EventEmitter<any> = new EventEmitter()
  @Output() openFileEvent: EventEmitter<any> = new EventEmitter()
  @Output() addEditPoliceStationEvent: EventEmitter<any> = new EventEmitter()
  @Output() updateFormEvent: EventEmitter<any> = new EventEmitter()
  @Output() disableEvent: EventEmitter<any> = new EventEmitter()
  @Output() mergeCellEvent: EventEmitter<any> = new EventEmitter()
  @Output() alignCellVerticallyEvent: EventEmitter<any> = new EventEmitter()
  @Output() alignCellHorizontallyEvent: EventEmitter<any> = new EventEmitter()
  @Output() updateRowFilesEvent: EventEmitter<any> = new EventEmitter()
  @Output() addColumnNumberEvent: EventEmitter<any> = new EventEmitter()
  @Output() editSelectableEvent: EventEmitter<any> = new EventEmitter()
  @Output() toggleTotalEvent: EventEmitter<any> = new EventEmitter()
  @Output() updateTableValuesEvent: EventEmitter<any> = new EventEmitter()
  @Output() addHeaderNameEvent: EventEmitter<any> = new EventEmitter()
  @Output() editHeaderEvent: EventEmitter<any> = new EventEmitter()
  @Output() removeHeaderRowEvent: EventEmitter<any> = new EventEmitter()

  @ViewChild("table") table: ElementRef;

  constructor(
    private _snackBar: MatSnackBar,
    private snackBarService: SnackBarService,
    public languageService: LanguageService,
    public dialog: MatDialog,
    private httpService: HttpService,
    private dataService: DataService,
  ) { }

  showTotalFooter = 0
  primaryKeyList

  get showTotalRow() {
    return this.columns.some(column => column.showTotal)
  }

  get hideFooter () {    
    return this.httpService.roleId != 4
  }

  get visibleHeaders() {
    if (this.tableHeaderRows) return this.tableHeaderRows.filter((header) => header.visible).map((header) => header.id);
  }

  headerRows = [1,2]

  ngOnInit(): void {
    if (this.selectPrimaryKey  && this.dataService.curFormId) {
      this.httpService.getPrimaryKeysBySubDivision(this.dataService.curFormId).subscribe(data => {this.primaryKeyList = data})
    }
  }

  removeHeaderRowHandler(headerIndex) {
    this.removeHeaderRowEvent.emit(headerIndex)
  }

  editHeaderHandler(headerRowIndex, columnProperty) {
    this.editHeaderEvent.emit({headerRowIndex, columnProperty})
  }

  updateMathOperationCells(row) {
    for (let key in row ) {
      if (row[key].type === 'mathOperations') {
        this.getMathValue(row, key)
      }      
    }
        
  }

  addHeaderNameHandler(headerRowIndex, columnProperty) {    
    this.addHeaderNameEvent.emit({ headerRowIndex, columnProperty })
  }

  toggleTotalHandler(column) {
    this.showTotalFooter++
    this.toggleTotalEvent.emit(column)
  }

  getFooterTableValue(columnProperty) {
    let totalNumber = 0
    this.dataSource.data.forEach(row => {
      if (row[columnProperty].value) {
        totalNumber += row[columnProperty].value        
      }
    })
    this.columns.filter(column => {
      if (column.property === columnProperty) {
        column["totalValue"] = totalNumber
      }
    })
    return totalNumber.toFixed(2)
  }

  getMathValue(row, key) {
    
    let operations = ['+', '-', '*', '/']
    let wantedString = ''
    let valid = true
    this.columns.forEach(column => {
      if (column.property === key) {
        if (column.mathOperationsData.length) {
          column.mathOperationsData.forEach(element => {
            if (operations.indexOf(element) < 0) {
              wantedString += row[element].value
            } else {
              wantedString += element
            }
          })
        } else {
          valid = false
        }
      }
    })
    if (valid) {      
      try {
        row[key].value = eval(wantedString)
        return eval(wantedString)
      } catch {        
        row[key].value = '/'
        return ''
      }
    }
  }

  updateTableValuesHandler() {
    this.updateTableValuesEvent.emit()
  }
  
  alignCell(event, row, columnProperty) {
    if (event.checked) {
      this.alignCellVerticallyEvent.emit({ row, columnProperty })
    } else {
      this.alignCellHorizontallyEvent.emit({ row, columnProperty })
    }
  }

  editSelectableHandler(column, row = null) {
    this.editSelectableEvent.emit({ column, row })
  }

  onClickDownload(event) {
    this.downloadPDF();
  }

  updateFilesValues(event, row, columnProperty) {
    this.updateRowFilesEvent.emit({ event, row, columnProperty })
  }

  public downloadPDF() {
    // console.log(this.content.nativeElement.innerHTML);
    const doc = new jsPDF();
    const specialElementHandlers = {
      '#editor': function (element, renderer) {
        return true;
      }
    };

    const content = this.table["_elementRef"].nativeElement;

    doc.fromHTML(content.outerHTML, 15, 15, {
      width: 190,
      'elementHandlers': specialElementHandlers
    });

    doc.save('test.pdf');
  }

  exportHTML() {
    var header = "<html xmlns:o='urn:schemas-microsoft-com:office:office' " +
      "xmlns:w='urn:schemas-microsoft-com:office:word' " +
      "xmlns='http://www.w3.org/TR/REC-html40'>" +
      "<head><meta charset='utf-8'><title>Export HTML to Word Document with JavaScript</title></head><body>";
    var footer = "</body></html>";
    var sourceHTML = header + document.getElementById("table").outerHTML + footer;

    var source = 'data:application/vnd.ms-word;charset=utf-8,' + encodeURIComponent(sourceHTML);
    var fileDownload = document.createElement("a");
    document.body.appendChild(fileDownload);
    fileDownload.href = source;
    fileDownload.download = 'document.doc';
    fileDownload.click();
    document.body.removeChild(fileDownload);
  }

  addEditPoliceStationHandler(row) {
    this.addEditPoliceStationEvent.emit(row)
  }

  updateFormHandler(row) {
    this.updateFormEvent.emit(row)
  }

  trackByProperty<T>(index: number, column) {
    return column.property;
  }

  trackById<T>(index: number, column) {
    return column.id;
  }

  dropHandler(event) {
    this.dropEvent.emit(event)
  }

  masterToggleHandler() {
    this.masterToggleEvent.emit()
  }

  itemActionHandler(row, action) {
    if (action === 'deleteSelected') {
      let message = 'Are you sure you want to delete this row?'
      const confirmDialog = this.dialog.open(ConfirmDialogComponent, {
        width: '500px',
        data: {
          message: message,
        }
      });

      confirmDialog.afterClosed().subscribe(result => {
        if (result) {
          this.itemActionEvent.emit({ row, action })
        }
      })
    } else {
      this.itemActionEvent.emit({ row, action })
    }
  }

  confirmRowHandler(row) {
    this.confirmRowEvent.emit(row)
  }

  createColumnDataHandler(index) {
    this.createColumnDataEvent.emit(index)
  }

  clearFilterRulesHandler(property, filterType, column, filterIndex, wholeColumn) {
    this.clearFilterRulesEvent.emit({ property, filterType, column, filterIndex, wholeColumn })
  }

  applyFiltersHandler(event, column) {

    if (column.rangeDate && column.rangeDate.start !== null) {
      this.filterValue = column.rangeDate.value
      this.filterType = { label: 'rangeDate', filterType: 'date' }
    }
    console.log(column);

    if (column.expectedFilterType === 'date' && (!this.filterType || !column.rangeDate.controls.end)) {
      event.stopPropagation()
      this.openSnackBar('Please fill both range values')
      return
    }
    this.applyFiltersEvent.emit({ column, filterValue: this.filterValue, filterType: this.filterType })
    this.filterValue = null
    this.filterType = null
    if (column.rangeDate && column.rangeDate.start) {
      column.rangeDate.start = null
      column.rangeDate.end = null
    }
  }

  deleteColumnHandler(index, column) {
    this.deleteColumnEvent.emit({ index, column })
  }

  editColumnHandler(index) {
    this.editColumnEvent.emit(index)
  }

  checkboxCheckedHandler(row, checkbox, event) {
    this.checkboxCheckedEvent.emit({ row, checkbox, event })
  }

  setLabelValuesHandler(row, property) {
    this.setLabelValuesEvent.emit({ row, property })
  }

  actionButtonHandler(event, row) {
    this.actionButtonEvent.emit({ event, row })
  }

  openFileHandler(url) {
    this.openFileEvent.emit(url)
  }

  getRatingsUniqueId(columnId, rowId, ratingsIndex) {
    return `${columnId}-${rowId}-${ratingsIndex}`
  }

  setDateTimeFilterValues(column, filterValue, filterType) {
    this.filterValue = filterValue
    this.filterType = filterType
    this.applyFiltersEvent.emit({ column, filterValue: this.filterValue, filterType: this.filterType })
    this.filterValue = null
    this.filterType = null
  }

  openSnackBar(message: string) {
    this._snackBar.open(message, 'Confirm', {
      duration: 5000
    });
  }

  disableHandler(ev: MatSlideToggleChange, row) {
    this.disableEvent.emit(row)
  }

  mergeCellHandler(row) {
    this.mergeCellEvent.emit(row)
  }

  addColumnNumberHandler(newTable, currColumnIndex, rightSide) {
    this.addColumnNumberEvent.emit({ newTable, currColumnIndex, rightSide })
  }

}