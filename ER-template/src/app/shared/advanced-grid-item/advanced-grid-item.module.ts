import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common'

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { FlexLayoutModule } from '@angular/flex-layout';
import { AngularMaterialModel } from '../angular-material.module';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { NgxDocViewerModule } from 'ngx-doc-viewer';
import {NgxPrintModule} from 'ngx-print';




import { AdvancedGridItemComponent } from './advanced-grid-item.component';
import { GridItemTableHeaderComponent } from './components/grid-item-table-header/grid-item-table-header.component';
import { GridItemTableComponent } from './components/grid-item-table/grid-item-table.component';
import { AddColumnDialogComponent } from './dialogs/add-column-dialog/add-column-dialog.component';
import { ConfirmDialogComponent } from './dialogs/confirm-dialog/confirm-dialog.component';
import { NewColumnDialogComponent } from './dialogs/new-column-dialog/dialog.component';
import { SetLabelsComponent } from './dialogs/set-labels/set-labels.component';
import { MergeDataComponent } from './components/merge-data/merge-data.component';
import { DateFormatPipe } from './pipes/date-format.pipe';
import { MAT_DATE_LOCALE } from '@angular/material/core';
import { FormBuilderModule } from 'src/app/form-builder/form-builder.module';
import { UploadFileComponent } from './components/upload-file/upload-file.component';
import { RouterModule } from '@angular/router';
import { DocViewerComponent } from './dialogs/doc-viewer/doc-viewer.component';
import { EditSelectableDataComponent } from './dialogs/edit-selectable-data/edit-selectable-data.component';
import { MatTabsModule } from '@angular/material/tabs';
import { PoctureDialogComponent } from './dialogs/pocture-dialog/pocture-dialog.component';
import {MatSliderModule} from '@angular/material/slider';
import { HyperlinkComponent } from './components/hyperlink/hyperlink.component';
import { CreateHyperlinkComponent } from './dialogs/create-hyperlink/create-hyperlink.component';
import { AddNewFormComponent } from 'src/app/police-app/forms/add-new-form/add-new-form.component';
import { PortalModule } from '@angular/cdk/portal';
import { OverlayModule } from '@angular/cdk/overlay';
import { NgxMatIntlTelInputModule } from 'ngx-mat-intl-tel-input';
import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatChipsModule } from '@angular/material/chips';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectModule } from '@angular/material/select';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatTooltipModule } from '@angular/material/tooltip';
import { IconModule } from '@visurel/iconify-angular';
import { PageLayoutModule } from 'src/@exai/components/page-layout/page-layout.module';
import { ContainerModule } from 'src/@exai/directives/container/container.module';
import { BasicTableModule } from '../basic-table/basic-table.module';
import { FormBuilderSharedModule } from '../form-status-history/form-builder-shared.module';
import { FormStatusHistoryModule } from '../form-status-history/form-status-history.module';
import { TableFormComponent } from './components/table-form/table-form.component';
import { TableFormViewComponent } from './components/table-form-view/table-form-view.component';
import { AddHeaderNameComponent } from './dialogs/add-header-name/add-header-name.component';
import { AddHeaderRowsComponent } from './dialogs/add-header-rows/add-header-rows.component';


@NgModule({
  declarations: [
    AdvancedGridItemComponent,
    GridItemTableComponent,
    GridItemTableHeaderComponent,
    AddColumnDialogComponent,
    ConfirmDialogComponent,
    NewColumnDialogComponent,
    SetLabelsComponent,
    MergeDataComponent,
    DateFormatPipe,
    UploadFileComponent,
    DocViewerComponent,
    EditSelectableDataComponent,
    PoctureDialogComponent,
    HyperlinkComponent,
    CreateHyperlinkComponent,
    AddNewFormComponent,
    TableFormComponent,
    TableFormViewComponent,
    AddHeaderNameComponent,
    AddHeaderRowsComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    AngularMaterialModel,
    FormsModule,
    ReactiveFormsModule,
    NgxMaterialTimepickerModule,
    FlexLayoutModule,
    DragDropModule,
    NgxDocViewerModule,
    NgxPrintModule,
    MatTabsModule,

    MatCardModule,
    MatButtonModule,
    FlexLayoutModule,
    PageLayoutModule,
    ContainerModule,
    MatIconModule,
    MatTableModule,
    MatPaginatorModule,
    MatCheckboxModule,
    MatDialogModule,
    MatSelectModule,
    MatSortModule,
    MatMenuModule,
    IconModule,
    MatTooltipModule,
    ReactiveFormsModule,
    MatButtonToggleModule,
    AngularMaterialModel,
    MatSlideToggleModule,
    MatInputModule,
    MatProgressSpinnerModule,
    MatTabsModule,
    MatSnackBarModule,
    MatChipsModule,
    BasicTableModule,
    MatProgressSpinnerModule,
    FormBuilderSharedModule,
    FormStatusHistoryModule,

    NgxMatIntlTelInputModule,
    OverlayModule,
    PortalModule
  ],
  exports: [
    AdvancedGridItemComponent,
    GridItemTableComponent,
    GridItemTableHeaderComponent,
    AddColumnDialogComponent,
    ConfirmDialogComponent,
    NewColumnDialogComponent,
    SetLabelsComponent,
    MergeDataComponent,
    DateFormatPipe,
    UploadFileComponent,
    DocViewerComponent,
    EditSelectableDataComponent,
    PoctureDialogComponent,
    HyperlinkComponent,
    CreateHyperlinkComponent,
    AddNewFormComponent,
    TableFormComponent,
    TableFormViewComponent,
    AddHeaderNameComponent,
    AddHeaderRowsComponent,
  ],
  providers: [{ provide: MAT_DATE_LOCALE, useValue: 'en-GB' }],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ],
})
export class AdvancedGridItemModule { }
