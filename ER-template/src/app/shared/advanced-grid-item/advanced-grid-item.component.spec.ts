import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdvancedGridItemComponent } from './advanced-grid-item.component';

describe('AdvancedGridItemComponent', () => {
  let component: AdvancedGridItemComponent;
  let fixture: ComponentFixture<AdvancedGridItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdvancedGridItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvancedGridItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
