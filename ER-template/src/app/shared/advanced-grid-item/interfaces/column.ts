import { FormGroup } from "@angular/forms";

export interface Column {
    expectedFilterType?: string,
    filterRules?: any,
    id?: string,
    label: string,
    property: string,
    rangeDate?: FormGroup,
    type: string,
    visible: boolean,
    dropdownData?: any,
    ratings?: number[],
    format?: string,
    addedData?: boolean
}
