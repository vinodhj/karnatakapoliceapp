export interface SelectFilter {
    label: string,
    filterType: string,
    buttonLabel?: number
}
