import { TestBed } from '@angular/core/testing';

import { DownloadToExcelService } from './download-to-excel.service';

describe('DownloadToExcelService', () => {
  let service: DownloadToExcelService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DownloadToExcelService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
