import { Injectable } from '@angular/core';
import { HttpService } from 'src/@exai/services/http.service';

@Injectable({
  providedIn: 'root'
})
export class DownloadToExcelService {

  constructor(
    private httpService: HttpService
  ) { }

  unwantedRowKeys = ['edit', 'hidden', 'id', 'userCantChange', 'actionsMergeStart', 'actionsRowSpan', 'cellVertical', 'visible', 'hiddenFromUser']

  downloadMasterTableToExcel(rows, ShowLabel = true, columns = null) {
    let jsonForExcel = []
    rows.forEach(Row => {
      let row = JSON.parse(JSON.stringify(Row))
      let obj = {}
      for (let key in row) {
        // check if the key is a key/value pair with column valid column
        // if (key !== 'edit' && key !== 'hidden' && key !== 'id' && key !== 'userCantChange' && key !== 'actionsMergeStart' && key !== 'actionsRowSpan' && key !== 'cellVertical' && key !== 'visible' && key !== 'hiddenFromUser') {
        if (this.unwantedRowKeys.indexOf(key) < 0) {
          if (row[key]) {
            if (row[key].type === 'mathOperations') {
              if (this.httpService.roleId == 4) {
                obj[key] = row[key].value
              }
            } else if (row[key].type === 'file') {
              let valueToDisplay = []
              row[key].value.forEach(file => {
                valueToDisplay.push(file.name)
              })
              row[key].value = valueToDisplay.join(', ')
            } else if (row[key].type === 'hyperlink') {
              let valueToDisplay = []
              row[key].value.forEach(link => {
                valueToDisplay.push(link.link)
              })
              row[key].value = valueToDisplay.join(', ')
            } else if (row[key].type === 'percentage') {
              obj[key] = `${ +row[key].value * 100} %`
            } else {
              obj[key] = row[key].value
            }
          }
        }
      }
      jsonForExcel.push(obj)
    })

    if (columns && this.httpService.roleId == 4) {
      let obj = {}
      columns.forEach((column, index) => {
        if (index > 1) {
          if (column.totalValue) {
            obj[column.property] = column.totalValue
          } else {
            obj[column.property] = ''
          }
        }
      })
      jsonForExcel.push(obj)
    }

    let arrData = typeof jsonForExcel != 'object' ? JSON.parse(jsonForExcel) : jsonForExcel;
    var CSV = 'sep=,' + '\r\n\n';

    //This condition will generate the Label/Header
    if (ShowLabel) {
      var row = "";

      //This loop will extract the label from 1st index of on array
      for (var index in arrData[0]) {
        //Now convert each value to string and comma-seprated
        row += index + ',';
      }

      row = row.slice(0, -1);

      //append Label row with line break
      CSV += row.toUpperCase() + '\r\n';
    }

    //1st loop is to extract each row
    for (var i = 0; i < arrData.length; i++) {
      var row = "";

      //2nd loop will extract each column and convert it in string comma-seprated
      for (var index in arrData[i]) {
        row += '"' + arrData[i][index] + '",';
      }

      row.slice(0, row.length - 1);

      //add a line break after each row
      CSV += row + '\r\n';
    }

    if (CSV == '') {
      alert("Invalid data");
      return;
    }

    //Generate a file name
    var fileName = "Grid_Item_Table_";

    //Initialize file format you want csv or xls    
    var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);

    var link = document.createElement("a");
    link.href = uri;

    //set the visibility hidden so it will not effect on your web-layout
    // link.style = "visibility:hidden";
    link.download = fileName + ".csv";

    //this part will append the anchor tag and remove it after automatic click
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);

  }

}
