// this service can be used to intercept and do anything with a http call, such as adding authorisation token to the request
// and adding error handler
import { Injectable, Injector } from "@angular/core";
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse, JsonpClientBackend } from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { catchError } from "rxjs/operators";
import { HttpService } from "src/@exai/services/http.service";
import { Router } from "@angular/router";
@Injectable({
  providedIn: "root",
})
export class HttpInterceptorService implements HttpInterceptor {
  constructor(private injector: Injector, private router: Router, private httpService: HttpService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let httpService = this.injector.get(HttpService);

    console.log(req.url);

    let authorizationHeader = `Bearer ${httpService.token}`;

    let tokenizedRequest = null;

    if(!req.url.includes('.s3.')) {
      tokenizedRequest = req.clone({
        setHeaders: {
          Authorization: authorizationHeader,
          UserData: JSON.stringify({
            branchId: httpService?.branchId,
            subDivisionId: httpService?.subDivisionId,
            divisionId: httpService?.divisionId
          })
        },
      });
    } else {
      tokenizedRequest = req.clone({
        setHeaders: {
          UserData: JSON.stringify({
            branchId: httpService?.branchId,
            subDivisionId: httpService?.subDivisionId,
            divisionId: httpService?.divisionId
          })
        },
      });
    }

    return next.handle(tokenizedRequest).pipe(
      catchError((error: HttpErrorResponse) => {
        let errorMsg = "";
        if (error.error instanceof ErrorEvent) {
          console.log("this is client side error");
          errorMsg = `Error client side error: ${error.error.message}`;

          this.httpService.errorLogCreate(this.router.url, null, errorMsg).subscribe(res => {
            console.log(res);
          })

        } else {
          console.log("this is server side error");
          errorMsg = `Error Code: ${error.status},  Message: ${error.message}`;

          console.log(error, "ERROR HAPPENED");

          this.httpService.errorLogCreate(this.router.url, null, errorMsg).subscribe(res => {
            console.log(res);
          })
        }

        return throwError(error);
      })
    );;
  }
}
