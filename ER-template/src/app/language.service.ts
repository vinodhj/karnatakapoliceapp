import { Injectable } from '@angular/core';
import { HttpService } from 'src/@exai/services/http.service';
@Injectable({
  providedIn: 'root'
})
export class LanguageService {
  language:any = null;
  constructor(private httpService:HttpService) {
    this.httpService.getJson('en').subscribe((response:any)=>{
      console.log(response);
      
      this.language = response;
    });
  }
}
