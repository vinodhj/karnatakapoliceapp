import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanActivateChild, CanDeactivate, CanLoad, Route, Router, RouterStateSnapshot, UrlSegment, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { HttpService } from 'src/@exai/services/http.service';
import { Role } from '../shared/enums/role.enum';

@Injectable({
  providedIn: 'root'
})

export class AuthenticationGuard implements CanActivate, CanActivateChild, CanDeactivate<unknown>, CanLoad {

  constructor(private httpService: HttpService, private router: Router) { }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.checkUserLogin(route);
  }

  canActivateChild(
    childRoute: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.canActivate(childRoute, state);
  }

  canDeactivate(
    component: unknown,
    currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot,
    nextState?: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return true;
  }

  canLoad(
    route: Route,
    segments: UrlSegment[]): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return true;
  }

  checkUserLogin(route: ActivatedRouteSnapshot): boolean {

    if (!this.httpService.isLoggedIn()) {
      this.router.navigate([`/login`]);
      return false;
    }

    const userRole = this.httpService.getRole();

    if (userRole === null || userRole === undefined) {
      this.router.navigate([`/login`]);
      return false;
    }

    if (route.data === null || route.data === undefined || Object.keys(route.data).length === 0)
      return true;

    if (route.data?.roles?.length === 0) {
      this.router.navigate([`/dashboard`]);
      return false;
    }

    if (!route.data?.roles?.map(String)
      .includes(userRole.toString())) {
      this.router.navigate([`/dashboard`]);
      return false;
    }

    return true;
  }
}