import { DOCUMENT } from '@angular/common';
import { AfterViewInit, ContentChild, Directive, ElementRef, EventEmitter, Inject, Input, OnDestroy, Output } from '@angular/core';
import { fromEvent, Subscription } from 'rxjs';
import { debounceTime, delay, takeUntil } from 'rxjs/operators';
import { DragHandlerElementDirective } from './drag-handler-element.directive';

@Directive({
  selector: '[exaiDragElement]'
})
export class DragElementDirective implements AfterViewInit, OnDestroy {

  private element!: HTMLElement;

  private subscriptions: Subscription[] = [];

  @Input() allowVertical = true;
  @Input() allowHorizontal = true;
  @Output() dragStarted = new EventEmitter();
  @Output() dragEnd = new EventEmitter();

   // 1 Added
   @ContentChild(DragHandlerElementDirective) handle!: DragHandlerElementDirective;
   handleElement!: HTMLElement;

  constructor(
    private elementRef: ElementRef,
    @Inject(DOCUMENT) private document: any
  ) {}

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    this.element = this.elementRef.nativeElement as HTMLElement;
    this.handleElement = this.handle?.elementRef?.nativeElement || this.element;
    this.initDrag();
  }

  initDrag(): void {
    const dragStart$ = fromEvent<MouseEvent>(this.handleElement, "mousedown");
    const dragEnd$ = fromEvent<MouseEvent>(this.document, "mouseup");
    const drag$ = fromEvent<MouseEvent>(this.document, "mousemove").pipe(
      takeUntil(dragEnd$)
    );

    // 2
    let initialX: number,
      initialY: number,
      currentX = 0,
      currentY = 0;

    let dragSub: Subscription;

    // 3
    const dragStartSub = dragStart$.subscribe((event: MouseEvent) => {
      initialX = event.clientX - currentX;
      initialY = event.clientY - currentY;
      this.element.classList.add('free-dragging');

      this.dragStarted.emit();

      // 4
      dragSub = drag$.subscribe((event: MouseEvent) => {
        event.preventDefault();

        

        currentX = event.clientX - initialX;
        currentY = event.clientY - initialY;

        if(this.allowHorizontal && this.allowVertical)
        this.element.style.transform =
          "translate3d(" + currentX + "px, " + currentY + "px, 0)";

        if(this.allowHorizontal && !this.allowVertical)
          this.element.style.transform =
            "translateX(" + currentX + "px)";

        if(!this.allowHorizontal && this.allowVertical)
          this.element.style.transform =
            "translateY(" + currentX + "px)";
       
      });
    });

    // 5
    const dragEndSub = dragEnd$.pipe(
      //delay time because of clickoutside directive
      debounceTime(1)
    ).subscribe(() => {
      initialX = currentX;
      initialY = currentY;
      this.element.classList.remove('free-dragging');
      this.dragEnd.emit();
      if (dragSub) {
        dragSub.unsubscribe();
      }
    });

    // 6
    this.subscriptions.push.apply(this.subscriptions, [
      dragStartSub,
      dragSub!,
      dragEndSub,
    ]);
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((s) => s?.unsubscribe());
  }
}
