import { Directive, ElementRef, EventEmitter, HostBinding, HostListener, Output } from '@angular/core';


@Directive({
  selector: '[exaiFileDragDrop]'
})
export class FileDragDropDirective {
  @HostBinding('class.fileover') fileOver: boolean;
  @Output() filesDropped = new EventEmitter<any>();

  constructor(private el: ElementRef) { }

  @HostListener('dragover', ['$event']) onDragOver(event) {
    event.preventDefault();
    event.stopPropagation();

   

    this.fileOver = true;
  }

  @HostListener('dragleave', ['$event']) onDragLeave(event) {
    event.preventDefault();
    event.stopPropagation();
    
    //fix hovering over child elements issue
    if(!event.currentTarget.contains(event.relatedTarget))
      this.fileOver = false;
  }

  @HostListener('drop', ['$event']) onDrop(event) {
    event.preventDefault();
    event.stopPropagation();

    this.fileOver = false;
    const files = event.dataTransfer.files;

    if(files.length > 0) {
      this.filesDropped.emit(files);
    }
  }

}
