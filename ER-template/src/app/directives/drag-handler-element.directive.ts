import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[exaiDragHandlerElement]'
})
export class DragHandlerElementDirective {

  constructor(public elementRef: ElementRef<HTMLElement>) {}
}
