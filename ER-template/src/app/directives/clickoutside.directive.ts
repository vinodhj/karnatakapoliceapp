import { Directive, ElementRef, EventEmitter, HostListener, Input, Output } from '@angular/core';

@Directive({
  selector: '[clickOutside]'
})
export class ClickoutsideDirective {

  @Output() clickOutside = new EventEmitter<void>();

  @Input() noTriggerElements: any[] = []; 
  @Input() noTriggerClasses: string[] = [];
  @Input() disableClickOutside: boolean = false;

  constructor(private elementRef: ElementRef) { }

  @HostListener('document:click', ['$event.target'])
  public onClick(target: any) {

    if(this.disableClickOutside) return;

    const clickedInside = this.elementRef.nativeElement.contains(target);
    
    const clickedEl = !this.noTriggerElements.some(el => el.contains(target));
 
    const clickedClass = !this.noTriggerClasses.some(className => target.classList.contains(className));

    if (!clickedInside && clickedEl && clickedClass) {
      this.clickOutside.emit(target);
    }
  }

}
