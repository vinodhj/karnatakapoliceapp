import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'controlName',
  pure: true
})
export class ControlNamePipe implements PipeTransform {

  transform(element: any, ...args: unknown[]): string {
    return element.label.split(' ').join('-').toLowerCase() + '+' + element.id;
  }

}
