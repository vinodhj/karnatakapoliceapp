import { AfterViewInit, Directive, ElementRef, EventEmitter, Input, OnDestroy, Output } from '@angular/core';
import Moveable, { OnScale } from "moveable";
import Resizable from 'resizable';
import { from, fromEvent, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Directive({
    selector: '[exaiMoveable]'
})
export class MoveableDirective implements AfterViewInit, OnDestroy {
    @Input() container;
    @Input() draggableOnly: boolean;
    @Output() resizing = new EventEmitter();
    @Output() resizeEnd = new EventEmitter();
    @Output() resizeStart = new EventEmitter();

    @Output() dragging = new EventEmitter();

    unsubscribe$ = new Subject();

    resizable;

    constructor(private el: ElementRef) { }

    ngAfterViewInit(): void {

        this.resizable = new Resizable(this.el.nativeElement, {
            threshold: 10,
            draggable: true,
            within: this.container ?? document.body,
        });

        fromEvent(this.resizable.draggable, 'drag').pipe(
            takeUntil(this.unsubscribe$)
        ).subscribe(() => {

            const draggableInstance = this.resizable.draggable;

            this.dragging.emit({ x: draggableInstance.prevX, y: draggableInstance.prevY, z: 0 });
            
        })

        fromEvent(this.resizable, 'resize').pipe(
            takeUntil(this.unsubscribe$)
        ).subscribe(event => {

            this.resizing.emit({ width: this.resizable.element.offsetWidth, height: this.resizable.element.offsetHeight });

        })

        fromEvent(this.resizable, 'resizestart').pipe(
            takeUntil(this.unsubscribe$)
        ).subscribe(event => {

            this.resizeStart.emit({ width: this.resizable.element.offsetWidth, height: this.resizable.element.offsetHeight });

        })

        fromEvent(this.resizable, 'resizeend').pipe(
            takeUntil(this.unsubscribe$)
        ).subscribe(event => {

            const draggableInstance = this.resizable.draggable;
            const position = { x: draggableInstance.prevX, y: draggableInstance.prevY, z: 0 };

            this.resizeEnd.emit({ width: this.resizable.element.offsetWidth, height: this.resizable.element.offsetHeight, position });

        })

        if(this.draggableOnly) {

           const handles = this.el.nativeElement.querySelectorAll(':scope > .resizable-handle');

           //if it's draggable hide handles
           handles.forEach(element => {
                element.style.display = 'none';
           });
        }
    }

    ngOnDestroy(): void {
        this.resizable.destroy();

        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }
}
