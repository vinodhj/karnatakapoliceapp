import { DOCUMENT } from '@angular/common';
import { AfterViewInit, ChangeDetectorRef, Directive, ElementRef, HostListener, Inject, Input, OnChanges, OnDestroy, SimpleChanges } from '@angular/core';
import Guides from "@scena/guides";
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

export enum RulerDirection {
  horizontal = 'horizontal',
  vertical = 'vertical'
}

@Directive({
  selector: '[exaiGuides]'
})
export class GuidesDirective implements AfterViewInit, OnDestroy {
  @Input() rulerDirection: RulerDirection = RulerDirection.horizontal;
  @Input() zoomObserver: Observable<number>;

  unsubscribe$ = new Subject();

  guides;
  scrollX = 0;
  scrollY = 0;

  constructor(private el: ElementRef, @Inject(DOCUMENT) private document: Document, private cdr: ChangeDetectorRef) {}

  ngAfterViewInit(): void {
    this.initializeGuides();

    if(this.zoomObserver)
    this.zoomObserver.pipe(
      takeUntil(this.unsubscribe$)
    ).subscribe(value => {
      //resize guides on zoom
      this.guides.resize()
      this.cdr.detectChanges();
    })
  }


  initializeGuides() {

    const rulerStyleHorizontal = { left: "30px", width: "calc(100% - 30px)", height: "100%" };
    const rulerStyleVertical = { top: "30px", height: "calc(100% - 30px)", width: "100%" };

    this.guides = new Guides(this.el.nativeElement, {
      type: this.rulerDirection,
      displayDragPos: true,
      zoom: 37.7952755,
      unit: 1,
      snapThreshold: 0,
      rulerStyle: this.rulerDirection === 'horizontal' ? rulerStyleHorizontal : rulerStyleVertical,
    }).on("changeGuides", e => {
      console.log(e.guides);
    });

  

    const box = this.document.querySelector('.box');

  box.addEventListener("click", () => {
    scrollX = 0;
    scrollY = 0;
    this.guides.scrollGuides(scrollY);
    this.guides.scroll(scrollX);
});
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.guides.resize();
  }

  ngOnDestroy(): void {
    this.guides.destroy();
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
