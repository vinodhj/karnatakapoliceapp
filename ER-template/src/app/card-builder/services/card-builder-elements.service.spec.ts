import { TestBed } from '@angular/core/testing';

import { CardBuilderElementsService } from './card-builder-elements.service';

describe('CardBuilderElementsService', () => {
  let service: CardBuilderElementsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CardBuilderElementsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
