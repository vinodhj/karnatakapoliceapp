import { Injectable } from '@angular/core';
import jsPDF  from 'jspdf'
import html2canvas from 'html2canvas';

@Injectable({
  providedIn: 'root'
})
export class CardBuilderPdfService {

  constructor() { }

  
  generatePdf(element) {

    html2canvas(element as any).then(canvas => {
        var imgWidth = 210;
        var pageHeight = 295;
        var imgHeight = canvas.height * imgWidth / canvas.width;
        var heightLeft = imgHeight;
        const contentDataURL = canvas.toDataURL('image/png');
        let pdfData = new jsPDF('p', 'mm', 'a4');
        var position = 0;
        pdfData.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)
        pdfData.save(`MyPdf.pdf`);
    });
  }
}
