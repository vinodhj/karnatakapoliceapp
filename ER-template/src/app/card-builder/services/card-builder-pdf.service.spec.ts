import { TestBed } from '@angular/core/testing';

import { CardBuilderPdfService } from './card-builder-pdf.service';

describe('CardBuilderPdfService', () => {
  let service: CardBuilderPdfService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CardBuilderPdfService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
