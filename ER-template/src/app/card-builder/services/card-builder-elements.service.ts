import { Injectable } from '@angular/core';
import { CardSizeConfiguration } from '../models/CardSizeConfiguration';
import * as uuid from "uuid";
import { BehaviorSubject, Subject } from 'rxjs';
import { cardBuilderTS } from './card-builder-template';
import { PREDEFINED_APPROVE_REJECT_FORM } from 'src/app/form-builder/form-builder-custom/models/predefined-approve-reject-form';

export interface CardConfiguration {
  cardSizeConfiguration: CardSizeConfiguration,
  cardParts: CardPart[]
}

export interface CardPart {
  title: string,
  imageData: {
    imageContent: string,
    imageRepeat: string,
    imagePosition: string,
    imageSize: string
  },
  elements: any[],
  type: string,
  borderColor: string,
  borderWidth: number
}

export interface CardSizesPredefined {
  title: string,
  sizes: {
    width: number,
    height: number,
    borderRadius: number,
  }
}

@Injectable()
export class CardBuilderElementsService {
  cardSizeConfigurationStatus: boolean = false;

  predefinedPageBuilder: any[] = JSON.parse(JSON.stringify(PREDEFINED_APPROVE_REJECT_FORM));

  zoom: BehaviorSubject<number> = new BehaviorSubject(0);

  savingForm$: BehaviorSubject<boolean> = new BehaviorSubject(false);

  constructor() { }

  predefinedSizesConfiguration: CardSizesPredefined[] = [
    {
      title: 'ATM Card', sizes: {
        width: 85.60,
        height: 53.98,
        borderRadius: 3.48
      }
    },
    {
      title: 'Visiting Card', sizes: {
        width: 88.9,
        height: 50.8,
        borderRadius: 0
      }
    },
  ]

  cardSizeConfigurationStatusToggle() {
    this.cardSizeConfigurationStatus = !this.cardSizeConfigurationStatus;
  }

  addElement(focusCardSide, element) {
    element = JSON.parse(JSON.stringify(element));

    element.id = uuid.v4();

    this.cardConfiguration.cardParts.map(part => {

      if (part.type === focusCardSide) {

        part.elements.push(element);

      }
    })

  }

  removeElement(parentElement, element) {
    parentElement.elements = parentElement.elements.filter(el => el.id != element.id);
  }

  // cardConfiguration: CardConfiguration = cardBuilderTS

  cardConfiguration: CardConfiguration = {
    cardSizeConfiguration: {
      width: 85.60,
      height: 53.98,
      borderRadius: 3.48
    },
    cardParts: [
      {
        title: "Front Side",
        imageData: {
          imageContent: '',
          imagePosition: 'center center',
          imageRepeat: 'no-repeat',
          imageSize: 'cover'
        },
        elements: [],
        type: 'front',
        borderColor: 'transparent',
        borderWidth: 0,
      },
      {
        title: "Back Side",
        imageData: {
          imageContent: '',
          imagePosition: 'center center',
          imageRepeat: 'no-repeat',
          imageSize: 'cover'
        },
        elements: [],
        type: 'back',
        borderColor: 'transparent',
        borderWidth: 0,
      }
    ]
  }

  elementResizeEnd(element, event) {
    element.styles.width = event.width;
    element.styles.height = event.height;
    element.styles.position = event.position;
  }

  dragginElement(element, event) {
    element.styles.position = event;
  }

  applyImageOnBothSides(elementFrom, image, isBackground = false) {

    //if it's background get parts and set them imageContent
    if(isBackground) {

      this.cardConfiguration.cardParts.map(part => {

        part.imageData.imageContent = image;

      })

      return;
    }

  }

  duplicateElement(cardSide, element) {

    this.cardConfiguration.cardParts.map(part => {

      if(cardSide.type == part.type) return;

      //deep copy of element
      const newElement = JSON.parse(JSON.stringify(element));
      //attach new id
      newElement.id = uuid.v4();

      //add to other side
      part.elements.push(newElement);

    })

  }
}
