import { TestBed } from '@angular/core/testing';

import { CardBuilderService } from './card-builder.service';

describe('CardBuilderService', () => {
  let service: CardBuilderService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CardBuilderService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
