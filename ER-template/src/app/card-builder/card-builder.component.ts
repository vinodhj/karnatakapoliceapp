import { CdkPortalOutletAttachedRef, ComponentPortal } from '@angular/cdk/portal';
import { AfterViewInit, ApplicationRef, Component, ComponentFactoryResolver, ComponentRef, ElementRef, EmbeddedViewRef, Injector, OnDestroy, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { forkJoin, Observable, of, Subject } from 'rxjs';
import { catchError, concatMap, finalize, map, switchMap, takeUntil, tap } from 'rxjs/operators';
import { fadeInUp400ms } from 'src/@exai/animations/fade-in-up.animation';
import { staggerAnimation } from 'src/@exai/animations/stagger.animation';
import { ConfigService } from 'src/@exai/services/config.service';
import { HttpService } from 'src/@exai/services/http.service';
import { FormBuilderCloneComponent } from '../form-builder/form-builder-clone/form-builder-clone.component';
import { FormBuilderElementsService } from '../form-builder/services/form-builder-elements.service';
import { FormBuilderIndexeddbService } from '../form-builder/services/form-builder-indexeddb.service';
import { CardBuilderPrintComponent } from './card-builder-result/card-builder-print/card-builder-print.component';
import { CardBuilderResultComponent } from './card-builder-result/card-builder-result.component';
import { RulerDirection } from './directives/guides.directive';
import { CardSizeConfiguration } from './models/CardSizeConfiguration';
import { CardBuilderElementsService } from './services/card-builder-elements.service';
import { CardBuilderPdfService } from './services/card-builder-pdf.service';
import { CardBuilderService } from './services/card-builder.service';

@Component({
  selector: 'exai-card-builder',
  templateUrl: './card-builder.component.html',
  styleUrls: ['./card-builder.component.scss'],
  animations: [
    fadeInUp400ms
  ],
  providers: [CardBuilderElementsService, FormBuilderElementsService]
})
export class CardBuilderComponent implements OnInit, OnDestroy {

  @ViewChild('buttonPrint') buttonPrint;

  Attach;

  isEdit = this.router.url.includes('edit');
  isFill = this.router.url.includes('fill-form');
  isPreview = this.router.url.includes('view-form');
  isCreate = this.router.url.includes('create');

  approveRejectConfiguration: boolean = false;
  minimizeActions: boolean = false;

  templateId: number;
  loadedData: boolean = false;

  unsubscribe$ = new Subject();
  cardPreview: boolean = this.isPreview || this.isFill ? true : false;

  cardSizeConfiguration: CardSizeConfiguration;
  focusCardSide: string = 'front';

  cardParts;

  zoomTransform;
  rulerDirection = RulerDirection;
  loading$: Observable<boolean>;
  cardPrintPortal: ComponentPortal<CardBuilderPrintComponent>;

  get cardSizeConfigurationActive() {
    return this.cardBuilderElements.cardSizeConfigurationStatus;
  }

  isFooterFixed$ = this.configService.config$.pipe(map(config => config.footer.fixed));

  constructor( private configService: ConfigService, private appRef: ApplicationRef,      private injector: Injector,private cardBuilderPdf: CardBuilderPdfService,private dialog: MatDialog, public cardBuilderElements: CardBuilderElementsService, public route: ActivatedRoute, private httpService: HttpService, private formBuilderIndexeddb: FormBuilderIndexeddbService, private router: Router, private snackbar: MatSnackBar, private vcRef: ViewContainerRef, private resolver: ComponentFactoryResolver ) {
    this.loading$ = this.cardBuilderElements.savingForm$; 
   }

  ngOnInit(): void {

    this.cardBuilderElements.zoom.pipe(
      takeUntil(this.unsubscribe$)
    ).subscribe(value => {

      this.zoomTransform = 1 + (+value / 100);

    })

    this.route.paramMap.pipe(
      switchMap(res => {
        this.templateId = +res.get('id');

        return this.httpService.templateGetContent(this.templateId).pipe(
          tap(data => {
            console.log(data, "HELLO WORLDE...");
          }),
          catchError(() => {
            //empty error if template is not found
            return of(null);
          }),
          finalize(() => this.loadedData = true)
        )
      })
    ).subscribe(content => {

      if(content) 
        this.cardBuilderElements.cardConfiguration = content;

        
        this.cardSizeConfiguration = this.cardBuilderElements.cardConfiguration.cardSizeConfiguration;
        this.cardParts = this.cardBuilderElements.cardConfiguration.cardParts;

    })

    // this.route.paramMap.pipe(
    //   switchMap(res => {
    //     this.templateId = +res.get('id');

    //     return this.httpService.templateGetContent(this.templateId).pipe(
    //       tap(data => {
    //         console.log(data, "HELLO WORLDE...");
    //       }),
    //       catchError(() => {
    //         //empty error if template is not found
    //         return of(null);
    //       }),
    //       concatMap((result) => {

    //         //if no data was found check db storage
    //         if (!result)
    //           return this.formBuilderIndexeddb.initializeDataIntoIndexedDb(
    //             this.templateId,
    //             this.cardBuilderElements.cardConfiguration);

    //         //data was found return result
    //         return of(result);

    //       }),
    //       finalize(() => this.loadedData = true)
    //     )
    //   }),
    //   switchMap(
    //     response => {
    //       if (response)
    //         this.cardBuilderElements.cardConfiguration = response;

    //       return this.httpService.templateGetApproveRejectContent(this.templateId);
    //     }
    //   )
    // ).subscribe((content: any) => {

    //   if (content)
    //     this.cardBuilderElements.cardConfiguration = content;

    // })

  }

  actionClicked(item) {

    if(item.multiple) return;

    if(item.actionBtn === 'cardSizeConfiguration') {
      this.cardBuilderElements.cardSizeConfigurationStatusToggle();
      
      return;
    } 

    if(item.actionBtn === 'save') {
      this.save();

      return;
    }


    this.addElement(item);
      
  }

  focusedElement(element) {
    this.focusCardSide = element;
  }

  addElement(element) {
    this.cardBuilderElements.addElement(this.focusCardSide, element);
  }

  save() {

    //if form save is already being clicked then return
    if(this.cardBuilderElements.savingForm$.value)
      return;

    const formId = +this.route.snapshot.paramMap.get('id');

    this.cardBuilderElements.savingForm$.next(true);

    forkJoin([
      this.templateUpdateContent(formId),
      this.templateUpdateApproveRejectContent(formId)
    ]).pipe(
      finalize(() => this.cardBuilderElements.savingForm$.next(false)),
    ).subscribe(response => {

      if (response[0] && response[1])
      this.router.navigate(['forms', 'config-form']);

    if (this.isEdit)
      this.snackbar.open(`Form ${response[0].message} updated successfully.`, 'Ok', {
        duration: 3000
      })

    if (this.isCreate)
      this.snackbar.open(`Form ${response[0].message} created successfully.`, 'Ok', {
        duration: 3000
      })

    })
  }

  templateUpdateApproveRejectContent(formId: number): Observable<any> {
    return this.httpService.templateUpdateApproveRejectContent(formId, JSON.stringify(this.cardBuilderElements.predefinedPageBuilder));
  }


  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  templateUpdateContent(formId: number): Observable<any> {
    return this.httpService.templateUpdateContent(formId, JSON.stringify(this.cardBuilderElements.cardConfiguration));
  }

  cloneForm() {

    let dialogConfiguration: any = {
      width: '25vw',
      height: 'min-content',
      data: {
        formId : +this.route.snapshot.paramMap.get('id'),
        cloneType: 'Card'
      },
      disableClose: true
    }

    const dialogRef = this.dialog.open(FormBuilderCloneComponent, dialogConfiguration);
  
    dialogRef.afterClosed().subscribe(res => {

      if(res) {

        this.loadedData = false;
  
        this.ngOnInit();

      }


    })
  }

  backForm() {
    if(this.isEdit)
      this.router.navigateByUrl('forms/config-form');
    else {
      this.router.navigateByUrl('forms/submitted-form');
    }
    
  }

  cancelForm() {

    const formId = +this.route.snapshot.paramMap.get('id');

    this.httpService.deleteForm(null, formId).subscribe(res => {

      this.router.navigateByUrl('forms/config-form');

    }, err => {

      this.snackbar.open('Cancelation Failed', 'Ok', {
        duration: 3000
      });

    })
  }

  printFile() {
    this.cardPrintPortal = new ComponentPortal(CardBuilderPrintComponent);
  }

  cardPrintPortalAttach(ref: CdkPortalOutletAttachedRef) {
    ref = ref as ComponentRef<CardBuilderPrintComponent>;
    ref.instance.cardConfiguration = this.cardBuilderElements.cardConfiguration;
    ref.instance.formRecordId =  +this.route.snapshot.paramMap.get('userFormId'); 
  }

} 
