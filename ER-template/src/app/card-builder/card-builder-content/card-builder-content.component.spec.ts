import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardBuilderContentComponent } from './card-builder-content.component';

describe('CardBuilderContentComponent', () => {
  let component: CardBuilderContentComponent;
  let fixture: ComponentFixture<CardBuilderContentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardBuilderContentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CardBuilderContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
