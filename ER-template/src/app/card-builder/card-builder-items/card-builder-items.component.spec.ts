import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardBuilderItemsComponent } from './card-builder-items.component';

describe('CardBuilderItemsComponent', () => {
  let component: CardBuilderItemsComponent;
  let fixture: ComponentFixture<CardBuilderItemsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardBuilderItemsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CardBuilderItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
