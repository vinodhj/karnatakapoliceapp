import { AfterViewInit, Component, ElementRef, EventEmitter, OnInit, Output, QueryList, ViewChildren } from '@angular/core';
import { Observable } from 'rxjs';
import { CardBuilderElementsService } from '../services/card-builder-elements.service';
import { elements } from './cardBuilderItems';

@Component({
  selector: 'exai-card-builder-items',
  templateUrl: './card-builder-items.component.html',
  styleUrls: ['./card-builder-items.component.scss']
})
export class CardBuilderItemsComponent implements OnInit, AfterViewInit {
  elements = elements;
  @Output() actionClickedEmit = new EventEmitter();

  loading$: Observable<boolean>;

  constructor(public cardBuilderElementsService: CardBuilderElementsService) {
    this.loading$ = this.cardBuilderElementsService.savingForm$; 
  }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
  }

  actionClicked(item) {
    this.actionClickedEmit.emit(item);
  }
}
