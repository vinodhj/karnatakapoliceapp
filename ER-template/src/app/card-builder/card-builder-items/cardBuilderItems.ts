export const elements = [
  {
    elementType: "form-multiple",
    multiple: true,
    icon: "add",
    label: "Form Elements",
    elements: [
      {
        "type": "text",
        "elementType": "form",
        "icon": "crop_landscape",
        "label": "Input",
        "value": "",
        placeholder: "Enter text",
        validators: {},
        mask: null,
        styles: {
          borderRadius: 1.5,
          backgroundColor: '#fff',
          borderColor: '#000',
          fontSize: 16,
          textColor: '#000',
          paddingSize: 5,
          width: '100%',
          height: 'auto',
          position: { x: 0, y: 0, z: 0 }
        }
      },
      // {
      //   "type": "date",
      //   "elementType": "form",
      //   "icon": "calendar_today",
      //   "label": "Date",
      //   "value": "",
      //   placeholder: "Enter date",
      //   hideCondition: {},
      //   disabledCondition: {},
      //   validators: {}
      // },
      // {
      //   "type": "select",
      //   "elementType": "form",
      //   "icon": "fact_check",
      //   "label": "Select",
      //   "value": "",
      //   hideCondition: {},
      //   disabledCondition: {},
      //   validators: {}
      // },

    ]
  },
  {
    "type": "image",
    "elementType": "card-image",
    "icon": "image",
    "label": "Image",
    imageData: {},
    styles: {
      width: 100,
      height: 'auto',
      position: { x: 0, y: 0, z: 0 }
    }
  },
  { 
    elementType: "divide" 
  },
  {
    type: "html",
    elementType: "editor",
    icon: "title",
    html: "",
    label: "Text",
    styles: {
      width: '100%',
      height: 'auto',
      position: { x: 0, y: 0, z: 0 }
    }
  },
  // {
  //   type: "div",
  //   elementType: "card-div",
  //   icon: "check_box_outline_blank"
  // },
  // {
  //   type: "div",
  //   elementType: "card-div",
  //   icon: "circle"
  // },
  {
    elementType: "divide"
  },
  {
    elementType: "card-configuration",
    icon: "credit_card",
    actionBtn: 'cardSizeConfiguration',
    label: "Card Size Settings"
  },
  {
    elementType: "divide"
  },
  {
    elementType: "save",
    icon: "save",
    actionBtn: 'save',
    label: "Save"
  },
];
