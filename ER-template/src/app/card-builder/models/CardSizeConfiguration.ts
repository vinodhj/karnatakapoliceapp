export interface CardSizeConfiguration {
    width: number,
    height: number,
    borderRadius: number
}