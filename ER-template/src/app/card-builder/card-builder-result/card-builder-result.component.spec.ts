import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardBuilderResultComponent } from './card-builder-result.component';

describe('CardBuilderResultComponent', () => {
  let component: CardBuilderResultComponent;
  let fixture: ComponentFixture<CardBuilderResultComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardBuilderResultComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CardBuilderResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
