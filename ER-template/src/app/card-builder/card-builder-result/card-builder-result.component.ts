import { trigger, state, style, transition, animate } from '@angular/animations';
import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { staggerAnimation } from 'src/@exai/animations/stagger.animation';
import { JsonFormControls } from 'src/app/form-builder/models/JsonForm';
import { cardAnimation } from '../animations/cardAnimation';
import { CardSizeConfiguration } from '../models/CardSizeConfiguration';
import { CardConfiguration, CardPart } from '../services/card-builder-elements.service';
import { distinctUntilChanged, finalize } from 'rxjs/operators';
import { ControlNamePipe } from '../pipes/control-name.pipe';
import { FormBuilderReactiveService } from 'src/app/form-builder/services/form-builder-reactive.service';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpService } from 'src/@exai/services/http.service';
import { FormBuilderFillDataService } from 'src/app/form-builder/services/form-builder-fill-data.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'exai-card-builder-result',
  templateUrl: './card-builder-result.component.html',
  styleUrls: ['./card-builder-result.component.scss'],
  animations: [cardAnimation]
})
export class CardBuilderResultComponent implements OnInit {
  data = {
    state: "flipped"
  };

  isEdit = this.router.url.includes('edit');
  isFill = this.router.url.includes('fill-form');
  isPreview = this.router.url.includes('view-form');
  isCreate = this.router.url.includes('create');

  @Input() cardConfiguration: CardConfiguration;

  public generatedForm: FormGroup = this.fb.group({});

  cardSizeConfiguration: CardSizeConfiguration;
  cardParts: CardPart[]; 
  submittingForm: boolean;
  updateAllowed: any;
  loadingUserData: boolean;

  constructor(private snackbar: MatSnackBar, private fb: FormBuilder, private formBuilderReactive: FormBuilderReactiveService, private router: Router, private httpService: HttpService, private route: ActivatedRoute, private formBuilderFillData: FormBuilderFillDataService) { }

  ngOnInit(): void {

    this.cardSizeConfiguration = this.cardConfiguration.cardSizeConfiguration;
    this.cardParts = this.cardConfiguration.cardParts;

    const allFormControls = [];

    this.cardParts.forEach(part => {

      part.elements.forEach(element => {

        if(element.elementType === 'form')
          allFormControls.push(element);

      })

    })

    this.createForm(allFormControls);

    this.isEditAllowed();

    this.generatedForm.valueChanges.pipe(
      distinctUntilChanged()
    ).subscribe((response) => {

      //we need disabled fields too
      const rawFormValues = this.generatedForm.getRawValue();

      console.log(rawFormValues);
    })

    if (this.isPreview) {
      this.fillFormData();
    }

    this.generatedForm.updateValueAndValidity();
  }


  cardClicked() {
    if (this.data.state === "default") {
      this.data.state = "flipped";
    } else {
      this.data.state = "default";
    }
  }

  createForm(controls: JsonFormControls[]) {
    for (const control of controls) {
      const validatorsToAdd = this.formBuilderReactive.returnValidators(control);

      const controlPipe = new ControlNamePipe();

      this.generatedForm.addControl(
        controlPipe.transform(control),
        this.fb.control(control.value, validatorsToAdd)
      );
    }
  }

  isEditAllowed() {

    if (this.isPreview)
      this.httpService.canUserEditFormRecord(this.route.snapshot.paramMap.get('userFormId')).subscribe((res: boolean) => {
        this.isPreview = !res;
        this.updateAllowed = res;
      });

  }

  fillFormData() {

    this.loadingUserData = true;

    this.httpService.getViewForFormBulder(this.route.snapshot.paramMap.get('userFormId')).pipe(
      finalize(() => this.loadingUserData = false)
    ).subscribe(res => {
      this.generatedForm.patchValue(res, {
        emitEvent: false,
        onlySelf: true
      });
    })
  }

  submitForm() {
    // if(this.isPreview) return;

    const data: any = this.formBuilderFillData.getFormData();

    if (data.formId && !this.updateAllowed) {
      this.submittingForm = true;
      this.httpService.formAddNew(data.formId, null, JSON.stringify(this.generatedForm.value), data.candidateName, data.candidateNumber, data.candidateEmail, data.formName)
        .pipe(
          finalize(() => this.submittingForm = false)
        ).subscribe(
          res => {
            this.router.navigateByUrl('/forms/submitted-form');
          },
          err => {

            this.snackbar.open(err?.error?.Message, "Ok", {
              duration: 3000
            });

          });
    }


    if(this.updateAllowed) {

      this.submittingForm = true;

      this.httpService.templateFormRecordUpdate(this.route.snapshot.paramMap.get('userFormId'), JSON.stringify(this.generatedForm.value)).pipe(
        finalize(() => this.submittingForm = false)
      ).subscribe(res => {
 
        this.router.navigateByUrl('/forms/submitted-form'); 
 
      }, err => { 
        this.snackbar.open(err?.error?.Message, "Ok", { 
          duration: 3000 
        });
      })

    }

  }
}
