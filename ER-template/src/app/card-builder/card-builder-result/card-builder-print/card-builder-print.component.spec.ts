import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardBuilderPrintComponent } from './card-builder-print.component';

describe('CardBuilderPrintComponent', () => {
  let component: CardBuilderPrintComponent;
  let fixture: ComponentFixture<CardBuilderPrintComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardBuilderPrintComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CardBuilderPrintComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
