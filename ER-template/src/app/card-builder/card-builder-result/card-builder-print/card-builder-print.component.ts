import { AfterViewInit, ChangeDetectorRef, Component, Input, NgZone, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router, ActivatedRoute } from '@angular/router';
import { distinctUntilChanged, finalize } from 'rxjs/operators';
import { HttpService } from 'src/@exai/services/http.service';
import { JsonFormControls } from 'src/app/form-builder/models/JsonForm';
import { FormBuilderFillDataService } from 'src/app/form-builder/services/form-builder-fill-data.service';
import { FormBuilderReactiveService } from 'src/app/form-builder/services/form-builder-reactive.service';
import { CardSizeConfiguration } from '../../models/CardSizeConfiguration';
import { ControlNamePipe } from '../../pipes/control-name.pipe';
import { CardConfiguration, CardPart } from '../../services/card-builder-elements.service';
import * as uuid from "uuid";

@Component({
  selector: 'exai-card-builder-print',
  templateUrl: './card-builder-print.component.html',
  styleUrls: ['./card-builder-print.component.scss']
})
export class CardBuilderPrintComponent implements OnInit, AfterViewInit {
  @ViewChild('buttonPrint') buttonPrint;
  @Input() cardConfiguration: CardConfiguration;
  @Input() formRecordId: number;
  @Input() userInput;

  public generatedForm: FormGroup = this.fb.group({});

  cardSizeConfiguration: CardSizeConfiguration;
  cardParts: CardPart[];
  submittingForm: boolean;
  updateAllowed: any;
  loadingUserData: boolean;

  printRandomId = uuid.v4();

  constructor(private ngZone: NgZone, private snackbar: MatSnackBar, private fb: FormBuilder, private formBuilderReactive: FormBuilderReactiveService, private router: Router, private httpService: HttpService, private route: ActivatedRoute, private formBuilderFillData: FormBuilderFillDataService, private cdr: ChangeDetectorRef) { }

  ngOnInit(): void {

    this.cardSizeConfiguration = this.cardConfiguration.cardSizeConfiguration;
    this.cardParts = this.cardConfiguration.cardParts;

    const allFormControls = [];

    this.cardParts.forEach(part => {

      part.elements.forEach(element => {

        if (element.elementType === 'form')
          allFormControls.push(element);

      })

    })

    this.createForm(allFormControls);

    this.generatedForm.valueChanges.pipe(
      distinctUntilChanged()
    ).subscribe((response) => {

      //we need disabled fields too
      const rawFormValues = this.generatedForm.getRawValue();

      console.log(rawFormValues);
    })

    this.fillFormData();

    this.generatedForm.updateValueAndValidity();
  }


  createForm(controls: JsonFormControls[]) {
    for (const control of controls) {
      const validatorsToAdd = this.formBuilderReactive.returnValidators(control);

      const controlPipe = new ControlNamePipe();

      this.generatedForm.addControl(
        controlPipe.transform(control),
        this.fb.control(control.value, validatorsToAdd)
      );
    }
  }


  fillFormData() {

    console.log(this.userInput);

    if (this.userInput) {

      this.generatedForm.patchValue(this.userInput, {
        emitEvent: false,
        onlySelf: true
      });
      //needed for activating print
      setTimeout(() => this.buttonPrint._elementRef.nativeElement.click());

    } else if (this.formRecordId) {

      this.httpService.getViewForFormBulder(this.formRecordId).pipe(
        finalize(() => this.loadingUserData = false)
      ).subscribe(res => {

        this.generatedForm.patchValue(res, {
          emitEvent: false,
          onlySelf: true
        });

        setTimeout(() => this.buttonPrint._elementRef.nativeElement.click());
      })
    } else {

      setTimeout(() => this.buttonPrint._elementRef.nativeElement.click());
    }

  }

  ngAfterViewInit(): void {
  }
}
