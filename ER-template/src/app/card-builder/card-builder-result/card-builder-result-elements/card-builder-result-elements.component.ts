import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'exai-card-builder-result-elements',
  templateUrl: './card-builder-result-elements.component.html',
  styleUrls: ['./card-builder-result-elements.component.scss']
})
export class CardBuilderResultElementsComponent implements OnInit {

  @Input() item;

  get positionStyles() {
    return `translate3d(${this.item.styles.position.x}px, ${this.item.styles.position.y}px, ${this.item.styles.position.z}px)`
  }

  constructor() { }

  ngOnInit(): void {
    console.log(this.item);
  }

}
