import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardBuilderResultElementsComponent } from './card-builder-result-elements.component';

describe('CardBuilderResultElementsComponent', () => {
  let component: CardBuilderResultElementsComponent;
  let fixture: ComponentFixture<CardBuilderResultElementsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardBuilderResultElementsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CardBuilderResultElementsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
