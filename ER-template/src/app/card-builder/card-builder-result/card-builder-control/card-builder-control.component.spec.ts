import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardBuilderControlComponent } from './card-builder-control.component';

describe('CardBuilderControlComponent', () => {
  let component: CardBuilderControlComponent;
  let fixture: ComponentFixture<CardBuilderControlComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardBuilderControlComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CardBuilderControlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
