import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardBuilderInputComponent } from './card-builder-input.component';

describe('CardBuilderInputComponent', () => {
  let component: CardBuilderInputComponent;
  let fixture: ComponentFixture<CardBuilderInputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardBuilderInputComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CardBuilderInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
