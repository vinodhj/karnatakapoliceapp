import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CardBuilderElementsService } from '../../services/card-builder-elements.service';

@Component({
  selector: 'exai-card-builder-input',
  templateUrl: './card-builder-input.component.html',
  styleUrls: ['./card-builder-input.component.scss']
})
export class CardBuilderInputComponent implements OnInit {
  @Input() item;
  @Input() container;
  @Input() allowInputTyping;

  @Output('duplicateElementEmitted') duplicateElementEmitted = new EventEmitter();
  @Output('removeEmitted') removeEmitted = new EventEmitter();

  get positionStyles() {
    return `translate3d(${this.item.styles.position.x}px, ${this.item.styles.position.y}px, ${this.item.styles.position.z}px)`
  }

  elementFocus: boolean = false;

  constructor(private cardBuilderElements: CardBuilderElementsService) { }

  ngOnInit(): void {
  }

  resizeEndEvent(event) {
    this.cardBuilderElements.elementResizeEnd(this.item, event);
  }

  draggingEvent(event) {
    this.cardBuilderElements.dragginElement(this.item, event);
  }
}
