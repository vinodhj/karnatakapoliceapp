import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardBuilderInputSettingsComponent } from './card-builder-input-settings.component';

describe('CardBuilderInputSettingsComponent', () => {
  let component: CardBuilderInputSettingsComponent;
  let fixture: ComponentFixture<CardBuilderInputSettingsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardBuilderInputSettingsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CardBuilderInputSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
