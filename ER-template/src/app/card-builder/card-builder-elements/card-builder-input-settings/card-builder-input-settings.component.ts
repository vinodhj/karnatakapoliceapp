import { Component, Input, OnInit } from '@angular/core';
import { popoverAnimation } from 'src/@exai/animations/popover.animation';

@Component({
  selector: 'exai-card-builder-input-settings',
  templateUrl: './card-builder-input-settings.component.html',
  styleUrls: ['./card-builder-input-settings.component.scss'],
  animations: [popoverAnimation]
})
export class CardBuilderInputSettingsComponent implements OnInit {
  @Input() item;

  toggleBackgroundPicker: boolean = false;
  toggleBorderPicker: boolean = false;
  toggleTextPicker: boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

  addMask(value) {
    this.item.mask = value;
  }
}
