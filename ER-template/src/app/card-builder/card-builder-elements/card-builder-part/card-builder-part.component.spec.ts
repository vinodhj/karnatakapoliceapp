import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardBuilderPartComponent } from './card-builder-part.component';

describe('CardBuilderPartComponent', () => {
  let component: CardBuilderPartComponent;
  let fixture: ComponentFixture<CardBuilderPartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardBuilderPartComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CardBuilderPartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
