import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { CardBuilderElementsService } from '../../services/card-builder-elements.service';
import { CardBuilderImageSettingsComponent } from '../card-builder-image-settings/card-builder-image-settings.component';

@Component({
  selector: 'exai-card-builder-part',
  templateUrl: './card-builder-part.component.html',
  styleUrls: ['./card-builder-part.component.scss']
})
export class CardBuilderPartComponent implements OnInit {
  @Input() part;
  @Input() cardSizeConfiguration;
  @Input() focusCardSide;

  allowInputTyping: boolean = false;

  constructor(private cardBuilderElements: CardBuilderElementsService, private dialog: MatDialog) { }

  ngOnInit(): void {
  }

  removeElement(element) {
    this.cardBuilderElements.removeElement(this.part, element);
  }

  openImageDialog() {

    const dialogRef = this.dialog.open(CardBuilderImageSettingsComponent, {
      data: { ...this.part, backgroundImageSetting: true },
      disableClose: true,
      maxHeight: '75vh'
    });

    dialogRef.afterClosed().subscribe(uploadedImage => {

      if(!uploadedImage) return;

      if(uploadedImage.applyOnBothSides)
        this.cardBuilderElements.applyImageOnBothSides(this.part, uploadedImage.imageContent, true);

      this.part.imageData.imageContent = uploadedImage.imageContent;

    })
  }

  duplicateElement(element) {

    this.cardBuilderElements.duplicateElement(this.part, element);

  }
}
