import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardBuilderHtmlSettingsComponent } from './card-builder-html-settings.component';

describe('CardBuilderHtmlSettingsComponent', () => {
  let component: CardBuilderHtmlSettingsComponent;
  let fixture: ComponentFixture<CardBuilderHtmlSettingsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardBuilderHtmlSettingsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CardBuilderHtmlSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
