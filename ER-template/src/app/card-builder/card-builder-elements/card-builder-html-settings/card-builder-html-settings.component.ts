import { AfterViewInit, ChangeDetectorRef, Component, Input, OnInit, ViewChild } from '@angular/core';
import { RichTextEditorComponent, TableService } from '@syncfusion/ej2-angular-richtexteditor';
import { popoverAnimation } from 'src/@exai/animations/popover.animation';

@Component({
  selector: 'exai-card-builder-html-settings',
  templateUrl: './card-builder-html-settings.component.html',
  styleUrls: ['./card-builder-html-settings.component.scss'],
  animations: [popoverAnimation],
  providers: [
    TableService,
  ]
})
export class CardBuilderHtmlSettingsComponent implements OnInit, AfterViewInit {
  @Input() item!: any;

  @ViewChild('textEditorRef')
  public rteObj: RichTextEditorComponent;

  public tools: any = {
    items: [
        'Bold', 'Italic', 'Underline', 'StrikeThrough', '|',
        'FontName', 'FontSize', 'FontColor', 'BackgroundColor', '|',
        'Alignments', '|', 'CreateLink',
        'Image', 'CreateTable'],
    };

  imageSettings = { 
    saveFormat: "Base64" 
  } 

  ngAfterViewInit(): void {
    
  }

  constructor(private cdr: ChangeDetectorRef) { }

  ngOnInit(): void {
    // this.rteObj.refresh();
  }
  
  refreshUi() {
    //fix for not refreshing ui
    setTimeout(() => this.rteObj.refreshUI());
  }

  ngOnDestroy(): void {
    this.rteObj.destroy();
  }
}
