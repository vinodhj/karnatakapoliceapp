import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardBuilderPartSettingsComponent } from './card-builder-part-settings.component';

describe('CardBuilderPartSettingsComponent', () => {
  let component: CardBuilderPartSettingsComponent;
  let fixture: ComponentFixture<CardBuilderPartSettingsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardBuilderPartSettingsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CardBuilderPartSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
