import { Component, Input, OnInit } from '@angular/core';
import { popoverAnimation } from 'src/@exai/animations/popover.animation';

@Component({
  selector: 'exai-card-builder-part-settings',
  templateUrl: './card-builder-part-settings.component.html',
  styleUrls: ['./card-builder-part-settings.component.scss'],
  animations: [popoverAnimation]
})
export class CardBuilderPartSettingsComponent implements OnInit {
  @Input() item!: any;

  toggleBorderPicker: boolean = false;

  imagePosition = [
    'left top',
    'left center',
    'left bottom',
    'right top',
    'right center',
    'right bottom',
    'center top',
    'center center',
    'center bottom',
  ]

  imageSize = [
    'auto','length','cover','contain','initial'
  ]

  
  imageRepeat = [
    'repeat', 'repeat-x', 'repeat-y', 'no-repeat', 'initial'
  ]

  constructor() { }

  ngOnInit(): void {
  }

}
