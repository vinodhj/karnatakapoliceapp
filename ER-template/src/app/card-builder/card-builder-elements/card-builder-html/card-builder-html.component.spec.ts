import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardBuilderHtmlComponent } from './card-builder-html.component';

describe('CardBuilderHtmlComponent', () => {
  let component: CardBuilderHtmlComponent;
  let fixture: ComponentFixture<CardBuilderHtmlComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardBuilderHtmlComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CardBuilderHtmlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
