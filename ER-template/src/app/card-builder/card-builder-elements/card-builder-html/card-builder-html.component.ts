import { ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output, ViewChild, ViewEncapsulation } from '@angular/core';
import { RichTextEditorComponent } from '@syncfusion/ej2-angular-richtexteditor';
import { CardBuilderElementsService } from '../../services/card-builder-elements.service';

@Component({
  selector: 'exai-card-builder-html',
  templateUrl: './card-builder-html.component.html',
  styleUrls: ['./card-builder-html.component.scss'],
})
export class CardBuilderHtmlComponent implements OnInit {
  @Input() item!: any;
  @Input() container!: any;
  @Output('removeEmitted') removeEmitted = new EventEmitter();
  @Output('duplicateElementEmitted') duplicateElementEmitted = new EventEmitter();

  elementFocus: boolean = false;

  constructor(private cardBuilderElements: CardBuilderElementsService) { }

  get positionStyles() {
    return `translate3d(${this.item.styles.position.x}px, ${this.item.styles.position.y}px, ${this.item.styles.position.z}px)`
  }


  ngOnInit(): void {
  }
  
  ngOnDestroy(): void {
  }

  resizeEndEvent(event) {
    this.cardBuilderElements.elementResizeEnd(this.item, event);
  }

  draggingEvent(event) {
    this.cardBuilderElements.dragginElement(this.item, event);
  }
}
