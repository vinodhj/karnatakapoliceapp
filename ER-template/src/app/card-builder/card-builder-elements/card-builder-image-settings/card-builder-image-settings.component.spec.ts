import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardBuilderImageSettingsComponent } from './card-builder-image-settings.component';

describe('CardBuilderImageSettingsComponent', () => {
  let component: CardBuilderImageSettingsComponent;
  let fixture: ComponentFixture<CardBuilderImageSettingsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardBuilderImageSettingsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CardBuilderImageSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
