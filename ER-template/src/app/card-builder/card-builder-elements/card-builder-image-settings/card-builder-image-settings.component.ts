import { HttpClient } from '@angular/common/http';
import { ChangeDetectorRef, Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import * as uuid from "uuid";

@Component({
  selector: 'exai-card-builder-image-settings',
  templateUrl: './card-builder-image-settings.component.html',
  styleUrls: ['./card-builder-image-settings.component.scss']
})
export class CardBuilderImageSettingsComponent implements OnInit {

  applyOnBothSides: boolean = false;

  keepAspectRatio: boolean = true;

  backgroundImageSetting: boolean = false;

  uploadedFile;

  constructor(private http: HttpClient, @Inject(MAT_DIALOG_DATA) public data: any, private cdr: ChangeDetectorRef, private snackbar: MatSnackBar) { }

  ngOnInit(): void {

    this.initializeImage();

  }
  
  initializeImage() {

    if(this.data.backgroundImageSetting)
      this.backgroundImageSetting = true;

    if(this.data?.imageData?.imageContent) {

      const { imageContent, width, height, originalHeight, originalWidth } = this.data.imageData;

      this.uploadedFile = { imageContent, width, height, originalHeight, originalWidth, applyOnBothSides: false };
    }
  }

  filesDropped(files) {

    const file = files[0];

    if(!file.type.includes('image/')) {

      this.fileIsNotImage(file);

      return;
    }

    file.attachmentId = uuid.v4();

    const reader = new FileReader();

    reader.onload = () => {
      
      var image: any = new Image();

      image.src = reader.result;

      image.onload = () => {

        this.uploadedFile = { imageContent: reader.result, width: image.width, height: image.height, originalWidth: image.width, originalHeight: image.height, applyOnBothSides: this.applyOnBothSides };

      }
    }

    reader.readAsDataURL(file);

  }

  fileIsNotImage(file) {
    this.snackbar.open("Only image file allowed! Please select image.", 'Ok', {
      duration: 3000
    })
  }

  uploadFilesFromInput(event) {
    const files = event.target.files;
    this.filesDropped(files);

    //reset value so it can be run multiple times
    event.target.value = '';
  }

  getMeta(url, callback) {
    const img = new Image();
    img.src = url;
    img.onload = () => { 
      callback(img.width, img.height); 
      
    }
}

  removeImage() {
    this.uploadedFile = { imageContent: null };
  }

  applyOnBothImages(event) {

    this.uploadedFile = { ...this.uploadedFile, applyOnBothSides: event.checked };

  }

}
