import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardBuilderTextComponent } from './card-builder-text.component';

describe('CardBuilderTextComponent', () => {
  let component: CardBuilderTextComponent;
  let fixture: ComponentFixture<CardBuilderTextComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardBuilderTextComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CardBuilderTextComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
