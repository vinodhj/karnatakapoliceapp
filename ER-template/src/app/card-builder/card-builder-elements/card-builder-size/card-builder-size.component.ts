import { Component, Input, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { CardSizeConfiguration } from '../../models/CardSizeConfiguration';
import { CardBuilderElementsService, CardSizesPredefined } from '../../services/card-builder-elements.service';

@Component({
  selector: 'exai-card-builder-size',
  templateUrl: './card-builder-size.component.html',
  styleUrls: ['./card-builder-size.component.scss']
})
export class CardBuilderSizeComponent implements OnInit {
  @Input() cardSizeConfiguration: CardSizeConfiguration;

  cardSizesPredefined: CardSizesPredefined[];

  standardSizes = new FormControl();

  constructor(public cardBuilderElements: CardBuilderElementsService) { }

  ngOnInit(): void {

    this.cardSizesPredefined = this.cardBuilderElements.predefinedSizesConfiguration;

    this.checkIsCustomSize(this.cardSizeConfiguration, this.cardSizesPredefined);

    //standard sizes dropdown set value on object
    this.standardSizes.valueChanges.subscribe(value => {

      if(!value) return;

      this.cardSizeConfiguration.width = value.width;
      this.cardSizeConfiguration.height = value.height;
      this.cardSizeConfiguration.borderRadius = value.borderRadius;

    });
  }

  checkIsCustomSize(currentSizeConfiguration, cardSizesPredefined) {

    //check if current values and some value from predefined values are match
    //if it's match then uncheck custom size and set value on dropdown
    let foundMatch: boolean;

    cardSizesPredefined.map(size => {

      if (currentSizeConfiguration.width === size.sizes.width &&
        currentSizeConfiguration.height === size.sizes.height &&
        currentSizeConfiguration.borderRadius === size.sizes.borderRadius) {

          this.standardSizes.patchValue(size.sizes);

          foundMatch = true;
        }

    })

    foundMatch ? this.allowCustomSize = false : this.allowCustomSize = true;

  }

  allowCustomSize: boolean = false;

  customSizeChange(event) {
    this.allowCustomSize = event.checked;
    
    //remove value from dropdown
    this.standardSizes.patchValue(null, { emitEvent: false, onlySelf: true });
  }
}
