import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardBuilderSizeComponent } from './card-builder-size.component';

describe('CardBuilderSizeComponent', () => {
  let component: CardBuilderSizeComponent;
  let fixture: ComponentFixture<CardBuilderSizeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardBuilderSizeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CardBuilderSizeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
