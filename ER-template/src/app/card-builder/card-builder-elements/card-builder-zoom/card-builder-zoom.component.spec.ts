import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardBuilderZoomComponent } from './card-builder-zoom.component';

describe('CardBuilderZoomComponent', () => {
  let component: CardBuilderZoomComponent;
  let fixture: ComponentFixture<CardBuilderZoomComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardBuilderZoomComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CardBuilderZoomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
