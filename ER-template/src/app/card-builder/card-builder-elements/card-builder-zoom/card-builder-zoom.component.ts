import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatSlider, MatSliderChange } from '@angular/material/slider';
import { map } from 'rxjs/operators';
import { ConfigService } from 'src/@exai/services/config.service';
import { CardBuilderElementsService } from '../../services/card-builder-elements.service';

@Component({
  selector: 'exai-card-builder-zoom',
  templateUrl: './card-builder-zoom.component.html',
  styleUrls: ['./card-builder-zoom.component.scss']
})
export class CardBuilderZoomComponent implements OnInit {
  zoomValue = 0;
  @ViewChild('slider') slider: MatSlider

  constructor(private cardBuilderElements: CardBuilderElementsService, private configService: ConfigService) { }

  isFooterFixed$ = this.configService.config$.pipe(map(config => config.footer.fixed));

  ngOnInit(): void {
    this.zoomValue = this.cardBuilderElements.zoom.value;
  }

  changeZoomValue(event) {
    this.cardBuilderElements.zoom.next(event.value);
  }

  zoom(direction) {
    
    //manualy zoom in and zoom out

    if(direction === 'in') {
      this.zoomValue += 10;

      if(this.zoomValue > 100)
        this.zoomValue = 100;
    }


    if(direction === 'out') {

      this.zoomValue -= 10;

      if(this.zoomValue < 0)
        this.zoomValue = 0;
    }
    
    this.cardBuilderElements.zoom.next(this.zoomValue);
    }
}
