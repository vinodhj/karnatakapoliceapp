import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { CardBuilderElementsService } from '../../services/card-builder-elements.service';
import { CardBuilderImageSettingsComponent } from '../card-builder-image-settings/card-builder-image-settings.component';

@Component({
  selector: 'exai-card-builder-image',
  templateUrl: './card-builder-image.component.html',
  styleUrls: ['./card-builder-image.component.scss']
})
export class CardBuilderImageComponent implements OnInit {
  @Input() item;
  @Input() container;
  @Output('duplicateElementEmitted') duplicateElementEmitted = new EventEmitter();
  
  @ViewChild('elementToDrag') elementToDrag: ElementRef

  @Output('removeEmitted') removeEmitted = new EventEmitter();

  get positionStyles() {
    return `translate3d(${this.item.styles.position.x}px, ${this.item.styles.position.y}px, ${this.item.styles.position.z}px)`
  }

  elementFocus: boolean = false;

  constructor(private dialog: MatDialog, private cardBuilderElements: CardBuilderElementsService) { }

  ngOnInit(): void {
  }

  openDialog() {

    const dialogRef = this.dialog.open(CardBuilderImageSettingsComponent, {
      data: this.item,
      disableClose: true,
      maxHeight: '75vh'
    });

    dialogRef.afterClosed().subscribe(uploadedImage => {

      if(!uploadedImage) return;

      if(uploadedImage.applyOnBothSides)
        this.cardBuilderElements.applyImageOnBothSides(this.item, uploadedImage.imageContent);
      
        this.item.imageData = {
        imageContent: uploadedImage.imageContent,
        width: uploadedImage.width,
        height: uploadedImage.height,
        originalHeight: uploadedImage.originalHeight,
        originalWidth: uploadedImage.originalWidth
      }
    })
  }

  resizeEndEvent(event) {
    this.cardBuilderElements.elementResizeEnd(this.item, event);
  }

  draggingEvent(event) {
    this.cardBuilderElements.dragginElement(this.item, event);
  }

}
