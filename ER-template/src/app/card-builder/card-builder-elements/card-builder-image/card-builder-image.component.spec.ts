import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardBuilderImageComponent } from './card-builder-image.component';

describe('CardBuilderImageComponent', () => {
  let component: CardBuilderImageComponent;
  let fixture: ComponentFixture<CardBuilderImageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardBuilderImageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CardBuilderImageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
