import { trigger, state, style, transition, animate } from "@angular/animations";

export const cardAnimation =  trigger("cardFlip", [
    state(
      "default",
      style({
        transform: "none"
      })
    ),
    state(
      "flipped",
      style({
        transform: "rotateY(180deg)"
      })
    ),
    state(
      "matched",
      style({
        visibility: "false",
        transform: "scale(0.03)",
        opacity: 0
      })
    ),
    transition("default => flipped", [animate("450ms ease-out")]),
    transition("flipped => default", [animate("450ms ease-out")]),
    transition("* => matched", [animate("450ms ease-in")])
  ])