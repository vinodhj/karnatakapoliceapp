import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CardBuilderRoutingModule } from './card-builder-routing.module';
import { CardBuilderComponent } from './card-builder.component';
import { CardBuilderContentComponent } from './card-builder-content/card-builder-content.component';
import { CardBuilderResultComponent } from './card-builder-result/card-builder-result.component';
import { GuidesDirective } from './directives/guides.directive';
import { MoveableDirective } from './directives/moveable.directive';
import { NgxMoveableModule, NgxMoveableComponent } from 'ngx-moveable';
import { CardBuilderInputComponent } from './card-builder-elements/card-builder-input/card-builder-input.component';
import { CardBuilderImageComponent } from './card-builder-elements/card-builder-image/card-builder-image.component';
import { CardBuilderTextComponent } from './card-builder-elements/card-builder-text/card-builder-text.component';
import { CardBuilderItemsComponent } from './card-builder-items/card-builder-items.component';
import { MatIconModule } from '@angular/material/icon';
import { CardBuilderSizeComponent } from './card-builder-elements/card-builder-size/card-builder-size.component';

import {MatInputModule} from '@angular/material/input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CardBuilderZoomComponent } from './card-builder-elements/card-builder-zoom/card-builder-zoom.component';
import {MatSliderModule} from '@angular/material/slider';
import { CardBuilderPartSettingsComponent } from './card-builder-elements/card-builder-part-settings/card-builder-part-settings.component';
import { MatTabsModule } from '@angular/material/tabs';
import { ExaiPopoverDirective } from '../form-builder/directives/popover.directive';
import { FormBuilderSharedModule } from '../shared/form-status-history/form-builder-shared.module';
import { CardBuilderPartComponent } from './card-builder-elements/card-builder-part/card-builder-part.component';
import { MatMenuModule } from '@angular/material/menu';
import { MatButtonModule } from '@angular/material/button';
import { CardBuilderInputSettingsComponent } from './card-builder-elements/card-builder-input-settings/card-builder-input-settings.component';
import { ColorPickerModule } from 'ngx-color-picker';
import { IMaskModule } from 'angular-imask';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { CardBuilderImageSettingsComponent } from './card-builder-elements/card-builder-image-settings/card-builder-image-settings.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { CardBuilderHtmlComponent } from './card-builder-elements/card-builder-html/card-builder-html.component';
import { RichTextEditorAllModule } from '@syncfusion/ej2-angular-richtexteditor';
import { CardBuilderHtmlSettingsComponent } from './card-builder-elements/card-builder-html-settings/card-builder-html-settings.component';
import { MatSelectModule } from '@angular/material/select';
import { CardBuilderControlComponent } from './card-builder-result/card-builder-control/card-builder-control.component';
import { CardBuilderResultElementsComponent } from './card-builder-result/card-builder-result-elements/card-builder-result-elements.component';
import { ControlNamePipe } from './pipes/control-name.pipe';
import { NgxMaskModule } from 'ngx-mask';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { FormBuilderModule } from '../form-builder/form-builder.module';
import { TooltipModule } from 'ng2-tooltip-directive';
import { NgxPrintModule } from 'ngx-print';
import {PortalModule} from '@angular/cdk/portal';
import { CardBuilderPrintComponent } from './card-builder-result/card-builder-print/card-builder-print.component';

@NgModule({
  declarations: [
    CardBuilderComponent,
    CardBuilderContentComponent,
    CardBuilderResultComponent,
    GuidesDirective,
    MoveableDirective,
    CardBuilderInputComponent,
    CardBuilderImageComponent,
    CardBuilderTextComponent,
    CardBuilderItemsComponent,
    CardBuilderSizeComponent,
    CardBuilderZoomComponent,
    CardBuilderPartSettingsComponent,
    CardBuilderPartComponent,
    CardBuilderInputSettingsComponent,
    CardBuilderImageSettingsComponent,
    CardBuilderHtmlComponent,
    CardBuilderHtmlSettingsComponent,
    CardBuilderControlComponent,
    CardBuilderResultElementsComponent,
    ControlNamePipe,
    CardBuilderPrintComponent,
  ],
  imports: [
    CommonModule,
    CardBuilderRoutingModule,
    NgxMoveableModule,
    MatIconModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
    MatSliderModule,
    MatTabsModule,
    FormBuilderSharedModule,
    MatMenuModule,
    MatButtonModule,
    ColorPickerModule,
    NgxMaskModule,
    MatCheckboxModule,
    MatDialogModule,
    MatSnackBarModule,
    RichTextEditorAllModule,
    MatSelectModule,
    MatProgressSpinnerModule,
    MatProgressBarModule,
    MatTooltipModule,
    FormBuilderModule,
    TooltipModule,
    NgxPrintModule,
    PortalModule
  ]
})
export class CardBuilderModule { }
