import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CardBuilderComponent } from './card-builder.component';

const routes: Routes = [
  { path: 'edit/:id', component: CardBuilderComponent },
  { path: 'create/:id', component: CardBuilderComponent },
  { path: 'fill-form/:id', component: CardBuilderComponent },
  { path: 'view-form/:id/:userFormId', component: CardBuilderComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CardBuilderRoutingModule { }
