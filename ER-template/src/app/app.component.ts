import { coerceBooleanProperty } from '@angular/cdk/coercion';
import { Platform } from '@angular/cdk/platform';
import { DOCUMENT } from '@angular/common';
import { AfterViewInit, Component, Inject, LOCALE_ID, OnInit, Renderer2 } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Settings } from 'luxon';
import { filter, map } from 'rxjs/operators';
import { HttpService } from 'src/@exai/services/http.service';
import { NavigationService } from 'src/@exai/services/navigation.service';
import { SplashScreenService } from 'src/@exai/services/splash-screen.service';
import { ConfigName } from '../@exai/interfaces/config-name.model';
import { ConfigService } from '../@exai/services/config.service';
import { Style, StyleService } from '../@exai/services/style.service';
import { SignalrService } from './police-app/notifications/services/signalr.service';
import { BroadcastService } from './shared/services/broadcast.service';
import { CacheDataService } from './shared/services/cache-data.service';

@Component({
  selector: 'exai-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, AfterViewInit {
  title = 'exai';


  constructor(private configService: ConfigService,
    private styleService: StyleService,
    private renderer: Renderer2,
    private platform: Platform,
    @Inject(DOCUMENT) private document: Document,
    @Inject(LOCALE_ID) private localeId: string,
    private route: ActivatedRoute,
    private navigationService: NavigationService,
    private httpService: HttpService,
    private services: HttpService,
    private splashScreenService: SplashScreenService,
    private cacheDataService: CacheDataService) {

    this.navigationService.setNavigationItems(this.httpService.getRole());

    Settings.defaultLocale = this.localeId;

    if (this.platform.BLINK) {
      this.renderer.addClass(this.document.body, 'is-blink');
    }

    this.route.queryParamMap.pipe(
      map(queryParamMap => queryParamMap.has('rtl') && coerceBooleanProperty(queryParamMap.get('rtl'))),
    ).subscribe(isRtl => {
      this.document.body.dir = isRtl ? 'rtl' : 'ltr';
      this.configService.updateConfig({
        rtl: isRtl
      });
    });

    this.route.queryParamMap.pipe(
      filter(queryParamMap => queryParamMap.has('layout'))
    ).subscribe(queryParamMap => this.configService.setConfig(queryParamMap.get('layout') as ConfigName));

    this.route.queryParamMap.pipe(
      filter(queryParamMap => queryParamMap.has('style'))
    ).subscribe(queryParamMap => this.styleService.setStyle(queryParamMap.get('style') as Style));


  }
  ngOnInit(): void {
    this.services.setTokenIfAvailable();
    this.navigationService.setRole(this.httpService.roleId)

  }
  ngAfterViewInit(): void {
    // throw new Error('Method not implemented.');
  }


}
