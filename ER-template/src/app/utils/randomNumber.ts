export function getUniqueId(parts: number): string {
  const stringArr = [];
  for (let i = 0; i < parts; i++) {
    // tslint:disable-next-line:no-bitwise
    const S4 = (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    stringArr.push(S4);
  }
  return stringArr.join('-');
}

// export function findId(value: any, arr: any) {
//   return arr.reduce((a: any, item: any) => {
//     if (a) return a;
//     if (item.elementType === value) return item;
//     if (item.children) return findId(value, item.children);
//   }, null);
// }


export function returnImages(object: any) {

  const resultImages = [];

  getAllImages(object, resultImages);

  return resultImages;
}

function getAllImages(object: any, result: any) {

  if (!object) return;


  if (object.hasOwnProperty('elementType')  && object.elementType === 'image') {
    result.push(object);
  }


  for (var i = 0; i < Object.keys(object).length; i++) {
    if (typeof object[Object.keys(object)[i]] == "object") {
      getAllImages(object[Object.keys(object)[i]], result);
    }
  }
}



export function returnAllFormControls(object: any) {
  const result: any = [];

  getAllFormControls(object, result);

  return result;
}
export function getAllFormControls(object: any, result: any) {

  if (!object) return;

  if(object.recursionSkip) return;

  if (object.hasOwnProperty('elementType') 
  && object.elementType === 'form' 
  || object.elementType === 'table'
  || object.elementType === 'image' || object.elementType === 'tableForm' || object.elementType === 'textEditor' || object.elementType === 'excel') {
    result.push(object);
  }

  for (var i = 0; i < Object.keys(object).length; i++) {
    if (typeof object[Object.keys(object)[i]] == "object") {
      getAllFormControls(object[Object.keys(object)[i]], result);
    }
  }
}

export function getAllFormControlsWithId(object: any, result: any, id: string) {

  if (!object) return;

  if (object.hasOwnProperty('id') && object.id == id) {
    result.push(object);
  }

  for (var i = 0; i < Object.keys(object).length; i++) {
    if (typeof object[Object.keys(object)[i]] == "object") {
      getAllFormControls(object[Object.keys(object)[i]], result);
    }
  }
}

export function isNullNumericIdFromSessionStorage(value: string | null): boolean {
  return value === null || value === undefined || value === `` || isNaN(Number(value));
}

export function calculateAspectRatioFit(srcWidth, srcHeight, maxWidth, maxHeight) {

  var ratio = Math.min(maxWidth / srcWidth, maxHeight / srcHeight);

  return { width: srcWidth*ratio, height: srcHeight*ratio };
}