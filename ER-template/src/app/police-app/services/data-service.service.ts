import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { StatusDetails } from 'src/app/shared/models/status-details';

@Injectable({
  providedIn: 'root'
})

export class DataService {
  formDetails: any = null;
  formJson: any = null;
  curFormId: number = null;
  displayIndex: number = 0;
  userResponse: any = null;

  formName: any = null;
  //recordName: any = null;
  formId: number = null;

  private statusDetailsSubject = new BehaviorSubject<StatusDetails | null>(null);
  public statusDetailsObs = this.statusDetailsSubject.asObservable();

  constructor() { }

  resetData() {
    this.formDetails = null;
    this.formJson = null;
    this.curFormId = null;
    this.curFormId = null;
    this.displayIndex = 0;
    this.userResponse = null;
  }

  setStatusDetails(statusDetails: StatusDetails | null) {
    this.statusDetailsSubject.next(statusDetails);
  }

  resetStatusDetails() {
    this.statusDetailsSubject.next(null);
  }
}
