import { Location } from '@angular/common';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AfterViewInit, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable, of } from 'rxjs';
import { DocViewerService } from './services/doc-viewer.service';



@Component({
  selector: 'exai-doc-viewer',
  templateUrl: './doc-viewer.component.html',
  styleUrls: ['./doc-viewer.component.scss']
})
export class DocViewerComponent implements OnInit, AfterViewInit {
  url: Observable<string>


  constructor(private route: ActivatedRoute, private location: Location, private docViewer: DocViewerService,
     private changeDetectionRef: ChangeDetectorRef, private http: HttpClient) { 

     }

  loading: boolean = true
  file

  ngOnInit(): void {
    this.url = of(this.route.snapshot.queryParams.url)
    console.log(this.docViewer.currentFile.value);
    
  }


  ngAfterViewInit(): void {
    // this.route.queryParams.subscribe(data => {
    //   this.url = data.url
    // }) 
    
    this.changeDetectionRef.detectChanges()
  }

  loaded() {
    this.loading = false
    this.changeDetectionRef.detectChanges()
  }

  goBack() {
    this.location.back()
  }

  downloadFile() {
    const headers = new HttpHeaders();

    this.http.get(this.route.snapshot.queryParams.url,{headers, responseType: 'blob' as 'json'}).subscribe(
      (response: any) =>{
          let dataType = response.type;
          let binaryData = [];
          binaryData.push(response);
          let downloadLink = document.createElement('a');
          downloadLink.href = window.URL.createObjectURL(new Blob(binaryData, {type: dataType}));

          downloadLink.setAttribute('download', this.docViewer.currentFile.value.name);
          document.body.appendChild(downloadLink);
          downloadLink.click();
      }
  )
  }
  
}
