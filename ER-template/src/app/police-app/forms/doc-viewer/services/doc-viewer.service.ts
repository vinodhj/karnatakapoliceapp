import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class DocViewerService {

  constructor() { }

  currentFile: BehaviorSubject<any> = new BehaviorSubject(null)

}
