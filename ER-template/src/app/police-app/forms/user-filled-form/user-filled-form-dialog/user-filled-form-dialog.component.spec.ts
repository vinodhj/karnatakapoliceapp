import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserFilledFormDialogComponent } from './user-filled-form-dialog.component';

describe('UserFilledFormDialogComponent', () => {
  let component: UserFilledFormDialogComponent;
  let fixture: ComponentFixture<UserFilledFormDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserFilledFormDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserFilledFormDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
