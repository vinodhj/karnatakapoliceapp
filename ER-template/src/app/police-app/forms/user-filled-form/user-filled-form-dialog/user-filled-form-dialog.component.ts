import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { finalize, map, withLatestFrom } from 'rxjs/operators';
import { HttpService } from 'src/@exai/services/http.service';
import { FormBuilderFillDataService } from 'src/app/form-builder/services/form-builder-fill-data.service';
import { LanguageService } from 'src/app/language.service';
import { DataService } from 'src/app/police-app/services/data-service.service';
import { getNonDuplicatedArray } from 'src/app/police-app/utils/utils';
import { CacheDataService } from 'src/app/shared/services/cache-data.service';

@Component({
  selector: 'exai-user-filled-form-dialog',
  templateUrl: './user-filled-form-dialog.component.html',
  styleUrls: ['./user-filled-form-dialog.component.scss']
})
export class UserFilledFormDialogComponent implements OnInit {

  formGroup: FormGroup;
  isLoading: boolean = false;
  loadingDataForDropdown: boolean = false;

  @ViewChild('phoneNumber') public phoneNumber

  constructor(
    public dialogRef: MatDialogRef<UserFilledFormDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder, private cacheDataService: CacheDataService, public languageService: LanguageService
    , private router: Router, private formBuilderFillDataService: FormBuilderFillDataService,
    private httpService: HttpService, private dataService: DataService) { }

  setupInitialValuesForEdit() {
    if (!this.data.editing) return;

    this.formGroup.get('name').setValue(this.data.name);
  }

  ngOnInit(): void {
    this.formGroup = this.fb.group({
      formName: [``, Validators.required],
      name: ['', Validators.required],
      mobileNumber: [` `, [Validators.required, Validators.pattern('[- +()0-9]+'), Validators.maxLength(13)]]
    });
    this.setupInitialValuesForEdit();
  }

  public ngAfterViewChecked() {
    // disabling country selection button
    try {
      this.phoneNumber.elRef.nativeElement.firstChild.children[0].disabled = 'true';
    }
    catch (e) { //ignore this

    }

  }

  selectComparision(option: any, value: any) {
    if (typeof (option) == 'object') {
      return option?.id == value?.id;
    }
    else return option.toString() == value.toString();
  }

  onCancel() {
    this.dialogRef.close();
  }

  onSubmitForm() {
    const objectData = {
      formName: this.formGroup.get('formName').value,
      candidateName: this.formGroup.get('name').value,
      candidateNumber: this.formGroup.get('mobileNumber').value,
      formId: this.data.formId,
    };

    if (!this.formGroup.valid) return;
    this.isLoading = true;
    this.formBuilderFillDataService.setFormData(objectData);
    this.dialogRef.close();

    if (this.dataService.formDetails.formType == 'Table') {
      this.router.navigate(['forms', 'create-new-record'], { queryParams: { formId: objectData.formId, recordName: objectData.formName } })
      return
    }

    if(this.dataService.formDetails.formType == 'Letter') {
      this.router.navigate(['form-builder', 'fill-form', objectData.formId]);
      return;
    }

    if(this.dataService.formDetails.formType == 'Card'){
    this.router.navigate(['card-builder', 'fill-form', objectData.formId]);
    return;
  }
  }
}