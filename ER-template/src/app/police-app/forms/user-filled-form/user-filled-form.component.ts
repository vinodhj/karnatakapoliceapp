import { SelectionModel } from '@angular/cdk/collections';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { AfterViewInit, ChangeDetectorRef, Component, ComponentFactoryResolver, ComponentRef, ElementRef, Input, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { FormArray, FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatChipInputEvent } from '@angular/material/chips';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatFormFieldDefaultOptions, MAT_FORM_FIELD_DEFAULT_OPTIONS } from '@angular/material/form-field';
import { MatPaginator } from '@angular/material/paginator';
import { MatSelectChange } from '@angular/material/select';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import icAdd from '@iconify/icons-ic/twotone-add';
import icDelete from '@iconify/icons-ic/twotone-delete';
import icEdit from '@iconify/icons-ic/twotone-edit';
import icFilterList from '@iconify/icons-ic/twotone-filter-list';
import icFolder from '@iconify/icons-ic/twotone-folder';
import icMoreHoriz from '@iconify/icons-ic/twotone-more-horiz';
import icSearch from '@iconify/icons-ic/twotone-search';
import { UntilDestroy } from '@ngneat/until-destroy';
// import jsPDF from 'jspdf';
import domtoimage from 'dom-to-image';
// for pdf download
import jsPDF from 'jspdf';
import moment from 'moment';
import { BehaviorSubject, Observable, ReplaySubject, Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';
import { fadeInUp400ms } from 'src/@exai/animations/fade-in-up.animation';
import { stagger40ms } from 'src/@exai/animations/stagger.animation';
import { TableColumn } from 'src/@exai/interfaces/table-column.interface';
import { HttpService } from 'src/@exai/services/http.service';
import { FormBuilderApproveRejectResultComponent } from 'src/app/form-builder/form-builder-custom/form-builder-result/form-builder-result.component';
import { FormBuilderDownloadService } from 'src/app/form-builder/services/form-builder-download.service';
import { FormBuilderFillDataService } from 'src/app/form-builder/services/form-builder-fill-data.service';
import { LanguageService } from 'src/app/language.service';
import { Role } from 'src/app/shared/enums/role.enum';
import { HtmlToDocxService } from 'src/app/shared/services/html-to-docx.service';
import { SnackBarService } from 'src/app/shared/services/snack-bar.service';
import { environment } from 'src/environments/environment';
import { PopUpComponent } from '../../pop-up/pop-up.component';
import { DataService } from '../../services/data-service.service';
import { EditFormComponent } from '../edit-form/edit-form.component';
import { SubmitPopupComponent } from './submit-popup/submit-popup.component';
import { UserFilledFormDialogComponent } from './user-filled-form-dialog/user-filled-form-dialog.component';
import { returnAllFormControls } from 'src/app/utils/randomNumber';
import { CommentDialogComponent } from './dialogs/comment-dialog/comment-dialog.component';
import { CdkPortalOutletAttachedRef, ComponentPortal } from '@angular/cdk/portal';
import { CardBuilderPrintComponent } from 'src/app/card-builder/card-builder-result/card-builder-print/card-builder-print.component';
import jwt_decode from 'jwt-decode';
@UntilDestroy()
@Component({
  selector: 'exai-user-filled-form',
  templateUrl: './user-filled-form.component.html',
  styleUrls: ['./user-filled-form.component.scss'],
  animations: [
    fadeInUp400ms,
    stagger40ms
  ],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        appearance: 'standard'
      } as MatFormFieldDefaultOptions
    }
  ]
})
export class UserFilledFormComponent implements OnInit, AfterViewInit {
  cardPrintPortals = [];
  layoutCtrl = new FormControl('boxed');
  subject$: ReplaySubject<IForm[]> = new ReplaySubject<IForm[]>(1);
  data$: Observable<IForm[]> = this.subject$.asObservable();
  customers: IForm[];
  roleID: number;
  @Input()
  columns: TableColumn<IForm>[] = [
    { label: 'Checkbox', property: 'checkbox', type: 'checkbox', visible: true },
    { label: 'Serial NO', property: 'serialNumber', type: 'serialNumber', visible: true, cssClasses: ['font-medium'] },
    { label: 'Record Id', property: 'alternativeId', type: 'text', visible: true, cssClasses: ['font-medium'] },
    { label: 'Record Name', property: 'recordName', type: 'text', visible: true, cssClasses: ['font-medium'] },
    { label: 'Form Id', property: 'formId', type: 'text', visible: true, cssClasses: ['font-medium'] },
    { label: 'Form Name', property: 'formName', type: 'text', visible: true, cssClasses: ['font-medium'] },
    { label: 'Form Type', property: 'formType', type: 'text', visible: true, cssClasses: ['font-medium'] },
    { label: 'Police-Station', property: 'policeStationName', type: 'text', visible: true, cssClasses: ['text-secondary', 'font-medium'] },
    { label: 'Filled By', property: 'createdBy', type: 'text', visible: true, cssClasses: ['text-secondary', 'font-medium'] },
    { label: 'Filled On', property: 'createdOn', type: 'text', visible: true, cssClasses: ['text-secondary', 'font-medium'] },
    { label: 'Modified By', property: 'modifiedBy', type: 'text', visible: false, cssClasses: ['text-secondary', 'font-medium'] },
    { label: 'Modified On', property: 'modifiedOn', type: 'text', visible: false, cssClasses: ['text-secondary', 'font-medium'] },
    { label: 'Status', property: 'status', type: 'text', visible: true, cssClasses: ['text-secondary', 'font-medium'] },
    { label: 'Comment', property: 'action1', type: 'button', visible: true },
    { label: 'Action', property: 'action', type: 'button', visible: true },
    { label: 'Action', property: 'actions', type: 'button', visible: true },
  ];

  dataSource: MatTableDataSource<IForm> | null;
  selection = new SelectionModel<IForm>(true, []);
  searchCtrl = new FormControl();
  searchPoliceStationControl = new FormControl();

  icEdit = icEdit;
  icSearch = icSearch;
  icDelete = icDelete;
  icAdd = icAdd;
  icFilterList = icFilterList;
  icMoreHoriz = icMoreHoriz;
  icFolder = icFolder;

  length: number;
  pageSize: 10;
  pageSizeOptions = [5, 10, 50];
  downloadLoading = false



  // data for dynamic form Variables
  formJson: any;
  userResponse: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  $userResponse: Observable<any> = this.userResponse.asObservable();

  userResponses: Array<any> = [];
  userResponseSubscription: Subscription;

  downloadInProgress: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  $downloadInProgress: Observable<boolean> = this.downloadInProgress.asObservable();

  @ViewChild(MatPaginator) paginator: MatPaginator;

  @ViewChild('dynform') pdfTable: ElementRef;

  public static templateId: number;

  userRole = this.httpservice.getRole();

  readonly separatorKeysCodes = [ENTER, COMMA] as const;


  // ALLOWED ACTIONS
  get canAddEntry(): boolean {
    return [Role.User, Role.PoliceStation].includes(this.userRole);
  }

  get canApproveReject(): boolean {
    var decoded:any = jwt_decode(sessionStorage.getItem('token'));
    return (decoded.nameid == this.formCreatedBy || [Role.SuperAdmin].includes(this.userRole))
  }

  get isPoliceUser(): boolean {
    return [Role.User].includes(this.userRole)
  }


  fillForm: FormArray;
  formLabels: Array<any>;
  getPoliceStation: Object;
  constructor(private dialog: MatDialog,
    private cdr: ChangeDetectorRef,
    private httpservice: HttpService,
    private formBuilder: FormBuilder,
    public activatedRoute: ActivatedRoute,
    private router: Router,
    public dataService: DataService,
    public _snackbar: MatSnackBar,
    public languageService: LanguageService,
    private formBuilderFillDataService: FormBuilderFillDataService,
    private snackBarService: SnackBarService,
    private formBuilderDownloadService: FormBuilderDownloadService,
    private vcRef: ViewContainerRef, private resolver: ComponentFactoryResolver,
    private htmlToDocxService: HtmlToDocxService,
    private httpService: HttpService
  ) {

    this.formLabels = [
      {
        label: 'N',
        type: 4,
        level: null,
        dependentIndex: null,
        // multiple:false
      },
      {
        label: 'Division',
        type: 2,
        level: 1,
        dependentIndex: 2,
        multiple: false
      },
      {
        label: 'Sub-Division',
        type: 2,
        level: 2,
        dependentIndex: 2,
        multiple: false
      },
      {
        label: 'Police Station',
        type: 2,
        level: 3,
        dependentIndex: 3,
        multiple: false
      },
      {
        label: 'Form Name',
        type: 1,
        level: 4,
        dependentIndex: null,
        disable: true,
      },
      {
        label: 'Name*',
        type: 1,
        level: null,
        dependentIndex: null,
        // multiple:false
      },
      {
        label: 'Mobile Number*',
        type: 1,
        level: null,
        dependentIndex: null,
        // multiple:false
      },
      {
        label: 'Email Id',
        type: 1,
        level: null,
        dependentIndex: null,
        // multiple:false
      },
    ];


    this.fillForm = new FormArray([
      new FormControl(''),
      new FormControl('', [Validators.required]),
      new FormControl('', [Validators.required]),
      new FormControl('', [Validators.required]),
      new FormControl('', [Validators.required]),
      new FormControl('', [Validators.required]),
      new FormControl('', [Validators.required, Validators.pattern(/^-?(0|[1-9]\d*)?$/)]),
      new FormControl(''),

    ])
  }

  formCreatedBy:number = 0;

  isLoading = true

  get visibleColumns() {
    return this.columns.filter(column => column.visible).map(column => column.property);
  }

  getTableData() {
    this.httpservice.getDetailedReport(EditFormComponent.templateId).subscribe((response: Array<IForm>) => {
      this.isLoading = false
      response.forEach((x: any, index: number) => {
        x.serialNumber = index + 1
        x['modifiedOn'] = moment(x.modifiedOn).format('L')
        x['createdOn'] = moment(x.createdOn).format('L')
      })

      this.formCreatedBy = response[0]?.formCreatedBy;


      this.dataSource = new MatTableDataSource(response);

      this.dataSource.filterPredicate = (data, filters) => {
        const matchFilter = [];
        const filterArray = filters.split('+');
        const columns = (<any>Object).values(data);
        const columnLabels = [...this.columns.filter(column => column?.visible && !column?.property?.includes(`actions`)).map(({ property }) => property) as string[]];
        filterArray.forEach(filter => {
          let formattedFilter = filter.trim().toLowerCase()
          const customFilter = [];
          columns.forEach(column => {
            this.wantedSearchedValues.forEach(colProperty => {

              if (colProperty == 'all') {
                for (let key in data) {
                  let formattedDataValue = ((('' + data[key]).toLowerCase().replace(/\s+/g, ' ')).trim())
                  if (columnLabels.indexOf(key) >= 0 && (formattedDataValue.includes(formattedFilter) || formattedDataValue == (formattedFilter))) {
                    customFilter.push(true)
                  }
                }
              } else {
                let formattedDataColProperty = (('' + data[colProperty]).toLowerCase()).trim()
                customFilter.push(formattedDataColProperty.includes(formattedFilter))
              }
            })
          });
          matchFilter.push(customFilter.some(Boolean)); // OR
        });
        return matchFilter.every(Boolean); // AND
      }
      this.dataSource.paginator = this.paginator;
      // this.httpservice.templateGetContent(Number(this.dataService.formId)).subscribe((response: any) => {
      //   if (response) {
      //     response.formJSONstring = JSON.parse(response.formJSONstring);
      //     response.formEditData = JSON.parse(response.formEditData);
      //     this.formJson = response;
      //   }
      // })
    });
  }
  ngOnInit() {
    if (EditFormComponent.templateId) {
      this.getTableData();
      this.data$.pipe(
        filter<IForm[]>(Boolean)
      ).subscribe(customers => {
        this.customers = customers;
        this.dataSource.data = customers;
      });

      this.searchPoliceStationControl.valueChanges.pipe().subscribe((value: any) => {
        this.dataSource.filter = value.trim().toLowerCase();
      })
    }
    else {
      this.router.navigate(['/forms/submitted-form/'])
    }

    // this.httpservice.getDepartmentsByLevel1(3).subscribe(data => {
    //   this.getPoliceStation = data;
    // })
    this.setRole();
  }

  clearSerach(event: any) {
    event.stopPropagation();
    this.searchPoliceStationControl.setValue('')
  }

  submitStatus(row: IForm, status: string) {
    // this.dialog.open(SubmitPopupComponent).afterClosed().subscribe((response) => {
    //   if (response.confirmed) {
    //     this.isLoading = true
    //     this.httpservice.postStatus(row.userFormId, statusCodes[status], response.comment).subscribe(() => {
    //       let string
    //       if (status === 'approve') {
    //         string = `Record Approved Successfully`
    //       } else if (status === 'submit') {
    //         string = `Record Submitted for Approval`
    //       } else {
    //         string = `Record Rejected Successfully`
    //       }
    //       this.snackBarService.showMessage(string, 2000)
    //       this.getTableData();
    //     })
    //   }
    // });
  }

  submitStatusV1(row: IForm, status: string) {
    let dialogConfiguration: MatDialogConfig = {
      width: '25vw',
      height: 'min-content',
      disableClose: false,
      data: {
        userFormId: row.userFormId,
        formId: row.formId,
        action: status
      }
    };
    this.dialog.open(FormBuilderApproveRejectResultComponent, dialogConfiguration)
      .afterClosed().subscribe(
        response => {
          if (response !== null && response !== undefined && response !== ``) {
            let reasonDropdown: string | null = null;
            switch (response?.data?.action) {
              case `approve`: reasonDropdown = response?.formData?.approveReason?.label; break;
              case `reject`: reasonDropdown = response?.formData?.rejectReason?.label; break;
              default: break;
            }
            this.httpservice.postStatus(row.userFormId, statusCodes[status], response?.formData?.comment, reasonDropdown)
              .subscribe(() => {
                let string: string;
                switch (response?.data?.action) {
                  case `approve`: string = `Record Approved Successfully`; break;
                  case `reject`: string = `Record Rejected Successfully`; break;
                  case `submit`: string = `Record Submitted for Approval`; break;
                  default: break;
                }
                this.snackBarService.showMessage(string, 2000);
                this.getTableData();
              })
          }
        }
      );
  }

  get onlySubmitted() {
    return this.selection.selected.every(row => row.status === 'Submitted')
  }

  get onlyReject() {
    return this.selection.selected.some(row => row.status === 'Submitted' || row.status === 'Approved')
  }

  get onlyDrafted() {
    return this.selection.selected.every(row => row.status === 'Draft')
  }
  get draftOrRejected() {
    return this.selection.selected.every(row => row.status === 'Draft' || row.status === 'Rejected')
  }

  disableApproveRejectMultiple = false
  submitMultipleStatus(status) {
    // NE BO SA
    let ids = []
    let idsToSubmit = []

    this.selection.selected.forEach(record => {

      if (record.status === 'Submitted' || record.status === 'Approved') {
        ids.push(record.userFormId)
      }
      if (!this.canApproveReject && (record.status === 'Draft' || record.status === 'Rejected')) {
        ids.push(record.userFormId)
        idsToSubmit.push(record.userFormId)
      }
    })
    if (status === 'approve') {
      this.httpservice.approveMultipleRecords(ids).subscribe(data => {
        this.snackBarService.showMessage('Approved Selected Records Successfully')
        this.getTableData()
      })
    } else if (status === 'reject') {
      this.httpservice.rejectMultipleRecords(ids).subscribe(data => {
        this.snackBarService.showMessage('Rejected Selected Records Successfully')
        this.getTableData()
      })
    } else if (status === 'submit') {
      this.httpservice.submitMultipleRecords(idsToSubmit).subscribe(data => {
        this.snackBarService.showMessage('Submitted Selected Records Successfully')
        this.getTableData()
      })
    }
    this.selection.clear()
  }

  submitSave(row: any) {
    this.dataService.formDetails = {
      userFormId: row.userFormId,
      status: row.status
    }
    this.dialog.open(SubmitPopupComponent).afterClosed().subscribe(() => {
    });
  }
  submit() {
    this.dialog.open(SubmitPopupComponent).afterClosed().subscribe(() => {

    });
  }

  ngAfterViewInit() {
  }

  setRole() {
    var getsession = window.sessionStorage.getItem("role");
    if (getsession == "1") {
      this.roleID = 1
    } else if (getsession == "2") {
      this.roleID = 2
    }
  }

  wantedSearchedValues = []
  searchValues = []

  add(event: MatChipInputEvent): void {
    console.log(event);

    if (event.value) {
      this.searchValues.push(event.value);
    }

    let wantedValues = []
    this.searchValues.forEach(el => {
      if (el.split(':').length == 1) {
        let textForSearch = el.split(':')[0];
        this.wantedSearchedValues.push('all')
        wantedValues.push(textForSearch)
      }
      else {
        let value = el.split(':')[1].trim()
        this.columns.forEach(column => {
          if (column.label === el.split(':')[0]) {
            this.wantedSearchedValues.push(column.property)
          }
        })
        wantedValues.push(value)
      }
    })
    this.dataSource.filter = wantedValues.join('+')
    event.chipInput!.clear();
  }

  remove(value): void {
    const index = this.searchValues.indexOf(value);
    this.wantedSearchedValues.splice(index, 1)
    let dataSourceFilters = this.dataSource.filter.split('+')
    let valueSplitted = value.split(':')

    if (index >= 0) {
      this.searchValues.splice(index, 1);
      if (valueSplitted.length > 1) {
        dataSourceFilters.splice(dataSourceFilters.indexOf(valueSplitted[1].trim()), 1);
      } else {
        dataSourceFilters.splice(dataSourceFilters.indexOf(valueSplitted[0].trim()), 1);
      }
      this.dataSource.filter = dataSourceFilters.join('+')
    }

    if (this.searchValues.length < 1) {
      this.dataSource.filter = ''
    }
  }

  appendToSearch(label) {
    let configuredLabelForSearch = `${label}: `
    this.searchCtrl.setValue(configuredLabelForSearch)
  }

  createFormNew(initialValue: any = null): void {
    let dialogConfiguration: MatDialogConfig = {
      width: '25vw',
      height: 'min-content',
      disableClose: true,
      data: {
        editing: false,
        formId: this.dataService.formId,
      }
    };

    if (initialValue) {
      dialogConfiguration.data = {
        formType: initialValue.templateType,
        departments: initialValue.departments,
        editing: true,
        name: initialValue.name,
        id: initialValue.id,
      }
    }

    const dialogRef = this.dialog.open(UserFilledFormDialogComponent, dialogConfiguration);

  }

  createForm(initialValue: any = null): void {
    let dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.width = '25vw';
    dialogConfig.height = 'min-content';
    this.formLabels[3].initialValue = this.dataService.formName;
    dialogConfig.data = {
      labels: this.formLabels,
      controls: this.fillForm
    }
    console.log(this.formLabels);
    const dialogRef = this.dialog.open(PopUpComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(result => {
      if (result?.value) {
        const objectData = {
          policeStationId: result.value[2].id,
          recordName: result.value[3],
          candidateName: result.value[4],
          candidateNumber: result.value[5],
          candidateEmail: result.value[6],
          formId: this.dataService.formId,
        }

        this.formBuilderFillDataService.setFormData(objectData)

        this.router.navigate(['form-builder', 'fill-form', this.dataService.formId]);
      }
    });
  }

  deleteCustomer(customer: IForm) {
    this.httpservice.deleteFormRecord(customer.userFormId)
  }

  deleteCustomers(customers: IForm[]) {
    customers.forEach(c => this.deleteCustomer(c))
    this.getTableData()
  }


  onFilterChange(value: string) {
    if (!this.dataSource) {
      return;
    }
    value = value.trim();
    value = value.toLowerCase();
    this.dataSource.filter = value;
  }

  toggleColumnVisibility(column: { visible: boolean; }, event: { stopPropagation: () => void; stopImmediatePropagation: () => void; }) {
    event.stopPropagation();
    event.stopImmediatePropagation();
    column.visible = !column.visible;
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  // masterToggle(ref) {
  //   this.isAllSelected() ?
  //     this.selection.clear() :
  //     this.dataSource.connect().value.forEach(row => this.selection.select(row));
  //     ref.checked=false;
  // }

  masterToggle(ref) {
    // console.log(ref);
    // if there is a selection then clear that selection
    if (this.isSomeSelected()) {

      this.selection.clear();
      ref.checked = false;

    } else {
      this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.connect().value.forEach(row => this.selection.select(row));
    }
  }

  isSomeSelected() {
    return this.selection.selected.length > 0;
  }


  trackByProperty<T>(index: number, column: TableColumn<T>) {
    return column.property;
  }

  onLabelChange(change: MatSelectChange, row: IForm) {
    const index = this.customers.findIndex(c => c === row);
    this.customers[index].labels = change.value;
    this.subject$.next(this.customers);
  }

  previewForm(row: any) {

    this.httpservice.currentRecord = row
    this.httpservice.recordCreatedBy = row.createdBy
    if (row.formType === 'Table') {
      if (this.canApproveReject) {
        this.router.navigate(['forms', 'edit-record'], { queryParams: { formId: this.dataService.formId, userFormId: row.userFormId, status: row.status, formName: row.recordName } })
      } else if (!this.canApproveReject) {
        this.router.navigate(['forms', 'preview-record'], { queryParams: { formId: this.dataService.formId, userFormId: row.userFormId, status: row.status, formName: row.recordName } })
      }
      // this.router.navigate(['forms', 'create-new-form',], { queryParams: { formId: this.dataService.formId, userFormId: row.userFormId } })
    } else if (row.formType === 'Letter') {
      this.router.navigate(['form-builder', 'view-form', this.dataService.formId, row.userFormId]);
    } else if (row.formType === 'Card') {
      this.router.navigate(['card-builder', 'view-form', this.dataService.formId, row.userFormId]);
    }
  }

  async downloadPDF(row: any, goToForm: boolean = false) {
    this.httpservice.formView(row.userFormId).subscribe((response: any) => {
      this.userResponse.next(null);

      setTimeout(() => {
        this.userResponse.next(response);

        if (goToForm) {
          this.dataService.displayIndex = 1;
          this.dataService.formJson = this.formJson;
          this.dataService.userResponse = this.userResponse.value;
          this.dataService.formDetails = {
            userFormId: row.userFormId,
          }
          this.router.navigate(['add-new-form']);
        }
        else {
          setTimeout(() => { this.actualDownload(this.formJson.id) }, 100);
        }
      }, 10);
    })
  }
  async actualDownload(componentId: string = null) {

    var img;
    var filename;
    var newImage;
    componentId = componentId ? componentId : 'dynform';
    await domtoimage.toPng(document.getElementById(componentId), { bgcolor: '#fff' }).then(function (dataUrl) {

      img = new Image();
      img.src = dataUrl;
      newImage = img.src;
      img.onload = function () {

        var pdfWidth = img.width;
        var pdfHeight = img.height;
        // FileSaver.saveAs(dataUrl, 'my-pdfimage.png'); // Save as Image
        var doc;
        if (pdfWidth > pdfHeight)
          doc = new jsPDF('l', 'px', [pdfWidth, pdfHeight]);
        else
          doc = new jsPDF('p', 'px', [pdfWidth, pdfHeight]);
        var width = doc.internal.pageSize.getWidth();
        var height = doc.internal.pageSize.getHeight();
        doc.addImage(newImage, 'PNG', 10, 10, width, height);
        filename = 'mypdf_' + '.pdf';
        doc.save(filename);
      };
    }).catch(function (error) {
      console.error(error);
    });
  }
  async downloadSelected() {
    this.userResponse.next(null);
    this.downloadInProgress.next(true);
    for (let i = 0; i < this.selection.selected.length; i++) {
      this.httpservice.formView(this.selection.selected[i].userFormId).subscribe(async (response: any) => {
        this.userResponses.push(response);
        if (this.userResponses.length == this.selection.selected.length) {
          this.downloadAllFiles();
        }
      })
    }

  }
  async downloadAllFiles() {
    let index = -1;
    let userResponseSubscription = this.userResponse.subscribe(async (response: any) => {
      console.log(response);
      if (index == this.selection.selected.length) {
        this.downloadInProgress.next(false);
      }
      let self = this;
      if (response) {
        setTimeout(async () => {
          await this.multidownload(this.formJson.formJSONstring.id).then(async function (dataUrl) {
            var img = new Image();
            img.src = dataUrl;
            var newImage = img.src;
            img.onload = async function () {
              var pdfWidth = img.width;
              var pdfHeight = img.height;
              // FileSaver.saveAs(dataUrl, 'my-pdfimage.png'); // Save as Image
              var doc;
              if (pdfWidth > pdfHeight) doc = new jsPDF('l', 'px', [pdfWidth, pdfHeight]);
              else doc = new jsPDF('p', 'px', [pdfWidth, pdfHeight]);
              var width = doc.internal.pageSize.getWidth();
              var height = doc.internal.pageSize.getHeight();
              doc.addImage(newImage, 'PNG', 10, 10, width, height);
              var filename = 'mypdf_' + '.pdf';
              await doc.save(filename);
              if (index < self.userResponses.length) {
                if (response) {
                  self.setUserResponseNext(null);
                }
                else self.setUserResponseNext(self.userResponses[++index]);
              }
              else
                userResponseSubscription.unsubscribe();
            };
          }).catch(function (error) {
            console.error(error);
          });
        }, 10);
      }
      else if (index < this.userResponses.length) {
        if (response) self.setUserResponseNext(null);
        else self.setUserResponseNext(self.userResponses[++index]);
      }
    })

  }
  private multidownload(id: string): Promise<any> {
    return domtoimage.toPng(document.getElementById(id), { bgcolor: '#fff' })
  }
  setUserResponseNext(value: any) {
    setTimeout(() => {
      this.userResponse.next(value);
    }, 10);
  }
  downloadExcel() {
    window.location.href = environment.backendUrl + `Form/report/dynamic?templateId=${this.dataService.formId}`;
    // this.httpservice.formReportDynamic(this.dataSource.data[0].formId).subscribe((response: any) => {
    //   var blobResponse = response as Blob;
    //   FS.saveAs(blobResponse, this.formJson.id);
    // });
  }


  downloadForms(type = null, userFormId = null, documentType = 'word', row = null) { 

    this.downloadLoading = true
    let selectedIds;
    //download selected filled forms
    if (type == null)
      selectedIds = this.selection.selected.map(form => form.userFormId)
    //download all filled forms
    if (type === 'all')
      selectedIds = this.dataSource.connect().value.map(form => form.userFormId)
    //download only one filled form
    if (type === 'single')
      selectedIds = [userFormId];
    if (this.dataService.formDetails.formType === 'Letter') {
      this.httpservice.getViewsForFormBulder(selectedIds, this.dataService.formId)
        .subscribe(({ forms, template }: any) => {
          this.downloadLoading = false
          forms.map(form => {
            if (form.userInput)
              this.fillTemplate(template.content, JSON.parse(form.userInput), documentType, `${form.name}`);
            else {
              this._snackbar.open(`Form ${form.name} was not found`, 'Ok', {
                duration: 3000
              });
            }
          })
        })
    } else if (this.dataService.formDetails.formType === 'Card') {

      this.httpservice.getViewsForFormBulder(selectedIds, this.dataService.formId)
        .subscribe(({ forms, template }: any) => {
          this.downloadLoading = false
          forms.map(form => {
            if (form.userInput)
              this.downloadCardPortals(template.content, JSON.parse(form.userInput), `${form.name}`);
            // this.fillTemplate(template.content, JSON.parse(form.userInput), documentType, `${form.name}`);
            else {
              this._snackbar.open(`Form ${form.name} was not found`, 'Ok', {
                duration: 3000
              });
            }
          })
        })

      // this._snackbar.open('Card type is unable for download', 'Ok', {
      //   duration: 3000
      // })

      return;

    } else {
      this.httpservice.getViewsForFormBulder(selectedIds, this.dataService.formId).subscribe(({ forms, template }: any) => {
        let mathOperationsColumnVisible = this.httpservice.roleId == 4        
        const matchFields = {}

        

        template.content.columns.forEach(column => {
          if (column.type === 'mathOperations' && mathOperationsColumnVisible) {            
            matchFields[column.property] = column.label
          }

          if (column.property != 'firstColumn' && column.property != 'actionsColumn' && column.type !== 'mathOperations') {
            matchFields[column.property] = column.label
          }
        })
        
        let userForms = []
        forms.forEach(form => {
          console.log({form});
          
          let userInput = JSON.parse(form.userInput)
          userForms.push({userInput, name: form.name})
        })

        userForms.forEach(form => {
          console.log({form});
          
          let formattedTableValues = []
          form.userInput.rows.forEach((row: any) => {
            let oneRowInTable = {};
            for (const rowKey in matchFields) {
              const headerName = matchFields[rowKey];
              if (row[rowKey])
                oneRowInTable[headerName] = { value: row[rowKey].value, rowSpan: row[rowKey].rowSpan, colSpan: row[rowKey].colSpan, type: row[rowKey].type, visible: true };
              if (!this.canApproveReject && row.hiddenFromUser) {
                oneRowInTable[headerName] = { value: row[rowKey].value, rowSpan: row[rowKey].rowSpan, colSpan: row[rowKey].colSpan, type: row[rowKey].type, visible: false };
              }

              if (!row[rowKey].value) {
                oneRowInTable[headerName].value = ''
              }
              if (row[rowKey].type === 'file' && row[rowKey].value) {
                let files = []
                row[rowKey].value.forEach(file => {
                  if (file.type && file.type === 'image') {
                    files.push({
                      imageContent: file.imageContent,
                      type: file.type
                    })
                  } else {
                    files.push({
                      url: file.url,
                      name: file.name
                    })
                  }
                })
                oneRowInTable[headerName].value = files
              }
              if (row[rowKey].type === 'hyperlink' && row[rowKey].value) {
                let links = []
                row[rowKey].value.forEach(link => {

                  links.push({
                    url: link.url,
                    name: link.name
                  })

                })
                oneRowInTable[headerName].value = links
              }
            }
            formattedTableValues.push(oneRowInTable);

          })

          let tableData = this.htmlToDocxService.generateTableWithMerge(formattedTableValues)
          

          if (documentType === 'word') {
            console.log(row);
            
            row ? this.htmlToDocxService.generateDocxFromHTML(tableData.outerHTML, row.recordName) : this.htmlToDocxService.generateDocxFromHTML(tableData.outerHTML, form.name)
          }

          if (documentType === 'pdf')
            this.htmlToDocxService.downloadAsJsPDF(tableData, row.recordName)
        })
        this.downloadLoading = false
      })
    }
  }

  downloadCardPortals(content, userInput, recordName) {
    this.cardPrintPortals.push({ portalReference: new ComponentPortal(CardBuilderPrintComponent), content, userInput, recordName });
  }

  cardPrintPortalAttach(ref: CdkPortalOutletAttachedRef, content, userInput, recordName) {

    console.log(ref, userInput, "HI");
    this.cdr.markForCheck();
    ref = ref as ComponentRef<CardBuilderPrintComponent>;

    ref.instance.cardConfiguration = content;
    ref.instance.userInput = userInput;

    this.cdr.detectChanges();
  }

  fillTemplate(content, filledForms, documentType, fileName = null) {

    const allFormControls = returnAllFormControls(content);

    allFormControls.forEach(contentField => {

      for (const id in filledForms) {

        if (id === contentField.id) {

          contentField.value = filledForms[id];

          if (contentField.type === 'table')
            contentField.htmlTable = filledForms[id];


          if(contentField.type === 'table-form')
            contentField.tableValue.rows = filledForms[id];

          if(contentField.type === 'excel-form') {
            //if user excel form is being filled then we have url from that excel
            //replace users excel with original excel 
            if(filledForms[id] && filledForms[id].excelUrl)
              contentField.tableValue.excelUrl = filledForms[id].excelUrl;
          }

        }

      }
    })

    this.formBuilderDownloadService.generateDataForDownload(content, null, documentType, fileName);


  };

  openCommentDialog(row) {
    const dialogRef = this.dialog.open(CommentDialogComponent, {
      width: '400px',
      data: {
        comment: row.comment,
        recordName: row.recordName
      },
    });

    dialogRef.afterClosed().subscribe()

  }

}

export class IForm {
  userFormId?: number;
  id?: number;
  formId?: string;
  formName?: string;
  formType?: string;
  recordName?: string;
  status?: string;
  policeStationName?: number;  
  createdBy?: string;
  createdOn?: string;
  modifiedBy?: string;
  modifiedOn?: string;
  labels?: any;
  showSubmitButton?: boolean;
  showReviewButton?: boolean;
  formCreatedBy?: number;
}

export enum statusCodes {
  "approve" = "AP",
  "submit" = "SU",
  "reject" = "RJ"
}

