import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { HttpService } from 'src/@exai/services/http.service';
import { LanguageService } from 'src/app/language.service';
import { DataService } from 'src/app/police-app/services/data-service.service';
import { HttpErrorResponse } from '@angular/common/http';
@Component({
  selector: 'exai-submit-popup',
  templateUrl: './submit-popup.component.html',
  styleUrls: ['./submit-popup.component.scss']
})
export class SubmitPopupComponent implements OnInit {
  public comment: string;
  constructor(
    private router: Router,
    public languageService: LanguageService,
    private httpService: HttpService,
    private dataService: DataService,
    private _snackbar: MatSnackBar,
    private dialogRef: MatDialogRef<SubmitPopupComponent>,
  ) { }

  commentControl: FormControl = new FormControl();
  ngOnInit(): void {
  }

  submitApp() {
    // this.router.navigate(['/forms/user-filled-form/']);
    this.dialogRef.close({comment: this.commentControl.value, confirmed: true});
  }


}
