import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserFilledFormComponent } from './user-filled-form.component';

describe('UserFilledFormComponent', () => {
  let component: UserFilledFormComponent;
  let fixture: ComponentFixture<UserFilledFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserFilledFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserFilledFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
