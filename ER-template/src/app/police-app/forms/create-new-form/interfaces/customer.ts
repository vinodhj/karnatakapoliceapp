export interface Customer {
    id?: number;
    imageSrc?: string;
    templateName?: string;
    templateType?: string;
    createdBy?: string;
    createdOn?: number;
    modifiedBy?: string;
    modifiedOn?: string;
    status?: string;
    labels?: any;
    LocationObj?: Object;
    SubdivisionObj?: Object
    PolicestationObj?: Object;
    name:string 
}
