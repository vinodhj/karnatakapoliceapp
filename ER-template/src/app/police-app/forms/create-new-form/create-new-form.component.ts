import { AfterViewInit, Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpService } from 'src/@exai/services/http.service';
import { MatTableDataSource } from '@angular/material/table';
import { PopUpComponent } from '../../pop-up/pop-up.component';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { FormArray, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Observable, of, ReplaySubject, Subject } from 'rxjs';
import { concatMap, filter, finalize, map, switchMap, takeUntil, tap } from 'rxjs/operators';
import { TableColumn } from 'src/@exai/interfaces/table-column.interface';
import { SelectionModel } from '@angular/cdk/collections';
import icMoreHoriz from '@iconify/icons-ic/twotone-more-horiz';
import icFolder from '@iconify/icons-ic/twotone-folder';
import icEdit from '@iconify/icons-ic/twotone-edit';
import icDelete from '@iconify/icons-ic/twotone-delete';
import icSearch from '@iconify/icons-ic/twotone-search';
import icAdd from '@iconify/icons-ic/twotone-add';
import { MatSelectChange } from '@angular/material/select';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import icFilterList from '@iconify/icons-ic/twotone-filter-list';
import { MatFormFieldDefaultOptions, MAT_FORM_FIELD_DEFAULT_OPTIONS } from '@angular/material/form-field';
import { fadeInUp400ms } from 'src/@exai/animations/fade-in-up.animation';
import { stagger40ms } from 'src/@exai/animations/stagger.animation';
import moment from 'moment';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { MatSnackBar } from '@angular/material/snack-bar';
import { LanguageService } from 'src/app/language.service';
import { DataService } from '../../services/data-service.service';
import { MatChipInputEvent } from '@angular/material/chips';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { SnackBarService } from 'src/app/shared/services/snack-bar.service';
import { Role } from 'src/app/shared/enums/role.enum';
import { HtmlToDocxService } from 'src/app/shared/services/html-to-docx.service';
import { AddNewFormDialogComponent } from './add-new-form-dialog/add-new-form-dialog.component';
import { ConfirmDialogComponent } from 'src/app/shared/advanced-grid-item/dialogs/confirm-dialog/confirm-dialog.component';

@UntilDestroy()
@Component({
  selector: 'exai-create-new-form',
  templateUrl: './create-new-form.component.html',
  styleUrls: ['./create-new-form.component.scss'],
  animations: [
    fadeInUp400ms,
    stagger40ms
  ],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        appearance: 'standard'
      } as MatFormFieldDefaultOptions
    }
  ]
})
export class CreateNewFormComponent implements OnInit, AfterViewInit, OnDestroy {

  layoutCtrl = new FormControl('boxed');
  subject$: ReplaySubject<Customer[]> = new ReplaySubject<Customer[]>(1);
  data$: Observable<Customer[]> = this.subject$.asObservable();
  customers: Customer[];
  formListData: Customer[];

  @Input()
  columns: TableColumn<Customer>[] = [
    // { label: 'Checkbox', property: 'checkbox', type: 'checkbox', visible: true },
    // { label: 'Id', property: 'id', type: 'text', visible: true, cssClasses: ['font-medium'] },
    { label: 'Serial NO', property: 'serialNumber', type: 'serialNumber', visible: true, cssClasses: ['font-medium'] },
    { label: 'Form Id', property: 'alternativeId', type: 'text', visible: true, cssClasses: ['font-medium'] },
    { label: 'Form Name', property: 'name', type: 'text', visible: true },
    { label: 'Form Type', property: 'templateType', type: 'text', visible: true },
    { label: 'Sub Divisions', property: 'subDivisionName', type: 'subDivision', visible: true },
    { label: 'Police Stations', property: 'policeStationsName', type: 'policeStatins', visible: true },
    { label: 'Created By', property: 'createdBy', type: 'text', visible: false },
    { label: 'Created On', property: 'createdOn', type: 'text', visible: false, cssClasses: ['text-secondary', 'font-medium'] },
    { label: 'Modified By', property: 'modifiedBy', type: 'text', visible: false, cssClasses: ['text-secondary', 'font-medium'] },
    { label: 'Modified On', property: 'modifiedOn', type: 'text', visible: false, cssClasses: ['text-secondary', 'font-medium'] },
    { label: 'Status', property: 'status', type: 'button', visible: true },
    { label: 'Action', property: 'actions', type: 'button', visible: true, cssClasses: ['text-secondary', 'font-medium'] },
  ];

  dataSource: MatTableDataSource<Customer> | null;
  selection = new SelectionModel<Customer>(true, []);
  searchCtrl = new FormControl();

  icEdit = icEdit;
  icSearch = icSearch;
  icDelete = icDelete;
  icAdd = icAdd;
  icFilterList = icFilterList;
  icMoreHoriz = icMoreHoriz;
  icFolder = icFolder;
  isPublished: boolean = false

  length: number;
  pageSize: 10;
  pageSizeOptions = [5, 10, 15, 20, 50];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  unsubscribe$ = new Subject();

  addNewForm: FormArray;
  formLabels: Array<any>;
  constructor(private dialog: MatDialog,
    private httpservice: HttpService,
    public activatedRoute: ActivatedRoute,
    private router: Router,
    public dataService: DataService,
    private _snackbar: MatSnackBar,
    public languageService: LanguageService,
    private htmlToDocxService: HtmlToDocxService,
    private snackBarService: SnackBarService
  ) { }

  get visibleColumns() {
    if (!this.allowActionsInsideTable)
      this.columns.map(column => column.property === 'actions' ? column.visible = false : null);

    return this.columns.filter(column => column.visible).map(column => column.property);
  }

  userRole = this.httpservice.getRole();

  get canChangeStatus(): boolean {
    return [
      Role.Admin,
      Role.DistrictHead,
      Role.DivisionHead,
      Role.StateHead,
      Role.SubDivision].includes(this.userRole);
  }

  // ALLOWED ACTIONS
  get canAddEntry(): boolean {
    return [
      Role.Admin,
      Role.DistrictHead,
      Role.DivisionHead,
      Role.StateHead,
      Role.SubDivision].includes(this.userRole);
  }

  get allowActionsInsideTable() {
    return [
      Role.SuperAdmin,
      Role.Admin,
      Role.DistrictHead,
      Role.DivisionHead,
      Role.StateHead,
      Role.SubDivision].includes(this.userRole);
  }

  get superAdmin() {
    return [Role.SuperAdmin].includes(this.userRole);
  }

  showActionsOnTable() {
    this.columns = this.columns.filter(c => c.label != 'Action');
  }

  isLoading = true

  ngOnInit() {


  }

  getDate() {
    this.formListData.forEach(element => {
      element['subDate'] = moment(element.createdOn).format('L');
      element['lastDate'] = moment(element.modifiedOn).format('L');
    });
  }

  tabledata() {

    this.isLoading = true;

    this.dataService.resetData();
    // this.dataSource = new MatTableDataSource<Customer>();
    this.activatedRoute.queryParams.pipe(
      switchMap(param => {
        return this.httpservice.getBasicFormByFormType(param.formType).pipe(
          finalize(() => this.isLoading = false)
        )
      })
    ).subscribe((response: Array<Customer>) => {


      response.forEach((x: any, index: number) => {
        x.serialNumber = index + 1
        x['modifiedOn'] = moment(x.modifiedOn).format('MMMM Do YYYY, h:mm a')
        x['createdOn'] = moment(x.createdOn).format('MMMM Do YYYY, h:mm a')
      })

      response.forEach((row: any) => {

        row.subDivisionName = this.getSubDivisions(row);
        row.policeStationsName = this.getPoliceStations(row);

      })
      console.log(response);
      

      this.dataSource = new MatTableDataSource(response);
      this.dataSource.paginator = this.paginator;
      // advanced filtering function


      this.dataSource.filterPredicate = (data, filters) => {

        const matchFilter = [];
        const filterArray = filters.split('+');
        const columns = Object.values(data);
        const columnLabels = [...this.columns.filter(column => column?.visible && !column?.property?.includes(`actions`)).map(({ property }) => property) as string[]];
        filterArray.forEach(filter => {
          let formattedFilter = filter.trim().toLowerCase()
          const customFilter = [];

          columns.forEach(column => {
            this.wantedSearchedValues.forEach(colProperty => {

              if (colProperty == 'all') {
                for (let key in data) {
                  let formattedDataValue = ((('' + data[key]).toLowerCase().replace(/\s+/g, ' ')).trim())
                  if (columnLabels.indexOf(key) >= 0 && (formattedDataValue.includes(formattedFilter) || formattedDataValue == (formattedFilter))) {
                    customFilter.push(true)
                  }
                }
              } else {
                let formattedDataColProperty = (('' + data[colProperty]).toLowerCase()).trim()
                customFilter.push(formattedDataColProperty.includes(formattedFilter))
              }
            })
          });

          matchFilter.push(customFilter.some(Boolean)); // OR
        });
        return matchFilter.every(Boolean); // AND
      }
      this.formListData = response;
      this.getDate()
    });




    // this.httpservice.getBasicForm().pipe(
    //   finalize(() => this.isLoading = false)
    // ).subscribe((response: Array<Customer>) => {

    //   response.forEach((x: any) => {
    //     x['modifiedOn'] = moment(x.modifiedOn).format('MMMM Do YYYY, h:mm a')
    //     x['createdOn'] = moment(x.createdOn).format('MMMM Do YYYY, h:mm a')
    //   })

    //   response.forEach((row: any) => {

    //     row.subDivisionName = this.getSubDivisions(row);
    //     row.policeStationsName = this.getPoliceStations(row);

    //   })

    //   this.dataSource = new MatTableDataSource(response);
    //   this.dataSource.paginator = this.paginator;
    //   // advanced filtering function


    //   this.dataSource.filterPredicate = (data, filters) => {

    //     const matchFilter = [];
    //     const filterArray = filters.split('+');
    //     const columns = Object.values(data);
    //     const columnLabels = [...this.columns.filter(column => column?.visible && !column?.property?.includes(`actions`)).map(({ property }) => property) as string[]];
    //     filterArray.forEach(filter => {
    //       let formattedFilter = filter.trim().toLowerCase()
    //       const customFilter = [];

    //       columns.forEach(column => {
    //         this.wantedSearchedValues.forEach(colProperty => {

    //           if (colProperty == 'all') {
    //             for (let key in data) {
    //               let formattedDataValue = ((('' + data[key]).toLowerCase().replace(/\s+/g, ' ')).trim())
    //               if (columnLabels.indexOf(key) >= 0 && (formattedDataValue.includes(formattedFilter) || formattedDataValue == (formattedFilter))) {
    //                 customFilter.push(true)
    //               }
    //             }
    //           } else {
    //             let formattedDataColProperty = (('' + data[colProperty]).toLowerCase()).trim()
    //             customFilter.push(formattedDataColProperty.includes(formattedFilter))
    //           }
    //         })
    //       });

    //       matchFilter.push(customFilter.some(Boolean)); // OR
    //     });
    //     return matchFilter.every(Boolean); // AND
    //   }
    //   this.formListData = response;
    //   this.getDate()
    // });

    // this.dataSource = new MatTableDataSource<Customer>();

    this.data$.pipe(
      filter<Customer[]>(Boolean)
    ).subscribe(customers => {
      this.customers = customers;
      this.dataSource.data = customers;
    });
  }

  ngAfterViewInit() {
    this.tabledata();

  }

  wantedSearchedValues = []

  add(event: MatChipInputEvent): void {
    console.log(event);

    if (event.value) {
      this.searchValues.push(event.value);
    }

    let wantedValues = []
    this.searchValues.forEach(el => {
      if (el.split(' :').length == 1) {
        let textForSearch = el.split(' :')[0];
        // this.dataSource.filter = textForSearch;
        this.wantedSearchedValues.push('all')
        wantedValues.push(textForSearch)
      }
      else {
        let value = el.split(' :')[1].trim()
        this.columns.forEach(column => {
          if (column.label === el.split(' :')[0]) {
            this.wantedSearchedValues.push(column.property)
          }
        })
        wantedValues.push(value)
      }
    })
    this.dataSource.filter = wantedValues.join('+')

    // Clear the input value
    event.chipInput!.clear();
  }

  remove(value): void {
    const index = this.searchValues.indexOf(value);
    let dataSourceFilters = this.dataSource.filter.split('+')
    let valueSplitted = value.split(':')

    if (index >= 0) {
      this.searchValues.splice(index, 1);
      if (valueSplitted.length > 1) {
        dataSourceFilters.splice(dataSourceFilters.indexOf(valueSplitted[1].trim()), 1);
      } else {
        dataSourceFilters.splice(dataSourceFilters.indexOf(valueSplitted[0].trim()), 1);
      }
      this.dataSource.filter = dataSourceFilters.join('+')
    }

    if (this.searchValues.length < 1) {
      this.dataSource.filter = ''
    }
  }

  appendToSearch(label) {
    let configuredLabelForSearch = `${label} : `
    this.searchCtrl.setValue(configuredLabelForSearch)
  }


  getNonDuplicatedArray(array: Array<any>) {
    let retVal = [];
    for (let item of array) {
      if (!retVal.includes(JSON.stringify(item))) {
        retVal.push(JSON.stringify(item));
      }
    }
    retVal = retVal.map((x: any) => { return JSON.parse(x) });
    return retVal;
  }

  createForm(initialValue: any = null): void {
    console.log({ initialValue });

    let dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.width = '25vw';
    dialogConfig.maxHeight = '200px';
    initialValue ? this.setLabels(true, true) : this.setLabels();
    if (initialValue) {
      this.formLabels[0].initialValue = initialValue ? initialValue.name : '';
      this.formLabels[1].initialValue = initialValue ? this.getNonDuplicatedArray(initialValue.departments.map((x: any) => { return x.parent.parent })) : '';
      this.formLabels[2].initialValue = initialValue ? this.getNonDuplicatedArray(initialValue.departments.map((x: any) => { return x.parent })) : '';
      this.formLabels[3].initialValue = initialValue ? this.getNonDuplicatedArray(initialValue.departments.map((x: any) => { return x })) : '';
      // this.formLabels[4].initialValue = initialValue ? initialValue.templateType : "";
    }


    let dialogConfiguration: MatDialogConfig = {
      width: '25vw',
      height: '80%',
      disableClose: true,
      data: {
        editing: false,
        templateType: this.activatedRoute.snapshot.queryParamMap.get('formType') ? this.activatedRoute.snapshot.queryParamMap.get('formType') : null,
      }
    };

    if (initialValue) {
      dialogConfiguration.data = {
        formType: initialValue.templateType,
        departments: initialValue.departments,
        editing: true,
        name: initialValue.name,
        id: initialValue.id
      }
    }

    const dialogRef = this.dialog.open(AddNewFormDialogComponent, dialogConfiguration);

    dialogRef.afterClosed().subscribe(response => {

      if (response === 'edited')
        this.tabledata();

    })
    // const dialogRef = this.dialog.open(PopUpComponent, dialogConfig);
    // dialogRef.afterClosed().subscribe(result => {
    //   if (result?.value) {

    //     this.isLoading = true;
    //     this.dataService.formDetails = result.value;

    //     this.addingPoliceStation ?
    //       this.httpservice.templateUpdateDepartment(initialValue.id, (result.value[3] as Array<any>).map((x: any) => { return x.id }).join(',')).pipe(
    //         finalize(() => this.isLoading = false)
    //       ).subscribe((response: any) => {

    //         this.tabledata();
    //       }) : this.httpservice.templateAddNew(result.value[0], (result.value[3] as Array<any>).map((x: any) => { return x.id }).join(','), result.value[4]).pipe(
    //         finalize(() => this.isLoading = false)
    //       ).subscribe((response: any) => {
    //         this.dataService.curFormId = response;
    //         this.dataService.displayIndex = 0;

    //         if (result.value[4] === 'Table') {
    //           console.log(response);

    //           this.router.navigate(['/forms', 'create-new-form'], {
    //             queryParams: { colNumber: result.colNumber },
    //           })

    //         } else {
    //           this.router.navigate(['form-builder', 'create', response])
    //         }
    //       });

    //   }

    // });

  }

  updateForm(form: Customer) {
    console.log(form);

    if (form.isPublished) {
      this.snackBarService.showMessage('Cannot Edit a Published Form', 3000)
      return
    } else {
      this.httpservice.templateGetContent(form.id).subscribe(data => {
        if (form.templateType === 'Table') {
          this.router.navigate(['/forms', 'create-new-form'], { queryParams: { formId: form.id, formName: form.name, edit: 'edit' } })
        } else if(form.templateType === 'Letter') {
          this.router.navigate(['form-builder', 'edit', form.id])
        } else if(form.templateType === 'Card') {
          this.router.navigate(['card-builder', 'edit', form.id])
        }
      })
    }
  }


  addingPoliceStation: boolean = false;

  addEditPoliceStation(customer$) {
    this.addingPoliceStation = true;
    this.createForm(customer$);
  }


  toggleColumnVisibility({ column, event }) {
    event.stopPropagation();
    event.stopImmediatePropagation();
    column.visible = !column.visible;
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  trackByProperty<T>(index: number, column: TableColumn<T>) {
    return column.property;
  }

  onLabelChange(change: MatSelectChange, row: Customer) {
    const index = this.customers.findIndex(c => c === row);
    this.customers[index].labels = change.value;
    this.subject$.next(this.customers);
  }

  disable(ev: MatSlideToggleChange, row) {
    console.log(row);
    let status = {
      id: row.id,
      isPublished: !row.isPublished
    }
    this.httpservice.templateModifyPublish(status).subscribe(data => {
      this.tabledata()
      if (data == "true") {
        this._snackbar.open("Form has been Published", '', {
          duration: 1000,
          // horizontalPosition: 'right'
        })
      }
    })

  }
  setLabels(readonly: boolean = false, disabled: boolean = false) {
    this.formLabels = [
      {
        label: 'Name',
        type: 1,
        level: null,
        dependentIndex: null,
        readonly
      },
      {
        label: 'Division',
        type: 2,
        level: 1,
        dependentIndex: 2,
        multiple: true
      },
      {
        label: 'Sub-Division',
        type: 2,
        level: 2,
        dependentIndex: 3,
        multiple: true
      },
      {
        label: 'Police Station',
        type: 2,
        level: 3,
        dependentIndex: null,
        multiple: true
      },
    ];
    this.addNewForm = new FormArray([
      new FormControl('', [Validators.required]),
      new FormControl('', [Validators.required]),
      new FormControl('', [Validators.required]),
      new FormControl('', [Validators.required]),
    ])

    if (!disabled) {
      this.formLabels.push({
        label: 'Form Type',
        type: 3,
        dependentIndex: null,
        multiple: false,
        level: null,
        options: ['Letter', 'Table'],
      });
      this.addNewForm.push(new FormControl('', [Validators.required]))
    }
  }

  createNewForm() {
    this.router.navigate(['/forms/create-new-form'])
  }


  updateStatus({ event, element }) {

    if (element === null ||
      element === undefined ||
      element === ``)
      return;
    let message = `Are you sure that you want to ${!event?.checked ? ` unpublish ` : ` publish `}this form?`;
    const confirmDialog = this.dialog.open(ConfirmDialogComponent, {
      width: `500px`,
      data: { message: message }
    });
    confirmDialog.afterClosed().subscribe(result => {
      if (result?.answer) {

        this.publishForm(element, event?.checked)

      } else {

        element.isPublished = !event.checked;

      }
    });
  }


  deleteForm(data) {

    let message = `Are you sure that you want to delete this form?`;
    const confirmDialog = this.dialog.open(ConfirmDialogComponent, {
      width: `500px`,
      data: { message: message }
    });

    confirmDialog.afterClosed().subscribe(result => {
      if (result?.answer) {

        this.httpservice.deleteForm(data).subscribe(res => {

          this.snackBarService.showMessage(`Form ${data.name} is successfully deleted`, 3000);

          this.tabledata();



        }, err => {

          this.snackBarService.showMessage(err?.error?.Message, 3000);

        });

        // this.publishForm(element, event?.checked)

      } else {

        // element.isPublished = !event.checked;

      }
    });
  }

  statusToggleLoading: boolean;


  publishForm(element: any, published): void {
    this.isLoading = true;

    this.httpservice.publishForm(element?.id, published).pipe(
      finalize(() => this.isLoading = false)
    ).subscribe(
      (data: any) => {
        this.snackBarService.showMessage(data?.message, 2000);
      },
      error => {
        this.snackBarService.showMessage(error.error?.message || "Error occured", 2000)
        this.tabledata();
      },
      () => element.isPublished = published
    );
  }

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.columns, event.previousIndex, event.currentIndex);
  }

  camelCaseToNormal(text) {
    const result = text.replace(/([A-Z])/g, " $1");
    const finalResult = result.charAt(0).toUpperCase() + result.slice(1);
    return finalResult
  }


  public getSubDivisions(rowValue) {
    let subDivisions = []
    rowValue.departments.forEach(element => {
      if (element.name === 'Police-Station') {
        subDivisions.push(element.parent.value)
      } else if (element.name === 'Sub-Division') {
        subDivisions.push(element.value)
      }
    })
    return [...new Set(subDivisions)]?.join(', ');
  }

  public getPoliceStations(row) {
    let policeStations = []
    row.departments.forEach(element => {
      if (element.name === 'Police-Station') {
        policeStations.push(element.value)
      }
    })
    return [...new Set(policeStations)].join(', ')
  }

  cloneForm(row) {
    console.log(row);

    let dialogConfiguration: MatDialogConfig = {
      width: '25vw',
      height: 'min-content',
      disableClose: true,
      data: {
        editing: false,
        cloning: true,
        cloneFormId: row.id,
        formType: row.templateType,
        disableFormTypeInput: true
      }
    };

    const dialogRef = this.dialog.open(AddNewFormDialogComponent, dialogConfiguration);

    dialogRef.afterClosed().subscribe(response => {

      if (response === 'edited')
        this.tabledata();

    })

  }

  downloadWord(type: string) {


    let formattedTableValues = []

    const matchFields = {
      serialNumber: 'SERIAL NO',
      alternativeId: 'FORM ID',
      name: 'FORM NAME',
      templateType: "FORM TYPE",
      subDivisionName: 'SUB DIVISIONS',
      policeStationsName: 'POLICE STATIONS',
      createdBy: "CREATED BY",
      createdOn: "CREATED ON",
      modifiedBy: "MODIFIED BY",
      modifiedOn: "MODIFIED ON",
      isActive: "STATUS"
    }

    //match column name with column label
    this.columns.map(column => {

      if (!column.visible) {

        for (const rowKey in matchFields) {

          const valueOfRow = matchFields[rowKey]

          if (valueOfRow.toLowerCase() === column.label.toLowerCase())
            delete matchFields[rowKey];

        }

      }

    })

    //if user filtered data then export only that data else all data
    const dataForExport = this.dataSource.filter ? this.dataSource.connect().value : this.dataSource.data;

    dataForExport.forEach((row: any) => {

      row.subDivisionName = this.getSubDivisions(row);
      row.policeStationsName = this.getPoliceStations(row);

      let oneRowInTable = {};

      for (const rowKey in matchFields) {
        const headerName = matchFields[rowKey];

        if (headerName === 'STATUS') {

          oneRowInTable[headerName] = row[rowKey] ? 'Published' : 'Unpublished';

        } else {

          oneRowInTable[headerName] = row[rowKey];

        }

      }

      formattedTableValues.push(oneRowInTable);

    })

    this.htmlToDocxService.downloadBasedOnType(type, formattedTableValues, 'config-forms');
  }

  addOnBlur = true;
  searchValues = [];

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}

export class Customer {
  id?: number;
  imageSrc?: string;
  templateName?: string;
  templateType?: string;
  createdBy?: string;
  createdOn?: number;
  modifiedBy?: string;
  modifiedOn?: string;
  status?: string;
  labels?: any;
  LocationObj?: Object;
  SubdivisionObj?: Object
  PolicestationObj?: Object;
  name: string;
  isPublished: boolean;
}