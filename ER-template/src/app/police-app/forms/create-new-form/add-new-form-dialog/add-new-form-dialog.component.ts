import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { BehaviorSubject, forkJoin } from 'rxjs';
import { finalize, map, tap } from 'rxjs/operators';
import { HttpService } from 'src/@exai/services/http.service';
import { LanguageService } from 'src/app/language.service';
import { DataService } from 'src/app/police-app/services/data-service.service';
import { noWhitespaceValidator } from 'src/app/shared/models/no-whitespace.validator';
import { SnackBarService } from 'src/app/shared/services/snack-bar.service';

@Component({
  selector: 'exai-add-new-form-dialog',
  templateUrl: './add-new-form-dialog.component.html',
  styleUrls: ['./add-new-form-dialog.component.scss']
})
export class AddNewFormDialogComponent implements OnInit {

  formGroup: FormGroup = this.fb.group({
    name: ['', [Validators.required, Validators.maxLength(75), noWhitespaceValidator()]],
    division: ['', Validators.required],
    subDivision: ['', Validators.required],
    policeStation: ['', Validators.required],
    formType: ['', Validators.required],
    colNumber: [1, [Validators.required, Validators.min(1)]],
    cloneFormId: null
  });

  isCloning: boolean = this.data.cloning;

  edit: boolean = false;
  isLoading: boolean = false;

  divisions: any[] = [];
  allSubDivisions: any[] = [];
  filteredSubDivisionsSubject: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);
  filteredSubDivisions = this.filteredSubDivisionsSubject;
  allPoliceStations: any[] = [];
  filteredPoliceStationsSubject: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);
  filteredPoliceStations = this.filteredPoliceStationsSubject;

  subDivisionChacked = false;
  policeStationChacked = false;
  divisionChacked = false;

  formTypeOptions$ = new BehaviorSubject([
    { id: 1, value: 'Letter' },
    { id: 2, value: 'Table' },
    { id: 3, value: 'Card' }
  ]);

  loadingDataForDropdown: boolean = true;

  constructor(
    public dialogRef: MatDialogRef<AddNewFormDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    public languageService: LanguageService,
    private dataService: DataService,
    private router: Router,
    private httpService: HttpService,
    private snackbar: MatSnackBar,
    private snackBarService: SnackBarService,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    if (this.data.templateType) {
      let wantedFormType = this.formTypeOptions$.value.filter(type => type.id == this.data.templateType)[0].value
      console.log(wantedFormType);

      this.formGroup.get('formType').patchValue(wantedFormType, { emitEvent: false });
    }


    this.reactOnChanges();
    this.initializeDataForDialog();
    this.getPrebuiltFormTemplates();
    this.cloningForm();
  }

  cloningForm() {
    if (!this.data.cloning)
      return;

    this.formGroup.get('formType').setValue(this.data.formType);
    this.formGroup.get('cloneFormId').setValue(this.data.cloneFormId);

  }

  reactOnChanges(): void {
    this.formGroup.get(`division`)
      .valueChanges.subscribe(
        value => {
          this.formGroup.get(`subDivision`).setValue(null);
          this.formGroup.get(`policeStation`).setValue(null);
          if (value === null || value === undefined || value === `` || value?.length === 0) {
            this.filteredSubDivisionsSubject.next(null);
            this.filteredPoliceStationsSubject.next(null);
            return;
          }
          this.filteredSubDivisionsSubject.next(
            this.allSubDivisions?.filter(subDivision => value?.map(({ id }) => id)?.includes(subDivision?.division?.id))
          );
        }
      );
    this.formGroup.get(`subDivision`)
      .valueChanges.subscribe(
        value => {
          this.formGroup.get(`policeStation`).setValue(null);
          if (value === null || value === undefined || value === `` || value?.length === 0) {
            this.filteredPoliceStationsSubject.next(null);
            return;
          }
          this.filteredPoliceStationsSubject.next(
            this.allPoliceStations?.filter(policeStations => value?.map(({ id }) => id)?.includes(policeStations?.subDivision?.id))
          );
        }
      );
  }

  initializeDataForDialog(): void {
    var requests = [
      this.httpService.getDivisionsDropdownV1(),
      this.httpService.getSubDivisions(),
      this.httpService.getAllPoliceStationsDropdown()
    ];
    forkJoin(requests).pipe(
      finalize(() => this.loadingDataForDropdown = false)
    ).subscribe(
      responses => {
        if (((responses[0] as any)?.items as any[])?.length > 0)
          this.divisions = [...(responses[0] as any)?.items as any[]];
        if ((responses[1] as any[])?.length > 0)
          this.allSubDivisions = [...responses[1] as any[]].filter(subDivision => subDivision.isActive)
        if (((responses[2] as any)?.data as any[])?.length > 0)
          this.allPoliceStations = [...(responses[2] as any)?.data as any[]].filter(policeStation => policeStation.isActive)
        this.patchInitialValues();
      }
    );
  }

  patchInitialValues(): void {

    if (this.data?.editing || this.data.cloning) {
      const departments = this.data.departments;
      console.log({ departments });


      const divisions = this.getNonDuplicatedArray(departments?.map((x: any) => { return x.parent.parent }));
      const subdivisions = this.getNonDuplicatedArray(departments?.map((x: any) => { return x.parent }));
      const policeStations = this.getNonDuplicatedArray(departments?.map((x: any) => { return x }));
      this.formGroup.patchValue({
        name: this.data?.name,
        formType: this.data?.formType,
        division: divisions
      });
      if (divisions.length === 0) return;
      const filteredSubDivisions = this.allSubDivisions?.filter(
        subDivision => divisions?.map(({ id }) => id)?.includes(subDivision?.division?.id));
      if (filteredSubDivisions.length === 0) return;
      this.filteredSubDivisionsSubject.next(filteredSubDivisions);
      const filteredPoliceStations = this.allPoliceStations?.filter(
        policeStation => filteredSubDivisions?.map(({ id }) => id)?.includes(policeStation?.subDivision?.id));
      if (filteredPoliceStations.length === 0) return;
      this.filteredPoliceStationsSubject.next(filteredPoliceStations);
      this.formGroup.patchValue({
        subDivision: subdivisions,
        policeStation: policeStations
      });
    } else {
      this.formGroup.patchValue({
        division: this.divisions
      }, {
        emitEvent: true
      });
      if (this.divisions.length === 0) return;
      const filteredSubDivisions = this.allSubDivisions?.filter(
        subDivision => this.divisions?.map(({ id }) => id)?.includes(subDivision?.division?.id));
      if (filteredSubDivisions.length === 0) return;
      this.filteredSubDivisionsSubject.next(filteredSubDivisions);
      const filteredPoliceStations = this.allPoliceStations?.filter(
        policeStation => filteredSubDivisions?.map(({ id }) => id)?.includes(policeStation?.subDivision?.id));
      if (filteredPoliceStations.length === 0) return;
      this.filteredPoliceStationsSubject.next(filteredPoliceStations);
      this.formGroup.patchValue({
        subDivision: filteredSubDivisions,
        policeStation: filteredPoliceStations
      });
      this.divisionChacked = true;
      this.subDivisionChacked = true;
      this.policeStationChacked = true;
    }
  }

  getNonDuplicatedArray(array: Array<any>): any[] {
    let retVal = [];

    if (!array) return [];

    for (let item of array) {
      if (!retVal.includes(JSON.stringify(item))) {
        retVal.push(JSON.stringify(item));
      }
    }
    retVal = retVal.map((x: any) => { return JSON.parse(x) });
    return retVal;
  }

  selectComparision(option: any, value: any): boolean {
    if (typeof (option) == `object`) {
      return option?.id == value?.id;
    }
    else return option.toString() == value.toString();
  }

  selectAll(event, controlName, selectRef): void {

    selectRef.close();

    switch (controlName) {
      case `division`:
        event.checked ?
          this.formGroup.get(controlName).setValue(this.divisions) :
          this.formGroup.get(controlName).setValue(null);
        break;
      case `subDivision`:
        event.checked ?
          this.formGroup.get(controlName).setValue(this.filteredSubDivisions?.value) :
          this.formGroup.get(controlName).setValue(null);
        break;
      case `policeStation`:
        event.checked ?
          this.formGroup.get(controlName).setValue(this.filteredPoliceStations?.value) :
          this.formGroup.get(controlName).setValue(null);
        break;
    }
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  onSubmitForm(): void {
    const name = this.formGroup.get(`name`).value;
    const policeStationIds = this.formGroup.get(`policeStation`)?.value?.map(({ id }) => id);
    const formType = this.formGroup.get(`formType`).value;

    const colNumber = this.formGroup.get(`colNumber`).value;
    if (!this.formGroup.valid) return;
    this.isLoading = true;
    if (this.data.editing)
      this.updateTemplate(policeStationIds);
    else
      this.createTemplate(name, policeStationIds, formType, colNumber);
  }

  updateTemplate(policeStationIds: number[] = []): void {
    // ajmo
    this.httpService.templateUpdateDepartment(this.data.id, this.formGroup.get('name').value, policeStationIds.toString()).pipe(
      finalize(() => this.isLoading = false)
    ).subscribe(() => {
      this.dialogRef.close(`edited`);
      this.snackbar.open("Police Station Updated Successfully", `OK`, { duration: 3000 })
    })
  }

  prebuiltTemplates;

  getPrebuiltFormTemplates() {
    this.httpService.getBasicForm().pipe(
      map((response: any[]) => response.filter(template => template.templateType === 'Letter'))
    ).subscribe(response => {

      this.prebuiltTemplates = response;

    });
  }

  createTemplate(name: string, policeStationIds: number[] = [], formType: any, colNumber: any): void {

    if (!this.formGroup.get('cloneFormId').value) {


      this.httpService.templateAddNew(name, policeStationIds.toString(), formType).pipe(
        finalize(() => this.isLoading = false)
      ).subscribe(response => {
        this.dialogRef.close();
        if (formType === `Table`) {
          this.dataService.curFormId = +response;
          this.router.navigate([`/forms`, `create-new-form`], {
            queryParams: { colNumber, formId: response, formName: name },
          })
          return;
        }

        if (formType === 'Letter')
          this.router.navigate([`form-builder`, `create`, response]);

        if (formType === 'Card')
          this.router.navigate(['card-builder', 'create', response]);
      });


    } else {

      this.httpService.templateAddNewClone(name, policeStationIds.toString(), formType, this.formGroup.get('cloneFormId').value).pipe(
        finalize(() => this.isLoading = false)
      ).subscribe(response => {
        this.dialogRef.close();
        this.snackBarService.showMessage(`${name} Clone Form Created Successfully`, 3000)
        if (formType === `Table`) {
          this.dataService.curFormId = +response;
          this.router.navigate([`/forms`, `create-new-form`], {
            queryParams: { colNumber, formId: response, formName: name },
          })
          return;
        }

        if (formType === 'Letter')
          this.router.navigate([`form-builder`, `create`, response]);

        if (formType === 'Card')
          this.router.navigate(['card-builder', 'create', response]);
      });

    }
  }
}