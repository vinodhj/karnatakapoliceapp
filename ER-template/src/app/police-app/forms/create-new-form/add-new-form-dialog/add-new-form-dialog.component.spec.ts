import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddNewFormDialogComponent } from './add-new-form-dialog.component';

describe('AddNewFormDialogComponent', () => {
  let component: AddNewFormDialogComponent;
  let fixture: ComponentFixture<AddNewFormDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddNewFormDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddNewFormDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
