import { CommonModule } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSelectModule } from '@angular/material/select';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatTooltipModule } from '@angular/material/tooltip';
import { IconModule } from '@visurel/iconify-angular';
import { PageLayoutModule } from 'src/@exai/components/page-layout/page-layout.module';
import { ContainerModule } from 'src/@exai/directives/container/container.module';
import { FormBuilderModule } from 'src/app/form-builder/form-builder.module';
import { BasicTableModule } from 'src/app/shared/basic-table/basic-table.module';
import { FormsRoutingModule } from './forms-routing.module';
import { FormsComponent } from './forms.component';
import { SubmitPopupComponent } from './user-filled-form/submit-popup/submit-popup.component';
import { UserFilledFormComponent } from './user-filled-form/user-filled-form.component';
import { AddNewFormComponent } from './add-new-form/add-new-form.component';
import { AdvancedGridItemModule } from 'src/app/shared/advanced-grid-item/advanced-grid-item.module';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatChipsModule } from '@angular/material/chips';
import { AddNewFormDialogComponent } from './create-new-form/add-new-form-dialog/add-new-form-dialog.component';
import { AngularMaterialModel } from 'src/app/shared/angular-material.module';
import { UserFilledFormDialogComponent } from './user-filled-form/user-filled-form-dialog/user-filled-form-dialog.component';
import { EditRecordComponent } from './edit-record/edit-record.component';
import { PreviewRecordComponent } from './preview-record/preview-record.component';
import { FormStatusHistoryModule } from 'src/app/shared/form-status-history/form-status-history.module';
import { FormBuilderSharedModule } from 'src/app/shared/form-status-history/form-builder-shared.module';

import { CreateNewRecordComponent } from './create-new-record/create-new-record.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { FormBuilderIndexeddbService } from 'src/app/form-builder/services/form-builder-indexeddb.service';
import { DocViewerComponent } from './doc-viewer/doc-viewer.component';
import { NgxDocViewerModule } from 'ngx-doc-viewer';
import { MatTabsModule } from '@angular/material/tabs';
import { CommentDialogComponent } from './user-filled-form/dialogs/comment-dialog/comment-dialog.component';
import { NgxPrintModule } from 'ngx-print';
import { NgxMatIntlTelInputModule } from 'ngx-mat-intl-tel-input';
import { OverlayModule } from '@angular/cdk/overlay';
import {PortalModule} from '@angular/cdk/portal';



@NgModule({
  declarations: [
    FormsComponent,
    UserFilledFormComponent,
    SubmitPopupComponent,
    AddNewFormDialogComponent,
    UserFilledFormDialogComponent,
    EditRecordComponent,
    PreviewRecordComponent,
    CreateNewRecordComponent,
    DocViewerComponent,
    CommentDialogComponent
  ],
  exports: [
    FormsComponent
  ],
  imports: [
    CommonModule,
    FormsRoutingModule,
    MatCardModule,
    MatButtonModule,
    FlexLayoutModule,
    PageLayoutModule,
    ContainerModule,
    MatIconModule,
    MatTableModule,
    MatPaginatorModule,
    MatCheckboxModule,
    MatDialogModule,
    MatSelectModule,
    MatSortModule,
    MatMenuModule,
    IconModule,
    MatTooltipModule,
    ReactiveFormsModule,
    MatButtonToggleModule,
    AngularMaterialModel,
    FormsModule,
    MatSlideToggleModule,
    MatInputModule,
    MatProgressSpinnerModule,
    MatTabsModule,
    MatSnackBarModule,
    FormBuilderModule,
    MatChipsModule,
    BasicTableModule,
    AdvancedGridItemModule,
    MatProgressSpinnerModule,
    FormBuilderSharedModule,
    FormStatusHistoryModule,
    DragDropModule,
    NgxDocViewerModule,
    FormBuilderModule,
    NgxPrintModule,
    NgxMatIntlTelInputModule,
    OverlayModule,
    PortalModule
  ],
  providers: [
    FormBuilderIndexeddbService
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ],
})
export class FormModule { }
