import { SelectionModel } from '@angular/cdk/collections';
import { ChangeDetectorRef, Component, Input, NgZone, OnDestroy, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatFormFieldDefaultOptions, MAT_FORM_FIELD_DEFAULT_OPTIONS } from '@angular/material/form-field';
import { MatPaginator } from '@angular/material/paginator';
import { MatSelectChange } from '@angular/material/select';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import moment from 'moment';
import { Observable, ReplaySubject, Subject } from 'rxjs';
import { filter, finalize, takeUntil } from 'rxjs/operators';
import { fadeInUp400ms } from 'src/@exai/animations/fade-in-up.animation';
import { stagger40ms } from 'src/@exai/animations/stagger.animation';
import { TableColumn } from 'src/@exai/interfaces/table-column.interface';
import { HttpService } from 'src/@exai/services/http.service';
import { LanguageService } from 'src/app/language.service';
import { PopUpComponent } from '../../pop-up/pop-up.component';
import { DataService } from '../../services/data-service.service';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { MatChipInputEvent } from '@angular/material/chips';
import { HtmlToDocxService } from 'src/app/shared/services/html-to-docx.service';
import { NotificationsService } from '../../notifications/services/notifications.service';



@UntilDestroy()
@Component({
  selector: 'exai-edit-form',
  templateUrl: './edit-form.component.html',
  styleUrls: ['./edit-form.component.scss'],
  animations: [
    fadeInUp400ms,
    stagger40ms
  ],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        appearance: 'standard'
      } as MatFormFieldDefaultOptions
    }
  ]
})
export class EditFormComponent implements OnDestroy {
  layoutCtrl = new FormControl('boxed');
  subject$: ReplaySubject<IForm[]> = new ReplaySubject<IForm[]>(1);
  data$: Observable<IForm[]> = this.subject$.asObservable();
  customers: IForm[];
  options: Array<Array<any>> = [];

  unsubscribe$ = new Subject();

  @Input()
  columns: TableColumn<IForm>[] = [
    { label: 'Serial NO', property: 'serialNumber', type: 'serialNumber', visible: true, cssClasses: ['font-medium'] },
    { label: 'Form Id', property: 'alternativeId', type: 'text', visible: true, cssClasses: ['font-medium'] },
    { label: 'Form Name', property: 'formName', type: 'text', visible: true, cssClasses: ['font-medium'] },
    { label: 'Form Type', property: 'formType', type: 'text', visible: true, cssClasses: ['font-medium'] },
    { label: 'Status', property: 'formStatus', type: 'text', visible: true, cssClasses: ['text-secondary', 'font-medium'] },
    { label: 'Count', property: 'count', type: 'text', visible: true, cssClasses: ['text-secondary', 'font-medium'] },
    { label: 'Action', property: 'actions', type: 'button', visible: true },
  ];

  dataSource: MatTableDataSource<IForm> | null;
  selection = new SelectionModel<IForm>(true, []);
  searchCtrl = new FormControl('');

  length: number;
  pageSize: 10;
  pageSizeOptions = [5, 10, 50];
  @ViewChild(MatPaginator) paginator: MatPaginator;

  fillForm: FormArray;
  formLabels: Array<any>;
  public static templateId: any;
  getPoliceStation: Object;

  isLoading = true

  constructor(
    private dialog: MatDialog,
    private httpservice: HttpService,
    public activatedRoute: ActivatedRoute,
    private router: Router,
    public dataService: DataService,
    public languageService: LanguageService,
    private htmlToDocxService: HtmlToDocxService,
    private notifications: NotificationsService,
    private cdr: ChangeDetectorRef,
    private ngZone: NgZone
  ) {

    this.formLabels = [
      {
        label: 'Division',
        type: 2,
        level: 1,
        dependentIndex: 1,
        // multiple:false
      },
      {
        label: 'Sub-Division',
        type: 2,
        level: 2,
        dependentIndex: 2,
        // multiple:false
      },
      {
        label: 'Police Station',
        type: 2,
        level: 3,
        dependentIndex: 3,
        // multiple:false
      },
      {
        label: 'Form Name',
        type: 2,
        level: 4,
        dependentIndex: null
      },
    ];

    this.fillForm = new FormArray([
      new FormControl('', [Validators.required]),
      new FormControl('', [Validators.required]),
      new FormControl('', [Validators.required]),
      new FormControl('', [Validators.required]),
    ])

  }

  get visibleColumns() {
    return this.columns.filter(column => column.visible).map(column => column.property);
  }


  getTableData() {

    this.dataService.resetData();

    this.isLoading = true;

    this.httpservice.getUserForm().pipe(
      finalize(() => this.isLoading = false)
    ).subscribe((response: Array<IForm>) => {

      this.isLoading = false;

      response.forEach((x: any, index: number) => {
        x.serialNumber = index + 1
        x['modifiedOn'] = moment(x.modifiedOn).format('L')
        x['createdOn'] = moment(x.createdOn).format('L')
      })
      this.dataSource = new MatTableDataSource(response);
      this.dataSource.paginator = this.paginator;

      // advanced filtering function
      this.dataSource.filterPredicate = (data, filters) => {
        const matchFilter = [];
        const filterArray = filters.split('+');
        const columns = (<any>Object).values(data);
        const columnLabels = [...this.columns.filter(column => column?.visible && !column?.property?.includes(`actions`)).map(({ property }) => property) as string[]];
        filterArray.forEach(filter => {
          let formattedFilter = filter.trim().toLowerCase()
          const customFilter = [];
          columns.forEach(column => {
            this.wantedSearchedValues.forEach(colProperty => {
              if (colProperty == 'all') {
                for (let key in data) {
                  let formattedDataValue = ((('' + data[key]).toLowerCase().replace(/\s+/g, ' ')).trim())
                  if (columnLabels.indexOf(key) >= 0 && (formattedDataValue.includes(formattedFilter) || formattedDataValue == (formattedFilter))) {
                    customFilter.push(true)
                  }
                }
              } else {
                let formattedDataColProperty = (('' + data[colProperty]).toLowerCase()).trim()
                customFilter.push(formattedDataColProperty.includes(formattedFilter))
              }
            })
          });
          matchFilter.push(customFilter.some(Boolean)); // OR
        });
        return matchFilter.every(Boolean); // AND
      }
    });

    this.data$.pipe(
      filter<IForm[]>(Boolean)
    ).subscribe(customers => {
      this.customers = customers;
      this.dataSource.data = customers;
    });
  }

  ngOnInit() {

    this.getTableData();

    this.notifications.getNotificationEvent().pipe(
      takeUntil(this.unsubscribe$)
    ).subscribe(() => {
      this.ngZone.run(() => this.getTableData())
    })
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  camelCaseToNormal(text) {
    const result = text.replace(/([A-Z])/g, " $1");
    const finalResult = result.charAt(0).toUpperCase() + result.slice(1);
    return finalResult
  }

  public getSubDivisions(rowValue) {
    let subDivisions = []
    rowValue.departments.forEach(element => {
      if (element.name === 'Police-Station') {
        subDivisions.push(element.parent.value)
      } else if (element.name === 'Sub-Division') {
        subDivisions.push(element.value)
      }
    })
    return subDivisions.join(', ')
  }

  public getPoliceStations(row) {
    let policeStations = []
    row.departments.forEach(element => {
      if (element.name === 'Police-Station') {
        policeStations.push(element.value)
      }
    })
    return policeStations.join(', ')
  }

  downloadWord(type: string) {
    let formattedTableValues = []

    const matchFields = {
      serialNumber: 'SERIAL NO',
      alternativeId: 'FORM ID',
      formName: 'FORM NAME',
      formType: "FORM TYPE",
      count: "COUNT",
      isActive: "STATUS",
    }

    this.columns.map(column => {

      if (!column.visible) {

        for (const rowKey in matchFields) {

          const valueOfRow = matchFields[rowKey]

          if (valueOfRow.toLowerCase() === column.label.toLowerCase())
            delete matchFields[rowKey];

        }

      }

    })

    //if user filtered data then export only that data else all data
    const dataForExport = this.dataSource.filter ? this.dataSource.connect().value : this.dataSource.data;


    dataForExport.forEach((row: any) => {

      // row.subDivisionName = this.getSubDivisions(row);
      // row.policeStationsName = this.getPoliceStations(row);

      let oneRowInTable = {};

      for (const rowKey in matchFields) {
        const headerName = matchFields[rowKey];

        if (headerName === 'STATUS') {

          oneRowInTable[headerName] = row[rowKey] ? 'Published' : 'Unpublished';

        } else {

          oneRowInTable[headerName] = row[rowKey];

        }

      }

      formattedTableValues.push(oneRowInTable);

    })

    this.htmlToDocxService.downloadBasedOnType(type, formattedTableValues, 'published-forms');
  }


  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.columns, event.previousIndex, event.currentIndex);
  }

  addOnBlur = true;
  searchValues = [];

  wantedSearchedValues = []

  add(event: MatChipInputEvent): void {
    if (event.value) {
      this.searchValues.push(event.value);
    }

    let wantedValues = []
    this.searchValues.forEach(el => {
      if (el.split(' :').length == 1) {
        let textForSearch = el.split(' :')[0];
        this.wantedSearchedValues.push('all')
        wantedValues.push(textForSearch)
      }
      else {
        let value = el.split(' :')[1].trim()
        this.columns.forEach(column => {
          if (column.label === el.split(' :')[0]) {
            this.wantedSearchedValues.push(column.property)
          }
        })
        wantedValues.push(value)
      }
    })
    this.dataSource.filter = wantedValues.join('+')

    // Clear the input value
    event.chipInput!.clear();
  }

  remove(value): void {
    const index = this.searchValues.indexOf(value);
    let dataSourceFilters = this.dataSource.filter.split('+')
    let valueSplitted = value.split(':')

    if (index >= 0) {
      this.searchValues.splice(index, 1);
      if (valueSplitted.length > 1) {
        dataSourceFilters.splice(dataSourceFilters.indexOf(valueSplitted[1].trim()), 1);
      } else {
        dataSourceFilters.splice(dataSourceFilters.indexOf(valueSplitted[0].trim()), 1);
      }
      this.dataSource.filter = dataSourceFilters.join('+')
    }

    if (this.searchValues.length < 1) {
      this.dataSource.filter = ''
    }
  }

  appendToSearch(label) {
    let configuredLabelForSearch = `${label} : `
    this.searchCtrl.setValue(configuredLabelForSearch)
  }

  createForm() {
    let dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.width = '25vw';
    dialogConfig.height = 'min-content';
    dialogConfig.data = {
      labels: this.formLabels,
      controls: this.fillForm
    }

    const dialogRef = this.dialog.open(PopUpComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(result => {
      if (result.value) {

        this.httpservice.templateGetContent(result.value[3].id).subscribe((reponse: any) => {
          reponse.formJSONstring = JSON.parse(reponse.formJSONstring);
          reponse.formEditData = JSON.parse(reponse.formEditData);
          this.dataService.displayIndex = 1;
          this.dataService.formJson = reponse;
          this.dataService.formDetails = result.value;
          this.router.navigate(['add-new-form'])
        });
      }
    });
  }

  openEdit($event: any) {
    console.log($event);
    EditFormComponent.templateId = $event.formId;
    this.dataService.formName = $event.formName;
    this.dataService.formId = $event.formId;

    this.dataService.formDetails = $event;

    this.router.navigate(['/forms/user-filled-form/'])
  }


  deleteCustomer(customer: IForm) {
    this.customers.splice(this.customers.findIndex((existingCustomer) => existingCustomer.id === customer.id), 1);
    this.selection.deselect(customer);
    this.subject$.next(this.customers);
  }

  deleteCustomers(customers: IForm[]) {
    customers.forEach(c => this.deleteCustomer(c));
  }

  onFilterChange(value: string) {
    if (!this.dataSource) {
      return;
    }
    value = value.trim();
    value = value.toLowerCase();
    this.dataSource.filter = value;
  }

  toggleColumnVisibility(event) {
    event.event.stopPropagation();
    event.event.stopImmediatePropagation();
    event.column.visible = !event.column.visible;
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  trackByProperty<T>(index: number, column: TableColumn<T>) {
    return column.property;
  }

  onLabelChange(change: MatSelectChange, row: IForm) {
    const index = this.customers.findIndex(c => c === row);
    this.customers[index].labels = change.value;
    this.subject$.next(this.customers);
  }

}
export class IForm {
  id?: number;
  imageSrc?: string;
  templateName?: string;
  templateType?: string;
  createdBy?: string;
  createdOn?: number;
  modifiedBy?: string;
  modifiedOn?: string;
  status?: string;
  labels?: any;

}
