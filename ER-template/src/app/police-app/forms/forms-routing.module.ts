import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthenticationGuard } from 'src/app/guards/authentication.guard';
import { Role } from 'src/app/shared/enums/role.enum';
import { AddNewFormComponent } from './add-new-form/add-new-form.component';
import { CreateNewFormComponent } from './create-new-form/create-new-form.component';
import { CreateNewRecordComponent } from './create-new-record/create-new-record.component';
import { DocViewerComponent } from './doc-viewer/doc-viewer.component';
import { EditFormComponent } from './edit-form/edit-form.component';
import { EditRecordComponent } from './edit-record/edit-record.component';
import { FormsComponent } from './forms.component';
import { PreviewRecordComponent } from './preview-record/preview-record.component';
import { UserFilledFormComponent } from './user-filled-form/user-filled-form.component';

const routes: Routes = [
  {
    path: ``,
    component: FormsComponent,
    children: [
      {
        path: `config-form`,
        component: CreateNewFormComponent,
        canActivate: [AuthenticationGuard],
        data: {
          roles: [
            Role.SuperAdmin,
            Role.Admin,
            Role.StateHead,
            Role.DistrictHead,
            Role.DivisionHead,
            Role.SubDivision,
            Role.PoliceStation
          ]
        }
      },
      {
        path: `submitted-form`,
        component: EditFormComponent,
        canActivate: [AuthenticationGuard],
        data: {
          roles: [
            Role.SuperAdmin,
            Role.Admin,
            Role.StateHead,
            Role.DistrictHead,
            Role.DivisionHead,
            Role.SubDivision,
            Role.PoliceStation,
            Role.User
          ]
        }
      },
      {
        path: `user-filled-form`,
        component: UserFilledFormComponent,
        canActivate: [AuthenticationGuard],
        data: {
          roles: [
            Role.SuperAdmin,
            Role.Admin,
            Role.StateHead,
            Role.DistrictHead,
            Role.DivisionHead,
            Role.SubDivision,
            Role.PoliceStation,
            Role.User
          ]
        }
      },
      {
        path: `create-new-form`,
        component: AddNewFormComponent,
        canActivate: [AuthenticationGuard],
        data: {
          roles: [
            Role.SuperAdmin,
            Role.Admin,
            Role.StateHead,
            Role.DistrictHead,
            Role.DivisionHead,
            Role.SubDivision,
            Role.PoliceStation,
            Role.User
          ]
        }
      },
      {
        path: `edit-record`,
        component: EditRecordComponent,
        canActivate: [AuthenticationGuard],
        data: {
          roles: [
            Role.SuperAdmin,
            Role.Admin,
            Role.StateHead,
            Role.DistrictHead,
            Role.DivisionHead,
            Role.SubDivision,
            Role.PoliceStation,
            Role.User
          ]
        }
      },
      {
        path: `preview-record`,
        component: PreviewRecordComponent,
        canActivate: [AuthenticationGuard],
        data: {
          roles: [
            Role.SuperAdmin,
            Role.Admin,
            Role.StateHead,
            Role.DistrictHead,
            Role.DivisionHead,
            Role.SubDivision,
            Role.PoliceStation,
            Role.User
          ]
        }
      },
      {
        path: `create-new-record`,
        component: CreateNewRecordComponent,
        canActivate: [AuthenticationGuard],
        data: {
          roles: [
            Role.SuperAdmin,
            Role.Admin,
            Role.StateHead,
            Role.DistrictHead,
            Role.DivisionHead,
            Role.SubDivision,
            Role.PoliceStation,
            Role.User
          ]
        }
      },
      {
        path: `doc-viewer`,
        component: DocViewerComponent,
        canActivate: [AuthenticationGuard],
        data: {
          roles: [
            Role.SuperAdmin,
            Role.Admin,
            Role.StateHead,
            Role.DistrictHead,
            Role.DivisionHead,
            Role.SubDivision,
            Role.PoliceStation,
            Role.User
          ]
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class FormsRoutingModule { }
