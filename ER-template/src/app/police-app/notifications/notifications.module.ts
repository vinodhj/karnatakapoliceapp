import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotificationsDialogComponent } from './notifications-dialog/notifications-dialog.component';
import { MatIconModule } from '@angular/material/icon';
import { InfiniteScrollModule } from "ngx-infinite-scroll";
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { NotificationsButtonComponent } from './notifications-button/notifications-button.component';



@NgModule({
  declarations: [
    NotificationsDialogComponent,
    NotificationsButtonComponent
  ],
  imports: [
    CommonModule,
    MatIconModule,
    InfiniteScrollModule,
    MatProgressBarModule
  ],
  exports: [
    NotificationsDialogComponent,
    InfiniteScrollModule,
    MatProgressBarModule,
    NotificationsButtonComponent
  ]
})
export class NotificationsModule { }
