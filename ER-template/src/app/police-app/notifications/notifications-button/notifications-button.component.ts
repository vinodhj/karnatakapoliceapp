import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Subject } from 'rxjs';
import { switchMap, takeUntil } from 'rxjs/operators';
import { PopoverService } from 'src/@exai/components/popover/popover.service';
import { HttpService } from 'src/@exai/services/http.service';
import { NotificationsDialogComponent } from '../notifications-dialog/notifications-dialog.component';
import { NotificationsService } from '../services/notifications.service';

declare var Howl;

@Component({
  selector: 'exai-notifications-button',
  templateUrl: './notifications-button.component.html',
  styleUrls: ['./notifications-button.component.scss']
})
export class NotificationsButtonComponent implements OnInit, OnDestroy {
  dropdownOpen: boolean;

  notificationsCount: number = 0;

  newNotification: boolean = false;

  unsubscribe$ = new Subject();

  changeTitleInterval;

  sound = new Howl({
    src: ['assets/sounds/mixkit-positive-notification-951.wav'],
    volume: 0.2
  });

  constructor(    private popover: PopoverService,
    private cd: ChangeDetectorRef, private titleService: Title, private http: HttpService, private notificationsService: NotificationsService) { }

    checkForUnreadNotifications() {

      this.http.getNotifications().subscribe((notification: any) => {

        this.notificationsCount = notification.numberOfUnreadNotifications;

        notification.items.map(item => {

          if(!item.read) 
          this.newNotification = true;

        })

      });

    }

  ngOnInit(): void {

    this.checkForUnreadNotifications();

    this.notificationsService.getReadAllNotifications().pipe(
      switchMap(() => this.http.markNotificationsAsRead()),
      takeUntil(this.unsubscribe$)
    ).subscribe(() => {

      this.openNotificationsDialog();
      
    });

    this.notificationsService.getNotificationEvent().pipe(
      takeUntil(this.unsubscribe$)
    ).subscribe((notification: any) => {

      this.sound.play();

      this.newNotification = true;

      this.notificationsCount = JSON.parse(notification).numberOfUnreadNotifications;

      this.changeTitles();

      this.cd.detectChanges();      
    });

  }


  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  changeTitles() {

    let changeTitle = false;

    this.changeTitleInterval = setInterval(() => {

      if(changeTitle) {
        this.titleService.setTitle('New Notification');

        changeTitle = false;
      } else {
        this.titleService.setTitle('Karnataka State Police');
        changeTitle = true;
      }


    }, 1500);

  }

  openNotificationsDialog() {
  
    this.newNotification = false;
    clearInterval(this.changeTitleInterval);
    
    this.titleService.setTitle('Karnataka State Police');
  }

  showNotificationPopover(originRef: HTMLElement) {

    this.openNotificationsDialog();

    this.dropdownOpen = true;
    this.cd.markForCheck();

    const popoverRef = this.popover.open({
      content: NotificationsDialogComponent,
      origin: originRef,
      offsetY: 12,
      position: [
        {
          originX: 'center',
          originY: 'top',
          overlayX: 'center',
          overlayY: 'bottom'
        },
        {
          originX: 'end',
          originY: 'bottom',
          overlayX: 'end',
          overlayY: 'top',
        },
      ]
    });

    popoverRef.afterClosed$.subscribe(() => {
      this.dropdownOpen = false;
      this.cd.markForCheck();
    });
  }

}
