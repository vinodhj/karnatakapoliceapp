import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { combineLatest, of, Subject } from 'rxjs';
import { concatMap, finalize, map, mergeMap, switchMap, takeUntil } from 'rxjs/operators';
import { HttpService } from 'src/@exai/services/http.service';
import { NotificationsService } from '../services/notifications.service';

@Component({
  selector: 'exai-notifications-dialog',
  templateUrl: './notifications-dialog.component.html',
  styleUrls: ['./notifications-dialog.component.scss']
})
export class NotificationsDialogComponent implements OnInit, OnDestroy {
  loadingNotifications: boolean;
  notifications = [];
  stopLoading: boolean;

  pageNumber = 1;
  initialLoad: boolean = true;

  unsubscribe$ = new Subject();

  constructor(private http: HttpService, private notificationsService: NotificationsService, private cdr: ChangeDetectorRef) { }

  ngOnInit(): void {
    this.getNotifications();

    this.notificationsService.getNotificationEvent().pipe(
      takeUntil(this.unsubscribe$)
    ).subscribe((notification: string) => {

      let notificationParsed = JSON.parse(notification);

      notificationParsed.message = JSON.parse(notificationParsed.notification.message);

      this.notifications.unshift(notificationParsed);

      this.cdr.detectChanges();

    })
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();

    this.notificationsService.notificationReadEvent$.next();
  }

  onScroll() {
    this.getNotifications();

  }

  getNotifications() {
    
    if(this.stopLoading)
      return;

    this.loadingNotifications = true;

    this.http.getNotifications(this.pageNumber).pipe(
      mergeMap(response => {

        return this.initialLoad ? this.http.markNotificationsAsRead().pipe(
          map(() => response)) 
        : of(response)
      }),
      finalize(() => this.loadingNotifications = false)
    ).subscribe(result => {

      if(result.items.length)
        this.notifications.push(...result.items);

      if(this.notifications.length == result.totalResultsCount)
        this.stopLoading = true;

      this.initialLoad = false;

    });

    this.pageNumber++;
  }

}
