import { Injectable } from '@angular/core';
import * as signalR from "@microsoft/signalr";  // or from "@microsoft/signalr" if you are using a new library
import { Subject } from 'rxjs';
import { HttpService } from 'src/@exai/services/http.service';
import { safeParseJsonString } from '../../utils/utils';
import { NotificationsService } from './notifications.service';

@Injectable({
  providedIn: 'root'
})
export class SignalrService {

  constructor(
    private httpService: HttpService,
    private notifications: NotificationsService
  ) { }

  options = {
    transport: signalR.HttpTransportType.ServerSentEvents,
    logging: signalR.LogLevel.Trace,
    accessTokenFactory: () => this.httpService.token
  };

  private hubConnection: signalR.HubConnection


  public invokeSignal(signal: string, data: any) {
    this.hubConnection.invoke(signal, data);
  }

  public getHubConnection() {
    return this.hubConnection;
  }

  public startConnection = () => {
    this.hubConnection = new signalR.HubConnectionBuilder()
      .configureLogging(signalR.LogLevel.Debug)
      // https://jsk1gc4ms9.execute-api.us-east-2.amazonaws.com/Prod/notifications'
      // https://localhost:5001/notifications
      // https://policeapp.examroom.ai:4431/notifications
      .withUrl('https://policeapp.examroom.ai:4431/notifications', this.options)
      .withAutomaticReconnect()
      .build();

    this.hubConnection
      .start()
      .then(() => console.log('Connection started'))
      .catch(err => console.log('Error while starting connection: ' + err));

    this.hubConnection.on(`PublishTemplate`, data => this.notifications.notificationEvent$.next(data));
  }
}
