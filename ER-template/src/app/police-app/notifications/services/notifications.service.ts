import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NotificationsService {

  notificationEvent$ = new Subject();
  notificationReadEvent$ = new Subject();

  constructor() { }

  getNotificationEvent() {
    return this.notificationEvent$.asObservable();
  }

  getReadAllNotifications() {
   return this.notificationReadEvent$.asObservable();
  }
}
