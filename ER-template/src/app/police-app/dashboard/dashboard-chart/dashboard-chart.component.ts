import { DOCUMENT } from '@angular/common';
import { AfterViewInit, ChangeDetectorRef, Component, ComponentFactoryResolver, Inject, Input, NgZone, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { BaseChartDirective, Color } from 'ng2-charts';
import { DashboardTooltipComponent } from '../dashboard-tooltip/dashboard-tooltip.component';

@Component({
  selector: 'exai-dashboard-chart',
  templateUrl: './dashboard-chart.component.html',
  styleUrls: ['./dashboard-chart.component.scss']
})
export class DashboardChartComponent implements OnInit, AfterViewInit {
  @ViewChild(BaseChartDirective) private _chart: BaseChartDirective;

  @Input() chartOptions
  @Input() chartType
  @Input() chartId
  @Input() chartTitle
  @Input() linkTo

  public doughnutChartColors: Color[] = [{
    backgroundColor: ['#1a1e63', '#FF8DAF', '#6D7CF2', '#EDD2FB', '#FFEEDD', '#F5DAD6', '#C9FFEA', '#BABBFF', '#9E9CFE']
   }];

  options: any = {
    maintainAspectRatio: false,
    responsive: true,
    legend: false
  }

  constructor(private vcRef: ViewContainerRef, private resolver: ComponentFactoryResolver, @Inject(DOCUMENT) private document: any, private cdr: ChangeDetectorRef, private ngZone: NgZone) { }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {

    if(this.chartOptions?.labels && this.chartOptions?.labels.length) {


    this.document.getElementById(this.chartId).innerHTML = (this._chart.chart.generateLegend() as any);

    this.document.getElementById(this.chartId).querySelectorAll('li').forEach(li => {
      const backgroundColor = li.querySelector('span').style.backgroundColor;
      const text = li.textContent;

      const compFactory = this.resolver.resolveComponentFactory(DashboardTooltipComponent);
      const componentRef = this.vcRef.createComponent(compFactory);

      componentRef.instance.setValues(text, 'background-color: ' + backgroundColor);


      li.replaceWith(componentRef.location.nativeElement);
    })

    
  }
  }

  componentRef;

}
