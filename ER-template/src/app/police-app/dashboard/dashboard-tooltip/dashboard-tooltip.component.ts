import { ChangeDetectorRef, Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';

@Component({
  selector: 'exai-dashboard-tooltip',
  templateUrl: './dashboard-tooltip.component.html',
  styleUrls: ['./dashboard-tooltip.component.scss']
})
export class DashboardTooltipComponent implements OnInit {
  @Input() text;
  @Input() backgroundColor;

  constructor(private cdr: ChangeDetectorRef) { }

  setValues(text, backgroundColor) {
    this.text = text;
    this.backgroundColor = backgroundColor;

    this.cdr.detectChanges();
  }

  ngOnInit(): void {
  }

}
