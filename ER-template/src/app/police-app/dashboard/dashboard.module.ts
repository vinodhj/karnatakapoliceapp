import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ChartsModule } from 'ng2-charts';


@NgModule({
  declarations: [
  ],
  imports: [
    CommonModule, 
    BrowserModule, 
    ChartsModule,
    
  ],
})
export class DashboardModule { }
