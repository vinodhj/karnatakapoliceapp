import { AfterViewInit, ChangeDetectorRef, Component, Inject, NgModule, OnInit, ViewChild } from '@angular/core';
import { ChartComponent } from "ng-apexcharts";
import { HttpService } from 'src/@exai/services/http.service';
import { LanguageService } from 'src/app/language.service';
import { BarChartOptions } from './interfaces/bar-chart-options';
import { ChartOptions } from './interfaces/chart-options';
import { BaseChartDirective, ChartsModule, Color } from 'ng2-charts';
import { ChartDataSets, ChartType } from 'chart.js';
import { SingleDataSet, Label, monkeyPatchChartJsLegend, monkeyPatchChartJsTooltip } from 'ng2-charts';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { DOCUMENT } from '@angular/common';
import Chart from 'chart.js';
import { drawRoundedEdges } from '../utils/draw-rounded-edges-bars';
import { finalize } from 'rxjs/operators';



@Component({
  selector: 'exai-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, AfterViewInit {

  @ViewChild(BaseChartDirective) private _chart: BaseChartDirective;

  isLoading = true;

  public chartOptionsDiv: Partial<ChartOptions>;
  public chartOptionsSubDiv: Partial<ChartOptions>;
  public chartOptionsPolStation: Partial<ChartOptions>;

  public doughnutChartType = 'doughnut';
  public barChartType = 'bar';
  public barChartLegend = true;
  public barChartPlugins = [];

  public barChartOptions: any = {
    responsive: true,
    title: { display: false },
    legend: false,
    maintainAspectRatio: false,
    // @ts-ignore
    isDoubleSideRounded: false,
    scales: {
      xAxes: [{
        stacked: true,
        ticks: {
          fontColor: 'rgb(117, 117, 117)',  // x axe labels (can be hexadecimal too)
        },
        gridLines: {
          color: '#f9f9f9'  // grid line color (can be removed or changed)
        }
      }],
      yAxes: [{
        stacked: true,
        ticks: {
          fontColor: 'rgb(117, 117, 117)',  // y axes numbers color (can be hexadecimal too)
          min: 0,
          beginAtZero: true,
          userCallback: function(label, index, labels) {
            // when the floored value is the same as the value we have a whole number
            if (Math.floor(label) === label) {
                return label;
            }

        },
        },
        gridLines: {
          color: '#f9f9f9'  // grid line color (can be removed or changed)
        },
      }]
    },
  };
  public barChartLabels: Label[] = ['Created', 'Published', 'Unpublished', 'Submitted', 'Approved', 'Rejected'];

  public barChartData: ChartDataSets[] = [
    { data: [] }
  ];

  ngOnInit(): void {
  }

  public colors: Color[] = [
    {
      backgroundColor: "#6d7cf29d",
      borderColor: "#fff",
      borderWidth: 2,
      hoverBackgroundColor: "#6d7cf29d",
      hoverBorderColor: "#0290FB",
    },
    // TODO: add more colors as needed
  ];

  options: any = {
    divisions: {
      title: {
        display: true,
        text: 'Divisions'
      },
      legend: { display: false },
      responsive: true
    },
    subdivisions: {
      title: {
        display: true,
        text: 'Sub-Divisions'
      },
      legend: { display: false },
      responsive: true
    },
    policestations: {
      title: {
        display: true,
        text: 'Police Stations'
      },
      legend: { display: false },
      responsive: true
    }
  }

  chartColorsAdd = [];

  ngAfterViewInit(): void {
    this.http.getDepart().pipe(
      finalize(() => this.isLoading = false)
    ).subscribe((data: any) => {
      this.chartOptionsDiv = {
        series: (data.Divisions as Array<any>).map((x: any) => { return x.count }) as Array<number>,
        labels: (data.Divisions as Array<any>).map((x: any) => { return x.department }) as Array<string>
      }
      this.chartOptionsSubDiv = {
        series: (data.SubDivisions as Array<any>).map((x: any) => { return x.count }) as Array<number>,
        labels: (data.SubDivisions as Array<any>).map((x: any) => { return x.department }) as Array<string>
      }
      this.chartOptionsPolStation = {
        series: (data.PoliceStations as Array<any>).map((x: any) => { return x.count }) as Array<number>,
        labels: (data.PoliceStations as Array<any>).map((x: any) => { return x.department }) as Array<string>
      }

      this.cdr.detectChanges();
    })


    this.http.getForm().subscribe((data: any) => {

      this.barChartData = [
        { data: Object.values(data) as Array<any>, barThickness: 50,  }
      ];

    })

  }

  constructor(
    public languageService: LanguageService,
    private http: HttpService,
    private cdr: ChangeDetectorRef,
    @Inject(DOCUMENT) private document: Document
  ) { 
    // Chart.defaults.global.defaultFontFamily = "'Press Start 2P', cursive";
    Chart.elements.Rectangle.prototype.draw = drawRoundedEdges;
  }

}

