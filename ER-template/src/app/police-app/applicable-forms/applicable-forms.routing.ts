import { ModuleWithProviders } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CreateNewFormComponent } from "../forms/create-new-form/create-new-form.component";
import { ApplicableFormsComponent } from "./applicable-forms.component";

const routes: Routes = [
    { path: '', component: ApplicableFormsComponent,
    children: [
        { path: "config-form", component: CreateNewFormComponent},
        { path: "**", pathMatch: "full", redirectTo:"config-form"},
        
    ]
 }
];

export const routing: ModuleWithProviders<RouterModule> = RouterModule.forChild(routes);