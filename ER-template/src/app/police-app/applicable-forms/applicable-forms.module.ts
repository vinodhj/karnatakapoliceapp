import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FlexLayoutModule } from "@angular/flex-layout";

import { PageLayoutModule } from "src/@exai/components/page-layout/page-layout.module";
import { ContainerModule } from "src/@exai/directives/container/container.module";
import { ApplicableFormsComponent } from "./applicable-forms.component";
import { routing } from "./applicable-forms.routing";

@NgModule({
    declarations: [
        ApplicableFormsComponent
    ],
    imports: [
        CommonModule,
        routing, 
        FlexLayoutModule,
        PageLayoutModule,
        ContainerModule,
    ]
})
export class ApplicationFormsModule { }