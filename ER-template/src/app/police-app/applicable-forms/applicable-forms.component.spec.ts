import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicableFormsComponent } from './applicable-forms.component';

describe('ApplicableFormsComponent', () => {
  let component: ApplicableFormsComponent;
  let fixture: ComponentFixture<ApplicableFormsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ApplicableFormsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicableFormsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
