import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import icMail from '@iconify/icons-ic/twotone-mail';
import { HttpService } from 'src/@exai/services/http.service';
import { NavigationService } from 'src/@exai/services/navigation.service';
import { ConfigService } from 'src/@exai/services/config.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { HttpErrorResponse } from '@angular/common/http';
import { LanguageService } from 'src/app/language.service';
import { catchError } from 'rxjs/operators';
import { of, throwError } from 'rxjs';

@Component({
  selector: 'exai-otp-verify',
  templateUrl: './otp-verify.component.html',
  styleUrls: ['./otp-verify.component.scss']
})
export class OtpVerifyComponent implements OnInit {
  form:FormGroup
 

  icMail = icMail;

  
  keyPressNumbers(event) {
    var charCode = (event.which) ? event.which : event.keyCode;
    // Only Numbers 0-9
    if ((charCode < 48 || charCode > 57)) {
      event.preventDefault();
      return false;
    } else {
      return true;
    }
  }
  constructor(
    private router: Router,
    private fb: FormBuilder,
    private services: HttpService,
    private configService: ConfigService,
    private navigationService: NavigationService,
    private _snackBar: MatSnackBar,
    public languageService: LanguageService
  ) { 
    
  }

  ngOnInit() {
    this.form = new FormGroup({
      otp: new FormControl('', [Validators.required, Validators.minLength(6)])
    })
  }

  // send() {
  //   this.router.navigate(['/login/set-password']);
  // }
  otpVerify(value) {
    
    // this._snackBar.open('otp Verified! ', '', {
    //   duration: 4000,
    // })
    // this.router.navigateByUrl('/login/set-password')
    let otp={
      userId:  this.services.tempId,
        otp:value.otp
      }
  
    this.services.otpVerify(otp).subscribe((data :any)=>{

      if(data){
        
        this.router.navigateByUrl('/login/set-password')
        this._snackBar.open('OTP verified ', '', {
          duration: 4000,
        })
       }
       else if(!data){
         
        this.router.navigateByUrl('/login/forgot-password')
        this._snackBar.open('OTP not matching ', '', {
          duration: 4000,
        })
       }
       err=>{
        console.log(err)
        this._snackBar.open('OTP not matching ', '', {
          duration: 4000,
        })
    }
     },
     (error)=>{

      this._snackBar.open( `${error?.error?.Message}`, '', {
        duration: 4000,
      }) 
      this.router.navigateByUrl('/login/forgot-password')
     })
   }
}