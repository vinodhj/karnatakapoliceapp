import { Component, OnInit } from '@angular/core';
// import { Component, OnInit } from '@angular/core';
import { ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import icVisibility from '@iconify/icons-ic/twotone-visibility';
import icVisibilityOff from '@iconify/icons-ic/twotone-visibility-off';
import { HttpService } from 'src/@exai/services/http.service';
import { NavigationService } from 'src/@exai/services/navigation.service';
import { ConfigService } from 'src/@exai/services/config.service';
import { LanguageService } from 'src/app/language.service';

@Component({
  selector: 'exai-set-password',
  templateUrl: './set-password.component.html',
  styleUrls: ['./set-password.component.scss']
})
export class SetPasswordComponent implements OnInit {
  form: FormGroup;
  password: AbstractControl;
  _password: AbstractControl;

  inputType = 'password';
  visible = false;

  icVisibility = icVisibility;
  icVisibilityOff = icVisibilityOff;

  constructor(private router: Router,
    private fb: FormBuilder,
    private cd: ChangeDetectorRef,
    private _snackBar: MatSnackBar,
    private services: HttpService,
    public languageService: LanguageService
  ) { }

  ngOnInit() {
    this.form = this.fb.group({
      password: ['', Validators.compose([Validators.required, Validators.minLength(4)])],
      _password: ['', Validators.compose([Validators.required, Validators.minLength(4)])],
    }, { validator: this.passwordMatchValidator('password', '_password') });

    this.password = this.form.controls['password'];
    this._password = this.form.controls['_password'];
  }

  toggleVisibility() {
    if (this.visible) {
      this.inputType = 'password';
      this.visible = false;
      this.cd.markForCheck();
    } else {
      this.inputType = 'text';
      this.visible = true;
      this.cd.markForCheck();
    }
  }
  public get confirmPasswordMismatch() {
    return (this.form.get('password').dirty || this.form.get('confirmPassword').dirty) && this.form.hasError('confirmedDoesNotMatch');
  }

  updatePassword(value: any) {
    if (this.password.value != this._password.value) {
      return this._snackBar.open("Password doesn't match", '', {
        duration: 3000,
        // horizontalPosition: 'right'
      });
    }
    let updatePwd = {
      id: this.services.tempId,
      newPassword: this.form.value.password,
    };
    console.log(updatePwd);

    this.services.setPassword(updatePwd).subscribe(data => {
      console.log(data)
      if (data) {
        this.router.navigateByUrl('login')
        this._snackBar.open("Password Updated Successfully", '', {
          duration: 3000,
          // horizontalPosition: 'right'
        })
      }
      else {
        this._snackBar.open("Password doesn't match", '', {
          duration: 3000,
          // horizontalPosition: 'right'
        })
      }
      err => {
        console.log(err)
        this._snackBar.open("Error Message", err, {
          duration: 3000,
          // horizontalPosition: 'right'
        })
      }
    })
  }

  passwordMatchValidator(controlName: string, matchingControlName: string) {
    return async (formGroup: FormGroup) => {
      const control = formGroup.controls[controlName];
      const matchingControl = formGroup.controls[matchingControlName];
      // return if another validator has already found an error on the matchingControl
      if (matchingControl.errors && !matchingControl.errors.mustMatch) return;
      return control.value != matchingControl.value ? matchingControl.setErrors({ mustMatch: true }) : matchingControl.setErrors(null);
    }
  }
}
