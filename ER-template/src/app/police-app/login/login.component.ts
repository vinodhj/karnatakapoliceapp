// import { Component, OnInit } from '@angular/core';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
// import { GlobalUserServiceService } from '../core-service/global-user-service.service';
import { HttpService } from 'src/@exai/services/http.service';
import { NavigationService } from 'src/@exai/services/navigation.service';
import { LanguageService } from 'src/app/language.service';
import { Role } from 'src/app/shared/enums/role.enum';
import { SnackBarService } from 'src/app/shared/services/snack-bar.service';
import { isNullNumericIdFromSessionStorage } from 'src/app/utils/randomNumber';

@Component({
  selector: 'exai-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {
  form: FormGroup;
  email: AbstractControl;
  password: AbstractControl;

  inputType = 'password';
  visible = false;

  isLoading = false

  public static loginDetails;

  constructor(private router: Router,
    private fb: FormBuilder,
    private changeDetection: ChangeDetectorRef,
    private snackbarService: SnackBarService,
    private services: HttpService,
    private _navigationService: NavigationService,
    public languageService: LanguageService
  ) { }

  ngOnInit() {
    document.body.classList.add('bg-pattern');

    this.form = this.fb.group({
      userName: ['', Validators.compose([Validators.required])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(6), Validators.maxLength(25)])]
    });
  }

  toggleVisibility() {
    if (this.visible) {
      this.inputType = 'password';
      this.visible = false;
      this.changeDetection.markForCheck();
    } else {
      this.inputType = 'text';
      this.visible = true;
      this.changeDetection.markForCheck();
    }
  }
  login() {
    let loginDetails = {
      username: this.form.value.userName,
      password: this.form.value.password,
    };
    this.isLoading = true
    this.services.login(loginDetails).subscribe((data: any) => {
      console.log(data);
      this.isLoading = false
      LoginComponent.loginDetails = data;
      this.services.token = data.token;
      this.services.name = data.firstname + ' ' + data.lastname;
      this.services.fname = data.firstname;
      this.services.lname = data.lastname;
      this.services.roleId = data.roleId;
      this.services.id = data.id;
      this.services.userId = data.userId;
      this.services.emailId = data.emailId;
      this.services.phone = data.phone;
      this.services.roleName = data.roleName;
      this.services.currentUsername = data.username;
      console.log(data);
      // this.services.userToDepartmentMapping = data.userToDepartmentMapping[0]?.policeStation

      //#region UserData
      this.services.branchId = isNullNumericIdFromSessionStorage(data.branchId) ? null : Number(data.branchId);
      this.services.subDivisionId = isNullNumericIdFromSessionStorage(data.subDivisionId) ? null : Number(data.subDivisionId);
      this.services.divisionId = isNullNumericIdFromSessionStorage(data.divisionId) ? null : Number(data.divisionId);
      //#endregion UserData

      this.services.setTokenIfAvailable();

      this.handleRoles(data?.roleId);

    },
      (error) => {
        this.isLoading = false
        this.snackbarService.showMessage('Invalid Credentials', 1000)
        return;
      });
  }

  handleRoles(roleId: number = null): void {
    if (!(roleId in Role))
      this.handleUndefinedRole();
    switch (roleId) {
      case Role.SuperAdmin:
      case Role.Admin:
      case Role.StateHead:
      case Role.DistrictHead:
      case Role.DivisionHead:
      case Role.SubDivision:
      case Role.PoliceStation:
      case Role.User: {
        this._navigationService.setNavigationItems(roleId);
        this.navigateToDashboard();
        break;
      }
      default:
        this.handleUndefinedRole();
        break;
    }
  }

  navigateToDashboard(): void {
    this.router.navigateByUrl('/dashboard');
    this.snackbarService.showMessage('Logged in Successfully!', 4000)
  }

  navigateToConfigForm(): void {
    this.router.navigateByUrl('/applicable-forms/config-form');
    this.snackbarService.showMessage('Logged in Successfully!', 4000);
  }

  handleUndefinedRole(): void {
    this.snackbarService.showMessage('User role does not exists!', 2000);
  }
}