import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import icMail from '@iconify/icons-ic/twotone-mail';
import { HttpService } from 'src/@exai/services/http.service';
import { NavigationService } from 'src/@exai/services/navigation.service';
import { ConfigService } from 'src/@exai/services/config.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { HttpErrorResponse } from '@angular/common/http';
import { LanguageService } from 'src/app/language.service';

@Component({
  selector: 'exai-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss'],
})
export class ForgotPasswordComponent implements OnInit {

  form = this.fb.group({
    username: [null, Validators.required]
  });

  icMail = icMail;

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private services: HttpService,
    private configService: ConfigService,
    private navigationService: NavigationService,
    private _snackBar: MatSnackBar,
    public languageService: LanguageService
  ) { }

  ngOnInit() {
  }

  forgotPwd() {
    let forgot = {
      username: this.form.value.username,
      // id: 4
    };

    this.services.forgotPwd(this.form.value.username).subscribe((data: any) => {
      console.log(data);
      this.services.tempId = data.id;
      if (data) {
        
        this.router.navigateByUrl('/login/otp-verify'),
        this._snackBar.open('OTP sent! ', '', {
          duration: 4000,
        })
      }
      else{
        

      }
    },
      error => {
        
        this._snackBar.open('Invalid Username!', " ", {
          duration: 1300,
        })
        return;
      });
      }
    }


