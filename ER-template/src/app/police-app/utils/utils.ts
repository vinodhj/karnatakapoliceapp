import moment from "moment";

/**
 * generate groups of 4 random characters
 * @example getUniqueId(1) : 607f
 * @example getUniqueId(2) : 95ca-361a-f8a1-1e73
 */
export function getUniqueId(parts: number): string {
  const stringArr = [];
  for (let i = 0; i < parts; i++) {
    // tslint:disable-next-line:no-bitwise
    const S4 = (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    stringArr.push(S4);
  }
  return stringArr.join('-');
}

export function getNonDuplicatedArray(array: Array<any>) {
  let retVal = [];
  for (let item of array) {
    if (!retVal.includes(JSON.stringify(item))) {
      retVal.push(JSON.stringify(item));
    }
  }
  retVal = retVal.map((x: any) => { return JSON.parse(x) });
  return retVal;
}

export function dateIsOneDay(value: any): boolean {
  return (isValidDate(value.dateFrom) ? new Date(
    new Date(value.dateFrom).getFullYear(),
    new Date(value.dateFrom).getMonth(),
    new Date(value.dateFrom).getDate()).getTime() === new Date(
      new Date().getFullYear(),
      new Date().getMonth(),
      new Date().getDate()).getTime() : false) &&
    (isValidDate(value.dateTo) ? new Date(
      new Date(value.dateTo).getFullYear(),
      new Date(value.dateTo).getMonth(),
      new Date(value.dateTo).getDate()).getTime() === new Date(
        new Date().getFullYear(),
        new Date().getMonth(),
        new Date().getDate()).getTime() : false);
}

export function dateIsOneWeek(value: any): boolean {
  return isValidDate(value.dateFrom) && isValidDate(value.dateTo) ? new Date(
    new Date(value.dateFrom).getFullYear(),
    new Date(value.dateFrom).getMonth(),
    new Date(value.dateFrom).getDate()).getTime() === new Date(
      new Date(moment().startOf('isoWeek').toDate()).getFullYear(),
      new Date(moment().startOf('isoWeek').toDate()).getMonth(),
      new Date(moment().startOf('isoWeek').toDate()).getDate()).getTime() : false;
}

export function dateIsOneMonth(value: any): boolean {
  return isValidDate(value.dateFrom) && isValidDate(value.dateTo) ? new Date(
    new Date(value.dateFrom).getFullYear(),
    new Date(value.dateFrom).getMonth(),
    new Date(value.dateFrom).getDate()).getTime() === new Date(
      new Date(moment().startOf('month').toDate()).getFullYear(),
      new Date(moment().startOf('month').toDate()).getMonth(),
      new Date(moment().startOf('month').toDate()).getDate()).getTime() : false;
}

export function dateIsOneYear(value: any): boolean {
  return isValidDate(value.dateFrom) && isValidDate(value.dateTo) ? new Date(
    new Date(value.dateFrom).getFullYear(),
    new Date(value.dateFrom).getMonth(),
    new Date(value.dateFrom).getDate()).getTime() === new Date(
      new Date(moment().startOf('year').toDate()).getFullYear(),
      new Date(moment().startOf('year').toDate()).getMonth(),
      new Date(moment().startOf('year').toDate()).getDate()).getTime() : false;
}

export function isValidStorage(): boolean {
  return typeof (Storage) !== `undefined`;
}

export function isValidStorageItem(item: string | any): boolean {
  return item !== null &&
    item !== undefined &&
    item !== `null` &&
    item !== `undefined` &&
    item !== ``;
}

export function safeParseJsonString(jsonString: string) {

  var valid = false;

  try {
    var json = JSON.parse(jsonString);
    valid = true;
  }
  catch (error) {
    console.error(error);
  }

  return valid && json !== null && json !== undefined && json !== `` ? json : ``;

}

export function isValidDate(date: Date | string | null): boolean {
  return date !== null && date !== undefined && date !== `` && date !== `Invalid Date`;
}