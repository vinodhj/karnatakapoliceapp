import { AfterViewInit, ChangeDetectorRef, Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSelectChange } from '@angular/material/select';
import { BehaviorSubject, of } from 'rxjs';
import { switchMap, tap } from 'rxjs/operators';
import { HttpService } from 'src/@exai/services/http.service';
import { LanguageService } from 'src/app/language.service';
;

@Component({
  selector: 'exai-pop-up',
  templateUrl: './pop-up.component.html',
  styleUrls: ['./pop-up.component.scss']
})
export class PopUpComponent implements OnInit, AfterViewInit {
  options: Array<Array<any>> = [];

  constructor(public dialogRef: MatDialogRef<PopUpComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public httpService: HttpService,
    public formBuilder: FormBuilder,
    public languageService: LanguageService,
    private cdr: ChangeDetectorRef
  ) { }

  isLoading = true
  isTabular = false
  colNumber = new FormControl(1)

  divisions: any[] = [];
  subDivisions: any[] = [];
  policeStations: any[] = [];

  ngOnInit(): void {
    if (this.data.noLoading)
      this.isLoading = false;

    if (this.data.configForms !== null && this.data.configForms !== undefined && this.data.configForms !== `` && this.data.configForms) {
      this.getInitialDropdownOptions();
      this.subscribeToFormChanges();
    }

    else {

      for (let i = 0; i < this.data.labels.length; i++) {
        this.options.push([]);
      }
      let indexOfLevel1 = (this.data.labels as Array<any>).indexOf(this.data.labels.filter((x: any) => { return x.level === 1 })[0]);

      this.httpService.getDivisions().subscribe((response: any) => {
        this.isLoading = false

        this.options[indexOfLevel1] = response;

        for (let i = 0; i < this.data.labels.length; i++) {
          if (this.data.labels[i].initialValue) {
            if (Array.isArray(this.data.labels[i].initialValue)) {
              this.options[i] = this.getNonDuplicatedArray([...this.options[i], ...this.data.labels[i].initialValue]);
            }
            else if (!(this.options[i].includes(this.data.labels[i].initialValue))) {
              this.options[i].push(this.data.labels[i].initialValue);
            }
            (this.data.controls.controls[i] as FormControl).setValue(this.data.labels[i].initialValue)
          }
        }
      });
    }
  }

  ngAfterViewInit() {
    console.log(this.options);
  }

  getInitialDropdownOptions(): void {
    const initialDivisions = this.data.labels.find(l => l.label === `Division`)?.initialValue;
    const initialSubDivisions = this.data.labels.find(l => l.label === `Sub-Division`)?.initialValue;
    const initialPoliceStations = this.data.labels.find(l => l.label === `Police Station`)?.initialValue;



    this.httpService.getDivisions().pipe(
      switchMap(divisions => {
        if ((divisions as any[])?.length === 0)
          return of();
        this.options[1] = [...divisions as any[]];
        if (initialDivisions !== null &&
          initialDivisions !== undefined &&
          initialDivisions !== `` &&
          initialDivisions?.length > 0 &&
          initialDivisions?.every(division => division !== null))
          return this.httpService.getSubDivisionsDropdownOptions((initialDivisions as any[])?.map(({ id }) => id));
        this.isLoading = false;
        return of();
      }),
      switchMap(subDivisions => {
        if ((subDivisions as any[])?.length === 0)
          return of();
        this.options[2] = [...subDivisions as any[]];
        if (initialSubDivisions !== null &&
          initialSubDivisions !== undefined &&
          initialSubDivisions !== `` &&
          initialSubDivisions?.length > 0 &&
          initialSubDivisions?.every(subDivision => subDivision !== null))
          return this.httpService.getPoliceStationsDropdownOptions((initialSubDivisions as any[])?.map(({ id }) => id));
        return of();
      }),
      switchMap(policeStations => {
        if ((policeStations as any[])?.length === 0)
          return of();
        this.options[3] = [...policeStations as any[]];
        if (initialPoliceStations !== null &&
          initialPoliceStations !== undefined &&
          initialPoliceStations !== `` &&
          initialPoliceStations?.length > 0 &&
          initialPoliceStations?.every(policeStations => policeStations !== null)
        )
          return of(this.data.controls);
        return of();
      })
    ).subscribe(
      response => {
        

        if (response !== null && response !== undefined && response !== ``) {
          this.data.controls?.controls[0]?.setValue(this.data.labels[0].initialValue, {
            emitEvent: false
          });
          this.data.controls?.controls[1]?.setValue(initialDivisions, {
            emitEvent: false,
            onlySelf: true
          });
          this.data.controls?.controls[2]?.setValue(initialSubDivisions, {
            emitEvent: false,
            onlySelf: true
          });
          this.data.controls?.controls[3]?.setValue(initialPoliceStations, {
            emitEvent: false,
            onlySelf: true
          });

        }

        this.isLoading = false;
      }
    );
  }

  subscribeToFormChanges(): void {
    this.data.controls?.controls[1]?.valueChanges.pipe(
      switchMap(divisions => {
        this.data.controls?.controls[2]?.setValue([], { emitEvent: true });
        this.cdr.detectChanges();
        this.options[2] = null;
        if ((divisions as any[]) === null || (divisions as any[]) === undefined || (divisions as any[])?.length === 0)
          return of();
        return this.httpService.getSubDivisionsDropdownOptions((divisions as any[])?.map(({ id }) => id));
      })
    ).subscribe(subDivisions => {
      this.options[2] = subDivisions as any[];
    }
    );

    this.data.controls?.controls[2]?.valueChanges.pipe(
      switchMap((subDivisions: any) => {
        this.data.controls?.controls[3]?.setValue([], { emitEvent: true });
        this.cdr.detectChanges();
        this.options[3] = null;
        if ((subDivisions as any[]) === null || (subDivisions as any[]) === undefined)
          return of();

        return this.httpService.getPoliceStationsDropdownOptions((subDivisions as any[])?.map(({ id }) => id));
      })
    ).subscribe(policeStations => {
      this.options[3] = policeStations as any[];
    });

    this.cdr.detectChanges()
  }

  selectComparision(option: any, value: any) {
      if (typeof (option) == 'object') {
        return option?.id == value?.id;
      }
      else return option.toString() == value.toString();

  }

  getNonDuplicatedArray(array: Array<any>) {
    let retVal = [];
    for (let item of array) {
      if (!retVal.includes(JSON.stringify(item))) {
        retVal.push(JSON.stringify(item));
      }
    }
    retVal = retVal.map((x: any) => { return JSON.parse(x) });
    return retVal;
  }
  getOptionsForChild(event: MatSelectChange, dependentIndex: number) {
    // if (dependentIndex) {
    //   this.isLoading = true
    //   this.httpService.getSubDivisions().subscribe((response: any) => {
    //     this.isLoading = false
    //     this.options[dependentIndex] = response;
    //   })
    // }
  }
  selectAll($event: MatCheckboxChange, curIndex: number, dependentIndex: number = null) {
    if (this.data.configForms !== null && this.data.configForms !== undefined && this.data.configForms !== `` && this.data.configForms) {
      if ($event.checked)
        this.data.controls.controls[curIndex].setValue(this.options[curIndex], { emitEvent: true });
      else
        this.data.controls.controls[curIndex].setValue([], { emitEvent: true });
    } else {
      if ($event.checked)
        this.data.controls.controls[curIndex].setValue([...this.options[curIndex]]);
      else
        this.data.controls.controls[curIndex].setValue([]);
      if (dependentIndex) {
        this.isLoading = true
        this.httpService.getDivisions().subscribe((response: any) => {
          this.isLoading = false
          this.options[dependentIndex] = response;
        })
      }
    }

    this.cdr.detectChanges();
  }

  setValue({ value }) {
    if (value === 'Table') {
      this.isTabular = true
    }
  }

  onSubmitForm() {
    if (this.data.controls.valid && this.colNumber.valid) {
      this.dialogRef.close({
        value: this.data.controls.value,
        colNumber: this.colNumber.value
      });
    }
    else {
      console.log('Invalid Form', this.data.controls.value);
    }
  }
  onCancel() {
    this.data.controls.reset()
    this.dialogRef.close();
  }

}
