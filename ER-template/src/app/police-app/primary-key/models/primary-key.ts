export class PrimaryKeyDto {
    id: number;
    alternativeId: string;
    divisionId: number;
    divisionName: string;
    subDivisionId: number;
    subDivisionName: string;
    keyHeadName: string;
    content: string | null;
    isDeleted: boolean;
    createdBy: number;
    createdByUsername: string;
    createdDateTime: string;
    modifiedBy: number;
    modifiedByUsername: string;
    modifiedDateTime: string;
}