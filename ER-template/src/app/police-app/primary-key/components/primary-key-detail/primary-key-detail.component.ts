import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BehaviorSubject, forkJoin, Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { HttpService } from 'src/@exai/services/http.service';
import { LanguageService } from 'src/app/language.service';
import { noWhitespaceValidator } from 'src/app/shared/models/no-whitespace.validator';

@Component({
  selector: 'exai-primary-key-detail',
  templateUrl: './primary-key-detail.component.html',
  styleUrls: ['./primary-key-detail.component.scss']
})
export class PrimaryKeyDetailComponent implements OnInit {

  formGroup: FormGroup = this.fb.group({
    keyHeadName: [``, [Validators.required, noWhitespaceValidator()]],
    division: [``, Validators.required],
    subDivision: [``, Validators.required],
    content: [``, [Validators.maxLength(4000)]]
  });
  isLoading: boolean = false;
  loadingDataForDropdown: boolean = true;
  nameAlreadyExists: boolean = false;

  divisions: any[] = [];
  allSubDivisions: any[] = [];
  filteredSubDivisionsSubject: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);
  filteredSubDivisions: Observable<any[]> = this.filteredSubDivisionsSubject.asObservable();

  dialogTitle: string = ``;
  selectedDivisions = this.divisions;

  constructor(
    public dialogRef: MatDialogRef<PrimaryKeyDetailComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public languageService: LanguageService,
    private fb: FormBuilder,
    private httpService: HttpService,
    private snackbar: MatSnackBar
  ) { }

  ngOnInit(): void {
    this.reactOnDivisionChange();
    this.initializeDataForDialog();
  }

  reactOnDivisionChange(): void {
    this.formGroup.get(`division`)
      .valueChanges.subscribe(
        value => {
          this.formGroup.get(`subDivision`).setValue(null);
          if (value === null || value === undefined || value === ``)
            return;
          this.filteredSubDivisionsSubject.next(
            this.allSubDivisions?.filter(subDivision => subDivision?.division?.id === value)
          );
        }
      );
  }

  initializeDataForDialog(): void {
    var requests = [
      this.httpService.getDivisionsDropdownV1(),
      this.httpService.getSubDivisions()
    ];
    forkJoin(requests).pipe(
      finalize(() => this.loadingDataForDropdown = false)
    ).subscribe(
      responses => {
        if (((responses[0] as any)?.items as any[])?.length > 0)
          this.divisions = [...(responses[0] as any)?.items as any[]];
        if ((responses[1] as any[])?.length > 0)
          this.allSubDivisions = [...responses[1] as any[]].filter(subDivision => subDivision.isActive);
        this.patchInitialValues();
      }
    );
  }

  patchInitialValues(): void {
    if (this.data?.editing) {
      this.dialogTitle = `Edit Primary Key`;
      this.formGroup.patchValue({
        keyHeadName: this.data?.keyHeadName,
        content: this.data?.content,
        division: this.divisions?.find(division => division?.id === this.data?.divisionId)?.id
      });
      const filteredSubDivisions = this.allSubDivisions?.filter(subDivision =>
        subDivision?.division?.id === this.divisions?.find(
          division => division?.id === this.data?.divisionId)?.id);
      if (filteredSubDivisions.length > 0) {
        this.filteredSubDivisionsSubject.next(this.allSubDivisions?.filter(subDivision =>
          subDivision?.division?.id === this.divisions?.find(
            division => division?.id === this.data?.divisionId)?.id));
        this.formGroup.get(`subDivision`).setValue(this.data?.subDivisionId);
      }
    } else {
      this.dialogTitle = `Add new Primary Key`;
    }
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  onSubmitForm(): void {
    if (!this.formGroup.valid) {
      this.formGroup.get(`keyHeadName`).markAsTouched();
      this.formGroup.get(`division`).markAsTouched();
      this.formGroup.get(`subDivision`).markAsTouched();
      this.formGroup.get(`content`).markAsTouched();
      return;
    }
    if (this.data.editing)
      this.updatePrimaryKey();
    else
      this.createPrimaryKey();
  }

  updatePrimaryKey(): void {
    let request: any = {
      id: this.data?.id,
      keyHeadName: this.formGroup.get(`keyHeadName`).value,
      divisionId: this.formGroup.get(`division`).value,
      subDivisionId: this.formGroup.get(`subDivision`).value,
      content: this.formGroup.get(`content`).value
    }
    this.httpService.updatePrimaryKey(request).pipe(
      finalize(() => this.isLoading = false)
    ).subscribe(response => {
      this.snackbar.open((response as any)?.message, 'OK', { duration: 2000 })
      this.dialogRef.close(true);
    });
  }

  createPrimaryKey(): void {
    let request: any = {
      keyHeadName: this.formGroup.get(`keyHeadName`).value,
      divisionId: this.formGroup.get(`division`).value,
      subDivisionId: this.formGroup.get(`subDivision`).value,
      content: this.formGroup.get(`content`).value
    }
    this.httpService.createPrimaryKey(request).pipe(
      finalize(() => this.isLoading = false)
    ).subscribe((response: any) => {
      this.snackbar.open(response.message, 'OK', { duration: 2000 })
      this.dialogRef.close(true);
    });
  }
}