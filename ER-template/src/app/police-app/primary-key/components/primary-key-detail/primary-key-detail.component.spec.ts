import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrimaryKeyDetailComponent } from './primary-key-detail.component';

describe('PrimaryKeyDetailComponent', () => {
  let component: PrimaryKeyDetailComponent;
  let fixture: ComponentFixture<PrimaryKeyDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PrimaryKeyDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PrimaryKeyDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
