import { SelectionModel } from '@angular/cdk/collections';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { Component, Input, ViewChild } from '@angular/core';
import { FormArray, FormControl, Validators } from '@angular/forms';
import { MatChipInputEvent } from '@angular/material/chips';
import { MatDialog } from '@angular/material/dialog';
import { MatFormFieldDefaultOptions, MAT_FORM_FIELD_DEFAULT_OPTIONS } from '@angular/material/form-field';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import moment from 'moment';
import { Observable, ReplaySubject } from 'rxjs';
import { filter } from 'rxjs/operators';
import { fadeInUp400ms } from 'src/@exai/animations/fade-in-up.animation';
import { stagger40ms } from 'src/@exai/animations/stagger.animation';
import { TableColumn } from 'src/@exai/interfaces/table-column.interface';
import { HttpService } from 'src/@exai/services/http.service';
import { LanguageService } from 'src/app/language.service';
import { ConfirmDialogComponent } from 'src/app/shared/advanced-grid-item/dialogs/confirm-dialog/confirm-dialog.component';
import { HtmlToDocxService } from 'src/app/shared/services/html-to-docx.service';
import { SnackBarService } from 'src/app/shared/services/snack-bar.service';
import { PrimaryKeyDto } from '../../models/primary-key';
import { PrimaryKeyDetailComponent } from '../primary-key-detail/primary-key-detail.component';

@Component({
  selector: 'exai-primary-key-list',
  templateUrl: './primary-key-list.component.html',
  styleUrls: ['./primary-key-list.component.scss'],
  animations: [
    fadeInUp400ms,
    stagger40ms
  ],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        appearance: 'standard'
      } as MatFormFieldDefaultOptions
    }
  ]
})

export class PrimaryKeyListComponent {

  layoutCtrl = new FormControl('boxed');
  subject$: ReplaySubject<PrimaryKeyDto[]> = new ReplaySubject<PrimaryKeyDto[]>(1);
  data$: Observable<PrimaryKeyDto[]> = this.subject$.asObservable();
  customers: PrimaryKeyDto[];
  formListData: PrimaryKeyDto[] = [];
  departments: any[] = [];
  addDivision: FormArray;
  formLabels: Array<any>;

  datesInput: { dateFrom: Date | number | null, dateTo: Date | number | null } | null = null;

  @Input()
  columns: TableColumn<PrimaryKeyDto>[] = [
    { label: 'Serial NO', property: 'serialNumber', type: 'serialNumber', visible: true, cssClasses: ['font-medium'] },
    { label: 'Key', property: 'id', type: 'text', visible: true, cssClasses: ['font-medium'] },
    { label: 'Key Head Name', property: 'keyHeadName', type: 'text', visible: true, cssClasses: ['font-medium'] },
    { label: 'Division', property: 'divisionName', type: 'text', visible: true, cssClasses: ['font-medium'] },
    { label: 'Sub Division', property: 'subDivisionName', type: 'text', visible: true, cssClasses: ['font-medium'] },
    { label: 'Created by', property: 'createdByUsername', type: 'text', visible: true, cssClasses: ['font-medium'] },
    { label: 'Created on', property: 'createdDateTime', type: 'text', visible: true, cssClasses: ['font-medium'] },
    { label: 'Modified by', property: 'modifiedByUsername', type: 'text', visible: true, cssClasses: ['font-medium'] },
    { label: 'Modified on', property: 'modifiedDateTime', type: 'text', visible: true, cssClasses: ['font-medium'] },
    { label: 'Action', property: 'actions', type: 'button', visible: true }
  ];

  length: number;
  pageSize: 10;
  pageSizeOptions = [5, 10, 50];

  @ViewChild(MatPaginator) paginator: MatPaginator;

  dataSource: MatTableDataSource<PrimaryKeyDto> | null;
  selection = new SelectionModel<PrimaryKeyDto>(true, []);
  searchCtrl = new FormControl();

  isLoading = true
  temporaryFilter: string | null = null;

  constructor(private dialog: MatDialog,
    private httpservice: HttpService,
    public languageService: LanguageService,
    private htmlToDocxService: HtmlToDocxService,
    private snackbar: MatSnackBar
  ) {

    this.formLabels = [
      {
        label: 'Division Name',
        type: 1,
        level: null,
        dependentIndex: null,
        initialValue: '',
      },
    ];

    this.addDivision = new FormArray([
      new FormControl('', [Validators.required]),
    ])
  }

  get visibleColumns() {
    return this.columns.filter(column => column.visible).map(column => column.property);
  }

  ngOnInit() {
    this.getPrimaryKeys();

    this.data$.pipe(
      filter<PrimaryKeyDto[]>(Boolean)
    ).subscribe(customers => {
      this.customers = customers;
      this.dataSource.data = customers;
    });
  }

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.columns, event.previousIndex, event.currentIndex);
  }

  addOnBlur = true;
  readonly separatorKeysCodes = [ENTER, COMMA] as const;
  searchValues = [];

  wantedSearchedValues = []

  add(event: MatChipInputEvent): void {
    this.wantedSearchedValues = []
    // Add our fruit
    if (event.value) {
      this.searchValues.push(event.value);
    }

    let wantedValues = []
    this.searchValues.forEach(el => {
      if (el.split(' :').length == 1) {
        let textForSearch = el.split(' :')[0];
        this.wantedSearchedValues.push('all')
        wantedValues.push(textForSearch)
      }
      else {
        let value = el.split(' :')[1].trim()
        this.columns.forEach(column => {
          if (column.label === el.split(' :')[0]) {
            this.wantedSearchedValues.push(column.property)
          }
        })
        wantedValues.push(value)
      }
    })
    this.dataSource.filter = wantedValues.join('+');

    // Clear the input value
    event.chipInput!.clear();
  }

  remove(value): void {
    const index = this.searchValues.indexOf(value);
    let dataSourceFilters = this.dataSource.filter.split('+')
    this.wantedSearchedValues.splice(index, 1)
    let valueSplitted = value.split(':')

    if (index >= 0) {
      this.searchValues.splice(index, 1);
      if (valueSplitted.length > 1) {
        dataSourceFilters.splice(dataSourceFilters.indexOf(valueSplitted[1].trim()), 1);
      } else {
        dataSourceFilters.splice(dataSourceFilters.indexOf(valueSplitted[0].trim()), 1);
      }
      this.dataSource.filter = dataSourceFilters.join('+')
    }

    if (this.searchValues.length < 1) {
      this.dataSource.filter = ''
    }
  }

  appendToSearch(label) {
    let configuredLabelForSearch = `${label} : `
    this.searchCtrl.setValue(configuredLabelForSearch)
  }

  camelCaseToNormal(text) {
    const result = text.replace(/([A-Z])/g, " $1");
    const finalResult = result.charAt(0).toUpperCase() + result.slice(1);
    return finalResult
  }

  getPrimaryKeys() {
    this.isLoading = true
    this.dataSource = new MatTableDataSource<PrimaryKeyDto>();
    this.httpservice.getPrimaryKeys().subscribe(
      (response: PrimaryKeyDto[]) => {

        this.isLoading = false;

        response.forEach((x: any, index: number) => {
          x.serialNumber = index + 1
          x['createdDateTime'] = moment(x.createdDateTime).format('MMMM Do YYYY, h:mm a');
          x['modifiedDateTime'] = moment(x.modifiedDateTime).format('MMMM Do YYYY, h:mm a')
        });

        this.formListData = response;
        this.dataSource = new MatTableDataSource<PrimaryKeyDto>(this.formListData);
        // advanced filtering function
        this.dataSource.filterPredicate = (data, filters) => {
          const matchFilter = [];
          const filterArray = filters.split('+');
          const columns = Object.values(data);
          const columnLabels = [...this.columns.filter(column => column?.visible && !column?.property?.includes(`actions`)).map(({ property }) => property) as string[]];
          filterArray.forEach(filter => {
            let formattedFilter = filter.trim().toLowerCase()
            const customFilter = [];

            columns.forEach(column => {
              this.wantedSearchedValues.forEach(colProperty => {

                if (colProperty == 'all') {
                  for (let key in data) {
                    let formattedDataValue = ((('' + data[key]).toLowerCase().replace(/\s+/g, ' ')).trim())
                    if (columnLabels.indexOf(key) >= 0 && (formattedDataValue.includes(formattedFilter) || formattedDataValue == (formattedFilter))) {
                      customFilter.push(true)
                    }
                  }
                } else {
                  if (colProperty === 'isActive') {
                    let formattedDataColProperty

                    if (data[colProperty]) {
                      formattedDataColProperty = 'true'
                    } else {
                      formattedDataColProperty = 'false'
                    }

                    customFilter.push(formattedDataColProperty.includes(formattedFilter) || formattedDataColProperty == formattedFilter)
                  } else {
                    let formattedDataColProperty = (('' + data[colProperty]).toLowerCase().replace(/\s+/g, ' ')).trim()
                    customFilter.push(formattedDataColProperty.includes(formattedFilter))
                  }
                }
              })
            });
            matchFilter.push(customFilter.some(Boolean)); // OR
          });

          return matchFilter.every(Boolean); // AND
        }
        this.dataSource.paginator = this.paginator;
      });
  }

  isValidDate(date: Date | string | number | null): boolean {
    return date !== null && date !== undefined && date !== `` && date !== `Invalid Date`;
  }

  toggleColumnVisibility({ column, event }) {
    event.stopPropagation();
    event.stopImmediatePropagation();
    column.visible = !column.visible;
  }

  trackByProperty<T>(index: number, column: TableColumn<T>) {
    return column.property;
  }

  public getSubDivisions(rowValue) {
    let subDivisions = []
    rowValue.departments.forEach(element => {
      if (element.name === 'Police-Station') {
        subDivisions.push(element.parent.value)
      } else if (element.name === 'Sub-Division') {
        subDivisions.push(element.value)
      }
    })
    return [...new Set(subDivisions)]?.join(', ');
  }

  public getPoliceStations(row) {
    let policeStations = []
    row.departments.forEach(element => {
      if (element.name === 'Police-Station') {
        policeStations.push(element.value)
      }
    })
    return [...new Set(policeStations)].join(', ')
  }

  createPrimaryKey(initialValue: any = null, update = false): void {
    let dialogConfiguration: any = {
      width: `30vw`,
      height: `min-content`,
      disableClose: true,
      data: {
        editing: false
      }
    }
    if (initialValue) {
      dialogConfiguration.data = {
        editing: true,
        keyHeadName: initialValue?.keyHeadName,
        divisionId: initialValue?.divisionId,
        subDivisionId: initialValue?.subDivisionId,
        content: initialValue?.content
      }
    }
    const dialogRef = this.dialog.open(PrimaryKeyDetailComponent, dialogConfiguration);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.getPrimaryKeys();
      }
    })
  }

  updatePrimaryKey(initialValue: any = null): void {
    let dialogConfiguration: any = {
      width: `30vw`,
      height: `min-content`,
      disableClose: true,
      data: {
        editing: true
      }
    }
    if (initialValue) {
      dialogConfiguration.data = {
        editing: true,
        id: initialValue?.id,
        keyHeadName: initialValue?.keyHeadName,
        divisionId: initialValue?.divisionId,
        subDivisionId: initialValue?.subDivisionId,
        content: initialValue?.content
      }
    }
    const dialogRef = this.dialog.open(PrimaryKeyDetailComponent, dialogConfiguration);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.getPrimaryKeys();
      }
    })
  }

  deletePrimaryKey(event): void {
    let message = `Are you sure that you want to delete this primary key?`;
    const confirmDialog = this.dialog.open(ConfirmDialogComponent, {
      width: `500px`,
      data: { message: message }
    });
    confirmDialog.afterClosed().subscribe(result => {
      if (result?.answer) {
        this.httpservice.deletePrimaryKey(event?.id).subscribe(
          response => {
            this.snackbar.open((response as any)?.message, 'OK', { duration: 2000 })
            this.getPrimaryKeys();
          }
        )
      }
    });
  }

  downloadWord(type: string) {

    let formattedTableValues = []

    const matchFields = {
      serialNumber: 'SERIAL NO',
      id: `KEY`,
      keyHeadName: `KEY HEAD NAME`,
      divisionName: `DIVISION`,
      subDivisionName: `SUB DIVISION`,
      createdByUsername: "CREATED BY",
      createdDateTime: "CREATED ON",
      modifiedByUsername: "MODIFIED BY",
      modifiedDateTime: "MODIFIED ON"
    }

    this.columns.map(column => {

      if (!column.visible) {

        for (const rowKey in matchFields) {

          const valueOfRow = matchFields[rowKey]

          if (valueOfRow.toLowerCase() === column.label.toLowerCase())
            delete matchFields[rowKey];

        }

      }

    })

    //if user filtered data then export only that data else all data
    const dataForExport = this.dataSource.filter ? this.dataSource.connect().value : this.dataSource.data;

    dataForExport.forEach((row: any) => {

      let oneRowInTable = {};

      for (const rowKey in matchFields) {
        const headerName = matchFields[rowKey];

        if (headerName === 'STATUS') {

          oneRowInTable[headerName] = row[rowKey] ? 'Active' : 'Inactive';

        } else {

          oneRowInTable[headerName] = row[rowKey];

        }

      }

      formattedTableValues.push(oneRowInTable);

    })


    this.htmlToDocxService.downloadBasedOnType(type, formattedTableValues, 'primary-keys');
  }
}