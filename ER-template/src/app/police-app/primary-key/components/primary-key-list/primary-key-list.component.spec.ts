import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrimaryKeyListComponent } from './primary-key-list.component';

describe('PrimaryKeyListComponent', () => {
  let component: PrimaryKeyListComponent;
  let fixture: ComponentFixture<PrimaryKeyListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PrimaryKeyListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PrimaryKeyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
