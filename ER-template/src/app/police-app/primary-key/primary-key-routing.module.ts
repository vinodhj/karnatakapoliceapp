import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PrimaryKeyListComponent } from './components/primary-key-list/primary-key-list.component';

const routes: Routes = [
    {
        path: ``,
        component: PrimaryKeyListComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class PrimaryKeyRoutingModule { }