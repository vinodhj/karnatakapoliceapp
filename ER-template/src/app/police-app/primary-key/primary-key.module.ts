import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { AngularMaterialModel } from 'src/app/shared/angular-material.module';
import { BasicTableModule } from 'src/app/shared/basic-table/basic-table.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PrimaryKeyRoutingModule } from './primary-key-routing.module';
import { PrimaryKeyListComponent } from './components/primary-key-list/primary-key-list.component';
import { PrimaryKeyDetailComponent } from './components/primary-key-detail/primary-key-detail.component';

@NgModule({
  declarations: [
    PrimaryKeyListComponent,
    PrimaryKeyDetailComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    AngularMaterialModel,
    BasicTableModule,
    PrimaryKeyRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ]
})

export class PrimaryKeyModule { }
