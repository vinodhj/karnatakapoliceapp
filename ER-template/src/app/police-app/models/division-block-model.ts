export class DivisionBlockDto {
    id: number;
    branchId: number;
    isActive: boolean;
    isDeleted: boolean;
    createdBy: number = 0;
}