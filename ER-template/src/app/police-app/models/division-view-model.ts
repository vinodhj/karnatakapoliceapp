export class DivisionViewModel {
    id: number;
    serialNumber: number;
    branchId: number | null;
    name: string;
    dataDetail: string | null;
    isActive: boolean;
    isDeleted: boolean;
    createdBy: string;
    createdDatetime: Date | string;
    modifiedBy: string | null;
    modifiedDatetime: Date | string | null;
}