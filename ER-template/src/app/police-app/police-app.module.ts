import { CommonModule } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgApexchartsModule } from 'ng-apexcharts';
import { FormBuilderModule } from '../form-builder/form-builder.module';
import { AdvancedGridItemModule } from '../shared/advanced-grid-item/advanced-grid-item.module';
import { AngularMaterialModel } from '../shared/angular-material.module';
import { SharedModule } from '../shared/shared.module';
import { AddNewFormComponent } from './add-new-form/add-new-form.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CreateNewFormComponent } from './forms/create-new-form/create-new-form.component';
import { EditFormComponent } from './forms/edit-form/edit-form.component';
import { PoliceAppRoutingModule } from './police-app-routing.module';
import { PoliceAppComponent } from './police-app.component';
import { PopUpComponent } from './pop-up/pop-up.component';
import { ProfileComponent } from './profile/profile.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { BasicTableModule } from '../shared/basic-table/basic-table.module';
import { EditPasswordDialogComponent } from './profile/edit-password-dialog/edit-password-dialog.component';
import { ChartsModule } from 'ng2-charts';
import { DashboardChartComponent } from './dashboard/dashboard-chart/dashboard-chart.component';
import { DashboardTooltipComponent } from './dashboard/dashboard-tooltip/dashboard-tooltip.component';
import { NgxMatIntlTelInputModule } from 'ngx-mat-intl-tel-input';

@NgModule({
  declarations: [
    PoliceAppComponent,
    CreateNewFormComponent,
    EditFormComponent,
    ProfileComponent,
    DashboardComponent,
    PopUpComponent,
    EditPasswordDialogComponent,
    DashboardChartComponent,
    DashboardTooltipComponent
  ],
  imports: [
    CommonModule,
    PoliceAppRoutingModule,
    FormBuilderModule,
    AngularMaterialModel,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    AdvancedGridItemModule,
    NgApexchartsModule,
    DragDropModule,
    BasicTableModule,
    NgxMatIntlTelInputModule,
    ChartsModule
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ],
})
export class PoliceAppModule { }
