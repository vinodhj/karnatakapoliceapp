export class TemplateDropdownOption {
    id: number;
    formName: string;
    alternativeId: string;
}