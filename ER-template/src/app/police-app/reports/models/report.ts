export class ReportDto {
    userFormId: number;
    userFormName: string;
    alternativeId: string;
    formId: number;
    formMasterId: number | null;
    formName: string;
    formTypeName: string;
    formCreatedBy: number;
    s3UserInput: string;
    // roleId: number;
    // roleName: string;
    userName: string;
    divisionColumnName: string | null;
    subDivisionColumnName: string | null;
    policeStationColumnName: string | null;
    currentStatusId: number | null;
    currentStatusName: string;
    createdBy: number;
    createdDateTime: string;
    createdDateTimeInitial: string;
}