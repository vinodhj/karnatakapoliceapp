import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportsComponent } from './components/reports/reports.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { AngularMaterialModel } from 'src/app/shared/angular-material.module';
import { BasicTableModule } from 'src/app/shared/basic-table/basic-table.module';
import { ReportsRoutingModule } from './reports-routing.module';
import { ReportsCalendarComponent } from './components/reports-calendar/reports-calendar.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ReportsListComponent } from './components/reports-list/reports-list.component';
import { ReportsDialogComponent } from './components/reports-dialog/reports-dialog.component';

@NgModule({
  declarations: [
    ReportsComponent,
    ReportsCalendarComponent,
    ReportsListComponent,
    ReportsDialogComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    AngularMaterialModel,
    BasicTableModule,
    ReportsRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ]
})

export class ReportsModule { }
