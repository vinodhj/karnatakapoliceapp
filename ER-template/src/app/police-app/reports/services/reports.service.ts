import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ReportsService {

  private reportDatesInputSubject = new BehaviorSubject({
    dateFrom: null,
    dateTo: null
  });

  reportDatesInput = this.reportDatesInputSubject.asObservable();

  constructor() { }

  sendReportInputData(dateInput: { dateFrom: Date | number | null, dateTo: Date | number | null }) {
    this.reportDatesInputSubject.next(dateInput);
  }

  refreshReportInputData() {
    this.reportDatesInputSubject.next({
      dateFrom: null,
      dateTo: null
    });
  }
}
