import { SelectionModel } from '@angular/cdk/collections';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormControl, Validators } from '@angular/forms';
import { MatChipInputEvent } from '@angular/material/chips';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import moment from 'moment';
import { forkJoin, Observable, of, ReplaySubject } from 'rxjs';
import { filter, switchMap } from 'rxjs/operators';
import { TableColumn } from 'src/@exai/interfaces/table-column.interface';
import { HttpService } from 'src/@exai/services/http.service';
import { LanguageService } from 'src/app/language.service';
import { DivisionViewModel } from 'src/app/police-app/models/division-view-model';
import { DataService } from 'src/app/police-app/services/data-service.service';
import { DownloadToExcelService } from 'src/app/shared/advanced-grid-item/services/download-to-excel.service';
import { Role } from 'src/app/shared/enums/role.enum';
import { ReportDto } from '../../models/report';
import { ReportsService } from '../../services/reports.service';
import { ReportsCalendarComponent } from '../reports-calendar/reports-calendar.component';
import jwt_decode from 'jwt-decode';

@Component({
  selector: 'exai-reports-list',
  templateUrl: './reports-list.component.html',
  styleUrls: ['./reports-list.component.scss']
})
export class ReportsListComponent implements OnInit {

  layoutCtrl = new FormControl('boxed');
  subject$: ReplaySubject<ReportDto[]> = new ReplaySubject<ReportDto[]>(1);
  data$: Observable<ReportDto[]> = this.subject$.asObservable();
  customers: ReportDto[];
  formListData: ReportDto[] = [];
  departments: any[] = [];
  addDivision: FormArray;
  formLabels: Array<any>;
  downloadEnabled: boolean = true;

  datesInput: { dateFrom: Date | number | null, dateTo: Date | number | null } | null = null;

  @Input()
  columns: TableColumn<DivisionViewModel>[] = [
    { label: 'Serial NO', property: 'serialNumber', type: 'serialNumber', visible: true, cssClasses: ['font-medium'] },
    { label: 'Record Id', property: 'alternativeId', type: 'text', visible: true, cssClasses: ['font-medium'] },
    { label: 'Form Name', property: 'formName', type: 'text', visible: true, cssClasses: ['font-medium'] },
    { label: 'Form Id', property: 'formMasterId', type: 'text', visible: true, cssClasses: ['font-medium'] },
    { label: 'Location', property: 'divisionColumnName', type: 'text', visible: true },
    { label: 'Sub Division', property: 'subDivisionColumnName', type: 'text', visible: true },
    { label: 'Police Station', property: 'policeStationColumnName', type: 'text', visible: true },
    { label: 'Current Status', property: 'currentStatusName', type: 'text', visible: true, cssClasses: ['font-medium'] },
    { label: 'Filled by', property: 'userName', type: 'text', visible: true, cssClasses: ['font-medium'] },
    { label: 'Created on', property: 'createdDateTime', type: 'text', visible: true, cssClasses: ['font-medium'] },
    { label: 'Action', property: 'actions', type: 'button', visible: true }
  ];

  length: number;
  pageSize: 10;
  pageSizeOptions = [5, 10, 50];

  @ViewChild(MatPaginator) paginator: MatPaginator;

  dataSource: MatTableDataSource<ReportDto> | null;
  selection = new SelectionModel<ReportDto>(true, []);
  searchCtrl = new FormControl();
  formName: string | null = null;
  formId: string | null = null;
  dateFrom: string | null = null;
  dateTo: string | null = null;

  isLoading = true
  temporaryFilter: string | null = null;

  userRole = this.httpservice.getRole();
  formCreatedBy: number = 0;
  get canApproveReject(): boolean {
    var decoded: any = jwt_decode(sessionStorage.getItem('token'));
    return (decoded.nameid === this.formCreatedBy || [Role.SuperAdmin].includes(this.userRole))
  }

  constructor(private dialog: MatDialog,
    private httpservice: HttpService,
    public languageService: LanguageService,
    private reportsService: ReportsService,
    private router: Router,
    private route: ActivatedRoute,
    private httpClient: HttpClient,
    private downloadToExcel: DownloadToExcelService,
    public dataService: DataService
  ) {

    this.formLabels = [
      {
        label: 'Division Name',
        type: 1,
        level: null,
        dependentIndex: null,
        initialValue: '',
      },
    ];

    this.addDivision = new FormArray([
      new FormControl('', [Validators.required]),
    ])
  }

  get visibleColumns() {
    return this.columns.filter(column => column.visible).map(column => column.property);
  }

  ngOnInit() {
    this.route.queryParams.pipe(
      switchMap(res => {
        this.formId = res[`formId`];
        this.dateFrom = res[`dateFrom`];
        this.dateTo = res[`dateTo`];
        if (this.formId === null ||
          this.formId === undefined ||
          this.formId === ``
        ) {
          return of(null);
        }
        return of(this.formId);
      })
    ).subscribe((content: any) => {
      if (content !== null) {
        this.getReportsV1(this.dateFrom, this.dateTo);
        this.data$.pipe(
          filter<ReportDto[]>(Boolean)
        ).subscribe(customers => {
          this.customers = customers;
          this.dataSource.data = customers;
        });
      } else {
        this.isLoading = false;
      }
    });
  }

  subscribeToDateInputChange(): void {
    this.reportsService.reportDatesInput.subscribe(
      data => {
        this.datesInput = data as { dateFrom: Date | number | null, dateTo: Date | number | null } | null;
        this.temporaryFilter = this.dataSource.filter;
        if (this.dataSource.filter === null || this.dataSource.filter === undefined || this.dataSource.filter === ``) {
          this.dataSource.filter = '----------------';
        } else {
          this.dataSource.filter = this.temporaryFilter;
        }
      }
    );
  }

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.columns, event.previousIndex, event.currentIndex);
  }

  addOnBlur = true;
  readonly separatorKeysCodes = [ENTER, COMMA] as const;
  searchValues = [];

  wantedSearchedValues = []

  add(event: MatChipInputEvent): void {
    this.wantedSearchedValues = []
    // Add our fruit
    if (event.value) {
      this.searchValues.push(event.value);
    }

    let wantedValues = []
    this.searchValues.forEach(el => {
      if (el.split(' :').length == 1) {
        let textForSearch = el.split(' :')[0];
        this.wantedSearchedValues.push('all')
        wantedValues.push(textForSearch)
      }
      else {
        let value = el.split(' :')[1].trim()
        this.columns.forEach(column => {
          if (column.label === el.split(' :')[0]) {
            this.wantedSearchedValues.push(column.property)
          }
        })
        wantedValues.push(value)
      }
    })
    this.dataSource.filter = wantedValues.join('+')

    // Clear the input value
    event.chipInput!.clear();
  }

  remove(value): void {
    const index = this.searchValues.indexOf(value);
    let dataSourceFilters = this.dataSource.filter.split('+')
    this.wantedSearchedValues.splice(index, 1)
    let valueSplitted = value.split(':')

    if (index >= 0) {
      this.searchValues.splice(index, 1);
      if (valueSplitted.length > 1) {
        dataSourceFilters.splice(dataSourceFilters.indexOf(valueSplitted[1].trim()), 1);
      } else {
        dataSourceFilters.splice(dataSourceFilters.indexOf(valueSplitted[0].trim()), 1);
      }
      this.dataSource.filter = dataSourceFilters.join('+')
    }

    if (this.searchValues.length < 1) {
      this.dataSource.filter = '----------------';
    }
  }

  appendToSearch(label) {
    let configuredLabelForSearch = `${label} : `
    this.searchCtrl.setValue(configuredLabelForSearch)
  }

  camelCaseToNormal(text) {
    const result = text.replace(/([A-Z])/g, " $1");
    const finalResult = result.charAt(0).toUpperCase() + result.slice(1);
    return finalResult
  }

  getReports() {
    this.isLoading = true
    this.dataSource = new MatTableDataSource<ReportDto>();
    this.httpservice.getReports().subscribe(
      (response: ReportDto[]) => {

        response.forEach((x: any, index: number) => {
          x.serialNumber = index + 1
          x[`createdDateTimeInitial`] = x.createdDateTime;
          x['createdDateTime'] = moment(x.createdDateTime).format('MMMM Do YYYY, h:mm a')
        });

        // response.forEach((row: any) => {

        //   row.subDivisionName = this.getSubDivisions(row);
        //   row.policeStationsName = this.getPoliceStations(row);

        // })

        this.isLoading = false;

        this.formListData = response;
        this.dataSource = new MatTableDataSource<ReportDto>(this.formListData);
        // advanced filtering function
        this.filter();
      });
  }

  getReportsV1(dateFrom = null, dateTo = null) {
    const dateFromValid = dateFrom !== null && dateFrom !== undefined && dateFrom !== `` ? new Date(Number(dateFrom)).toDateString() : ``;
    const dateToValid = dateTo !== null && dateTo !== undefined && dateTo !== `` ? new Date(Number(dateTo)).toDateString() : ``;
    this.isLoading = true
    this.dataSource = new MatTableDataSource<ReportDto>();
    this.httpservice.getReportsV1(this.formId, dateFromValid, dateToValid).subscribe(
      (response: ReportDto[]) => {
        response.forEach((x: any, index: number) => {
          x.serialNumber = index + 1
          x[`createdDateTimeInitial`] = x.createdDateTime;
          x['createdDateTime'] = moment(x.createdDateTime).format('MMMM Do YYYY, h:mm a')
        });

        this.formCreatedBy = response[0]?.formCreatedBy;

        if (response[0]?.formTypeName !== `Table`)
          this.downloadEnabled = false;


        // response.forEach((row: any) => {

        //   row.subDivisionName = this.getSubDivisions(row);
        //   row.policeStationsName = this.getPoliceStations(row);

        // })

        this.isLoading = false;

        this.formListData = response;
        this.dataSource = new MatTableDataSource<ReportDto>(this.formListData);
        // advanced filtering function
        this.filter();
      });
  }

  filter(): void {
    this.dataSource.filterPredicate = (data, filters) => {
      const matchFilter = [];
      const filterArray = filters.split('+');
      const columns = Object.values(data);
      const columnLabels = [...this.columns.filter(column => column?.visible && !column?.property?.includes(`actions`)).map(({ property }) => property) as string[]];
      if (filterArray.indexOf('----------------') === -1) {
        filterArray.forEach(filter => {
          let formattedFilter = filter.trim().toLowerCase()
          const customFilter = [];
          columns.forEach(column => {
            this.wantedSearchedValues.forEach(colProperty => {
              if (colProperty == 'all') {
                for (let key in data) {
                  let formattedDataValue = ((('' + data[key]).toLowerCase().replace(/\s+/g, ' ')).trim());
                  if (columnLabels.indexOf(key) >= 0 && (formattedDataValue.includes(formattedFilter) || formattedDataValue == (formattedFilter)) && this.applyDateFilter(data)) {
                    customFilter.push(true)
                  }
                }
              } else {
                if (colProperty === 'isActive') {
                  let formattedDataColProperty

                  if (data[colProperty]) {
                    formattedDataColProperty = 'true'
                  } else {
                    formattedDataColProperty = 'false'
                  }
                  customFilter.push(formattedDataColProperty.includes(formattedFilter) || formattedDataColProperty == formattedFilter)
                } else {
                  let formattedDataColProperty = (('' + data[colProperty]).toLowerCase().replace(/\s+/g, ' ')).trim()
                  customFilter.push(formattedDataColProperty.includes(formattedFilter) && this.applyDateFilter(data))
                }
              }
            })
          });
          matchFilter.push(customFilter.some(Boolean)); // OR
        });
      } else {
        const customFilter = [];
        if (this.applyDateFilter(data)) {
          customFilter.push(true)
        }
        matchFilter.push(customFilter.some(Boolean)); // OR
      }
      return matchFilter.every(Boolean); // AND
    }
    this.dataSource.paginator = this.paginator;
  }

  applyDateFilter(data: ReportDto): boolean {
    return (this.isValidDate(this.datesInput?.dateFrom) &&
      this.isValidDate(new Date(data?.createdDateTimeInitial)) ?
      new Date(
        new Date(data?.createdDateTimeInitial).getFullYear(),
        new Date(data?.createdDateTimeInitial).getMonth(),
        new Date(data?.createdDateTimeInitial).getDate()).getTime() >=
      new Date(
        new Date(this.datesInput?.dateFrom).getFullYear(),
        new Date(this.datesInput?.dateFrom).getMonth(),
        new Date(this.datesInput?.dateFrom).getDate()).getTime() : true) &&
      (this.isValidDate(this.datesInput?.dateTo) &&
        this.isValidDate(new Date(data?.createdDateTimeInitial)) ?
        new Date(
          new Date(data?.createdDateTimeInitial).getFullYear(),
          new Date(data?.createdDateTimeInitial).getMonth(),
          new Date(data?.createdDateTimeInitial).getDate()).getTime() <=
        new Date(
          new Date(this.datesInput?.dateTo).getFullYear(),
          new Date(this.datesInput?.dateTo).getMonth(),
          new Date(this.datesInput?.dateTo).getDate()).getTime() : true)
  }

  isValidDate(date: Date | string | number | null): boolean {
    return date !== null && date !== undefined && date !== `` && date !== `Invalid Date`;
  }

  toggleColumnVisibility({ column, event }) {
    event.stopPropagation();
    event.stopImmediatePropagation();
    column.visible = !column.visible;
  }

  trackByProperty<T>(index: number, column: TableColumn<T>) {
    return column.property;
  }

  public getSubDivisions(rowValue) {
    let subDivisions = []
    rowValue.departments.forEach(element => {
      if (element.name === 'Police-Station') {
        subDivisions.push(element.parent.value)
      } else if (element.name === 'Sub-Division') {
        subDivisions.push(element.value)
      }
    })
    return [...new Set(subDivisions)]?.join(', ');
  }

  public getPoliceStations(row) {
    let policeStations = []
    row.departments.forEach(element => {
      if (element.name === 'Police-Station') {
        policeStations.push(element.value)
      }
    })
    return [...new Set(policeStations)].join(', ')
  }

  downloadWord(type: string) {

    if (type === 'excel') {

      this.generateCombinedReportsTable();

    }


    // let formattedTableValues = []

    // const matchFields = {
    //   serialNumber: 'SERIAL NO',
    //   alternativeId: `RECORD ID`,
    //   formMasterId: `FORM ID`,
    //   divisionColumnName: `LOCATION`,
    //   subDivisionColumnName: `SUB DIVISION`,
    //   policeStationColumnName: `POLICE STATION`,
    //   currentStatusName: `CURRENT STATUS`,
    //   userName: `FILLED BY`,
    //   createdDateTime: `CREATED ON`
    // }

    // this.columns.map(column => {

    //   if (!column.visible) {

    //     for (const rowKey in matchFields) {

    //       const valueOfRow = matchFields[rowKey]

    //       if (valueOfRow.toLowerCase() === column.label.toLowerCase())
    //         delete matchFields[rowKey];

    //     }

    //   }

    // })

    // //if user filtered data then export only that data else all data
    // const dataForExport = this.dataSource.filter ? this.dataSource.connect().value : this.dataSource.data;

    // dataForExport.forEach((row: any) => {

    //   let oneRowInTable = {};

    //   for (const rowKey in matchFields) {
    //     const headerName = matchFields[rowKey];

    //     if (headerName === 'STATUS') {

    //       oneRowInTable[headerName] = row[rowKey] ? 'Active' : 'Inactive';

    //     } else {

    //       oneRowInTable[headerName] = row[rowKey];

    //     }

    //   }

    //   formattedTableValues.push(oneRowInTable);

    // })


    // this.htmlToDocxService.downloadBasedOnType(type, formattedTableValues, 'reports');
  }

  generateCombinedReportsTable() {

    const records = this.dataSource.connect().value;


    console.log(records.some(record => record.formTypeName != 'Table'))

    if (records.some(record => record.formTypeName != 'Table'))
      return;

    const s3Urls = [];

    records.forEach(record => {

      const generatedCalls = this.httpClient.get(record.s3UserInput);

      s3Urls.push(generatedCalls);
    })



    forkJoin(s3Urls).subscribe(res => {

      const tables = res.map((table: any) => table.rows);

      const allRows = [];

      tables.forEach(table => {

        allRows.push(...table);

      })

      this.downloadToExcel.downloadMasterTableToExcel(allRows, true);
    })


  }


  openSelectDatesDialog(): void {
    this.dialog.open(ReportsCalendarComponent, {
      width: `30vw`,
      height: 'min-content',
      data: {
        dateFrom: this.datesInput?.dateFrom,
        dateTo: this.datesInput?.dateTo
      },
      disableClose: false
    });
  }

  previewRecord(row: any): void {
    this.httpservice.currentRecord = row
    this.httpservice.recordCreatedBy = row.createdBy
    if (row.formTypeName === 'Table') {
      if (this.canApproveReject) {
        this.router.navigate(['forms', 'edit-record'], { queryParams: { formId: row.formId, userFormId: row.userFormId, status: row.currentStatusName, formName: row.userFormName } })
      } else if (!this.canApproveReject) {
        this.router.navigate(['forms', 'preview-record'], { queryParams: { formId: row.formId, userFormId: row.userFormId, status: row.currentStatusName, formName: row.userFormName } })
      }
    } else if (row.formTypeName === 'Letter') {
      this.router.navigate(['form-builder', 'view-form', row.formId, row.userFormId]);
    } else if (row.formTypeName === 'Card') {
      this.router.navigate(['card-builder', 'view-form', row.formId, row.userFormId]);
    }
  }

}

