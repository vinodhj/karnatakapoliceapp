import { SelectionModel } from '@angular/cdk/collections';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { ChangeDetectorRef, Component, Input, ViewChild } from '@angular/core';
import { FormArray, FormControl, Validators } from '@angular/forms';
import { MatChipInputEvent } from '@angular/material/chips';
import { MatDialog } from '@angular/material/dialog';
import { MatFormFieldDefaultOptions, MAT_FORM_FIELD_DEFAULT_OPTIONS } from '@angular/material/form-field';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { UntilDestroy } from '@ngneat/until-destroy';
import moment from 'moment';
import { Observable, ReplaySubject } from 'rxjs';
import { filter } from 'rxjs/operators';
import { fadeInUp400ms } from 'src/@exai/animations/fade-in-up.animation';
import { stagger40ms } from 'src/@exai/animations/stagger.animation';
import { TableColumn } from 'src/@exai/interfaces/table-column.interface';
import { HttpService } from 'src/@exai/services/http.service';
import { LanguageService } from 'src/app/language.service';
import { DivisionViewModel } from 'src/app/police-app/models/division-view-model';
import { HtmlToDocxService } from 'src/app/shared/services/html-to-docx.service';
import { ReportDto } from '../../models/report';
import { ReportsService } from '../../services/reports.service';
import { ReportsCalendarComponent } from '../reports-calendar/reports-calendar.component';

@Component({
  selector: 'exai-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss'],
  animations: [
    fadeInUp400ms,
    stagger40ms
  ],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        appearance: 'standard'
      } as MatFormFieldDefaultOptions
    }
  ]
})

@UntilDestroy()

export class ReportsComponent {

  layoutCtrl = new FormControl('boxed');
  subject$: ReplaySubject<ReportDto[]> = new ReplaySubject<ReportDto[]>(1);
  data$: Observable<ReportDto[]> = this.subject$.asObservable();
  customers: ReportDto[];
  formListData: ReportDto[] = [];
  departments: any[] = [];
  addDivision: FormArray;
  formLabels: Array<any>;

  datesInput: { dateFrom: Date | number | null, dateTo: Date | number | null } | null = null;

  @Input()
  columns: TableColumn<DivisionViewModel>[] = [
    { label: 'Serial NO', property: 'serialNumber', type: 'serialNumber', visible: true, cssClasses: ['font-medium'] },
    { label: 'Record Id', property: 'alternativeId', type: 'text', visible: true, cssClasses: ['font-medium'] },
    { label: 'Form Id', property: 'formMasterId', type: 'text', visible: true, cssClasses: ['font-medium'] },
    { label: 'Location', property: 'divisionColumnName', type: 'text', visible: true },
    { label: 'Sub Division', property: 'subDivisionColumnName', type: 'text', visible: true },
    { label: 'Police Station', property: 'policeStationColumnName', type: 'text', visible: true },
    { label: 'Current Status', property: 'currentStatusName', type: 'text', visible: true, cssClasses: ['font-medium'] },
    { label: 'Filled by', property: 'userName', type: 'text', visible: true, cssClasses: ['font-medium'] },
    { label: 'Created on', property: 'createdDateTime', type: 'text', visible: true, cssClasses: ['font-medium'] }
  ];

  length: number;
  pageSize: 10;
  pageSizeOptions = [5, 10, 50];

  @ViewChild(MatPaginator) paginator: MatPaginator;

  dataSource: MatTableDataSource<ReportDto> | null;
  selection = new SelectionModel<ReportDto>(true, []);
  searchCtrl = new FormControl();

  isLoading = true
  temporaryFilter: string | null = null;

  constructor(private dialog: MatDialog,
    private httpservice: HttpService,
    public languageService: LanguageService,
    private htmlToDocxService: HtmlToDocxService,
    private reportsService: ReportsService
  ) {

    this.formLabels = [
      {
        label: 'Division Name',
        type: 1,
        level: null,
        dependentIndex: null,
        initialValue: '',
      },
    ];

    this.addDivision = new FormArray([
      new FormControl('', [Validators.required]),
    ])
  }

  get visibleColumns() {
    return this.columns.filter(column => column.visible).map(column => column.property);
  }

  ngOnInit() {
    // this.getDepartments();
    this.getReports();

    this.data$.pipe(
      filter<ReportDto[]>(Boolean)
    ).subscribe(customers => {
      this.customers = customers;
      this.dataSource.data = customers;
    });

    this.subscribeToDateInputChange();
  }

  subscribeToDateInputChange(): void {
    this.reportsService.reportDatesInput.subscribe(
      data => {
        this.datesInput = data as { dateFrom: Date | number | null, dateTo: Date | number | null } | null;
        this.temporaryFilter = this.dataSource.filter;
        if (this.dataSource.filter === null || this.dataSource.filter === undefined || this.dataSource.filter === ``) {
          this.dataSource.filter = '----------------';
        } else {
          this.dataSource.filter = this.temporaryFilter;
        }
      }
    );
  }

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.columns, event.previousIndex, event.currentIndex);
  }

  addOnBlur = true;
  readonly separatorKeysCodes = [ENTER, COMMA] as const;
  searchValues = [];

  wantedSearchedValues = []

  add(event: MatChipInputEvent): void {
    this.wantedSearchedValues = []
    // Add our fruit
    if (event.value) {
      this.searchValues.push(event.value);
    }

    let wantedValues = []
    this.searchValues.forEach(el => {
      if (el.split(' :').length == 1) {
        let textForSearch = el.split(' :')[0];
        this.wantedSearchedValues.push('all')
        wantedValues.push(textForSearch)
      }
      else {
        let value = el.split(' :')[1].trim()
        this.columns.forEach(column => {
          if (column.label === el.split(' :')[0]) {
            this.wantedSearchedValues.push(column.property)
          }
        })
        wantedValues.push(value)
      }
    })
    this.dataSource.filter = wantedValues.join('+')

    // Clear the input value
    event.chipInput!.clear();
  }

  remove(value): void {
    const index = this.searchValues.indexOf(value);
    let dataSourceFilters = this.dataSource.filter.split('+')
    this.wantedSearchedValues.splice(index, 1)
    let valueSplitted = value.split(':')

    if (index >= 0) {
      this.searchValues.splice(index, 1);
      if (valueSplitted.length > 1) {
        dataSourceFilters.splice(dataSourceFilters.indexOf(valueSplitted[1].trim()), 1);
      } else {
        dataSourceFilters.splice(dataSourceFilters.indexOf(valueSplitted[0].trim()), 1);
      }
      this.dataSource.filter = dataSourceFilters.join('+')
    }

    if (this.searchValues.length < 1) {
      this.dataSource.filter = '----------------';
    }
  }

  appendToSearch(label) {
    let configuredLabelForSearch = `${label} : `
    this.searchCtrl.setValue(configuredLabelForSearch)
  }

  camelCaseToNormal(text) {
    const result = text.replace(/([A-Z])/g, " $1");
    const finalResult = result.charAt(0).toUpperCase() + result.slice(1);
    return finalResult
  }

  getReports() {
    this.isLoading = true
    this.dataSource = new MatTableDataSource<ReportDto>();
    this.httpservice.getReports().subscribe(
      (response: ReportDto[]) => {

        response.forEach((x: any, index: number) => {
          x.serialNumber = index + 1
          x[`createdDateTimeInitial`] = x.createdDateTime;
          x['createdDateTime'] = moment(x.createdDateTime).format('MMMM Do YYYY, h:mm a')
        });

        // response.forEach((row: any) => {

        //   row.subDivisionName = this.getSubDivisions(row);
        //   row.policeStationsName = this.getPoliceStations(row);

        // })

        this.isLoading = false;

        this.formListData = response;
        this.dataSource = new MatTableDataSource<ReportDto>(this.formListData);
        // advanced filtering function
        this.filter();
      });
  }

  filter(): void {
    this.dataSource.filterPredicate = (data, filters) => {
      const matchFilter = [];
      const filterArray = filters.split('+');
      const columns = Object.values(data);
      const columnLabels = [...this.columns.filter(column => column?.visible && !column?.property?.includes(`actions`)).map(({ property }) => property) as string[]];
      if (filterArray.indexOf('----------------') === -1) {
        filterArray.forEach(filter => {
          let formattedFilter = filter.trim().toLowerCase()
          const customFilter = [];
          columns.forEach(column => {
            this.wantedSearchedValues.forEach(colProperty => {
              if (colProperty == 'all') {
                for (let key in data) {
                  let formattedDataValue = ((('' + data[key]).toLowerCase().replace(/\s+/g, ' ')).trim());
                  if (columnLabels.indexOf(key) >= 0 && (formattedDataValue.includes(formattedFilter) || formattedDataValue == (formattedFilter)) && this.applyDateFilter(data)) {
                    customFilter.push(true)
                  }
                }
              } else {
                if (colProperty === 'isActive') {
                  let formattedDataColProperty

                  if (data[colProperty]) {
                    formattedDataColProperty = 'true'
                  } else {
                    formattedDataColProperty = 'false'
                  }
                  customFilter.push(formattedDataColProperty.includes(formattedFilter) || formattedDataColProperty == formattedFilter)
                } else {
                  let formattedDataColProperty = (('' + data[colProperty]).toLowerCase().replace(/\s+/g, ' ')).trim()
                  customFilter.push(formattedDataColProperty.includes(formattedFilter) && this.applyDateFilter(data))
                }
              }
            })
          });
          matchFilter.push(customFilter.some(Boolean)); // OR
        });
      } else {
        const customFilter = [];
        if (this.applyDateFilter(data)) {
          customFilter.push(true)
        }
        matchFilter.push(customFilter.some(Boolean)); // OR
      }
      return matchFilter.every(Boolean); // AND
    }
    this.dataSource.paginator = this.paginator;
  }

  applyDateFilter(data: ReportDto): boolean {
    return (this.isValidDate(this.datesInput?.dateFrom) &&
      this.isValidDate(new Date(data?.createdDateTimeInitial)) ?
      new Date(
        new Date(data?.createdDateTimeInitial).getFullYear(),
        new Date(data?.createdDateTimeInitial).getMonth(),
        new Date(data?.createdDateTimeInitial).getDate()).getTime() >=
      new Date(
        new Date(this.datesInput?.dateFrom).getFullYear(),
        new Date(this.datesInput?.dateFrom).getMonth(),
        new Date(this.datesInput?.dateFrom).getDate()).getTime() : true) &&
      (this.isValidDate(this.datesInput?.dateTo) &&
        this.isValidDate(new Date(data?.createdDateTimeInitial)) ?
        new Date(
          new Date(data?.createdDateTimeInitial).getFullYear(),
          new Date(data?.createdDateTimeInitial).getMonth(),
          new Date(data?.createdDateTimeInitial).getDate()).getTime() <=
        new Date(
          new Date(this.datesInput?.dateTo).getFullYear(),
          new Date(this.datesInput?.dateTo).getMonth(),
          new Date(this.datesInput?.dateTo).getDate()).getTime() : true)
  }

  isValidDate(date: Date | string | number | null): boolean {
    return date !== null && date !== undefined && date !== `` && date !== `Invalid Date`;
  }

  toggleColumnVisibility({ column, event }) {
    event.stopPropagation();
    event.stopImmediatePropagation();
    column.visible = !column.visible;
  }

  trackByProperty<T>(index: number, column: TableColumn<T>) {
    return column.property;
  }

  public getSubDivisions(rowValue) {
    let subDivisions = []
    rowValue.departments.forEach(element => {
      if (element.name === 'Police-Station') {
        subDivisions.push(element.parent.value)
      } else if (element.name === 'Sub-Division') {
        subDivisions.push(element.value)
      }
    })
    return [...new Set(subDivisions)]?.join(', ');
  }

  public getPoliceStations(row) {
    let policeStations = []
    row.departments.forEach(element => {
      if (element.name === 'Police-Station') {
        policeStations.push(element.value)
      }
    })
    return [...new Set(policeStations)].join(', ')
  }

  downloadWord(type: string) {

    let formattedTableValues = []

    const matchFields = {
      serialNumber: 'SERIAL NO',
      alternativeId: `RECORD ID`,
      formMasterId: `FORM ID`,
      divisionColumnName: `LOCATION`,
      subDivisionColumnName: `SUB DIVISION`,
      policeStationColumnName: `POLICE STATION`,
      currentStatusName: `CURRENT STATUS`,
      userName: `FILLED BY`,
      createdDateTime: `CREATED ON`
    }

    this.columns.map(column => {

      if (!column.visible) {

        for (const rowKey in matchFields) {

          const valueOfRow = matchFields[rowKey]

          if (valueOfRow.toLowerCase() === column.label.toLowerCase())
            delete matchFields[rowKey];

        }

      }

    })

    //if user filtered data then export only that data else all data
    const dataForExport = this.dataSource.filter ? this.dataSource.connect().value : this.dataSource.data;

    dataForExport.forEach((row: any) => {

      let oneRowInTable = {};

      for (const rowKey in matchFields) {
        const headerName = matchFields[rowKey];

        if (headerName === 'STATUS') {

          oneRowInTable[headerName] = row[rowKey] ? 'Active' : 'Inactive';

        } else {

          oneRowInTable[headerName] = row[rowKey];

        }

      }

      formattedTableValues.push(oneRowInTable);

    })


    this.htmlToDocxService.downloadBasedOnType(type, formattedTableValues, 'reports');
  }

  openSelectDatesDialog(): void {
    this.dialog.open(ReportsCalendarComponent, {
      width: `30vw`,
      height: 'min-content',
      data: {
        dateFrom: this.datesInput?.dateFrom,
        dateTo: this.datesInput?.dateTo
      },
      disableClose: false
    });
  }
}