import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import moment from 'moment';
import { dateIsOneDay, dateIsOneMonth, dateIsOneWeek, dateIsOneYear, isValidDate } from 'src/app/police-app/utils/utils';
import { ReportsService } from '../../services/reports.service';

export const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'DD-MM-YYYY',
    monthYearLabel: 'YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'YYYY',
  },
};

@Component({
  selector: 'exai-reports-calendar',
  templateUrl: './reports-calendar.component.html',
  styleUrls: ['./reports-calendar.component.scss'],
  providers: [
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ]
})
export class ReportsCalendarComponent implements OnInit {

  formGroup: FormGroup = this.formBuilder.group({
    dateFrom: [``],
    dateTo: [``]
  });
  predefinedDatesForm: FormGroup = this.formBuilder.group({
    predefinedDate: [``],
  });

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private formBuilder: FormBuilder,
    private reportsService: ReportsService
  ) { }

  ngOnInit(): void {
    this.reactOnPredefinedDateChange();
    this.patchSearchForm();
    this.emitFormValues();
  }

  emitFormValues(): void {
    this.formGroup.valueChanges.subscribe(
      value => {
        this.reportsService.sendReportInputData({
          dateFrom: isValidDate(value.dateFrom) ? new Date(value.dateFrom).getTime() : null,
          dateTo: isValidDate(value.dateTo) ? new Date(value.dateTo).getTime() : null
        })
        this.setRadioButtons(value);
      }
    );
  }

  patchSearchForm(): void {
    this.formGroup.patchValue({
      dateFrom: this.data?.dateFrom === null ? null : new Date(this.data?.dateFrom),
      dateTo: this.data?.dateTo === null ? null : new Date(this.data?.dateTo)
    });
    this.setRadioButtons({
      dateFrom: this.data?.dateFrom === null ? null : new Date(this.data?.dateFrom),
      dateTo: this.data?.dateTo === null ? null : new Date(this.data?.dateTo)
    });
  }

  clearDateFrom(): void {
    this.formGroup.get(`dateFrom`).setValue(null);
  }

  clearDateTo(): void {
    this.formGroup.get(`dateTo`).setValue(null);
  }

  reactOnPredefinedDateChange(): void {
    this.predefinedDatesForm.valueChanges.subscribe(
      value => {
        switch (value?.predefinedDate) {
          case `daily`:
            this.formGroup.patchValue({
              dateFrom: new Date(),
              dateTo: new Date()
            });
            break;
          case `weekly`:
            this.formGroup.patchValue({
              dateFrom: moment().startOf('isoWeek').toDate(),
              dateTo: new Date()
            });
            break;
          case `monthly`:
            this.formGroup.patchValue({
              dateFrom: moment().startOf('month').toDate(),
              dateTo: new Date()
            });
            break;
          case `yearly`:
            this.formGroup.patchValue({
              dateFrom: moment().startOf('year').toDate(),
              dateTo: new Date()
            });
            break;
        }
      }
    )
  }

  setRadioButtons(value: any): void {
    if (dateIsOneDay(value)) {
      this.predefinedDatesForm.get(`predefinedDate`).setValue(`daily`, { emitEvent: false });
      return;
    }
    if (dateIsOneWeek(value)) {
      this.predefinedDatesForm.get(`predefinedDate`).setValue(`weekly`, { emitEvent: false });
      return;
    }
    if (dateIsOneMonth(value)) {
      this.predefinedDatesForm.get(`predefinedDate`).setValue(`monthly`, { emitEvent: false });
      return;
    }
    if (dateIsOneYear(value)) {
      this.predefinedDatesForm.get(`predefinedDate`).setValue(`yearly`, { emitEvent: false });
      return;
    }
    this.predefinedDatesForm.get(`predefinedDate`).setValue(null, { emitEvent: false });
  }
}
