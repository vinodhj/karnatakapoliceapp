import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportsCalendarComponent } from './reports-calendar.component';

describe('ReportsCalendarComponent', () => {
  let component: ReportsCalendarComponent;
  let fixture: ComponentFixture<ReportsCalendarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReportsCalendarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportsCalendarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
