import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Validators } from 'ngx-editor';
import { LanguageService } from 'src/app/language.service';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import moment from 'moment';
import { dateIsOneDay, dateIsOneMonth, dateIsOneWeek, dateIsOneYear, isValidDate } from 'src/app/police-app/utils/utils';
import { combineLatest, Observable } from 'rxjs';
import { ReportsService } from '../../services/reports.service';
import { Router } from '@angular/router';
import { TemplateDropdownOption } from '../../models/template-dropdown-option';
import { HttpService } from 'src/@exai/services/http.service';
import { map } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material/snack-bar';

export const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'DD-MM-YYYY',
    monthYearLabel: 'YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'YYYY',
  },
};

@Component({
  selector: 'exai-reports-dialog',
  templateUrl: './reports-dialog.component.html',
  styleUrls: ['./reports-dialog.component.scss'],
  providers: [
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ]
})
export class ReportsDialogComponent implements OnInit {

  formGroup: FormGroup = this.fb.group({
    formName: [``],
    formId: [``],
    dateFrom: [``],
    dateTo: [``]
  });
  predefinedDatesForm: FormGroup = this.fb.group({
    predefinedDate: [``],
  });
  isLoading: boolean = false;
  invalidForm = true;
  dialogTitle: string = `Search reports`;
  templateDropdownOptions: TemplateDropdownOption[] = [];
  filteredTemplateDropdownOptions: Observable<TemplateDropdownOption[]>;

  constructor(
    public dialogRef: MatDialogRef<ReportsDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public languageService: LanguageService,
    private fb: FormBuilder,
    private reportsService: ReportsService,
    private router: Router,
    private httpService: HttpService,
    private snackbar: MatSnackBar
  ) { }

  ngOnInit(): void {
    this.subscribeOnFormNameChange();
    this.filterOptions();
    this.getTemplateOptions();
    this.subscribeOnFormIdChange();
  }

  subscribeOnFormIdChange() {
    this.formGroup.get('formId').valueChanges.subscribe(value => {

      const findForm = this.templateDropdownOptions.find(template => template.alternativeId === value);

      if(!findForm) {
        this.formGroup.get('formName').setValue(null);
      } else {

        this.formGroup.get('formName').setValue(findForm);
      
      }


    })
  }

  subscribeOnFormNameChange(): void {
    this.formGroup.get(`formName`).valueChanges.subscribe(
      value => {

        if(value == null)
          return;


        this.formGroup.get('formId').setValue(value?.alternativeId, {emitEvent: false, onlySelf: true});
      }
    );
  }

  checkValueOnBlur() {
    //if user typed only string then remove selection from input

    if(typeof this.formGroup.get('formName').value === 'string') {
      this.snackbar.open('You need to select name from dropdown', 'Ok', {
        duration: 2500
      })
      this.formGroup.get('formName').setValue(null, {emitEvent: false, onlySelf: true}) 
    }

  }

  checkValue(value) {
    return value ? value.formName : null;
  }

  getTemplateOptions(): void {
    this.httpService.getTemplateOptions().subscribe(
      response => this.templateDropdownOptions = [...response as TemplateDropdownOption[]],
      error => console.error(error),
      () => {
        this.reactOnPredefinedDateChange();
        this.subscribeOnValidation();
        this.reactOnFormDateChange();
      }
    )
  }

  filterOptions(): void {
    this.filteredTemplateDropdownOptions = this.formGroup.get(`formName`).valueChanges.pipe(
      map(value => (typeof value === `string` ? value : value?.name)),
      map(name => (name ? this._filter(name) : this.templateDropdownOptions.slice())),
    );
  }

  private _filter(name: string): TemplateDropdownOption[] {
    const filterValue = name.toLowerCase();
    return this.templateDropdownOptions.filter(option => option.formName.toLowerCase().includes(filterValue));
  }

  reactOnFormDateChange(): void {
    this.formGroup.valueChanges.subscribe(
      value => {
        this.setRadioButtons({
          dateFrom: isValidDate(value.dateFrom) ? new Date(value.dateFrom).getTime() : null,
          dateTo: isValidDate(value.dateTo) ? new Date(value.dateTo).getTime() : null
        });
      }
    )
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  onSubmitForm(): void {
    if (!this.formGroup.valid || this.invalidForm) {
      this.formGroup.get(`formName`).markAsTouched();
      this.formGroup.get(`formId`).markAsTouched();
      this.formGroup.get(`dateTo`).markAsTouched();
      this.formGroup.get(`dateFrom`).markAsTouched();
      return;
    }

    this.reportsService.sendReportInputData({
      dateFrom: isValidDate(this.formGroup.get(`dateFrom`).value) ?
        new Date(this.formGroup.get(`dateFrom`).value).getTime() : null,
      dateTo: isValidDate(this.formGroup.get(`dateTo`).value) ?
        new Date(this.formGroup.get(`dateTo`).value).getTime() : null
    });
    let formId = this.formGroup.get(`formId`)?.value === null || this.formGroup.get(`formId`)?.value === undefined ? `` : this.formGroup.get(`formId`)?.value;

    const dateFrom = isValidDate(this.formGroup.get(`dateFrom`).value) ? new Date(this.formGroup.get(`dateFrom`).value).getTime() : ``;
    const dateTo = isValidDate(this.formGroup.get(`dateTo`).value) ? new Date(this.formGroup.get(`dateTo`).value).getTime() : ``;
    this.router.navigate([`reports`], { queryParams: { formId, dateFrom, dateTo } });
    this.dialogRef.close();
  }

  subscribeOnValidation(): void {
    combineLatest([
      this.formGroup.get(`formName`).valueChanges,
      this.formGroup.get(`formId`).valueChanges
    ]).subscribe(
      ([formName, formId]) => {
        if (
          (formName === null ||
            formName === undefined ||
            formName === ``) &&
          (formId === null ||
            formId === undefined ||
            formId === ``)
        ) {
          this.invalidForm = true;
        } else {
          this.invalidForm = false;
        }
      }
    );
    this.formGroup.get(`formName`).setValue(null, { emitEvent: true, onlySelf: true });
    this.formGroup.get(`formId`).setValue(null, { emitEvent: true, onlySelf: true });
  }

  clearDateFrom(): void {
    this.formGroup.get(`dateFrom`).setValue(null);
    this.setRadioButtons({
      dateFrom: null,
      dateTo: this.formGroup.get(`dateTo`).value === null ?
        null : new Date(this.formGroup.get(`dateTo`).value)
    });
  }

  clearDateTo(): void {
    this.formGroup.get(`dateTo`).setValue(null);
    this.setRadioButtons({
      dateFrom: this.formGroup.get(`dateFrom`).value === null ?
        null : new Date(this.formGroup.get(`dateFrom`).value),
      dateTo: null
    });
  }

  reactOnPredefinedDateChange(): void {
    this.predefinedDatesForm.valueChanges.subscribe(
      value => {
        switch (value?.predefinedDate) {
          case `daily`:
            this.formGroup.patchValue({
              dateFrom: new Date(),
              dateTo: new Date()
            });
            break;
          case `weekly`:
            this.formGroup.patchValue({
              dateFrom: moment().startOf('isoWeek').toDate(),
              dateTo: new Date()
            });
            break;
          case `monthly`:
            this.formGroup.patchValue({
              dateFrom: moment().startOf('month').toDate(),
              dateTo: new Date()
            });
            break;
          case `yearly`:
            this.formGroup.patchValue({
              dateFrom: moment().startOf('year').toDate(),
              dateTo: new Date()
            });
            break;
        }
      }
    )
  }

  setRadioButtons(value: any): void {
    if (dateIsOneDay(value)) {
      this.predefinedDatesForm.get(`predefinedDate`).setValue(`daily`, { emitEvent: false });
      return;
    }
    if (dateIsOneWeek(value)) {
      this.predefinedDatesForm.get(`predefinedDate`).setValue(`weekly`, { emitEvent: false });
      return;
    }
    if (dateIsOneMonth(value)) {
      this.predefinedDatesForm.get(`predefinedDate`).setValue(`monthly`, { emitEvent: false });
      return;
    }
    if (dateIsOneYear(value)) {
      this.predefinedDatesForm.get(`predefinedDate`).setValue(`yearly`, { emitEvent: false });
      return;
    }
    this.predefinedDatesForm.get(`predefinedDate`).setValue(null, { emitEvent: false });
  }

  showConfirm(): void {
    console.log(`confirmation`);
  }
}
