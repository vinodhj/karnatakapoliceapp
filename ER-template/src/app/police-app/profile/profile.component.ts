import { Component, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { finalize } from 'rxjs/operators';
import { HttpService } from 'src/@exai/services/http.service';
import { LanguageService } from 'src/app/language.service';
import { ErrorMessage } from 'src/app/shared/models/error-message';
import { noWhitespaceValidator } from 'src/app/shared/models/no-whitespace.validator';
import { PROFILE_ERROR_MESSAGES } from 'src/app/shared/models/profile-error-messages';
import { UserProfile } from 'src/app/shared/models/user-profile';
import { UserProfileInput } from 'src/app/shared/models/user-profile-input';
import { SnackBarService } from 'src/app/shared/services/snack-bar.service';
import { regexEmail } from '../manage-user/interfaces/emailPasswordRegex.interface';
import { EditPasswordDialogComponent } from './edit-password-dialog/edit-password-dialog.component';

@Component({
  selector: 'exai-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})

export class ProfileComponent {

  profileForm: FormGroup = this.formBuilder.group({
    firstName: [``, [Validators.required, Validators.maxLength(64), noWhitespaceValidator()]],
    lastName: [``, [Validators.required, Validators.maxLength(64), noWhitespaceValidator()]],
    phone: [` `, [Validators.required, Validators.maxLength(13), Validators.pattern("^[\+]?[0-9]*$")]],
    email: [{ value: ``, disabled: true }, [Validators.required, Validators.maxLength(128), Validators.pattern(regexEmail)]],
    role: [{ value: ``, disabled: true }, [Validators.required, Validators.maxLength(64)]],
    division: [{ value: ``, disabled: true }, [Validators.required, Validators.maxLength(1000)]],
    subDivision: [{ value: ``, disabled: true }, [Validators.required, Validators.maxLength(1000)]],
    policeStation: [{ value: ``, disabled: true }, [Validators.required, Validators.maxLength(1000)]],
    password: [{ value: `xxxxxxxxxx`, disabled: true }, Validators.required]
  });
  errorMessages: ErrorMessage[] = PROFILE_ERROR_MESSAGES;
  profile: any | null = null;
  loadingData: boolean;

  @ViewChild('phoneNumber') public phoneNumber

  constructor(
    private formBuilder: FormBuilder,
    private httpService: HttpService,
    private dialog: MatDialog,
    private snackBarService: SnackBarService,
    public languageService: LanguageService
  ) { }

  ngOnInit(): void {
    this.getProfile();
  }

  public ngAfterViewChecked() {
    // disabling country selection button
    try {
      this.phoneNumber.elRef.nativeElement.firstChild.children[0].disabled = 'true';
    }
    catch (e) { //ignore this
    
    }

  }

  getProfile(): void {
    this.loadingData = true;

    this.httpService.getUserProfile().pipe(
      finalize(() => this.loadingData = false)
    ).subscribe(
      response => this.profile = response as UserProfile,
      error => console.error(error),
      () => this.patchProfileForm()
    );
  }

  saveProfile(): void {
    if (this.profileForm.invalid)
      return;
    const input = this.initSaveInput();
    this.httpService.saveProfile(input).subscribe(
      response => {
        if (response === null || response === undefined) {
          this.snackBarService.showMessage("Error occured!", 3000);
        }
        else {
          this.snackBarService.showMessage(`User ${this.profileForm.get('firstName').value} ${this.profileForm.get('lastName').value} Updated Successfully`, 3000);
          this.profileForm.patchValue({
            firstName: response?.firstname,
            lastName: response?.lastname,
            phone: response?.phone
          });
        }
      }
    );
  }

  initSaveInput(): UserProfileInput {
    const input = new UserProfileInput();
    input.firstName = this.profileForm.get(`firstName`)?.value;
    input.lastName = this.profileForm.get(`lastName`)?.value;
    input.phone = this.profileForm.get(`phone`)?.value?.toString();
    return input;
  }

  patchProfileForm(): void {
    if (this.profile === null || this.profile === undefined)
      return;
    this.profileForm.patchValue({
      firstName: this.profile.firstName,
      lastName: this.profile.lastName,
      phone: this.profile.phone,
      email: this.profile.email,
      role: this.profile.roleName,
      division: this.getDivisionNames()      
    });
    if (this.profile.roleId > 6)
      this.profileForm.patchValue({
        policeStation: this.getPoliceStationNames()
      });
    if (this.profile.roleId > 5)
      this.profileForm.patchValue({
        subDivision: this.getSubDivisionNames()
      });
  }

  getDivisionNames(): string {
    return this.profile.policeStations.map((policeStation) => policeStation?.divisionName).join(`, `);
  }

  getSubDivisionNames(): string {
    return this.profile.policeStations.map((policeStation) => policeStation?.subDivisionName).join(`, `);
  }

  getPoliceStationNames(): string {
    return this.profile.policeStations.map(({ policeStationName }) => policeStationName).join(`, `);
  }

  getErrorMessages(control: string): string | null {
    const messages: { key: string; message: string; }[] = [];
    this.errorMessages.map(item => {
      if (item?.formControlName === control)
        if (item?.errors?.length > 0)
          Object.keys(this.profileForm.get(control).errors).map(error => {
            const errorMessage = item?.errors?.find(errorMessage => errorMessage?.key === error);
            if (errorMessage !== null && errorMessage !== undefined)
              messages.push(errorMessage);
          });
    });
    if (messages?.length > 0)
      return messages.map(({ message }) => message).join(` `);
  }

  openEditPasswordDialog(): void {
    let dialogConfiguration: MatDialogConfig = {
      width: `35vw`,
      height: `min-content`,
      disableClose: false,
      data: {}
    };
    this.dialog.open(EditPasswordDialogComponent, dialogConfiguration);
  }
}
