import { Component, Inject, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { HttpService } from 'src/@exai/services/http.service';
import { ErrorMessage } from 'src/app/shared/models/error-message';
import { PASSWORD_ERROR_MESSAGES } from 'src/app/shared/models/profile-error-messages';
import { regexPassword } from '../../manage-user/interfaces/emailPasswordRegex.interface';
import { UserProfilePassword } from 'src/app/shared/models/user-profile-password';
import { SnackBarService } from 'src/app/shared/services/snack-bar.service';
import { noWhitespaceValidator } from 'src/app/shared/models/no-whitespace.validator';

@Component({
  selector: 'exai-edit-password-dialog',
  templateUrl: './edit-password-dialog.component.html',
  styleUrls: ['./edit-password-dialog.component.scss']
})

export class EditPasswordDialogComponent implements OnInit {

  passwordForm: FormGroup;
  errorMessages: ErrorMessage[] = PASSWORD_ERROR_MESSAGES;

  constructor(private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<EditPasswordDialogComponent>,
    private httpService: HttpService,
    private snackBarService: SnackBarService,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
    this.passwordForm = this.formBuilder.group({
      password: [``, [Validators.required, Validators.pattern(regexPassword), noWhitespaceValidator()]],
      newPassword: [``, [Validators.required, Validators.pattern(regexPassword), noWhitespaceValidator()]],
      repeatNewPassword: [``, [Validators.required, Validators.pattern(regexPassword), noWhitespaceValidator()]]
    }, { validators: this.checkPasswords });
  }

  checkPasswords: ValidatorFn = (group: AbstractControl): ValidationErrors | null => {
    let newPassword = group.get(`newPassword`).value;
    let repeatNewPassword = group.get(`repeatNewPassword`).value;
    return newPassword === repeatNewPassword ? null : { notSame: true };
  }

  getErrorMessages(control: string): string | null {
    const messages: { key: string; message: string; }[] = [];
    this.errorMessages.map(item => {
      if (item?.formControlName === control)
        if (item?.errors?.length > 0)
          Object.keys(this.passwordForm.get(control).errors).map(error => {
            const errorMessage = item?.errors?.find(errorMessage => errorMessage?.key === error);
            if (errorMessage !== null && errorMessage !== undefined)
              messages.push(errorMessage);
          });
    });
    if (messages?.length > 0)
      return messages.map(({ message }) => message).join(` `);
  }

  savePassword(): void {
    const input = this.initSaveInput();
    this.httpService.savePassword(input).subscribe(
      saved => {
        saved ? this.snackBarService.showMessage("Successful!", 3000) : this.snackBarService.showMessage("Error occured!", 3000)
        this.dialogRef.close();
      },
      error => this.snackBarService.showMessage(`Current password is not valid!`, 3000)
    );
  }

  initSaveInput(): UserProfilePassword {
    const input = new UserProfilePassword();
    input.currentPassword = this.passwordForm.get(`password`)?.value;
    input.newPassword = this.passwordForm.get(`newPassword`)?.value;
    input.confirmNewPassword = this.passwordForm.get(`repeatNewPassword`)?.value?.toString();
    return input;
  }

  onCancel() {
    this.dialogRef.close();
  }

}
