import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CustomLayoutComponent } from '../custom-layout/custom-layout.component';
import { AuthenticationGuard } from '../guards/authentication.guard';
import { Role } from '../shared/enums/role.enum';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProfileComponent } from './profile/profile.component';

const routes: Routes = [
  {
    path: ``, component: CustomLayoutComponent,
    children: [
      {
        path: `forms`,
        loadChildren: () => import(`./forms/forms.module`).then(m => m.FormModule),
        canActivate: [AuthenticationGuard],
        data: {
          roles: [
            Role.SuperAdmin,
            Role.Admin,
            Role.StateHead,
            Role.DistrictHead,
            Role.DivisionHead,
            Role.SubDivision,
            Role.PoliceStation,
            Role.User
          ]
        }
      },
      {
        path: `primary-keys`,
        loadChildren: () => import(`./primary-key/primary-key.module`).then(m => m.PrimaryKeyModule),
        canActivate: [AuthenticationGuard],
        data: {
          roles: [
            Role.SuperAdmin,
            Role.Admin,
            Role.StateHead,
            Role.DistrictHead
          ]
        }
      },
      {
        path: `reports`,
        loadChildren: () => import(`./reports/reports.module`).then(m => m.ReportsModule),
        canActivate: [AuthenticationGuard],
        data: {
          roles: [
            Role.SuperAdmin,
            Role.Admin,
            Role.StateHead,
            Role.DistrictHead,
            Role.DivisionHead,
            Role.SubDivision,
            Role.PoliceStation,
            Role.User
          ]
        }
      },
      {
        path: `manage-user`,
        loadChildren: () => import(`./manage-user/manage-user.module`).then(m => m.ManageUserModule),
        canActivate: [AuthenticationGuard],
        data: {
          roles: [
            Role.SuperAdmin,
            Role.Admin,
            Role.StateHead,
            Role.DistrictHead,
            Role.DivisionHead,
            Role.SubDivision,
            Role.PoliceStation
          ]
        }
      },
      {
        path: `manage-dept`,
        loadChildren: () => import(`./manage-dept/manage-dept.module`).then(m => m.ManageDeptModule),
        canActivate: [AuthenticationGuard],
        data: {
          roles: [
            Role.SuperAdmin,
            Role.Admin,
            Role.StateHead,
            Role.DistrictHead,
            Role.DivisionHead,
            Role.SubDivision,
            Role.PoliceStation
          ]
        }
      },
      {
        path: `form-builder`,
        loadChildren: () => import('../form-builder/form-builder.module').then(m => m.FormBuilderModule),
        canActivate: [AuthenticationGuard],
        data: {
          roles: [
            Role.SuperAdmin,
            Role.Admin,
            Role.StateHead,
            Role.DistrictHead,
            Role.DivisionHead,
            Role.SubDivision,
            Role.PoliceStation,
            Role.User
          ]
        }
      },
      {
        path: `card-builder`,
        loadChildren: () => import('../card-builder/card-builder.module').then(m => m.CardBuilderModule),
        canActivate: [AuthenticationGuard],
        data: {
          roles: [
            Role.SuperAdmin,
            Role.Admin,
            Role.StateHead,
            Role.DistrictHead,
            Role.DivisionHead,
            Role.SubDivision,
            Role.PoliceStation,
            Role.User
          ]
        }
      },
      {
        path: "applicable-forms",
        loadChildren: () => import(`./applicable-forms/applicable-forms.module`).then(m => m.ApplicationFormsModule),
        canActivate: [AuthenticationGuard],
        data: {
          roles: [
            Role.SuperAdmin,
            Role.Admin,
            Role.StateHead,
            Role.DistrictHead,
            Role.DivisionHead,
            Role.SubDivision,
            Role.PoliceStation,
            Role.User
          ]
        }
      },
      {
        path: `submitted-forms`,
        loadChildren: () => import(`./submitted-form/submitted-form.module`).then(m => m.SubmittedFormsModule),
        canActivate: [AuthenticationGuard],
        data: {
          roles: [
            Role.SuperAdmin,
            Role.Admin,
            Role.StateHead,
            Role.DistrictHead,
            Role.DivisionHead,
            Role.SubDivision,
            Role.PoliceStation,
            Role.User
          ]
        }
      },
      {
        path: `profile`,
        component: ProfileComponent,
        canActivate: [AuthenticationGuard],
        data: {
          roles: [
            Role.SuperAdmin,
            Role.Admin,
            Role.StateHead,
            Role.DistrictHead,
            Role.DivisionHead,
            Role.SubDivision,
            Role.PoliceStation,
            Role.User
          ]
        }
      },
      {
        path: `dashboard`,
        component: DashboardComponent,
        canActivate: [AuthenticationGuard],
        data: {
          roles: [
            Role.SuperAdmin,
            Role.Admin,
            Role.StateHead,
            Role.DistrictHead,
            Role.DivisionHead,
            Role.SubDivision,
            Role.PoliceStation,
            Role.User
          ]
        }
      }
    ]
  },
  {
    path: `**`,
    redirectTo: `dashboard`
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class PoliceAppRoutingModule { }