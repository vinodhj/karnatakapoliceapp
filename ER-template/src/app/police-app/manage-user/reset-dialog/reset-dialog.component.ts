import { HttpErrorResponse } from '@angular/common/http';
import { Component, Inject, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, FormGroupDirective, NgForm, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { HttpService } from 'src/@exai/services/http.service';
import { LanguageService } from 'src/app/language.service';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const invalidCtrl = !!(control && control.invalid && control.parent.dirty);
    const invalidParent = !!(control && control.parent && control.parent.invalid && control.parent.dirty);

    return (invalidCtrl || invalidParent);
  }
}

@Component({
  selector: 'exai-reset-dialog',
  templateUrl: './reset-dialog.component.html',
  styleUrls: ['./reset-dialog.component.scss']
})
export class ResetDialogComponent implements OnInit {
  form: FormGroup
  userID
  username: any;
  policestation: any;
  subdivision: any;
  division: any;
  selectedData: any;
  matcher = new MyErrorStateMatcher();
  constructor(public dialogRef: MatDialogRef<ResetDialogComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: any, private http: HttpService,
    private _snackbar: MatSnackBar,
    private fb: FormBuilder,
    public languageService: LanguageService) {
    console
    this.userID = this.data.id;

  }

  checkPasswords: ValidatorFn = (group: AbstractControl): ValidationErrors | null => {
    let pass = group.get('password').value;
    let confirmPass = group.get('confirmPassword').value
    return pass === confirmPass ? null : { notSame: true }
  }
  ngOnInit(): void {
    this.form = this.fb.group({
      password: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(25)]],
      confirmPassword: ['']
    }, { validators: this.checkPasswords })
  }
  cancel() {
    this.dialogRef.close()
  }
  user
  submit() {
    let userpassword = {
      id: this.userID,
      newPassword: this.form.value.password
    }

    this.http.resetPassword(userpassword).subscribe(res => {
      console.log(res)

      if (res) {
        this.dialogRef.close()
        this._snackbar.open('Save Sucessfully', 'OKAY', {
          duration: 1000,
        })
      }
    }, (error: HttpErrorResponse) => {
      this._snackbar.open(`${error.error.message}`, 'OKAY', {
        duration: 2000,
      })
      this.dialogRef.close()
    })
  }

}


