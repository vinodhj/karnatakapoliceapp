import { SelectionModel } from '@angular/cdk/collections';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { DatePipe } from '@angular/common';
import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatChipInputEvent } from '@angular/material/chips';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatFormFieldDefaultOptions, MAT_FORM_FIELD_DEFAULT_OPTIONS } from '@angular/material/form-field';
import { MatPaginator } from '@angular/material/paginator';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import icpassword from '@iconify/icons-ic/twotone-password';
import { UntilDestroy } from '@ngneat/until-destroy';
import { error } from 'console';
import { Observable, ReplaySubject } from 'rxjs';
import { filter, finalize } from 'rxjs/operators';
import { TableColumn } from 'src/@exai/interfaces/table-column.interface';
import { HttpService } from 'src/@exai/services/http.service';
import { LanguageService } from 'src/app/language.service';
import { ConfirmDialogComponent } from 'src/app/shared/advanced-grid-item/dialogs/confirm-dialog/confirm-dialog.component';
import { BasicTableComponent } from 'src/app/shared/basic-table/basic-table.component';
import { Role } from 'src/app/shared/enums/role.enum';
import { HtmlToDocxService } from 'src/app/shared/services/html-to-docx.service';
import { SnackBarService } from 'src/app/shared/services/snack-bar.service';
import { AddUserDialogComponent } from '../add-user-dialog/add-user-dialog.component';
import { BulkDialogComponent } from '../bulk-dialog/bulk-dialog.component';
import { EditDialogNewComponent } from '../edit-dialog-new/edit-dialog-new.component';
import { EditDialogComponent } from '../edit-dialog/edit-dialog.component';
import { ResetDialogComponent } from '../reset-dialog/reset-dialog.component';
import { crumb } from './interfaces/crumb.interface';
import { IUser } from './interfaces/iUser.interface';


@UntilDestroy()
@Component({
  selector: 'exai-user-manage',
  templateUrl: './user-manage.component.html',
  styleUrls: ['./user-manage.component.scss'],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        appearance: 'standard'
      } as MatFormFieldDefaultOptions
    }
  ]
})
export class UserManageComponent implements OnInit {
  isActive: boolean = true
  layoutCtrl = new FormControl('boxed');
  subject$: ReplaySubject<IUser[]> = new ReplaySubject<IUser[]>(1);
  data$: Observable<IUser[]> = this.subject$.asObservable();
  IUsers: IUser[];

  @Input()
  columns: TableColumn<IUser>[] = [
    // { label: 'Checkbox', property: 'checkbox', type: 'checkbox', visible: true },
    { label: 'Serial NO', property: 'serialNumber', type: 'serialNumber', visible: true, cssClasses: ['font-medium'] },
    { label: 'Id', property: 'id', type: 'text', visible: true, cssClasses: ['font-medium'] },
    { label: 'Role Name', property: 'roleName', type: 'text', visible: true, cssClasses: ['font-medium'] },
    { label: 'User Name', property: 'UserName', type: 'text', visible: true },
    { label: 'Police Station', property: 'Policestation', type: 'text', visible: true },
    { label: 'Sub Division', property: 'Subdivision', type: 'text', visible: true },
    { label: 'Location', property: 'Location', type: 'text', visible: true, cssClasses: ['text-secondary', 'font-medium'] },
    { label: 'First Name', property: 'firstname', type: 'text', visible: false },
    { label: 'Last Name', property: 'lastname', type: 'text', visible: false },
    { label: 'Phone Number', property: 'phone', type: 'text', visible: false },
    { label: 'Email', property: 'emailId', type: 'text', visible: false },
    { label: 'Created On', property: 'createdDatetime', type: 'text', visible: false },
    { label: 'Created By', property: 'createdByUsername', type: 'text', visible: false },
    { label: 'Modified By', property: 'modifiedByUsername', type: 'text', visible: false },
    { label: 'Modified On', property: 'modifiedDatetime', type: 'text', visible: false },
    { label: 'Status', property: 'isActive', type: 'button', visible: true },
    { label: 'Action', property: 'actions', type: 'button', visible: true },
  ];

  dataSource: MatTableDataSource<IUser> | null;
  selection = new SelectionModel<IUser>(true, []);
  searchCtrl = new FormControl();

  length: number;
  pageSize: number = 10;
  pageSizeOptions = [5, 10, 50];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  isvarplock = icpassword

  isLoading = true

  constructor(private dialog: MatDialog,
    private httpservice: HttpService,
    public languageService: LanguageService,
    private htmlToDocxService: HtmlToDocxService,
    private snackBarService: SnackBarService
  ) { }

  get visibleColumns() {
    return this.columns.filter(column => column.visible).map(column => column.property);
  }

  userRole = this.httpservice.getRole();

  user: Array<IUser> = []

  pipe: any = new DatePipe('en-US'); // Use your own locale

  tabledata() {
    this.isLoading = true;

    this.user = [];
    this.httpservice.getUsers().pipe(
      finalize(() => this.isLoading = false)
    ).subscribe((response: any) => {

      this.isLoading = false
      for (let i = 0; i < response.length; i++) {
        let newUsers: Array<IUser> = [];
        let newUser = <IUser>{
          id: response[i].id,
          UserName: response[i].username,
          roleID: response[i].roleId,
          isActive: response[i].isActive,
          firstname: response[i].firstname,
          lastname: response[i].lastname,
          phone: response[i].phone,
          emailId: response[i].emailId,
          PolicestationId: response[i].policeStationId,
          roleName: response[i].roleNames,
          policeStationName: response[i].policeStationName,
          createdByUsername: response[i].createdByUsername,
          modifiedByUsername: response[i].modifiedByUsername,
          canDeleteUser: response[i].canDeleteUser,
          createdDatetime: response[i]?.createdDatetime,
          modifiedDatetime: response[i]?.modifiedDatetime,
          serialNumber: i + 1
        };

        newUser.Subdivision = response[i].subDivisionName;
        newUser.SubdivisionId = response[i].subDivisionId;

        let policeStationConcatinated = '';

        response[i].userToDepartmentMapping.map((department, index) => {

          policeStationConcatinated += (index >= 1 ? ', ' : '') + department.policeStation.name;

        })

        newUser.Policestation = policeStationConcatinated;
        newUser.Location = response[i].divisionName

        if (response[i].userRole[0].roleId == 6) {
          newUser.Policestation = null;
        }
        else if (response[i].userRole[0].roleId < 6 && response[i].userRole[0].roleId >= 3) {
          newUser.Subdivision = null;
          newUser.Policestation = null;
        }
        else if (response[i].userRole[0].roleId < 3) {
          newUser.Subdivision = null;
          newUser.Policestation = null;
          newUser.Location = null;
        }

        // newUsers.push(<IUser>{UserName:response[i].username});
        // for (let o of response[i].userToDepartmentMapping) {
        //   newUser.Policestation = o.policeStation.value;
        //   newUser.Subdivision = o.policeStation.parent.value;
        //   newUser.SubdivisionId = o.policeStation.parent?.id;
        //   newUser.Location = o.policeStation.parent.parent?.value;
        //   newUser.LocationId = o.policeStation.parent.parent?.id;
        //   newUser.PolicestationId = o.policeStationId;
        //   newUser.PolicestationObj = o.policeStation;
        //   newUser.SubdivisionObj = o.policeStation.parent;
        //   newUser.LocationObj = o.policeStation.parent.parent;
        // }

        newUsers.push(newUser);

        this.user = [...this.user, ...newUsers];

      }

      this.dataSource = new MatTableDataSource(this.user);
      this.dataSource.paginator = this.paginator;

      // advanced filtering function
      this.dataSource.filterPredicate = (data, filters) => {
        const matchFilter = [];
        const filterArray = filters.split('+');
        const columns = Object.values(data);
        filterArray.forEach(filter => {
          let formattedFilter = filter.toLowerCase().trim()
          const customFilter = [];
          console.log(this.wantedSearchedValues);
          const columnLabels = [...this.columns.filter(column => column?.visible && !column?.property?.includes(`actions`)).map(({ property }) => property) as string[]];
          columns.forEach(column => {
            this.wantedSearchedValues.forEach(colProperty => {
              if (colProperty == 'all') {
                for (let key in data) {
                  let formattedDataValue = ((('' + data[key]).toLowerCase().replace(/\s+/g, ' ')).trim())
                  // console.log(formattedDataValue);
                  // console.log(formattedFilter);

                  if (columnLabels.indexOf(key) >= 0 && (formattedDataValue.includes(formattedFilter) || formattedDataValue == (formattedFilter))) {
                    customFilter.push(true)
                  }
                }
              } else {
                if (colProperty === 'isActive') {
                  let formattedDataColProperty

                  if (data[colProperty]) {
                    formattedDataColProperty = 'true'
                  } else {
                    formattedDataColProperty = 'false'
                  }

                  customFilter.push(formattedDataColProperty.includes(formattedFilter) || formattedDataColProperty == formattedFilter)
                } else {
                  let formattedDataColProperty = (('' + data[colProperty]).toLowerCase().replace(/\s+/g, ' ')).trim()
                  if (colProperty === 'divisionname') {
                    formattedDataColProperty = data["subdivision"].division.name.toString().toLowerCase().replace(/\s+/g, ' ').trim()
                  } else if (colProperty === 'subdivisionname') {
                    formattedDataColProperty = data["subdivision"].name.toString().toLowerCase().replace(/\s+/g, ' ').trim()
                  }

                  customFilter.push(formattedDataColProperty.includes(formattedFilter))
                }
              }
            })
          });
          matchFilter.push(customFilter.some(Boolean)); // OR
        });
        return matchFilter.every(Boolean); // AND
      }
    });

    this.data$.pipe(
      filter<IUser[]>(Boolean)
    ).subscribe(IUsers => {
      this.IUsers = IUsers;
      this.dataSource.data = IUsers;
    });
  }
  ngOnInit() {
    this.tabledata();
  }
  
  get canBulkImport() {
    return [Role.SuperAdmin].includes(this.userRole);
  }

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.columns, event.previousIndex, event.currentIndex);
  }

  addOnBlur = true;
  readonly separatorKeysCodes = [ENTER, COMMA] as const;
  searchValues = [];
  wantedSearchedValues = []

  add(event: MatChipInputEvent): void {
    this.wantedSearchedValues = []
    if (event.value) {
      this.searchValues.push(event.value);
    }

    let wantedValues = []
    this.searchValues.forEach(el => {
      if (el.split(' :').length == 1) {
        let textForSearch = el.split(' :')[0];
        this.wantedSearchedValues.push('all')
        wantedValues.push(textForSearch)
      } else {
        let value = el.split(' :')[1].trim()
        this.columns.forEach(column => {
          if (column.label === el.split(' :')[0]) {
            this.wantedSearchedValues.push(column.property)
          }
        })
        wantedValues.push(value)
      }
    })
    this.dataSource.filter = wantedValues.join('+')
    console.log(this.dataSource.filter);

    // Clear the input value
    event.chipInput!.clear();
  }

  remove(value): void {
    const index = this.searchValues.indexOf(value);
    let dataSourceFilters = this.dataSource.filter.split('+')
    this.wantedSearchedValues.splice(index, 1)
    let valueSplitted = value.split(':')

    if (index >= 0) {
      this.searchValues.splice(index, 1);
      if (valueSplitted.length > 1) {
        dataSourceFilters.splice(dataSourceFilters.indexOf(valueSplitted[1].trim()), 1);
      } else {
        dataSourceFilters.splice(dataSourceFilters.indexOf(valueSplitted[0].trim()), 1);
      }
      this.dataSource.filter = dataSourceFilters.join('+')
    }

    if (this.searchValues.length < 1) {
      this.dataSource.filter = ''
    }
  }

  appendToSearch(label) {
    let configuredLabelForSearch = `${label} : `
    this.searchCtrl.setValue(configuredLabelForSearch)
  }


  openEdit(row: crumb) {
    console.log(row);
    const dialogRef = this.dialog.open(EditDialogComponent, {
      data: row,
      disableClose: true
    })
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.tabledata()
    });
  }

  openEditNew(row: crumb): void {

    let dialogConfiguration: MatDialogConfig = {
      width: '40%',
      minWidth: '30rem',
      maxHeight: '80vh',
      disableClose: true,
      data: {
        editing: true,
        ...row
      }
    };

    const dialogRef = this.dialog.open(EditDialogNewComponent, dialogConfiguration);

    dialogRef.afterClosed().subscribe(res => {
      if (res)
        this.tabledata();
    })

  }

  createUser() {
    const dialogRef = this.dialog.open(AddUserDialogComponent, {
      data: {},
      width: '40%',
      disableClose: true
    }
    )
    dialogRef.afterClosed().subscribe(result => {
      this.tabledata()
    });
  }
  bulkAddUsers(e) {

    e.click();
  }

  bulkImport(e) {
    const file: File = e.target.files[0];
    
    if (file) {
      const confirmDialog = this.dialog.open(ConfirmDialogComponent, {
        width: `500px`,
        data: { message: 'Are you sure you want to upload this file?' }
      });
      
      confirmDialog.afterClosed().subscribe(result => {
        if (result?.answer) {
          this.isLoading = true;
          const formData = new FormData();
          formData.append('request', file);
          this.httpservice.bulkAddUsers(formData).subscribe(result => {
            console.log(result);
            let arr = result;
            this.isLoading = false;
            const confirmDialog = this.dialog.open(BulkDialogComponent, {
              width: `600px`,
              data: { messages: result },
        
            });
        
            confirmDialog.afterClosed().subscribe();
            //this.snackBarService.showMessage(arr, 30000);
            this.tabledata();
          }, err => {
            this.snackBarService.showMessage(err?.error?.Message, 30000);
          });
        } else {
          // element.isPublished = !event.checked;
        }
      });
    }
  }

  openReset(row) {
    const dialogRef = this.dialog.open(ResetDialogComponent, {
      data: row,
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(result => {
      this.tabledata()
    });
  }

  disable(ev: MatSlideToggleChange, row) {
    console.log(row);
    let status = {
      id: row.id,
      isActive: !row.isActive
    }
    this.httpservice.getstatus(status).subscribe(data => {
      this.tabledata()
    })
  }

  updateStatus({ event, element }) {
    let updatedStatusUser = {
      ...element,
      isActive: event.checked
    }
    if (element === null ||
      element === undefined ||
      element === ``)
      return;
    let message = `Are you sure that you want to ${!event?.checked ? ` disable ` : ` enable `}this user?`;
    const confirmDialog = this.dialog.open(ConfirmDialogComponent, {
      width: `500px`,
      data: { message: message },

    });

    confirmDialog.afterClosed().subscribe(result => {



      if (result?.answer) {

        this.httpservice.updateStatusUser(updatedStatusUser).subscribe((data: any) => {
          (data as boolean) ?
            this.snackBarService.showMessage(data.message, 2000) :
            this.snackBarService.showMessage(`Something went wrong.`, 2000);
          this.tabledata();
        },
          error => this.snackBarService.showMessage(error?.toString(), 2000)
        );

      } else {

        element.isActive = !event.checked;

      }
    });
  }

  // updateStatus({ event, element }) {

  //   if (element === null ||
  //     element === undefined ||
  //     element === ``)
  //     return;
  //   let message = `Are you sure that you want to ${!event?.checked ? ` unpublish ` : ` publish `}this form?`;
  //   const confirmDialog = this.dialog.open(ConfirmDialogComponent, {
  //     width: `500px`,
  //     data: { message: message }
  //   });
  //   confirmDialog.afterClosed().subscribe(result => {
  //     if (result?.answer) {

  //       this.httpservice.updateStatusUser(updatedStatusUser).subscribe(data => this.tabledata());

  //     } else {

  //       element.isPublished = !event.checked;

  //     }
  //   });
  // }

  deleteIUser(IUser: IUser) {
    this.IUsers.splice(this.IUsers.findIndex((existingIUser) => existingIUser.id === IUser.id), 1);
    this.selection.deselect(IUser);
    this.subject$.next(this.IUsers);
  }

  deleteIUsers(IUsers: IUser[]) {
    IUsers.forEach(c => this.deleteIUser(c));
  }

  toggleColumnVisibility({ column, event }) {
    event.stopPropagation();
    event.stopImmediatePropagation();
    column.visible = !column.visible;
  }

  trackByProperty<T>(index: number, column: TableColumn<T>) {
    return column.property;
  }

  public getSubDivisions(rowValue) {

    return rowValue.Subdivision;
  }

  public getPoliceStations(row) {

    return row.Policestation;
  }

  downloadWord(type: string) {
    let formattedTableValues = []

    const matchFields = {
      serialNumber: 'SERIAL NO',
      id: 'ID',
      roleName: 'ROLE NAME',
      UserName: "USER NAME",
      policeStationsName: 'POLICE STATION',
      subDivisionName: 'SUB DIVISION',
      Location: "LOCATION",
      firstname: "First Name",
      lastname: "Last Name",
      emailId: "Email",
      phone: "PHONE NUMBER",
      createdByUsername: "Created By",
      createdDatetime: "Created On",
      modifiedByUsername: "Modified By",
      modifiedDatetime: "Modified On",
      isActive: "STATUS",
    }

    this.columns.map(column => {

      if (!column.visible) {

        for (const rowKey in matchFields) {

          const valueOfRow = matchFields[rowKey]

          if (valueOfRow.toLowerCase() === column.label.toLowerCase())
            delete matchFields[rowKey];

        }

      }

    })

    //if user filtered data then export only that data else all data
    const dataForExport = this.dataSource.filter ? this.dataSource.connect().value : this.dataSource.data;


    dataForExport.forEach((row: any) => {

      row.subDivisionName = this.getSubDivisions(row);
      row.policeStationsName = this.getPoliceStations(row);

      let oneRowInTable = {};

      for (const rowKey in matchFields) {
        const headerName = matchFields[rowKey];

        oneRowInTable[headerName] = row[rowKey];

        if (headerName === 'STATUS') {

          oneRowInTable[headerName] = row[rowKey] ? 'Active' : 'Inactive';

        } else {

          oneRowInTable[headerName] = row[rowKey];

        }

      }

      formattedTableValues.push(oneRowInTable);

    })


    this.htmlToDocxService.downloadBasedOnType(type, formattedTableValues, 'users');
  }


  deleteUser(user) {
    let message = `Are you sure that you want to delete this user?`;

    const confirmDialog = this.dialog.open(ConfirmDialogComponent, {
      width: `500px`,
      data: { message: message }
    });

    confirmDialog.afterClosed().subscribe(result => {
      if (result?.answer) {

        this.httpservice.deleteUser(user).subscribe(res => {

          this.snackBarService.showMessage(`User ${user.UserName} is successfully deleted`, 3000);

          this.tabledata();


        }, err => {

          this.snackBarService.showMessage(err?.error?.Message, 3000);

        });


      } else {

        // element.isPublished = !event.checked;

      }
    });
  }

  downloadBulkImportTemplate(): void {
    this.httpservice.downloadBulkImportTemplate();
  }
}



