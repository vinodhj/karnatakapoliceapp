export class IUser {
    id?: number;
    UserName?: string;
    Policestation?: string;
    PolicestationId?: number;
    Subdivision?: string;
    SubdivisionId?: string;
    Location?: string;
    LocationId?: string;
    labels?: string;
    isActive?: boolean
    roleID?: number
    roleName?: string
    LocationObj?: Object;
    SubdivisionObj?: Object
    PolicestationObj?: Object;
  }