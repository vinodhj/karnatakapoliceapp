import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IconModule } from '@visurel/iconify-angular';
import { PageLayoutModule } from 'src/@exai/components/page-layout/page-layout.module';
import { AngularMaterialModel } from 'src/app/shared/angular-material.module';
import { BasicTableModule } from 'src/app/shared/basic-table/basic-table.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { AddUserDialogComponent } from './add-user-dialog/add-user-dialog.component';
import { EditDialogComponent } from './edit-dialog/edit-dialog.component';
import { ManageUserRoutingModule } from './manage-user-routing.module';
import { ManageUserComponent } from './manage-user.component';
import { ResetDialogComponent } from './reset-dialog/reset-dialog.component';
import { UserManageComponent } from './user-manage/user-manage.component';
import { EditDialogNewComponent } from './edit-dialog-new/edit-dialog-new.component';
import {NgxMatIntlTelInputModule} from 'ngx-mat-intl-tel-input';
import { BulkDialogComponent } from './bulk-dialog/bulk-dialog.component';


@NgModule({
  declarations: [
    ManageUserComponent,
    UserManageComponent,
    AddUserDialogComponent,
    ResetDialogComponent,
    EditDialogComponent,
    EditDialogNewComponent,
    BulkDialogComponent
  ],
  imports: [
    CommonModule,
    ManageUserRoutingModule,
    AngularMaterialModel,
    FlexLayoutModule,
    SharedModule,
    SharedModule,
    IconModule,
    FormsModule,
    ReactiveFormsModule,
    PageLayoutModule,
    DragDropModule,
    BasicTableModule,
    NgxMatIntlTelInputModule
  ]
})
export class ManageUserModule { }
