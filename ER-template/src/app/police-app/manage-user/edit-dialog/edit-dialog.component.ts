import { HttpErrorResponse } from '@angular/common/http';
import { AfterViewInit, Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSelectChange } from '@angular/material/select';
import { MatSnackBar } from '@angular/material/snack-bar';
import { id } from 'date-fns/locale';
import { HttpService } from 'src/@exai/services/http.service';
import { LanguageService } from 'src/app/language.service';
import { regexEmail } from '../interfaces/emailPasswordRegex.interface'

@Component({
  selector: 'exai-edit-dialog',
  templateUrl: './edit-dialog.component.html',
  styleUrls: ['./edit-dialog.component.scss']
})
export class EditDialogComponent implements OnInit, AfterViewInit {
  viewLoading = true

  form: FormGroup
  getDivision
  durationInSeconds = 5;
  myProp: any
  getRole
  getSubdivison
  getPoliceStation
  myData: any;
  username: any;
  Policestation: any;
  PolicestationId: any;
  policestationName: any;
  subdivision: any;
  division: any;
  userID;
  roleID: number = null;
  rolename: any;
  getView = []
  selectedPolicestation: any;
  selectedData;

  role = [
    { id: 1, name: this.languageService.language?.dropdown.super },
    { id: 2, name: this.languageService.language?.dropdown.admin }
  ]
  firstname: any;
  lastName: any;
  Email: any;
  Phone: any;
  roleName: any;

  constructor(public dialogRef: MatDialogRef<EditDialogComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: any,
    private http: HttpService,
    private fb: FormBuilder,
    private _snackbar: MatSnackBar,
    public languageService: LanguageService) {
    this.username = this.data.username;
    this.roleID = Number(this.data.roleID);
    this.roleName = this.data.roleName
    this.Policestation = this.data.Policestation;
    this.subdivision = this.data.Subdivision;
    this.division = this.data.Location;
    this.userID = this.data.id;
    this.selectedData = this.data
    this.policestationName = this.data.policeStationName
  }

  updates() {
    //;
    let user = {
      id: this.userID,
      roleId: Number(this.form.value.role),
      username: this.form.value.username,
      firstname: this.form.value.firstname,
      lastname: this.form.value.lastName,
      // password: this.form.value.password,
      emailId: this.form.value.emailId,
      phone: this.form.value.phone,
      policeStationId: Number(this.form.value.role) == 1 ? -1 : Number(this.form.value.policestation?.id)
    }

    this.http.postUsers(user).subscribe(response => {
      //
      console.log(response)
      if (response) {
        this.dialogRef.close()
        this._snackbar.open('Save Sucessfully', 'OKAY', {
          duration: 1000,
          // horizontalPosition: 'right'
        })
      }
    }, (error: HttpErrorResponse) => {
      //;
      this._snackbar.open('Unable to edit User' , 'OKAY', {
        duration: 2000,
        // horizontalPosition: 'right'
      })
      this.dialogRef.close()
    })
  }
  ngOnInit(): void {
    this.form = new FormGroup({
      role: new FormControl('', [Validators.required]),
      division: new FormControl('', [Validators.required]),
      subdivision: new FormControl('', [Validators.required]),
      policestation: new FormControl('', [Validators.required]),
      username: new FormControl('', [Validators.required]),
      firstname: new FormControl('', [Validators.required]),
      lastName: new FormControl('', [Validators.required]),
      phone: new FormControl('', [Validators.required]),
      emailId: new FormControl('', [Validators.required, Validators.pattern(regexEmail)]),
      // password: new FormControl('', [Validators.required, Validators.maxLength(8)]),
      // confirmPassword: new FormControl('', [Validators.required, Validators.maxLength(8)]),
    })



    this.http.getDivisions().subscribe(data => {
      this.getDivision = data
      this.getDivision.push(this.data.LocationObj);
      this.getDivision = this.getNonDuplicatedArray(this.getDivision);
      this.getSubdivison = [this.data.SubdivisionObj];
      this.getPoliceStation = [this.data.PolicestationObj];
      setTimeout(() => {
        this.form.controls.division.setValue(this.data.LocationObj);
        this.form.controls.subdivision.setValue(this.data.SubdivisionObj);
        this.form.controls.policestation.setValue(this.data.PolicestationObj);
        this.form.controls.role.setValue(this.data.roleID)
      }, 1500)
    })
    this.http.getViewUser(this.userID).subscribe(data => {
      this.viewLoading = false
      this.getView.push(data)
    })

  }
  selectComparision(option: any, value: any) {
    if (typeof (option) == 'object') {
      return option.id == value.id;
    }
    else return option.toString() == value.toString();
  }

  getNonDuplicatedArray(array: Array<any>) {
    let retVal = [];
    for (let item of array) {
      if (!retVal.includes(JSON.stringify(item))) {
        retVal.push(JSON.stringify(item));
      }
    }
    retVal = retVal.map((x: any) => { return JSON.parse(x) });
    return retVal;
  }

  getOptionsForChild(event: MatSelectChange, levelId: number) {
    this.http.getDepartmentsByLevel1(levelId,
      typeof (event.value) === "object" ? event.value.id : event.value.map((x: any) => {
        return x.id
      }).join(',')).subscribe((response: any) => {

        levelId == 2 ? this.getSubdivison = this.getNonDuplicatedArray([...response]) : this.getPoliceStation = this.getNonDuplicatedArray([...response]);
      })
  }

  cancel() {
    this.dialogRef.close()
  }

  selectStation(event: any) {
    console.log('event', event.value)
    this.selectedPolicestation = event.value
  }

  click(id) {
    //
    if (id == '1') {
      this.form.controls['division'].disable();
      this.form.controls['policestation'].disable();
      this.form.controls['subdivision'].disable();
    } else if (id == '2') {
      this.form.controls['division'].enable();
      this.form.controls['policestation'].enable();
      this.form.controls['subdivision'].enable();
    }
  }
  ngAfterViewInit() {
    if (this.roleID == 1) {
      this.form.controls['division'].disable();
      this.form.controls['policestation'].disable();
      this.form.controls['subdivision'].disable();
    } else {
      this.form.controls['division'].enable();
      this.form.controls['policestation'].enable();
      this.form.controls['subdivision'].enable();
    }
  }


}
