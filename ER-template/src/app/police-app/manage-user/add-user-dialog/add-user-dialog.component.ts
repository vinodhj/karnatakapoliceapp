import { Component, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, FormGroupDirective, NgForm, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { MatDialogRef } from '@angular/material/dialog';
import { of } from 'rxjs';
import { finalize, map, switchMap, tap } from 'rxjs/operators';
import { HttpService } from 'src/@exai/services/http.service';
import { LanguageService } from 'src/app/language.service';
import { Role } from 'src/app/shared/enums/role.enum';
import { noWhitespaceValidator } from 'src/app/shared/models/no-whitespace.validator';
import { UserRoleToCreate } from 'src/app/shared/models/user-role-to-create';
import { SnackBarService } from 'src/app/shared/services/snack-bar.service';
import { regexEmail, regexPassword } from '../interfaces/emailPasswordRegex.interface';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const invalidCtrl = !!(control?.invalid && control?.parent?.dirty);
    const invalidParent = !!(control?.parent?.invalid && control?.parent?.dirty);

    return invalidCtrl || invalidParent;
  }
}

@Component({
  selector: 'exai-add-user-dialog',
  templateUrl: './add-user-dialog.component.html',
  styleUrls: ['./add-user-dialog.component.scss']
})
export class AddUserDialogComponent implements OnInit {
  allUsers
  getDivision
  durationInSeconds = 5;
  myProp: any
  getRole
  getSubdivison
  getPoliceStation
  form: FormGroup
  myData: any;
  username: any;
  policestationId: any;
  subdivision: any;
  division: any;
  roleId
  matcher = new MyErrorStateMatcher();

  @ViewChild('phoneNumber') public phoneNumber;


  loadingData: boolean;

  role = [
    { id: 1, name: this.languageService.language?.dropdown.super },
    { id: 2, name: this.languageService.language?.dropdown.admin }
  ];
  roles: UserRoleToCreate[] = [];

  selectedPolicestation: any;  

  constructor(
    public dialogRef: MatDialogRef<AddUserDialogComponent>,
    private http: HttpService,
    private snackBarService: SnackBarService,
    public languageService: LanguageService,
  ) { }

  ngOnInit(): void {

    this.http.getUserRolesToCreate().subscribe(
      response => this.roles = [...response as UserRoleToCreate[]]
        .filter(role => role?.name.includes(`District Head`) || role?.name.includes(`Police Users`))
    );

    this.http.getDivisionsDropdown().subscribe(data => {
      this.getDivision = data
    })

    this.http.getUsers().subscribe(data => {
      this.allUsers = data
    })

    this.form = new FormGroup({
      role: new FormControl('', [Validators.required]),
      division: new FormControl('', [Validators.required]),
      subdivision: new FormControl('', [Validators.required]),
      policestation: new FormControl('', [Validators.required]),
      username: new FormControl('', [Validators.required, Validators.maxLength(40), noWhitespaceValidator()]),
      firstname: new FormControl('', [Validators.required, Validators.maxLength(40), noWhitespaceValidator()]),
      lastName: new FormControl('', [Validators.required, Validators.maxLength(40), noWhitespaceValidator()]),
      phone: new FormControl('', [Validators.required, Validators.pattern('[- +()0-9]+'), noWhitespaceValidator(), Validators.maxLength(13), Validators.minLength(13)]),
      email: new FormControl('', [Validators.pattern(regexEmail)]),
      password: new FormControl('', [Validators.required, Validators.pattern(regexPassword), Validators.minLength(6), Validators.maxLength(25), noWhitespaceValidator()]),
      confirmPassword: new FormControl('', [Validators.required, Validators.pattern(regexPassword), noWhitespaceValidator()]),
    }, { validators: this.checkPasswords })

    this.form.get('phone').patchValue(' ')

    
    
    this.subscribeToFormChanges();
    
    // this.form.get('phone').patchValue('+91')
    
    // this.http.getSubDivisions().subscribe((response: any) => {
    //   this.getSubdivison = response
    // })
    
    // this.http.getAllPoliceStations().subscribe(response => {
      //   this.getPoliceStation = response
      // })
    }
    
    logPhone() {
      console.log(this.form.get('phone'))
    }
  public ngAfterViewChecked() {
    // disabling country selection button
    try {
      this.phoneNumber.elRef.nativeElement.firstChild.children[0].disabled = 'true';
    }
    catch (e) { //ignore this
    
    }
  }

  checkPasswords: ValidatorFn = (group: AbstractControl): ValidationErrors | null => {
    let pass = group.get('password').value;
    let confirmPass = group.get('confirmPassword').value
    return pass === confirmPass ? null : { notSame: true }
  }

  // get isPoliceUser(): boolean{
  //   return false;
  // }

  // check = true;

  divisionStar: boolean = true;
  subDivisionStar: boolean = true;
  policeStationStar: boolean = true;

  click(id) {

    if (id == '1' || id == '2') 
    {
      this.form.get('division').clearValidators();
      this.form.get('subdivision').clearValidators();
      this.form.get('policestation').clearValidators();

      this.form.controls['division'].disable();
      this.form.controls['policestation'].disable();
      this.form.controls['subdivision'].disable();

      this.divisionStar = false;
      this.subDivisionStar = false;
      this.policeStationStar = false;
    } 
    else if (id == '3' || id == '4' || id == '5') 
    {
      this.form.get('division').setValidators(Validators.required);
      this.form.get('subdivision').clearValidators();
      this.form.get('policestation').clearValidators();

      this.form.get('policestation').setValue(null);
      this.form.controls['policestation'].disable();
      this.form.get('subdivision').setValue(null);
      this.form.controls['subdivision'].disable();

      this.divisionStar = true;
      this.subDivisionStar = false;
      this.policeStationStar = false;
    }
    else if (id == '6') 
    {
      this.form.get('division').setValidators(Validators.required);
      this.form.get('subdivision').setValidators(Validators.required);
      this.form.get('policestation').clearValidators();

      this.form.get('policestation').setValue(null);
      this.form.controls['policeStation'].disable();

      this.divisionStar = true;
      this.subDivisionStar = true;
      this.policeStationStar = false;
    }
    else
    {
      this.form.get('division').setValidators(Validators.required);
      this.form.get('subdivision').setValidators(Validators.required);
      this.form.get('policestation').setValidators(Validators.required);

      this.form.controls['division'].enable();
      this.form.controls['policestation'].enable();
      this.form.controls['subdivision'].enable();

      this.divisionStar = true;
      this.subDivisionStar = true;
      this.policeStationStar = true;
    }
  }

  saveuser() {
    let user = {
      id: 0,
      roleId: this.form.value.role,
      username: this.form.value.username,
      firstname: this.form.value.firstname,
      lastname: this.form.value.lastName,
      password: this.form.value.password,
      emailId: this.form.value.email,
      phone: this.form.value.phone.trim(),
      policestationId: (this.form.value.policestation == null) ? null : this.form.value.policestation,
      policeStationsIds: '' + this.form.value.policestation,
      divisionId: (this.form.value.division == null) ? null : this.form.value.division.id,
      subDivisionId: (this.form.value.subDivision == null) ? null : this.form.value.subDivision.id
    }

    console.log(user);

    let userAlreadyExists = false;
    let emailAlreadyExists = false;

    this.allUsers.forEach(user => {
      user.username === this.form.value.username ? userAlreadyExists = true : null
    })

    this.allUsers.forEach(user => {
      user.emailId === this.form.value.email && this.form.value.email !== "" ? emailAlreadyExists = true : null
    })

    if (userAlreadyExists || emailAlreadyExists) {
      if (userAlreadyExists)
        this.snackBarService.showMessage('User With This User Name Already Exists', 2000)

      if (emailAlreadyExists)
        this.snackBarService.showMessage('User With This Email Already Exists', 2000)

    } else {

      this.loadingData = true

      this.http.postUsers(user).pipe(
        finalize(() => this.loadingData = false)
      ).subscribe((response: any) => {
        console.log({ response });
        if (response.status) {
          this.dialogRef.close()
          this.snackBarService.showMessage(`User ${this.form.get('username').value} Created Successfully`, 3000)
          this.form.reset()
          this.dialogRef.close()
        }
        this.snackBarService.showMessage(response.message, 3000)
      })
    }

  }
  cancel() {
    this.form.reset()
    this.dialogRef.close()
  }
  selectStation(event: any) {
    this.selectedPolicestation = event.value
  }

  subscribeToFormChanges(): void {
    this.form.controls.division.valueChanges.pipe(
      switchMap(division => {
        if (division === null || division === undefined || division === ``)
          return of();

        this.loadingData = true;

        return this.http.getSubDivisionsDropdownOptions([division?.id]).pipe(
          finalize(() => this.loadingData = false)
        );
      })
    ).subscribe(
      subDivisions => {
        this.getSubdivison = [];
        this.getSubdivison = subDivisions;

        this.form.get('subdivision').setValue(null);
        this.form.get('policestation').setValue(null);
      }
    );
    this.form.controls.subdivision.valueChanges.pipe(
      switchMap(subdivision => {
        if (subdivision === null || subdivision === undefined || subdivision === ``)
          return of();

        this.loadingData = true;

        return this.http.getPoliceStationsDropdownOptions([subdivision?.id]).pipe(
          map((response: any) => response.data)
        ).pipe(
          finalize(() => this.loadingData = false)
        );
      })
    ).subscribe(
      policeStations => {
        this.getPoliceStation = [];
        this.getPoliceStation = policeStations;

        this.form.get('policestation').setValue(null);
      }
    );
  }
}
