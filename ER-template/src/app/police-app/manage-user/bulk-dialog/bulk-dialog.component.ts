import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'exai-bulk-dialog',
  templateUrl: './bulk-dialog.component.html',
  styleUrls: ['./bulk-dialog.component.scss']
})
export class BulkDialogComponent implements OnInit {
  messages : String[] = [];

  constructor(@Inject(MAT_DIALOG_DATA) private data: {messages: String[]}, private matDialogRef: MatDialogRef<BulkDialogComponent>) {

   }

  ngOnInit(): void {
    this.messages = this.data.messages;
  }

  ngOnDestroy(){
    this.matDialogRef.close(this.data);
  }

  onCloseClick(){
    this.matDialogRef.close();
  }

}
