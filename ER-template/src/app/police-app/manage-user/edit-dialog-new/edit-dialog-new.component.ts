import { Component, Inject, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { BehaviorSubject, forkJoin, throwError } from 'rxjs';
import { catchError, finalize, map, withLatestFrom } from 'rxjs/operators';
import { HttpService } from 'src/@exai/services/http.service';
import { FormBuilderFillDataService } from 'src/app/form-builder/services/form-builder-fill-data.service';
import { LanguageService } from 'src/app/language.service';
import { noWhitespaceValidator } from 'src/app/shared/models/no-whitespace.validator';
import { UserRoleToCreate } from 'src/app/shared/models/user-role-to-create';
import { CacheDataService } from 'src/app/shared/services/cache-data.service';
import { SnackBarService } from 'src/app/shared/services/snack-bar.service';
import { regexEmail } from '../interfaces/emailPasswordRegex.interface';

@Component({
  selector: 'exai-edit-dialog-new',
  templateUrl: './edit-dialog-new.component.html',
  styleUrls: ['./edit-dialog-new.component.scss']
})
export class EditDialogNewComponent implements OnInit {

  loadingDataForDropdown: boolean = true;

  formGroup: FormGroup;
  isLoading: boolean = false;
  divisionOptions$ = new BehaviorSubject([]);
  subDivisionOptions$ = new BehaviorSubject([]);
  policeStationOptions$ = new BehaviorSubject([]);
  roles: UserRoleToCreate[] = [];
  dialogTitle: string = `Edit User`;

  @ViewChild('phoneNumber')
  public phoneNumber;

  constructor(
    public dialogRef: MatDialogRef<EditDialogNewComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder, private cacheDataService: CacheDataService, public languageService: LanguageService, private snackBarService: SnackBarService,
    private router: Router, private snackbar: MatSnackBar, private formBuilderFillDataService: FormBuilderFillDataService, private http: HttpService) { }


  userData: any;

  ngOnInit(): void {
    this.formGroup = this.fb.group({
      role: ['', Validators.required],
      division: ['', [Validators.required]],
      subDivision: ['', [Validators.required]],
      policeStation: ['', [Validators.required]],
      username: ['', [Validators.required, noWhitespaceValidator()]],
      firstname: ['', [Validators.required, Validators.maxLength(64), noWhitespaceValidator()]],
      lastname: ['', [Validators.required, Validators.maxLength(64), noWhitespaceValidator()]],
      phone: ['', [Validators.required, Validators.pattern('[- +()0-9]+'), noWhitespaceValidator(), Validators.maxLength(13), Validators.minLength(13)]],
      emailId: ['', [Validators.pattern(regexEmail)]]
    });


    forkJoin([
      this.cacheDataService.getInformationsForCaching(),
      this.http.getViewUser(this.data.id),
      this.http.getUserRolesToCreate()
    ]).pipe(
      map(([cacheDataService, userData, userRoles]) => {

        this.divisionOptions$.next(this.cacheDataService.getDivisionsSubject().value);

        this.userData = userData;

        this.roles = [...userRoles as UserRoleToCreate[]]
          .filter(role => role?.name.includes(`District Head`) || role?.name.includes(`Police Users`));

        this.setupInitialValuesForEdit(userData, userRoles);

      }),
      finalize(() => this.loadingDataForDropdown = false),
      catchError((err) => {

        this.snackbar.open(err?.error?.Message, "Ok", {
          duration: 2500
        });

        this.dialogRef.close();

        return throwError(err);
      })
    ).subscribe();

    this.formGroup.get('division').valueChanges.pipe(
      withLatestFrom(this.cacheDataService.getSubDivisionsSubject()),
      map(([selectedDivisions, subdivisions]) => {
        let divisionIds = selectedDivisions;

        const filteredSubdivisions = subdivisions.filter(subdivision => divisionIds === subdivision.division.id && subdivision.isActive);
        return filteredSubdivisions;
      })
    ).subscribe(value => {
      this.formGroup.get('subDivision').setValue([]);
      this.policeStationOptions$.next([]);
      this.subDivisionOptions$.next(value);
    });

    this.formGroup.get('subDivision').valueChanges.pipe(
      withLatestFrom(this.cacheDataService.getPoliceStationsSubject()),
      map(([selectedSubDivisions, policeStations]) => {
        let subDivisionIds = selectedSubDivisions;

        console.log(policeStations)

        const filteredPoliceStations = policeStations.filter(policeStation => subDivisionIds === policeStation.subDivision.id && policeStation.isActive);
        return filteredPoliceStations;
      })
    ).subscribe(value => {
      this.policeStationOptions$.next(value);
    });
  }

  public ngAfterViewChecked() {
    // disabling country selection button
    try {
      this.phoneNumber.elRef.nativeElement.firstChild.children[0].disabled = 'true';
    }
    catch (e) { //ignore this
    
    }
  }

  divisionStar: boolean = true;
  subDivisionStar: boolean = true;
  policeStationStar: boolean = true;

  setupInitialValuesForEdit(userData, userRoles) {
    if (!this.data.editing) return;

    console.log(userData, userRoles, "DATA");
    // const roleId = this.data.roleID;

    this.formGroup.get('role').setValue(userData.roleId);
    this.formGroup.get('division').setValue(userData.divisionId);
    if(userData.roleId >= 6)
    this.formGroup.get('subDivision').setValue(userData.subDivisionId);
    else {
      this.formGroup.get('subDivision').clearValidators();
      this.formGroup.controls['subDivision'].disable();
      this.subDivisionStar = false;     
    }
    if(userData.roleId >= 7)
    this.formGroup.get('policeStation').setValue(userData.policeStationId);
    else {
      this.formGroup.get('policeStation').clearValidators();
      this.formGroup.controls['policeStation'].disable();
      this.policeStationStar = false;
    }
    this.formGroup.get('username').setValue(this.userData.username);
    this.formGroup.get('firstname').setValue(this.userData.firstname);
    this.formGroup.get('lastname').setValue(this.userData.lastname);
    this.formGroup.get('emailId').setValue(this.userData.emailId);
    this.formGroup.get('phone').setValue(this.userData.phone);

    this.formGroup.get('username').disable();

    //this.click(userData.roleId);

    // this.formGroup.get('emailId').disable();

  }

  selectComparision(option: any, value: any) {
    if (typeof (option) == 'object') {
      return option?.id == value?.id;
    }
    else return option.toString() == value.toString();
  }

  selectAll(event, data, controlName) {
    let optionsData = data;
    if (controlName === 'policeStation')
      optionsData = data.map((x: any) => { return x.id })
    event.checked ?
      this.formGroup.get(controlName).setValue(optionsData) :
      this.formGroup.get(controlName).setValue([]);
  }

  onCancel() {
    this.dialogRef.close();
  }

  onSubmitForm() {
    console.log(this.formGroup);

    if (!this.formGroup.valid) return;

    const values = {
      id: this.data.id,
      roleId: this.formGroup.get('role').value,
      username: this.formGroup.get('username').value,
      firstname: this.formGroup.get('firstname').value,
      lastname: this.formGroup.get('lastname').value,
      emailId: this.formGroup.get('emailId').value,
      phone: this.formGroup.get('phone').value,
      policeStationId: (this.formGroup.value.policeStation == null) ? null : this.formGroup.value.policeStation,
      subDivisionId: (this.formGroup.value.subDivision == null) ? null : this.formGroup.value.subDivision,
      divisionId: (this.formGroup.value.division == null) ? null : this.formGroup.value.division,
      //divisionId: (this.formGroup.get('division').value == null) ? null : this.formGroup.get('division').value.Id
    }

    console.log(values);


    this.isLoading = true;

    this.http.editUser(values).pipe(
      finalize(() => this.isLoading = false)
    ).subscribe((response: any) => {

      if (response.status) {
        this.snackBarService.showMessage(`${this.formGroup.get('username').value} updated Successfully`, 3000)
        this.dialogRef.close(true);
      }
      else {
        this.snackBarService.showMessage(response.message, 3000)
      }

    })
  }

  click(id) {

    if (id == '1' || id == '2') 
    {
      this.formGroup.get('division').clearValidators();
      this.formGroup.get('subDivision').clearValidators();
      this.formGroup.get('policeStation').clearValidators();

      this.formGroup.controls['division'].disable();
      this.formGroup.controls['policeStation'].disable();
      this.formGroup.controls['subDivision'].disable();

      this.divisionStar = false;
      this.subDivisionStar = false;
      this.policeStationStar = false;
    } 
    else if (id == '3' || id == '4' || id == '5') 
    {
      this.formGroup.get('division').setValidators(Validators.required);
      this.formGroup.get('subDivision').clearValidators();
      this.formGroup.get('policeStation').clearValidators();

      this.formGroup.get('policeStation').setValue([]);
      this.formGroup.controls['policeStation'].disable();
      this.formGroup.get('subDivision').setValue([]);
      this.formGroup.controls['subDivision'].disable();

      this.divisionStar = true;
      this.subDivisionStar = false;
      this.policeStationStar = false;
    }
    else if (id == '6') 
    {
      this.formGroup.get('division').setValidators(Validators.required);
      this.formGroup.get('subDivision').setValidators(Validators.required);
      this.formGroup.get('policeStation').clearValidators();

      this.formGroup.get('policeStation').setValue([]);
      this.formGroup.controls['policeStation'].disable();

      this.divisionStar = true;
      this.subDivisionStar = true;
      this.policeStationStar = false;
    }
    else
    {
      this.formGroup.get('division').setValidators(Validators.required);
      this.formGroup.get('subDivision').setValidators(Validators.required);
      this.formGroup.get('policeStation').setValidators(Validators.required);

      this.formGroup.controls['division'].enable();
      this.formGroup.controls['policeStation'].enable();
      this.formGroup.controls['subDivision'].enable();

      this.divisionStar = true;
      this.subDivisionStar = true;
      this.policeStationStar = true;
    }
  }
}