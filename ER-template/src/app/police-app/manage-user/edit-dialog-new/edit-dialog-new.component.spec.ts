import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditDialogNewComponent } from './edit-dialog-new.component';

describe('EditDialogNewComponent', () => {
  let component: EditDialogNewComponent;
  let fixture: ComponentFixture<EditDialogNewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditDialogNewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditDialogNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
