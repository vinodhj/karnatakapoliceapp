import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PoliceAppComponent } from './police-app.component';

describe('PoliceAppComponent', () => {
  let component: PoliceAppComponent;
  let fixture: ComponentFixture<PoliceAppComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PoliceAppComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PoliceAppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
