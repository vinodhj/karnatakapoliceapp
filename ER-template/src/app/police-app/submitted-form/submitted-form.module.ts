import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { routing } from "../applicable-forms/applicable-forms.routing";
import { SubmittedFormComponent } from "./submitted-form.component";


@NgModule({
    declarations: [
        SubmittedFormComponent
    ],
    imports: [
        CommonModule,
        routing,
    ]
})
export class SubmittedFormsModule { }