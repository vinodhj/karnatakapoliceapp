import { ModuleWithProviders } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { EditFormComponent } from "../forms/edit-form/edit-form.component";
import { SubmittedFormComponent } from "./submitted-form.component";

const routes: Routes = [
    { path: '', component: SubmittedFormComponent,
    children: [
        { path: "submitted-form", component: EditFormComponent},
        { path: "**", pathMatch: "full", redirectTo:"submitted-form"},
        
    ]
 }
];

export const routing: ModuleWithProviders<RouterModule> = RouterModule.forChild(routes);