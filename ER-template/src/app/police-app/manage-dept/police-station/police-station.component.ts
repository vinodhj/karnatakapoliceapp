import { SelectionModel } from '@angular/cdk/collections';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { DatePipe } from '@angular/common';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormControl, Validators } from '@angular/forms';
import { MatChipInputEvent } from '@angular/material/chips';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatFormFieldDefaultOptions, MAT_FORM_FIELD_DEFAULT_OPTIONS } from '@angular/material/form-field';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { UntilDestroy } from '@ngneat/until-destroy';
import { Observable, ReplaySubject } from 'rxjs';
import { filter, finalize } from 'rxjs/operators';
import { fadeInUp400ms } from 'src/@exai/animations/fade-in-up.animation';
import { stagger40ms } from 'src/@exai/animations/stagger.animation';
import { TableColumn } from 'src/@exai/interfaces/table-column.interface';
import { HttpService } from 'src/@exai/services/http.service';
import { LanguageService } from 'src/app/language.service';
import { ConfirmDialogComponent } from 'src/app/shared/advanced-grid-item/dialogs/confirm-dialog/confirm-dialog.component';
import { HtmlToDocxService } from 'src/app/shared/services/html-to-docx.service';
import { SnackBarService } from 'src/app/shared/services/snack-bar.service';
import { IPolicestation, PoliceStationVM } from './interfaces/ipolicestation.interface';
import { PoliceStationDialogComponent } from './police-station-dialog/police-station-dialog.component';

@UntilDestroy()
@Component({
  selector: 'exai-police-station',
  templateUrl: './police-station.component.html',
  styleUrls: ['./police-station.component.scss'],
  animations: [
    fadeInUp400ms,
    stagger40ms
  ],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        appearance: 'standard'
      } as MatFormFieldDefaultOptions
    }
  ]
})
export class PoliceStationComponent implements OnInit {

  layoutCtrl = new FormControl('boxed');
  subject$: ReplaySubject<PoliceStationVM[]> = new ReplaySubject<PoliceStationVM[]>(1);
  data$: Observable<PoliceStationVM[]> = this.subject$.asObservable();
  customers: PoliceStationVM[];
  formListData: PoliceStationVM[] = [];
  addStation: FormArray;
  formLabels: Array<any>;
  departments: any[] = [];
  deparments: any[];

  @Input()
  columns: TableColumn<IPolicestation>[] = [
    // { label: 'Checkbox', property: 'checkbox', type: 'checkbox', visible: true },
    { label: 'Serial NO', property: 'serialNumber', type: 'serialNumber', visible: true, cssClasses: ['font-medium'] },
    { label: 'Police-Station Id', property: 'id', type: 'text', visible: true, cssClasses: ['font-medium'] },
    { label: 'Division', property: 'divisionname', type: 'text', visible: true, cssClasses: ['font-medium'] },
    { label: 'Sub-Division', property: 'subdivisionname', type: 'text', visible: true, cssClasses: ['font-medium'] },
    { label: 'Police-Station', property: 'policestation', type: 'text', visible: true, cssClasses: ['font-medium'] },
    { label: 'Created by', property: 'createdByUsername', type: 'text', visible: false, cssClasses: ['font-medium'] },
    { label: 'Created on', property: 'createdDatetime', type: 'text', visible: false, cssClasses: ['font-medium'] },
    { label: 'Modified by', property: 'modifiedByUsername', type: 'text', visible: false, cssClasses: ['font-medium'] },
    { label: 'Modified on', property: 'modifiedDatetime', type: 'text', visible: false, cssClasses: ['font-medium'] },
    { label: 'Status', property: 'isActive', type: 'button', visible: true },
    { label: 'Action', property: 'actions', type: 'button', visible: true },
  ];

  dataSource: MatTableDataSource<PoliceStationVM> | null;
  selection = new SelectionModel<PoliceStationVM>(true, []);
  searchCtrl = new FormControl();

  length: number;
  pageSize: 10;
  pageSizeOptions = [5, 10, 50];
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private dialog: MatDialog,
    private httpservice: HttpService,
    private snackBarService: SnackBarService,
    public languageService: LanguageService,
    private htmlDownloadFile: HtmlToDocxService
  ) {
    this.formLabels = [
      {
        label: 'Police Station',
        type: 1,
        level: null,
        dependentIndex: null,
        initialValue: '',
        multiple: false
      },
      {
        label: 'Division',
        type: 2,
        level: 1,
        dependentIndex: 2,
        initialValue: ''
      },
      {
        label: 'Sub-Division',
        type: 2,
        level: 2,
        dependentIndex: null,
        initialValue: ''
      },
    ];

    this.addStation = new FormArray([
      new FormControl('', [Validators.required]),
      new FormControl('', [Validators.required]),
      new FormControl('', [Validators.required]),
    ])
  }

  isLoading = true
  get visibleColumns() {
    return this.columns.filter(column => column.visible).map(column => column.property);
  }

  ngOnInit() {
    this.getDepartments();

    this.data$.pipe(
      filter<PoliceStationVM[]>(Boolean)
    ).subscribe(customers => {
      this.customers = customers;
      this.dataSource.data = customers;
    })
  }


  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.columns, event.previousIndex, event.currentIndex);
  }

  addOnBlur = true;
  readonly separatorKeysCodes = [ENTER, COMMA] as const;
  searchValues = [];
  wantedSearchedValues = []

  add(event: MatChipInputEvent): void {
    this.wantedSearchedValues = []
    if (event.value) {
      this.searchValues.push(event.value);
    }

    let wantedValues = []
    this.searchValues.forEach(el => {
      if (el.split(' :').length == 1) {
        let textForSearch = el.split(' :')[0];
        this.wantedSearchedValues.push('all')
        wantedValues.push(textForSearch)
      } else {
        let value = el.split(' :')[1].trim()
        this.columns.forEach(column => {
          if (column.label === el.split(' :')[0]) {
            this.wantedSearchedValues.push(column.property)
          }
        })
        wantedValues.push(value)
      }
    })
    this.dataSource.filter = wantedValues.join('+')

    // Clear the input value
    event.chipInput!.clear();
  }

  remove(value): void {
    const index = this.searchValues.indexOf(value);
    this.wantedSearchedValues.splice(index, 1)
    let dataSourceFilters = this.dataSource.filter.split('+')
    let valueSplitted = value.split(':')

    if (index >= 0) {
      this.searchValues.splice(index, 1);
      if (valueSplitted.length > 1) {
        dataSourceFilters.splice(dataSourceFilters.indexOf(valueSplitted[1].trim()), 1);
      } else {
        dataSourceFilters.splice(dataSourceFilters.indexOf(valueSplitted[0].trim()), 1);
      }
      this.dataSource.filter = dataSourceFilters.join('+')
    }

    if (this.searchValues.length < 1) {
      this.dataSource.filter = ''
    }
  }

  appendToSearch(label) {
    let configuredLabelForSearch = `${label} : `
    this.searchCtrl.setValue(configuredLabelForSearch)
  }

  pipe: any = new DatePipe('en-US'); // Use your own locale

  getDepartments() {

    this.isLoading = true;

    this.deparments = [];
    this.formListData = [];
    this.httpservice.getAllPoliceStationsV1().pipe(
      finalize(() => this.isLoading = false)
    ).subscribe((response: any) => {
      this.isLoading = false;
      for (let i = 0; i < response?.data?.length; i++) {
        if (response?.data[i].subDivision) {

          let formData = {
            id: response?.data[i].id,
            subdivision: response?.data[i]?.subDivision,
            division: response?.data[i]?.subDivision?.division,
            subdivisionname: response?.data[i]?.subDivision?.name,
            policestation: response?.data[i]?.name,
            isActive: response?.data[i]?.isActive,
            modifiedByUsername: response?.data[i].modifiedByUsername,
            createdByUsername: response?.data[i].createdByUsername,
            createdDatetime: response?.data[i].createdDatetime,
            modifiedDatetime: response?.data[i].modifiedDatetime,
            serialNumber: i + 1
          }
          if (response?.data[i].subDivision.division) {
            formData['divisionname'] = response?.data[i]?.subDivision?.division?.name;
          }
          this.formListData.push(formData as unknown as PoliceStationVM);
        }
      }
      this.dataSource = new MatTableDataSource(this.formListData);
      this.dataSource.paginator = this.paginator;
      // advanced filtering function
      this.dataSource.filterPredicate = (data, filters) => {
        const matchFilter = [];
        const filterArray = filters.split('+');
        const columns = Object.values(data);
        filterArray.forEach(filter => {
          let formattedFilter = filter.trim().toLowerCase()
          const customFilter = [];
          console.log(this.wantedSearchedValues);

          columns.forEach(column => {
            this.wantedSearchedValues.forEach(colProperty => {
              let valid
              if (colProperty == 'all') {
                for (let key in data) {
                  this.columns.forEach(col => {
                    if (col.property === key) {
                      valid = col.visible
                      return
                    }
                  })

                  if (valid) {
                    let formattedDataValue = ((('' + data[key]).toLowerCase().replace(/\s+/g, ' ')).trim())
                    if (formattedDataValue.includes(formattedFilter) || formattedDataValue == (formattedFilter)) {
                      customFilter.push(true)
                    }
                  }
                }
              } else {
                if (colProperty === 'isActive') {
                  let formattedDataColProperty

                  if (data[colProperty]) {
                    formattedDataColProperty = 'true'
                  } else {
                    formattedDataColProperty = 'false'
                  }

                  customFilter.push(formattedDataColProperty.includes(formattedFilter) || formattedDataColProperty == formattedFilter)
                } else {
                  this.columns.forEach(col => {
                    if (col.property === colProperty) {
                      valid = col.visible
                      return
                    }
                  })

                  if (valid) {
                    let formattedDataColProperty = (('' + data[colProperty]).toLowerCase().replace(/\s+/g, ' ')).trim()
                    if (colProperty === 'divisionname') {
                      formattedDataColProperty = data["subdivision"].division.name.toString().toLowerCase().replace(/\s+/g, ' ').trim()
                    } else if (colProperty === 'subdivisionname') {
                      formattedDataColProperty = data["subdivision"].name.toString().toLowerCase().replace(/\s+/g, ' ').trim()
                    }

                    customFilter.push(formattedDataColProperty.includes(formattedFilter))
                  }
                }
              }
            })
          });
          matchFilter.push(customFilter.some(Boolean)); // OR
        });
        return matchFilter.every(Boolean); // AND
      }
    });
  }

  createStation(initialValue: any = null, update = false): void {
    let dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.width = '25vw';
    dialogConfig.height = 'min-content';

    if (initialValue) {
      this.formLabels[0].initialValue = initialValue ? initialValue.policestation : '';
      this.formLabels[1].initialValue = initialValue ? initialValue.division : '';
      this.formLabels[2].initialValue = initialValue ? initialValue.subdivision : '';
    }

    dialogConfig.data = {
      labels: this.formLabels,
      controls: this.addStation
    }

    let dialogConfiguration: any = {
      width: '25vw',
      height: 'min-content',
      disableClose: true,
      data: {
        editing: false
      }
    }

    if (initialValue) {
      dialogConfiguration.data = {
        editing: true,
        division: initialValue?.division,
        subdivision: initialValue?.subdivision,
        id: initialValue?.id,
        name: initialValue?.policestation,
        branchId: initialValue?.branchId,
        isActive: initialValue?.isActive,
        isDeleted: initialValue?.isDeleted
      }
    }

    const dialogRef = this.dialog.open(PoliceStationDialogComponent, dialogConfiguration);

    dialogRef.afterClosed().subscribe(result => {

      if (result)
        this.getDepartments();

    })

    // dialogRef.afterClosed().subscribe(result => {
    //   if (result) {    
    //     let policeStationExists = false    
    //     let wantedResult = {
    //       value: result.value[0],
    //       parentId: result.value[2].id
    //     }

    //     this.formListData.forEach(policeStation => {
    //       policeStation.policestation === wantedResult.value ? policeStationExists = true : null
    //     })

    //     if (policeStationExists) {
    //       this.snackBarService.showMessage('Police Station With This Name Already Exists', 2000)
    //     } else {
    //       if (update) {
    //         wantedResult["id"] = initialValue.index,
    //         this.httpservice.updatePoliceStation(wantedResult).subscribe(
    //           (res) => {
    //             this.addStation.reset()
    //             this.getDepartments();
    //             this.snackBarService.showMessage('Saved Successfully')
    //           }
    //         );

    //       } else {
    //         wantedResult["orderNo"] = 0
    //         this.httpservice.createPoliceStation(wantedResult).subscribe(
    //           (res) => {
    //             this.addStation.reset()
    //             this.getDepartments();
    //             this.snackBarService.showMessage('Saved Successfully')
    //           }
    //         );
    //       }
    //     }
    //   }
    // });
  }

  updateStatus({ event, element }) {
    let message = `Are you sure that you want to ${event?.checked ? ` unblock ` : ` block `}this police station?`;
    const confirmDialog = this.dialog.open(ConfirmDialogComponent, {
      width: `500px`,
      data: { message: message }
    });
    confirmDialog.afterClosed().subscribe(
      result => result?.answer ? this.sendBlockRequest({ event, element }) : this.getDepartments());
  }

  sendBlockRequest({ event, element }): void {
    const requestModel = {
      id: element?.id,
      branchId: element?.branchId,
      isActive: event?.checked,
      isDeleted: !event?.checked,
      createdBy: 0
    }
    this.httpservice.updateStatusPoliceV1(requestModel).subscribe(
      (data: any) => {
        data?.status ? this.snackBarService.showMessage(data?.message, 2000) : null;
        this.getDepartments();
      },
      error => this.snackBarService.showMessage(error?.error?.message || "Error happened", 2000));
  }

  toggleColumnVisibility({ column, event }) {
    event.stopPropagation();
    event.stopImmediatePropagation();
    column.visible = !column.visible;
  }

  trackByProperty<T>(index: number, column: TableColumn<T>) {
    return column.property;
  }

  public getSubDivisions(rowValue) {

    return rowValue.Subdivision;
  }

  public getPoliceStations(row) {

    return row.Policestation;
  }

  downloadWord(type: string) {
    let formattedTableValues = []

    const matchFields = {
      serialNumber: 'SERIAL NO',
      id: 'POLICE-STATION ID',
      divisionname: "DIVISION",
      subdivisionname: "SUB-DIVISION",
      policestation: "POLICE-STATION",
      createdByUsername: "CREATED BY",
      createdDatetime: "CREATED ON",
      modifiedByUsername: "MODIFIED BY",
      modifiedDatetime: "MODIFIED ON",
      isActive: "STATUS",
    }

    this.columns.map(column => {

      if (!column.visible) {

        for (const rowKey in matchFields) {

          const valueOfRow = matchFields[rowKey]

          if (valueOfRow.toLowerCase() === column.label.toLowerCase())
            delete matchFields[rowKey];

        }

      }

    })


    //if user filtered data then export only that data else all data
    const dataForExport = this.dataSource.filter ? this.dataSource.connect().value : this.dataSource.data;


    dataForExport.forEach((row: any) => {

      row.subDivisionName = this.getSubDivisions(row);
      row.policeStationsName = this.getPoliceStations(row);

      let oneRowInTable = {};

      for (const rowKey in matchFields) {
        const headerName = matchFields[rowKey];

        oneRowInTable[headerName] = row[rowKey];


        if (headerName === 'STATUS') {

          oneRowInTable[headerName] = row[rowKey] ? 'Active' : 'Inactive';

        } else {

          oneRowInTable[headerName] = row[rowKey];

        }

      }

      formattedTableValues.push(oneRowInTable);

    })


    this.htmlDownloadFile.downloadBasedOnType(type, formattedTableValues, 'police-stations')
  }

}