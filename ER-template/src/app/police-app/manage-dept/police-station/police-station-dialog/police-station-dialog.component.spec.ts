import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PoliceStationDialogComponent } from './police-station-dialog.component';

describe('PoliceStationDialogComponent', () => {
  let component: PoliceStationDialogComponent;
  let fixture: ComponentFixture<PoliceStationDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PoliceStationDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PoliceStationDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
