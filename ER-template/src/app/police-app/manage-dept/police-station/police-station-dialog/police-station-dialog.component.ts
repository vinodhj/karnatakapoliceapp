import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BehaviorSubject, forkJoin, Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { HttpService } from 'src/@exai/services/http.service';
import { LanguageService } from 'src/app/language.service';
import { noWhitespaceValidator } from 'src/app/shared/models/no-whitespace.validator';
import { ManageDivisionDialogComponent } from '../../manage-division/manage-division-dialog/manage-division-dialog.component';

@Component({
  selector: 'exai-police-station-dialog',
  templateUrl: './police-station-dialog.component.html',
  styleUrls: ['./police-station-dialog.component.scss']
})

export class PoliceStationDialogComponent implements OnInit {

  formGroup: FormGroup = this.fb.group({
    name: [``, [Validators.required, noWhitespaceValidator()]],
    division: [``, Validators.required],
    subDivision: [``, Validators.required]
  });
  isLoading: boolean = false;
  loadingDataForDropdown: boolean = true;
  nameAlreadyExists: boolean = false;

  divisions: any[] = [];
  allSubDivisions: any[] = [];
  filteredSubDivisionsSubject: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);
  filteredSubDivisions: Observable<any[]> = this.filteredSubDivisionsSubject.asObservable();

  dialogTitle: string = ``;
  selectedDivisions = this.divisions;

  constructor(
    public dialogRef: MatDialogRef<ManageDivisionDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public languageService: LanguageService,
    private fb: FormBuilder,
    private httpService: HttpService,
    private snackbar: MatSnackBar
  ) { }

  ngOnInit(): void {
    this.reactOnDivisionChange();
    this.initializeDataForDialog();
  }

  reactOnDivisionChange(): void {
    this.formGroup.get(`division`)
      .valueChanges.subscribe(
        value => {
          this.formGroup.get(`subDivision`).setValue(null);
          if (value === null || value === undefined || value === ``)
            return;
          this.filteredSubDivisionsSubject.next(
            this.allSubDivisions?.filter(subDivision => subDivision?.division?.id === value)
          );
        }
      );
  }

  initializeDataForDialog(): void {
    var requests = [
      this.httpService.getDivisionsDropdownV1(),
      this.httpService.getSubDivisions()
    ];
    forkJoin(requests).pipe(
      finalize(() => this.loadingDataForDropdown = false)
    ).subscribe(
      responses => {
        if (((responses[0] as any)?.items as any[])?.length > 0)
          this.divisions = [...(responses[0] as any)?.items as any[]];
        if ((responses[1] as any[])?.length > 0)
          this.allSubDivisions = [...responses[1] as any[]].filter(subDivision => subDivision.isActive);
        this.patchInitialValues();
      }
    );
  }

  patchInitialValues(): void {
    if (this.data?.editing) {
      this.dialogTitle = `Edit Police Station`;
      this.formGroup.patchValue({
        name: this.data?.name,
        division: this.divisions?.find(division => division?.id === this.data?.division?.id)?.id
      });
      const filteredSubDivisions = this.allSubDivisions?.filter(subDivision =>
        subDivision?.division?.id === this.divisions?.find(
          division => division?.id === this.data?.division?.id)?.id);
      if (filteredSubDivisions.length > 0) {
        this.filteredSubDivisionsSubject.next(this.allSubDivisions?.filter(subDivision =>
          subDivision?.division?.id === this.divisions?.find(
            division => division?.id === this.data?.division?.id)?.id));
        this.formGroup.get(`subDivision`).setValue(this.data?.subdivision?.id);
      }
    } else {
      this.dialogTitle = `Add new Police Station`;
    }
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  onSubmitForm(): void {
    if (!this.formGroup.valid) {
      this.formGroup.get(`name`).markAsTouched();
      this.formGroup.get(`division`).markAsTouched();
      this.formGroup.get(`subDivision`).markAsTouched();
      return;
    }
    if (this.data.editing)
      this.updatePoliceStation();
    else
      this.createPoliceStation();
  }

  updatePoliceStation(): void {
    let request: any = {
      id: this.data?.id,
      branchId: this.data?.branchId,
      divisionId: this.formGroup.get(`division`).value,
      subDivisionId: this.formGroup.get(`subDivision`).value,
      name: this.formGroup.get(`name`).value,
      dataDetail: null,
      isActive: this.data?.isActive,
      isDeleted: this.data?.isDeleted,
      createdBy: 0
    }
    this.httpService.updatePoliceStationV1(request).pipe(
      finalize(() => this.isLoading = false)
    ).subscribe(response => {
      this.snackbar.open((response as any)?.message, 'OK', { duration: 2000 })
      this.dialogRef.close(true);
    });
  }

  createPoliceStation(): void {
    let request: any = {
      branchId: this.httpService.branchId,
      divisionId: this.formGroup.get(`division`).value,
      subDivisionId: this.formGroup.get(`subDivision`).value,
      name: this.formGroup.get(`name`).value,
      dataDetail: null,
      isActive: true,
      isDeleted: false,
      createdBy: 0
    }
    this.httpService.createPoliceStationV1(request).pipe(
      finalize(() => this.isLoading = false)
    ).subscribe((response: any) => {
      this.snackbar.open(response.message, 'OK', { duration: 2000 })
      this.dialogRef.close(true);
    });
  }
}