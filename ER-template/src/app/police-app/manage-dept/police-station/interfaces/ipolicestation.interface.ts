import { SubDivisionV1 } from "../../manage-sub-division/interfaces/ISubdivision.interface";

export interface IPolicestation {
  id?: number;
  labels?: any;
  index: number;
  division: any;
  divisionname: string;
  subdivisionname: string;
  subdivision: any;
  policestation: string;
  isActive?: boolean;
}

export class PoliceStationVM {
  id: number;
  branchId: number | null;
  divisionId: number | null;
  subDivisionId: number | null;
  name: string;
  dataDetail: string;
  isActive: boolean;
  isDeleted: boolean;
  createdBy: string;
  createdDatetime: string;
  modifiedBy: string;
  modifiedDatetime: string;
  subDivision: SubDivisionV1;
}