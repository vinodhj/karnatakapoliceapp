import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IconModule } from '@visurel/iconify-angular';
import { PageLayoutModule } from 'src/@exai/components/page-layout/page-layout.module';
import { AngularMaterialModel } from 'src/app/shared/angular-material.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { ManageDeptRoutingModule } from './manage-dept-routing.module';
import { ManageDeptComponent } from './manage-dept.component';
import { ManageDivisionComponent } from './manage-division/manage-division.component';
import { ManageSubDivisionComponent } from './manage-sub-division/manage-sub-division.component';
import { PoliceStationComponent } from './police-station/police-station.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { BasicTableModule } from 'src/app/shared/basic-table/basic-table.module';
import { ManageDivisionDialogComponent } from './manage-division/manage-division-dialog/manage-division-dialog.component';
import { ManageSubDivisionDialogComponent } from './manage-sub-division/manage-sub-division-dialog/manage-sub-division-dialog.component';
import { PoliceStationDialogComponent } from './police-station/police-station-dialog/police-station-dialog.component';



@NgModule({
  declarations: [
    ManageDeptComponent,
    ManageDivisionComponent,
    ManageSubDivisionComponent,
    PoliceStationComponent,
    ManageDivisionDialogComponent,
    ManageSubDivisionDialogComponent,
    PoliceStationDialogComponent,
  ],
  imports: [
    CommonModule,
    ManageDeptRoutingModule, 
    AngularMaterialModel,
    SharedModule,
    IconModule,
    FormsModule,
    ReactiveFormsModule,
    PageLayoutModule,
    DragDropModule,
    BasicTableModule
  ]
})
export class ManageDeptModule { }
