import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ManageDivisionComponent } from './manage-division/manage-division.component';
import { ManageSubDivisionComponent } from './manage-sub-division/manage-sub-division.component';
import { PoliceStationComponent } from './police-station/police-station.component';

const routes: Routes = [
  { path: 'manage-division', component: ManageDivisionComponent },
  { path: 'manage-sub-division', component: ManageSubDivisionComponent },
  { path: 'police-station', component: PoliceStationComponent },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManageDeptRoutingModule { }
