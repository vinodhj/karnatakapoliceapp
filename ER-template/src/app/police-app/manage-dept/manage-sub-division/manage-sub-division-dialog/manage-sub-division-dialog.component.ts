import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BehaviorSubject, of } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { HttpService } from 'src/@exai/services/http.service';
import { LanguageService } from 'src/app/language.service';
import { noWhitespaceValidator } from 'src/app/shared/models/no-whitespace.validator';
import { CacheDataService } from 'src/app/shared/services/cache-data.service';
import { ManageDivisionDialogComponent } from '../../manage-division/manage-division-dialog/manage-division-dialog.component';
@Component({
  selector: 'exai-manage-sub-division-dialog',
  templateUrl: './manage-sub-division-dialog.component.html',
  styleUrls: ['./manage-sub-division-dialog.component.scss']
})
export class ManageSubDivisionDialogComponent implements OnInit {
  formGroup: FormGroup;
  isLoading: boolean = false;
  loadingDataForDropdown: boolean = true;
  divisionOptions$ = new BehaviorSubject([]);
  dialogTitle: string = ``;
  constructor(
    public dialogRef: MatDialogRef<ManageDivisionDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private snackbar: MatSnackBar, public languageService: LanguageService, private fb: FormBuilder, private httpService: HttpService, private cacheDataService: CacheDataService,) { }
  nameAlreadyExists: boolean = false;
  initializeOnEdit() {
    if (this.data.editing) {
      this.dialogTitle = `Edit Sub Division`;
      this.formGroup.get('name').setValue(this.data.name, {
        emitEvent: false,
        onlySelf: true
      });
      this.formGroup.get('division').setValue(this.data.division, {
        emitEvent: false,
        onlySelf: true
      });
    } else
      this.dialogTitle = `Add new Sub Division`;
  }
  ngOnInit(): void {
    this.formGroup = this.fb.group({
      name: ['', [Validators.required, Validators.maxLength(64), noWhitespaceValidator()]],
      division: ['', Validators.required],
    })
    this.cacheDataService.getInformationsForCaching().pipe(
      finalize(() => this.loadingDataForDropdown = false)
    ).subscribe(() => {
      //initial divisions dropdown
      this.divisionOptions$.next(this.cacheDataService.getDivisionsSubject().value);
    });
    this.initializeOnEdit();
  }
  onCancel() {
    this.dialogRef.close();
  }
  onSubmitForm() {
    if (!this.formGroup.valid) {
      this.formGroup.get('division').markAsTouched();
      this.formGroup.get('name').markAsTouched();
      return;
    }
    let user = {
      name: this.formGroup.get('name').value,
      divisionId: this.formGroup.get('division').value
    };
    this.isLoading = true;
    if (this.data.editing) {
      let updateFormat = {
        name: this.formGroup.get('name').value,
        divisionId: this.formGroup.get('division').value,
        id: this.data.id
      }
      this.httpService.updateSubDivisionV1(updateFormat).pipe(
        finalize(() => this.isLoading = false)
      ).subscribe(
        (res: any) => {
          console.log(res);
          this.dialogRef.close(true);
          this.snackbar.open(res.message, 'OK', { duration: 2000 })
        },
        error =>
          this.snackbar.open(`Sub-Division with this name already exists`, `OK`, { duration: 2000 })
      );
      return;
    }
    this.httpService.createSubDivisionV1(user).pipe(
      finalize(() => this.isLoading = false)
    ).subscribe(
      (res: any) => {
        this.dialogRef.close(true);
        this.snackbar.open(res.message, 'OK', { duration: 2000 })
      },
      error => {
        this.snackbar.open(error?.error?.Message, 'OK', { duration: 2000 })
      }
    );
  }
}