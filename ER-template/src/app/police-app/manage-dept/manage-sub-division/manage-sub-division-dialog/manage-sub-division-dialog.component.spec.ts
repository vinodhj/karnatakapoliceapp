import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageSubDivisionDialogComponent } from './manage-sub-division-dialog.component';

describe('ManageSubDivisionDialogComponent', () => {
  let component: ManageSubDivisionDialogComponent;
  let fixture: ComponentFixture<ManageSubDivisionDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManageSubDivisionDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageSubDivisionDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
