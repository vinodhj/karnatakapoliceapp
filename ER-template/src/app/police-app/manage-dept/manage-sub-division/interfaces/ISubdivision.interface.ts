export interface ISubdivision {
  id?: number;
  labels?: any;
  index: number;
  division: any;
  divisionname: string;
  subdivision: string;
  isActive?: boolean;
}

export class SubDivisionV1 {
  id: number;
  branchId: number | null;
  divisionId: number | null;
  division: any;
  divisionname: string;
  name: string;
  dataDetail: string | null;
  isActive: boolean | null;
  isDeleted: boolean | null;
  createdBy: number;
  createdByUsername: string;
  createdDatetime: Date | string;
  modifiedBy: number | null;
  modifiedByUsername: string | null;
  modifiedDatetime: Date | string | null;
  serialNumber;
}