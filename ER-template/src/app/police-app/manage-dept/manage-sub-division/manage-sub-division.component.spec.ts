import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageSubDivisionComponent } from './manage-sub-division.component';

describe('ManageSubDivisionComponent', () => {
  let component: ManageSubDivisionComponent;
  let fixture: ComponentFixture<ManageSubDivisionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManageSubDivisionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageSubDivisionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
