import { SelectionModel } from '@angular/cdk/collections';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { DatePipe } from '@angular/common';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormControl, Validators } from '@angular/forms';
import { MatChipInputEvent } from '@angular/material/chips';
import { MatDialog } from '@angular/material/dialog';
import { MatFormFieldDefaultOptions, MAT_FORM_FIELD_DEFAULT_OPTIONS } from '@angular/material/form-field';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { UntilDestroy } from '@ngneat/until-destroy';
import { Observable, ReplaySubject } from 'rxjs';
import { filter, finalize } from 'rxjs/operators';
import { fadeInUp400ms } from 'src/@exai/animations/fade-in-up.animation';
import { stagger40ms } from 'src/@exai/animations/stagger.animation';
import { TableColumn } from 'src/@exai/interfaces/table-column.interface';
import { HttpService } from 'src/@exai/services/http.service';
import { LanguageService } from 'src/app/language.service';
import { ConfirmDialogComponent } from 'src/app/shared/advanced-grid-item/dialogs/confirm-dialog/confirm-dialog.component';
import { HtmlToDocxService } from 'src/app/shared/services/html-to-docx.service';
import { SnackBarService } from 'src/app/shared/services/snack-bar.service';
import { SubDivisionV1 } from './interfaces/ISubdivision.interface';
import { ManageSubDivisionDialogComponent } from './manage-sub-division-dialog/manage-sub-division-dialog.component';
@UntilDestroy()

@Component({
  selector: 'exai-manage-sub-division',
  templateUrl: './manage-sub-division.component.html',
  styleUrls: ['./manage-sub-division.component.scss'],
  animations: [
    fadeInUp400ms,
    stagger40ms
  ],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        appearance: 'standard'
      } as MatFormFieldDefaultOptions
    }
  ]
})

export class ManageSubDivisionComponent implements OnInit {

  layoutCtrl = new FormControl('boxed');
  subject$: ReplaySubject<SubDivisionV1[]> = new ReplaySubject<SubDivisionV1[]>(1);
  data$: Observable<SubDivisionV1[]> = this.subject$.asObservable();
  customers: SubDivisionV1[];
  formListData: SubDivisionV1[] = [];
  addSubDivision: FormArray;
  formLabels: Array<any>;
  deparments: any[] = [];

  @Input()
  columns: TableColumn<SubDivisionV1>[] = [
    // { label: 'Checkbox', property: 'checkbox', type: 'checkbox', visible: true },
    { label: 'Serial NO', property: 'serialNumber', type: 'serialNumber', visible: true, cssClasses: ['font-medium'] },
    { label: 'SUB-DIVISION ID', property: 'id', type: 'text', visible: true, cssClasses: ['font-medium'] },
    { label: 'Division', property: 'divisionname', type: 'text', visible: true, cssClasses: ['font-medium'] },
    { label: 'Sub-Division', property: 'name', type: 'text', visible: true, cssClasses: ['font-medium'] },
    { label: 'Created by', property: 'createdByUsername', type: 'text', visible: true, cssClasses: ['font-medium'] },
    { label: 'Created on', property: 'createdDatetime', type: 'text', visible: true, cssClasses: ['font-medium'] },
    { label: 'Modified by', property: 'modifiedByUsername', type: 'text', visible: true, cssClasses: ['font-medium'] },
    { label: 'Modified on', property: 'modifiedDatetime', type: 'text', visible: true, cssClasses: ['font-medium'] },
    { label: 'Status', property: 'isActive', type: 'button', visible: true },
    { label: 'Action', property: 'actions', type: 'button', visible: true }
  ];


  dataSource: MatTableDataSource<SubDivisionV1> | null;
  selection = new SelectionModel<SubDivisionV1>(true, []);
  searchCtrl = new FormControl();


  length: number;
  pageSize: 10;
  pageSizeOptions = [5, 10, 50];
  @ViewChild(MatPaginator) paginator: MatPaginator;

  isLoading = true

  constructor(private dialog: MatDialog,
    private httpservice: HttpService,
    private snackBarService: SnackBarService,
    public languageService: LanguageService,
    private snackbar: MatSnackBar,
    private htmlDownloadFile: HtmlToDocxService
  ) {

    this.formLabels = [
      {
        label: 'Division',
        type: 2,
        level: 1,
        dependentIndex: null,
        initialValue: '',
        multiple: false
      },
      {
        label: 'Sub Division',
        type: 1,
        level: null,
        dependentIndex: null,
        initialValue: ''
      },
    ];

    this.addSubDivision = new FormArray([
      new FormControl('', [Validators.required]),
      new FormControl('', [Validators.required]),
    ])
  }

  get visibleColumns() {
    return this.columns.filter(column => column.visible).map(column => column.property);
  }

  ngOnInit() {
    this.getDepartments();

    this.data$.pipe(
      filter<SubDivisionV1[]>(Boolean)
    ).subscribe(customers => {
      this.customers = customers;
      this.dataSource.data = customers;
    })
  }

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.columns, event.previousIndex, event.currentIndex);
  }

  addOnBlur = true;
  readonly separatorKeysCodes = [ENTER, COMMA] as const;
  searchValues = [];
  wantedSearchedValues = []

  add(event: MatChipInputEvent): void {
    this.wantedSearchedValues = []
    if (event.value) {
      this.searchValues.push(event.value);
    }

    let wantedValues = []
    this.searchValues.forEach(el => {
      if (el.split(' :').length == 1) {
        let textForSearch = el.split(' :')[0];
        this.wantedSearchedValues.push('all')
        wantedValues.push(textForSearch)
      }
      else {
        let value = el.split(' :')[1].trim()
        this.columns.forEach(column => {
          if (column.label === el.split(' :')[0]) {
            this.wantedSearchedValues.push(column.property)
          }
        })
        wantedValues.push(value)
      }
    })
    this.dataSource.filter = wantedValues.join('+')
    // Clear the input value
    event.chipInput!.clear();
  }

  remove(value): void {
    const index = this.searchValues.indexOf(value);
    let dataSourceFilters = this.dataSource.filter.split('+')
    this.wantedSearchedValues.splice(index, 1)
    let valueSplitted = value.split(':')

    if (index >= 0) {
      this.searchValues.splice(index, 1);
      if (valueSplitted.length > 1) {
        dataSourceFilters.splice(dataSourceFilters.indexOf(valueSplitted[1].trim()), 1);
      } else {
        dataSourceFilters.splice(dataSourceFilters.indexOf(valueSplitted[0].trim()), 1);
      }
      this.dataSource.filter = dataSourceFilters.join('+')
    }

    if (this.searchValues.length < 1) {
      this.dataSource.filter = ''
    }
  }

  appendToSearch(label) {
    let configuredLabelForSearch = `${label} : `
    this.searchCtrl.setValue(configuredLabelForSearch)
  }

  pipe: any = new DatePipe('en-US'); // Use your own locale

  getDepartments() {
    this.deparments = [];
    this.formListData = [];

    this.isLoading = true;

    this.httpservice.getSubDivisions().pipe(
      finalize(() => this.isLoading = false)
    ).subscribe((response: Array<any>) => {
      console.log(response);
      
      this.isLoading = false;
      for (let i = 0; i < response.length; i++) {
        if (response[i]?.division) {
          this.formListData.push(<SubDivisionV1>{
            id: response[i].id,
            name: response[i].name,
            division: response[i].division,
            divisionname: response[i].division.name,
            isActive: response[i].isActive,
            modifiedByUsername: response[i].modifiedByUsername,
            createdByUsername: response[i].createdByUsername,
            modifiedDatetime: response[i].modifiedDatetime ? this.pipe.transform(response[i].modifiedDatetime, 'MMM dd yyyy h:mm a') : "",
            createdDatetime: response[i].createdDatetime ? this.pipe.transform(response[i].createdDatetime, 'MMM dd yyyy h:mm a') : "",
            serialNumber: i + 1
          });
        }
      }

      this.dataSource = new MatTableDataSource(this.formListData);
      this.dataSource.paginator = this.paginator;
      // advanced filtering function
      this.dataSource.filterPredicate = (data, filters) => {
        const matchFilter = [];
        const filterArray = filters.split('+');
        const columns = (<any>Object).values(data);
        const columnLabels = [...this.columns.filter(column => column?.visible && !column?.property?.includes(`actions`)).map(({ property }) => property) as string[]];
        filterArray.forEach(filter => {
          let formattedFilter = filter.trim().toLowerCase()
          const customFilter = [];
          columns.forEach(column => {
            console.log(this.wantedSearchedValues);
            
            this.wantedSearchedValues.forEach(colProperty => {

              if (colProperty == 'all') {
                for (let key in data) {
                  let formattedDataValue = ((('' + data[key]).toLowerCase().replace(/\s+/g, ' ')).trim())
                  if (columnLabels.indexOf(key) >= 0 && (formattedDataValue.includes(formattedFilter) || formattedDataValue == (formattedFilter))) {
                    customFilter.push(true)
                  }
                }
              } else {
                let formattedDataColProperty = (('' + data[colProperty]).toLowerCase()).trim()
                customFilter.push(formattedDataColProperty.includes(formattedFilter))
              }
            })
          });
          matchFilter.push(customFilter.some(Boolean)); // OR
        });
        return matchFilter.every(Boolean); // AND
      }
    });
  }

  createSubDivision(initialValue: any = null, action): void {

    let dialogConfiguration: any = {
      width: '25vw',
      height: 'min-content',
      data: {
        editing: false,
      },
      disableClose: true
    }


    if (initialValue) {

      dialogConfiguration.data = {
        editing: true,
        name: initialValue.name,
        division: initialValue.division.id,
        id: initialValue.id,
      }

    }

    const dialogRef = this.dialog.open(ManageSubDivisionDialogComponent, dialogConfiguration);

    dialogRef.afterClosed().subscribe(result => {

      if (result)
        this.getDepartments();

    })
  }

  toggleColumnVisibility({ column, event }) {
    event.stopPropagation();
    event.stopImmediatePropagation();
    column.visible = !column.visible;
  }

  trackByProperty<T>(index: number, column: TableColumn<T>) {
    return column.property;
  }

  updateStatus({ event, element }) {
    let message = `Are you sure that you want to ${event?.checked ? ` unblock ` : ` block `}this sub division?`;
    const confirmDialog = this.dialog.open(ConfirmDialogComponent, {
      width: `500px`,
      data: { message: message }
    });
    confirmDialog.afterClosed().subscribe(
      result => result?.answer ? this.sendBlockRequest({ event, element }) : this.getDepartments());
  }

  sendBlockRequest({ event, element }): void {
    let status = {
      id: element?.id,
      blocked: !event?.checked
    }
    this.httpservice.updateStatusSubDivisionV1(status).subscribe((data: any) => {
      if ((data as any)?.id !== null && (data as any)?.id !== undefined && (data as any)?.id !== ``) {
        this.snackBarService.showMessage(data?.message, 2000);
      } else {
        this.snackBarService.showMessage(`Something went wrong.`, 2000);
      }
      this.getDepartments()
    },
      error => {
        error?.toString().startsWith(`Error Code: 400`) ?
          this.snackbar.open(`Active Sub-Division with this name already exists.`, `OK`, { duration: 2000 }) :
          this.snackbar.open(error, `OK`, { duration: 2000 });
        this.getDepartments();
      }
    )
  }

  public getSubDivisions(rowValue) {

    return rowValue.Subdivision;
  }

  public getPoliceStations(row) {

    return row.Policestation;
  }

  downloadWord(type: string) {
    let formattedTableValues = []

    const matchFields = {
      serialNumber: 'SERIAL NO',
      id: 'SUB-DIVISION ID',
      divisionname: "DIVISION",
      name: "SUB-DIVISION",
      createdByUsername: "CREATED BY",
      createdDatetime: "CREATED ON",
      modifiedByUsername: "MODIFIED BY",
      modifiedDatetime: "MODIFIED ON",
      isActive: "STATUS"
    }

    this.columns.map(column => {

      if(!column.visible) {

        for (const rowKey in matchFields) {

          const valueOfRow = matchFields[rowKey]

          if(valueOfRow.toLowerCase() === column.label.toLowerCase())
            delete matchFields[rowKey];

        }

      }

    })
    
    //if user filtered data then export only that data else all data
    const dataForExport = this.dataSource.filter ? this.dataSource.connect().value : this.dataSource.data;


    dataForExport.forEach((row: any) => {

      row.subDivisionName = this.getSubDivisions(row);
      row.policeStationsName = this.getPoliceStations(row);

      let oneRowInTable = {};

      for (const rowKey in matchFields) {
        const headerName = matchFields[rowKey];

        if (headerName === 'STATUS') {

          oneRowInTable[headerName] = row[rowKey] ? 'Active' : 'Inactive';

        } else {

          oneRowInTable[headerName] = row[rowKey];

        }



      }

      formattedTableValues.push(oneRowInTable);

    })


    this.htmlDownloadFile.downloadBasedOnType(type, formattedTableValues, 'sub-divisions')
  }

}
