export class IDivision {
    id?: number;
    labels?: any;
    index: number;
    division: string;
  }