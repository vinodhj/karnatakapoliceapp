import { SelectionModel } from '@angular/cdk/collections';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { ChangeDetectorRef, Component, Input, ViewChild } from '@angular/core';
import { FormArray, FormControl, Validators } from '@angular/forms';
import { MatChipInputEvent } from '@angular/material/chips';
import { MatDialog } from '@angular/material/dialog';
import { MatFormFieldDefaultOptions, MAT_FORM_FIELD_DEFAULT_OPTIONS } from '@angular/material/form-field';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { UntilDestroy } from '@ngneat/until-destroy';
import { Observable, ReplaySubject } from 'rxjs';
import { filter } from 'rxjs/operators';
import { fadeInUp400ms } from 'src/@exai/animations/fade-in-up.animation';
import { stagger40ms } from 'src/@exai/animations/stagger.animation';
import { TableColumn } from 'src/@exai/interfaces/table-column.interface';
import { HttpService } from 'src/@exai/services/http.service';
import { LanguageService } from 'src/app/language.service';
import { ConfirmDialogComponent } from 'src/app/shared/advanced-grid-item/dialogs/confirm-dialog/confirm-dialog.component';
import { EnumerableResponse } from 'src/app/shared/models/enumerable-response';
import { HtmlToDocxService } from 'src/app/shared/services/html-to-docx.service';
import { SnackBarService } from 'src/app/shared/services/snack-bar.service';
import { DivisionViewModel } from '../../models/division-view-model';
import { ManageDivisionDialogComponent } from './manage-division-dialog/manage-division-dialog.component';

@Component({
  selector: 'exai-manage-division',
  templateUrl: './manage-division.component.html',
  styleUrls: ['./manage-division.component.scss'],
  animations: [
    fadeInUp400ms,
    stagger40ms
  ],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        appearance: 'standard'
      } as MatFormFieldDefaultOptions
    }
  ]
})

@UntilDestroy()

export class ManageDivisionComponent {

  layoutCtrl = new FormControl('boxed');
  subject$: ReplaySubject<DivisionViewModel[]> = new ReplaySubject<DivisionViewModel[]>(1);
  data$: Observable<DivisionViewModel[]> = this.subject$.asObservable();
  customers: DivisionViewModel[];
  formListData: DivisionViewModel[] = [];
  departments: any[] = [];
  addDivision: FormArray;
  formLabels: Array<any>;

  @Input()
  columns: TableColumn<DivisionViewModel>[] = [
    // { label: 'Checkbox', property: 'checkbox', type: 'checkbox', visible: true },
    { label: 'Serial NO', property: 'serialNumber', type: 'serialNumber', visible: true, cssClasses: ['font-medium'] },
    { label: 'Division Id', property: 'id', type: 'text', visible: true, cssClasses: ['font-medium'] },
    { label: 'Division', property: 'name', type: 'text', visible: true, cssClasses: ['font-medium'] },
    { label: 'Created by', property: 'createdByUsername', type: 'text', visible: true, cssClasses: ['font-medium'] },
    { label: 'Created on', property: 'createdDatetime', type: 'text', visible: true, cssClasses: ['font-medium'] },
    { label: 'Modified by', property: 'modifiedByUsername', type: 'text', visible: true, cssClasses: ['font-medium'] },
    { label: 'Modified on', property: 'modifiedDatetime', type: 'text', visible: true, cssClasses: ['font-medium'] },
    { label: 'Status', property: 'isActive', type: 'button', visible: true },
    { label: 'Action', property: 'actions', type: 'button', visible: true },
  ];

  length: number;
  pageSize: 10;
  pageSizeOptions = [5, 10, 50];

  @ViewChild(MatPaginator) paginator: MatPaginator;

  dataSource: MatTableDataSource<DivisionViewModel> | null;
  selection = new SelectionModel<DivisionViewModel>(true, []);
  searchCtrl = new FormControl();

  isLoading = true

  constructor(private dialog: MatDialog,
    private httpservice: HttpService,
    private snackBarService: SnackBarService,
    public languageService: LanguageService,
    private htmlToDocxService: HtmlToDocxService,
    private changeDetectionRef: ChangeDetectorRef
  ) {

    this.formLabels = [
      {
        label: 'Division Name',
        type: 1,
        level: null,
        dependentIndex: null,
        initialValue: '',
      },
    ];

    this.addDivision = new FormArray([
      new FormControl('', [Validators.required]),
    ])
  }

  get visibleColumns() {
    return this.columns.filter(column => column.visible).map(column => column.property);
  }

  ngOnInit() {
    this.getDepartments();

    this.data$.pipe(
      filter<DivisionViewModel[]>(Boolean)
    ).subscribe(customers => {
      this.customers = customers;
      this.dataSource.data = customers;
    });
  }

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.columns, event.previousIndex, event.currentIndex);
  }

  addOnBlur = true;
  readonly separatorKeysCodes = [ENTER, COMMA] as const;
  searchValues = [];

  wantedSearchedValues = []

  add(event: MatChipInputEvent): void {
    this.wantedSearchedValues = []
    // Add our fruit
    if (event.value) {
      this.searchValues.push(event.value);
    }

    let wantedValues = []
    this.searchValues.forEach(el => {
      if (el.split(' :').length == 1) {
        let textForSearch = el.split(' :')[0];
        this.wantedSearchedValues.push('all')
        wantedValues.push(textForSearch)
      }
      else {
        let value = el.split(' :')[1].trim()
        this.columns.forEach(column => {
          if (column.label === el.split(' :')[0]) {
            this.wantedSearchedValues.push(column.property)
          }
        })
        wantedValues.push(value)
      }
    })
    this.dataSource.filter = wantedValues.join('+')

    console.log(this.dataSource.filter);

    // Clear the input value
    event.chipInput!.clear();
  }

  remove(value): void {
    const index = this.searchValues.indexOf(value);
    let dataSourceFilters = this.dataSource.filter.split('+')
    this.wantedSearchedValues.splice(index, 1)
    let valueSplitted = value.split(':')

    if (index >= 0) {
      this.searchValues.splice(index, 1);
      if (valueSplitted.length > 1) {
        dataSourceFilters.splice(dataSourceFilters.indexOf(valueSplitted[1].trim()), 1);
      } else {
        dataSourceFilters.splice(dataSourceFilters.indexOf(valueSplitted[0].trim()), 1);
      }
      this.dataSource.filter = dataSourceFilters.join('+')
    }

    if (this.searchValues.length < 1) {
      this.dataSource.filter = ''
    }
  }

  appendToSearch(label) {
    let configuredLabelForSearch = `${label} : `
    this.searchCtrl.setValue(configuredLabelForSearch)
  }

  camelCaseToNormal(text) {
    const result = text.replace(/([A-Z])/g, " $1");
    const finalResult = result.charAt(0).toUpperCase() + result.slice(1);
    return finalResult
  }

  getDepartments() {
    this.isLoading = true
    this.dataSource = new MatTableDataSource<DivisionViewModel>();
    this.httpservice.getDivisionsV1().subscribe(
      (response: EnumerableResponse<DivisionViewModel>) => {

        response.items.forEach((row, index: number) => {
          row.serialNumber = index + 1
        })
        
        this.isLoading = false

        this.formListData = response?.items;
        this.dataSource = new MatTableDataSource<DivisionViewModel>(this.formListData);
        // advanced filtering function
        this.dataSource.filterPredicate = (data, filters) => {
          const matchFilter = [];
          const filterArray = filters.split('+');
          const columns = Object.values(data);
          const columnLabels = [...this.columns.filter(column => column?.visible && !column?.property?.includes(`actions`)).map(({ property }) => property) as string[]];
          filterArray.forEach(filter => {
            let formattedFilter = filter.trim().toLowerCase()
            const customFilter = [];

            columns.forEach(column => {
              this.wantedSearchedValues.forEach(colProperty => {

                if (colProperty == 'all') {
                  for (let key in data) {
                    let formattedDataValue = ((('' + data[key]).toLowerCase().replace(/\s+/g, ' ')).trim())
                    if (columnLabels.indexOf(key) >= 0 && (formattedDataValue.includes(formattedFilter) || formattedDataValue == (formattedFilter))) {
                      customFilter.push(true)
                    }
                  }
                } else {
                  if (colProperty === 'isActive') {
                    //  console.log(colProperty);
                    let formattedDataColProperty

                    if (data[colProperty]) {
                      formattedDataColProperty = 'true'
                    } else {
                      formattedDataColProperty = 'false'
                    }

                    customFilter.push(formattedDataColProperty.includes(formattedFilter) || formattedDataColProperty == formattedFilter)
                  } else {
                    let formattedDataColProperty = (('' + data[colProperty]).toLowerCase().replace(/\s+/g, ' ')).trim()
                    customFilter.push(formattedDataColProperty.includes(formattedFilter))
                  }
                }
              })
            });
            matchFilter.push(customFilter.some(Boolean)); // OR
          });

          return matchFilter.every(Boolean); // AND
        }
        this.dataSource.paginator = this.paginator;
      });
  }

  createDivision(initialValue = null) {
    let dialogConfiguration = {
      width: '25vw',
      height: 'min-content',
      data: {
        editing: false
      },
      disableClose: true
    }

    const dialogRef = this.dialog.open(ManageDivisionDialogComponent, dialogConfiguration);

    dialogRef.afterClosed().subscribe(result => {

      if (result)
        this.getDepartments();

    })
  }

  updateDivision(initialValue: any = null): void {

    let dialogConfiguration = {
      width: '25vw',
      height: 'min-content',
      data: {
        editing: true,
        values: initialValue
      },
      disableClose: true
    }

    const dialogRef = this.dialog.open(ManageDivisionDialogComponent, dialogConfiguration);

    dialogRef.afterClosed().subscribe(result => {

      if (result)
        this.getDepartments();

    })

    // dialogConfig.data = {
    //   labels: this.formLabels,
    //   controls: this.addDivision
    // }
    // const dialogRef = this.dialog.open(PopUpComponent, dialogConfig);

    // dialogRef.afterClosed().subscribe(result => {
    //   if (result) {
    //     const user = {
    //       id: initialValue ? initialValue.id : 0,
    //       name: initialValue ? initialValue.name : "",
    //       value: result.value[0],
    //       levelId: initialValue ? initialValue.levelId : 1,
    //       parentId: initialValue ? initialValue.parentId : null,
    //       orderNo: initialValue ? initialValue.orderNo : 0,
    //       isActive: initialValue ? initialValue.isActive : true,
    //       parent: initialValue ? initialValue.parent : ""
    //     };
    //     this.httpservice.divisionNameExists(user.value).pipe(
    //       switchMap((data: any)=> {
    //         console.log(data);
    //         if (!data) {
    //           return this.httpservice.updateDivision(user)
    //         } else {
    //           this.snackBarService.showMessage('Division Name Already Exists', 2000)
    //           return []
    //         }
    //       })
    //     ).subscribe(result => {
    //       console.log(result);
    //         this.addDivision.reset()
    //         this.getDepartments();
    //         this.snackBarService.showMessage('Saved Successfully')
    //     })
    //   }
    // });
  }

  onClickDelete(initialValue: any) {
    const user = {
      id: initialValue ? initialValue.id : 0,
      name: initialValue ? initialValue.name : "",
      value: initialValue ? initialValue.value : "",
      levelId: initialValue ? initialValue.levelId : 1,
      parentId: initialValue ? initialValue.parentId : null,
      orderNo: initialValue ? initialValue.orderNo : 0,
      isActive: false,
      parent: initialValue ? initialValue.parent : ""
    };
    this.httpservice.createDepartmentsBylevel(user).subscribe(
      (res) => {
        this.getDepartments();
        this.snackBarService.showMessage('Division Deleted Successfully')
      },
      (err: any) => {
        this.snackBarService.showMessage('Please Enter Division Name')
      }
    );
  }

  toggleColumnVisibility({ column, event }) {
    event.stopPropagation();
    event.stopImmediatePropagation();
    column.visible = !column.visible;
  }

  trackByProperty<T>(index: number, column: TableColumn<T>) {
    return column.property;
  }

  updateStatus({ event, element }) {
    let message = `Are you sure that you want to ${event?.checked ? ` unblock ` : ` block `}this division?`;
    const confirmDialog = this.dialog.open(ConfirmDialogComponent, {
      width: `500px`,
      data: { message: message }
    });
    confirmDialog.afterClosed().subscribe(
      result => result?.answer ? this.sendBlockRequest({ event, element }) : this.getDepartments());
  }

  sendBlockRequest({ event, element }): void {
    const requestModel = {
      id: element?.id,
      branchId: element?.branchId,
      isActive: event?.checked,
      isDeleted: !event?.checked,
      createdBy: 0
    }
    this.httpservice.updateStatusDivisionV1(requestModel).subscribe(
      (data: any) => {
        data?.status ? this.snackBarService.showMessage(data?.message, 2000) : null;
        this.getDepartments();
      },
      error => this.snackBarService.showMessage(error?.error?.message || "Error happened", 2000));
  }


  public getSubDivisions(rowValue) {

    return rowValue.Subdivision;
  }

  public getPoliceStations(row) {

    return row.Policestation;
  }

  downloadWord(type: string) {

    let formattedTableValues = []

    const matchFields = {
      serialNumber: 'SERIAL NO',
      id: 'DIVISION ID',
      name: "DIVISION",
      createdByUsername: "CREATED BY",
      createdDatetime: "CREATED ON",
      modifiedByUsername: "MODIFIED BY",
      modifiedDatetime: "MODIFIED ON",
      isActive: "STATUS",
    }

    this.columns.map(column => {

      if (!column.visible) {

        for (const rowKey in matchFields) {

          const valueOfRow = matchFields[rowKey]

          if (valueOfRow.toLowerCase() === column.label.toLowerCase())
            delete matchFields[rowKey];

        }

      }

    })

    //if user filtered data then export only that data else all data
    const dataForExport = this.dataSource.filter ? this.dataSource.connect().value : this.dataSource.data;

    dataForExport.forEach((row: any) => {

      row.subDivisionName = this.getSubDivisions(row);
      row.policeStationsName = this.getPoliceStations(row);

      let oneRowInTable = {};

      for (const rowKey in matchFields) {
        const headerName = matchFields[rowKey];

        if (headerName === 'STATUS') {

          oneRowInTable[headerName] = row[rowKey] ? 'Active' : 'Inactive';

        } else {

          oneRowInTable[headerName] = row[rowKey];

        }

      }

      formattedTableValues.push(oneRowInTable);

    })


    this.htmlToDocxService.downloadBasedOnType(type, formattedTableValues, 'divisions');
  }

}