import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageDivisionDialogComponent } from './manage-division-dialog.component';

describe('ManageDivisionDialogComponent', () => {
  let component: ManageDivisionDialogComponent;
  let fixture: ComponentFixture<ManageDivisionDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManageDivisionDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageDivisionDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
