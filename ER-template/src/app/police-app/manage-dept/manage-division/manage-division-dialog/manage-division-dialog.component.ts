import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { finalize } from 'rxjs/operators';
import { HttpService } from 'src/@exai/services/http.service';
import { LanguageService } from 'src/app/language.service';
import { noWhitespaceValidator } from 'src/app/shared/models/no-whitespace.validator';

@Component({
  selector: 'exai-manage-division-dialog',
  templateUrl: './manage-division-dialog.component.html',
  styleUrls: ['./manage-division-dialog.component.scss']
})

export class ManageDivisionDialogComponent implements OnInit {

  formGroup: FormGroup;
  isLoading: boolean = false;

  dialogTitle: string = ``;

  constructor(
    public dialogRef: MatDialogRef<ManageDivisionDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private snackbar: MatSnackBar,
    public languageService: LanguageService,
    private fb: FormBuilder,
    private httpService: HttpService
  ) { }

  nameAlreadyExists: boolean = false;

  initializeOnEdit() {

    if (this.data.values) {
      this.dialogTitle = `Edit Division`;
      this.formGroup.get('name').setValue(this.data.values.name, {
        emitEvent: false,
        onlySelf: true
      });
    } else
      this.dialogTitle = `Add new Division`;

  }

  ngOnInit(): void {

    this.formGroup = this.fb.group({
      name: ['', [Validators.required, Validators.maxLength(64), noWhitespaceValidator()]]
    })

    // this.formGroup.get('name').valueChanges.pipe(
    //   distinctUntilChanged(),
    //   debounceTime(70),
    //   switchMap(value => !this.data.editing ? this.httpService.divisionNameExists(value, null) : of(false))
    // ).subscribe((value: boolean) => {
    //   this.nameAlreadyExists = value;

    //   if (value)
    //     this.snackbar.open('Division name already exists', 'OK', {
    //       duration: 2000
    //     })
    // })

    this.initializeOnEdit();

  }

  onCancel() {
    this.dialogRef.close();
  }

  onSubmitForm() {
    const data = {
      id: this.data?.values?.id,
      branchId: this.httpService.branchId,
      name: this.formGroup.get(`name`)?.value,
      dataDetail: null,
      isActive: true,
      isDeleted: false,
      createdBy: 0
    }

    this.isLoading = true;

    if (this.data.editing) {
      this.httpService.updateDivisionV1(data).subscribe(
        response => {
          this.dialogRef.close(true);
          this.snackbar.open((response as any)?.message, 'OK', { duration: 2000 });
        }
      )
    } else {
      this.httpService.createDivisionV1(data).pipe(
        finalize(() => this.isLoading = false)
      ).subscribe((response) => {
        this.dialogRef.close(true);
        this.snackbar.open((response as any)?.message, 'OK', { duration: 2000 })
      })
    }
  }
}
