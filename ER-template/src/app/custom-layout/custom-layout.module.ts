import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutModule } from '../../@exai/layout/layout.module';
import { CustomLayoutComponent } from './custom-layout.component';
import { SidenavModule } from '../../@exai/layout/sidenav/sidenav.module';
import { ToolbarModule } from '../../@exai/layout/toolbar/toolbar.module';
import { FooterModule } from '../../@exai/layout/footer/footer.module';
import { ConfigPanelModule } from '../../@exai/components/config-panel/config-panel.module';
import { SidebarModule } from '../../@exai/components/sidebar/sidebar.module';
import { QuickpanelModule } from '../../@exai/layout/quickpanel/quickpanel.module';


@NgModule({
  declarations: [CustomLayoutComponent],
  imports: [
    CommonModule,
    LayoutModule,
    SidenavModule,
    ToolbarModule,
    FooterModule,
    ConfigPanelModule,
    SidebarModule,
    QuickpanelModule
  ]
})
export class CustomLayoutModule {
}
