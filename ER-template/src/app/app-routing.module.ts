import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthenticationGuard } from './guards/authentication.guard';

const routes: Routes = [
  {
    path: ``,
    redirectTo: `login`,
    pathMatch: `full`
  },
  {
    path: `login`,
    loadChildren: () => import(`./police-app/login/login.module`).then(m => m.LoginModule)
  },
  {
    path: '', loadChildren: () => import('./police-app/police-app.module').then(m => m.PoliceAppModule),
    canActivate: [AuthenticationGuard],
  },
  {
    path: `**`,
    redirectTo: `dashboard`
  }
];
@NgModule({
  imports: [RouterModule.forRoot(routes, {
    preloadingStrategy: PreloadAllModules,
    scrollPositionRestoration: 'enabled',
    relativeLinkResolution: 'corrected',
    anchorScrolling: 'enabled',
    useHash: true
  })],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
