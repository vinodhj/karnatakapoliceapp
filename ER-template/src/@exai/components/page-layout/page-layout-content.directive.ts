import { Directive } from '@angular/core';

@Directive({
  selector: '[exaiPageLayoutContent],exai-page-layout-content',
  host: {
    class: 'exai-page-layout-content'
  }
})
export class PageLayoutContentDirective {

  constructor() { }

}
