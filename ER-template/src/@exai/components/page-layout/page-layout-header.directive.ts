import { Directive } from '@angular/core';

@Directive({
  selector: '[exaiPageLayoutHeader],exai-page-layout-header',
  host: {
    class: 'exai-page-layout-header'
  }
})
export class PageLayoutHeaderDirective {

  constructor() { }

}

