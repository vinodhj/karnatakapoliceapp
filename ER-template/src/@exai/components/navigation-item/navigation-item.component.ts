import { Component, Input, OnInit } from '@angular/core';
import { NavigationItem, NavigationLink } from '../../interfaces/navigation-item.interface';
import { filter, map, startWith, tap } from 'rxjs/operators';
import { ActivatedRoute, NavigationEnd, NavigationStart, Router } from '@angular/router';
import { NavigationService } from '../../services/navigation.service';
import { trackByRoute } from '../../utils/track-by';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ReportsDialogComponent } from 'src/app/police-app/reports/components/reports-dialog/reports-dialog.component';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'exai-navigation-item',
  templateUrl: './navigation-item.component.html',
  styleUrls: ['./navigation-item.component.scss']
})

export class NavigationItemComponent implements OnInit {

  @Input() item: NavigationItem;
  reportsDialogRef: MatDialogRef<ReportsDialogComponent>;

  isActive$ = this.router.events.pipe(
    filter(event => event instanceof NavigationEnd),
    startWith(null),
    map(() => (item: NavigationItem) => this.hasActiveChilds(item) || this.isReports(item))
  );

  // reportsDialogSubject = new BehaviorSubject<boolean>(false);
  // isActiveReportsDialog = false;

  isLink = this.navigationService.isLink;
  isDropdown = this.navigationService.isDropdown;
  isSubheading = this.navigationService.isSubheading;
  isDialog = this.navigationService.isDialog;
  trackByRoute = trackByRoute;

  constructor(
    private navigationService: NavigationService,
    private router: Router,
    private route: ActivatedRoute,
    private dialog: MatDialog
  ) { }

  ngOnInit() { }

  isReports(item): boolean {
    if (item?.route === `/reports`) {
      return this.router.url.includes(`reports`);
    }
    return false;
  }

  hasActiveChilds(parent: NavigationItem): boolean {
    if (this.isLink(parent)) {
      if (parent.queryParams) {
        return this.route.snapshot.queryParamMap.get('formType') === parent.queryParams
      } else {
        return this.router.isActive(parent.route as string, true);
      }
    }
    if (this.isDropdown(parent) || this.isSubheading(parent)) {
      return parent.children.some(child => {
        if (this.isDropdown(child)) {
          return this.hasActiveChilds(child);
        }

        if ((this.isLink(child) && !this.isFunction(child.route))) {
          return this.router.isActive(child.route as string, true) || this.route.snapshot.queryParamMap.get('formType') === child.queryParams;
        }
        return false;
      });
    }
    return false;
  }

  isFunction(prop: NavigationLink['route']) {
    return prop instanceof Function;
  }

  openDialog(route: string): void {
    if (route === `/reports`) {
      let dialogConfiguration: any = {
        width: `30vw`,
        height: `min-content`,
        disableClose: true,
        data: {
          editing: false
        }
      }
      this.reportsDialogRef = this.dialog.open(ReportsDialogComponent, dialogConfiguration);
      // this.isActiveReportsDialog = true;
      // var elements = document.querySelectorAll(".navigation-item");
      // let e;
      // elements.forEach(element => {
      //   if (element.classList.contains("text-primary-contrast")) {
      //     e = element;
      //     e?.classList.remove("text-primary-contrast");
      //     e?.classList.remove("bg-primary");
      //     e?.classList.add("navigation-color");
      //   }
      // })
      this.reportsDialogRef.afterClosed().subscribe(result => {
        // if (this.router.url?.startsWith(`/reports`)) {
        //   var reports = document.querySelector("#reports");
        //   reports?.classList.add("text-primary-contrast");
        //   reports?.classList.add("bg-primary");
        //   reports?.classList.remove("navigation-color");
        // } else {
        //   e?.classList.add("text-primary-contrast");
        //   e?.classList.add("bg-primary");
        //   e?.classList.remove("navigation-color");
        // }
        // this.isActiveReportsDialog = false;
      });
    }
  }
}