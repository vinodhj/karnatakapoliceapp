import { Component, Input } from '@angular/core';
import { map } from 'rxjs/operators';
import { ConfigService } from '../../services/config.service';
import { LayoutService } from '../../services/layout.service';
import { NavigationService } from '../../services/navigation.service';
import { trackByRoute } from '../../utils/track-by';

@Component({
  selector: 'exai-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent {
  roleName = sessionStorage.getItem('roleName');

  @Input() collapsed: boolean;

  collapsedOpen$ = this.layoutService.sidenavCollapsedOpen$;
  title$ = this.configService.config$.pipe(map(config => config.sidenav.title));

  imageUrl$ = this.configService.config$.pipe(map(config => config.sidenav.imageUrl));

  showCollapsePin$ = this.configService.config$.pipe(map(config => config.sidenav.showCollapsePin));

  items = this.navigationService.getItems();
  trackByRoute = trackByRoute;
  constructor(private navigationService: NavigationService,
    private layoutService: LayoutService,
    private configService: ConfigService) {}

  onMouseEnter() {
    this.layoutService.collapseOpenSidenav();
  }
  
  onMouseLeave() {
    this.layoutService.collapseCloseSidenav();
  }

  toggleCollapse() {
    this.collapsed ? this.layoutService.expandSidenav() : this.layoutService.collapseSidenav();
  }
}
