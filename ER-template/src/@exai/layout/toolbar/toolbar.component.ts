import { ChangeDetectorRef, Component, ElementRef, HostBinding, Input, NgZone, OnInit, ViewChild } from '@angular/core';
import { LayoutService } from '../../services/layout.service';
import icBookmarks from '@iconify/icons-ic/twotone-bookmarks';
import emojioneUS from '@iconify/icons-emojione/flag-for-flag-united-states';
import emojioneDE from '@iconify/icons-emojione/flag-for-flag-germany';
import icMenu from '@iconify/icons-ic/twotone-menu';
import { ConfigService } from '../../services/config.service';
import { catchError, map } from 'rxjs/operators';
import icPersonAdd from '@iconify/icons-ic/twotone-person-add';
import icAssignmentTurnedIn from '@iconify/icons-ic/twotone-assignment-turned-in';
import icBallot from '@iconify/icons-ic/twotone-ballot';
import icDescription from '@iconify/icons-ic/twotone-description';
import icAssignment from '@iconify/icons-ic/twotone-assignment';
import icReceipt from '@iconify/icons-ic/twotone-receipt';
import icDoneAll from '@iconify/icons-ic/twotone-done-all';
import { NavigationService } from '../../services/navigation.service';
import icArrowDropDown from '@iconify/icons-ic/twotone-arrow-drop-down';
import { PopoverService } from '../../components/popover/popover.service';
import { MegaMenuComponent } from '../../components/mega-menu/mega-menu.component';
import icSearch from '@iconify/icons-ic/twotone-search';
import { NavigationEnd, Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { BehaviorSubject } from 'rxjs';
import { HttpService } from 'src/@exai/services/http.service';
import { error } from '@angular/compiler/src/util';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FormControl } from '@angular/forms';
import { LanguageService } from 'src/app/language.service';
import { NotificationsDialogComponent } from 'src/app/police-app/notifications/notifications-dialog/notifications-dialog.component';
import { BroadcastService } from 'src/app/shared/services/broadcast.service';
@Component({
  selector: 'exai-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {

  @ViewChild('configpanel') configpanel;

  @Input() mobileQuery: boolean;

  @Input()
  @HostBinding('class.shadow-b')
  hasShadow: boolean;

  roleName = sessionStorage.getItem('roleName');

  languageSelected:FormControl = new FormControl('en');

  navigationItems = this.navigationService.getItems();

  isHorizontalLayout$ = this.configService.config$.pipe(map(config => config.layout === 'horizontal'));
  isVerticalLayout$ = this.configService.config$.pipe(map(config => config.layout === 'vertical'));
  isNavbarInToolbar$ = this.configService.config$.pipe(map(config => config.navbar.position === 'in-toolbar'));
  isNavbarBelowToolbar$ = this.configService.config$.pipe(map(config => config.navbar.position === 'below-toolbar'));

  icSearch = icSearch;
  icBookmarks = icBookmarks;
  emojioneUS = emojioneUS;
  emojioneDE = emojioneDE;
  icMenu = icMenu;
  icPersonAdd = icPersonAdd;
  icAssignmentTurnedIn = icAssignmentTurnedIn;
  icBallot = icBallot;
  icDescription = icDescription;
  icAssignment = icAssignment;
  icReceipt = icReceipt;
  icDoneAll = icDoneAll;
  icArrowDropDown = icArrowDropDown;

  constructor(private layoutService: LayoutService,
    private configService: ConfigService,
    private navigationService: NavigationService,
    private popoverService: PopoverService,
    private router: Router,
    private services: HttpService,
    private snackBar: MatSnackBar,
    public languageService : LanguageService,
    private broadcastService: BroadcastService,
    private ngZone: NgZone
  ) { }

  routerSubscription: Subscription;
  HomeActive: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  HomeActive$: Observable<boolean> = this.HomeActive.asObservable();
  ngOnInit() {    
    this.languageSelected.valueChanges.subscribe((value:any)=>{
      this.services.getJson(value).subscribe((response:any)=>{
        this.languageService.language = response;
      })
    })
    this.languageSelected.setValue('en');

    //subscribe on message between tabs, it will emit when user logout
    this.broadcastService.messagesOfType('logoutEvent').subscribe(this.handleBroadcastMessage.bind(this));
  }

  handleBroadcastMessage = (result): void => {

    console.log(sessionStorage.getItem('id'), result.payload);

    //check if the user is the same user then logout, if it's different user then don't logout
    if(sessionStorage.getItem('id') === result.payload.userId)
      this.ngZone.run(() => this.logout()); 

  };

  openQuickpanel() {
    this.layoutService.openQuickpanel();
  }

  openSidenav() {
    this.layoutService.openSidenav();
  }

  logout() {
    this.services.clearToken();
    this.router.navigateByUrl('/login')    
    window.location.reload();
  }

  openMegaMenu(origin: ElementRef | HTMLElement) {
    this.popoverService.open({
      content: MegaMenuComponent,
      origin,
      position: [
        {
          originX: 'start',
          originY: 'bottom',
          overlayX: 'start',
          overlayY: 'top'
        },
        {
          originX: 'end',
          originY: 'bottom',
          overlayX: 'end',
          overlayY: 'top',
        },
      ]
    });
  }

  openSearch() {
    this.layoutService.openSearch();
  }

  navigateHome() {
    this.router.navigateByUrl('dashboard');
  }

}
