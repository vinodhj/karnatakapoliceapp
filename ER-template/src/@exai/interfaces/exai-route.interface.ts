import { Route } from '@angular/router';

export interface exaiRouteData {
  scrollDisabled?: boolean;
  toolbarShadowEnabled?: boolean;
  containerEnabled?: boolean;

  [key: string]: any;
}

export interface exaiRoute extends Route {
  data?: exaiRouteData;
  children?: exaiRoute[];
}

export type exaiRoutes = exaiRoute[];
