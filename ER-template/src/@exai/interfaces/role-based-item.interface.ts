import { Icon } from "@visurel/iconify-angular";
import { Role } from "src/app/shared/enums/role.enum";

export interface RoleBasedItem {
    type: string;
    label: string;
    route: string | Function | null;
    queryParams?: string
    icon: Icon | null;
    roles: Role[];
    children: RoleBasedItem[];
}