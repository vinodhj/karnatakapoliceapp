import { Icon } from '@visurel/iconify-angular';

export type NavigationItem = NavigationLink | NavigationDropdown | NavigationSubheading | NavigationDialog;

export interface NavigationLink {
  type: 'link';
  route: string | any;
  queryParams?: string,
  fragment?: string;
  label: string;
  icon?: Icon;
  routerLinkActiveOptions?: { exact: boolean };
  badge?: {
    value: string;
    bgClass: string;
    textClass: string;
  };
}

export interface NavigationDropdown {
  type: 'dropdown';
  label: string;
  icon?: Icon;
  route: string | any | null;
  queryParams?: string,
  children: Array<NavigationLink | NavigationDropdown>;
  badge?: {
    value: string;
    bgClass: string;
    textClass: string;
  };
}

export interface NavigationSubheading {
  type: 'subheading';
  label: string;
  route: string | any | null;
  children: Array<NavigationLink | NavigationDropdown>;
}

export interface NavigationDialog {
  type: `dialog`;
  route: string | any | null;
  label: string;
  icon?: Icon;
}
