export interface TableColumn<T> {
  label: string;
  property: keyof T | string;
  type: 'text' | 'image' | 'badge' | 'progress' | 'checkbox' | 'button' | 'subDivision' | 'policeStatins' | 'serialNumber'
  visible?: boolean;
  cssClasses?: string[];
}
