import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { ConfigService } from './config.service';
import { NavigationService } from './navigation.service';
import { catchError, concatMap, map, mergeMap, switchMap, tap } from 'rxjs/operators';
import { EMPTY, forkJoin, Observable, of } from 'rxjs';
import { Role } from 'src/app/shared/enums/role.enum';
import jwt_decode from "jwt-decode";
import saveAs from 'file-saver';
import { StatusDetails } from 'src/app/shared/models/status-details';
import { UserProfile } from 'src/app/shared/models/user-profile';
import { UserProfileInput } from 'src/app/shared/models/user-profile-input';
import { UserProfilePassword } from 'src/app/shared/models/user-profile-password';
import { isNullNumericIdFromSessionStorage, returnAllFormControls, returnImages } from 'src/app/utils/randomNumber';
import { EnumerableResponse } from 'src/app/shared/models/enumerable-response';
import { DivisionViewModel } from 'src/app/police-app/models/division-view-model';
import { DatePipe } from '@angular/common';
import moment from 'moment';
import { ReportDto } from 'src/app/police-app/reports/models/report';
import { PrimaryKeyDto } from 'src/app/police-app/primary-key/models/primary-key';
import { TemplateDropdownOption } from 'src/app/police-app/reports/models/template-dropdown-option';
import { SaveSettings } from 'src/app/form-builder/form-builder-elements/form-builder-excel/excel-model';

@Injectable({
  providedIn: 'root'
})

export class HttpService {
  [x: string]: any;
  tempId = "";
  token: string = null;
  name: string = null;
  id: any;



  // profile component

  fname: string = null;
  lname: string = null;
  roleId: any;
  userId: any;
  policeStation
  email;
  phone;
  roleName;
  policeStationName: string;
  subDivisionName: string;
  divisionName: string;
  userToDepartmentMapping;

  currentUsername: string
  recordCreatedBy: string
  currentRecord

  pipe = new DatePipe('en-US'); // Use your own locale

  private _emailDetails;
  public get emailDetails() {
    return this._emailDetails;
  }
  public set emailDetails(value) {
    this._emailDetails = value;
  }

  branchId: number | null = null;
  subDivisionId: number | null = null;
  divisionId: number | null = null;

  constructor(private http: HttpClient, private global: ConfigService,
    private _navigationService: NavigationService) {
    this.setTokenIfAvailable();
  }

  clearToken() {
    let B = sessionStorage.clear();
  }

  setTokenIfAvailable() {

    if (sessionStorage.getItem('token')) {
      // console.log(sessionStorage);id,fname: string =null; lname: string =null; roleId: any;userId :any; email; phone; 
      // roleName; policeStationName;// subDivisionName;// divisionName;

      this.token = sessionStorage.getItem('token');
      this.name = sessionStorage.getItem('name');
      this.id = sessionStorage.getItem('id');
      this.fname = sessionStorage.getItem('fname');
      this.lname = sessionStorage.getItem('lname');
      this.roleId = sessionStorage.getItem('roleId');
      this.userId = sessionStorage.getItem('userId');
      this.emailId = sessionStorage.getItem('emailId');
      this.phone = sessionStorage.getItem('phone');
      this.roleName = sessionStorage.getItem('roleName');
      this.policeStationName = sessionStorage.getItem('policeStationName');
      this.subDivisionName = sessionStorage.getItem('subDivisionName');
      this.divisionName = sessionStorage.getItem('divisionName');
      this.currentUsername = sessionStorage.getItem('currentUsername');

      //#region UserData
      this.branchId = isNullNumericIdFromSessionStorage(sessionStorage.getItem('branchId')) ? null : Number(sessionStorage.getItem('branchId'));
      this.subDivisionId = isNullNumericIdFromSessionStorage(sessionStorage.getItem('subDivisionId')) ? null : Number(sessionStorage.getItem('subDivisionId'));
      this.divisionId = isNullNumericIdFromSessionStorage(sessionStorage.getItem('divisionId')) ? null : Number(sessionStorage.getItem('divisionId'));
      //#endregion UserData

    } else if (this.token) {

      console.log(this.id, "HELLO FROM ID");

      sessionStorage.setItem('token', this.token);
      sessionStorage.setItem('name', this.name);
      sessionStorage.setItem('id', this.id);
      sessionStorage.setItem('fname', this.fname);
      sessionStorage.setItem('lname', this.lname);
      sessionStorage.setItem('roleId', this.roleId);
      sessionStorage.setItem('currentUsername', this.currentUsername);
      sessionStorage.setItem('userId', this.userId);
      sessionStorage.setItem('emailId', this.emailId);
      sessionStorage.setItem('phone', this.phone);
      sessionStorage.setItem('roleName', this.roleName);
      sessionStorage.setItem('policeStationName', this.userToDepartmentMapping?.value);
      sessionStorage.setItem('subDivisionName', this.userToDepartmentMapping?.parent?.value);
      sessionStorage.setItem('divisionName', this.userToDepartmentMapping?.parent?.parent?.value)
      // sessionStorage.setItem('subDivisionName' , this.subDivisionName);
      // sessionStorage.setItem('divisionName' , this.divisionName)

      //#region UserData
      sessionStorage.setItem('branchId', this.branchId?.toString());
      sessionStorage.setItem('subDivisionId', this.subDivisionId?.toString());
      sessionStorage.setItem('divisionId', this.divisionId?.toString());
      //#endregion UserData
    }

    if (!this.roleId && sessionStorage.getItem('role')) {
      var role = sessionStorage.getItem('role');
      this.roleId = Number(role);
      this._navigationService.setRole(this.roleId);
    } else if (this.roleId) {
      sessionStorage.setItem('role', String(this.roleId));
    }

    if (!this.userId && sessionStorage.getItem('id')) {
      var id = sessionStorage.getItem('id');
      this.userId = Number(id);
      // this._navigationService.setid(this.roleId);
    } else if (this.userId) {
      sessionStorage.setItem('id', String(this.userId));
    }
  }

  login(loginDetails: any) {
    return this.http.post(environment.backendUrl + "Account/login", loginDetails);
  }

  logout(logoutDetails: any) {
    return this.http.post(environment.backendUrl + "Account/logout", logoutDetails);
  }

  register(reqBody: any) {
    return this.http.post(environment.backendUrl + "Account/register", reqBody);
  }

  isLoggedIn(): boolean {
    return this.token !== null && this.token !== undefined && this.token !== ``;
  }

  getRole(): Role {
    if (!(this.roleId in Role))
      return null;
    return +this.roleId as Role;
  }

  postMasterTableData(tableData) {
    this.data
    return this.http.post(environment.backendUrl + 'PageTemplate/create', tableData)
  }

  getMasterTableData() {
    return this.http.get(environment.backendUrl + 'PageTemplate/showall')
  }

  // getDepartmentsByLevel(levelId: number, departmentIds: string = null) {
  //   let fetchUrl = `Department/getdepartmentdetails?levelId=${levelId}&parentIds=${departmentIds ? departmentIds : ``}`;
  //   return this.http.get(environment.backendUrl + fetchUrl);
  // }
  getDivisions() {
    return this.http.get(environment.backendUrl + 'divisions')
  }

  getTemplateOptions(): Observable<TemplateDropdownOption[]> {
    return this.http.get<TemplateDropdownOption[]>(environment.backendUrl + `Template/list-dropdown`);
  }

  getDivisionsV1(): Observable<EnumerableResponse<DivisionViewModel>> {
    return this.http.get<EnumerableResponse<DivisionViewModel>>(
      `${environment.backendUrl}division/GetDivision`,
      {
        params: new HttpParams({
          fromObject: { branchId: this.branchId }
        })
      });
  }

  getReports(dateFrom: string | null = ``, dateTo: string | null = ``): Observable<ReportDto[]> {
    return this.http.get<ReportDto[]>(
      `${environment.backendUrl}reports`, {
      params: new HttpParams({
        fromObject: { dateFrom, dateTo }
      })
    });
  }

  getReportsV1(formId: string, dateFrom: string | null = ``, dateTo: string | null = ``): Observable<any> {
    console.log(dateFrom, dateTo);
    return this.http.get<any>(
      // `${environment.backendUrl}reports`, {
      `${environment.backendUrl}FormRecord/detailedlistreport`, {
      params: new HttpParams({
        fromObject: { alternativeId: formId, dateFrom, dateTo }
      })
    });
  }

  getPrimaryKeys(): Observable<PrimaryKeyDto[]> {
    return this.http.get<PrimaryKeyDto[]>(`${environment.backendUrl}primary-key`);
  }

  getPrimaryKeysBySubDivision(formId): Observable<PrimaryKeyDto[]> {
    return this.http.get<PrimaryKeyDto[]>(`${environment.backendUrl}primary-key/get-by-subdivisions?templateId=${formId}`);
  }

  getDivisionsDropdownV1() {
    return this.http.get(
      `${environment.backendUrl}division/GetActiveDivision`,
      {
        params: new HttpParams({
          fromObject: { branchId: this.branchId }
        })
      });
  }

  postUsers(user: any) {
    return this.http.post(environment.backendUrl + `UserV1/addnewuser`, user);
  }
  bulkAddUsers(file: any) {
    return this.http.post(environment.backendUrl + `UserV1/bulk-insert-users`, file);
  }

  editUser(user: any) {
    return this.http.post(environment.backendUrl + `UserV1/updateuser`, user);
  }

  getUsers() {
    return this.http.get(environment.backendUrl + `UserV1/GetUsers`).pipe(
      map((users: any) => users.items),
      map(users => {

        console.log(users);

        users.map(x => {
          x.roleNames === 'Police Station' ? x.roleNames = 'Police Station Head' : x.roleNames;

          return x;
        })

        return users;

      })
    );
  }

  getForm() {
    return this.http.get(environment.backendUrl + `Dashboard/forms`);
  }

  getDepart() {
    return this.http.get(environment.backendUrl + `Dashboard/departments`);
  }

  // createDepartmentsBylevel(user: any) {
  //   return this.http.post(environment.backendUrl + `Department/savedepartment`, user);
  // }

  createDivision(division) {
    return this.http.post(environment.backendUrl + 'divisions', division)
  }

  createDivisionV1(division) {
    return this.http.post(environment.backendUrl + 'division/create', division);
  }

  updateSubDivision(subDivision) {
    return this.http.put(environment.backendUrl + 'SubDivision/modifysubdivision', subDivision)
  }

  updateSubDivisionV1(subDivision) {
    return this.http.put(environment.backendUrl + 'SubDivisionV1/modifysubdivision', subDivision)
  }

  updateDivision(division) {
    return this.http.put(environment.backendUrl + `divisions/${division.id}`, division)
  }

  updateDivisionV1(division) {
    return this.http.post(`${environment.backendUrl}division/modify`, division);
  }

  getDivision(divisionId) {
    return this.http.get(environment.backendUrl + `divisions/${divisionId}`)
  }

  getSubDivisions() {
    return this.http.get(environment.backendUrl + 'SubDivisionV1/showallsubdivisions')
  }

  updateStatusUser(user) {
    return this.http.post(environment.backendUrl + 'User/setstatus', user)
  }

  createSubDivision(subDivision) {
    return this.http.post(environment.backendUrl + 'SubDivision/addsubdivision', subDivision)
  }

  createSubDivisionV1(subDivision) {
    return this.http.post(environment.backendUrl + 'SubDivisionV1/addsubdivision', subDivision)
  }

  getAllPoliceStations() {
    return this.http.get(environment.backendUrl + 'PoliceStation/showallpolicestations')
  }

  getAllPoliceStationsV1() {
    return this.http.get(environment.backendUrl + 'PoliceStationV1/showallpolicestations');
  }

  getAllPoliceStationsDropdown() {
    return this.http.get(environment.backendUrl + 'PoliceStationV1/showallpolicestations')
  }

  createPoliceStation(station) {
    return this.http.post(environment.backendUrl + 'PoliceStation/addpolicestation', station)
  }

  createPoliceStationV1(station) {
    return this.http.post(environment.backendUrl + `PoliceStationV1/create`, station);
  }

  createPrimaryKey(primaryKey) {
    return this.http.post(environment.backendUrl + `primary-key`, primaryKey);
  }

  updatePoliceStation(station) {
    return this.http.put(environment.backendUrl + `PoliceStation/modifypolicestation/${station.id}`, station)
  }

  updatePoliceStationV1(station) {
    return this.http.post(environment.backendUrl + `PoliceStationV1/modify`, station);
  }

  updatePrimaryKey(primaryKey) {
    return this.http.put(environment.backendUrl + `primary-key`, primaryKey);
  }

  forgotPwd(forgot) {
    // console.log(this.global.username);
    // console.log(forgot);

    // return this.http.post(environment.backendUrl+`Account/sendotp?username=superadmin`,forgot);
    return this.http.post(environment.backendUrl + `Account/sendotp?username=${forgot}`, {});
  }
  otpVerify(otp) {
    return this.http.post(environment.backendUrl + "Account/verifyotp", otp);
  }
  setPassword(updatePwd: any) {
    return this.http.post(environment.backendUrl + 'User/resetpassowrd', updatePwd);
  }

  getBasicForm() {
    return this.http.get(environment.backendUrl + `Template/list`);
  }
  getBasicFormByFormType(formType) {
    console.log(formType);

    return this.http.get(environment.backendUrl + `Template/list?templateType=${formType ? formType : 0}`);
  }
  resetPassword(userpassword: any) {
    return this.http.post(environment.backendUrl + `User/resetpassowrd`, userpassword)
  }
  getstatus(status: any) {
    return this.http.post(environment.backendUrl + `User/setstatus`, status)
  }
  getView(id) {
    return this.http.get(environment.backendUrl + `FormRecord/getrecordbyid?id=${id}`)
  }

  getViewUser(id) {
    return this.http.get(environment.backendUrl + `UserV1/view?id=${id}`);
  }

  getViewForFormBulder(id) {
    // get-form-record

    return this.http.get(environment.backendUrl + `FormRecord/get-form-record?formRecordId=${id}`, {
      responseType: 'text'
    })
      .pipe(
        switchMap((s3Link: string) => s3Link ? this.http.get(s3Link) : of(null))
      )
  }

  getViewsForFormBulder(ids, templateId) {
    const params: HttpParams = new HttpParams({ fromObject: { ids, templateId } })

    return this.http.get(environment.backendUrl + `FormRecord/views`, { params, }).pipe(
      map(({ forms, template }: any) => {
        return { template: { ...template, content: template.content }, forms }
      }),
      concatMap(({ forms, template }: any) => {
        return this.http.get(template.content).pipe(
          map(content => ({ template: { ...template, content }, forms }))
        )
      }),
      concatMap(({ forms, template }: any) => {

        const s3Links = [];

        forms.forEach(form => {
          s3Links.push(this.http.get(form.userInput))
        });

        return forkJoin(s3Links).pipe(
          map(contents => {

            let formattedForms = [];
            //match form with content from s3
            forms.forEach((form, index) => {
              formattedForms.push({ ...form, userInput: JSON.stringify(contents[index]) })
            });

            return { template, forms: formattedForms }
          })
        )
      }),
    )
  }

  getViewCompleted(id) {
    return this.http.get(environment.backendUrl + `Form/viewcomplete?id=${id}`)
  }

  getUserForm() {
    return this.http.get(environment.backendUrl + `Form/getallpublishedforms`);
  }

  getDetailedReport(templateId: number) {
    return this.http.get(environment.backendUrl + `FormRecord/detailedlist?templateId=${templateId}`);
  }

  deleteFormRecord(userFormId) {
    return this.http.delete(environment.backendUrl + '/FormRecord/deleteformrecord?id=' + userFormId)
  }

  deletePrimaryKey(primaryKeyId) {
    return this.http.delete(environment.backendUrl + 'primary-key?id=' + primaryKeyId);
  }

  postStatus(userFormId: number, status: 'AP' | 'SU' | 'RJ', comment: string, reason: string | null = null) {
    return this.http.post(environment.backendUrl + `FormRecord/action`, { userFormId, status, comment, reason });
  }

  getDepartmentsByLevel1(levelId: number, departmentIds: string = null) {
    return this.http.get(environment.backendUrl + `Department/getdepartmentdetails?levelId=${levelId}&parentIds=${departmentIds ? departmentIds : ``}`);
  }

  templateAddNew(name: string, policeStationIds: string, templateType: string) {
    return this.http.put(environment.backendUrl + `Template/addnew`, { name: name, policeStation: policeStationIds, options: templateType });
  }

  templateAddNewClone(name: string, policeStationIds: string, templateType: string, formId) {
    return this.http.put(environment.backendUrl + `Template/addnewclone?formId=${formId}`, { name: name, policeStation: policeStationIds, options: templateType });
  }

  // templateUpdateContent(templateId: number, updatedTemplateContent: string) {
  //   return this.http.post(environment.backendUrl + `Template/updatecontent`, { id: templateId, content: updatedTemplateContent });
  // }

  templateUpdateContent(templateId: number, updatedTemplateContent: string) {
    return this.http.put(environment.backendUrl + `Template/create-form-template`, { formId: templateId, templateContent: updatedTemplateContent });
  }

  templateUpdateApproveRejectContent(templateId: number, updatedTemplateApproveRejectContent: string) {
    return this.http.put(environment.backendUrl + `Template/create-approve-reject-template`, { formId: templateId, templateContent: updatedTemplateApproveRejectContent });
  }

  approveMultipleRecords(userFormIds) {
    return this.http.put(environment.backendUrl + `formRecord/approveformrecords`, {}, { params: new HttpParams({ fromObject: { userFormIds } }) })
  }

  rejectMultipleRecords(userFormIds) {
    return this.http.put(environment.backendUrl + `formRecord/rejectformrecords`, {}, { params: new HttpParams({ fromObject: { userFormIds } }) })
  }

  submitMultipleRecords(userFormIds) {
    return this.http.put(environment.backendUrl + `formRecord/submitformrecords`, {}, { params: new HttpParams({ fromObject: { userFormIds } }) })
  }

  // templateGetContent(templateId: number) {
  //   return this.http.get(environment.backendUrl + `Template/content?id=${templateId}`)
  // }

  templateGetContent(templateId: number) {
    return this.http.get(environment.backendUrl + `Template/get-form-template?formId=${templateId}`, {
      responseType: 'text'
    }).pipe(
      switchMap((s3Link: string) => s3Link ? this.http.get(s3Link) : of(null)),
    )
  }

  tableFilesUpload(files) {
    return this.http.put(environment.backendUrl + `files/upload-files`, files)
  }

  updateTableFilesUpload(file) {
    return this.http.post(environment.backendUrl + `files/get-presigned-urls`, file)
  }

  getPresignedUrls(body) {
    return this.http.post(environment.backendUrl + `files/get-presigned-urls`, body);
  }

  getPresignedUrlsPut(body) {
    return this.http.put(environment.backendUrl + `files/generate-presigned-urls`, body);
  }

  saveSpreadsheet(formData: FormData) {
    return this.http.post(environment.backendUrl + `files/save-syncfusion-excel-file`, formData, {
      responseType: 'text'
    });
  }

  templateGetApproveRejectContent(templateId: number) {
    return this.http.get(environment.backendUrl + `Template/get-approve-reject-template?formId=${templateId}`);
  }

  templateModifyPublish(status) {
    return this.http.post(environment.backendUrl + `Template/publish`, status)
  }

  templateFormRecordUpdate(id, content) {
    return this.http.put(environment.backendUrl + 'FormRecord/update-form-record', { id, content })
  }

  canUserEditFormRecord(formId) {
    return this.http.get(environment.backendUrl + `FormRecord/get-form-record-created-by?formRecordId=${formId}`);
  }

  formAddNew(formId: number, policeStationId: string, content: string, candidateName: string, candidatePhoneNo: string, candidateEmailId: string, formName: string) {
    return this.http.put(environment.backendUrl + `FormRecord/create-form-record`,
      {
        id: 0,
        formId,
        formName: formName,
        candidateName,
        candidatePhoneNo,
        candidateEmailId,
        content,
      })
  }

  formView(UserFormId: number) {
    return this.http.get(environment.backendUrl + `Form/view?id=${UserFormId}`);
  }
  // /api/FormRecord/get-form-record
  formUpdateContent(UserFormId: number, UserFormContent: string) {
    return this.http.post(environment.backendUrl + `FormRecord/updateformrecord`, { id: UserFormId, content: UserFormContent })
  }
  getJson(filename: string) {
    return this.http.get(`/assets/language/${filename}.json`)
  }

  formReportDynamic(templateId: string) {
    return this.http.get(environment.backendUrl + `Form/report/dynamic?templateId=${templateId}`, { observe: 'response', responseType: 'blob' })
  }
  formAction(userFormId: number, status: string, comment: string) {
    return this.http.post(environment.backendUrl + `FormRecord/action`, { userFormId: userFormId, status: status, comment: comment });
  }
  // ajmo
  templateUpdateDepartment(templateId: number, name: string, policeStationIds: string) {
    return this.http.post(environment.backendUrl + `Template/updatedepartment`, {
      id: templateId,
      name,
      policeStationIds
    })
  }

  updateStatusDivision(row) {
    return this.http.put(`${environment.backendUrl}divisions/${row.id}/switch-blocked-status?blocked=${row.blocked}`, row)
  }

  updateStatusDivisionV1(requestModel) {
    return this.http.post(`${environment.backendUrl}division/blockorunblock`, requestModel);
  }

  updateStatusPoliceV1(requestModel) {
    return this.http.post(`${environment.backendUrl}PoliceStationV1/blockorunblock`, requestModel);
  }

  updateStatusForm(row) {
    return this.http.put(`${environment.backendUrl}Form/${row.id}/switch-blocked-status?blocked=${row.blocked}`, row)
  }

  deleteForm(row, id = null) {
    return this.http.delete(`${environment.backendUrl}Template/deactivate-template?templateId=${row?.id || id}`);
  }

  deleteUser(row) {
    return this.http.delete(`${environment.backendUrl}UserV1/delete-user?userId=${row.id}`);
  }

  publishForm(formId: number, published: boolean) {
    return this.http.put(`${environment.backendUrl}Template/${formId}/switch-published-status`, {}, { params: new HttpParams({ fromObject: { published } }) });
  }

  emailTrigger(formId) {
    return this.http.put(`${environment.backendUrl}Template/${formId}/sendEmails`, {});
  }

  getUserRolesToCreate() {
    if (this.token === null || this.token === undefined || this.token === ``)
      return null;
    var decodedToken = jwt_decode(this.token);
    return this.http.get(`${environment.backendUrl}UserRole/getrolesbyuserid`).pipe(
      map((result: any) => {

        result.map(x => {
          x.name === 'Police Station' ? x.name = 'Police Station Head' : x.name;

          return x;
        })

        return result;
      })
    );
  }

  updateStatusSubDivision(row) {
    if (row.isActive) {
      return this.http.put(`${environment.backendUrl}SubDivision/unblocksubdivision?id=${row.index}`, row)
    } else {
      return this.http.put(`${environment.backendUrl}SubDivision/blocksubdivision?id=${row.index}`, row)
    }
  }

  updateStatusSubDivisionV1(row) {
    return this.http.put(`${environment.backendUrl}SubDivisionV1/switchblockedstatus?id=${row.id}&blocked=${row.blocked}`, {})
  }

  divisionNameExists(name, id) {
    console.log(name);

    return this.http.get(environment.backendUrl + `divisions/name-already-exists?name=${name}&id=${id ?? ''}`)
  }

  errorLogCreate(pageUrl, methodName, errorMessage) {
    // "id": 0,
    // "pageURL": "string",
    // "methodName": "string",
    // "module": "string",
    // "errorMessage": "string",
    // "createdBy": 0

    return this.http.post(environment.backendUrl + `ErrorLog/createlog`, {
      pageUrl,
      methodName,
      errorMessage,
      module: "KarnatakaPolice",
      createdBy: JSON.parse(sessionStorage.getItem('roleId')) ?? 0
    }).pipe(
      catchError(err => {

        return EMPTY;
      })
    );
  }

  getSubDivisionsDropdownOptions(divisionIds: number[]) {
    return this.http.get(`${environment.backendUrl}SubDivisionV1/getallsubdivisionsdropdown`, { params: new HttpParams({ fromObject: { divisions: divisionIds } }) });
  }

  getPoliceStationsDropdownOptions(subdivisionIds: number[]) {
    return this.http.get(`${environment.backendUrl}PoliceStationV1/getallpolicestationsdropdown`, { params: new HttpParams({ fromObject: { subDivisions: subdivisionIds } }) });
  }

  generatePdfFromS3HTML(html: string, pdfFileName: string | null = null) {
    const options: { [property: string]: any } = { headers: new HttpHeaders({ 'Content-Type': `application/json` }) };
    return this.http.put<{ htmlIdentifier: string }>(
      `https://asgardtestapi.examroom.ai:4431/item-import/files/save-html-content-to-storage`,
      { html }, options).pipe(
        switchMap(response => {
          if (response?.htmlIdentifier === null || response?.htmlIdentifier === undefined || response?.htmlIdentifier === ``)
            return of();
          options.headers.set(`Accept`, `application/pdf`);
          options.params = new HttpParams({ fromObject: { htmlIdentifier: response?.htmlIdentifier as string } });
          options.responseType = `blob` as `json`;
          return this.http.get<Blob>(`https://asgardtestapi.examroom.ai:4431/item-import/files/get-html-from-s3-as-pdf`, options);
        })
      ).subscribe({
        next: (blob: Blob) => {
          if (pdfFileName !== null && pdfFileName !== undefined && pdfFileName !== ``)
            saveAs(blob, `${pdfFileName}.pdf`);
          saveAs(blob);
        },
        error: (error: Error) => console.error(`File download: ${error}`)
      });
  }

  generatePdfFromHTML(html: string, pdfFileName: string | null = null) {

    const now = Date.now();
    const dateFormatted = this.pipe.transform(now, 'medium');

    const options: { [property: string]: any } = {
      headers: new HttpHeaders({
        'Content-Type': `application/json`,
        'Accept': `application/pdf`
      }),
      responseType: `blob` as `json`,
      params: new HttpParams({ fromObject: { pdfFileName } }),
      observe: `response`
    };
    return this.http.post(`${environment.backendUrl}files/convert-html-to-pdf`, { html }, options)
      .subscribe({
        next: (data: any) => {
          if (pdfFileName !== null && pdfFileName !== undefined && pdfFileName !== ``) {

            saveAs(data?.body, `${pdfFileName}-${dateFormatted}.pdf`);

          } else {

            saveAs(data?.body);

          }
        },
        error: (error: Error) => console.error(`File download: ${error}`)
      });

  }

  uploadHTMLToDownloadAsPDF(content) {


    var htmlstr = content.outerHTML;
    var blob = new Blob([htmlstr], { type: "text/html" });

    const formData = new FormData();

    formData.append("thumbnail", blob);

    this.http.post(`https://asgardtestapi.examroom.ai:4431/item-import/files/upload/convert-html-to-pdf`, formData).subscribe(res => {
      console.log(res);
    })
  }

  getStatusDetails(userFormId: number): Observable<StatusDetails> {
    return this.http.get<StatusDetails>(`${environment.backendUrl}FormRecord/gethistorybyrecordid`, { params: new HttpParams({ fromObject: { id: userFormId } }) });
  }

  getDivisionsDropdown() {
    return this.http.get(`${environment.backendUrl}divisions/dropdown`);
  }

  getUserProfile(): Observable<UserProfile> {
    const options: { [property: string]: any } = { headers: new HttpHeaders({ 'Content-Type': `application/json` }) };
    return this.http.get<UserProfile>(`${environment.backendUrl}User/profile`, options);
  }

  saveProfile(userProfileInput: UserProfileInput): Observable<any> {
    const options: { [property: string]: any } = { headers: new HttpHeaders({ 'Content-Type': `application/json` }) };
    return this.http.put<any>(`${environment.backendUrl}User/editprofile`, userProfileInput, options);
  }

  savePassword(userProfilePassword: UserProfilePassword): Observable<boolean> {
    const options: { [property: string]: any } = { headers: new HttpHeaders({ 'Content-Type': `application/json` }) };
    return this.http.post<boolean>(`${environment.backendUrl}Account/resetpassword`, userProfilePassword, options);
  }

  getNotifications(page: number = 1) {
    return this.http.get(`${environment.backendUrl}notifications?page=${page}`).pipe(
      map((response: any) => {

        const formattedItems = response.items?.map(item => {
          item.message = JSON.parse(item.message);

          return item;
        })

        response.items = formattedItems;

        return response;
      }),
    );
  }

  markNotificationsAsRead() {
    return this.http.put(`${environment.backendUrl}notifications/mark-all-as-read`, {});
  }

  clearNotificationMessages() {
    return this.http.delete(`${environment.backendUrl}notifications/clear-old-notifications`);
  }

  downloadBulkImportTemplate() {
    const options: { [property: string]: any } = { headers: new HttpHeaders({ 'Content-Type': `application/json` }) };
    options.headers.set(`Accept`, `application/vnd.openxmlformats-officedocument.spreadsheetml.sheet`);
    options.responseType = `blob` as `json`;
    return this.http.get<any>(`${environment.backendUrl}UserV1/download-bulk-import-template`, options).subscribe(
      (response: Blob) => {
        if (response === null || response === undefined)
          return of();
        saveAs(response, `BulkImportTemplate.xlsx`);
      });
  }
}