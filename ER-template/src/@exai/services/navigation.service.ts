import { Injectable } from '@angular/core';
import {
  NavigationDialog,
  NavigationDropdown, NavigationItem, NavigationLink,
  NavigationSubheading
} from '../interfaces/navigation-item.interface';
import { Subject } from 'rxjs';
import { Role } from 'src/app/shared/enums/role.enum';
import { RoleBasedItem } from '../interfaces/role-based-item.interface';
import { MatDialog } from '@angular/material/dialog';
import icLayers from '@iconify/icons-ic/twotone-layers';

@Injectable({
  providedIn: 'root'
})
export class NavigationService {

  private roleBasedItems: RoleBasedItem[] = [
    {
      type: `link`,
      label: `Dashboard`,
      route: `/dashboard`,
      icon: icLayers,
      roles: [
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8
      ],
      children: []
    },
    {
      type: `link`,
      label: `Forms`,
      route: `/forms/submitted-form`,
      icon: icLayers,
      roles: [
        8
      ],
      children: []
    },
    {
      type: `dropdown`,
      label: `Create Forms`,
      route: null,
      icon: icLayers,
      roles: [
        1,
        2,
        3,
        4,
        5,
        6,
        7
      ],
      children: [
        {
          type: `link`,
          label: `Config Forms`,
          route: `/forms/config-form`,
          icon: null,
          roles: [
            1,
            2,
            3,
            4,
            5,
            6,
            7
          ],
          children: []
        },
        {
          type: `link`,
          label: `Create Letter`,
          route: `/forms/config-form`,
          queryParams: '1',
          icon: null,
          roles: [
            1,
            4
          ],
          children: []
        },
        {
          type: `link`,
          label: `Create Table`,
          route: `/forms/config-form`,
          queryParams: '2',
          icon: null,
          roles: [
            1,
            4
          ],
          children: []
        },
        {
          type: `link`,
          label: `Create Card`,
          route: `/forms/config-form`,
          queryParams: '3',
          icon: null,
          roles: [
            1,
            4
          ],
          children: []
        },
        {
          type: `link`,
          label: `Published Forms`,
          route: `/forms/submitted-form`,
          icon: null,
          roles: [
            1,
            2,
            3,
            4,
            5,
            6,
            7
          ],
          children: []
        },
        {
          type: `link`,
          label: `Primary Keys`,
          route: `/primary-keys`,
          icon: icLayers,
          roles: [
            1,
            2,
            3,
            4
          ],
          children: []
        }
      ]
    },
    {
      type: `link`,
      label: `Reports`,
      route: `/reports`,
      icon: icLayers,
      roles: [
        1,
        2,
        3,
        4
      ],
      children: []
    },
    {
      type: `link`,
      label: `Division`,
      route: `/manage-dept/manage-division`,
      icon: icLayers,
      roles: [
        1,
        2,
        3,
        4,
        5,
        6,
        7
      ],
      children: []
    },
    {
      type: `link`,
      label: `Sub-Division`,
      route: `/manage-dept/manage-sub-division`,
      icon: icLayers,
      roles: [
        1,
        2,
        3,
        4,
        5,
        6,
        7
      ],
      children: []
    },
    {
      type: `link`,
      label: `Police-Station`,
      route: `/manage-dept/police-station`,
      icon: icLayers,
      roles: [
        1,
        2,
        3,
        4,
        5,
        6,
        7
      ],
      children: []
    },
    {
      type: `link`,
      label: `Manage-User`,
      route: `/manage-user/user`,
      icon: icLayers,
      roles: [
        1,
        2,
        3,
        4,
        5,
        6,
        7
      ],
      children: []
    }
  ];
  protected items: NavigationItem[] = [];

  private _openChangeSubject = new Subject<NavigationDropdown>();
  openChange$ = this._openChangeSubject.asObservable();

  constructor(
    private dialog: MatDialog
  ) { }

  triggerOpenChange(item: NavigationDropdown) {
    this._openChangeSubject.next(item);
  }

  isLink(item: NavigationItem): item is NavigationLink {
    return item.type === 'link';
  }

  isDropdown(item: NavigationItem): item is NavigationDropdown {
    return item.type === 'dropdown';
  }

  isSubheading(item: NavigationItem): item is NavigationSubheading {
    return item.type === 'subheading';
  }

  isDialog(item: NavigationItem): item is NavigationDialog {
    return item?.route === `/reports`;
  }

  getItems(): any[] {
    return this.items;
  }

  setRole(role: number) {
    console.log(role);
  }

  public setNavigationItems(role: Role): void {
    if (this.roleBasedItems?.length === 0)
      return;

    this.items.length = 0;
    this.items = [...this.getItemsForCurrentRole(this.roleBasedItems, role) as NavigationItem[]];
  }

  private getItemsForCurrentRole = (roleBasedItems: RoleBasedItem[], role: Role) => {
    return roleBasedItems.reduce(
      (items: RoleBasedItem[], item: RoleBasedItem) => {
        const newItem: RoleBasedItem = {
          type: item?.type,
          label: item?.label,
          route: item?.route,
          queryParams: item.queryParams,
          icon: item?.icon,
          roles: item?.roles,
          children: item?.children
        };
        if (item?.children?.length > 0)
          newItem.children = this.getItemsForCurrentRole(item?.children, role);
        if (item.roles?.includes(role))
          items.push(newItem);
        return items;
      }, []);
  };
}