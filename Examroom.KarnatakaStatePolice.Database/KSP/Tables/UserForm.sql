﻿CREATE TABLE [KSP].[UserForm]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY(1,1),
	[FormId] INT,
	[UserId] INT,
	[Name] VARCHAR(200),
	[FileName] VARCHAR(250),
	[SystemFileName] VARCHAR(500),
	[CreatedBy] INT NOT NULL,
	[CreatedDatetime] DATETIME NOT NULL,
	[ModifiedBy] INT NOT NULL,
	[ModifiedDatetime] DATETIME NOT NULL
)
