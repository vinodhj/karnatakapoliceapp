﻿
CREATE TABLE [KSP].[ErrorLog](
	[Id]			[int] IDENTITY(1,1) NOT NULL PRIMARY KEY, 
	[PageURL]		[nvarchar](2000) NULL,
	[MethodName]	[nvarchar](2000) NULL, 
	[Module]		[nvarchar](50) NULL, 
	[ErrorMessage]  [nvarchar](max) NULL, 
	[IsActive]        BIT            DEFAULT ((1)) NOT NULL,
    [IsDeleted]       BIT            DEFAULT ((0)) NOT NULL,
    [CreatedBy]       INT ,
    [CreatedDatetime] SMALLDATETIME  DEFAULT (getutcdate()) NOT NULL,
    [ModifiedBy]	  INT ,
    [ModifiedDatetime]   SMALLDATETIME   
)
