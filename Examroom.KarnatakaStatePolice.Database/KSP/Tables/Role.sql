﻿CREATE TABLE [KSP].[Role] (
    [Id]              INT            IDENTITY (1, 1) NOT NULL,
    [Name]            NVARCHAR (64) NOT NULL,
    [Description]     NVARCHAR (64) NOT NULL,
    [IsActive]        BIT            DEFAULT ((1)) NOT NULL,
    [IsDeleted]       BIT            DEFAULT ((1)) NOT NULL,
    [CreatedBy]       INT    NOT NULL,
    [CreatedDatetime] SMALLDATETIME  DEFAULT (getutcdate()) NOT NULL,
    [ModifiedBy]   INT    NOT NULL,
    [ModifiedDatetime]   SMALLDATETIME  DEFAULT (getutcdate()) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    FOREIGN KEY ([CreatedBy]) REFERENCES [KSP].[User] ([Id]),
    FOREIGN KEY ([ModifiedBy]) REFERENCES [KSP].[User] ([Id])
);

