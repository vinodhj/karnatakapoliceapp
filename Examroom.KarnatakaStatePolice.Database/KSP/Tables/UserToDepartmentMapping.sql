﻿CREATE TABLE [KSP].[UserToDepartmentMapping]
(
	[Id] INT NOT NULL Identity(1,1) PRIMARY KEY, 
    [UserId] INT NOT NULL, 
    [PoliceStationId] INT NOT NULL, 
    [IsActive] BIT NOT NULL,
    [CreatedBy] INT NOT NULL,
	[CreatedDatetime] DATETIME NOT NULL,
	[ModifiedBy] INT NOT NULL,
	[ModifiedDatetime] DATETIME NOT NULL,
	FOREIGN KEY ([PoliceStationId]) REFERENCES [KSP].[DepartmentMapping] (Id),
	FOREIGN KEY ([UserId]) REFERENCES [KSP].[User] (Id)
)
