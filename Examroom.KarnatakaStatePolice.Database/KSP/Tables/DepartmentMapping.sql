﻿CREATE TABLE [KSP].[DepartmentMapping]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY(1,1),
	[Name] VARCHAR(50) NOT NULL,
	[Value] VARCHAR(250) NOT NULL,
	[LevelId] INT NULL,
	[ParentId] INT,
	[OrderNo] INT,
	[CreatedBy] INT NOT NULL,
	[CreatedDatetime] DATETIME NOT NULL,
	[ModifiedBy] INT NOT NULL,
	[ModifiedDatetime] DATETIME NOT NULL,
	[IsActive] BIT NOT NULL DEFAULT (0), 
    FOREIGN KEY ([ParentId]) REFERENCES [KSP].[DepartmentMapping] (Id)
)
