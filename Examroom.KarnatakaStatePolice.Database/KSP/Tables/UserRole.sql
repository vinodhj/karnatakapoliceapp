﻿CREATE TABLE [KSP].[UserRole] (
    [Id]              INT           IDENTITY (1, 1) NOT NULL,
    [UserId]          INT           NULL,
    [RoleId]          INT           NULL,
    [IsActive]       BIT           DEFAULT ((1)) NOT NULL,
    [IsAppRole]       BIT           DEFAULT ((1)) NOT NULL,
    [CreatedBy]       INT   NOT NULL,
    [CreatedDatetime] SMALLDATETIME DEFAULT (getutcdate()) NOT NULL,
    [ModifiedBy]   INT   NOT NULL,
    [ModifiedDatetime]   SMALLDATETIME DEFAULT (getutcdate()) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    FOREIGN KEY ([UserId]) REFERENCES [KSP].[User] ([Id]),
    FOREIGN KEY ([RoleId]) REFERENCES [KSP].[Role] ([Id]),
    FOREIGN KEY ([CreatedBy]) REFERENCES [KSP].[User] ([Id]),
    FOREIGN KEY ([ModifiedBy]) REFERENCES [KSP].[User] ([Id]),
    CONSTRAINT [User_Role] UNIQUE NONCLUSTERED
    (
      [UserId], [RoleId]
    )
);

