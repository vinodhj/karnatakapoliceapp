﻿CREATE TABLE [KSP].[User]
(
	[Id]                    INT     IDENTITY (1, 1) NOT NULL,
    [Firstname]             NVARCHAR (64) NOT NULL,
    [Lastname]              NVARCHAR (64) NOT NULL,
    [Username]              NVARCHAR (64) NOT NULL,
    [Pseudoname]            NVARCHAR (64) NULL,
    [EmailId]               NVARCHAR (128) NOT NULL,
    [PasswordHash]          VARBINARY (256) NOT NULL,
    [PasswordSalt]          VARBINARY (256) NOT NULL,
    [PasswordResetRequired] BIT            DEFAULT ((1)) NOT NULL,
    [Phone]                 VARCHAR (16)   NOT NULL,
    [ImageUrl]              NVARCHAR (MAX) NULL,
    [IsActive]              BIT            DEFAULT ((1)) NOT NULL,
    [IsDeleted]             BIT            DEFAULT ((0)) NOT NULL,
    [CreatedBy]             INT    NULL,
    [CreatedDatetime]       SMALLDATETIME  DEFAULT (getutcdate()) NOT NULL,
    [ModifiedBy]         INT    NULL,
    [ModifiedDatetime]         SMALLDATETIME  DEFAULT (getutcdate()) NOT NULL,
    [CountryCode] NVARCHAR(50) NULL, 
    PRIMARY KEY CLUSTERED ([Id] ASC),
    FOREIGN KEY ([CreatedBy]) REFERENCES [KSP].[User] ([Id]),
    FOREIGN KEY ([ModifiedBy]) REFERENCES [KSP].[User] ([Id]),
    INDEX [Pseudoname] NONCLUSTERED
    (
      [Pseudoname] asc
    ),
    INDEX [Firstname_Lastname] NONCLUSTERED
    (
      [Firstname] asc, [Lastname] asc
    )
)
