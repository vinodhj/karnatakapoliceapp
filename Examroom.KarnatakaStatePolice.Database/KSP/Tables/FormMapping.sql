﻿CREATE TABLE [KSP].[FormMapping]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY(1,1),
	[FormId] INT,
	[DepartmentId] INT,
	[IsActive] INT,
	[CreatedBy] INT NOT NULL,
	[CreatedDatetime] DATETIME NOT NULL,
	[ModifiedBy] INT NOT NULL,
	[ModifiedDatetime] DATETIME NOT NULL,
	FOREIGN KEY ([FormId]) REFERENCES Ksp.FormMaster(Id),
	FOREIGN KEY ([DepartmentId]) REFERENCES ksp.DepartmentMapping(Id),
)
