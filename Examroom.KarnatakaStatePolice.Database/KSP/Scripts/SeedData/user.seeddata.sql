﻿--
-- BEGIN SEED DATA SECTION
SET NOCOUNT ON;
PRINT 'SEEDING [dbo].[User] TABLE.';
DECLARE  @ROW_COUNT INT = 0;
DECLARE @SeedData TABLE (
    [Id]                    INT           NOT NULL,
    [Firstname]             NVARCHAR (64) NOT NULL,
    [Lastname]              NVARCHAR (64) NOT NULL,
    [Username]              NVARCHAR (64) NOT NULL,
    [Pseudoname]            NVARCHAR (64) NULL,
    [EmailId]               NVARCHAR (128) NOT NULL,
    [PasswordHash]          VARBINARY (256) NOT NULL,
    [PasswordSalt]          VARBINARY (256) NOT NULL,
    [PasswordResetRequired] BIT            DEFAULT ((1)) NOT NULL,
    [CountryCode]           VARCHAR (6)    NOT NULL,
    [Phone]                 VARCHAR (16)   NOT NULL,
    [ImageUrl]              NVARCHAR (MAX) NULL,
    [IsActive]              BIT            DEFAULT ((1)) NOT NULL,
    [IsDeleted]             BIT            DEFAULT ((0)) NOT NULL,
    [CreatedBy]             INT    NULL,
    [CreatedDatetime]       SMALLDATETIME  DEFAULT (getutcdate()) NOT NULL,
    [LastUpdatedBy]         INT    NULL,
    [LastUpdatedOn]         SMALLDATETIME  DEFAULT (getutcdate()) NOT NULL
);

INSERT INTO @SeedData([Id],[Firstname],[Lastname],[Username],[PseudoName],[EmailId],[PasswordHash],[PasswordSalt],[PasswordResetRequired],[CountryCode],[Phone],[IsActive],[IsDeleted],[CreatedBy],[CreatedDatetime],[LastUpdatedBy],[LastUpdatedOn]) VALUES(1, 'System', 'Administrator', 'SystemAdministrator', 'SystemAdministrator','', CONVERT(VARBINARY(MAX), '0xE1E43452668AD0219EC98EB7DA64A31B01B4EEEA78C660F7F267EA4614D2C248B71A6331E3C379F71ACD822C5CE6C488159A419E8375471ED8928D98DD06250F', 1), CONVERT(VARBINARY(MAX), '0x1C370F2466DAA1D1E0E5F2783C076219F9843B723D2F9A3DE3C33A6EF8EBE092718F5AD664F8FACCF922AC3B2FECF6F0EE30FD8FDA9D239F4B3077CA0AD15BB7CCFF63006B7452D3F31A6766F1BADAAF96D4E1B4622F2DD62260DB509EACCE735A876D96F85E2879DC5F4BF2607B74DD4A8400D8D978BD263BDC2EC116B13297', 1), 0, '', '', 0, 1, null, GETUTCDATE(), null, GETUTCDATE())
INSERT INTO @SeedData([Id],[Firstname],[Lastname],[Username],[PseudoName],[EmailId],[PasswordHash],[PasswordSalt],[PasswordResetRequired],[CountryCode],[Phone],[IsActive],[IsDeleted],[CreatedBy],[CreatedDatetime],[LastUpdatedBy],[LastUpdatedOn]) VALUES(2, 'Administrator', 'KSP', 'KSPAdministrator', 'KSPAdministrator','KSPAdministrator@gmail.com', CONVERT(VARBINARY(MAX), '0xE1E43452668AD0219EC98EB7DA64A31B01B4EEEA78C660F7F267EA4614D2C248B71A6331E3C379F71ACD822C5CE6C488159A419E8375471ED8928D98DD06250F', 1), CONVERT(VARBINARY(MAX), '0x1C370F2466DAA1D1E0E5F2783C076219F9843B723D2F9A3DE3C33A6EF8EBE092718F5AD664F8FACCF922AC3B2FECF6F0EE30FD8FDA9D239F4B3077CA0AD15BB7CCFF63006B7452D3F31A6766F1BADAAF96D4E1B4622F2DD62260DB509EACCE735A876D96F85E2879DC5F4BF2607B74DD4A8400D8D978BD263BDC2EC116B13297', 1), 0, '', '', 0, 1, 1, GETUTCDATE(), 1, GETUTCDATE())


-- END SEED DATA SECTION

-- INSET NEW RECORDS.
INSERT [dbo].[User] ([Id],[Firstname],[Lastname],[Username],[PseudoName],[EmailId],[PasswordHash],[PasswordSalt],[PasswordResetRequired],[CountryCode],[Phone],[IsActive],[IsDeleted],[CreatedBy],[CreatedDatetime],[LastUpdatedBy],[LastUpdatedOn])
SELECT seed.[Id],  seed.[Firstname], seed.[Lastname], seed.[Username], seed.[PseudoName], seed.[EmailId], seed.[PasswordHash], seed.[PasswordSalt], seed.[PasswordResetRequired], seed.[CountryCode], seed.[Phone], seed.[IsActive], seed.[IsDeleted], seed.[CreatedBy], seed.[CreatedDatetime], seed.[LastUpdatedBy], seed.[LastUpdatedOn]
FROM @SeedData seed
    LEFT OUTER JOIN [dbo].[User] U
        ON U.[Id] = seed.[Id]
WHERE U.[Id] IS NULL;

SELECT @ROW_COUNT = @@ROWCOUNT;
IF(@ROW_COUNT > 0)
PRINT 'SUCCESSFULLY INSERTED '+TRIM(STR(@ROW_COUNT))+' RECORDS IN [dbo].[User] TABLE.';
-- INSET NEW RECORDS.


-- UPDATE EXISTING RECOREDS IF ANY CHANGES.
SET @ROW_COUNT = 0;
UPDATE [dbo].[User]
SET [Username] = seed.[Username], [IsActive] = seed.[IsActive],
[LastUpdatedOn] = SYSDATETIMEOFFSET()
FROM [dbo].[User] U
INNER JOIN  @SeedData seed
ON seed.[Id] = U.[Id]
WHERE U.[Username] <> seed.[Username] or U.[IsActive] <> seed.[IsActive];

SELECT @ROW_COUNT = @@ROWCOUNT;
IF(@ROW_COUNT > 0)
PRINT 'SUCCESSFULLY UPDATED '+TRIM(STR(@ROW_COUNT))+' RECORDS IN [dbo].[User] TABLE.';
SET NOCOUNT OFF;
GO

