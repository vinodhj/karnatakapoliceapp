﻿PRINT 'SEEDING [KSP].[Role] TABLE.';
DECLARE  @ROW_COUNT INT = 0;
DECLARE @SeedData TABLE (
    [Id]                    INT           NOT NULL,
    [Name]                  NVARCHAR (128) NOT NULL,
    [Description]           NVARCHAR (128) NOT NULL, 
    [IsActive]              BIT            DEFAULT ((1)) NOT NULL,
    [IsDeleted]             BIT            DEFAULT ((0)) NOT NULL 
);

INSERT INTO @SeedData([Id],[Name],[Description]) VALUES(1,'SuperAdmin','Can add admins and forms')
INSERT INTO @SeedData([Id],[Name],[Description]) VALUES(2,'Country Head','Country Head')
INSERT INTO @SeedData([Id],[Name],[Description]) VALUES(3,'State Head','State Head')
INSERT INTO @SeedData([Id],[Name],[Description]) VALUES(4,'District Head','District Head')
INSERT INTO @SeedData([Id],[Name],[Description]) VALUES(5,'Division Head','District Head')
INSERT INTO @SeedData([Id],[Name],[Description]) VALUES(6,'Sub Division','Sub Division')
INSERT INTO @SeedData([Id],[Name],[Description]) VALUES(7,'Police Station','Police Station') 
INSERT INTO @SeedData([Id],[Name],[Description]) VALUES(8,'Police Users','Police Users') 

SET IDENTITY_INSERT  [KSP].[Role] ON
INSERT INTO  [KSP].[Role]([Id],[Name],[Description],CreatedBy,CreatedDatetime,ModifiedBy,ModifiedDatetime)
SELECT seed.[Id],seed.[Name],seed.[Description],1,SYSDATETIMEOFFSET(),1,SYSDATETIMEOFFSET()
FROM @SeedData seed
LEFT OUTER JOIN [KSP].[Role] U ON U.[Id] = seed.[Id]
WHERE U.[Id] IS NULL; 
SET IDENTITY_INSERT  [KSP].[Role] OFF;

SET @ROW_COUNT = 0;
UPDATE [KSP].[Role]
SET [Name] = seed.[Name],[Description]=seed.[Description],[IsActive] = seed.[IsActive],[ModifiedDatetime] = SYSDATETIMEOFFSET(),
[IsDeleted]=0
FROM [KSP].[Role] U
INNER JOIN  @SeedData seed ON seed.[Id] = U.[Id]
WHERE seed.[Id] = U.[Id]

SELECT @ROW_COUNT = @@ROWCOUNT;
IF(@ROW_COUNT > 0)
PRINT 'SUCCESSFULLY UPDATED '+TRIM(STR(@ROW_COUNT))+' RECORDS IN [KSP].[Role] TABLE.';
SET NOCOUNT OFF;
GO

