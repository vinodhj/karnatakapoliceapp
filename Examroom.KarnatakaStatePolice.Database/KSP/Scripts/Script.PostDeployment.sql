﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/
SET IDENTITY_INSERT  [KSP].[Role] ON
:r .\SeedData\Role.seeddata.sql
SET IDENTITY_INSERT  [KSP].[Role] OFF;

SET IDENTITY_INSERT [dbo].[User] ON
:r .\SeedData\user.seeddata.sql
SET IDENTITY_INSERT  [dbo].[User] OFF;