using Examroom.KarnatakaStatePolice.DataAccess;
using Examroom.KarnatakaStatePolice.Service.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using System;
using System.Text;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class Bootstrapper
    {
        public static void AddSwagger(this IServiceCollection services)
        {
            services.AddSwaggerGen(s =>
            {
                s.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "Karnataka State Police API",
                    Contact = new OpenApiContact
                    {
                        Name = "ksp.AI",
                        Url = new Uri("https://examroom.ai/")
                    }
                });

                s.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Description = "JWT Authorization header using the Bearer scheme (Example: Bearer ey56yhgdh8dyh)",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer"
                });

                s.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            }
                        },
                        Array.Empty<string>()
                    }
                });
            });
        }

        public static void AddApiAuthentication(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["TokenKey"])),
                        ValidateIssuer = false,
                        ValidateAudience = false
                    };
                });
        }

        public static void AddCorsPolicy(this IServiceCollection services)
        {
            services.AddCors(c =>
            {
                c.AddPolicy("MyPolicy", options => options
                   .SetIsOriginAllowed(origin => true)
                   .AllowAnyMethod()
                   .AllowAnyHeader()
                   .AllowCredentials());
            });
        }

        public static void AddApiControllers(this IServiceCollection services)
        {
            services.AddControllers().AddNewtonsoftJson(options =>
                options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);
        }

        public static void AddPoliceAppSetting(this IServiceCollection services, IConfiguration configuration)
        {
            IServiceCollection serviceCollection = services.AddDbContext<KspContext>(options =>
               options.UseSqlServer(configuration.GetConnectionString("DefaultConnection")));

            services.AddScoped<IAccountService, AccountService>();
            services.AddScoped<ITokenService, TokenService>();
            services.AddScoped<IDepartmentService, DepartmentService>();
            services.AddScoped<IUsersService, UsersService>();
            services.AddScoped<IFormService, FormService>(); 
            services.AddScoped<IErrorLogService, ErrorLogService>();  
            services.AddScoped<IEmailService, EmailService>();
            services.AddScoped<IPoliceStationService, PoliceStationService>();
            services.AddScoped<IDivisionService, DivisionService>();
            services.AddScoped<ISubDivisionService, SubDivisionService>(); 
        }
    }
}