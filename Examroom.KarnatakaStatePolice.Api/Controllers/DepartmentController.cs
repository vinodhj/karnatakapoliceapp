﻿using Examroom.KarnatakaStatePolice.DataAccess.Models;
using Examroom.KarnatakaStatePolice.Service.Dtos;
using Examroom.KarnatakaStatePolice.Service.Helpers;
using Examroom.KarnatakaStatePolice.Service.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examroom.KarnatakaStatePolice.Api.Controllers
{
    [Authorize]
    public class DepartmentController : BaseApiController
    {
        private readonly IDepartmentService _departmentService;
        public DepartmentController(IDepartmentService departmentService)
        {
            _departmentService = departmentService;
        }

        [HttpGet("getdepartmentdetails")]
        public async Task<IActionResult> GetDepartmentDetails(int levelId, string parentIds = null)
        {
            try
            {
                var result = await _departmentService.GetDepartmentDetails(UserId, RoleId, levelId, parentIds);
                return Ok(result);
            }
            catch (KspException ex)
            {
                return GetStatus(ex);
            }
            catch (Exception ex)
            {
                return GetStatus(ex);
            }
        }
        [HttpGet("deptmapping")]
        public async Task<IActionResult>GetDeptMapping(string name)
        {
            try
            {
                var result = await _departmentService.GetDeptMapping(name);
                return Ok(result);
            }
            catch (KspException ex)
            {
                return GetStatus(ex);
            }
            catch (Exception ex)
            {
                return GetStatus(ex);
            }
        }

        [HttpPost("savedepartment")]
        public async Task<IActionResult> SaveDepartment(RoleDto departmentMappingDto)
            {
            try
            {
                var result = await _departmentService.AddDepartmentDetails(UserId ,departmentMappingDto);
                return Ok(result);
            }
            catch (KspException ex)
            {
                return GetStatus(ex);
            }
            catch (Exception ex)
            {
                return GetStatus(ex);
            }
        }

        [HttpGet("getdepartmentdropdowndist")]
        public async Task<IActionResult> GetDepartmentDropDownList(int levelId)
        {
            try
            {
                var result = await _departmentService.GetDepartmentDropDownList(levelId);
                return Ok(result);
            }
            catch (KspException ex)
            {
                return GetStatus(ex);
            }
            catch (Exception ex)
            {
                return GetStatus(ex);
            }
        }

        /*
         
         Task<List<DepartmentMappingDto>> GetDivisions(string parentIds = null);
        Task<List<DepartmentMappingDto>> GetSubDivisions(string parentIds = null);
        Task<List<DepartmentMappingDto>> GetPoliceStations(string parentIds = null);
        */
        [HttpGet("divisions")]
        public async Task<IActionResult> GetDivisions(string parentIds = null)
        {
            try
            {
                var result = await _departmentService.GetDivisions(parentIds);
                return Ok(result);
            }
            catch (KspException ex)
            {
                return GetStatus(ex);
            }
            catch (Exception ex)
            {
                return GetStatus(ex);
            }
        }

        [HttpGet("subdivisions")]
        public async Task<IActionResult> GetSubDivisions(string parentIds = null)
        {
            try
            {
                var result = await _departmentService.GetSubDivisions(parentIds);
                return Ok(result);
            }
            catch (KspException ex)
            {
                return GetStatus(ex);
            }
            catch (Exception ex)
            {
                return GetStatus(ex);
            }
        }

        [HttpGet("policestations")]
        public async Task<IActionResult> GetPoliceStations(string parentIds = null)
        {
            try
            {
                var result = await _departmentService.GetPoliceStations(parentIds);
                return Ok(result);
            }
            catch (KspException ex)
            {
                return GetStatus(ex);
            }
            catch (Exception ex)
            {
                return GetStatus(ex);
            }
        }
    }
}
