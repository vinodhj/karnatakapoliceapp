﻿using Examroom.KarnatakaStatePolice.Service.Dtos;
using Examroom.KarnatakaStatePolice.Service.Helpers;
using Examroom.KarnatakaStatePolice.Service.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Examroom.KarnatakaStatePolice.Api.Controllers
{
    public class AccountController : BaseApiController
    {
        private readonly IAccountService _accountService;

        public AccountController(IAccountService accountService)
        {
            _accountService = accountService;
        }

        [HttpPost("register")]
        public async Task<IActionResult> Register(RegisterDto userDto)
        {
            try
            {
                var user = await _accountService.Register(userDto);
                return Ok(user);
            }
            catch (KspException ex)
            {
                return GetStatus(ex);
            }
            catch (Exception ex)
            {
                return GetStatus(ex);
            }
        }

        [HttpPost("login")]
        public async Task<IActionResult> Login(LoginDto loginDto)
        {
            try
            {
                var user = await _accountService.Login(loginDto);
                return Ok(user);
            }
            catch (KspException ex)
            {
                return GetStatus(ex);
            }
            catch (Exception ex)
            {
                return GetStatus(ex);
            }
        }

        [HttpPost("sendotp")]
        public async Task<IActionResult> SendOtp(string username)
        {
            try
            {
                var user = await _accountService.SendOtp(username);
                return Ok(user);
            }
            catch (KspException ex)
            {
                return GetStatus(ex);
            }
            catch (Exception ex)
            {
                return GetStatus(ex);
            }
        }

        [HttpPost("verifyotp")]
        public async Task<IActionResult> VerfiyOtp(VertifyOtpDto vertifyOtpDto)
        {
            try
            {
                var user = await _accountService.VerfiyOtp(vertifyOtpDto);
                return Ok(user);
            }
            catch (KspException ex)
            {
                return GetStatus(ex);
            }
            catch (Exception ex)
            {
                return GetStatus(ex);
            }

        }

        [HttpGet("roles")]
        public async Task<IActionResult> GetRoles()
        {
            try
            {
                List<RoleDto> roles = await _accountService.GetRoles();
                return Ok(roles);
            }
            catch (KspException ex)
            {
                return GetStatus(ex);
            }
            catch (Exception ex)
            {
                return GetStatus(ex);
            }
        }
    }
}
