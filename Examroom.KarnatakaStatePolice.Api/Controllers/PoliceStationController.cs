﻿using Examroom.KarnatakaStatePolice.Service.Dtos;
using Examroom.KarnatakaStatePolice.Service.Helpers;
using Examroom.KarnatakaStatePolice.Service.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examroom.KarnatakaStatePolice.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class PoliceStationController : BaseApiController
    {
        private readonly IPoliceStationService _policeStationService;

        public PoliceStationController(IPoliceStationService policeStationService)
        {
            _policeStationService = policeStationService;
        }

        [HttpPost("addpolicestation")]
        public async Task<IActionResult> AddPoliceStation([FromBody] CreatePoliceStationDto policeStationDto)
        {
            try
            {
                var result = await _policeStationService.AddPoliceStation(UserId, policeStationDto);
                return Ok(result);
            }
            catch (KspException ex)
            {
                return GetStatus(ex);
            }
            catch (Exception ex)
            {
                return GetStatus(ex);
            }
        }

        [HttpPut("modifypolicestation/{id}")]
        public async Task<IActionResult> ModifyPoliceStation(int id,[FromBody] UpdatePoliceStationDto policeStationDto)
        {
            try
            {
                var result = await _policeStationService.ModifyPoliceStation(UserId, id, policeStationDto);
                return Ok(result);
            }
            catch (KspException ex)
            {
                return GetStatus(ex);
            }
            catch (Exception ex)
            {
                return GetStatus(ex);
            }
        }
        [HttpPut("blockpolicestation/{id}")]
        public async Task<IActionResult> BlockPoliceStation(int id)
        {

            try
            {
                var result = await _policeStationService.BlockPoliceStation(UserId, id);
                return Ok(result);
            }
            catch (KspException ex)
            {
                return GetStatus(ex);
            }
            catch (Exception ex)
            {
                return GetStatus(ex);
            }
        }
        [HttpPut("unblockpolicestation/{id}")]
        public async Task<IActionResult> UnblockPoliceStation(int id)
        {
            try
            {
                var result = await _policeStationService.UnblockPoliceStation(UserId, id);
                return Ok(result);
            }
            catch (KspException ex)
            {
                return GetStatus(ex);
            }
            catch (Exception ex)
            {
                return GetStatus(ex);
            }
        }
        [HttpGet("showallpolicestations")]
        public async Task<IActionResult> GetAllPoliceStations()
        {
            try
            {
                var result = await _policeStationService.GetAllPoliceStations();
                return Ok(result);
            }
            catch (KspException ex)
            {
                return GetStatus(ex);
            }
            catch (Exception ex)
            {
                return GetStatus(ex);
            }
        }
        [HttpGet("showallpolicestationsdropdown")]
        public async Task<IActionResult> GetAllPoliceStationsDropdown()
        {
            try
            {
                var result = await _policeStationService.GetAllPoliceStationsDropdown();
                return Ok(result);
            }
            catch (KspException ex)
            {
                return GetStatus(ex);
            }
            catch (Exception ex)
            {
                return GetStatus(ex);
            }
        }
        [HttpGet("getallpolicestationsforparent/{parentId}")]
        public async Task<IActionResult> GetAllPoliceStationsForParent(int parentId)
        {
            try
            {
                var result = await _policeStationService.GetAllPoliceStationsForParent(parentId);
                return Ok(result);
            }
            catch (KspException ex)
            {
                return GetStatus(ex);
            }
            catch (Exception ex)
            {
                return GetStatus(ex);
            }
        }
        [HttpGet("getpolicestation/{id}")]
        public async Task<IActionResult> GetPoliceStation(int id)
        {
            try
            {
                var result = await _policeStationService.GetPoliceStation(id);
                return Ok(result);
            }
            catch (KspException ex)
            {
                return GetStatus(ex);
            }
            catch (Exception ex)
            {
                return GetStatus(ex);
            }
        }

    }
}
