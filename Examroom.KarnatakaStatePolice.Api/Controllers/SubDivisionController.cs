﻿using Examroom.KarnatakaStatePolice.Service.Dtos;
using Examroom.KarnatakaStatePolice.Service.Helpers;
using Examroom.KarnatakaStatePolice.Service.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examroom.KarnatakaStatePolice.Api.Controllers
{
    /// <summary>
    /// Defines the <see cref="SubDivisionController"/>.
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class SubDivisionController : BaseApiController
    {
        /// <summary>
        /// Defines the <see cref="ISubDivisionService"/>.
        /// </summary>
        private readonly ISubDivisionService _subDivisionService;
        /// <summary>
        /// Initializes a new instance of <see cref="SubDivisionController"/>.
        /// </summary>
        /// <param name="subDivisionService"></param>
        public SubDivisionController(ISubDivisionService subDivisionService)
        {
            _subDivisionService = subDivisionService;
        }
        /// <summary>
        /// The ShowAllSubDivisions.
        /// </summary>
        /// <returns></returns>
        [HttpGet("showallsubdivisions")]
        public async Task<IActionResult> ShowAllSubDivisions()
        {
            try
            {
                var result = await _subDivisionService.ShowAllSubDivisions();
                return Ok(result);
            }
            catch (KspException ex)
            {
                return GetStatus(ex);
            }
            catch (Exception ex)
            {
                return GetStatus(ex);
            }
        }
        /// <summary>
        /// The GetAllSubDivisionsDropdown.
        /// </summary>
        /// <returns></returns>
        [HttpGet("getallsubdivisionsdropdown")]
        public async Task<IActionResult> GetAllSubDivisionsDropdown()
        {
            try
            {
                var result = await _subDivisionService.GetAllSubDivisionsDropdown();
                return Ok(result);
            }
            catch (KspException ex)
            {
                return GetStatus(ex);
            }
            catch (Exception ex)
            {
                return GetStatus(ex);
            }
        }
        /// <summary>
        /// The GetSubDivisionById.
        /// </summary>
        /// <param name="subDivisionId"></param>
        /// <returns></returns>
        [HttpGet("getsubdivisionbyid")]
        public async Task<IActionResult> GetSubDivisionById(int subDivisionId)
        {
            try
            {
                var result = await _subDivisionService.GetSubDivisionById(subDivisionId);
                return Ok(result);
            }
            catch (KspException ex)
            {
                return GetStatus(ex);
            }
            catch (Exception ex)
            {
                return GetStatus(ex);
            }
        }
        /// <summary>
        /// The AddSubDivision.
        /// </summary>
        /// <param name="createSubDivision"></param>
        /// <returns></returns>
        [HttpPost("addsubdivision")]
        public async Task<IActionResult> AddSubDivision(CreateSubDivisionDto createSubDivision)
        {
            try
            {
                var result = await _subDivisionService.AddSubDivision(UserId, createSubDivision);
                return Ok(result);
            }
            catch (KspException ex)
            {
                return GetStatus(ex);
            }
            catch (Exception ex)
            {
                return GetStatus(ex);
            }
        }
        /// <summary>
        /// The ModifySubDivision.
        /// </summary>
        /// <param name="updateSubDivision"></param>
        /// <returns></returns>
        [HttpPut("modifysubdivision")]
        public async Task<IActionResult> ModifySubDivision(UpdateSubDivisionDto updateSubDivision)
        {
            try
            {
                var result = await _subDivisionService.ModifySubDivision(UserId, updateSubDivision);
                return Ok(result);
            }
            catch (KspException ex)
            {
                return GetStatus(ex);
            }
            catch (Exception ex)
            {
                return GetStatus(ex);
            }
        }
        /// <summary>
        /// The BlockSubDivision.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPut("blocksubdivision")]
        public async Task<IActionResult> BlockSubDivision(int id)
        {
            try
            {
                var result = await _subDivisionService.BlockSubDivision(UserId, id);
                return Ok(result);
            }
            catch (KspException ex)
            {
                return GetStatus(ex);
            }
            catch (Exception ex)
            {
                return GetStatus(ex);
            }
        }
        /// <summary>
        /// The UnblockSubDivision.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPut("unblocksubdivision")]
        public async Task<IActionResult> UnblockSubDivision(int id)
        {
            try
            {
                var result = await _subDivisionService.UnblockSubDivision(UserId, id);
                return Ok(result);
            }
            catch (KspException ex)
            {
                return GetStatus(ex);
            }
            catch (Exception ex)
            {
                return GetStatus(ex);
            }
        }
    }
}
