﻿using Examroom.KarnatakaStatePolice.Service.Dtos;
using Examroom.KarnatakaStatePolice.Service.Helpers;
using Examroom.KarnatakaStatePolice.Service.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examroom.KarnatakaStatePolice.Api.Controllers
{
    [Authorize]
    public class UserController : BaseApiController
    {
        private readonly IUsersService _usersService;
        public UserController(IUsersService usersService)
        {
            _usersService = usersService;
        }
        [HttpGet("list")]
        public async Task<IActionResult> GetUsers()
        {
            try
            {
                var result = await _usersService.GetUsers();
                return Ok(result);
            }
            catch (KspException ex)
            {
                return GetStatus(ex);
            }
            catch (Exception ex)
            {
                return GetStatus(ex);
            }
        }
        [HttpGet("view")]
        public async Task<IActionResult> GetUser(int id)
        {
            try
            {
                var result = await _usersService.GetUser(id);
                return Ok(result);
            }
            catch (KspException ex)
            {
                return GetStatus(ex);
            }
            catch (Exception ex)
            {
                return GetStatus(ex);
            }
        }

        [HttpPost("saveuser")]
        public async Task<IActionResult> SaveUser(AddUserDto userDto)
        {
            try
            {

                var result = await _usersService.SaveUser(UserId, userDto);
                return Ok(result);
            }
            catch (KspException ex)
            {
                return GetStatus(ex);
            }
            catch (Exception ex)
            {
                return GetStatus(ex);
            }
        }

        [HttpPost("setstatus")]
        public async Task<IActionResult> SetUserStatus(UpdateUserStatusDto updateUserStatusDto)
        {
            try
            {
                var result = await _usersService.SetUserStatus(UserId, updateUserStatusDto);
                return Ok(result);
            }
            catch (KspException ex)
            {
                return GetStatus(ex);
            }
            catch (Exception ex)
            {
                return GetStatus(ex);
            }
        }
        [AllowAnonymous]
        [HttpPost("resetpassowrd")]
        public async Task<IActionResult> ResetPassword(UpdatePasswordDto updatePasswordDto)
        {
            try
            {
                bool user = await _usersService.ResetPassword(UserId, updatePasswordDto);
                return Ok(user);
            }
            catch (KspException ex)
            {
                return GetStatus(ex);
            }
            catch (Exception ex)
            {
                return GetStatus(ex);
            }
        }

    }
}
