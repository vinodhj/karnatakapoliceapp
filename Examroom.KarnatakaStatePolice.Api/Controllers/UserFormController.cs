﻿//using Examroom.KarnatakaStatePolice.Service.Dtos;
//using Examroom.KarnatakaStatePolice.Service.Helpers;
//using Examroom.KarnatakaStatePolice.Service.Services;
//using Microsoft.AspNetCore.Authorization;
//using Microsoft.AspNetCore.Http;
//using Microsoft.AspNetCore.Mvc;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;

//namespace Examroom.KarnatakaStatePolice.Api.Controllers
//{
//    [Route("api/[controller]")]
//    [ApiController]
//    //[Authorize]
//    public class UserFormController : BaseApiController
//    {
//        private readonly IFormService _formService;
//        public UserFormController(IFormService formService)
//        {
//            _formService = formService;
//        }

//        #region Templates

//        [HttpGet("templates")]
//        public async Task<IActionResult> GetTemplates()
//        {
//            try
//            {
//                var result = await _formService
//                                .GetTemplates(UserId, RoleId);
//                return Ok(result);
//            }
//            catch (KspException kex)
//            {
//                return StatusCode(Convert.ToInt32(kex.StatusCode), kex.Message);
//            }
//            catch (Exception ex)
//            {
//                return StatusCode(500, ex.Message);
//            }
//        }

//        [HttpPost("addtemplate")]
//        public async Task<IActionResult> AddTemplate(IFormFile file, [FromForm] string name, [FromForm] string departmentIds, [FromForm] string content)
//        {
//            try
//            {
//                var result = await _formService.AddTemplate(UserId, RoleId, file, name, departmentIds, content);
//                return Ok(result);
//            }
//            catch (KspException kex)
//            {
//                return StatusCode(Convert.ToInt32(kex.StatusCode), kex.Message);
//            }
//            catch (Exception ex)
//            {
//                return StatusCode(500, ex.Message + " " + ex.InnerException?.Message);
//            }
//        }

//        [HttpGet("xltemplatecontent")]
//        public async Task<IActionResult> GetTemplateContent(int id)
//        {
//            try
//            {
//                var templateContent = await _formService.GetTemplateContent(id);
//                return Ok(templateContent);
//            }
//            catch (KspException kex)
//            {
//                return StatusCode(Convert.ToInt32(kex.StatusCode), kex.Message);
//            }
//            catch (Exception ex)
//            {
//                return StatusCode(500, ex.Message);
//            }
//        }

//        [HttpGet("xlformcontent")]
//        public async Task<IActionResult> GetFormContent(int id)
//        {
//            try
//            {
//                var formContent = await _formService.GetFormContent(id);
//                return Ok(formContent);
//            }
//            catch (KspException kex)
//            {
//                return StatusCode(Convert.ToInt32(kex.StatusCode), kex.Message);
//            }
//            catch (Exception ex)
//            {
//                return StatusCode(500, ex.Message);
//            }
//        }

//        [HttpGet("doctemplate")]
//        public async Task<IActionResult> DownloadTemplate(int id)
//        {
//            try
//            {
//                var downloadContent = await _formService.DownloadTemplate(id);
//                return File(downloadContent.FileInBytes, downloadContent.ContentType, downloadContent.FileName);
//            }
//            catch (KspException kex)
//            {
//                return StatusCode(Convert.ToInt32(kex.StatusCode), kex.Message);
//            }
//            catch (Exception ex)
//            {
//                return StatusCode(500, ex.Message);
//            }
//        }
//        [HttpPost("updatetemplate")]
//        public async Task<IActionResult> UpdateTemplate([FromForm] int templateId, [FromForm] string content)
//        {
//            try
//            {
//                var result = await _formService.UpdateTemplate(UserId, templateId, content);
//                return Ok(result);
//            }
//            catch (KspException kex)
//            {
//                return StatusCode(Convert.ToInt32(kex.StatusCode), kex.Message);
//            }
//            catch (Exception ex)
//            {
//                return StatusCode(500, ex.Message);
//            }
//        }

//        #endregion


//        #region Forms
//        [HttpGet("forms")]
//        public async Task<IActionResult> GetForms()
//        {
//            try
//            {
//                var forms = await _formService
//                                .GetForms(UserId, RoleId);

//                return Ok(forms);
//            }
//            catch (KspException kex)
//            {
//                return StatusCode(Convert.ToInt32(kex.StatusCode), kex.Message);
//            }
//            catch (Exception ex)
//            {
//                return StatusCode(500, ex.Message);
//            }
//        }
//        [HttpPost("addform")]
//        public async Task<IActionResult> AddForm([FromForm] int templateId, [FromForm] string name)
//        {
//            try
//            {
//                var result = await _formService.AddEmptyForm(UserId, RoleId, templateId, name);
//                return Ok(result);
//            }
//            catch (KspException kex)
//            {
//                return StatusCode(Convert.ToInt32(kex.StatusCode), kex.Message);
//            }
//            catch (Exception ex)
//            {
//                return StatusCode(500, ex.Message);
//            }
//        }
//        [HttpGet("docform")]
//        public async Task<IActionResult> GetDocForm(int id)
//        {
//            try
//            {
//                var downloadContent = await _formService.GetForm(id);
//                return File(downloadContent.FileInBytes, downloadContent.ContentType, downloadContent.FileName);
//            }
//            catch (KspException kex)
//            {
//                return StatusCode(Convert.ToInt32(kex.StatusCode), kex.Message);
//            }
//            catch (Exception ex)
//            {
//                return StatusCode(500, ex.Message);
//            }
//        }

//        [HttpPost("updateform")]
//        public async Task<IActionResult> UpdateForm([FromForm] int formId, IFormFile file, [FromForm] string content)
//        {
//            try
//            {
//                var result = await _formService.UpdateForm(UserId, formId, file, content);
//                return Ok(result);
//            }
//            catch (KspException kex)
//            {
//                return StatusCode(Convert.ToInt32(kex.StatusCode), kex.Message);
//            }
//            catch (Exception ex)
//            {
//                return StatusCode(500, ex.Message);
//            }
//        }

//        [HttpPost("updatedocform")]
//        public async Task<IActionResult> UpdateDocForm([FromForm] int formId, [FromForm] string content)
//        {
//            try
//            {
//                var result = await _formService.UpdateDocForm(UserId, formId,  content);
//                return Ok(result);
//            }
//            catch (KspException kex)
//            {
//                return StatusCode(Convert.ToInt32(kex.StatusCode), kex.Message);
//            }
//            catch (Exception ex)
//            {
//                return StatusCode(500, ex.Message);
//            }
//        }

//        [HttpPost("submitdocform")]
//        public async Task<IActionResult> SubmitDocForm([FromForm] int formId, [FromForm] string content)
//        {
//            try
//            {
//                var result = await _formService.SubmitDocForm(UserId, formId, content);
//                return Ok(result);
//            }
//            catch (KspException kex)
//            {
//                return StatusCode(Convert.ToInt32(kex.StatusCode), kex.Message);
//            }
//            catch (Exception ex)
//            {
//                return StatusCode(500, ex.Message);
//            }
//        }

//        [HttpGet("userinput") ]
//        public async Task<IActionResult> GetUserInput(int id)
//        {
//            try
//            {
//                var result = await _formService.GetUserInput(id);
//                return Ok(result);
//            }
//            catch (KspException kex)
//            {
//                return StatusCode(Convert.ToInt32(kex.StatusCode), kex.Message);
//            }
//            catch (Exception ex)
//            {
//                return StatusCode(500, ex.Message);
//            }
//        }
//        [HttpPost("submit")]
//        public async Task<IActionResult> SubmitForApproval(int id)
//        {
//            try
//            {
//                var result = await _formService.SubmitForApproval(UserId, id);
//                return Ok(result);
//            }
//            catch (KspException kex)
//            {
//                return StatusCode(Convert.ToInt32(kex.StatusCode), kex.Message);
//            }
//            catch (Exception ex)
//            {
//                return StatusCode(500, ex.Message);
//            }
//        }
//        [HttpPost("approve")]
//        public async Task<IActionResult> ApproveForm(int id)
//        {
//            try
//            {
//                var result = await _formService.ApproveForm(UserId, id);
//                return Ok(result);
//            }
//            catch (KspException kex)
//            {
//                return StatusCode(Convert.ToInt32(kex.StatusCode), kex.Message);
//            }
//            catch (Exception ex)
//            {
//                return StatusCode(500, ex.Message);
//            }
//        }

//        [HttpPost("reject")]
//        public async Task<IActionResult> RejectForm(int id)
//        {
//            try
//            {
//                var result = await _formService.RejectForm(UserId, id);
//                return Ok(result);
//            }
//            catch (KspException kex)
//            {
//                return StatusCode(Convert.ToInt32(kex.StatusCode), kex.Message);
//            }
//            catch (Exception ex)
//            {
//                return StatusCode(500, ex.Message);
//            }
//        }
//        #endregion


//        [HttpGet("downloadxltemplate")]
//        public async Task<IActionResult> DownloadExcelTemplate(int templateId)
//        {
//            try
//            {
//                DownloadContentDto downloadContent = await _formService.DownloadExcelTemplate(templateId);
//                return File(downloadContent.FileInBytes, "application/octet-stream", downloadContent.FileName);
//            }
//            catch (KspException kex)
//            {
//                return StatusCode(Convert.ToInt32(kex.StatusCode), kex.Message);
//            }
//            catch (Exception ex)
//            {
//                return StatusCode(500, ex.Message);
//            }
//        }

//        [HttpGet("downloadxlform")]
//        public async Task<IActionResult> DownloadExcelForm(int formId)
//        {
//            try
//            {
//                DownloadContentDto downloadContent = await _formService.DownloadExcelForm(formId);
//                return File(downloadContent.FileInBytes, "application/octet-stream", downloadContent.FileName);
//            }
//            catch (KspException kex)
//            {
//                return StatusCode(Convert.ToInt32(kex.StatusCode), kex.Message);
//            }
//            catch (Exception ex)
//            {
//                return StatusCode(500, ex.Message);
//            }
//        }

//        [HttpGet("formdetail")]
//        public async Task<IActionResult> GetFormDetails(int formId)
//        {
//            try
//            {
//                var result = await _formService.GetFormDetails(formId);
//                return Ok(result);
//            }
//            catch (KspException kex)
//            {
//                return StatusCode(Convert.ToInt32(kex.StatusCode), kex.Message);
//            }
//            catch (Exception ex)
//            {
//                return StatusCode(500, ex.Message);
//            }
//        }
//    }
//}
