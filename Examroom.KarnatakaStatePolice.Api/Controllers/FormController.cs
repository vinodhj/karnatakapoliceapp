﻿using Examroom.KarnatakaStatePolice.Service.Dtos;
using Examroom.KarnatakaStatePolice.Service.Helpers;
using Examroom.KarnatakaStatePolice.Service.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examroom.KarnatakaStatePolice.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class FormController : BaseApiController
    {
        private readonly IFormService _formService;

        public FormController(IFormService formService)
        {
            _formService = formService;
        }

        [HttpGet("list")]
        public async Task<IActionResult> GetForms()
        {
            try
            {
                var result = await _formService.GetFormsSummary(UserId, RoleId);
                return Ok(result);
            }
            catch(KspException kEx)
            {
                return GetStatus(kEx);
            }
            catch (Exception ex)
            {
                return GetStatus(ex);
            }
        }

        [HttpGet("detailedlist")]
        public async Task<IActionResult> GetDetailedList(int templateId, string policeStationIds= null)
        {
            try
            {
                var result = await _formService.GetUserFormDetails(UserId, RoleId, templateId, policeStationIds);
                return Ok(result);
            }
            catch (KspException kEx)
            {
                return GetStatus(kEx);
            }
            catch (Exception ex)
            {
                return GetStatus(ex);
            }
        }

        [HttpGet("policestations")]
        public async Task<IActionResult> GetPoliceStationsForTemplate(int templateId)
        {
            try
            {
                List<RoleDto> result = await _formService.GetPoliceStationsForForm(UserId, RoleId, templateId);
                return Ok(result);
            }
            catch (KspException kEx)
            {
                return GetStatus(kEx);
            }
            catch (Exception ex)
            {
                return GetStatus(ex);
            }
        }

        [HttpGet("view")]
        public async Task<IActionResult> GetForm(int id)
        {
            try
            {
                var result = await _formService.GetFormContent(id);
                return Ok(result);
            }
            catch (KspException kEx)
            {
                return GetStatus(kEx);
            }
            catch (Exception ex)
            {
                return GetStatus(ex);
            }
        }

        [HttpPost("addnew")]
        public async Task<IActionResult> AddNewUserForm(AddNewUserFormDto addNewUserFormDto)
        {
            try
            {
                var result = await _formService.AddNewUserForm(UserId, RoleId, addNewUserFormDto);
                return Ok(result);
            }
            catch (KspException kEx)
            {
                return GetStatus(kEx);
            }
            catch (Exception ex)
            {
                return GetStatus(ex);
            }
        }
        [HttpPost("update")]
        public async Task<IActionResult> UpdateFormContent(UpdateFormContentDto formContentDto)
        {
            try
            {
                bool result = await _formService.UpdateFormContent(UserId, RoleId, formContentDto);
                return Ok(result);
            }
            catch (KspException kEx)
            {
                return GetStatus(kEx);
            }
            catch (Exception ex)
            {
                return GetStatus(ex);
            }
        }

        [HttpPost("viewcomplete")]
        public async Task<IActionResult>  GetCompleteForm(int userFormId)
        {
            try
            {
                var result = await _formService.GetCompleteFormDetails(UserId,  userFormId);
                return Ok(result);
            }
            catch (KspException kEx)
            {
                return GetStatus(kEx);
            }
            catch (Exception ex)
            {
                return GetStatus(ex);
            }
        }

        [HttpPost("action")]
        public async Task<IActionResult> ActionOnForm(ActionOnFromDto actionDto)
        {
            try
            {
                var result = await _formService.ActionOnForm(UserId, actionDto);
                return Ok(result);
            }
            catch (KspException kEx)
            {
                return GetStatus(kEx);
            }
            catch (Exception ex)
            {
                return GetStatus(ex);
            }
        }


        [HttpGet("report/static")]
        public async Task<IActionResult> DownloadStaticFiles(int templateId, string formIds = null)
        {
            try
            {
                var downloadContent = await _formService.DownloadStaticFiles(UserId, RoleId, templateId, formIds);
                return File(downloadContent.FileInBytes, downloadContent.ContentType, downloadContent.FileName);
            }
            catch (KspException ex)
            {
                return GetStatus(ex);
            }
            catch (Exception ex)
            {
                return GetStatus(ex);
            }
        }
        [AllowAnonymous]
        [HttpGet("report/dynamic")]
        public async Task<IActionResult> DownloadUserResponse(int templateId)
        {
            try
            {
                var downloadContent = await _formService.DownloadDynamicFiles(UserId, RoleId, templateId);
                return File(downloadContent.FileInBytes, downloadContent.ContentType, downloadContent.FileName);
            }
            catch (KspException ex)
            {
                return GetStatus(ex);
            }
            catch (Exception ex)
            {
                return GetStatus(ex);
            }
        }
    }
}
