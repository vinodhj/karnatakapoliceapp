﻿using Examroom.KarnatakaStatePolice.Service.Helpers;
using Examroom.KarnatakaStatePolice.Service.Models;
using Examroom.KarnatakaStatePolice.Service.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examroom.KarnatakaStatePolice.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class DashboardController : BaseApiController
    {
        private IFormService _formService;
        private IDepartmentService _departmentService;

        public DashboardController(IFormService formService, IDepartmentService departmentService)
        {
            _formService = formService;
            _departmentService = departmentService;
        }

        [HttpGet("forms")]
        public async Task<IActionResult> GetFormsSummary()
        {
            try
            {
                var result = await _formService
                                .GetFormsSummaryDashboard(UserId, RoleId);
                return Ok(result);
            }
            catch(KspException kspEx)
            {
                return GetStatus(kspEx);
            }
            catch (Exception ex)
            {
                return GetStatus(ex);
            }
        }

        [HttpGet("departments")]
        public async Task<IActionResult> GetDepartmentsSummary()
        {
            try
            {
                var result = await _departmentService
                                .GetDepartmentSummary(UserId, RoleId);
                return Ok(result);
            }
            catch (KspException kspEx)
            {
                return GetStatus(kspEx);
            }
            catch (Exception ex)
            {
                return GetStatus(ex);
            }
        }

    }
}
