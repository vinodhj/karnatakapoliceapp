﻿using Examroom.KarnatakaStatePolice.Service.Dtos;
using Examroom.KarnatakaStatePolice.Service.Helpers;
using Examroom.KarnatakaStatePolice.Service.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examroom.KarnatakaStatePolice.Api.Controllers
{
    //[Authorize]
    public class TemplateController : BaseApiController
    {
        private readonly IFormService _formService;
        public TemplateController(IFormService formService)
        {
            _formService = formService;
        }

        [HttpGet("list")]
        public async Task<IActionResult> GetTemplates()
        {
            try
            {
                var result = await _formService.GetTemplates(UserId, RoleId);
                return Ok(result);
            }
            catch (KspException ex)
            {
                return GetStatus(ex);
            }
            catch (Exception ex)
            {
                return GetStatus(ex);
            }
        }

        [HttpPut("addnew")]
        public async Task<IActionResult> AddTemplate(AddTemplateDto addTempateDto)
        {
            try
            {
                int result = await _formService.AddTemplate(UserId, RoleId, addTempateDto);
                return Ok(result);
            }
            catch (KspException ex)
            {
                return GetStatus(ex);
            }
            catch (Exception ex)
            {
                return GetStatus(ex);
            }
        }


        [HttpPost("updatecontent")]
        public async Task<IActionResult> UpdateTemplate(UpdateTemplateContentDto updateTemplateContentDto)
        {
            try
            {
                var result = await _formService.UpdateTemplateContent(UserId, RoleId, updateTemplateContentDto);
                return Ok(result);
            }
            catch (KspException ex)
            {
                return GetStatus(ex);
            }
            catch (Exception ex)
            {
                return GetStatus(ex);
            }
        }

        [HttpGet("content")]
        public async Task<IActionResult> GetTemplateContent(int id)
        {
            try
            {
                var result = await _formService.GetTemplateContent(id);
                return Ok(result);
            }
            catch (KspException ex)
            {
                return GetStatus(ex);
            }
            catch (Exception ex)
            {
                return GetStatus(ex);
            }
        }

        [HttpPost("publish")]
        public async Task<IActionResult> SetTemplateStatus(SetTemplateStatusDto statusDto)
        {
            try
            {
                var result = await _formService.SetTemplateStatus(UserId, RoleId, statusDto);
                return Ok(result);
            }
            catch (KspException ex)
            {
                return GetStatus(ex);
            }
            catch (Exception ex)
            {
                return GetStatus(ex);
            }
        }

        [HttpPost("updatedepartment")]
        public async Task<IActionResult> UpdateDepartments(UpdateDetartmentDto updateDto)
        {
            try
            {
                bool result = await _formService.UpdateDepartment(UserId, RoleId, updateDto);
                return Ok(result);
            }
            catch (KspException ex)
            {
                return GetStatus(ex);
            }
            catch (Exception ex)
            {
                return GetStatus(ex);
            }
        }

    }
}
