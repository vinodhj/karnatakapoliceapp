﻿using Examroom.KarnatakaStatePolice.Service.Dtos;
using Examroom.KarnatakaStatePolice.Service.Helpers;
using Examroom.KarnatakaStatePolice.Service.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examroom.KarnatakaStatePolice.Api.Controllers
{

    public class ErrorLogController : BaseApiController
    {
        private readonly IErrorLogService _Service;
        private ILogger<ErrorLogController> _logger;
        public ErrorLogController(IErrorLogService errorLogService, ILogger<ErrorLogController> logger)
        {
            _Service = errorLogService;
            _logger = logger;
        }


        [HttpPost("createlog")]
        public async Task<ActionResult<CommonOutput>> CreateLog(ErrorLogDto errorLogDto)
        {
            try
            {
                _logger.LogInformation($"Log Input");
                _logger.LogInformation($"{JsonConvert.SerializeObject(errorLogDto)}");
                var result = await _Service.Create(errorLogDto);
                return Ok(result);
            }
            catch (KspException ex)
            {
                return StatusCode(Convert.ToInt32(ex.StatusCode), ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }
    }
}
