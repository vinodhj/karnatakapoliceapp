﻿using Examroom.KarnatakaStatePolice.Service.Dtos;
using Examroom.KarnatakaStatePolice.Service.Helpers;
using Examroom.KarnatakaStatePolice.Service.Models;
using Examroom.KarnatakaStatePolice.Service.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net;
using System.Threading.Tasks;

namespace Examroom.KarnatakaStatePolice.Api.Controllers
{
    /// <summary>
    /// Defines the <see cref="DivisionController" />.
    /// </summary>
    [Route("api/divisions")]
    [ApiController]
    [Authorize]
    public class DivisionController : BaseApiController
    {
        /// <summary>
        /// Defines the _divisionService.
        /// </summary>
        private readonly IDivisionService _divisionService;

        /// <summary>
        /// Initializes a new instance of the <see cref="DivisionController"/> class.
        /// </summary>
        /// <param name="divisionService">The divisionService<see cref="IDivisionService"/>.</param>
        public DivisionController(IDivisionService divisionService)
        {
            _divisionService = divisionService ?? throw new ArgumentNullException(nameof(divisionService));
        }

        /// <summary>
        /// The CreateDivision.
        /// </summary>
        /// <param name="createDivisionDto">The createDivisionDto<see cref="CreateDivisionDto"/>.</param>
        /// <returns>The <see cref="Task{ActionResult{RoleDto}}"/>.</returns>
        [HttpPost(Name = "CreateDivision")]
        public async Task<ActionResult<RoleDto>> CreateDivision(
            [FromBody] CreateDivisionDto createDivisionDto)
        {
            try
            {
                var newDivision = await _divisionService.CreateDivisionAsync(UserId, createDivisionDto);

                if (newDivision.Id is default(int))
                    throw new KspException("Something went wrong.", HttpStatusCode.BadRequest);

                return Ok(newDivision);
            }
            catch (KspException kspException)
            {
                return StatusCode(
                    Convert.ToInt32(kspException.StatusCode),
                    JsonConvert.SerializeObject(new ErrorMessageDto(kspException.Message))
                );
            }
            catch (Exception exception)
            {
                return StatusCode(
                    Convert.ToInt32(HttpStatusCode.InternalServerError),
                    JsonConvert.SerializeObject(new ErrorMessageDto(exception.Message))
                );
            }
        }

        /// <summary>
        /// The GetDivisions.
        /// </summary>
        /// <returns>The <see cref="Task{ActionResult{List{RoleDto}}}"/>.</returns>
        [HttpGet(Name = "GetDivisions")]
        public async Task<ActionResult<List<RoleDto>>> GetDivisions()
        {
            try
            {
                var divisions = await _divisionService.GetDivisionsAsync();

                return Ok(divisions);
            }
            catch (KspException kspException)
            {
                return StatusCode(
                    Convert.ToInt32(kspException.StatusCode), 
                    JsonConvert.SerializeObject(new ErrorMessageDto(kspException.Message))
                );
            }
            catch (Exception exception)
            {
                return StatusCode(
                    Convert.ToInt32(HttpStatusCode.InternalServerError), 
                    JsonConvert.SerializeObject(new ErrorMessageDto(exception.Message))
                );
            }
        }

        /// <summary>
        /// The GetDivisionsDropdown.
        /// </summary>
        /// <returns>The <see cref="Task{ActionResult{List{DepartmentMappingViewDto}}}"/>.</returns>
        [HttpGet("dropdown", Name = "GetDivisionsDropdown")]
        public async Task<ActionResult<List<DepartmentMappingViewDto>>> GetDivisionsDropdown()
        {
            try
            {
                var divisions = await _divisionService.GetDivisionsDropdownAsync();

                return Ok(divisions);
            }
            catch (KspException kspException)
            {
                return StatusCode(
                    Convert.ToInt32(kspException.StatusCode),
                    JsonConvert.SerializeObject(new ErrorMessageDto(kspException.Message))
                );
            }
            catch (Exception exception)
            {
                return StatusCode(
                    Convert.ToInt32(HttpStatusCode.InternalServerError),
                    JsonConvert.SerializeObject(new ErrorMessageDto(exception.Message))
                );
            }
        }

        /// <summary>
        /// The GetDivision.
        /// </summary>
        /// <param name="id">The id<see cref="int"/>.</param>
        /// <returns>The <see cref="Task{ActionResult{RoleDto}}"/>.</returns>
        [HttpGet("{id}", Name = "GetDivision")]
        public async Task<ActionResult<RoleDto>> GetDivision([FromRoute] int id)
        {
            try
            {
                var division = await _divisionService.GetDivisionAsync(id);

                return Ok(division);
            }
            catch (KspException kspException)
            {
                return StatusCode(
                    Convert.ToInt32(kspException.StatusCode),
                    JsonConvert.SerializeObject(new ErrorMessageDto(kspException.Message))
                );
            }
            catch (Exception exception)
            {
                return StatusCode(
                    Convert.ToInt32(HttpStatusCode.InternalServerError),
                    JsonConvert.SerializeObject(new ErrorMessageDto(exception.Message))
                );
            }
        }

        /// <summary>
        /// The UpdateDivision.
        /// </summary>
        /// <param name="updateDivisionDto">The updateDivisionDto<see cref="UpdateDivisionDto"/>.</param>
        /// <returns>The <see cref="Task{ActionResult{RoleDto}}"/>.</returns>
        [HttpPut("{id}", Name = "UpdateDivision")]
        public async Task<ActionResult<RoleDto>> UpdateDivision([FromRoute] int id, [FromBody] UpdateDivisionDto updateDivisionDto)
        {
            if (id != updateDivisionDto.Id)
                return BadRequest(JsonConvert.SerializeObject(new ErrorMessageDto("Ids are not matching.")));

            try
            {
                var division = await _divisionService.UpdateDivisionAsync(UserId, updateDivisionDto);

                return Ok(division);
            }
            catch (KspException kspException)
            {
                return StatusCode(
                    Convert.ToInt32(kspException.StatusCode),
                    JsonConvert.SerializeObject(new ErrorMessageDto(kspException.Message))
                );
            }
            catch (Exception exception)
            {
                return StatusCode(
                    Convert.ToInt32(HttpStatusCode.InternalServerError),
                    JsonConvert.SerializeObject(new ErrorMessageDto(exception.Message))
                );
            }
        }

        /// <summary>
        /// The SwitchBlockedStatus.
        /// </summary>
        /// <param name="id">The id<see cref="int"/>.</param>
        /// <param name="blocked">The blocked<see cref="bool"/>.</param>
        /// <returns>The <see cref="Task{ActionResult}"/>.</returns>
        [HttpPut("{id}/switch-blocked-status", Name = "SwitchBlockedStatus")]
        public async Task<ActionResult> SwitchBlockedStatus(
            [FromRoute][BindRequired] int id, [FromQuery][BindRequired, DefaultValue(false)] bool blocked = false)
        {
            try
            {
                await _divisionService.SwitchBlockedStatusAsync(UserId, id, blocked);

                return NoContent();
            }
            catch (KspException kspException)
            {
                return StatusCode(
                    Convert.ToInt32(kspException.StatusCode),
                    JsonConvert.SerializeObject(new ErrorMessageDto(kspException.Message))
                );
            }
            catch (Exception exception)
            {
                return StatusCode(
                    Convert.ToInt32(HttpStatusCode.InternalServerError),
                    JsonConvert.SerializeObject(new ErrorMessageDto(exception.Message))
                );
            }
        }
    }
}