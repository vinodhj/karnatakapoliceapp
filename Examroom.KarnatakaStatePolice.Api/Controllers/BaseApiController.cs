﻿using Examroom.KarnatakaStatePolice.Service.Helpers;
using Examroom.KarnatakaStatePolice.Service.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Examroom.KarnatakaStatePolice.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BaseApiController : Controller
    {
        protected int UserId;
        protected int RoleId;

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            base.OnActionExecuting(context);
            Get_ClaimDetails();
        }
        private void Get_ClaimDetails()
        {
            int.TryParse(this.User?.Claims?.FirstOrDefault(c => c.Type.Equals(ClaimTypes.NameIdentifier, StringComparison.OrdinalIgnoreCase))?.Value, out UserId);
            int.TryParse(this.User?.Claims?.FirstOrDefault(c => c.Type.Equals(ClaimTypes.Role, StringComparison.OrdinalIgnoreCase))?.Value, out RoleId);
            //UserId = 330;
            //RoleId = 2;
        }
        private string GetMessage(Exception ex)
        {
            var msg = ex.Message;

            if (ex?.InnerException?.Message != null)
                msg += $". {ex.InnerException.Message}";

            return JsonConvert.SerializeObject(new ErrorMessageDto(msg));
        }
        protected IActionResult GetStatus(KspException kspException)
        {
            return StatusCode(Convert.ToInt32(kspException.StatusCode), JsonConvert.SerializeObject(new ErrorMessageDto( kspException.Message)));
        }

        protected IActionResult GetStatus(Exception exception)
        {
            return StatusCode(Convert.ToInt32(HttpStatusCode.InternalServerError), GetMessage(exception));
        }
    }
}
