import { Component, OnInit ,Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { HttpService } from '../http.service';
import { FormControl } from '@angular/forms';
import { DataFilterService } from '../Core-Services/data-filter.service';
import { debounceTime } from 'rxjs/operators';
import { BehaviorSubject, Observable } from 'rxjs';
@Component({
  selector: 'app-user-detail-dialog',
  templateUrl: './user-detail-dialog.component.html',
  styleUrls: ['./user-detail-dialog.component.scss']
})
export class UserDetailDialogComponent implements OnInit {
  isAdmin:boolean=false;
  getDepartmentsService:any;
  getPoliceStationService:any;
  policeStation:any;
  locationDropdownSelection:any;
  getSubDivisionService:any;
  usersService:any;
  subdivsion:any;
  filteredSubdivisions:any;
  filterdStaions:any;
  searchTermService:any;
  subdivisionDropdownSelection:any;
  userNameinput:any;
  emailIdInput:any;
  firstNameInput:any;
  lastNameInput:any;
  phoneNumberInput:any;
  passwordInput:any;
  selectedPoliceStation:any;
  roleId:any;
  
  searchControl: FormControl = new FormControl('');
  deparments: any[] = [];
  usersData:any[]=[];
   users: any[] = [
    {value: 1, name: 'Super Admin'},
    {value: 2, name: 'Admin'}
  ];
  filteredDepartments: BehaviorSubject<Array<any>> = new BehaviorSubject<Array<any>>(
    []
  );
  $filteredDepartments: Observable<Array<any>> = this.filteredDepartments.asObservable();
  constructor(   public dialogRef: MatDialogRef<UserDetailDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any ,public httpService:HttpService,public dataFilter:DataFilterService) { }

  ngOnInit(): void {
    this.getsubDivisions();
    this.getDepartments();
    this.getpoliceStations()
    this.getUsers();
    this.searchTermService = this.searchControl.valueChanges
    .pipe(debounceTime(350))
    .subscribe((searchTerm: any) => {
      this.filteredDepartments.next(
        this.dataFilter.filterThings(this.deparments, searchTerm)
      );
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
  onRoleSelect(event:any){
    console.log(event);
    if(event==2){
      this.isAdmin=true;
      this.roleId=2;
    }
    else
    this.isAdmin=false;
    this.roleId=1
  }
  getDepartments(){
    this.deparments = [];
    this.getDepartmentsService = this.httpService.getDepartmentsByLevel(1).subscribe(
      (res) => {

        let data = Object.values(res);
        for (let i = 0; i < data.length; i++) {
          this.deparments.push(data[i]);
        }
        this.searchControl.setValue('');
      },
      (err: any) => {
        // let devMessage = `file:manage-panelists.component.ts,method:getPanelist,userId:${
        //   this.globalUserService.userId
        // };${JSON.stringify(err)}`;
        // this.httpService.logError(devMessage);
      }
    );
  }
  selectStation(event:any){
    this.selectedPoliceStation=event.id;
  }
  selectLocation(event:any){
    this.locationDropdownSelection=event.id;
    this.filterSubdivisionsonAdd(this.locationDropdownSelection);
  }
  filterSubdivisionsonAdd(dropdownSelection:any){
     this.filteredSubdivisions=this.subdivsion.filter((sd: { parentId: any; }) => sd.parentId == dropdownSelection);
  }

  selectSubDivision(event:any){
    this.subdivisionDropdownSelection=event.id;
    this.filterPoliceStationonAdd(this.subdivisionDropdownSelection);
  }
  filterPoliceStationonAdd(subdivisionDropdownSelection:any){
    this.filterdStaions=this.policeStation.filter((sd: { parentId: any; }) => sd.parentId == subdivisionDropdownSelection);
  }
  getsubDivisions(){
    this.subdivsion=[];
    this.deparments = [];
    this.getSubDivisionService = this.httpService.getDepartmentsByLevel(2).subscribe(
      (res) => {
        let data = Object.values(res);
        for (let i = 0; i < data.length; i++) {
          console.log("data ==>",data);

          this.subdivsion.push(data[i]);
        }
        this.searchControl.setValue('');
      },
      (err: any) => {
        // let devMessage = `file:manage-panelists.component.ts,method:getPanelist,userId:${
        //   this.globalUserService.userId
        // };${JSON.stringify(err)}`;
        // this.httpService.logError(devMessage);
      }
    );
  }
  getpoliceStations(){
    this.getPoliceStationService = this.httpService.getDepartmentsByLevel(3).subscribe(
      (res) => {
        let data = Object.values(res);
        this.policeStation = [];
        for (let i = 0; i < data.length; i++) {
          this.policeStation.push(data[i]);
        }
        this.searchControl.setValue('');
      },
      (err: any) => {
        // let devMessage = `file:manage-panelists.component.ts,method:getPanelist,userId:${
        //   this.globalUserService.userId
        // };${JSON.stringify(err)}`;
        // this.httpService.logError(devMessage);
      }
    );

  }
  onSubmitForm(){
    this.firstNameInput =(document.getElementById("firstname") as HTMLInputElement).value;
    this.lastNameInput =(document.getElementById("lastname") as HTMLInputElement).value;
    this.userNameinput =(document.getElementById("username") as HTMLInputElement).value;
    this.emailIdInput=(document.getElementById("emailId") as HTMLInputElement).value;
    this.passwordInput=(document.getElementById("password") as HTMLInputElement).value;
    this.phoneNumberInput=(document.getElementById("phone") as HTMLInputElement).value;
    const user = {
      id: 0,
      userRoleId: 0,
      roleId:this.roleId,
      username: this.userNameinput,
      firstname: this.firstNameInput,
      lastname:this.lastNameInput,
      password: this.passwordInput,
      emailId:this.emailIdInput,
      phone: this.phoneNumberInput,
      token: "string",
      policeStationId: this.selectedPoliceStation,
      // userRole: [
      //   {
      //     id: 0,
      //     userId: 0,
      //     roleId: this.roleId,
      //     isActive: true
      //   }
      //]
    };
    this.getDepartmentsService = this.httpService.postUsers(user).subscribe(
      (res) => {
        let data = Object.values(res);
        for (let i = 0; i < data.length; i++) {
          this.usersData.push(data[i]);
        }
        this.searchControl.setValue('');
        console.log(res);
        this.getUsers();
      },
      (err: any) => {

      }
    );
  }
  getUsers(){
    this.usersService = this.httpService.getUsers().subscribe(
      (res) => {
        let data = Object.values(res);
        for (let i = 0; i < data.length; i++) {
          console.log("data ==>",data);

          this.usersData.push(data[i]);
        }
        this.searchControl.setValue('');
      },
      (err: any) => {
      }
    );

  }
}
