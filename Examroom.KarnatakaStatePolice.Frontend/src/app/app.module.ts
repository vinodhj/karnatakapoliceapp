import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { IllustrationComponent } from './illustration/illustration.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxJdenticonModule, JDENTICON_CONFIG } from 'ngx-jdenticon';
import { NgsRevealModule } from 'ngx-scrollreveal';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { MatRippleModule } from '@angular/material/core';
import {MatButtonModule } from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatCardModule} from '@angular/material/card';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select'
import { MatInputModule } from '@angular/material/input';
import { MatExpansionModule } from '@angular/material/expansion';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpInterceptorService } from './http-interceptor.service';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { Guard } from './guard';
import { CreateTestComponent } from './create-test/create-test.component';
import {MatStepperModule } from '@angular/material/stepper';
import { DynamicTableComponent } from './dynamic-table/dynamic-table.component';
import {MatTableModule } from '@angular/material/table';
import { SuperAdminDashboardComponent } from './super-admin-dashboard/super-admin-dashboard.component';
import { ExcelComponent } from './excel/excel.component';
import { LocationComponent } from './location/location.component';
import { SubDivisionComponent } from './sub-division/sub-division.component';
import { PoliceStationComponent } from './police-station/police-station.component';
import { UserMappingComponent } from './user-mapping/user-mapping.component';
import { UserDetailDialogComponent } from './user-detail-dialog/user-detail-dialog.component';

// import SpreadsheetModule from 'x-data-spreadsheet';
@NgModule({
  declarations: [
    AppComponent,
    IllustrationComponent,
    LandingPageComponent,
    AdminDashboardComponent,
    CreateTestComponent,
    DynamicTableComponent,
    SuperAdminDashboardComponent,
    ExcelComponent,
    LocationComponent,
    SubDivisionComponent,
    PoliceStationComponent,
    UserMappingComponent,
    UserDetailDialogComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    NgxJdenticonModule,
    FormsModule,
    ReactiveFormsModule,
    NgsRevealModule,
    MatDialogModule,
    MatSidenavModule,
    MatSnackBarModule,
    MatRippleModule,
    MatButtonModule,
    MatIconModule,
    MatCardModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    HttpClientModule,
    MatExpansionModule,
    MatStepperModule,
    MatTableModule,
    MatFormFieldModule
    // SpreadsheetModule,
  ],
  exports: [ MatFormFieldModule, MatInputModule ],
  providers: [
    {
      provide: JDENTICON_CONFIG,
      useValue: {
        lightness: {
          color: [0.26, 0.53],
          grayscale: [0.71, 0.9],
        },
        saturation: {
          color: 0.99,
          grayscale: 0.42,
        },
        backColor: '#000000e8',
      },
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpInterceptorService,
      multi: true,
    },
    Guard,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
