import { Component, OnInit } from '@angular/core';
// import Spreadsheet from 'x-data-spreadsheet';
import x_spreadsheet from 'x-data-spreadsheet';
import { Options } from 'x-data-spreadsheet';

@Component({
  selector: 'app-excel',
  templateUrl: './excel.component.html',
  styleUrls: ['./excel.component.scss'],
})
export class ExcelComponent implements OnInit {
  workbook: any;

  constructor() {}

  ngOnInit(): void {}

  readExcel(event: any) {
    // this.workbook = new Excel.Workbook();
    // const target: DataTransfer = <DataTransfer>event.target;
    // if (target.files.length !== 1) {
    //   throw new Error('Cannot use multiple files');
    // }
    // const arryBuffer = new Response(target.files[0]).arrayBuffer();
    // let self = this;
    // // arryBuffer.then(function (data) {
    // //   self.workbook.xlsx.load(data).then(function () {
    // //     // play with this.workbook and worksheet now
    // //     console.log(self.workbook);
    // //     const worksheet = self.workbook.getWorksheet(1);
    // //     console.log('rowCount: ', worksheet.rowCount);
    // //     // worksheet.eachRow(function (row: any, rowNumber: number) {
    // //     //   // console.log(row, rowNumber);
    // //     // });
    // //   });
    // // });
    const rows10: any = { len: 1000 };
    for (let i = 0; i < 1000; i += 1) {
      rows10[i] = {
        cells: {
          0: { text: 'A-' + i },
          1: { text: 'B-' + i },
          2: { text: 'C-' + i },
          3: { text: 'D-' + i },
          4: { text: 'E-' + i },
          5: { text: 'F-' + i },
        },
      };
    }
    const rows = {
      len: 80,
      1: {
        cells: {
          0: { text: 'testingtesttestetst' },
          2: { text: 'testing' },
        },
      },
      2: {
        cells: {
          0: { text: 'render', style: 0 },
          1: { text: 'Hello' },
          2: { text: 'haha', merge: [1, 1] },
        },
      },
      8: {
        cells: {
          8: { text: 'border test', style: 0 },
        },
      },
    };
    const s = new x_spreadsheet('#x-spreadsheet-demo', <Options>{
      mode: 'edit',
      showToolbar: true,
      showGrid: true,
      showBottomBar: true,
    })
      .loadData([
        {
          freeze: 'B3',
          styles: [
            {
              bgcolor: '#f4f5f8',
              textwrap: true,
              color: '#900b09',
              border: {
                top: ['thin', '#0366d6'],
                bottom: ['thin', '#0366d6'],
                right: ['thin', '#0366d6'],
                left: ['thin', '#0366d6'],
              },
            },
          ],
          merges: ['C3:D4'],
          cols: {
            len: 10,
            2: { width: 200 },
          },
          rows,
        },
        { name: 'sheet-test', rows: rows10 },
      ])
      .change((cdata) => {
        console.log(cdata);
        // console.log('>>>', s.getData());
      });
  }
}
