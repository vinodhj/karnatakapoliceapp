import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { LocationComponent } from './location/location.component';
import {SubDivisionComponent} from './sub-division/sub-division.component'
import { CreateTestComponent } from './create-test/create-test.component';
import { PoliceStationComponent } from './police-station/police-station.component';
import { UserMappingComponent } from './user-mapping/user-mapping.component';
import { ExcelComponent } from './excel/excel.component';
import { Guard } from './guard';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { SuperAdminDashboardComponent } from './super-admin-dashboard/super-admin-dashboard.component';

const routes: Routes = [
  { path: '', component: LandingPageComponent },
  {
    path: 'adminDashboard',
    component: AdminDashboardComponent,
    canActivate: [Guard],
  },
  {
    path: 'location',
    component: LocationComponent,
    canActivate: [Guard],
  },
  {
    path: 'subdivision',
    component: SubDivisionComponent,
    canActivate: [Guard],
  },
  {
    path: 'policestation',
    component: PoliceStationComponent,
    canActivate: [Guard],
  },
  {
    path: 'usermapping',
    component: UserMappingComponent,
    canActivate: [Guard],
  },
  {
    path: 'createNewTest',
    component: CreateTestComponent,
    canActivate: [Guard],
  },
  {
    path: 'superAdminDashboard',
    component: SuperAdminDashboardComponent,
    canActivate: [Guard],
  },
  {
    path: 'excelPlayground',
    component: ExcelComponent,
    canActivate: [Guard],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
