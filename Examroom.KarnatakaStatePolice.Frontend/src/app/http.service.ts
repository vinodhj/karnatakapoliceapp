import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from 'src/environments/environment';
import {UserService} from './user.service';
@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private http: HttpClient,
    private userService: UserService) { }

  login(reqBody:any){
    return this.http.post(environment.backendUrl+"Account/login",reqBody);
  }
  register(reqBody:any){
    return this.http.post(environment.backendUrl+"Account/register",reqBody);
  }
  getDepartmentsByLevel(levelId:number){
    return this.http.get(environment.backendUrl + `Department/getdepartmentdetails?levelId=${levelId}`);
  }
  postUsers(user:any){
    return this.http.post(environment.backendUrl+ `User/saveuser`,user);
  }
  getUsers(){
    return this.http.get(environment.backendUrl + `User/getusers`);
  }
  createDepartmentsBylevel(user:any){
    return this.http.post(environment.backendUrl+ `Department/savedepartment`,user);
  }
  getAllTestsByAdmin(){
    return this.http.get(environment.backendUrl + `Tests/getAllTestsByAdmin?adminID=${this.userService.UserID}`);
  }
  createNewTest(reqBody:any){
    return this.http.post(environment.backendUrl+`Tests/createNewTest`,reqBody);
  }
  updateTest(reqBody:any){
    return this.http.post(environment.backendUrl+`Tests/updateTest`,reqBody);
  }
  createdummycandidatesinbulk(reqBody:any,testID:number,numDummyCandidates:number,minAbility:number,maxAbility:number){
    return this.http.post(
      environment.backendUrl +
      `Account/createdummycandidatesinbulk?TestId=${testID}&numDummyCandidates=${numDummyCandidates}&minAbility=${minAbility}&maxAbility=${maxAbility}`,reqBody
    );
  }
  generateQuestionItemSet(reqBody:any,testID:number, numQuestions:number){
    return this.http.post(
      environment.backendUrl +
        `Tests/generateQuestionItemSet?TestID=${testID}&numQuestions=${numQuestions}`,
      reqBody
    );
  }
  clearTestDataCompletely(testID:number){
    return this.http.delete(
      environment.backendUrl + `Tests/clearCompleteTestData?TestID=${testID}`
    );
  }
  simulateResponsesForCaliberation(testID:number){
    return this.http.post(
      environment.backendUrl +
        `Simulations/simulateResponsesForCaliberation?TestId=${testID}`,null
    );
  }
}
