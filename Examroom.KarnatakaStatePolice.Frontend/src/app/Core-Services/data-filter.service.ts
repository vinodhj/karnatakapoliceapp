import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class DataFilterService {
  constructor() {}
  //  here
  filterThings(things: Array<any>, searchTerm: string) {
    return things.filter((item: any) => {
      return JSON.stringify(item)
        .toLowerCase()
        .includes(searchTerm.toLowerCase());
    });
  }
}
