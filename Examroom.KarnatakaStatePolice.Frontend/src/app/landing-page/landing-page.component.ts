import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {Router} from '@angular/router';
import {UserService} from 'src/app/user.service';
import { HttpService } from '../http.service';
@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.scss'],
})
export class LandingPageComponent implements OnInit {
  loading: boolean = true;
  isShown:boolean=false;
  isNewUser = false;
  regLoginForm!: FormGroup;
  constructor(
    public userService: UserService, 
    public router: Router,
    private _formBuilder:FormBuilder,
    private http: HttpService) {
      this.regLoginForm = this._formBuilder.group({
        Username:['',Validators.required],
        Password:['',Validators.required]
      })
  }

  ngOnInit() {
    this.loading = true;
    this.initialize();
    this.setupHeaderBg();
  }
  ngAfterViewInit() {
    console.log('View Initialised!');
  }

  registerORlogin() {
    if(this.regLoginForm.valid){
      if(this.isNewUser){
        let reqBody = {
          username: this.regLoginForm.value.Username,
          password: this.regLoginForm.value.Password,
          roleId: 1,
          ability: 0,
        };
        this.http.register(reqBody).subscribe((response:any)=>{
          if(response){
            this.userService.setUserData(response);
          }
        });
      }
      else{
        let reqBody = {
          username: this.regLoginForm.value.Username,
          password: this.regLoginForm.value.Password,
        };
        this.http.login(reqBody).subscribe((response: any) => {
          if (response) {
            this.userService.setUserData(response);
          }
        });
      }
    }
  }

  initialize() {
    const doc = document.documentElement;
    doc.classList.remove('no-js');
    doc.classList.add('js');
  }
  setupHeaderBg() {
    const doc = document.documentElement;
    const win = window;
    const headerBg: HTMLElement = document.querySelector(
      '.site-header-large-bg span'
    ) as HTMLElement;

    function setHeaderBgHeight() {
      var bodyHeight = doc.getElementsByTagName('body')[0].clientHeight;
      if (headerBg) headerBg.style.height = `${bodyHeight}px`;
    }

    setHeaderBgHeight();
    win.addEventListener('load', setHeaderBgHeight);
    win.addEventListener('resize', setHeaderBgHeight);
  }
  redirect() {
    if(this.userService.isSuperAdmin)
    this.router.navigateByUrl('superAdminDashboard');
    else this.router.navigateByUrl('adminDashboard');
  }
}
