import { HttpService } from '../http.service';
import { Component, ComponentFactoryResolver, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { BehaviorSubject, Observable } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { DataFilterService } from '../Core-Services/data-filter.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
@Component({
  selector: 'app-sub-division',
  templateUrl: './sub-division.component.html',
  styleUrls: ['./sub-division.component.scss']
})
export class SubDivisionComponent implements OnInit {
dropOptions:any;
inputText:any;
input:any;
deparments: any[] = [];
subdivsion:any[]=[];
getDepartmentsService: any;
getSubDivisionService: any;
searchTermService: any;
parentId:any;
selectedValue:any;
departmentId=[];
searchControl: FormControl = new FormControl('');
locationList: any[] = [];
filteredDepartments: BehaviorSubject<Array<any>> = new BehaviorSubject<Array<any>>(
  []
);
$filteredDepartments: Observable<Array<any>> = this.filteredDepartments.asObservable();
  constructor(private httpService:HttpService, private dataFilter: DataFilterService) { }

  ngOnInit(): void {
    this.getSubDivisions();
    this.getDepartments();
    this.searchTermService = this.searchControl.valueChanges
    .pipe(debounceTime(350))
    .subscribe((searchTerm: any) => {
      this.filteredDepartments.next(
        this.dataFilter.filterThings(this.deparments, searchTerm)
      );
    });
  }

  getSubDivisions() {
    this.subdivsion = [];
    this.getSubDivisionService = this.httpService.getDepartmentsByLevel(2).subscribe(
      (res) => {
        let data = Object.values(res);
        for (let i = 0; i < data.length; i++) {
          console.log("data ==>",data);

          this.subdivsion.push(data[i]);


        }
        // for(let i=0;i<this.deparments.length;i++){
        //   this.locationList.push(this.deparments[i].name)
        // }

        //alert(JSON.stringify(this.subdivsion));
        // console.log(this.locationList);
        this.searchControl.setValue('');
      },
      (err: any) => {
        // let devMessage = `file:manage-panelists.component.ts,method:getPanelist,userId:${
        //   this.globalUserService.userId
        // };${JSON.stringify(err)}`;
        // this.httpService.logError(devMessage);
      }
    );
  }
  getDepartments() {
    this.deparments = [];
    this.getDepartmentsService = this.httpService.getDepartmentsByLevel(1).subscribe(
      (res) => {

       // 

        let data = Object.values(res);

        for (let i = 0; i < data.length; i++) {
          this.deparments.push(data[i]);
        }
        // for(let i=0;i<this.deparments.length;i++){
        //   this.locationList.push(this.deparments[i].name)
        // }
        // console.log(this.locationList);
        this.searchControl.setValue('');
        //alert(JSON.stringify(this.deparments));

      },
      (err: any) => {
        // let devMessage = `file:manage-panelists.component.ts,method:getPanelist,userId:${
        //   this.globalUserService.userId
        // };${JSON.stringify(err)}`;
        // this.httpService.logError(devMessage);
      }
    );

  }
  addNewField(){
      this.inputText =(document.getElementById("departmentNametop") as HTMLInputElement).value;
    this.deparments = [];
   
   const user = {
     id:0,
     name: "",
     value:this.inputText,
     levelId:2,
     parentId:this.parentId,
     orderNo:0,
     isActive: true
   };
   this.getDepartmentsService = this.httpService.createDepartmentsBylevel(user).subscribe(
     (res) => {
       let data = Object.values(res);
       for (let i = 0; i < data.length; i++) {
         this.deparments.push(data[i]);
       }
       this.searchControl.setValue('');
       console.log(res);
       this.getDepartments();
       this.getSubDivisions();
     },
     (err: any) => {
       // let devMessage = `file:manage-panelists.component.ts,method:getPanelist,userId:${
       //   this.globalUserService.userId
       // };${JSON.stringify(err)}`;
       // this.httpService.logError(devMessage);
     }
   );
 

  }
  selectLocation(event?:any){
    this.parentId=event.id;
    this.selectedValue=event.name
  }
  selectTableDropdown(event?:any){
    this.parentId=event.id
    this.selectedValue=event.name
  }
  onSave(subdivId:any,parentId:any,id:any){
    this.deparments = [];
    this.inputText =(document.getElementById("department_" + id) as HTMLInputElement).value;
   const user = {
     id:subdivId,
     name: "",
     value:this.inputText,
     levelId:2,
     parentId:parentId,
     orderNo:0,
     isActive: true
   };
   this.getDepartmentsService = this.httpService.createDepartmentsBylevel(user).subscribe(
     (res) => {
       let data = Object.values(res);
       for (let i = 0; i < data.length; i++) {
         this.deparments.push(data[i]);
       }
       this.searchControl.setValue('');
       console.log(res);
       this.getDepartments();
       this.getSubDivisions();
     },
     (err: any) => {
       // let devMessage = `file:manage-panelists.component.ts,method:getPanelist,userId:${
       //   this.globalUserService.userId
       // };${JSON.stringify(err)}`;
       // this.httpService.logError(devMessage);
     }
   );
 

  }
  onDelete(subdivId:any,parentId:any,id:any){
    this.deparments = [];
    this.inputText =(document.getElementById("department_" + id) as HTMLInputElement).value;
   const user = {
     id:subdivId,
     name: "",
     value:this.inputText,
     levelId:2,
     parentId:parentId,
     orderNo:0,
     isActive: false
   };
   this.getDepartmentsService = this.httpService.createDepartmentsBylevel(user).subscribe(
     (res) => {
       let data = Object.values(res);
       for (let i = 0; i < data.length; i++) {
         this.deparments.push(data[i]);
       }
       this.searchControl.setValue('');
       console.log(res);
       this.getDepartments();
       this.getSubDivisions();
     },
     (err: any) => {
       // let devMessage = `file:manage-panelists.component.ts,method:getPanelist,userId:${
       //   this.globalUserService.userId
       // };${JSON.stringify(err)}`;
       // this.httpService.logError(devMessage);
     }
   );
 

  }


}
