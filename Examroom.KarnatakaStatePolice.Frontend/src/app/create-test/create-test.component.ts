import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog,MatDialogConfig } from '@angular/material/dialog';
import { DynamicTableComponent } from '../dynamic-table/dynamic-table.component';
import {HttpService} from '../http.service';
import {UserService} from '../user.service';

@Component({
  selector: 'app-create-test',
  templateUrl: './create-test.component.html',
  styleUrls: ['./create-test.component.scss'],
})
export class CreateTestComponent implements OnInit {
  firstStepForm!: FormGroup;
  DummyCandidatesGenForm!: FormGroup;
  ItemSetGenForm!: FormGroup;

  stepsCompleted: Array<boolean> = [false, false,false,false,false];

  constructor(
    private _formBuilder: FormBuilder,
    private http: HttpService,
    private userService: UserService,
    private dialog:MatDialog
  ) {
    this.firstStepForm = this._formBuilder.group({
      testName: ['', Validators.required],
      baselineAbility: [
        0,
        [Validators.required, Validators.min(-3), Validators.max(3)],
      ],
      stoppingAbility: [
        2.9,
        [Validators.required, Validators.min(-3), Validators.max(3)],
      ],
    });
    this.DummyCandidatesGenForm = this._formBuilder.group({
      numCandidates: [
        5,
        [Validators.required, Validators.max(30), Validators.min(5)],
      ],
      minAbility: [
        -3,
        [Validators.required, Validators.min(-3), Validators.max(3)],
      ],
      maxAbility: [
        3,
        [Validators.required, Validators.min(-3), Validators.max(3)],
      ],
    });
    this.ItemSetGenForm = this._formBuilder.group({
      questionItemSetName: ['', Validators.required],
      numQuestions: [
        '',
        [Validators.required, Validators.max(350), Validators.min(5)],
      ],
    });
  }

  createdTestData: any = null;
  generatedItemSet: any = null;
  generatedCandidates: any = null;
  generatedResponses: any = null;
  caliberatedItemSet: any = null;

  createORupdateTest() {
    if (this.stepsCompleted[0] && this.firstStepForm.valid) {
      let reqBody = {
        testID: this.createdTestData.testID,
        testName: this.firstStepForm.value.testName,
        createdBy: this.userService.UserID,
        baselineAbility: this.firstStepForm.value.baselineAbility,
        stoppingAbility: this.firstStepForm.value.stoppingAbility,
      };
      this.http.updateTest(reqBody).subscribe((response: any) => {
        this.createdTestData = response;
      });
    } else if (this.firstStepForm.valid) {
      let reqBody = {
        testName: this.firstStepForm.value.testName,
        createdBy: this.userService.UserID,
        baselineAbility: this.firstStepForm.value.baselineAbility,
        stoppingAbility: this.firstStepForm.value.stoppingAbility,
      };
      this.http.createNewTest(reqBody).subscribe((response: any) => {
        this.stepsCompleted[0] = true;
        this.createdTestData = response;
      });
    }
  }
  generateItemSet() {
    if (this.ItemSetGenForm.valid && this.generatedItemSet == null) {
      let reqBody = {
        questionItemSetName: this.ItemSetGenForm.value.questionItemSetName,
      };
      this.http
        .generateQuestionItemSet(
          reqBody,
          this.createdTestData.testID,
          this.ItemSetGenForm.value.numQuestions
        )
        .subscribe((response: any) => {
          this.generatedItemSet = response;
          this.stepsCompleted[2] = true;
        });
    }
  }
  generateDummyCandidates() {
    if (this.DummyCandidatesGenForm.valid && this.generatedCandidates == null) {
      let reqBody = {
        userID: this.userService.UserID,
      };
      this.http
        .createdummycandidatesinbulk(
          reqBody,
          this.createdTestData.testID,
          this.DummyCandidatesGenForm.value.numCandidates,
          this.DummyCandidatesGenForm.value.minAbility,
          this.DummyCandidatesGenForm.value.maxAbility
        )
        .subscribe((response: any) => {
          this.generatedCandidates = response;
          this.stepsCompleted[1] = true;
        });
    }
  }
  generateAndSimulateResponses(){
    if(this.generatedResponses == null){
    this.http.simulateResponsesForCaliberation(this.createdTestData.testID).subscribe((response:any)=>{
      this.generatedResponses = response;
    });
    }
  }
  caliberateItemSet() {

  }

  openViewDialog(data:any){
    const dialogConfig = new MatDialogConfig();
      dialogConfig.hasBackdrop = true;
      dialogConfig.minWidth = '90vw';
      dialogConfig.minHeight = '90vh';
      dialogConfig.id = 'viewData';
      dialogConfig.data = data;
      const dialogRef = this.dialog.open(DynamicTableComponent, dialogConfig);
  }

  ngOnInit(): void {

  }
}
