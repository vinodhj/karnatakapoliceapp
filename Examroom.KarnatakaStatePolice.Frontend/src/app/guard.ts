import { Injectable } from '@angular/core';
import {
  CanLoad,
  Route,
  Router,
  UrlSegment,
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
} from '@angular/router';
import { Observable } from 'rxjs';
import { UserService } from './user.service';

@Injectable()
export class Guard implements  CanActivate {
  constructor(
    private router: Router,
    private userService: UserService
  ) {}

  canActivate( route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    let r = route.url.toString();
    if (
      this.userService.roleId == 0 &&
      (r === 'adminDashboard' || r === 'createNewTest')
    ) {
      return true;
    } else if (this.userService.roleId == 1 && r === 'candidate') {
      return true;
    } else {
      return true;
    }
  }
}
