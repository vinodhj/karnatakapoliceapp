import { ElementRef, ViewChild } from '@angular/core';
import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import * as XLSX from 'xlsx';
@Component({
  selector: 'app-dynamic-table',
  templateUrl: './dynamic-table.component.html',
  styleUrls: ['./dynamic-table.component.scss'],
})
export class DynamicTableComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<DynamicTableComponent>,
    @Inject(MAT_DIALOG_DATA) private data: any
  ) {}

  @ViewChild('Table', { static: true }) table!: ElementRef;

  displayedColumns!: string[];
  dataSource!: any[];

  ngOnInit() {
    // http request to get the data
    this.dataSource = this.data;
    this.displayedColumns = [];
    for (let key of Object.keys(this.data[0])) {
      if (this.data[0][key]) this.displayedColumns.push(key);
    }
  }
  exportAsExcel() {
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(
      this.table.nativeElement
    ); //converts a DOM TABLE element to a worksheet
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

    /* save to file */
    XLSX.writeFile(wb, 'GeneratedData.xlsx');
  }
  
}
