import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
// import { NgsRevealConfig } from 'ngx-scrollreveal';

@Injectable({
  providedIn: 'root',
})
export class UserService {

  UserID!:number;
  Username!:string;
  CreatedOn!:Date;
  roleId!:number;
  Ability!:number;
  Token:string = "";

  userLoggedIn = false;
  isSuperAdmin = false;
  constructor(
    public snackBar: MatSnackBar) {}

  setUserDetailsToAndFromLocalStorage(){
    if (!this.userLoggedIn && localStorage.getItem('userData')) {
      let userData = JSON.parse(String(localStorage.getItem('userData')));
      this.UserID = userData.UserID;
      this.Username = userData.Username;
      this.roleId = userData.roleId;
      this.CreatedOn = userData.CreatedOn;
      this.Token = userData.Token;
      this.Ability = userData.Ability;
      this.userLoggedIn = true;
      this.isSuperAdmin = this.roleId == 0;
    } else if (this.userLoggedIn) {
      let userData = {
        UserID: this.UserID,
        Username: this.Username,
        roleId: this.roleId,
        CreatedOn: this.CreatedOn,
        Token: this.Token,
        Ability: this.Ability,
      };
      localStorage.setItem('userData', JSON.stringify(userData));
    }
  }
  setUserData(userData:any){
    this.UserID = userData.userID;
    this.Username = userData.username;
    this.roleId = userData.roleId;
    this.CreatedOn = userData.createdOn;
    this.Token = userData.token;
    this.Ability = userData.ability;
    this.userLoggedIn = true;
    this.isSuperAdmin = this.roleId == 0;
    this.setUserDetailsToAndFromLocalStorage();
  }
}
