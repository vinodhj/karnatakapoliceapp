import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpService } from '../http.service';
import { UserService } from '../user.service';
@Component({
  selector: 'app-super-admin-dashboard',
  templateUrl: './super-admin-dashboard.component.html',
  styleUrls: ['./super-admin-dashboard.component.scss'],
})
export class SuperAdminDashboardComponent implements OnInit {
  constructor(
    private router: Router,
    private httpService: HttpService,
    private userService: UserService
  ) {}

  tests: Array<any> = [];
  ngOnInit(): void {
    this.userService.setUserDetailsToAndFromLocalStorage();
    this.getAllTests();
  }
  getAllTests() {
    this.httpService.getAllTestsByAdmin().subscribe((response: any) => {
      if (response) {
        this.tests = response;
        // this.tests = [...this.tests,this.tests[0],this.tests[0],this.tests[0]];
        console.log(this.tests);
      }
    });
  }
  deleteTest(test: any) {
    this.httpService
      .clearTestDataCompletely(test.testID)
      .subscribe((response: any) => {
        if (response) {
          setTimeout(() => {
            this.getAllTests();
          }, 2000);
        }
      });
  }

  createNewTest() {
    this.router.navigateByUrl('createNewTest');
  }
}
