import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {HttpService} from '../http.service';
import {UserService} from '../user.service';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { DataFilterService } from '../Core-Services/data-filter.service';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-location',
  templateUrl: './location.component.html',
  styleUrls: ['./location.component.scss']
})
export class LocationComponent implements OnInit {
  deparments: any[] = [];
  getDepartmentsService: any;
  searchTermService: any;


  constructor(
    private router: Router,
    private httpService: HttpService,
    private userService: UserService,
    private httpClient:HttpClient,
    private dataFilter: DataFilterService
  ) {}
  inputText:any;
  tests: Array<any> = [];
  ngOnInit(): void {
    // this.userService.setUserDetailsToAndFromLocalStorage();
    // this.getAllTests();
    this.getDepartments();
    this.searchTermService = this.searchControl.valueChanges
      .pipe(debounceTime(350))
      .subscribe((searchTerm: any) => {
        this.filteredDepartments.next(
          this.dataFilter.filterThings(this.deparments, searchTerm)
        );
      });
  }
  
  addDeparments() {
    this.deparments = [];
     this.inputText =(document.getElementById("departmentName") as HTMLInputElement).value;
    const user = {
      id:0,
      name: "",
      value:this.inputText,
      levelId:1,
      parentId:null,
      orderNo:0,
      isActive: true
    };
    this.getDepartmentsService = this.httpService.createDepartmentsBylevel(user).subscribe(
      (res) => {
        let data = Object.values(res);
        for (let i = 0; i < data.length; i++) {
          this.deparments.push(data[i]);
        }
        this.searchControl.setValue('');
        this.getDepartments();
      },
      (err: any) => {
        // let devMessage = `file:manage-panelists.component.ts,method:getPanelist,userId:${
        //   this.globalUserService.userId
        // };${JSON.stringify(err)}`;
        // this.httpService.logError(devMessage);
      }
    );
  
  }
  getDepartments() {
    this.deparments = [];
    this.getDepartmentsService = this.httpService.getDepartmentsByLevel(1).subscribe(
      (res) => {
        let data = Object.values(res);
        for (let i = 0; i < data.length; i++) {
          this.deparments.push(data[i]);
          console.log(this.deparments+"departments")
        }
        this.searchControl.setValue('');
      },
      (err: any) => {
        // let devMessage = `file:manage-panelists.component.ts,method:getPanelist,userId:${
        //   this.globalUserService.userId
        // };${JSON.stringify(err)}`;
        // this.httpService.logError(devMessage);
      }
    );
  }
  onClickDelete(idx:any){
    this.deparments = [];
    this.inputText =(document.getElementById("department_" + idx) as HTMLInputElement).value;
   const user = {
     id:idx,
     name: "",
     value:this.inputText,
     levelId:1,
     parentId:null,
     orderNo:0,
     isActive: false
   };
   this.getDepartmentsService = this.httpService.createDepartmentsBylevel(user).subscribe(
     (res) => {
       let data = Object.values(res);
       for (let i = 0; i < data.length; i++) {
         this.deparments.push(data[i]);
       }
       this.searchControl.setValue('');
       console.log(res);
       this.getDepartments();
     },
     (err: any) => {
       // let devMessage = `file:manage-panelists.component.ts,method:getPanelist,userId:${
       //   this.globalUserService.userId
       // };${JSON.stringify(err)}`;
       // this.httpService.logError(devMessage);
     }
   );

  }
  onClickSave(idx:any){
    this.deparments = [];
    this.inputText =(document.getElementById("department_" + idx) as HTMLInputElement).value;
   const user = {
     id:idx,
     name: "",
     value:this.inputText,
     levelId:1,
     parentId:null,
     orderNo:0,
     isActive: true
   };
   this.getDepartmentsService = this.httpService.createDepartmentsBylevel(user).subscribe(
     (res) => {
       let data = Object.values(res);
       for (let i = 0; i < data.length; i++) {
         this.deparments.push(data[i]);
       }
       this.searchControl.setValue('');
       console.log(res);
       this.getDepartments();
     },
     (err: any) => {
       // let devMessage = `file:manage-panelists.component.ts,method:getPanelist,userId:${
       //   this.globalUserService.userId
       // };${JSON.stringify(err)}`;
       // this.httpService.logError(devMessage);
     }
   );
      
  }
  searchControl: FormControl = new FormControl('');
  filteredDepartments: BehaviorSubject<Array<any>> = new BehaviorSubject<Array<any>>(
    []
  );
  $filteredDepartments: Observable<Array<any>> = this.filteredDepartments.asObservable();
  // getAllTests(){
  //   this.httpService.getAllTestsByAdmin().subscribe((response: any) => {
  //     if (response) {
  //       this.tests = response;
  //       // this.tests = [...this.tests,this.tests[0],this.tests[0],this.tests[0]];
  //       console.log(this.tests);
  //     }
  //   });
  // }
  // deleteTest(test:any){
  //   
  //   this.httpService.clearTestDataCompletely(test.testID).subscribe((response:any)=>{
  //     if(response){
  //       setTimeout(()=>{
  //         this.getAllTests();
  //       },2000);

  //     }
  //   })
  // }

  // createNewTest() {
  //   this.router.navigateByUrl('createNewTest');
  // }

  // getAllTests(){
  //   this.httpService.getAllTestsByAdmin().subscribe((response: any) => {
  //     if (response) {
  //       this.tests = response;
  //       // this.tests = [...this.tests,this.tests[0],this.tests[0],this.tests[0]];
  //       console.log(this.tests);
  //     }
  //   });
  // }
  // deleteTest(test:any){
  //   
  //   this.httpService.clearTestDataCompletely(test.testID).subscribe((response:any)=>{
  //     if(response){
  //       setTimeout(()=>{
  //         this.getAllTests();
  //       },2000);

  //     }
  //   })
  // }

  // createNewTest() {
  //   this.router.navigateByUrl('createNewTest');
  // }
}

