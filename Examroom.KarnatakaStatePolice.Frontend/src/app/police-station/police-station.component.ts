import { HttpService } from '../http.service';
import { Component, ComponentFactoryResolver, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { BehaviorSubject, Observable } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { DataFilterService } from '../Core-Services/data-filter.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-police-station',
  templateUrl: './police-station.component.html',
  styleUrls: ['./police-station.component.scss']
})
export class PoliceStationComponent implements OnInit {
  dropOptions:any;
inputText:any;
input:any;
deparments: any[] = [];
subdivsion:any[]=[];
policeStation:any[]=[];
getDepartmentsService: any;
getSubDivisionService: any;
getPoliceStationService:any;
searchTermService: any;
parentId:any;
locationDropdownSelection:any;
subdivisionDropdownSelection:any;
filteredSubdivisions:any
filteredGridSubdivisions:any
selectedValue:any;
departmentId=[];
searchControl: FormControl = new FormControl('');
locationList: any[] = [];
filteredDepartments: BehaviorSubject<Array<any>> = new BehaviorSubject<Array<any>>(
  []
);
$filteredDepartments: Observable<Array<any>> = this.filteredDepartments.asObservable();

  constructor(private httpService:HttpService, private dataFilter: DataFilterService) { }

  ngOnInit(): void {
    this.getSubDivisions();
    this.getDepartments();
    this.getPoliceStation()
    this.searchTermService = this.searchControl.valueChanges
    .pipe(debounceTime(350))
    .subscribe((searchTerm: any) => {
      this.filteredDepartments.next(
        this.dataFilter.filterThings(this.deparments, searchTerm)
      );
    });
  }
 
  getPoliceStation(){
    this.policeStation=[];
    this.getPoliceStationService = this.httpService.getDepartmentsByLevel(3).subscribe(
      (res) => {
        let data = Object.values(res);
        this.policeStation = [];
        for (let i = 0; i < data.length; i++) {
          this.policeStation.push(data[i]);


        }
        //alert(this.policeStation)
        this.searchControl.setValue('');
      },
      (err: any) => {
        // let devMessage = `file:manage-panelists.component.ts,method:getPanelist,userId:${
        //   this.globalUserService.userId
        // };${JSON.stringify(err)}`;
        // this.httpService.logError(devMessage);
      }
    );
  }
  getSubDivisions() {
    this.subdivsion = [];
    this.getSubDivisionService = this.httpService.getDepartmentsByLevel(2).subscribe(
      (res) => {
        let data = Object.values(res);
        for (let i = 0; i < data.length; i++) {
          console.log("data ==>",data);

          this.subdivsion.push(data[i]);
        }
        this.searchControl.setValue('');
      },
      (err: any) => {
        // let devMessage = `file:manage-panelists.component.ts,method:getPanelist,userId:${
        //   this.globalUserService.userId
        // };${JSON.stringify(err)}`;
        // this.httpService.logError(devMessage);
      }
    );
  }
  getDepartments() {
    this.deparments = [];
    this.getDepartmentsService = this.httpService.getDepartmentsByLevel(1).subscribe(
      (res) => {

        let data = Object.values(res);
        for (let i = 0; i < data.length; i++) {
          this.deparments.push(data[i]);
        }
        // console.log(this.locationList);
        this.searchControl.setValue('');
        //alert(JSON.stringify(this.deparments));

      },
      (err: any) => {
        // let devMessage = `file:manage-panelists.component.ts,method:getPanelist,userId:${
        //   this.globalUserService.userId
        // };${JSON.stringify(err)}`;
        // this.httpService.logError(devMessage);
      }
    );

  }

  addNewField(){
      this.inputText =(document.getElementById("departmentNametop") as HTMLInputElement).value;
   const user = {
     id:0,
     name:"",
     value: this.inputText,
     levelId:3,
     parentId:this.subdivisionDropdownSelection,
     orderNo:0,
     isActive:true
   };
   this.getDepartmentsService = this.httpService.createDepartmentsBylevel(user).subscribe(
     (res) => {
       let data = Object.values(res);
       for (let i = 0; i < data.length; i++) {
         this.deparments.push(data[i]);
       }
       this.searchControl.setValue('');
       console.log(res);
       this.getPoliceStation();
     },
     (err: any) => {
       // let devMessage = `file:manage-panelists.component.ts,method:getPanelist,userId:${
       //   this.globalUserService.userId
       // };${JSON.stringify(err)}`;
       // this.httpService.logError(devMessage);
     }
   );
 

  }

  GetValue(subId : any){
    const subdivision = this.subdivsion.filter(sd => sd.id == subId.parentId);
    const dep = this.deparments.filter(sd => sd.id == subdivision[0].parentId);
    return (dep[0] !== undefined)? dep[0].id : 0;
  }
  filterSubdivisions(policestation? : any){
    this.filteredSubdivisions=this.subdivsion.filter(sd => sd.id == policestation.parentId);
    //console.log(Array.from(this.deparments.values()).filter((item: Event) => this.locationDropdownSelection === this.subdivsion[5]));
  }

  selectLocation(event?:any){
    this.locationDropdownSelection=event.id;
    this.filterSubdivisionsonAdd(this.locationDropdownSelection);
  }
  gridLocationChange(event:any){
    
  }
  filterSubdivisionsonAdd(dropdownSelection:any){
    this.filteredSubdivisions=this.subdivsion.filter(sd => sd.parentId == dropdownSelection);
  }
  selectSubDivision(event?:any){
    this.subdivisionDropdownSelection=event.id;

  }
  onSave(subdivId:any){
    this.input =(document.getElementById(subdivId) as HTMLInputElement).value
   const user = {
     id:subdivId,
     name: "",
     value:this.input,
     levelId:3,
     parentId:this.parentId,
     orderNo:0,
     isActive:true
   };
   this.getDepartmentsService = this.httpService.createDepartmentsBylevel(user).subscribe(
     (res) => {
       let data = Object.values(res);
       for (let i = 0; i < data.length; i++) {
         this.deparments.push(data[i]);
       }
       this.searchControl.setValue('');
       console.log(res);
       this.getDepartments();
     },
     (err: any) => {
       // let devMessage = `file:manage-panelists.component.ts,method:getPanelist,userId:${
       //   this.globalUserService.userId
       // };${JSON.stringify(err)}`;
       // this.httpService.logError(devMessage);
     }
   );
 

  }
}
