// Import pages, components and helper functions.
import Home from './views/pages/Home.js';
import ExcelPlayground from './views/pages/ExcelPlayground.js';
import About from './views/pages/About.js';
import Items from './views/pages/Items.js';
import ItemShow from './views/pages/ItemShow.js';
import Register from './views/pages/Register.js';
import Error404 from './views/pages/Error404.js';
import location from './views/pages/locations.js';
import login from './views/pages/login.js';
import subdivision from './views/pages/subdivisions.js';
import Templates from './views/pages/Templates.js';
import policestation from './views/pages/policestation.js'
import usermanagement from './views/pages/usermanagement.js';
import ViewForms from './views/pages/ViewForm.js';
import { HttpService } from './views/Core/http.service.js';
import Navbar from './views/components/Navbar.js';
import Footer from './views/components/Footer.js';
import { parseRequestUrl } from './services/utils.js';
import ViewTemplate from './views/pages/ViewTemplate.js';
import EditForm from './views/pages/EditForm.js';
import ViewExcel from './views/pages/ViweExcel.js';
import ViewTemplates from './views/pages/ViewTemplates.js';
import ReviewXLForm from './views/pages/ReviewXLForm.js'
import EditExcelTemplate from './views/pages/EditXlTemplate.js';
import EditExceLForm  from './views/pages/EditXLForm.js';
import ReviewDocForm from './views/pages/ReviewDocForm.js';


// List of supported routes. Any url other than these will render 404 page.
const routes = {
  '/' : login,
  '/Home': Home,
  '/about': About,
  '/items': Items,
  '/items/:id': ItemShow,
  '/register': Register,
  '/locations': location,
  '/templates':Templates,
  '/viewtemplate':ViewTemplate,
  '/subdivision': subdivision,
  '/exceltools': ExcelPlayground,
  '/policestation':policestation,
  '/usermanagement':usermanagement,
  '/forms':ViewForms,
  '/editform':EditForm,
  '/viewexcel': ViewExcel,
  '/editxltemplate' : EditExcelTemplate,
  '/viewtemplates' : ViewTemplates,
  '/editxlform':EditExceLForm,
  '/reviewxlform':ReviewXLForm,
  '/reviewdocform': ReviewDocForm,
  '/location':Location
};
 
const adminroutes = {
  '/' : login,
  '/templates':Templates,
  '/viewtemplate':ViewTemplate,
  '/forms':ViewForms,
  '/viewexcel': ViewExcel,
  '/editxltemplate' : EditExcelTemplate,
  '/viewtemplates' : ViewTemplates,
  '/editxlform':EditExceLForm,
  '/editform':EditForm

};
const loginpage = {
  '/' : login
};

window.environment = {
  production: false,
  backendUrl: 'https://kspformapi.examroom.ai/api/',
  token: ''
};
window.https = new HttpService();//to do global
/**
 * The router code. Takes a URL, checks against the list of
 * supported routes and then renders the corresponding content page.
 */
const router = async () => {
  // Lazy load view element:
  const header = null || document.getElementById('header_root');
  const content = null || document.getElementById('page_root');
  const footer = null || document.getElementById('footer_root');

  // Render the header and footer of the page.
  header.innerHTML = await Navbar.render();
  await Navbar.after_render();
  footer.innerHTML = await Footer.render();
 await Footer.after_render();


  // Destructure the parsed URl from the addressbar.
  const { resource, id, verb } = parseRequestUrl();

  // Parse the URL and if it has an id part, change it with the string ":id".
  const parsedUrl =
    (resource ? '/' + resource : '/') +
    (id ? '/:id' : '') +
    (verb ? '/' + verb : '');

  const token = sessionStorage.getItem('token');
  const role = sessionStorage.getItem('roleid');
  // Render the page from map of supported routes or render 404 page.

  if(role==1 && token){
    const page = routes[parsedUrl] || Error404;

      content.innerHTML = await page.render();

      // if (resource === 'viewtemplate' || resource == 'editform')
      //     await page.after_render(id);
      // else
      //     await page.after_render();

      await page.after_render();

  }
  else if(role ==2 && token )
  {
    const page = adminroutes[parsedUrl] || Error404;
    content.innerHTML = await page.render();
    await page.after_render();
  }
  else {
    const page = loginpage[parsedUrl] || Error404;
    content.innerHTML = await page.render();
    await page.after_render();
  }
};




/**
 * Add event listeners
 */

// Listen on hash change.
window.addEventListener('hashchange', router);

// Listen on page load.
window.addEventListener('load', router);