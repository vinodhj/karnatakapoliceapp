

  export class HttpService {
      async getApi (apiUrl){
        try {
           const tokenvalue = sessionStorage.getItem('token');
          // Create options for the fetch function.
          //const options = { cache: 'force-cache' };
          // Get a response from the API.


          const headersInit = new headersInit();
          const response = await fetch(environment.backendUrl + apiUrl, {
            headers: {
              'Authorization':"Bearer "+tokenvalue,
              'Content-Type': 'application/json'
            }
          });
          // Parse response into JSON.
          const data = await response.json();
          return data;
        } catch (error) {
          // Print catched error to the console.
          console.log('(App) Error occured while getting data.', error);
        }
      };


      async postData(apiUrl, requestBody){
        const tokenvalue = sessionStorage.getItem('token')
          try{
                 return fetch(apiUrl, {
                        method: 'POST',
                        body : requestBody,
                        headers: {
                          'Authorization':"Bearer "+tokenvalue,
                          'Content-Type': 'application/json'
                        },
                        referrer: 'no-referrer'
                      })

          } catch (error) {

          }
      };

      async loadHtml(slideName) {
        const response = await fetch(`./views//Html/${slideName}.html`);
        return await response.text();
    }
  }