export class ClassExcel {

    static xspr = null;
    async render() {
        return /*html*/ `
        <div class="form-container" id="frmMain"> 
            <div class="row col-lg-10 lead">
            &nbsp;&nbsp; <label id="lblName"></label>
            </div>
            <div class="row col-lg-2">
                Status : <label id="lblStatus"/>
            </div>
            <div class="row col-lg-6">
                &nbsp;&nbsp;  <button type="submit" id="btnSubmit" class="btn btn-main">Submit</button>
            </div>
            <div class="row col-lg-offset-5 col-lg-1 right-justify">
                <button type="submit" id="btnBack" class="btn btn-main">Back</button>
            </div>
        </div>
       <div id="x-spreadsheet-demo" syle="margin-top:5rem"></div>
      `;
    };
    /**
     * All the code related to DOM interactions and controls go in here.
     * This is a separate call as these can be registered only after the DOM has been painted.
     */

    stox(wb) {
        var out = [];
        wb.SheetNames.forEach(function(name) {
            var o = { name: name, rows: {} };
            var ws = wb.Sheets[name];
            var aoa = XLSX.utils.sheet_to_json(ws, { raw: false, header: 1 });
            aoa.forEach(function(r, i) {
                var cells = {};
                r.forEach(function(c, j) { cells[j] = ({ text: c }); });
                o.rows[i] = { cells: cells };
            })
            out.push(o);
        });
        return out;
    };

    xtos(sdata) {
        var out = XLSX.utils.book_new();
        sdata.forEach(function(xws) {
            var aoa = [
                []
            ];
            var rowobj = xws.rows;
            for (var ri = 0; ri < rowobj.len; ++ri) {
                var row = rowobj[ri];
                if (!row) continue;
                aoa[ri] = [];
                Object.keys(row.cells).forEach(function(k) {
                    var idx = +k;
                    if (isNaN(idx)) return;
                    aoa[ri][idx] = row.cells[k].text;
                });
            }
            var ws = XLSX.utils.aoa_to_sheet(aoa);
            XLSX.utils.book_append_sheet(out, ws, xws.name);
        });
        console.log(out);
        return out;
    };


    after_render = async() => {
            let self = this;

            $(document).ready(function(e) {

                let formId = sessionStorage.getItem('formId');

                //fnLoadDetails(formId);

                var url = window.environment.backendUrl + `UserForm/xlformcontent?id=${formId}`;
                let detailUrl = window.environment.backendUrl + `UserForm/formdetail?formId=${formId}`;
                let config = { headers: { 'Authorization': 'Bearer ' + sessionStorage.getItem('token') } };

                axios
                    .get(detailUrl, config)
                    .then(res => {

                        $('#btnSubmit').hide();

                        console.log(res.data);

                        $('#lblName').text(res.data.formName);
                        $('#lblStatus').text(res.data.status);

                        if (res.data.status == 'Draft' || res.data.status == 'Rejected') {
                            $('#btnSubmit').show();
                        }

                    });



                axios.get(url, config).then(res => {
                    // console.log(res.data);
                    //  response= (res.data);
                    var self = this;
                    this.xspr = x_spreadsheet('#x-spreadsheet-demo', {
                        mode: 'edit',
                        showToolbar: true,
                        showGrid: true,
                        showBottomBar: true,
                        showContextmenu: true,
                        view: {
                            height: () => document.documentElement.clientHeight - 100,
                            width: () => document.documentElement.clientWidth,
                        },
                    }).loadData([res.data[0]]).change((cdata) => {

                        console.log('calling data');
                        // console.log(cdata);
                        //
                        console.log('>>>', self.xspr.getData());
                        let formId = sessionStorage.getItem('formId');
                        //templateId = templateId.replace('Dynamic-', '');
                        var content = JSON.stringify(self.xspr.getData());
                        console.log(content);
                        var url = window.environment.backendUrl + `UserForm/updateform`;
                        var send = new FormData();
                        send.append('formId', formId);
                        send.append('content', content);
                        let config = {
                            headers: {
                                'Authorization': 'Bearer ' + sessionStorage.getItem('token')
                            }
                        }
                        axios.post(url, send, config).then(res => {
                                console.log('success');
                            })
                            .catch(e => {
                                console.log(JSON.stringify(e));
                            })

                    });
                    this.xspr.on('cell-selected', (cell, ri, ci) => {
                        console.log('cell:', cell, ', ri:', ri, ', ci:', ci);
                    }).on('cell-edited', (text, ri, ci) => {
                        console.log('text:', text, ', ri: ', ri, ', ci:', ci);
                    });

                })
            });


            $('#btnBack').click(function(e) {
                window.location.href = '#/forms';
            });

            $("#xport").click(function(e) {

                e.preventDefault();
                e.stopPropagation();
                var new_wb = this.xtos(this.xspr.getData());
                /* write file and trigger a download */
                XLSX.writeFile(new_wb, 'sheetjs.xlsx', {});


            });

            $('#btnSubmit').click(function(e) {
                e.preventDefault();
                e.stopPropagation();

                let appFormId = sessionStorage.getItem('formId');
                console.log(sessionStorage.getItem('token'));
                var url = window.environment.backendUrl + `UserForm/submit?id=${appFormId}`;

                axios.post(url, {}, {
                    headers: {
                        'Authorization': 'Bearer ' + sessionStorage.getItem('token')
                    }
                }).then(res => {
                    alert('File Submitted successfully!');
                    $('#btnSubmit').hide();
                    $('#lblStatus').text('Submitted');
                });
            })

            var process_wb = (function() {
                var XPORT = document.getElementById('xport');

                return function process_wb(wb) {
                    /* convert to x-spreadsheet form */
                    var data = self.stox(wb);

                    /* update x-spreadsheet */
                    self.xspr.loadData(data);
                    XPORT.disabled = false;
                    // console.log("output", new Date());
                };
            })();

            var do_file = (function() {
                return function do_file(files) {
                    //var ff = files[0];
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        // console.log("onload", new Date());
                        var data = e.target.result;
                        process_wb(XLSX.read(data, { type: 'binary' }));
                    };
                    reader.readAsBinaryString(files);
                    // reader.readAsBinaryString(f);
                };
            })();
        }
        // Initialize function, create initial tokens with itens that are already selected by the user
    init(element) {
        // Create div that wroaps all the elements inside (select, elements selected, search div) to put select inside
        const wrapper = document.createElement("div");
        wrapper.addEventListener("click", clickOnWrapper);
        wrapper.classList.add("multi-select-component");

        // Create elements of search
        const search_div = document.createElement("div");
        search_div.classList.add("search-container");
        const input = document.createElement("input");
        input.classList.add("selected-input");
        input.setAttribute("autocomplete", "off");
        input.setAttribute("tabindex", "0");
        input.addEventListener("keyup", inputChange);
        input.addEventListener("keydown", deletePressed);
        input.addEventListener("click", openOptions);

        const dropdown_icon = document.createElement("a");
        dropdown_icon.setAttribute("href", "#");
        dropdown_icon.classList.add("dropdown-icon");

        dropdown_icon.addEventListener("click", clickDropdown);
        const autocomplete_list = document.createElement("ul");
        autocomplete_list.classList.add("autocomplete-list")
        search_div.appendChild(input);
        search_div.appendChild(autocomplete_list);
        search_div.appendChild(dropdown_icon);

        // set the wrapper as child (instead of the element)
        element.parentNode.replaceChild(wrapper, element);
        // set element as child of wrapper
        wrapper.appendChild(element);
        wrapper.appendChild(search_div);

        createInitialTokens(element);
        addPlaceholder(wrapper);

        function removePlaceholder(wrapper) {
            const input_search = wrapper.querySelector(".selected-input");
            input_search.removeAttribute("placeholder");
        }

        function addPlaceholder(wrapper) {
            const input_search = wrapper.querySelector(".selected-input");
            const tokens = wrapper.querySelectorAll(".selected-wrapper");
            if (!tokens.length && !(document.activeElement === input_search))
                input_search.setAttribute("placeholder", "---------");
        }


        //  that create the initial set of tokens with the options selected by the users
        function createInitialTokens(select) {
            let {
                options_selected
            } = getOptions(select);
            const wrapper = select.parentNode;
            for (let i = 0; i < options_selected.length; i++) {
                createToken(wrapper, options_selected[i]);
            }
        }


        // Listener of user search
        function inputChange(e) {
            e.preventDefault();
            e.stopPropagation();
            const wrapper = e.target.parentNode.parentNode;
            const select = wrapper.querySelector("select");
            const dropdown = wrapper.querySelector(".dropdown-icon");

            const input_val = e.target.value;

            if (input_val) {
                dropdown.classList.add("active");
                populateAutocompleteList(select, input_val.trim());
            } else {
                dropdown.classList.remove("active");
                const event = new Event('click');
                dropdown.dispatchEvent(event);
            }
        }


        // Listen for clicks on the wrapper, if click happens focus on the input
        function clickOnWrapper(e) {
            e.preventDefault();
            e.stopPropagation();
            const wrapper = e.target;
            if (wrapper.tagName == "DIV") {
                const input_search = wrapper.querySelector(".selected-input");
                const dropdown = wrapper.querySelector(".dropdown-icon");
                if (!dropdown.classList.contains("active")) {
                    const event = new Event('click');
                    dropdown.dispatchEvent(event);
                }
                input_search.focus();
                const in_search = wrapper.querySelector(".selected-input");
                in_search.removeAttribute("placeholder");
                removePlaceholder(wrapper);
            }

        }

        function openOptions(e) {
            e.preventDefault();
            e.stopPropagation();
            const input_search = e.target;
            const wrapper = input_search.parentElement.parentElement;
            const dropdown = wrapper.querySelector(".dropdown-icon");
            if (!dropdown.classList.contains("active")) {
                const event = new Event('click');
                dropdown.dispatchEvent(event);
            }
            e.stopPropagation();

        }

        //  that create a token inside of a wrapper with the given value
        function createToken(wrapper, value) {
            const search = wrapper.querySelector(".search-container");
            // Create token wrapper
            const token = document.createElement("div");
            token.classList.add("selected-wrapper");
            const token_span = document.createElement("span");
            token_span.classList.add("selected-label");
            token_span.innerText = value;
            const close = document.createElement("a");
            close.classList.add("selected-close");
            close.setAttribute("tabindex", "-1");
            close.setAttribute("data-option", value);
            close.setAttribute("data-hits", 0);
            close.setAttribute("href", "#");
            close.innerText = "x";
            close.addEventListener("click", removeToken, false)
            token.appendChild(token_span);
            token.appendChild(close);
            wrapper.insertBefore(token, search);
        }


        // Listen for clicks in the dropdown option
        function clickDropdown(e) {

            const dropdown = e.target;
            const wrapper = dropdown.parentNode.parentNode;
            const input_search = wrapper.querySelector(".selected-input");
            const select = wrapper.querySelector("select");
            dropdown.classList.toggle("active");

            if (dropdown.classList.contains("active")) {
                const in_search = wrapper.querySelector(".selected-input");
                in_search.removeAttribute("placeholder");
                removePlaceholder(wrapper);
                input_search.focus();

                if (!input_search.value) {
                    populateAutocompleteList(select, "", true);
                } else {
                    populateAutocompleteList(select, input_search.value);

                }
            } else {
                clearAutocompleteList(select);
                addPlaceholder(wrapper);
            }
        }


        // Clears the results of the autocomplete list
        function clearAutocompleteList(select) {
            const wrapper = select.parentNode;

            const autocomplete_list = wrapper.querySelector(".autocomplete-list");
            autocomplete_list.innerHTML = "";
        }

        // Populate the autocomplete list following a given query from the user
        function populateAutocompleteList(select, query, dropdown = false) {
            const {
                autocomplete_options
            } = getOptions(select);


            let options_to_show;

            if (dropdown)
                options_to_show = autocomplete_options;
            else
                options_to_show = autocomplete(query, autocomplete_options);

            const wrapper = select.parentNode;
            const input_search = wrapper.querySelector(".search-container");
            const autocomplete_list = wrapper.querySelector(".autocomplete-list");
            autocomplete_list.innerHTML = "";
            const result_size = options_to_show.length;

            if (result_size == 1) {

                const li = document.createElement("li");
                li.innerText = options_to_show[0];
                li.setAttribute('data-value', options_to_show[0]);
                li.addEventListener("click", selectOption);
                autocomplete_list.appendChild(li);
                if (query.length == options_to_show[0].length) {
                    const event = new Event('click');
                    li.dispatchEvent(event);

                }
            } else if (result_size > 1) {

                for (let i = 0; i < result_size; i++) {
                    const li = document.createElement("li");
                    li.innerText = options_to_show[i];
                    li.setAttribute('data-value', options_to_show[i]);
                    li.addEventListener("click", selectOption);
                    autocomplete_list.appendChild(li);
                }
            } else {
                const li = document.createElement("li");
                li.classList.add("not-cursor");
                li.innerText = "No options found";
                autocomplete_list.appendChild(li);
            }
        }


        // Listener to autocomplete results when clicked set the selected property in the select option 
        function selectOption(e) {
            const wrapper = e.target.parentNode.parentNode.parentNode;
            const input_search = wrapper.querySelector(".selected-input");
            const option = wrapper.querySelector(`select option[value="${e.target.dataset.value}"]`);

            option.setAttribute("selected", "");
            createToken(wrapper, e.target.dataset.value);
            if (input_search.value) {
                input_search.value = "";
            }

            input_search.focus();

            e.target.remove();
            const autocomplete_list = wrapper.querySelector(".autocomplete-list");


            if (!autocomplete_list.children.length) {
                const li = document.createElement("li");
                li.classList.add("not-cursor");
                li.innerText = "No options found";
                autocomplete_list.appendChild(li);
            }

            const event = new Event('keyup');
            input_search.dispatchEvent(event);
            e.stopPropagation();
        }


        //  that returns a list with the autcomplete list of matches
        function autocomplete(query, options) {
            // No query passed, just return entire list
            if (!query) {
                return options;
            }
            let options_return = [];

            for (let i = 0; i < options.length; i++) {
                if (query.toLowerCase() === options[i].slice(0, query.length).toLowerCase()) {
                    options_return.push(options[i]);
                }
            }
            return options_return;
        }


        // Returns the options that are selected by the user and the ones that are not
        function getOptions(select) {
            // Select all the options available
            const all_options = Array.from(
                select.querySelectorAll("option")
            ).map(el => el.value);

            // Get the options that are selected from the user
            const options_selected = Array.from(
                select.querySelectorAll("option:checked")
            ).map(el => el.value);

            // Create an autocomplete options array with the options that are not selected by the user
            const autocomplete_options = [];
            all_options.forEach(option => {
                if (!options_selected.includes(option)) {
                    autocomplete_options.push(option);
                }
            });

            autocomplete_options.sort();

            return {
                options_selected,
                autocomplete_options
            };

        }

        // Listener for when the user wants to remove a given token.
        function removeToken(e) {

            e.preventDefault();
            e.stopPropagation();

            // Get the value to remove
            const value_to_remove = e.target.dataset.option;
            const wrapper = e.target.parentNode.parentNode;
            const input_search = wrapper.querySelector(".selected-input");
            const dropdown = wrapper.querySelector(".dropdown-icon");
            // Get the options in the select to be unselected
            const option_to_unselect = wrapper.querySelector(`select option[value="${value_to_remove}"]`);
            option_to_unselect.removeAttribute("selected");
            // Remove token attribute
            e.target.parentNode.remove();
            input_search.focus();
            dropdown.classList.remove("active");
            const event = new Event('click');
            dropdown.dispatchEvent(event);
        }

        // Listen for 2 sequence of hits on the delete key, if this happens delete the last token if exist
        function deletePressed(e) {
            const wrapper = e.target.parentNode.parentNode;
            const input_search = e.target;
            const key = e.keyCode || e.charCode;
            const tokens = wrapper.querySelectorAll(".selected-wrapper");

            if (tokens.length) {
                const last_token_x = tokens[tokens.length - 1].querySelector("a");
                let hits = +last_token_x.dataset.hits;

                if (key == 8 || key == 46) {
                    if (!input_search.value) {

                        if (hits > 1) {
                            // Trigger delete event
                            const event = new Event('click');
                            last_token_x.dispatchEvent(event);
                        } else {
                            last_token_x.dataset.hits = 2;
                        }
                    }
                } else {
                    last_token_x.dataset.hits = 0;
                }
            }
            return true;
        }

        // You can call this  if you want to add new options to the select plugin
        // Target needs to be a unique identifier from the select you want to append new option for example #multi-select-plugin
        // Example of usage addOption("#multi-select-plugin", "tesla", "Tesla")
        function addOption(target, val, text) {
            const select = document.querySelector(target);
            let opt = document.createElement('option');
            opt.value = val;
            opt.innerHTML = text;
            select.appendChild(opt);
        }

    }




}
var EditExceLForm = new ClassExcel();
export default EditExceLForm;