const ViewExcel = {
    async render() {
        return https.loadHtml('viewexcel');
    },
    after_render: async () => {

        $(document).ready(function () {

            let url =window.environment.backendUrl + 'UserForm/forms';
            console.log()
            $.ajax({
                type: "GET",
                url: url,
                contentType: 'application/json',
                headers: { 'Authorization': 'Bearer ' + sessionStorage.getItem('token') },
                dataType: 'json',
                success: function (response) {
                    var output = '';
                    response.forEach(item => {
                        output += `<tr>
                        <td>${item.id}</td>
                        <td>${item.formName}</td>
                        <td>${item.name}</td>
                        <td>${item.createdBy}</td>
                        <td>${item.createdOn}</td>
                        <td><button class='lnk-view' id='lnk-${item.id}'>Edit</a></td>
                    </tr>`;

                    });

                    document.getElementById('tblBody').innerHTML = output;
                },
                error: function (error) {
                    console.log(error);
                },
                dataType: 'json'
            });

        });

        document.addEventListener('click', function (e) {

            e.stopPropagation();
            
            if (e.target.id.startsWith('lnk-')) {
                var id = e.target.id.replace('lnk-', '');
                console.log(id);

                window.location.href = '#/editform/' + id;
            }
        });

    }

};
export default ViewExcel;