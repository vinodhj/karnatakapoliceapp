//import environment from './environment.js';
  
const policestation = {
    /**
     * Render the page content.
     */
    render: async () => {
      
      // Map over items and build card components.
      return https.loadHtml('policestation');
    },
    /**
     * All the code related to DOM interactions and controls go in here.
     * This is a separate call as these can be registered only after the DOM has been painted.
     */
    after_render: async () => {

        $(document).ready(function () {  
      
            
            var grid_selector = "#policeStationgrid-table";
            var pager_selector = "#policeStationgrid-pager";              
            const tokenvalue = sessionStorage.getItem('token')
            $(grid_selector).jqGrid({             
              url: environment.backendUrl + 'Department/deptmapping?name=Police-Station',
              mtype: "GET",
              datatype: "json",
              jsonReader : {
                root : "response",
                page : "page",
                total : "total",
                records : "records",
                repeatitems : false,
                id : "column1"
            },
              loadBeforeSend: function(jqXHR) {
                jqXHR.setRequestHeader("Authorization", "Bearer "+tokenvalue);
            },
              height: 450,
              colNames:['id', 'name','Police Station','Sub Division','Location','subid','locid'],
              colModel:[
                {name:'id',index:'id',hidden:true, width:55},
                {name:'name',index:'name',hidden:true,width:95},
                {name:'value',index:'value', width:300},
                {name:'parent.value',index:'value', width:300},    
                {name:'parent.parent.value',index:'value', width:300}, 
                {name:'parent.id',index:'value', width:300,hidden:true},
                {name:'parent.parent.id',index:'value',hidden:true, width:300},            
              ],
              loadonce: true,
              beforeSelectRow: function(rowid, e)
              {
                var selRowIds = $("#policeStationgrid-table").jqGrid("getGridParam", "selarrrow");
                if(rowid != selRowIds[0]){
                  jQuery("#policeStationgrid-table").jqGrid('resetSelection');
                }
                return(true);
              },
              rowNum:10,
              rowList:[10,20,30],
              pager : pager_selector,
              altRows: true,                                                 
              multiselect: true,
              multiboxonly: false,
            //   caption: "jqGrid with listing",
              emptyrecords: 'No Records Found',
              loadComplete : function() {
                  var table = this;
                  setTimeout(function(){
                        //updatePagerIcons(table);
                        $('#grid').trigger( 'reloadGrid' );
                  }, 0);
              }
            });
  
        })  ;

        $("#stationAddForm").dialog({
            autoOpen : false, modal : true, show : "blind", hide : "blind",width:700
          });
        
          $("#openForm").click(function (e) {
            e.preventDefault();  
            e.stopPropagation();
            const tokenvalue = sessionStorage.getItem('token')
            $.ajax({  
                type: "GET",
                 url:environment.backendUrl+"Department/deptmapping?name=Place-Name",
                  dataType: "json",
                   contentType: "application/json",
                   beforeSend: function(xhr){xhr.setRequestHeader('Authorization', "Bearer "+tokenvalue);},
                   success: function (res)  
                {  
                    $("#policelocations").empty();
                    $("#policesubdivisions").empty();
                    $("#policelocations").append($("<option></option>").val(0).html("Select Division"));
                    $("#policesubdivisions").append($("<option></option>").val(0).html("Select Sub Division"));  
                    $.each(res, function (data, value) {  
                        $("#policelocations").append($("<option></option>").val(value.id).html(value.value)); 
                    })  
                }  
  
            });
            $("#stationAddForm").dialog("open");
          }); 
          $("#openForm").click(function (e) {
            e.preventDefault();  
            e.stopPropagation();
            const tokenvalue = sessionStorage.getItem('token')
            $("#policelocations").unbind('change').change(function(event){
                event.stopPropagation();
                event.preventDefault();
                var selected=$("#policelocations").val();
                $("#policesubdivisions").empty();
                $.each($.subdivisions, function (data, value) {  
                        
                      if(selected==value.parentId){
                          $("#policesubdivisions").append($("<option></option>").val(value.id).html(value.value)); 
                      }
                  
              })  
                // console.log(selected);
                
              });
            $.ajax({  
                type: "GET", url:environment.backendUrl+"Department/deptmapping?name=Sub-Division",
                beforeSend: function(xhr){xhr.setRequestHeader('Authorization', "Bearer "+tokenvalue);},
                  contentType: "application/json",
                  success: function (res)  
                {  
                    $("#policesubdivisions").empty();
                    var selected = $("#policelocations").val();
                    $.subdivisions = res;
                    $.each(res, function (data, value) {  
                        
                          $("#policesubdivisions").empty();
                            if(selected==value.parentId){
                                $("#policesubdivisions").append($("<option></option>").val(value.id).html(value.value)); 
                            }
                        
                    })  
                }  
  
            });
            $("#stationAddForm").dialog("open");
          }); 
         
        $("#policesaveBtn").click(function(e) {
          e.preventDefault();  
          e.stopPropagation();
            $("#stationAddForm").dialog("close");
            var getValue = $('#policestationinput').val();
            event.preventDefault();  
            const apiUrl = window.environment.backendUrl +'Department/savedepartment';
            const requestData = {
                id:0,
                name: "",
                value:getValue,
                levelId:3,
                parentId:$('#policesubdivisions').val(),
                orderNo:0,
                isActive: true
              };
            https.postData(apiUrl, JSON.stringify(requestData))
              .then(function (response) {
                  // The API call was successful!
                  if (response.ok) {
                        $("#policeStationgrid-table").trigger( 'reloadGrid' );

                    return response.json();
                  } else {
                    // return Promise.reject(response);
                  }
                }).then(function (data) {
                  // This is the JSON from our response
                  console.log(data);
                }).catch(function (err) {
                  // There was an error
                  console.warn('Something went wrong.', err);
                }); 
     
        });

        $("#stationEditForm").dialog({
            autoOpen : false, modal : true, show : "blind", hide : "blind",width:700
          });

          $("#stationeditopenForm").click(function () {
            const tokenvalue = sessionStorage.getItem('token')
            $.ajax({  
                type: "GET", url:environment.backendUrl+"Department/deptmapping?name=Place-Name", 
                dataType: "json", 
                beforeSend: function(xhr){xhr.setRequestHeader('Authorization', "Bearer "+tokenvalue);},
                contentType: "application/json", 
                success: function (res)  
                {  
                    $("#editlocationPolice").empty();
                    $.each(res, function (data, value) {  
                        $("#editlocationPolice").append($("<option></option>").val(value.id).html(value.value)); 
                    })  
                }  
            });
            $.ajax({  
                type: "GET", 
                url:environment.backendUrl+"Department/deptmapping?name=Sub-Division  ",
                beforeSend: function(xhr){xhr.setRequestHeader('Authorization', "Bearer "+tokenvalue);},
                 contentType: "application/json", 
                success: function (res)  
                {  
                    $("#subdivisionspopuppolice").empty();
                    $.each(res, function (data, value) {  
                        $("#subdivisionspopuppolice").append($("<option></option>").val(value.id).html(value.value)); 
                    })  
                    var rid = jQuery('#policeStationgrid-table').jqGrid("getGridParam", "selrow");
                    if (rid) {
                      var row = jQuery('#policeStationgrid-table').jqGrid("getRowData", rid);
                      $('#editlocationPolice [value="'+row["parent.parent.id"]+'"]').attr("selected", "selected"); 
                     // $('#editlocationPolice [value="'+row["parent.parent.id"]+'"]').attr("selected", "selected"); 
                      $('#subdivisionspopuppolice [value="'+row["parent.id"]+'"]').attr("selected", "selected");  
       
                  }
                }  
            });
            $("#stationEditForm").dialog("open");
           
            var rid = jQuery('#policeStationgrid-table').jqGrid("getGridParam", "selrow");
            if (rid) {
                var row = jQuery('#policeStationgrid-table').jqGrid("getRowData", rid);
                
                $('#policepopupsubdivisioninput').val(row.value);     

                //$('#editlocationPolice [value="'+ row.parent.parent.id+'"]').attr("selected", "selected"); 

                //  $('#editlocationPolice [value="'+row["'parent.parent.id'"]+'"]').attr("selected", "selected"); 
                // $('#subdivisionspopuppolice [value="'+row["parent.id"]+'"]').attr("selected", "selected");  
            }
          }); 
       
          $("#policesaveeditBtn").click(function(e) {
            e.preventDefault();  
            e.stopPropagation();
            $("#stationEditForm").dialog("close");
            var getValue = $('#policepopupsubdivisioninput').val();
            var rid = jQuery('#policeStationgrid-table').jqGrid("getGridParam", "selrow");
            if (rid) {
                var row = jQuery('#policeStationgrid-table').jqGrid("getRowData", rid);
                var id = row.id;
            }
            event.preventDefault();  
            const apiUrl = window.environment.backendUrl + 'Department/savedepartment';
            const requestData = {
                id:row.id,
                name: "",
                value:getValue,
                levelId:3,
                parentId:$("#subdivisionspopuppolice").val(),
                orderNo:0,
                isActive: true
              };
            https.postData(apiUrl, JSON.stringify(requestData))
              .then(function (response) {
                  // The API call was successful!
                  if (response.ok) {
                    $("#policeStationgrid-table").trigger( 'reloadGrid' );

                    return response.json();
                  } else {
                    return Promise.reject(response);
                  }
                }).then(function (data) {
                  // This is the JSON from our response
                  console.log(data);
                }).catch(function (err) {
                  // There was an error
                  console.warn('Something went wrong.', err);
                }); 
     
        });
        
        $("#policedeleteForm").dialog({
            autoOpen : false, modal : true, show : "blind", hide : "blind"
          });

          $("#stationdeleteopenForm").click(function () {
            $("#policedeleteForm").dialog("open");
            var rid = jQuery('#policeStationgrid-table').jqGrid("getGridParam", "selrow");
            if (rid) {
                var row = jQuery('#policeStationgrid-table').jqGrid("getRowData", rid);
                $('#subdeletelocationinput').val(row.value);                
            }
          }); 



          $("#policedeleteBtn").click(function(e) {
            e.preventDefault();  
            e.stopPropagation();
            $("#policedeleteForm").dialog("close");
            var rid = jQuery('#policeStationgrid-table').jqGrid("getGridParam", "selrow");
            if (rid) {
                var row = jQuery('#policeStationgrid-table').jqGrid("getRowData", rid);

            }
            event.preventDefault();  
            const apiUrl = window.environment.backendUrl +'Department/savedepartment';
            const requestData = {
                id:row.id,
                name: "",
                value:row.value,
                levelId:3,
                parentId:null,
                orderNo:0,
                isActive: false
              };
            https.postData(apiUrl, JSON.stringify(requestData))
              .then(function (response) {
                  // The API call was successful!
                  if (response.ok) {
                    $("#policeStationgrid-table").trigger( 'reloadGrid' );

                    return response.json();
                  } 
                  else if(response.status==500){
                    alert("Entity Cannot Be Deleted");
                  }
                  
                  else {
                    return Promise.reject(response);
                  }
                }).then(function (data) {
                  // This is the JSON from our response
                  console.log(data);
                }).catch(function (err) {
                  // There was an error
                  console.warn('Something went wrong.', err);
                }); 
     
        });
       
    }
    
  };
  
  export default policestation;
  