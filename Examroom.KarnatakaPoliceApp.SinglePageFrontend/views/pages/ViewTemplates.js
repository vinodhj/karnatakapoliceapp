const ViewTemplates = {
    async render() {
        return https.loadHtml('viewtemplates');
    },

    after_render: async () =>{
        let xspr;
        function stox(wb) {
            var out = [];
            wb.SheetNames.forEach(function (name) {
                var o = { name: name, rows: {} };
                var ws = wb.Sheets[name];
                var aoa = XLSX.utils.sheet_to_json(ws, { raw: false, header: 1 });
                aoa.forEach(function (r, i) {
                    var cells = {};
                    r.forEach(function (c, j) { cells[j] = ({ text: c }); });
                    o.rows[i] = { cells: cells };
                })
                out.push(o);
            });
            return out;
        }
        function handleFile(e) { 
            var f = e.target.files[0];
            var reader = new FileReader();
            reader.onload = function (e) {
                console.log("onload", new Date());
                var data = stox(XLSX.read(e.target.result, { type: 'binary' }));
                xspr.loadData(data);
            };
            reader.readAsBinaryString(f); 
            setTimeout(()=>{
                console.log('>>>>>',xspr.getData());
            },5000)
        }
        (function () {
            console.log('kjsdf')
            var xlf = document.getElementById('xlf');
            if (!xlf.addEventListener) return;
            xlf.addEventListener('change', handleFile, false);
        }) ();
        
        $(document).ready(function(e){
                xspr = x_spreadsheet('#x-spreadsheet-demo', {
                    mode: 'edit',
                    showToolbar: true,
                    showGrid: true,
                    showBottomBar: true,
                    showContextmenu: true,
                    view: {
                        height: () => document.documentElement.clientHeight - 100,
                        width: () => document.documentElement.clientWidth,
                    },
                    
                }).loadData([{}]).change((cdata) => {
                    // console.log(cdata);
                    console.log('>>>', xspr.getData());
                });
                xspr.on('cell-selected', (cell, ri, ci) => {
                    console.log('cell:', cell, ', ri:', ri, ', ci:', ci);
                }).on('cell-edited', (text, ri, ci) => {
                    console.log('text:', text, ', ri: ', ri, ', ci:', ci);
                });
            fnGetRegionNames();

            fnLoadTemplates();
            
            $("#AddTemplate").dialog({
                autoOpen: false, modal: true, show: "blind", hide: "blind", height : 600, width:600
            });

            $("#openForm").click(function (e) {
                e.preventDefault();  
                e.stopPropagation();
                $("#AddTemplate").dialog("open");
            });

            $("#place").change(function(){
                var selValue = $("#place").val();
                console.log("Selected Place "+ selValue);
                getSubDivisionsDropdown();
            });

            $("#subdev").change(function(){
                var selValue = $("#subdev").val();
                console.log("Selected Sub Devision "+ selValue);
                getPoliceStationDropdown();
            });

            $("#dept").change(function(){
                var SelDept = $("#dept").val();
                console.log("Selected Departmet "+ SelDept);
            });  
            
            $("#saveBtn").click(function (e) {

                e.stopPropagation();
                e.preventDefault();

                var SelDept = $("#dept").val();
                console.log("Selected Departmet "+ SelDept);
                var name = $('#name').val();

                var file = $('#xlf').prop('files')[0];
                var fileName = file.name;
               
                var formData = new FormData();
                formData.append('name', name);
                formData.append('departmentIds', SelDept);

                if(file.type == 'text/html')
                    formData.append('file',file);
                else
                    formData.append('content', JSON.stringify([xspr.getData()[0]]));

                const apiUrl = window.environment.backendUrl +'UserForm/addtemplate';
                $.ajax({
                    type:'post',
                    url: apiUrl,
                    data: formData,
                    headers: { 'Authorization': 'Bearer ' + sessionStorage.getItem('token') },
                    processData: false,
                    contentType: false,
                    enctype: 'multipart/form-data',
                    error: function(response) {
                      //displayResponseMessage(response);
                      console.log(response);
                    },
                    success: function(response) {
                        fnLoadTemplates();   
                        console.log(" Successfull");
                        $("#AddTemplate").dialog("close");
                    },
                  });
                  
            });

            
        document.addEventListener('click', function (e) {

            // e.preventDefault();
            e.stopPropagation();

            if (e.target.id.startsWith('lnk-')) {
                var id = e.target.id.replace('lnk-', '');
                var action = e.target.innerHTML;

                if(action == 'Edit'){
                    
                    sessionStorage.setItem('templateId', id);
                    window.location.href = '#/editxltemplate';
                    // window.location.reload(true);
                }
                else
                {   
                    sessionStorage.setItem('templateId', id);
                    window.location.href = '#/viewtemplate';
                    // window.location.reload(true);
                }
            }
        });

        });

        function fnGetRegionNames(){
            $.ajax({
                method: "get",
                headers: { 'Authorization': 'Bearer ' + sessionStorage.getItem('token') },
                url: window.environment.backendUrl +'Department/deptmapping?name=Place-Name',
                async: true,
                success: function (result) {
                    var str = document.getElementById('place');
                    /* dropdownObject = new MultiSelectDropdown();
                    console.log(dropdownObject); */
                    for (var i = 0; i < result.length; i++) {
                        str.innerHTML = str.innerHTML + '<option value=' + result[i].id + '>' + result[i].value + '</option>';
                    }
                
                    
                }
            });    
        }

        function fnLoadTemplates(){

            let url =window.environment.backendUrl + 'UserForm/templates';

            $.ajax({
                type: "GET",
                url: url,
                contentType: 'application/json',
                headers: { 'Authorization': 'Bearer ' + sessionStorage.getItem('token') },
                dataType: 'json',
                success: function (response) {
                    var output = '';
                    response.forEach(item => {

                        var btnName = 'View';

                        if(item.templateType == 'Dynamic')
                            btnName = 'Edit';

                        output += `<tr>
                        <td>${item.id}</td>
                        <td>${item.name}</td>
                        <td>${item.templateType}</td>
                        <td>${item.createdBy}</td>
                        <td>${item.createdOn}</td>
                        <td>${item.modifiedBy}</td>
                        <td>${item.modifiedOn}</td>
                        <td><button class='grid-lnk' title='${btnName} Template' id='lnk-${item.id}'>${btnName}</a></td>
                    </tr>`;

                    });

                    document.getElementById('tblBody').innerHTML = output;
                },
                error: function (error) {
                    console.log(error);
                },
                dataType: 'json'
            });
        }

        function getStrArray(arr){
            let strArr = '';

            for(let i=0;i<arr.length; i++){
                strArr+=',' + arr[i];
            }

            return strArr.substring(1, strArr.length);
        }

        function getSubDivisionsDropdown() {
            var selectedValues = getStrArray($('#place').val())
            //console.log(selectedValues);
            $.ajax({
                method: "get",
                headers: { 'Authorization': 'Bearer ' + sessionStorage.getItem('token') },
                url: window.environment.backendUrl + 'Department/getdepartmentdetails?levelId=2&parentIds=' + selectedValues,
                async: true,
                success: function (result) {
                    var str = document.getElementById('subdev');
                    str.innerHTML = '';
                    /* dropdownObject = new MultiSelectDropdown();
                    console.log(dropdownObject); */
                    for (var i = 0; i < result.length; i++) {
                        str.innerHTML = str.innerHTML + '<option value=' + result[i].id + '>' + result[i].value + '</option>';
                    }

                }
            });

        }

        function getPoliceStationDropdown() {
            var parentIds = getStrArray( $('#subdev').val());
            //console.log(parentIds);
            $.ajax({
                method: "get",
                headers: { 'Authorization': 'Bearer ' + sessionStorage.getItem('token') },
                url: window.environment.backendUrl +'Department/getdepartmentdetails?levelId=3&parentIds=' + parentIds,
                async: true,
                success: function (result) {
                    var str = document.getElementById('dept');
                    str.innerHTML = '';
                    /* dropdownObject = new MultiSelectDropdown();
                    console.log(dropdownObject); */
                    for (var i = 0; i < result.length; i++) {
                        str.innerHTML = str.innerHTML + '<option value=' + result[i].id + '>' + result[i].value + '</option>';
                    }

                }
            });
        }
     
    }
 
}

export default ViewTemplates;