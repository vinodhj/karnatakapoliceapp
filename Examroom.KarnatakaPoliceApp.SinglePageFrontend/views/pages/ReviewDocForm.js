const ReviewDocForm = {
  /**
   * Render the page content.
   */
  render: async () => {
    //         `
    //         function Say_Hello() {
    //             alert("Hello Poftut.com")
    //             }
    // `
    return https.loadHtml('reviewdocform');

  },


  after_render: async () => {


    $(document).ready(function (e) {

      fnLoadFormDetails();
      fnLoadData();


      $("#savebtn").click(function (e) {

        e.preventDefault();
        e.stopPropagation();

        var arr = [];
        var myform = document.getElementById('frameID');
        var inp = myform.getElementsByTagName('input');
        console.log(inp);
        for (let i = 0; i < inp[0].form.length; i++) {
          arr.push(inp[0].form[i].value);
        }
        console.log(JSON.stringify(arr));
        var formData = new FormData();

        var id = sessionStorage.getItem('formId');

        formData.append('formId', id);
        formData.append('content', JSON.stringify(arr));
        const apiUrl = window.environment.backendUrl + 'UserForm/updatedocform';
        $.ajax({
          type: 'post',
          url: apiUrl,
          data: formData,
          headers: { 'Authorization': 'Bearer ' + sessionStorage.getItem('token') },
          processData: false,
          contentType: false,
          enctype: 'multipart/form-data',
          error: function (response) {
            //displayResponseMessage(response);
            console.log(response);
          },
          success: function (response) {
            console.log(" Successfull");
          },

        });

      });

      $('#btnReject').click(function(e){
        e.preventDefault();
        e.stopPropagation();

        let appFormId = sessionStorage.getItem('formId');
        console.log(sessionStorage.getItem('token'));
        var url = window.environment.backendUrl + `UserForm/reject?id=${appFormId}`;

        axios.post(url,{},
            {
                headers: {
                    'Authorization': 'Bearer ' + sessionStorage.getItem('token')
                }
            }).then(res=> {
                alert('File Rejected successfully!');
                $('#btnApprove').hide();
                $('#btnReject').hide();
                $('#lblStatus').text('Rejected');
            });
    });

    $('#btnApprove').click(function(e){
        e.preventDefault();
        e.stopPropagation();

        let appFormId = sessionStorage.getItem('formId');
        console.log(sessionStorage.getItem('token'));
        var url = window.environment.backendUrl + `UserForm/approve?id=${appFormId}`;

        axios.post(url,{},
            {
                headers: {
                    'Authorization': 'Bearer ' + sessionStorage.getItem('token')
                }
            }).then(res=> {
                alert('File Approved successfully!');
                $('#btnApprove').hide();
                $('#btnReject').hide();
                $('#lblStatus').text('Approved');
            });
    });
    

    });

    function fnLoadFormDetails(){
      
      let formId = sessionStorage.getItem('formId');
      let detailUrl = window.environment.backendUrl + `UserForm/formdetail?formId=${formId}`;
            let config = { headers: {'Authorization': 'Bearer ' + sessionStorage.getItem('token')}};
            
            axios
            .get(detailUrl, config)
            .then(res=> {
      
                $('#btnApprove').hide();
                $('#btnReject').hide();

                console.log(res.data);

                $('#lblName').text(res.data.formName);
                $('#lblStatus').text(res.data.status);

                if(res.data.status == 'Submitted'){
                    $('#btnApprove').show();
                    $('#btnReject').show();
                }
                
            });
    }

    function fnLoadData() {

      var formId = sessionStorage.getItem('formId');
      var url = window.environment.backendUrl + `UserForm/docform?id=${formId}`;

      $.ajax({
        type: 'get',
        url: url,
        success: function (response) {
          $('#frameID').html(response);
          

          const apiUrl = window.environment.backendUrl + `UserForm/userinput?id=${formId}`;

          //var a = JSON.parse("[\"sndcj\",\"\",\"\",\"dsncj\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\"]");
          $.ajax({
            type: 'get',
            url: apiUrl,
            headers: { 'Authorization': 'Bearer ' + sessionStorage.getItem('token') },
            error: function (response) {
              //displayResponseMessage(response);
              console.log(response);
            },
            success: function (response) {
              console.log(" Successfull");
              var a=(response);
              var myform = document.getElementById('frameID');
              var inp = myform.getElementsByTagName('input');
              // console.log(inp);
              for (let i = 0; i < inp[0].form.length; i++) {
                //  arr.push(inp[0].form[i].value);
                inp[0].form[i].value = a[i];
              }
              console.log('>>', inp)
            },

          });

          //  var a = JSON.parse(response.content);
         

         
         
        }, error: function (err) {
          alert(err.error);
        }
      });
    }

  }


};
export default ReviewDocForm;