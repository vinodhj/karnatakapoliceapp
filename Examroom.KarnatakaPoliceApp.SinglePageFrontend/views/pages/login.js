const Login = {
    /**
     * Render the page content.
     */
    render: async () => {
      sessionStorage.removeItem('token');
        return https.loadHtml('login');
    },
    /**
     * All the code related to DOM interactions and controls go in here.
     * This is a separate call as these can be registered only after the DOM has been painted.
     */
    after_render: async () => {
      $("#logout").hide();
        document.getElementById("myForm").addEventListener("submit", async function(e){
            e.preventDefault();    //stop form from submitting
            var emailid = document.getElementById('txtEmailId').value;
            var pwd = document.getElementById('txtPassword').value;
            const apiUrl = window.environment.backendUrl + 'Account/login';

            const requestData = {
                username:emailid,
                password:pwd
            }

            await https
                    .postData(apiUrl, JSON.stringify(requestData))
                    .then(async function(response ){

                      if(response.ok){
                        var output = await response.json();

                        console.log('>>', output);

                        sessionStorage.setItem("token",output.token);
                        sessionStorage.setItem("roleid",output.roleId);
                        sessionStorage.setItem('roleName', output.roleName);
                        
                        //window.localStorage.setItem();
                        // window.localStorage.setItem()
                        if(output.roleId==1){
                          window.location.href = "#/usermanagement";
                        }
                        else{
                          window.location.href = "#/forms";
                        }
                         
                      }
                    }).catch(e=>{
                      console.log(e);
                    });        
        });
    }
  };
  export default Login;