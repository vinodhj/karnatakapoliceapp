export class ClassExcelPlayground {

    static xspr = null;
    async render() {
        return /*html*/ `
        <input type="file"  class="btn btn-warning" value="Import XLSX" name="xlfile" id="xlf" />
        <input type="submit"  class="btn btn-warning"value="Export to XLSX!" id="xport" disabled="true">
        <button id="share" class="btn btn-warning"> Share </button>
        <div id="dropdownList">
            <span id="close-btn">&times;</span>
                <div class="container">
                    <select multiple data-multi-select-plugin id="sel">
                        <option value="">Select</option>
                    </select>                    
                </div>
               <br/>
        </div>
        <br/>
        <div id="x-spreadsheet-demo"></div>
        
      `;
    };
    /**
     * All the code related to DOM interactions and controls go in here.
     * This is a separate call as these can be registered only after the DOM has been painted.
     */

    stox(wb) {
        var out = [];
        wb.SheetNames.forEach(function (name) {
            var o = { name: name, rows: {} };
            var ws = wb.Sheets[name];
            var aoa = XLSX.utils.sheet_to_json(ws, { raw: false, header: 1 });
            aoa.forEach(function (r, i) {
                var cells = {};
                r.forEach(function (c, j) { cells[j] = ({ text: c }); });
                o.rows[i] = { cells: cells };
            })
            out.push(o);
        });
        return out;
    };
    xtos(sdata) {
        var out = XLSX.utils.book_new();
        sdata.forEach(function (xws) {
            var aoa = [[]];
            var rowobj = xws.rows;
            for (var ri = 0; ri < rowobj.len; ++ri) {
                var row = rowobj[ri];
                if (!row) continue;
                aoa[ri] = [];
                Object.keys(row.cells).forEach(function (k) {
                    var idx = +k;
                    if (isNaN(idx)) return;
                    aoa[ri][idx] = row.cells[k].text;
                });
            }
            var ws = XLSX.utils.aoa_to_sheet(aoa);
            XLSX.utils.book_append_sheet(out, ws, xws.name);
        });
        return out;
    };


    after_render() {
        let self = this;
        var saveIcon = 'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBzdGFuZGFsb25lPSJubyI/PjwhRE9DVFlQRSBzdmcgUFVCTElDICItLy9XM0MvL0RURCBTVkcgMS4xLy9FTiIgImh0dHA6Ly93d3cudzMub3JnL0dyYXBoaWNzL1NWRy8xLjEvRFREL3N2ZzExLmR0ZCI+PHN2ZyB0PSIxNTc3MTc3MDkyOTg4IiBjbGFzcz0iaWNvbiIgdmlld0JveD0iMCAwIDEwMjQgMTAyNCIgdmVyc2lvbj0iMS4xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHAtaWQ9IjI2NzgiIHdpZHRoPSIxOCIgaGVpZ2h0PSIxOCIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PC9zdHlsZT48L2RlZnM+PHBhdGggZD0iTTIxMy4zMzMzMzMgMTI4aDU5Ny4zMzMzMzRhODUuMzMzMzMzIDg1LjMzMzMzMyAwIDAgMSA4NS4zMzMzMzMgODUuMzMzMzMzdjU5Ny4zMzMzMzRhODUuMzMzMzMzIDg1LjMzMzMzMyAwIDAgMS04NS4zMzMzMzMgODUuMzMzMzMzSDIxMy4zMzMzMzNhODUuMzMzMzMzIDg1LjMzMzMzMyAwIDAgMS04NS4zMzMzMzMtODUuMzMzMzMzVjIxMy4zMzMzMzNhODUuMzMzMzMzIDg1LjMzMzMzMyAwIDAgMSA4NS4zMzMzMzMtODUuMzMzMzMzeiBtMzY2LjkzMzMzNCAxMjhoMzQuMTMzMzMzYTI1LjYgMjUuNiAwIDAgMSAyNS42IDI1LjZ2MTE5LjQ2NjY2N2EyNS42IDI1LjYgMCAwIDEtMjUuNiAyNS42aC0zNC4xMzMzMzNhMjUuNiAyNS42IDAgMCAxLTI1LjYtMjUuNlYyODEuNmEyNS42IDI1LjYgMCAwIDEgMjUuNi0yNS42ek0yMTMuMzMzMzMzIDIxMy4zMzMzMzN2NTk3LjMzMzMzNGg1OTcuMzMzMzM0VjIxMy4zMzMzMzNIMjEzLjMzMzMzM3ogbTEyOCAwdjI1NmgzNDEuMzMzMzM0VjIxMy4zMzMzMzNoODUuMzMzMzMzdjI5OC42NjY2NjdhNDIuNjY2NjY3IDQyLjY2NjY2NyAwIDAgMS00Mi42NjY2NjcgNDIuNjY2NjY3SDI5OC42NjY2NjdhNDIuNjY2NjY3IDQyLjY2NjY2NyAwIDAgMS00Mi42NjY2NjctNDIuNjY2NjY3VjIxMy4zMzMzMzNoODUuMzMzMzMzek0yNTYgMjEzLjMzMzMzM2g4NS4zMzMzMzMtODUuMzMzMzMzeiBtNDI2LjY2NjY2NyAwaDg1LjMzMzMzMy04NS4zMzMzMzN6IG0wIDU5Ny4zMzMzMzR2LTEyOEgzNDEuMzMzMzMzdjEyOEgyNTZ2LTE3MC42NjY2NjdhNDIuNjY2NjY3IDQyLjY2NjY2NyAwIDAgMSA0Mi42NjY2NjctNDIuNjY2NjY3aDQyNi42NjY2NjZhNDIuNjY2NjY3IDQyLjY2NjY2NyAwIDAgMSA0Mi42NjY2NjcgNDIuNjY2NjY3djE3MC42NjY2NjdoLTg1LjMzMzMzM3ogbTg1LjMzMzMzMyAwaC04NS4zMzMzMzMgODUuMzMzMzMzek0zNDEuMzMzMzMzIDgxMC42NjY2NjdIMjU2aDg1LjMzMzMzM3oiIHAtaWQ9IjI2NzkiIGZpbGw9IiMyYzJjMmMiPjwvcGF0aD48L3N2Zz4='
        var preview = 'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBzdGFuZGFsb25lPSJubyI/PjwhRE9DVFlQRSBzdmcgUFVCTElDICItLy9XM0MvL0RURCBTVkcgMS4xLy9FTiIgImh0dHA6Ly93d3cudzMub3JnL0dyYXBoaWNzL1NWRy8xLjEvRFREL3N2ZzExLmR0ZCI+PHN2ZyB0PSIxNjIxMzI4NTkxMjQzIiBjbGFzcz0iaWNvbiIgdmlld0JveD0iMCAwIDEwMjQgMTAyNCIgdmVyc2lvbj0iMS4xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHAtaWQ9IjU2NjMiIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB3aWR0aD0iMjAwIiBoZWlnaHQ9IjIwMCI+PGRlZnM+PHN0eWxlIHR5cGU9InRleHQvY3NzIj48L3N0eWxlPjwvZGVmcz48cGF0aCBkPSJNNTEyIDE4Ny45MDRhNDM1LjM5MiA0MzUuMzkyIDAgMCAwLTQxOC41NiAzMTUuNjQ4IDQzNS4zMjggNDM1LjMyOCAwIDAgMCA4MzcuMTIgMEE0MzUuNDU2IDQzNS40NTYgMCAwIDAgNTEyIDE4Ny45MDR6TTUxMiAzMjBhMTkyIDE5MiAwIDEgMSAwIDM4NCAxOTIgMTkyIDAgMCAxIDAtMzg0eiBtMCA3Ni44YTExNS4yIDExNS4yIDAgMSAwIDAgMjMwLjQgMTE1LjIgMTE1LjIgMCAwIDAgMC0yMzAuNHpNMTQuMDggNTAzLjQ4OEwxOC41NiA0ODUuNzZsNC44NjQtMTYuMzg0IDQuOTI4LTE0Ljg0OCA4LjA2NC0yMS41NjggNC4wMzItOS43OTIgNC43MzYtMTAuODggOS4zNDQtMTkuNDU2IDEwLjc1Mi0yMC4wOTYgMTIuNjA4LTIxLjMxMkE1MTEuNjE2IDUxMS42MTYgMCAwIDEgNTEyIDExMS4xMDRhNTExLjQ4OCA1MTEuNDg4IDAgMCAxIDQyNC41MTIgMjI1LjY2NGwxMC4yNCAxNS42OGMxMS45MDQgMTkuMiAyMi41OTIgMzkuMTA0IDMyIDU5Ljc3NmwxMC40OTYgMjQuOTYgNC44NjQgMTMuMTg0IDYuNCAxOC45NDQgNC40MTYgMTQuODQ4IDQuOTkyIDE5LjM5Mi0zLjIgMTIuODY0LTMuNTg0IDEyLjgtNi40IDIwLjA5Ni00LjQ4IDEyLjYwOC00Ljk5MiAxMi45MjhhNTExLjM2IDUxMS4zNiAwIDAgMS0xNy4yOCAzOC40bC0xMi4wMzIgMjIuNC0xMS45NjggMjAuMDk2QTUxMS41NTIgNTExLjU1MiAwIDAgMSA1MTIgODk2YTUxMS40ODggNTExLjQ4OCAwIDAgMS00MjQuNDQ4LTIyNS42bC0xMS4zMjgtMTcuNTM2YTUxMS4yMzIgNTExLjIzMiAwIDAgMS0xOS44NC0zNS4wMDhMNTMuMzc2IDYxMS44NGwtOC42NC0xOC4yNC0xMC4xMTItMjQuMTI4LTcuMTY4LTE5LjY0OC04LjMyLTI2LjYyNC0yLjYyNC05Ljc5Mi0yLjQ5Ni05LjkyeiIgcC1pZD0iNTY2NCI+PC9wYXRoPjwvc3ZnPg=='

        var saveIconEL = document.createElement('img');
        // saveIconEL.src = saveIcon;
        saveIconEL.width = 16;
        saveIconEL.height = 16;
        var previewEl = document.createElement('img')
        // previewEl.src = preview;
        previewEl.width = 16
        previewEl.height = 16

        this.xspr = x_spreadsheet('#x-spreadsheet-demo', {
            mode: 'edit',
            showToolbar: true,
            showGrid: true,
            showBottomBar: true,
            showContextmenu: true,
            view: {
                height: () => document.documentElement.clientHeight - 100,
                width: () => document.documentElement.clientWidth,
            },
            extendToolbar: {
                left: [
                    {
                        tip: 'Save',
                        el: saveIconEL,
                        icon: saveIcon,
                        onClick: (data, sheet) => {
                            console.log('click save button：', data, sheet)
                        }
                    }
                ],
                right: [
                    {
                        tip: 'Preview',
                        el: previewEl,
                        icon: preview,
                        onClick: (data, sheet) => {
                            console.log('click preview button：', data)
                        }
                    }
                ],
            }
        }).loadData([{}]).change((cdata) => {
            // console.log(cdata);
            console.log('>>>', self.xspr.getData());
        });

        this.xspr.on('cell-selected', (cell, ri, ci) => {
            console.log('cell:', cell, ', ri:', ri, ', ci:', ci);
        }).on('cell-edited', (text, ri, ci) => {
            console.log('text:', text, ', ri: ', ri, ', ci:', ci);
        });


        var process_wb = (function () {
            var XPORT = document.getElementById('xport');

            return function process_wb(wb) {
                /* convert to x-spreadsheet form */
                var data = self.stox(wb);

                /* update x-spreadsheet */
                self.xspr.loadData(data);
                XPORT.disabled = false;
                console.log("output", new Date());
            };
        })();

        var do_file = (function () {
            return function do_file(files) {
                var f = files[0];
                var reader = new FileReader();
                reader.onload = function (e) {
                    console.log("onload", new Date());
                    var data = e.target.result;
                    process_wb(XLSX.read(data, { type: 'binary' }));
                };
                reader.readAsBinaryString(f);
            };
        })();

        (function () {
               // console.log('kjsdf')
                var xlf = document.getElementById('xlf');
                var xport = document.getElementById('xport');
                var shareBtn = document.getElementById('share');
                var modalBackground = document.getElementById('dropdownList');
                var closeBtn = document.getElementById('close-btn');
                if (!xlf.addEventListener) return;
                function handleFile(e) { do_file(e.target.files); }
                function export_xlsx() {

                    var new_wb = self.xtos(self.xspr.getData());
                    /* write file and trigger a download */
                    XLSX.writeFile(new_wb, 'sheetjs.xlsx', {});
                }
                xlf.addEventListener('change', handleFile, false);
                xport.addEventListener('click', export_xlsx, false);
                shareBtn.addEventListener('click', openShareModal, false);
                closeBtn.addEventListener('click', closeShareModal, false);
            }) ();
        }
    // Initialize function, create initial tokens with itens that are already selected by the user
    init(element) {
            // Create div that wroaps all the elements inside (select, elements selected, search div) to put select inside
            const wrapper = document.createElement("div");
            wrapper.addEventListener("click", clickOnWrapper);
            wrapper.classList.add("multi-select-component");

            // Create elements of search
            const search_div = document.createElement("div");
            search_div.classList.add("search-container");
            const input = document.createElement("input");
            input.classList.add("selected-input");
            input.setAttribute("autocomplete", "off");
            input.setAttribute("tabindex", "0");
            input.addEventListener("keyup", inputChange);
            input.addEventListener("keydown", deletePressed);
            input.addEventListener("click", openOptions);

            const dropdown_icon = document.createElement("a");
            dropdown_icon.setAttribute("href", "#");
            dropdown_icon.classList.add("dropdown-icon");

            dropdown_icon.addEventListener("click", clickDropdown);
            const autocomplete_list = document.createElement("ul");
            autocomplete_list.classList.add("autocomplete-list")
            search_div.appendChild(input);
            search_div.appendChild(autocomplete_list);
            search_div.appendChild(dropdown_icon);

            // set the wrapper as child (instead of the element)
            element.parentNode.replaceChild(wrapper, element);
            // set element as child of wrapper
            wrapper.appendChild(element);
            wrapper.appendChild(search_div);

            createInitialTokens(element);
            addPlaceholder(wrapper);

            function removePlaceholder(wrapper) {
                const input_search = wrapper.querySelector(".selected-input");
                input_search.removeAttribute("placeholder");
            }

            function addPlaceholder(wrapper) {
                const input_search = wrapper.querySelector(".selected-input");
                const tokens = wrapper.querySelectorAll(".selected-wrapper");
                if (!tokens.length && !(document.activeElement === input_search))
                    input_search.setAttribute("placeholder", "---------");
            }


            //  that create the initial set of tokens with the options selected by the users
            function createInitialTokens(select) {
                let {
                    options_selected
                } = getOptions(select);
                const wrapper = select.parentNode;
                for (let i = 0; i < options_selected.length; i++) {
                    createToken(wrapper, options_selected[i]);
                }
            }


            // Listener of user search
            function inputChange(e) {
                e.preventDefault();
                e.stopPropagation();
                const wrapper = e.target.parentNode.parentNode;
                const select = wrapper.querySelector("select");
                const dropdown = wrapper.querySelector(".dropdown-icon");
                
                const input_val = e.target.value;

                if (input_val) {
                    dropdown.classList.add("active");
                    populateAutocompleteList(select, input_val.trim());
                } else {
                    dropdown.classList.remove("active");
                    const event = new Event('click');
                    dropdown.dispatchEvent(event);
                }
            }


            // Listen for clicks on the wrapper, if click happens focus on the input
            function clickOnWrapper(e) {
                e.preventDefault();
                e.stopPropagation();
                const wrapper = e.target;
                if (wrapper.tagName == "DIV") {
                    const input_search = wrapper.querySelector(".selected-input");
                    const dropdown = wrapper.querySelector(".dropdown-icon");
                    if (!dropdown.classList.contains("active")) {
                        const event = new Event('click');
                        dropdown.dispatchEvent(event);
                    }
                    input_search.focus();
                    const in_search = wrapper.querySelector(".selected-input");
                    in_search.removeAttribute("placeholder");
                    removePlaceholder(wrapper);
                }

            }

            function openOptions(e) {
                e.preventDefault();
                e.stopPropagation();
                const input_search = e.target;
                const wrapper = input_search.parentElement.parentElement;
                const dropdown = wrapper.querySelector(".dropdown-icon");
                if (!dropdown.classList.contains("active")) {
                    const event = new Event('click');
                    dropdown.dispatchEvent(event);
                }
                e.stopPropagation();

            }

            //  that create a token inside of a wrapper with the given value
            function createToken(wrapper, value) {
                const search = wrapper.querySelector(".search-container");
                // Create token wrapper
                const token = document.createElement("div");
                token.classList.add("selected-wrapper");
                const token_span = document.createElement("span");
                token_span.classList.add("selected-label");
                token_span.innerText = value;
                const close = document.createElement("a");
                close.classList.add("selected-close");
                close.setAttribute("tabindex", "-1");
                close.setAttribute("data-option", value);
                close.setAttribute("data-hits", 0);
                close.setAttribute("href", "#");
                close.innerText = "x";
                close.addEventListener("click", removeToken, false)
                token.appendChild(token_span);
                token.appendChild(close);
                wrapper.insertBefore(token, search);
            }


            // Listen for clicks in the dropdown option
            function clickDropdown(e) {

                const dropdown = e.target;
                const wrapper = dropdown.parentNode.parentNode;
                const input_search = wrapper.querySelector(".selected-input");
                const select = wrapper.querySelector("select");
                dropdown.classList.toggle("active");

                if (dropdown.classList.contains("active")) {
                    const in_search = wrapper.querySelector(".selected-input");
                    in_search.removeAttribute("placeholder");
                    removePlaceholder(wrapper);
                    input_search.focus();

                    if (!input_search.value) {
                        populateAutocompleteList(select, "", true);
                    } else {
                        populateAutocompleteList(select, input_search.value);

                    }
                } else {
                    clearAutocompleteList(select);
                    addPlaceholder(wrapper);
                }
            }


            // Clears the results of the autocomplete list
            function clearAutocompleteList(select) {
                const wrapper = select.parentNode;

                const autocomplete_list = wrapper.querySelector(".autocomplete-list");
                autocomplete_list.innerHTML = "";
            }

            // Populate the autocomplete list following a given query from the user
            function populateAutocompleteList(select, query, dropdown = false) {
                const {
                    autocomplete_options
                } = getOptions(select);


                let options_to_show;

                if (dropdown)
                    options_to_show = autocomplete_options;
                else
                    options_to_show = autocomplete(query, autocomplete_options);

                const wrapper = select.parentNode;
                const input_search = wrapper.querySelector(".search-container");
                const autocomplete_list = wrapper.querySelector(".autocomplete-list");
                autocomplete_list.innerHTML = "";
                const result_size = options_to_show.length;

                if (result_size == 1) {

                    const li = document.createElement("li");
                    li.innerText = options_to_show[0];
                    li.setAttribute('data-value', options_to_show[0]);
                    li.addEventListener("click", selectOption);
                    autocomplete_list.appendChild(li);
                    if (query.length == options_to_show[0].length) {
                        const event = new Event('click');
                        li.dispatchEvent(event);

                    }
                } else if (result_size > 1) {

                    for (let i = 0; i < result_size; i++) {
                        const li = document.createElement("li");
                        li.innerText = options_to_show[i];
                        li.setAttribute('data-value', options_to_show[i]);
                        li.addEventListener("click", selectOption);
                        autocomplete_list.appendChild(li);
                    }
                } else {
                    const li = document.createElement("li");
                    li.classList.add("not-cursor");
                    li.innerText = "No options found";
                    autocomplete_list.appendChild(li);
                }
            }


            // Listener to autocomplete results when clicked set the selected property in the select option 
            function selectOption(e) {
                const wrapper = e.target.parentNode.parentNode.parentNode;
                const input_search = wrapper.querySelector(".selected-input");
                const option = wrapper.querySelector(`select option[value="${e.target.dataset.value}"]`);

                option.setAttribute("selected", "");
                createToken(wrapper, e.target.dataset.value);
                if (input_search.value) {
                    input_search.value = "";
                }

                input_search.focus();

                e.target.remove();
                const autocomplete_list = wrapper.querySelector(".autocomplete-list");


                if (!autocomplete_list.children.length) {
                    const li = document.createElement("li");
                    li.classList.add("not-cursor");
                    li.innerText = "No options found";
                    autocomplete_list.appendChild(li);
                }

                const event = new Event('keyup');
                input_search.dispatchEvent(event);
                e.stopPropagation();
            }


            //  that returns a list with the autcomplete list of matches
            function autocomplete(query, options) {
                // No query passed, just return entire list
                if (!query) {
                    return options;
                }
                let options_return = [];

                for (let i = 0; i < options.length; i++) {
                    if (query.toLowerCase() === options[i].slice(0, query.length).toLowerCase()) {
                        options_return.push(options[i]);
                    }
                }
                return options_return;
            }


            // Returns the options that are selected by the user and the ones that are not
            function getOptions(select) {
                // Select all the options available
                const all_options = Array.from(
                    select.querySelectorAll("option")
                ).map(el => el.value);

                // Get the options that are selected from the user
                const options_selected = Array.from(
                    select.querySelectorAll("option:checked")
                ).map(el => el.value);

                // Create an autocomplete options array with the options that are not selected by the user
                const autocomplete_options = [];
                all_options.forEach(option => {
                    if (!options_selected.includes(option)) {
                        autocomplete_options.push(option);
                    }
                });

                autocomplete_options.sort();

                return {
                    options_selected,
                    autocomplete_options
                };

            }

            // Listener for when the user wants to remove a given token.
            function removeToken(e) {

                e.preventDefault();
                e.stopPropagation();

                // Get the value to remove
                const value_to_remove = e.target.dataset.option;
                const wrapper = e.target.parentNode.parentNode;
                const input_search = wrapper.querySelector(".selected-input");
                const dropdown = wrapper.querySelector(".dropdown-icon");
                // Get the options in the select to be unselected
                const option_to_unselect = wrapper.querySelector(`select option[value="${value_to_remove}"]`);
                option_to_unselect.removeAttribute("selected");
                // Remove token attribute
                e.target.parentNode.remove();
                input_search.focus();
                dropdown.classList.remove("active");
                const event = new Event('click');
                dropdown.dispatchEvent(event);
            }

            // Listen for 2 sequence of hits on the delete key, if this happens delete the last token if exist
            function deletePressed(e) {
                const wrapper = e.target.parentNode.parentNode;
                const input_search = e.target;
                const key = e.keyCode || e.charCode;
                const tokens = wrapper.querySelectorAll(".selected-wrapper");

                if (tokens.length) {
                    const last_token_x = tokens[tokens.length - 1].querySelector("a");
                    let hits = +last_token_x.dataset.hits;

                    if (key == 8 || key == 46) {
                        if (!input_search.value) {

                            if (hits > 1) {
                                // Trigger delete event
                                const event = new Event('click');
                                last_token_x.dispatchEvent(event);
                            } else {
                                last_token_x.dataset.hits = 2;
                            }
                        }
                    } else {
                        last_token_x.dataset.hits = 0;
                    }
                }
                return true;
            }

            // You can call this  if you want to add new options to the select plugin
            // Target needs to be a unique identifier from the select you want to append new option for example #multi-select-plugin
            // Example of usage addOption("#multi-select-plugin", "tesla", "Tesla")
            function addOption(target, val, text) {
                const select = document.querySelector(target);
                let opt = document.createElement('option');
                opt.value = val;
                opt.innerHTML = text;
                select.appendChild(opt);
            }

        }




    }
var ExcelPlayground = new ClassExcelPlayground();
export default ExcelPlayground;
