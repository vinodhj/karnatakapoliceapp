//import environment from './environment.js';
  
const subdivision = {
    /**
     * Render the page content.
     */
    render: async () => {
      return https.loadHtml('subdivision');
    },
    /**
     * All the code related to DOM interactions and controls go in here.
     * This is a separate call as these can be registered only after the DOM has been painted.
     */
    after_render: async () => {

        $(document).ready(function () {  
      
            
            var grid_selector = "#grid-table";
            var pager_selector = "#grid-pager";              
            const tokenvalue = sessionStorage.getItem('token')
            $(grid_selector).jqGrid({
              url: environment.backendUrl + 'Department/deptmapping?name=Sub-Division',
              mtype: "GET",
              loadBeforeSend: function(jqXHR) {
                jqXHR.setRequestHeader("Authorization", "Bearer "+tokenvalue);
              },
              datatype: "json",
              height: 450,
              colNames:['id', 'name','Locations','Sub Divisions', 'parentId'],
              colModel:[
                {name:'id',index:'id',hidden: true},
                {name:'name',index:'name',hidden: true },
                {name:'parent.value',index:'value', width:300},  
                {name:'value',index:'value', width:300},
                {name:'parentId',index:'value', width:300, hidden: true}         
              ],
              loadonce: true,
              
              beforeSelectRow: function(rowid, e)
              {
                var selRowIds = $("#grid-table").jqGrid("getGridParam", "selarrrow");
                if(rowid != selRowIds[0]){
                  jQuery("#grid-table").jqGrid('resetSelection');
                }
                return(true);
              },
              rowNum:10,
              rowList:[10,20,30],
              pager : pager_selector,
              altRows: true,                                                 
              multiselect: true,
              multiboxonly: false,
            //   caption: "jqGrid with listing",
              emptyrecords: 'No Records Found',
              loadComplete : function() {
                  var table = this;
                  setTimeout(function(){
                        //updatePagerIcons(table);
                        $('#grid').trigger( 'reloadGrid' );
                  }, 0);
              }
            });
  
        })  ;

        $("#subAddForm").dialog({
            autoOpen : false, modal : true, show : "blind", hide : "blind",width:650
          });
        
          $("#openForm").click(function (e) {
            e.preventDefault();  
            e.stopPropagation();
            const tokenvalue = sessionStorage.getItem('token')
            $.ajax({  
                type: "GET",
                 url:environment.backendUrl+"Department/deptmapping?name=Place-Name",
                 beforeSend: function(xhr){xhr.setRequestHeader('Authorization', "Bearer "+tokenvalue);},
                  dataType: "json", 
                  contentType: "application/json",
                   success: function (res)  
                {  
                    $("#subaddlocations").empty();
                    $("#subaddlocations").append($("<option></option>").val(0).html("Select Division"));
                    $.each(res, function (data, value) {  
                        $("#subaddlocations").append($("<option></option>").val(value.id).html(value.value)); 
                    })  
                }  
  
            });
            $("#subAddForm").dialog("open");
          }); 
        $("#subsaveBtn").click(function(e) {
          e.preventDefault();  
          e.stopPropagation();
            $("#subAddForm").dialog("close");
            var getValue = $('#subdivisioninput').val();
            event.preventDefault();  
            const apiUrl = window.environment.backendUrl +'Department/savedepartment';
            const requestData = {
                id:0,
                name: "",
                value:getValue,
                levelId:2,
                parentId:$('#subaddlocations').val(),
                orderNo:0,
                isActive: true
              };
            https.postData(apiUrl, JSON.stringify(requestData))
              .then(function (response) {
                  // The API call was successful!
                  if (response.ok) {
                     $("#grid-table").trigger( 'reloadGrid' );

                    return response.json();
                  } else {
                    // return Promise.reject(response);
                  }
                }).then(function (data) {
                  // This is the JSON from our response
                  console.log(data);
                }).catch(function (err) {
                  // There was an error
                  console.warn('Something went wrong.', err);
                }); 
     
        });

        $("#subEditForm").dialog({
            autoOpen : false, modal : true, show : "blind", hide : "blind",width:650
          });

          $("#subeditopenForm").click(function (e) {
            e.preventDefault();  
            e.stopPropagation();
            const tokenvalue = sessionStorage.getItem('token')
            $.ajax({  
                type: "GET", 
                beforeSend: function(xhr){xhr.setRequestHeader('Authorization', "Bearer "+tokenvalue);},
                url:environment.backendUrl+"Department/deptmapping?name=Place-Name", 
                dataType: "json", 
                contentType: "application/json", 
                success: function (res)  
                {  
                    $("#locationspopup").empty();
                    $.each(res, function (data, value) {  
                        $("#locationspopup").append($("<option></option>").val(0).html("Select Division")); 
                    });
                    var rid = jQuery('#grid-table').jqGrid("getGridParam", "selrow");
            if (rid) {
                var row = jQuery('#grid-table').jqGrid("getRowData", rid);
                //$('#popupsubdivisioninput').val(row.value);     
               
               // $("#locationspopup").attr($("selected").val(row.value).html(row.value));   
                // $('#locationspopup[value=valueToSelect]', row.value).attr('selected', 'selected');  
                 $('#locationspopup [value="'+ row.parentId+'"]').attr("selected", "selected"); 
                    //$('#locationspopup option:eq('+ row.parentId+')').attr('selected', 'selected');
            }
                }  
            });
            $("#subEditForm").dialog("open");
            var rid = jQuery('#grid-table').jqGrid("getGridParam", "selrow");
            if (rid) {
                var row = jQuery('#grid-table').jqGrid("getRowData", rid);
                $('#popupsubdivisioninput').val(row.value);     
               
               // $("#locationspopup").attr($("selected").val(row.value).html(row.value));   
                // $('#locationspopup[value=valueToSelect]', row.value).attr('selected', 'selected');   
            }
          }); 
       
          $("#subsaveeditBtn").click(function(e) {
            e.preventDefault();  
            e.stopPropagation();
            $("#subEditForm").dialog("close");
            var getValue = $('#popupsubdivisioninput').val();
            var  idselection= $('#locationspopup').val();
            var rid = jQuery('#grid-table').jqGrid("getGridParam", "selrow");
            if (rid) {
                var row = jQuery('#grid-table').jqGrid("getRowData", rid);
             //   var id = row.id;
            }
            event.preventDefault();  
            const apiUrl = window.environment.backendUrl + 'Department/savedepartment';
            const requestData = {
                id:row.id,
                name: "",
                value:getValue,
                levelId:2,
                parentId:idselection,
                orderNo:0,
                isActive: true
              };
            https.postData(apiUrl, JSON.stringify(requestData))
              .then(function (response) {
                  // The API call was successful!
                  if (response.ok) {
                    $("#grid-table").trigger( 'reloadGrid' );

                    return response.json();
                  } else {
                    return Promise.reject(response);
                  }
                }).then(function (data) {
                  // This is the JSON from our response
                  console.log(data);
                }).catch(function (err) {
                  // There was an error
                  console.warn('Something went wrong.', err);
                }); 
     
        });
        
        $("#subdeleteForm").dialog({
            autoOpen : false, modal : true, show : "blind", hide : "blind"
          });

          $("#subdeleteopenForm").click(function () {
            $("#subdeleteForm").dialog("open");
            var rid = jQuery('#grid-table').jqGrid("getGridParam", "selrow");
            if (rid) {
                var row = jQuery('#grid-table').jqGrid("getRowData", rid);
                $('#subdeletelocationinput').val(row.value);                
            }
          }); 

          $("#subdeleteBtn").click(function(e) {
            e.preventDefault();  
            e.stopPropagation();
            $("#subdeleteForm").dialog("close");
            var rid = jQuery('#grid-table').jqGrid("getGridParam", "selrow");
            if (rid) {
                var row = jQuery('#grid-table').jqGrid("getRowData", rid);

            }
            event.preventDefault();  
            const apiUrl = window.environment.backendUrl +'Department/savedepartment';
            const requestData = {
                id:row.id,
                name: "",
                value:row.value,
                levelId:2,
                parentId:null,
                orderNo:0,
                isActive: false
              };
            https.postData(apiUrl, JSON.stringify(requestData))
              .then(function (response) {
                  // The API call was successful!
                  if (response.ok) {
                    $("#grid-table").trigger( 'reloadGrid' );

                    return response.json();
                  } 
                  else if(response.status==500){
                    alert("Entity Cannot Be Deleted");
                  }
                  
                  else {
                    return Promise.reject(response);
                  }
                }).then(function (data) {
                  // This is the JSON from our response
                  console.log(data);
                }).catch(function (err) {
                  // There was an error
                  console.warn('Something went wrong.', err);
                }); 
     
        });
       
    }
    
  };
  
  export default subdivision;
  