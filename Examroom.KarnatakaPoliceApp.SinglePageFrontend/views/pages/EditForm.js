const EditForm = {
  /**
   * Render the page content.
   */
  render: async () => {
    //         `
    //         function Say_Hello() {
    //             alert("Hello Poftut.com")
    //             }
    // `
    return https.loadHtml('editform');

  },


  after_render: async () => {


    $(document).ready(function (e) {

      fnLoadFormDetails();
      fnLoadData();

      $('#btnSaveAndSubmit').click(function(e){
        e.preventDefault();
        e.stopPropagation();

        var arr = [];
        var myform = document.getElementById('frameID');
        var inp = myform.getElementsByTagName('input');
        console.log(inp);
        for (let i = 0; i < inp[0].form.length; i++) {
          arr.push(inp[0].form[i].value);
        }
        console.log(JSON.stringify(arr));
        var formData = new FormData();

        var id = sessionStorage.getItem('formId');

        formData.append('formId', id);
        formData.append('content', JSON.stringify(arr));
        const apiUrl = window.environment.backendUrl + 'UserForm/submitdocform';
        $.ajax({
          type: 'post',
          url: apiUrl,
          data: formData,
          headers: { 'Authorization': 'Bearer ' + sessionStorage.getItem('token') },
          processData: false,
          contentType: false,
          enctype: 'multipart/form-data',
          error: function (response) {
            //displayResponseMessage(response);
            console.log(response);
          },
          success: function (response) {
            $('#lblStatus').text('Submitted');
            $('#btnSaveAndSubmit').hide();
            $('#savebtn').hide();
          },

        });

      });

      

      $('#btnBack').click(function(e){
        window.location.href = '#/forms';
    });

      $("#savebtn").click(function (e) {

        e.preventDefault();
        e.stopPropagation();

        var arr = [];
        var myform = document.getElementById('frameID');
        var inp = myform.getElementsByTagName('input');
        console.log(inp);
        for (let i = 0; i < inp[0].form.length; i++) {
          arr.push(inp[0].form[i].value);
        }
        console.log(JSON.stringify(arr));
        var formData = new FormData();

        var id = sessionStorage.getItem('formId');

        formData.append('formId', id);
        formData.append('content', JSON.stringify(arr));
        const apiUrl = window.environment.backendUrl + 'UserForm/updatedocform';
        $.ajax({
          type: 'post',
          url: apiUrl,
          data: formData,
          headers: { 'Authorization': 'Bearer ' + sessionStorage.getItem('token') },
          processData: false,
          contentType: false,
          enctype: 'multipart/form-data',
          error: function (response) {
            //displayResponseMessage(response);
            console.log(response);
          },
          success: function (response) {
            console.log(" Successfull");
          },

        });

      });

    });

    function fnLoadFormDetails(){
      
      let formId = sessionStorage.getItem('formId');
      let detailUrl = window.environment.backendUrl + `UserForm/formdetail?formId=${formId}`;
            let config = { headers: {'Authorization': 'Bearer ' + sessionStorage.getItem('token')}};
            
            axios
            .get(detailUrl, config)
            .then(res=> {
      
               // $('#savebtn').hide();

                console.log(res.data);

                $('#lblName').text(res.data.formName);
                $('#lblStatus').text(res.data.status);

                if(res.data.status == 'Draft' || res.data.status == 'Rejected'){
                    $('#btnSubmit').show();
                }
                
            });
    }

    function fnLoadData() {

      var formId = sessionStorage.getItem('formId');
      var url = window.environment.backendUrl + `UserForm/docform?id=${formId}`;

      $.ajax({
        type: 'get',
        url: url,
        success: function (response) {
          $('#frameID').html(response);
          

          const apiUrl = window.environment.backendUrl + `UserForm/userinput?id=${formId}`;

          //var a = JSON.parse("[\"sndcj\",\"\",\"\",\"dsncj\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\"]");
          $.ajax({
            type: 'get',
            url: apiUrl,
            headers: { 'Authorization': 'Bearer ' + sessionStorage.getItem('token') },
            error: function (response) {
              //displayResponseMessage(response);
              console.log(response);
            },
            success: function (response) {
              console.log(" Successfull");
              var a=(response);
              var myform = document.getElementById('frameID');
              var inp = myform.getElementsByTagName('input');
              // console.log(inp);
              for (let i = 0; i < inp[0].form.length; i++) {
                //  arr.push(inp[0].form[i].value);
                inp[0].form[i].value = a[i];
              }
             
              console.log('<<', myform.innerHTML);

            },

          });

          //  var a = JSON.parse(response.content);
         

         
         
        }, error: function (err) {
          alert(err.error);
        }
      });
    }

  }


};
export default EditForm;