const ViewForms = {
    async render() {
        return https.loadHtml('viewform');
    },
    after_render: async () => {

        $(document).ready(function () {

            // load forms on page-load
            fnLoadForms(); 
            fnLoadTemplateDropdown();

            $("#AddForm").dialog({
                autoOpen: false, modal: true, show: "blind", hide: "blind"
            });

            $("#openForm").click(function (e) {
                e.preventDefault();  
                e.stopPropagation();
                $("#AddForm").dialog("open");
            });

            $("#saveBtn").click(function (e) {

                e.preventDefault();  
                e.stopPropagation();

                $("#AddTemplate").dialog("close");
                var SelTemplate = $("#template").val();
                var name = $('#nameinput').val();
                var formData = new FormData();
                formData.append('name', name);
                formData.append('templateId', SelTemplate);
                //formData.append('file', filenameinput.files[0])
                const apiUrl = window.environment.backendUrl + 'UserForm/addform';

                $.ajax({
                    type: 'post',
                    url: apiUrl,
                    data: formData,
                    headers: { 'Authorization': 'Bearer ' + sessionStorage.getItem('token') },
                    processData: false,
                    contentType: false,
                    enctype: 'multipart/form-data',
                    error: function (response) {
                        //displayResponseMessage(response);
                        //fnLoadForms();
                        //logic to close the form
                    },
                    success: function (response) {
                        $("#AddTemplate").dialog("close");
                        fnLoadForms();
                        console.log(" Successfull");
                    },
                });
            });
        });


        function fnLoadForms() {
            let url = window.environment.backendUrl + 'UserForm/forms';
            let role = sessionStorage.getItem('roleName');

            console.log(role);
            let editBtnText = 'Edit';
            if(role == 'SuperAdmin')
                editBtnText = 'Review';

            $.ajax({
                type: "GET",
                url: url,
                contentType: 'application/json',
                headers: { 'Authorization': 'Bearer ' + sessionStorage.getItem('token') },
                dataType: 'json',
                success: function (response) {
                    var output = '';
                    response.forEach(item => {
                        output += `<tr>
                        <td>${item.id}</td>
                        <td>${item.templateName}</td>
                        <td>${item.templateType}</td>
                        <td>${item.policeStationName}</td>
                        <td>${item.formName}</td>
                        <td>${item.status}</td>
                        <td>${item.createdBy}</td>
                        <td>${item.createdOn}</td>
                        <td>${item.modifiedBy}</td>
                        <td>${item.modifiedOn}</td>
                        <td><button class='grid-lnk' id='lnk-${item.templateType}-${item.id}'>${editBtnText}</a></td>
                    </tr>`;

                    });

                    document.getElementById('tblBody').innerHTML = output;
                },
                error: function (error) {
                    console.log(error);
                },
                dataType: 'json'
            });
        }

        document.addEventListener('click', function (e) {

            e.stopPropagation();

            if (e.target.id.startsWith('lnk-')) {

                let role = sessionStorage.getItem('roleName');

                let id = ''
                if (e.target.id.startsWith('lnk-Static-')) {
                    id = e.target.id.replace('lnk-Static-', '');
                    sessionStorage.setItem('formId', id);
                    if(role == 'SuperAdmin')
                        window.location.href = '#/reviewdocform';
                    else
                        window.location.href = '#/editform';

                }
                else {
                    id = e.target.id.replace('lnk-Dynamic-', '');
                    sessionStorage.setItem('formId', id);
                    if(role == 'SuperAdmin')
                        window.location.href = '#/reviewxlform';
                    else
                        window.location.href = '#/editxlform';
                }
            }
        });

        function fnLoadTemplateDropdown() {

            $.ajax({
                method: "get",
                url: window.environment.backendUrl + 'UserForm/templates',
                headers: { 'Authorization': 'Bearer ' + sessionStorage.getItem('token') },
                async: true,
                success: function (result) {
                    var str = document.getElementById('template');
                    /* dropdownObject = new MultiSelectDropdown();
                    console.log(dropdownObject); */
                    for (var i = 0; i < result.length; i++) {
                        str.innerHTML = str.innerHTML + '<option value=' + result[i].id + '>' + result[i].name + '</option>';
                    }


                }
            });
        }
        // $("#template").change(function(){
        //     var templateId = $("#template").val();
        //    // console.log("Selected Departmet "+ SelDept);
        // });
    }

};
export default ViewForms;