const ViewTemplate = {
    /**
     * Render the page content.
     */
    render: async() => {
        return https.loadHtml('viewtemplate');
    },

    after_render: async() => {

        $(document).ready(function() {


            var templateId = sessionStorage.getItem('templateId');
            var url = window.environment.backendUrl + `UserForm/doctemplate?id=${templateId}`;

            $.ajax({
                type: 'get',
                url: url,
                success: function(response) {
                    $('#frameID').html(response);
                },
                error: function(err) {
                    alert(err.error);
                }
            });

            // const response = await axios.get(url, {'Authorization' : 'Bearer ' + sessionStorage.getItem('token')});
            // const iframe = document.getElementById('frameID');
            // iframe.innerHTML = (response.data);

        });


        /*    $("#savebtn").click(function () {
           
      
       
            event.preventDefault();
            var formData = new FormData();
           // formData.append('name', name);
          var file=new File([iframe.innerHTML],{type:'application/html'});
           console.log(file);
            formData.append('formId', id);
            formData.append('file', file)
            const apiUrl =window.environment.backendUrl + 'UserForm/updateform';
            $.ajax({
                type:'post',
                url: apiUrl,
                data: formData,
                processData: false,
                contentType: false,
                enctype: 'multipart/form-data',
                error: function(response) {
                  //displayResponseMessage(response);
                  console.log(response);
                },
                success: function(response) {
                  console.log(" Successfull");
                },
              });
          
        });
 */
    }


};
export default ViewTemplate;