//import MultiSelectDropdown from "./MultiSelectDropdown.js";
const Templates = {
    /**
     * Render the page content.
     */
    async render() {
        return https.loadHtml('templates');
    },
    /**
     * All the code related to DOM interactions and controls go in here.
     * This is a separate call as these can be registered only after the DOM has been painted.
     */

    after_render: async () => {

        $(document).ready(function () {

            $('#place').change(function () {
                getSubDivisionsDropdown();
            });

            $('#subdev').change(function (e) {

                
               getPoliceStationDropdown();
            });

            let url =window.environment.backendUrl + 'UserForm/templates';

            $.ajax({
                type: "GET",
                url: url,
                contentType: 'application/json',
                headers: { 'Authorization': 'Bearer ' + sessionStorage.getItem('token') },
                dataType: 'json',
                success: function (response) {
                    var output = '';
                    response.forEach(item => {

                        var btnName = 'View';

                        if(item.templateType == 'Dynamic')
                            btnName = 'Edit';

                        output += `<tr>
                        <td>${item.id}</td>
                        <td>${item.name}</td>
                        <td>${item.templateType}</td>
                        <td>${item.createdBy}</td>
                        <td>${item.createdOn}</td>
                        <td>${item.modifiedBy}</td>
                        <td>${item.modifiedOn}</td>
                        <td><button class='grid-lnk' title='${btnName} Template' id='lnk-${item.id}'>${btnName}</a></td>
                    </tr>`;

                    });

                    document.getElementById('tblBody').innerHTML = output;
                },
                error: function (error) {
                    console.log(error);
                },
                dataType: 'json'
            });

        });

        function getStrArray(arr){
            let strArr = '';

            for(let i=0;i<arr.length; i++){
                strArr+=',' + arr[i];
            }

            return strArr.substring(1, strArr.length);
        }

        function getSubDivisionsDropdown() {
            var selectedValues = getStrArray($('#place').val())
            //console.log(selectedValues);
            $.ajax({
                method: "get",
                headers: { 'Authorization': 'Bearer ' + sessionStorage.getItem('token') },
                url: window.environment.backendUrl + 'Department/getdepartmentdetails?levelId=2&parentIds=' + selectedValues,
                async: true,
                success: function (result) {
                    var str = document.getElementById('subdev');
                    str.innerHTML = '';
                    /* dropdownObject = new MultiSelectDropdown();
                    console.log(dropdownObject); */
                    for (var i = 0; i < result.length; i++) {
                        str.innerHTML = str.innerHTML + '<option value=' + result[i].id + '>' + result[i].value + '</option>';
                    }

                }
            });

        }

        function getPoliceStationDropdown() {
            var parentIds = getStrArray( $('#subdev').val());
            //console.log(parentIds);
            $.ajax({
                method: "get",
                headers: { 'Authorization': 'Bearer ' + sessionStorage.getItem('token') },
                url: window.environment.backendUrl +'Department/getdepartmentdetails?levelId=3&parentIds=' + parentIds,
                async: true,
                success: function (result) {
                    var str = document.getElementById('dept');
                    str.innerHTML = '';
                    /* dropdownObject = new MultiSelectDropdown();
                    console.log(dropdownObject); */
                    for (var i = 0; i < result.length; i++) {
                        str.innerHTML = str.innerHTML + '<option value=' + result[i].id + '>' + result[i].value + '</option>';
                    }

                }
            });

        }
     
        $.ajax({
            method: "get",
            headers: { 'Authorization': 'Bearer ' + sessionStorage.getItem('token') },
            url: window.environment.backendUrl +'Department/deptmapping?name=Place-Name',
            async: true,
            success: function (result) {
                var str = document.getElementById('place');
                /* dropdownObject = new MultiSelectDropdown();
                console.log(dropdownObject); */
                for (var i = 0; i < result.length; i++) {
                    str.innerHTML = str.innerHTML + '<option value=' + result[i].id + '>' + result[i].value + '</option>';
                }
            
                
            }
        });


        document.addEventListener('click', function (e) {

            //e.preventDefault();
            e.stopPropagation();

            if (e.target.id.startsWith('lnk-')) {
                var id = e.target.id.replace('lnk-', '');
                var action = e.target.innerHTML;

                if(action == 'Edit'){
                    
                    sessionStorage.setItem('templateId', id);
                    window.location.href = '#/editxltemplate';
                     window.location.reload(true);
                    

                }
                else{
                    sessionStorage.setItem('templateId', id);

                    window.location.href = '#/viewtemplate' ;
                }
                //window.location.href = '#/viewtemplate/' + id;
            }
        });
        $("#AddTemplate").dialog({
            autoOpen: false, modal: true, show: "blind", hide: "blind", height : 600, width:600
        });

        $("#openForm").click(function () {
            $("#AddTemplate").dialog("open");
        });

        $("#place").change(function(){
            var selValue = $("#place").val();
            console.log("Selected Place "+ selValue);
        });
        $("#subdev").change(function(){
            var selValue = $("#subdev").val();
            console.log("Selected Sub Devision "+ selValue);
        });
        $("#dept").change(function(){
            var SelDept = $("#dept").val();
            console.log("Selected Departmet "+ SelDept);
        });

        $("#saveBtn").click(function (e) {

            e.preventDefault();  
            e.stopPropagation();
           
            $("#AddTemplate").dialog("close");
            var SelDept = $("#dept").val();
            console.log("Selected Departmet "+ SelDept);
            var name = $('#name').val();
           
            var formData = new FormData();
           // formData.append('name', name);
            formData.append('departmentIds', SelDept);
            formData.append('file', filenameinput.files[0])
            const apiUrl = window.environment.backendUrl +'UserForm/addtemplate';
            $.ajax({
                type:'post',
                url: apiUrl,
                headers: { 'Authorization': 'Bearer ' + sessionStorage.getItem('token') },
                data: formData,
                processData: false,
                contentType: false,
                enctype: 'multipart/form-data',
                error: function(response) {
                  //displayResponseMessage(response);
                  console.log(response);
                },
                success: function(response) {
                  console.log(" Successfull");
                },
              });
              
        });

    }

};
export default Templates;