//import environment from './environment.js';

const locations = {
    /**
     * Render the page content.
     */
    render: async () => {
      var grid_selector = "#grid-table";
      $("#cb_"+grid_selector[0].id).hide();
      
      
      // Map over items and build card components.
      return https.loadHtml('locations');
    },
    /**
     * All the code related to DOM interactions and controls go in here.
     * This is a separate call as these can be registered only after the DOM has been painted.
     */
    after_render: async () => {

      $(document).ready(function(){
        var grid_selector = "#grid-table";
        var pager_selector = "#grid-pager";

        $("#cb_"+grid_selector[0].id).hide();

        const tokenvalue = sessionStorage.getItem('token')



        $('.edit-location').click(function(e){
        });


        $(grid_selector).jqGrid({
          url: environment.backendUrl + 'Department/deptmapping?name=Place-Name',
          mtype: "GET",
          jsonReader : {
            root : "response",
            page : "page",
            total : "total",
            records : "records",
            repeatitems : false,
            id : "id"
        },
          loadBeforeSend: function(jqXHR) {
            jqXHR.setRequestHeader("Authorization", "Bearer "+tokenvalue);
          },
          datatype: "json",
          height: 330,
          colNames:['value','Division'],
          colModel:[
          {name:'id',index:'value', width:95,hidden:true},
          {name:'value',index:'value', width:600},
          //{name:'body',index:'body', width:400, sortable:false}
            // {name:'value',index:'value', width:600},

          ],
          //loadonce: true,
          rowNum : 10,
          rowList:[10,20,30],
          pager : pager_selector,
          viewrecords : true,
          gridview : true,
          // scroll:1,
	        loadonce:true,
          altRows: true,
          multiselect: true,
          multiboxonly: false,
          onSelectRow: function (id) {           
            var rid = jQuery('#grid-table').jqGrid("getGridParam", "selrow");
            if (rid) {
                var row = jQuery('#grid-table').jqGrid("getRowData", rid);
                //  alert(row.name+", "+row.value+", "+row.id);
            }
        },

          // cmTemplate: { title: false },
          // gridComplete: function(){
          //   var ids = jQuery(grid_selector).jqGrid('getDataIDs');
          //   for(var i=0;i < ids.length;i++){
          //     var cl = ids[i];
          //     var editBtn = `<input type='button' class='edit-location'  value='E' id='btn-edit-region-${cl}'/>`;
          //     console.log(editBtn);
          //     jQuery(grid_selector).jqGrid('setRowData',ids[i],{act:editBtn});
          //   }
          // },
          // // caption: "jqGrid with listing",
          // emptyrecords: 'No Records Found',
        beforeSelectRow: function(rowid, e)
    {
      var selRowIds = $("#grid-table").jqGrid("getGridParam", "selarrrow");
            if(rowid != selRowIds[0]){
              jQuery("#grid-table").jqGrid('resetSelection');
            }
        // This code resets the checkbox when another is selected
        // BUT, it is causing problems with the Cell editing when another row is selected
        //jQuery("#grid-table").jqGrid('resetSelection');
        return(true);
    },
          loadComplete : function() {
              var table = this;
              setTimeout(function(){
                    //updatePagerIcons(table);
                    $('#grid').trigger( 'reloadGrid' );
              }, 0);
          }
        });

        // var editLocation = function(id) {
        //   alert(id);
        //   $("#EditForm").dialog("open");
        //   var rid = jQuery('#grid-table').jqGrid("getGridParam", "selrow");
        //   if (rid) {
        //       var row = jQuery('#grid-table').jqGrid("getRowData", rid);
        //       $('#editlocationinput').val(row.value);
        //       // alert(row.name+", "+row.value+", "+row.id);

        //   }
        // }

        $("#AddForm").dialog({
          autoOpen : false, modal : true, show : "blind", hide : "blind"
        });

        $("#openForm").click(function () {
          $("#AddForm").dialog("open");
        });
        $("#savelocationBtn").click(function(e) {
          e.preventDefault();  
          e.stopPropagation();
            $("#AddForm").dialog("close");
            var getValue = $('#locationinput').val();
            event.preventDefault();
            const apiUrl = window.environment.backendUrl + 'Department/savedepartment';
            const requestData = {
                id:0,
                name: "",
                value:getValue,
                levelId:1,
                parentId:null,
                orderNo:0,
                isActive: true
              };
            https.postData(apiUrl, JSON.stringify(requestData))
              .then(function (response) {
                  // The API call was successful!
                  if (response.ok) {
                    $("#grid-table").trigger( 'reloadGrid' );

                    return response.json();
                  } else {
                    return Promise.reject(response);
                  }
                }).then(function (data) {
                  // This is the JSON from our response
                  console.log(data);
                }).catch(function (err) {
                  // There was an error
                  console.warn('Something went wrong.', err);
                });

        });

        $("#EditForm").dialog({
            autoOpen : false, modal : true, show : "blind", hide : "blind"
          });

          $("#editopenForm").click(function (e) {
            e.preventDefault();  
            e.stopPropagation();
            $("#EditForm").dialog("open");
            var rid = jQuery('#grid-table').jqGrid("getGridParam", "selrow");
            if (rid) {
                var row = jQuery('#grid-table').jqGrid("getRowData", rid);
                $('#editlocationinput').val(row.value);
                // alert(row.name+", "+row.value+", "+row.id);

            }
          });

          $("#saveeditBtn").click(function(e) {
            e.preventDefault();  
            e.stopPropagation();
            $("#EditForm").dialog("close");
            var getValue = $('#editlocationinput').val();
            var rid = jQuery('#grid-table').jqGrid("getGridParam", "selrow");
            if (rid) {
                var row = jQuery('#grid-table').jqGrid("getRowData", rid);
                var id = row.id;
            }
            event.preventDefault();
            const apiUrl = window.environment.backendUrl +'Department/savedepartment';
            const requestData = {
                id:row.id,
                name: "",
                value:getValue,
                levelId:1,
                parentId:null,
                orderNo:0,
                isActive: true
              };
            https.postData(apiUrl, JSON.stringify(requestData))
              .then(function (response) {
                  // The API call was successful!
                  if (response.ok) {
                    $("#grid-table").trigger( 'reloadGrid' );

                    return response.json();
                  } else {
                    return Promise.reject(response);
                  }
                }).then(function (data) {
                  // This is the JSON from our response
                  console.log(data);
                }).catch(function (err) {
                  // There was an error
                  console.warn('Something went wrong.', err);
                });

        });

        $("#deleteForm").dialog({
            autoOpen : false, modal : true, show : "blind", hide : "blind"
          });

          $("#deleteopenForm").click(function (e) {
            e.preventDefault();  
            e.stopPropagation();
            $("#deleteForm").dialog("open");
            var rid = jQuery('#grid-table').jqGrid("getGridParam", "selrow");
            if (rid) {
                var row = jQuery('#grid-table').jqGrid("getRowData", rid);

                $('#deletelocationinput').val(row.value);
            }
          });

          $("#deleteBtn").click(function(e) {
            e.preventDefault();  
            e.stopPropagation();
            $("#deleteForm").dialog("close");
            var rid = jQuery('#grid-table').jqGrid("getGridParam", "selrow");
            if (rid) {
                var row = jQuery('#grid-table').jqGrid("getRowData", rid);

            }
            event.preventDefault();
            const apiUrl = window.environment.backendUrl + 'Department/savedepartment';
            const requestData = {
                id:row.id,
                name: "",
                value:row.value,
                levelId:1,
                parentId:null,
                orderNo:0,
                isActive: false
              };
            https.postData(apiUrl, JSON.stringify(requestData))
              .then(function (response) {
                  // The API call was successful!
                  if (response.ok) {
                    $("#grid-table").trigger( 'reloadGrid' );

                    return response.json();
                  }
                  else if(response.status==500){
                    alert("Entity Cannot Be Deleted");
                  }

                  else {
                    return Promise.reject(response);
                  }
                }).then(function (data) {
                  // This is the JSON from our response
                  console.log(data);
                }).catch(function (err) {
                  // There was an error
                  console.warn('Something went wrong.', err);
                });

        });


      });
    }
  };

  export default locations;
