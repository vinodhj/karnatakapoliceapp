//import environment from './environment.js';
  
const usermanagement = {
    /**
     * Render the page content.
     */
    render: async () => {
      return https.loadHtml('usermanagement');
    },
    /**
     * All the code related to DOM interactions and controls go in here.
     * This is a separate call as these can be registered only after the DOM has been painted.
     */
    after_render: async () => {

        $(document).ready(function () {  
      
         // displayUsers();
            
            var grid_selector = "#usergrid-table";
            var pager_selector = "#usergrid-pager";              
            const tokenvalue = sessionStorage.getItem('token')
            $(grid_selector).jqGrid({
              url: environment.backendUrl + 'User/getusers',
              mtype: "GET",
              loadBeforeSend: function(jqXHR) {
                jqXHR.setRequestHeader("Authorization", "Bearer "+tokenvalue);
            },
              datatype: "json",
              height: 450,
              colNames:['id', 'User Name','Police Station','Sub Division','Location','firstname','lastname','email','phone','userRoleid','userToDepartmentMappingid','locid','subid','policeid'],
              colModel:[
                {name:'id',index:'id',hidden: true, width:55},
                {name:'username',index:'name', width:95},
                {name:'userToDepartmentMapping.0.policeStation.value',index:'id', width:300},    
                {name:'userToDepartmentMapping.0.policeStation.parent.value',index:'value', width:300},
                {name:'userToDepartmentMapping.0.policeStation.parent.parent.value',index:'value', width:300}, 
                {name:'userToDepartmentMapping.0.policeStation.parent.parent.id',index:'value', width:300,hidden: true}, 
                {name:'userToDepartmentMapping.0.policeStation.parent.id',index:'value', width:300,hidden: true},
                {name:'userToDepartmentMapping.0.policeStation.id',index:'id', width:300,hidden: true}, 
                {name:'firstname',index:'id',hidden: true, width:55},
                {name:'lastname',index:'id',hidden: true, width:55},
                {name:'emailId',index:'id',hidden: true, width:55},
                {name:'phone',index:'id',hidden: true, width:55},
                {name:'userRole.0.roleId',index:'id',hidden: true, width:55},
                {name:'userToDepartmentMapping.0.id',index:'id',hidden: true, width:55},

                //id   
                // {name:'userToDepartmentMapping.0.policeStation.parent.id',index:'value', width:300,hidden: true},
                // {name:'userToDepartmentMapping.0.policeStation.parent.parent.id',index:'value', width:300,hidden: true},  
              ],
              loadonce: true,
              beforeSelectRow: function(rowid, e)
              {
                var selRowIds = $("#usergrid-table").jqGrid("getGridParam", "selarrrow");
                if(rowid != selRowIds[0]){
                  jQuery("#usergrid-table").jqGrid('resetSelection');
                }
                return(true);
              },
              rowNum:10,
              rowList:[10,20,30],
              pager : pager_selector,
              altRows: true,                                                 
              multiselect: true,
              multiboxonly: false,
            //   caption: "jqGrid with listing",
              emptyrecords: 'No Records Found',
              loadComplete : function() {
                  var table = this;
                  setTimeout(function(){
                        //updatePagerIcons(table);
                        $('#grid').trigger( 'reloadGrid' );
                  }, 0);
              }
            });
  
        })  ;


        $("#userstationAddForm").dialog({
            autoOpen : false, modal : true, show : "blind", hide : "blind",height : 500, width:750
          });
                      
          $("#users").unbind('change').change(function(event){
            event.stopPropagation();
            event.preventDefault();
            var userroleselected=$("#users").val();
            if(userroleselected==1){
                $("#userpolicelocations").hide();
                $("#userpolicesubdivisions").hide();
                $("#userpolicesStations").hide();
                $("#regionlabel").hide();
                $("#sublabel").hide();
                $("#policelabel").hide();          
            }
            else {
                $("#userpolicelocations").show();
                $("#userpolicesubdivisions").show();
                $("#userpolicesStations").show();
                $("#regionlabel").show();
                $("#sublabel").show();
                $("#policelabel").show();
            }

            
          });
        
          $("#openForm").click(function () {
            const tokenvalue = sessionStorage.getItem('token')
            $("#users").empty();
            $("#users").append($("<option></option>").val(0).html("Select Role")); 
            $("#users").append($("<option></option>").val(1).html("Super Admin")); 
            $("#users").append($("<option></option>").val(2).html("Admin")); 

            
            

            
            $.ajax({  
                type: "GET", 
                beforeSend: function(xhr){xhr.setRequestHeader('Authorization', "Bearer "+tokenvalue);},
                url:environment.backendUrl+"Department/deptmapping?name=Place-Name",
                 dataType: "json", 
                 contentType: "application/json", 
                 success: function (res)  
                {  
                    $("#userpolicelocations").empty();
                    $("#userpolicesubdivisions").empty();
                    $("#userpolicesStations").empty();

                    $("#userpolicelocations").append($("<option></option>").val(0).html("Select Location")); 
                    $("#userpolicesubdivisions").append($("<option></option>").val(0).html("Select Sub Division")); 
                    $("#userpolicesStations").append($("<option></option>").val(0).html("Select PoliceStation")); 
                    $.each(res, function (data, value) {  
                        $("#userpolicelocations").append($("<option></option>").val(value.id).html(value.value)); 
                    })  
                }  
  
            });
            $("#userstationAddForm").dialog("open");
          }); 
          $("#openForm").click(function () {

            $("#confirmpassword").on('keyup', function(){
              var password = $("#password").val();
              var confirmPassword = $("#confirmpassword").val();
              if (password != confirmPassword){
                $("#CheckPasswordMatch").html("Password does not match !").css("color","red");
                $("#usersaveBtn").prop('disabled',true);
              }
               
              else{
                $("#CheckPasswordMatch").html("Password match !").css("color","green");
                $("#usersaveBtn").prop('disabled',false);
              }
           
             });
            const tokenvalue = sessionStorage.getItem('token')
            $.ajax({  
                type: "GET", 
                url:environment.backendUrl+"Department/deptmapping?name=Sub-Division", 
                dataType: "json",
                beforeSend: function(xhr){xhr.setRequestHeader('Authorization', "Bearer "+tokenvalue);},
                 contentType: "application/json", 
                success: function (res)  
                {  
                    
                    var selected = $("#userpolicelocations").val();
                    $.subdivisions = res;
                    $.each(res, function (data, value) {  
                        
                        //   $("#userpolicesubdivisions").empty();
                            if(selected==value.parentId){
                                $("#userpolicesubdivisions").append($("<option></option>").val(value.id).html(value.value)); 
                            }                        
                    })  
                }  
  
            });
            $("#userpolicelocations").unbind('change').change(function(event){
                event.stopPropagation();
                event.preventDefault();
                var selected=$("#userpolicelocations").val();
                $("#userpolicesubdivisions").empty();
                $.each($.subdivisions, function (data, value) {  
                        
                      if(selected==value.parentId){
                          $("#userpolicesubdivisions").append($("<option></option>").val(value.id).html(value.value)); 
                      }


                  
              })  
              var selected=$("#userpolicesubdivisions").val();
              $("#userpolicesStations").empty();
              $.each($.policestations, function (data, value) {  
                      
                    if(selected==value.parentId){
                        $("#userpolicesStations").append($("<option></option>").val(value.id).html(value.value)); 
                    }                 
            })  

                
              });
              $.ajax({  
                type: "GET",
                beforeSend: function(xhr){xhr.setRequestHeader('Authorization', "Bearer "+tokenvalue);},
                 url:environment.backendUrl+"Department/deptmapping?name=Police-Station",
                  dataType: "json",
                   contentType: "application/json", 
                   success: function (res)  
                {  
                    //$("#userpolicesStations").empty();
                    var selected = $("#userpolicelocations").val();
                    $.policestations = res;
                    $.each(res, function (data, value) {  
                        
                        //   $("#userpolicesubdivisions").empty();
                            if(selected==value.parentId){
                                $("#userpolicesubdivisions").append($("<option></option>").val(value.id).html(value.value)); 
                            }                        
                    })  
                }  
  
            });


              $("#userpolicesubdivisions").unbind('change').change(function(event){
                $("#userpolicesubdivisions").append($("<option></option>").val(0).html("")); 
                event.stopPropagation();
                event.preventDefault();
                var selected=$("#userpolicesubdivisions").val();
                $("#userpolicesStations").empty();
                $.each($.policestations, function (data, value) {  
                        
                      if(selected==value.parentId){
                          $("#userpolicesStations").append($("<option></option>").val(value.id).html(value.value)); 
                      }                 
              })  

                
              });
            $("#userstationAddForm").dialog("open");


          }); 

 
         
        $("#usersaveBtn").click(function(e) {
            $("#userstationAddForm").dialog("close");
            var getuserValue = $('#userinput').val();
            var getfirstnameValue = $('#firstnameinput').val();
            var getlastValue = $('#lastnameinput').val();
            var getpasswordValue = $('#password').val();
            var getemailValue = $('#emailid').val();
            var getphoneValue = $('#phone').val();
            var stationselected=$("#userpolicesStations").val();
            var userroleselected=$("#users").val();
            e.preventDefault();  
            e.stopPropagation();
            const apiUrl = window.environment.backendUrl + 'User/saveuser';
            const requestData = {
                id: 0,
                userRoleId: 0,
                roleId:userroleselected,
                username: getuserValue,
                firstname:getfirstnameValue,
                lastname:getlastValue,
                password: getpasswordValue,
                emailId:getemailValue,
                phone: getphoneValue,
                token: "string",
                policeStationId: stationselected,
                userRole: [
                  {
                    id: 0,
                    userId: 0,
                    roleId: userroleselected,
                    isActive: true
                  }
                ]
              };
            https.postData(apiUrl, JSON.stringify(requestData))
              .then(function (response) {
                  // The API call was successful!
                  if (response.ok) {
                        $("#usergrid-table").trigger( 'reloadGrid' );
                        
                    return response.json();
                
                  } else {
                    // return Promise.reject(response);
                  }
                }).then(function (data) {
                  displayUsers();
                  // This is the JSON from our response
                  console.log(data);
                }).catch(function (err) {
                  // There was an error
                  console.warn('Something went wrong.', err);
                }); 
     
        });


        /////edit form

        $("#userEditForm").dialog({
            autoOpen : false, modal : true, show : "blind", hide : "blind",height : 500, width:750
          });
          $("#userEditPassworrdForm").dialog({
            autoOpen : false, modal : true, show : "blind", hide : "blind",height : 300, width:500
          });


          $("#editusers").unbind('change').change(function(event){
            event.stopPropagation();
            event.preventDefault();
            var userroleselected=$("#editusers").val();
            if(userroleselected==1){
                $("#locationspopupuser").hide();
                $("#subdivisionspopupuser").hide();
                $("#userpolicesStations").hide();
                $("#policeStationpopupuser").hide();
                $("#regionlabeledit").hide();
                $("#subdiveditlabel").hide();
                $("#policeeditlabel").hide();
                
                
                
                
            }
            else {
                $("#locationspopupuser").show();
                $("#subdivisionspopupuser").show();
                $("#userpolicesStations").show();
                $("#policeStationpopupuser").show();
                $("#regionlabeledit").show();
                $("#subdiveditlabel").show();
                $("#policeeditlabel").show();
            }

            
          });
          
          $("#usereditopenForm").click(function () {
            const tokenvalue = sessionStorage.getItem('token')
            $("#editusers").empty();
            $("#editusers").append($("<option></option>").val(0).html("Select Role")); 
            $("#editusers").append($("<option></option>").val(1).html("Super Admin")); 
            $("#editusers").append($("<option></option>").val(2).html("Admin")); 
            $.ajax({  
                type: "GET", 
                
                beforeSend: function(xhr){xhr.setRequestHeader('Authorization', "Bearer "+tokenvalue);},
                url:environment.backendUrl+"Department/deptmapping?name=Place-Name", 
                dataType: "json", 
                contentType: "application/json", 
                success: function (res)  
                {  
                    $("#locationspopupuser").empty();
                    $.each(res, function (data, value) {  
                        $("#locationspopupuser").append($("<option></option>").val(value.id).html(value.value)); 
                    });

                }  
            });
            $.ajax({  
                type: "GET", 
                beforeSend: function(xhr){xhr.setRequestHeader('Authorization', "Bearer "+tokenvalue);},
                url:environment.backendUrl+"Department/deptmapping?name=Sub-Division", 
                dataType: "json", contentType: "application/json", 
                success: function (res)  
                {  
                    $("#subdivisionspopupuser").empty();
                    $.each(res, function (data, value) {  
                        $("#subdivisionspopupuser").append($("<option></option>").val(value.id).html(value.value)); 
                    })  
                }  
            });
            $.ajax({  
                type: "GET", 
                url:environment.backendUrl+"Department/deptmapping?name=Police-Station", 
                beforeSend: function(xhr){xhr.setRequestHeader('Authorization', "Bearer "+tokenvalue);},
                dataType: "json",
                 contentType: "application/json", 
                 success: function (res)  
                {  
                    $("#policeStationpopupuser").empty();
                    $.each(res, function (data, value) {  
                        $("#policeStationpopupuser").append($("<option></option>").val(value.id).html(value.value)); 
                    })  

                    var rid = jQuery('#usergrid-table').jqGrid("getGridParam", "selrow");
                    if (rid) {
                        var row = jQuery('#usergrid-table').jqGrid("getRowData", rid);
                       // $('#locationspopupuser [value="'+row["'userToDepartmentMapping.0.policeStation.value'"]+'"]').attr("selected", "selected"); 
                        $('#locationspopupuser [value="'+ row["userToDepartmentMapping.0.policeStation.parent.parent.id"]+'"]').attr("selected", "selected"); 
                        $('#subdivisionspopupuser [value="'+row["userToDepartmentMapping.0.policeStation.parent.id"]+'"]').attr("selected", "selected");  
                        $('#policeStationpopupuser [value="'+row["userToDepartmentMapping.0.policeStation.id"]+'"]').attr("selected", "selected");
        
                    }
                }  
            });
            $("#userEditForm").dialog("open");
           
            var rid = jQuery('#usergrid-table').jqGrid("getGridParam", "selrow");
            if (rid) {
                var row = jQuery('#usergrid-table').jqGrid("getRowData", rid);
   
                $('#edituser').val(row.username);     

                $("#editfirstnameinput").val(row.firstname);
                $("#editlastnameinput").val(row.lastname);
                $("#editemailidpopup").val(row.emailId);
                $("#editphone").val(row.phone);

                var selectedSubdivision=row["userToDepartmentMapping.0.policeStation.parent.id"];
                var selectedLocation= row["userToDepartmentMapping.0.policeStation.parent.parent.id"];
                // $('#locationspopupuser [value="'+row["'userToDepartmentMapping.0.policeStation.value'"]+'"]').attr("selected", "selected"); 
                // $('#locationspopupuser [value="'+ row["userToDepartmentMapping.0.policeStation.parent.parent.value"]+'"]').attr("selected", "selected"); 
                // $('#subdivisionspopupuser [value="'+row["userToDepartmentMapping.0.policeStation.parent.value"]+'"]').attr("selected", "selected");  
                // $('#policeStationpopupuser [value="'+row["userToDepartmentMapping.0.policeStation.value"]+'"]').attr("selected", "selected");

                 $('#editusers [value="'+row["userRole.0.roleId"]+'"]').attr("selected", "selected");
            }
          }); 

               
          $("#resetPasswordForm").click(function () {
            const tokenvalue = sessionStorage.getItem('token')
            $("#editusers").empty();
            $("#editpassword").val();
            $("#editconfirmpassword").val();
            
           // $('#edituser').val(row.username);   
           $("#editconfirmpassword").on('keyup', function(){
            var editpassword = $("#editpassword").val();
            var editconfirmpassword = $("#editconfirmpassword").val();
            if (editpassword != editconfirmpassword){
              $("#editCheckPasswordMatch").html("Password does not match !").css("color","red");
              $("#resetBtn").prop('disabled',true);
            }
             
            else{
              $("#editCheckPasswordMatch").html("Password match !").css("color","green");
              $("#resetBtn").prop('disabled',false);
            }
         
           });

           
            $("#userEditPassworrdForm").dialog("open");
        
            var rid = jQuery('#usergrid-table').jqGrid("getGridParam", "selrow");
            if (rid) {
                var row = jQuery('#usergrid-table').jqGrid("getRowData", rid);
   
              

              //  $("#editconfirmpassword").on('keyup', function(){
              //   var editpassword = $("#editpassword").val();
              //   var editconfirmpassword = $("#editconfirmpassword").val();
              //   if (editpassword != editconfirmpassword){
              //     $("#editCheckPasswordMatch").html("Password does not match !").css("color","red");
              //     $("#resetBtn").prop('disabled',true);
              //   }
                 
              //   else{
              //     $("#editCheckPasswordMatch").html("Password match !").css("color","green");
              //     $("#resetBtn").prop('disabled',false);
              //   }
             
              //  });
               


            }
          }); 
       
          $("#usersaveeditBtn").click(function() {
            $("#userEditForm").dialog("close");
            var rid = jQuery('#usergrid-table').jqGrid("getGridParam", "selrow");
            if (rid) {
                var row = jQuery('#usergrid-table').jqGrid("getRowData", rid);
                var id = row.id;
                var userroleid = row["userRole.0.id"];
                var usertodepartmentid =row["userToDepartmentMapping.0.id"]
            }
            var getuserValue = $('#edituser').val();
            var getfirstnameValue = $('#editfirstnameinput').val();
            var getlastValue = $('#editlastnameinput').val();
            var getemailValue = $('#editemailidpopup').val();
            var getphoneValue = $('#editphone').val();
            var stationselected=$("#policeStationpopupuser").val();
            var userroleselected=$("#editusers").val();
            event.preventDefault();  
            const apiUrl = window.environment.backendUrl +'User/saveuser';
            const requestData = {
                id: id,
                userRoleId: userroleid,
                roleId:userroleselected,
                username: getuserValue,
                firstname:getfirstnameValue,
                lastname:getlastValue,
                emailId:getemailValue,
                phone: getphoneValue,
                token: "string",
                policeStationId: stationselected,
                userRole: [
                  {
                    id: id,
                    userId: userroleid,
                    roleId: userroleselected,
                    isActive: true
                  }
                ],
                userToDepartmentMapping: [
                  {

                    id: usertodepartmentid,
                    userId: userroleid,
                    policeStationId: stationselected,
                    isActive: true,
                    policeStation: {
                      id: stationselected,
                      name: "",
                      value: "",
                      levelId: 0,
                      parentId: 0,
                      orderNo: 0,
                      isActive: true,
                      "parent": "",
                  }
                }
                ]            
              };
            https.postData(apiUrl, JSON.stringify(requestData))
              .then(function (response) {
                  // The API call was successful!
                  if (response.ok) {
                    $("#usergrid-table").trigger( 'reloadGrid' );

                    return response.json();
                  } else {
                    return Promise.reject(response);
                  }
                }).then(function (data) {
                  // This is the JSON from our response
                  console.log(data);
                }).catch(function (err) {
                  // There was an error
                  console.warn('Something went wrong.', err);
                }); 
     
        });


        $("#resetBtn").click(function() {
          $("#userEditPassworrdForm").dialog("close");
          var rid = jQuery('#usergrid-table').jqGrid("getGridParam", "selrow");
          if (rid) {
              var row = jQuery('#usergrid-table').jqGrid("getRowData", rid);
              var id = row.id;
              var userroleid = row["userRole.0.id"];
              var usertodepartmentid =row["userToDepartmentMapping.0.id"]
          }
          // var getuserValue = $('#edituser').val();
          // var getfirstnameValue = $('#editfirstnameinput').val();
          // var getlastValue = $('#editlastnameinput').val();
          // var getemailValue = $('#editemailidpopup').val();
          // var getphoneValue = $('#editphone').val();
          // var stationselected=$("#policeStationpopupuser").val();
          // var userroleselected=$("#editusers").val();
          var editpassword=$("#editpassword").val();
          var editconfirmpassword=$("editconfirmpassword").val();
          event.preventDefault();  
          const apiUrl = window.environment.backendUrl +'User/editpassword?id=id';
          const requestData = {
              id: id,
              password:editpassword           
            };
          // https.postData(apiUrl, JSON.stringify(requestData))
          //   .then(function (response) {
          //       // The API call was successful!
          //       if (response.ok) {
          //         $("#usergrid-table").trigger( 'reloadGrid' );

          //         return response.json();
          //       } else {
          //         return Promise.reject(response);
          //       }
          //     }).then(function (data) {
          //       // This is the JSON from our response
          //       console.log(data);
          //     }).catch(function (err) {
          //       // There was an error
          //       console.warn('Something went wrong.', err);
          //     }); 
   
      });


        //delete form
        
        $("#userdeleteForm").dialog({
            autoOpen : false, modal : true, show : "blind", hide : "blind"
          });

          $("#userdeleteopenForm").click(function () {
            $("#userdeleteForm").dialog("open");
            var rid = jQuery('#usergrid-table').jqGrid("getGridParam", "selrow");
            if (rid) {
                var row = jQuery('#usergrid-table').jqGrid("getRowData", rid);
                $('#subdeletelocationinput').val(row.value);                
            }
          }); 



          $("#userdeleteBtn").click(function() {
            $("#userdeleteForm").dialog("close");
            var rid = jQuery('#usergrid-table').jqGrid("getGridParam", "selrow");
            if (rid) {
                var row = jQuery('#usergrid-table').jqGrid("getRowData", rid);
                var id = row.id;

            }
            var getuserValue = $('#edituser').val();
            var getfirstnameValue = $('#editfirstnameinput').val();
            var getlastValue = $('#editlastnameinput').val();
            var getemailValue = $('#editemailidpopup').val();
            var getphoneValue = $('#editphone').val();
            var stationselected=$("#policeStationpopupuser").val();
            var userroleselected=$("#editusers").val();
            event.preventDefault();  
            const apiUrl = window.environment.backendUrl + 'User/saveuser';
            const requestData = {
                id: id,
                userRoleId: 0,
                roleId:0,
                username: getuserValue,
                firstname:getfirstnameValue,
                lastname:getlastValue,
                emailId:getemailValue,
                phone: getphoneValue,
                token: "string",
                policeStationId: 0,
                isActive: false,
                userRole: [
                  {
                    id: id,
                    userId: id,
                    roleId: 0,
                    isActive: false
                  }
                ]
              };
            https.postData(apiUrl, JSON.stringify(requestData))
              .then(function (response) {
                  // The API call was successful!
                  if (response.ok) {
                    $("#usergrid-table").trigger( 'reloadGrid' );

                    return response.json();
                  }
                  else if(response.status==500){
                    alert("Entity Cannot Be Deleted");
                  }
                   else {
                    return Promise.reject(response);
                  }
                }).then(function (data) {
                  // This is the JSON from our response
                  console.log(data);
                }).catch(function (err) {
                  // There was an error
                  console.warn('Something went wrong.', err);
                }); 
     
        });
       
    }
    
  };
  
  export default usermanagement;
  