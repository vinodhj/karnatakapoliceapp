const Navbar = {
  /**
   * Render the component content.
   */
  render: async () => {
    // Define a list of navbar links.

    const token = sessionStorage.getItem('token');
    const role = sessionStorage.getItem('roleid');
    if(role==1 && token){

      const links = [ 'Templates','Division','Sub-Division','Police-Station','User Management', 'ViewForms' ];
      // Build html with navigation links.
      const navLinks = links
        .map(
          link =>
            /*html*/ `<li class="nav-item"><a class="nav-link" href="/#/${link.toLowerCase()}">${link}</a></li>`
        )
        .join('\n');
      // return /*html*/ `
      //   <nav class="navbar navbar-expand-md">
      //     <a class="navbar-brand-custom">
      //       <img src="../logo.png" width="70" height="70" alt="Breaking Bad">
      //     </a>
          // <a>
          // <button class="logoutclass" id="logout">Logout</button>
          // </a>
      //     <ul class="navbar-nav">
      //       ${navLinks}
      //     </ul>
      //   </nav>
      // `;
      return /*html*/ `
      <nav class="navbar navbar-expand-lg ">
      <img src="../logo.png" width="70" height="70" alt="Breaking Bad">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" href="/#/viewtemplates">Templates</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/#/forms">Forms</a>
      </li>
          <li class="nav-item">
          <a class="nav-link" href="/#/locations">Division</a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="/#/subdivision">Sub-Division</a>
      </li>
      </li>
      <li class="nav-item">
      <a class="nav-link" href="/#/policestation">Police-Station</a>
      </li>
          <li class="nav-item">
            <a class="nav-link" href="/#/usermanagement">User Management</a>
          </li>
        </ul>
        <form class="d-flex flex-row-reverse">
        <img src="../examroom-logo.svg" width="150" height="25" alt="Examroom.AI" style="margin-top:1rem">
        <a class="p-2"" id="logout" type="submit">Logout</a>
      </form>
        
      </div>
    </nav>
    `;
    }
    else if(role ==2 && token){

      const links = ['ViewForms','Templates'];
      // Build html with navigation links.
      const navLinks = links
        .map(
          link =>
            /*html*/ `<li class="nav-item"><a class="nav-link" href="/#/${link.toLowerCase()}">${link}</a></li>`
        )
        .join('\n');
        return /*html*/ `
      <nav class="navbar navbar-expand-lg">
      <img src="../logo.png" width="70" height="70" alt="Karnataka State Police">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav">
          <li class="nav-item active">
            <a class="nav-link" href="/#/forms">Forms </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/#/templates" active>Templates</a>
          </li>
        </ul>
        <form class="d-flex flex-row-reverse" style="align-item:center;">
        <img src="../examroom-logo.svg" width="150" height="25" alt="Examroom.AI" style="margin-top:1rem">
        <button class="btn class="p-2"" id="logout" type="submit" style="margin-top:1rem; color:#fff;">Logout</button>
        
      </form>
        
      </div>

    </nav>
    `;
    

    }
    else {
      const links = [''];
      // Build html with navigation links.
      const navLinks = links
        .map(
          link =>
            /*html*/ `<li class="nav-item"><a class="nav-link" href="/#/${link.toLowerCase()}">${link}</a></li>`
        )
        .join('\n');
      return /*html*/ `
        <nav class="navbar navbar-expand-md">
          <a class="navbar-brand-custom">
            <img src="../logo.png" width="70" height="70" alt="Breaking Bad">
          </a>
          <ul class="navbar-nav">
            ${navLinks}
          </ul>
          <img src="../examroom-logo.svg" width="150" height="25" alt="Examroom.AI" style="margin-top:1rem; margin-left:105rem">
        </nav>
      `;

    }
 
  },
  /**
   * All the code related to DOM interactions and controls go in here.
   * This is a separate call as these can be registered only after the DOM has been painted.
   */
  after_render: async () => {
    $("#logout"). click(function() {
      sessionStorage.removeItem('token');
      sessionStorage.removeItem('roleid');
      window.location.href="/#";

    });
   }
};

export default Navbar;
