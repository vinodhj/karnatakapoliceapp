const Footer = {
  /**
   * Render the component content.
   */
  render: async () => {
    return /*html*/ `
      <p id="footerPTag">&#169; 2021-2022 ExamRoom.AI All Rights Reserved
      </p>
     
    `;
  },
  /**
   * All the code related to DOM interactions and controls go in here.
   * This is a separate call as these can be registered only after the DOM has been painted.
   */
  after_render: async () => {
    // Select a node that will contain the clock and date.
  
    /**
     * Set inner html of selected node to current time and update it every second.
     */
  
  }
};

export default Footer;
